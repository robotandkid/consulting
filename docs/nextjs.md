# NextJS

## Markdown

- https://nextjs.org/blog/markdown
- Use either `remark` and `remark-html` (see [blog-starter](https://github.com/vercel/next.js/tree/canary/examples/blog-starter)) OR
- use MDX

### MDX

- use `@next/mdx` and `@mdx-js/loader` to treat MDX files as pages...
- but have to use custom front-matter loader for YAML support
- use `next-mdx-remote` to fetch MDX from a remote source.
- use `MDXProvider` for theming MDX pages (from local)
