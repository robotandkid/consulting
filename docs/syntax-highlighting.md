# Syntax Highlighting

- See:

  - codeBlocks.mdx
  - next.config.js

- Under the hood, it uses the rehype plugin to add CSS classes to code blocks,
  that is, styling at the SPAN level.
- It was disappointing since it's mainly for just changing
  colors/italics/bold/size.
- If you want to get creative and do something like add styling at the BLOCK
  level, it can be next to impossible.
- A second issue is that there are just so many different CSS classes to style.
  The first is rather incomplete and it's an ongoing process.
