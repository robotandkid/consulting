# SEO

## Checklist

- [ ] build a site map

  - you might need a sitemap if:
    - Your site is really large (+500 pages)
    - Your site is not comprehensively linked internally, that is, many pages
      are isolated are not well linked to each other
    - Your site is new
  - [Sitemaps](https://developers.google.com/search/docs/advanced/sitemaps/overview)
  - [Sitemap format](https://www.sitemaps.org/protocol.html#sitemapXMLExample)

- [ ] create unique, accurate page titles
  - A title is the topic of the page.
  - Can list the name of your website/business
  - Can include other bits of important information like the physical location
    of the business or maybe a few of its main focuses or offerings
  - Titles can be both short and informative.
  - Example: `Brandon's Baseball Cards - Buy Cards, Baseball News, Card Prices`
- [ ] add a meta description
  - summary of what the page is about
  - can be a sentence or two, or even a short paragraph.
  - _might_ be used as snippets for your pages in search results (so should be
    long enough to show up in search results)
  - should be unique
  - Example: `<meta name="description" content="Brandon's Baseball Cards provides a large selection of vintage and modern baseball cards for sale. We also offer daily baseball news and events.">`
- [ ] use heading tags to emphasize important text
- [ ] add structured data markup
  - See
    [here](https://developers.google.com/search/docs/advanced/structured-data/intro-structured-data)
  - Supported
    [types](https://developers.google.com/search/docs/advanced/structured-data/search-gallery)
  - [Rich Results Test](https://search.google.com/test/rich-results) for validating markup.
  - [LocalBusiness schema](https://schema.org/LocalBusiness)
  - You can mark up many business-relevant entities:
    - Products you're selling
    - Business location
    - Videos about your products or business
    - Opening hours
    - Events listings
    - Recipes
    - Your company logo, and many more!
  - Use the [Rich Results Test](https://search.google.com/test/rich-results) to validate markup
- [ ] Organize your site hierarchy
  - use meaningful URL slugs
- [ ] Use meaningful (i.e., not generic) ALT tags
- [ ] Use short anchor text
- [ ] Use the [meta
      name="viewport"](https://developer.mozilla.org/en-US/docs/Web/HTML/Viewport_meta_tag)
      tag

  ```html
  <meta
    name="viewport"
    content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1"
  ></meta>
  ```

- [ ] add faviicon
- [ ] ensure it passes the security audit in Webpagetest.org

## Maintenance (ongoing

- [ ] ensure it passes the security audit in Webpagetest.org
- [ ] [Rich Results Status
      Reports](https://support.google.com/webmasters/answer/7552505)

## Tools

- [Search Console](https://search.google.com/search-console)
- [URL inspector](https://search.google.com/search-console/welcome)
- [Mobile Friendly Test](https://search.google.com/test/mobile-friendly)
- [Keyword Planner](https://ads.google.com/home/tools/keyword-planner/)
- [Page Speed Insight](https://developers.google.com/speed/pagespeed/insights/)
- [Webpagetest.org](https://www.webpagetest.org/)

## Resources

- [SEO Starter
  Guide](https://developers.google.com/search/docs/beginner/seo-starter-guide)
- [Google Webmaster
  Guidelines](https://developers.google.com/search/docs/advanced/guidelines/webmaster-guidelines)
- [Google Search Central](https://developers.google.com/search/)
- [The basics of JS SEO](https://developers.google.com/search/docs/guides/javascript-seo-basics)
- [Localization](https://developers.google.com/search/docs/advanced/crawling/localized-versions)
- [CSP](https://developer.mozilla.org/en-US/docs/
  .google.com/search/docs/advanced/appearance/good-titles-snippets)
- [Structured Data](https://developers.google.com/search/docs/guides/intro-structured-data)
  - <https://developers.google.com/search/blog/2007/09/improve-snippets-with-meta-description>
