# Icons

- [Free SVGs](https://www.svgrepo.com)
- [Convert SVG to react icon](https://react-svgr.com/playground/?icon=true&titleProp=true&typescript=true)
