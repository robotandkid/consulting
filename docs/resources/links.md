# Links

- [Performant Web Fonts](https://leerob.io/blog/fonts)
- [MDX Code Highlighting](https://ped.ro/blog/code-blocks-but-better)
- [Intro to jscodeshift](https://feifan.blog/posts/an-introduction-to-jscodeshift)
  - [AST Playground](https://astexplorer.net/)
- docs
  - docusaurous
  - https://www.11ty.dev/docs/
