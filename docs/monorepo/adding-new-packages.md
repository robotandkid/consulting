# Adding New Package to the Monorepo

## TODO Checklist for Improvement

Add to template:

- [ ] landing hero
- [ ] fonts
- [ ] i8n
- [ ] SEO
- [ ] styled components
  - [ ] AppThemeProvider
  - [ ] GlobalStyle
- [ ] nextjs
  - [ ] app page
  - [ ] document page
