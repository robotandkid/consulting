# Architecture Overview

## Why Lerna?

- It comes down to having a tool chain that I can understand.
- Compare with Nx---while it's super easy to get started, going outside the
  blessed path is hard. You basically have to learn this custom template DSL.
- Ditto Rush.
- What I like about Lerna is that it can be just Javascript (or Python or bash or
  \<insert favorite language\> under the hood).
- Lerna is good enough for my use case.

## Use Case

- The monorepo is a few component packages consumed by various apps.

```mermaid
classDiagram
Component2 --> Component1
App1 --> Component1
App2 --> Component2
App3 --> Component1
App3 --> Component2
```

## Gotchas

### Use cmdAddPackage/cmdUpgradePackage to add packages to a workspace

- The reason is because you'll need to pin packages at specific versions. For.
  Every. Package.
- I hit a weird issue where the component library used v5.2.1 of
  `styled-components`. (The package JSON requested `^5.2.1`.)
- The app requested v5.2.3 (via `^5.2.3`)
- I would have expected the component to use 5.2.3.
- Unfortunately, both versions were used. /shrug
- so I added `cmdAddPackage` (more about these commands/tasks below)
- what this does is use `yarn list` under the hood to figure out which version of a package is installed
- then it will install that package/version

### cmdLernaChangedRunTests runs tests on workspaces and its dependencies _when they change_

- Under the hood this uses `lerna changed`
- then it iterates thru those workspaces, running its tests

### You have to build your own CLI commands/tasks

- At first this was hard but then I realized that I can use whatever tool I want!
- I stumbled on [Clipanion](https://mael.dev/clipanion/), a typesafe library
  used to build CLI apps.
- It's used by `yarn` to build it's CLI.
- And then I realized I could also use [template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) to run bash scripts, styled-components style!

```typescript
bash`
  echo "${JSON.stringify(foobar)}" > ${rootDir}/foobar.json
`;
```

- putting clipanion to build a CLI plus template literals for executing tasks resulted in a simple way to build tasks

## App Directory Structure

- All apps have pages.
- Some apps use MDX
- The problem is co-location. You want related files to be near where they are used.
- So this doesn't work:

```bash
pages
   blog1.mdx
   blog2.mdx
src
   components
      blog1
        component1.tsx
        component2.tsx
      blog2
        component1.tsx
      shared
        component.tsx
```

The reason the above doesn't work is because, for example, `blog1.mdx` will likely change along with all it's components. Another issue is that `components` becomes a grab bag of single-use components.

- Alternatively, we can mix pages with components:

```bash
src
  pages
    blog1
       index.mdx
       component1.tsx
       component2.tsx
    blog2
       index.mdx
       component1.tsx
  components
     shared1 <-- shared by at least 2 components!
  utils <-- ditto

```
