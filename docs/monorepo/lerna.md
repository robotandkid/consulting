# Lerna

## Resources

- https://doppelmutzi.github.io/monorepo-lerna-yarn-workspaces/
- https://classic.yarnpkg.com/en/docs/workspaces/

## Monorepo Build on Netlify

1. Get an app/component's list of dependencies
2. Translate package names to directories
3. Figure out if any of these have changed since the last commit.

## Get the dependency list for the monorepo

```bash
yarn lerna --graph --all
```

## Translate package names to directories

```bash
yarn lerna ls --all --long --json # OR --parseable
```

## Figure out if packages have changed since last commit

Can use `lerna diff` or `lerna changed` but that requires you to call this before running `lerna publish`.

```bash
git diff --name-only HEAD bb62649 apps/urielavalos apps/leander-tile
```

## Alternatively

It's a two-step process

```bash
# first get the dependency graph for the app/component
yarn lerna ls --all --graph
# then check to see if the dependencies changed
# since the given commit
yarn lerna ls --all --long --parseable --since 5167c9b
```

If lerna is setup correctly, it will ignore doc changes!!!

This doesn't work, despite the docs.

```bash
yarn lerna ls --scope "@robotandkid/urielavalos" --all --long --json --include-dependencies --since 5167c9b
```

### Resources

- https://dev.to/hipstersmoothie/javascript-monorepo-tooling-48b9
- https://www.npmjs.com/package/@mono-repo/utils

## Plugin

- <strike>set the baseDirectory (may have to edit commands</strike> This requires that you actually publish packages. There may be a way to do this.
  - plugin will receive packageJson as argument
- fail a build: `utils.build.cancelBuild('message')`
- the app netlify config looks like this:
- the reason is because if you set the base directory as the app directory, all dependencies must be published somewhere.
- that means every app installs the root workspace. Long term is definitely tech
  debt. Short term there are only a handful of extra deps on each app (so we're
  fine!)
- that means every app will have the root netlify config and root package.json. That means we'll need to jump through extra hoops to get app meta data.

### Resources

- https://docs.netlify.com/configure-builds/build-plugins/create-plugins/
