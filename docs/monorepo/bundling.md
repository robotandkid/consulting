# Bundling

Side-effect optimization is turned on by via `"sideEffects": false` declaration
in the library package json.
This will assume modules do NOT have side effects and so the following will strip `import "extras"`:

```typescript
import "extras";
import React from "react";

function Foo() {
  return <div>Hello</div>;
}
```

You can disable side effect optimization on a per module basis in the rollup config file:

```javascript
export default {
  //...
  treeshake: {
    // This tells rollup NOT to drop
    // `import "extras"`
    moduleSideEffects: ["extras"],
  },
};
```
