# Team

## How to Ask Good Questions

It's really hard to communicate, especially on a distributed team. This is all the more reason to focus on this aspect of the job. I think one of the biggest parts of communication you can work on is asking questions. Asking questions is good, but there IS such a thing as a bad question. And I absolutely DO think we should try to only ask good questions.

I have some suggestions for asking good questions that I have either collected from my experiences or taken from other people far more experienced than me. I'll describe them in chunks below and I will tailor my suggestions to helping you improve your question asking skills.

(Note: this advice is tailored towards complex questions. Simple questions like "I think UAT is down, can anyone else verify?" in the room booking channel are fine. I'm specifically speaking about questions like: "this endpoint appears to be broken, can anyone look at this?")

### Due Diligence

A lot of times questions are asked internally, or to the client, that don't seem to have the proper work done before hand when the questions are asked. In many cases, these questions could be filtered out before they even get asked.

When you ask a question, it is beneficial to all parties involved to make sure that you have exhausted all avenues and resources available to you before you pull in someone else in. This is for two reasons: 1) you build up trust with others that you are asking questions that actually require outside help and 2) so you don't end up wasting time by asking questions that could be answered with more up front due diligence.

If people start to see a pattern of questions asked without the proper dues paid, that will strain those relationships. We especially want to avoid this in client channels. People want to help, but they also want to be sure they're helping you because you need it and not just because the question was raised before you fully tried to answer it yourself.

A final note on due diligence: do not rush to claim you've found a bug unless you are VERY sure. This is doubly important for when we are speaking in client channels. If you are running into an issue, first try to resolve it. If that doesn't work figure out why it's happening. If you can't do that, that's the time to form a question and see if you can get help.

### Clarity

One of the major areas I think you should focus on is clarity. Assuming you have done all the due diligence for your question, it's now time to focus on the formation of the question. For questions of a complex nature you need to be precise, informative, and succinct with the problem. You should describe the problem carefully and clearly. The details you should include are:

- symptoms of the bug/issue
- environment this is occurring in research done to understand the problem
- diagnostic steps takes to solve/understand the problem before you asked the
  question
- any recent or relevant changes to your system that might affect this issue
  (this is not always easy to come up with)
- detailed reproduction steps (I navigated to xxx, logged in with yyy, clicked
  zzz, and saw ...)

Another thing to remember: volume is opposite succinctness
and precision. If you provide TOO much information it will be difficult to
focus on the relevant information. If you are concerned with a typing issue,
provide the following:

- why do you need this type
- where is it being used (branch with changes you are trying out)
- a reproduction scenario (playground link)

If you also provide a bunch of screenshots of the code, or screenshots of
errors, you're just detracting from the useful information and making it harder
to start looking at your actual problem. Focus on quality of information over
quantity.

Speaking of screenshots, screenshots of code are almost NEVER a good way to communicate details, as you now only have a static window into part of the problem without context or explanation. It also becomes difficult for anyone to replicate your problem or check for typos, since they can't properly copy/interact with what you are showing. Always provide code in snippets/branches/playground-style links.
