# Shaders

Shaders are written in GLSL (OpenGL Shading Language). Not to be confused with
_vertex shaders_.

- Send data to the shader via **uniforms**.
- **vec2 vTextureCoord** - the position of the current pixel.
- **sampler2D uSampler** - the image data
- **texture2D(imaga data, point)** - returns the color of the pixel in the image
  at that point.
  - **gl_FragCoord** - gives us access to the pixel the shader is running on

## Usage

- PixiJS does not seem to like a uniform with the name `resolution`. Use
  `u_resolution` instead.
- PixiJS seems to prefer `Point`s for `uniform vec2`s.

## ShaderToy

Note: even though the inputs are specified in WebGL 1.0, they are written with
WebGL 2.0.

### Inputs

```frag
uniform vec3 iResolution;           // viewport resolution (in pixels)
uniform float iTime;                 // shader playback time (in seconds)
uniform vec4 iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
out vec4 fragColor;
```

Src: <https://stackoverflow.com/a/66733778/555493>

- There is are `varying` or `attribute` keywords. Instead:
  - attribute => in
  - varying => out
- There is no `gl_FragColor`. Instead, declare an `out` variable:
  - `out vec4 fragColor;`
- [How the mouse coordinates work in shadertoy](https://www.shadertoy.com/view/llySRh)
- textures:
  - by default, mipmap is enabled, wrap_mode, and VFLIP (which is just 1.0 -
    p.y, where p is the loaded texture)
- Another big change is that colors are between 0 and 1.

### Program

- declare `#version 300 es` in the very top of the file. **Make sure there is no
  whitespace.**
- with a PixiJS Filter, both the vertex shader and the fragment shader need to
  declare `#version 300es`.

## Resources

- [Tutorial](https://www.html5rocks.com/en/tutorials/webgl/shaders/)
- [ShaderToy](https://stackoverflow.com/questions/24820004/how-to-implement-a-shadertoy-shader-in-three-js)
- <https://www.shadertoy.com/browse>
- [PixiJS examples](https://pixijs.io/examples/#/filters-advanced/shadertoy-filter-rendertexture.js)
- [Creating your own](https://thebookofshaders.com/)
- <https://glslsandbox.com>
- [Gentle Intro to PixiJS
  Shaders](https://www.awwwards.com/a-gentle-introduction-to-shaders-with-pixi-js.html)

## Pin-Ups

- [Traveling Wave Fronts](http://webglplayground.net/share/traveling-wave-fronts?gallery=1&fullscreen=0&width=800&height=600&header=1)
- [Cyber Fuji](https://www.shadertoy.com/view/Wt33Wf)
- [Base Warp (pink)](https://www.shadertoy.com/view/tdG3Rd)
- [Simplicity Galaxy](https://www.shadertoy.com/view/MslGWN)
- [Rectangle City](https://glslsandbox.com/e#81130.0)
- [Voxel Edges](https://www.shadertoy.com/view/4dfGzs)
- [Clouds](https://www.shadertoy.com/view/XslGRr)
- [Torus](https://www.shadertoy.com/view/7dcyRX)
- [Van Goh](https://www.shadertoy.com/view/fstyD4)
