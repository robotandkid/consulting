# i8n

## i8n library

- tried using lingui but it was buggy and also under staffed.
- so switched to [i18next](https://react.i18next.com/getting-started)
- it's kinda big though---coming in around 17kb usage.

## Setup

### Client

- Inject the i18n Provider in the app template. According to the [official
  docs], we may not need to use the Provider component. Or [do
  we?](https://react.i18next.com/latest/ssr)
- Load the current locale in the app template.
- Generate a typed version of useTranslation from the translations. Is there a
  way to generate this so that all the translations are _not_ loaded in the
  client?

### SSR Server

```json
// next.config.js
  i18n: {
    defaultLocale: "es",
    locales: ["en", "es"],
  },
```

- Configure i8n via getStaticProps, meaning that you'll load all translations
  here.
- Load the provider via getStaticProps.

**Note:** Try not to load all translations in the client. i.e., use
getStaticProps to inject the current translation dynamically.

**Note:** I haven't been able to figure out how to get this working yet

### Netlify

```bash
Build command           cd apps/leander-tile && yarn next build
Publish directory       apps/leander-tile/.next
```

- Ensure Netlify redirects are disabled. The reason is because nextJS i18n
  routing uses rewrites, and the current docs say to prefer nextJS rewrites over
  netlify redirects.
- Ensure that you're using the latest nextjs plugin on netlify! (See
  <https://github.com/netlify/netlify-plugin-nextjs>)
- The above ensures Nextjs runs as a server.

## Notes

- In theory, we could use Nelitfy redirects by disabling nextjs i18n rewrites.
- However, there's currently a bug that keeps us from disabling i18n rewrites.
- This approach is preferred since it wouldn't require a Nextjs server.

## Appendix

- In netlify, the `_redirects` needs to live in the "public" directory
