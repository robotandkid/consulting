# Theme Organization

- We mostly follow [this
  pattern](https://medium.com/welldone-software/css-theme-organization-best-practices-a8e375d92c7c)
  - level 1: colors, fonts
  - level 2: concepts
  - level 3: entities
- `concepts` should consume only level 1 objects.
- `entities` should consume only `concepts`.

  - Initially, I tried listing specific properties (ex: font-size for headers),
  - However, this approach proved cumbersome to consume.
  - And so I switched to defining `css` styled-component objects. They are
    defined as functions because of a typescript limitation (as in _my_
    typescript limitation). Ex:

    ```typescript
    const entities = {
      landing: () => css`
        font-family: ${concepts.primary.font};
        color: ${concepts.primary.color};
      `,
    };
    ```

## Caveats

- Components should consume only `entities`.

## Limitations

- This pattern allows for color and font overrides _but not size and spacing
  overrides_.
