# Framer Motion

## Notes

- Today 8/4/2021 I saw the strangest bug. Here's the diff

```diff
@ apps/urielavalos/src/components/UrielAvalosLandingHero/UrielAvalosLandingHero.tsx:16 @ import { sharedTitleStyle } from "../../styles/styles";

const smallTitleFontSize = "2rem";

-const Title = styled(motion.h1)`
+const Title = styled(FullHero)`
  ${sharedTitleStyle}
  text-align: center;
  font-size: 8rem;
```

- The strangeness is that FullHero is just another `motion.h1`, so
  it's completely unnecessary.
- I investigated possibly running two versions of the library but that
  was not the case.
- I suspect it is an issue with rollup and how it builds the component
  library.
