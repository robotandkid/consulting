# Design

- [Raw Magazine](https://brutalistwebsites.com/raw/)

## Inspiration

- <https://nopattern.com>
- <https://www.pw-magazine.com/essentials>
- <https://www.baugeld-spezialisten.de/>
- <https://creativeshowoff.nl/#bureaus>
- <https://theoutline.com/>
- <http://wekflatm.kr/?ckattempt=2>

## Main Website

- <https://www.purho.it/en/collection/> - what I love about this is the bold
  type. Giant type for the text. Also, not gonna lie, love the mouseover pastels
  in the gallery.
- <https://grafik.co.nz/project/public-library/>
  - that Loading spinner is amazing!
  - i love the mouseovers on desktop. THe main downside is that on mobile it's
    almost impossible to figure out what to click.
  - wow wow wow. Look at the landing hero and gallery - <https://grafik.co.nz/>
  - also, i love the loaded/loading spinner
- <https://www.studiogruhl.com/about-us> - this About page is nice yet simple
- <https://3hd.tv/> - love the crazy video background
- <http://hman.love/> - love the spinning call to action button. Can you do this
  with CSS?
- <http://kunsthallezurich.ch/en>

## Photo resources

- <https://www.pexels.com/photo/young-grain-5865/>
