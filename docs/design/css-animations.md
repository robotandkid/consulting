# Animations

## Parallax Effect

- <https://samuelkraft.com/blog/spring-parallax-framer-motion-guide>

## Moving Gradient

- [Technique 1: Moving Gradient](https://www.gradient-animator.com/)
- [Technique 2: CSS Blend Modes](https://highrise.digital/blog/css-blend-modes/)

## 3D Animations

- <https://3dtransforms.desandro.com/>
