# System Architecture

## Overview

- <https://github.com/checkcheckzz/system-design-interview#-hot-questions-and-reference>
- <https://www.youtube.com/watch?v=UzLMhqg3_Wc>
- [How to Design TinyURL](https://www.youtube.com/watch?v=fMZMm_0ZhK4)

## Process

- Features
- Define APIs
- Availability
- Latency / performance
- Scalability
- Durability
- Class Diagram
- Security & Privacy
- Cost effectiveness

## Concepts

- Vertical/horizontal scaling
- CAP Theorem
- Data

  - ACID vs BASE
  - Relational DB vs NoSQL
  - Partitioning/sharding data
  - Optimistic vs Pessimistic locking
  - Strong vs Eventual consistency
  - Caching
  - Data center/racks/hosts
  - Distributed Database Design
    - most important by order:
      1. durability - not lose customer data
      2. availability - usually storing data in a slow way is preferable
      3. performance
    - strongly consistent

- Types of NoSQL
  - key/value
  - wide column
  - document
  - graph based
- Consistent hashing
- CPU / memory / hard drive / network bandwidth
- Random vs sequential read/write on disk
- network
  - http vs http2 vs websockets
  - TCP/IP model
  - ipv4 vs ipv6
  - TCP vs UDP
  - DBS lookup
  - https & TLS
  - public key infrastructure & certificate
  - symmetric vs assymmetric encryption
  - Load balancer -> L4 vs L7
  - CDNs & Edge network
- bloom filters & count-min sketch
- PAXOS - consensus over distributed hosts
  - leader election
- Design patterns & OOP
- virtual machines & containers
- pubsub over queue
- map reduce
- multi-threading concurrency, locks synchronization, CAS

## Tools

- Cassandra
- MongoDB / couchbase
- mysql
- memcached
- redis
- [Zookeeper](https://www.instaclustr.com/apache-zookeeper-meets-the-dining-philosophers/)
  - designed to store metadata configuration in a distributed system. Optimized
    for Consistency, Availability, Performance. Optimized for reads.
- Kafka
- NGINX
- HAProxy
- Solr, elastic search
- Blobstore like Amazon S3
- Docker / kubernetes / MesOS
- Hadoop/ spark / HDFS

## CDN

- <https://kilthub.cmu.edu/articles/journal_contribution/Globally_distributed_content_delivery/6605972>

## Google MobWrite

- <https://code.google.com/archive/p/google-mobwrite/wikis/Theory.wiki>
