# Startup

- [Inspiration](https://www.oakharborwebdesigns.com/)
  - [Reddit thread](https://www.reddit.com/r/webdev/comments/i3zb0p/how_should_i_begin_getting_in_contact_with/g0f67el/?utm_source=reddit&utm_medium=web2x&context=3)
- what's it going to cost? 2k is too much, so try lower... say $100 a month
- build a portfolio
- show them the code and how the site changes for different sizes (and language)
- explain how it is super fast and Google likes this! Google ranks sites by mobile performance
- be very selective of clients---so that they don't bail on you the first month!
  Find ones w/ good reviews, quality work...
- most small business edits will never be complicated
- tell them how much it costs to make---3k. That cost is baked into the $150.
- hire a SEO partner on demand
- direct line to me
- bring down cost the first year if they can't pay
- customer owns the domain. But I keep the code. If you want the code, then its
  2.5k
- the model is a win-win
  - sell a relationship
  - $150 investment that can turn into a $800 profit.
  - client gets a nice/performant website from an expert. Business doesn't need
    a retainer with an agency for maintenance/changes. Developer gets a steady
    cash/flow.
- flaticon.com
