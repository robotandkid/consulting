# Instagram Feed

**Note:**

- problem: instagram basic app doesn't let you download images. Probably the
  only thing it lets you do is render them from the app website. This is a
  problem because you then can't cache the images and/or transform them. So your
  app suffers from performance issues.
- problem: instagram throttles users when they try to download images. When I
  visited `leandertile`'s public profile, they only let me download the public
  images about 3 times. That means that while I could probably get away with
  supporting a single client. I won't be able to reliably download the public
  profiles of a dozen clients.
  - Well I could. But it's basically an arms race. I'll likely need a VPN. Multiple instagram accounts.
- problem: I'll have to use oEmbed! This requires an approved app.

For now, I'll manually copy client instagram photos until I've setup oEmbed.

## Instagram Basic App

The step will likely change in the future but as the time of this writing, the steps are:

- Set up a facebook developer account.
- Follow [these
  steps](https://developers.facebook.com/docs/instagram-basic-display-api/getting-started)
  to setup an Instagram app.
- You'll need to know how to [get a long-lived token](https://developers.facebook.com/docs/instagram-basic-display-api/getting-started)
- Submit app?

### Token flow

- Invite client as instagram test user
- After client accepts, they will be redirected to app page which collects their token
- serverless function will upgrade token to long-lived token
- and store it in Firebase

**Notes:**

1. An important caveat is that Netlify functions are limited to 100 hrs each
   month...for all sites. See here:
   <https://www.netlify.com/products/functions/>. This _should_ be OK---we're
   just going to use that to pre-render Instagram posts _each build_. So maybe a
   few secs of usage each build.

## Script to Download Instagram Photos

This script will download all images on the current web page.

**Warning:** instagram throttles this use and so it can be used probably only a few times.

```javascript
var images = document.getElementsByTagName("img");
var srcList = [];
var i = 0;

for (i = 0; i < images.length; i++) srcList.push(images[i].src);

i = 0;

setTimeout(function _downloader() {
  if (i < srcList.length) {
    var link = document.createElement("a");
    link.id = i;
    link.download = srcList[i];
    link.href = srcList[i];
    link.target = "_blank";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    i++;
  }
  setTimeout(_downloader, 5000);
}, 5000);
```

The cons are:

- cypress won't (cheaply) run in netlify
- so cypress will have to run locally in a raspberry pi or something like that
- it'll download the files locally then upload to an image CDN, where they can
  be served properly.
