# MDX

## Architecture

- Basically, this exists because of the DX around writing content.
- This provides:
  - an easy way to write arbitrary code blocks.
  - meta information for each post that can be used while building the app

### ROI

- The configuration/setup is actually very expensive. On top of that, there's
  poor community support. So to manage a few content files, the ROI is **NOT**
  there.

## And Typescript

- Typescript support is non-existent.
- As a work around I'm following this pattern:
  - create a component (with typescript)
  - consume that component in MDX
  - inject that component via the MDX provider

```typescript
// Component.tsx
export function Component() {
  // ...
}
```

```mdx
Hello world

<Component />
```

```typescript
import { Component } from "./Component";

export const components = {
  wrapper: Wrapper,
  h1: (props: any) => <Title {...props} as="h1" />,
  // ...,
  Component,
};
```

## Workarounds

- You may have to insert `&nsbp;` (non-breaking spaces) when it adds new-lines incorrectly
- You may have to insert empty `<p></p>` blocks for the same reason
