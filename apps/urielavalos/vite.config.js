/** @type {import("vite").UserConfig} */
export default {
  publicDir: "public",
  define: {
    "process.env": process.env,
  },
};
