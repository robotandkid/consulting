import { defineConfig } from "@pandacss/dev";
// import { themePreset } from "@robotandkid/sprite-motion";

export default defineConfig({
  // presets: [themePreset],

  clean: true,
  // Whether to use css reset
  preflight: false,

  // Where to look for your css declarations
  include: ["./src/**/*.{js,jsx,ts,tsx}", "./pages/**/*.{js,jsx,ts,tsx}"],

  // Files to exclude
  exclude: [],

  // Useful for theme customization
  theme: {
    extend: {
      tokens: {
        colors: {
          black: { 100: { value: "black" } },
          customGray: { 100: { value: "#a9a9a9" } },
          dark: { value: "#081820" },
          "sprite-motion-gb": {
            50: { value: "#081820" },
            51: { value: "#346856" },
            52: { value: "#88c070" },
            53: { value: "#e0f8d0" },
          },
        },
        fonts: {
          "sprite-motion": {
            value: "mrpixel, monospace",
          },
          comic: {
            value: "hey-comic, monospace",
          },
        },
      },
      keyframes: {
        blink: {
          to: {
            visibility: "hidden",
          },
        },
        pulse: {
          "0%": {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore should work
            transform: "scale(0.5)",
          },
          "100%": {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore should work
            transform: "scale(1.5)",
          },
        },
      },
    },
  },

  // The output directory for your css system
  outdir: "styled-system",
  jsxFramework: "react",
});
