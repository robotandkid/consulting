# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.97.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.97.2...@robotandkid/urielavalos@0.97.3) (2025-03-05)


### Bug Fixes

* **urielavalos:** compiles again ([e21f213](https://gitlab.com/robotandkid/consulting/commit/e21f213ea42389a6af59ecc81473a47bf0aef35a))





## [0.97.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.97.1...@robotandkid/urielavalos@0.97.2) (2025-03-05)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.97.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.97.0...@robotandkid/urielavalos@0.97.1) (2025-03-05)


### Bug Fixes

* **urielavalos:** compiles again bitches ([3b7b511](https://gitlab.com/robotandkid/consulting/commit/3b7b511dce7fe6b6175b0de687cea645aee133a0))





# [0.97.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.96.0...@robotandkid/urielavalos@0.97.0) (2024-10-20)


### Features

* **urielavalos:** added portal demo ([3fb490f](https://gitlab.com/robotandkid/consulting/commit/3fb490fbf1e27d6f708ad572f5d53225c7113ec4))
* **urielavalos:** added watermark ([5d61436](https://gitlab.com/robotandkid/consulting/commit/5d61436a91ca60eca622586f33346e7a298c7e63))





# [0.96.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.95.0...@robotandkid/urielavalos@0.96.0) (2024-10-05)


### Features

* **urielavalos:** tshirt club 001 ([94edfed](https://gitlab.com/robotandkid/consulting/commit/94edfede029860a9066f274cc4b7575a071d2e67))





# [0.95.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.94.0...@robotandkid/urielavalos@0.95.0) (2024-10-04)


### Bug Fixes

* **urielavalos:** compiles again ([c5fc228](https://gitlab.com/robotandkid/consulting/commit/c5fc228ea4ea01890f21eaeb19448a9178feb291))


### Features

* **urielavalos:** tshirt design 01 ([bf2ed58](https://gitlab.com/robotandkid/consulting/commit/bf2ed584fa0c4263ddd48307f05e773856fd716d))





# [0.94.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.93.2...@robotandkid/urielavalos@0.94.0) (2024-10-03)


### Features

* **urielavalos:** added happy manipulative relationship ([1158885](https://gitlab.com/robotandkid/consulting/commit/11588851aa960992dad3c710fd0dd0858f9a9c7a))





## [0.93.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.93.1...@robotandkid/urielavalos@0.93.2) (2024-10-03)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.93.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.93.0...@robotandkid/urielavalos@0.93.1) (2024-10-01)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.93.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.92.1...@robotandkid/urielavalos@0.93.0) (2024-10-01)


### Features

* **urielavalos:** added shirt club design [#1](https://gitlab.com/robotandkid/consulting/issues/1) ([8f5ea43](https://gitlab.com/robotandkid/consulting/commit/8f5ea435034b5ce103c286e3c9550188f9bd3745))





## [0.92.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.92.0...@robotandkid/urielavalos@0.92.1) (2024-09-28)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.92.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.91.0...@robotandkid/urielavalos@0.92.0) (2024-09-27)


### Features

* **urieaavalos:** added 9/01 comic ([4d1f36e](https://gitlab.com/robotandkid/consulting/commit/4d1f36e1f2b2ec7df5b7e80f11158a7c5bd19e1f))
* **urielavalos:** added 8/24 comic ([35e53ef](https://gitlab.com/robotandkid/consulting/commit/35e53efbdc6a54e20afa3eff222ee2b81fbeedc9))
* **urielavalos:** added more t-shirt designs ([a39f4a4](https://gitlab.com/robotandkid/consulting/commit/a39f4a40474e7c77456b6a4b90a4d07c518d97af))
* **urielavalos:** latest ([9a316ff](https://gitlab.com/robotandkid/consulting/commit/9a316ff7ce3c74664d654fa290751f85434329c4))





# [0.91.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.90.2...@robotandkid/urielavalos@0.91.0) (2024-08-17)


### Features

* **urielavalos:** added a robot's journey ([5cb82b1](https://gitlab.com/robotandkid/consulting/commit/5cb82b1c2bad218e84aea5691e56238c124eb101))
* **urielavalos:** added how not to train your robot ([08fe3ea](https://gitlab.com/robotandkid/consulting/commit/08fe3eab26b9c2057b0a481304a2d23f0f0a946e))
* **urielavalos:** added watch out for the shy ones ([96cd06a](https://gitlab.com/robotandkid/consulting/commit/96cd06a3b540480569b3e1a01f20348234e32363))





## [0.90.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.90.1...@robotandkid/urielavalos@0.90.2) (2024-08-08)


### Bug Fixes

* **sprite-motion:** compiles again ([7f9241f](https://gitlab.com/robotandkid/consulting/commit/7f9241f4ac4abbe50ca8dfab8c4e161c7e6a1b0f))





## [0.90.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.90.0...@robotandkid/urielavalos@0.90.1) (2024-08-08)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.90.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.89.0...@robotandkid/urielavalos@0.90.0) (2024-08-08)


### Features

* **urielavalos:** added 8/5 comic ([8e2a40d](https://gitlab.com/robotandkid/consulting/commit/8e2a40d14258cd7ef0b085e03668fbeebec55372))
* **urielavalos:** added a robot reading a book ([42ef93a](https://gitlab.com/robotandkid/consulting/commit/42ef93a911ff7df8d1ffb9ccd394d8aa7134b477))
* **urielavalos:** added I can't hear you ([d42ce26](https://gitlab.com/robotandkid/consulting/commit/d42ce26e4c0c5085d1a4a109fd54df5f79296af0))
* **urielavalos:** added summer duran ([2d5e201](https://gitlab.com/robotandkid/consulting/commit/2d5e201473f174458e7c62580c2bcd0ea1f46d10))
* **urielavalos:** connected all newly added comics/art ([dfadb1c](https://gitlab.com/robotandkid/consulting/commit/dfadb1cf50f494e4e9c981a65337b6ce84317ae1))





# [0.89.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.88.0...@robotandkid/urielavalos@0.89.0) (2024-07-25)


### Features

* **urielavalos:** added Relaxing Coffee in the Park ([3226319](https://gitlab.com/robotandkid/consulting/commit/3226319a7e3f6b7c7c292e2928c0c1be91692588))
* **urielavalos:** updated tshirt design [#2](https://gitlab.com/robotandkid/consulting/issues/2) ([b401ea7](https://gitlab.com/robotandkid/consulting/commit/b401ea7ce654025bb80f5344865a683e73cd5bbd))





# [0.88.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.87.0...@robotandkid/urielavalos@0.88.0) (2024-07-17)


### Features

* **urielavalos:** updated tshirt design ([7770928](https://gitlab.com/robotandkid/consulting/commit/77709281a81e86153747820939089a1f0fbb03b0))





# [0.87.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.86.0...@robotandkid/urielavalos@0.87.0) (2024-07-16)


### Features

* **urielavalos:** added captain jack ([49ced28](https://gitlab.com/robotandkid/consulting/commit/49ced28c6ce5203c8f48c009be121e29722f76b3))
* **urielavalos:** added chrono 6/24 ([086473d](https://gitlab.com/robotandkid/consulting/commit/086473df5a3393b6ca2a1a967c3dbdff403d0768))
* **urielavalos:** added comic 7/3 ([a98a341](https://gitlab.com/robotandkid/consulting/commit/a98a341ffbc46188b80449fc84b7e50e82a4bba1))
* **urielavalos:** added self-portrait ([53683a8](https://gitlab.com/robotandkid/consulting/commit/53683a8e5eceb1f3d0f1e9cefb5bf1676d2e8284))
* **urielavalos:** hooked up latest ([2af72ff](https://gitlab.com/robotandkid/consulting/commit/2af72ff867cccd609cf2dea89541a212e0495a41))





# [0.86.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.85.0...@robotandkid/urielavalos@0.86.0) (2024-06-30)


### Features

* **urielavalos:** added comic 6/28 ([cdb81ed](https://gitlab.com/robotandkid/consulting/commit/cdb81ed7c9df8915d102c95eef1e1e1c1a2ae587))





# [0.85.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.84.0...@robotandkid/urielavalos@0.85.0) (2024-06-28)


### Features

* **urielavalos:** mainly internal projects ([aebe71d](https://gitlab.com/robotandkid/consulting/commit/aebe71d76c566ca08dc2770f5a54c21d7627b3ac))





# [0.84.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.83.0...@robotandkid/urielavalos@0.84.0) (2024-06-22)


### Features

* **urielavalos:** added Summer Traffic ([a5f7664](https://gitlab.com/robotandkid/consulting/commit/a5f76645633bee275866772061d4b51c11dcd7d4))





# [0.83.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.82.0...@robotandkid/urielavalos@0.83.0) (2024-06-20)


### Features

* **urielavalos:** added 6/19 art ([abb806e](https://gitlab.com/robotandkid/consulting/commit/abb806eaa744e52d8dd40c125fac1d592274521a))





# [0.82.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.81.0...@robotandkid/urielavalos@0.82.0) (2024-06-19)


### Features

* **urielavalos:** added love-in-motion poolside ([607402b](https://gitlab.com/robotandkid/consulting/commit/607402b55878f7ce6779237695ea3ee6f6a55a6e))





# [0.81.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.80.0...@robotandkid/urielavalos@0.81.0) (2024-06-17)


### Features

* **urielavalos:** added a self-portrait ([907829c](https://gitlab.com/robotandkid/consulting/commit/907829c1e585df77f0733add86e370609696f461))





# [0.80.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.79.0...@robotandkid/urielavalos@0.80.0) (2024-06-15)


### Features

* **urielavalos:** added waterfall ([904ae61](https://gitlab.com/robotandkid/consulting/commit/904ae6183b57b88b04a8d7f2275f6ea578b21219))





# [0.79.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.78.0...@robotandkid/urielavalos@0.79.0) (2024-06-14)


### Features

* **urielavalos:** added chrono canvas 6/12 ([3cc02fc](https://gitlab.com/robotandkid/consulting/commit/3cc02fcbf03a17f0dfe322f94d02f61693bbc08d))
* **urielavalos:** added love in motion 6/13 ([07c3d24](https://gitlab.com/robotandkid/consulting/commit/07c3d2489eb50fe12e228ba7dbeb0af00481b88a))
* **urielavalos:** added t-shirt design [#1](https://gitlab.com/robotandkid/consulting/issues/1) ([2c755a0](https://gitlab.com/robotandkid/consulting/commit/2c755a00aca77f84b340c0ac9af3f9e05bc131cc))
* **urielavalos:** connected latest posts ([b3e8e0a](https://gitlab.com/robotandkid/consulting/commit/b3e8e0ade9a7cd51d051ed3906af56fa3f3cc9b4))





# [0.78.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.77.0...@robotandkid/urielavalos@0.78.0) (2024-06-12)


### Features

* **urielavalos:** added Brooklyn Bridge ([c6958b6](https://gitlab.com/robotandkid/consulting/commit/c6958b65f88c47ffa88313f8c72b72aef095f0b0))
* **urielavalos:** added Clouds ([de6a7b9](https://gitlab.com/robotandkid/consulting/commit/de6a7b95260ed1b0a52b73c943438ca29b3df9bc))





# [0.77.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.76.0...@robotandkid/urielavalos@0.77.0) (2024-06-10)


### Features

* **urielavalos:** added Don't Touch My LEgo Sword Display ([9359f9a](https://gitlab.com/robotandkid/consulting/commit/9359f9ad9eeee7783b50cdff4d570ff0b88fe702))





# [0.76.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.75.0...@robotandkid/urielavalos@0.76.0) (2024-06-09)


### Features

* **urielavalos:** added a difficult relationship ([c5d4bad](https://gitlab.com/robotandkid/consulting/commit/c5d4badaa6f90d86162ec1a95fddecb0ff754f14))





# [0.75.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.74.1...@robotandkid/urielavalos@0.75.0) (2024-06-06)


### Features

* **urielavalos:** added anniversary flowers to love in motion ([41a271b](https://gitlab.com/robotandkid/consulting/commit/41a271bdbdc28688fc94e438adf496a68e481c66))
* **urielavalos:** added ephemeral self-portrait ([c1af4cd](https://gitlab.com/robotandkid/consulting/commit/c1af4cda3aa60f1d76f8bf4ed07b4ae08db7a00e))





## [0.74.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.74.0...@robotandkid/urielavalos@0.74.1) (2024-06-06)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.74.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.73.0...@robotandkid/urielavalos@0.74.0) (2024-06-05)


### Features

* **urielavalos:** added Blooming Rose ([0c4e341](https://gitlab.com/robotandkid/consulting/commit/0c4e341e44985fa2c2e6c7e2a0c29bb2ec5b3088))
* **urielavalos:** added Summer Love ([dd18c0f](https://gitlab.com/robotandkid/consulting/commit/dd18c0f5d8653018c795110f0c78becad9caf0ac))





# [0.73.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.72.0...@robotandkid/urielavalos@0.73.0) (2024-05-20)


### Features

* **urielavalos:** added Tai Chi and Pride and Joy ([01367d9](https://gitlab.com/robotandkid/consulting/commit/01367d9ba4cd0760dab7adcb7234a6175c402427))





# [0.72.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.71.0...@robotandkid/urielavalos@0.72.0) (2024-05-18)


### Bug Fixes

* **urielavalos:** compiles again ([82eff53](https://gitlab.com/robotandkid/consulting/commit/82eff53fcb57059e3b3fb20c08b724e459d1ec88))


### Features

* **urieavalos:** added Yoga for chronos ([569c5dc](https://gitlab.com/robotandkid/consulting/commit/569c5dca960235be82a078a76170ce055e589808))
* **urielavalos:** added Siblings ([f26c110](https://gitlab.com/robotandkid/consulting/commit/f26c110ffa4a17f3ba224d1f0f0915a6f6b1aa53))





# [0.71.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.70.0...@robotandkid/urielavalos@0.71.0) (2024-05-17)


### Features

* **urielavalos:** added cmw ([e27fb2a](https://gitlab.com/robotandkid/consulting/commit/e27fb2acca53d84e5ea0ecc030f370dd34c4f0d1))





# [0.70.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.67.1...@robotandkid/urielavalos@0.70.0) (2024-05-17)


### Features

* **urielavalos:** added a bunch of stuff ([d160ee5](https://gitlab.com/robotandkid/consulting/commit/d160ee54624fb17b1c2c47259c6d65cfd413a11f))
* **urielavalos:** added latest chrno canvas ([695bdb5](https://gitlab.com/robotandkid/consulting/commit/695bdb54f193333ef76f9a42224388ff0c9eaf2a))
* **urielavalos:** added love in motion ([b27e0f3](https://gitlab.com/robotandkid/consulting/commit/b27e0f3b8879551b0d9b3c62b23cfef7097e067c))
* **urielavalos:** added next chrono canvas ([c1034e3](https://gitlab.com/robotandkid/consulting/commit/c1034e344fdc7da02a252d34cd3dba40530a9198))
* **urielavalos:** well shit why didn't it stash properly ([0fd807b](https://gitlab.com/robotandkid/consulting/commit/0fd807b5eeba969a7e4fd32a8b7106bed8ff8c3f))





# [0.69.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.67.1...@robotandkid/urielavalos@0.69.0) (2024-05-17)


### Features

* **urielavalos:** added a bunch of stuff ([d160ee5](https://gitlab.com/robotandkid/consulting/commit/d160ee54624fb17b1c2c47259c6d65cfd413a11f))
* **urielavalos:** added latest chrno canvas ([695bdb5](https://gitlab.com/robotandkid/consulting/commit/695bdb54f193333ef76f9a42224388ff0c9eaf2a))
* **urielavalos:** added love in motion ([b27e0f3](https://gitlab.com/robotandkid/consulting/commit/b27e0f3b8879551b0d9b3c62b23cfef7097e067c))
* **urielavalos:** added next chrono canvas ([c1034e3](https://gitlab.com/robotandkid/consulting/commit/c1034e344fdc7da02a252d34cd3dba40530a9198))
* **urielavalos:** well shit why didn't it stash properly ([0fd807b](https://gitlab.com/robotandkid/consulting/commit/0fd807b5eeba969a7e4fd32a8b7106bed8ff8c3f))





# [0.68.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.67.1...@robotandkid/urielavalos@0.68.0) (2024-05-15)


### Features

* **urielavalos:** added latest chrno canvas ([3bcfd45](https://gitlab.com/robotandkid/consulting/commit/3bcfd4533045f87404fdbbea5ec7afbae457f399))
* **urielavalos:** added love in motion ([bf7c41a](https://gitlab.com/robotandkid/consulting/commit/bf7c41a846f81a88c89997ce4357f09317b813f6))
* **urielavalos:** added next chrono canvas ([73da2f5](https://gitlab.com/robotandkid/consulting/commit/73da2f5dc6374604c7e6b5adf337ca5ea5aadb6d))
* **urielavalos:** well shit why didn't it stash properly ([e6f48de](https://gitlab.com/robotandkid/consulting/commit/e6f48dea52298d04289be351a3205dd3bdd17c6b))





## [0.67.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.67.0...@robotandkid/urielavalos@0.67.1) (2024-05-10)


### Bug Fixes

* **urielavalos:** compiles again ([60f09cd](https://gitlab.com/robotandkid/consulting/commit/60f09cd2f4fd7b9a4773debff40ec27ba9fbf9f2))





# [0.67.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.66.0...@robotandkid/urielavalos@0.67.0) (2024-05-10)


### Features

* compiles again ([5120289](https://gitlab.com/robotandkid/consulting/commit/512028916232c3a13ff60d0ab8929522812fdbc7))
* **panda:** upgraded to newer version ([a12c630](https://gitlab.com/robotandkid/consulting/commit/a12c6308b8028227e7ecac8e82d282430543339d))
* **uriel-avalos:** updated mespeak ([c40f8de](https://gitlab.com/robotandkid/consulting/commit/c40f8defa90455b8299edf96b5b8132f4da2df73))
* **urielavalos:** added new chrono canvas ([437feed](https://gitlab.com/robotandkid/consulting/commit/437feed0110d92b49af39c1387c4e4512dc83861))
* **urielavalos:** added next chrono canvas ([8350f1f](https://gitlab.com/robotandkid/consulting/commit/8350f1fa6e8106d4e60a56a133890a82f7cd5f32))
* **urielavalos:** added pinocchio ([8191c99](https://gitlab.com/robotandkid/consulting/commit/8191c99e113352d9f499a25ff4dd2f46a94578b3))
* **urielavalos:** added wwa ([78b5337](https://gitlab.com/robotandkid/consulting/commit/78b5337cf6d2689a908d8b451c37b15d0dd7260f))
* **urielavalos:** latest chrono canvas ([6ac9fa8](https://gitlab.com/robotandkid/consulting/commit/6ac9fa8d0c80a0cce3190cc2e4a66a77261f8ae6))
* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





# [0.66.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.65.0...@robotandkid/urielavalos@0.66.0) (2024-05-02)


### Features

* **urielavalos:** added next chrono canvas ([7cf7613](https://gitlab.com/robotandkid/consulting/commit/7cf7613d26a6623839a1bf29298cd46c01f1a0ac))
* **urielavalos:** added next installment of chrono canvas ([d48d8ae](https://gitlab.com/robotandkid/consulting/commit/d48d8aeb849b7ec0a3f46fae9d3cbfe04310c929))





# [0.65.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.64.0...@robotandkid/urielavalos@0.65.0) (2024-04-26)


### Features

* **urielavalos:** added It's Just Veggies ([edd2304](https://gitlab.com/robotandkid/consulting/commit/edd2304fedeeecede0a758cfaf42fe3d7ff1508d))





# [0.64.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.63.0...@robotandkid/urielavalos@0.64.0) (2024-04-25)


### Features

* **urielavalos:** added chrono 4/22 ([0c45c4a](https://gitlab.com/robotandkid/consulting/commit/0c45c4ad78434a95de468c19aca4fa8fab7269e6))
* **urielavalos:** added chrono 4/23 ([aba5858](https://gitlab.com/robotandkid/consulting/commit/aba5858911265b80086277634fa5706a9ccb01df))





# [0.63.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.62.1...@robotandkid/urielavalos@0.63.0) (2024-04-22)


### Bug Fixes

* **urielavalos:** whoops forgot to commit files ([7360e9b](https://gitlab.com/robotandkid/consulting/commit/7360e9b7c3419a43bb2eb3843a2d6d20a6ec1457))


### Features

* **urielavalos:** added chrono 4/17 ([940d038](https://gitlab.com/robotandkid/consulting/commit/940d0389a8a5ca03d31a08a89b91673c449ba163))
* **urielavalos:** added chrono 4/17 ([bfc5fcc](https://gitlab.com/robotandkid/consulting/commit/bfc5fcc3ff272b75b2dfcb136f31600400f2bf49))
* **urielavalos:** added chrono 4/18 ([2b318db](https://gitlab.com/robotandkid/consulting/commit/2b318dbd32f08d6f8f5993a0173be47c7cb98f47))
* **urielavalos:** added chrono 4/18 ([6289715](https://gitlab.com/robotandkid/consulting/commit/628971596614da2eb4df2d8d30733a7b605efbaa))
* **urielavalos:** added chrono 4/20 ([4db7fd2](https://gitlab.com/robotandkid/consulting/commit/4db7fd2e1da32cb3e5776ace5d891e6fd0f9eb62))
* **urielavalos:** added chronocanvas 4/11 ([b6e15ce](https://gitlab.com/robotandkid/consulting/commit/b6e15ceabddb029dee4bfa47e8a5c23df33e5407))
* **urielavalos:** added draft of the portal part 2 ([8a00334](https://gitlab.com/robotandkid/consulting/commit/8a00334aafa3a72c8c9b7220da6ea8697538e5f2))
* **urielavalos:** added draft of the portal part 2 ([d6ad6ac](https://gitlab.com/robotandkid/consulting/commit/d6ad6ac8b245d5a2a2973c13f2626070b9c88dfb))
* **urielavalos:** updated Portal and more ([5632f32](https://gitlab.com/robotandkid/consulting/commit/5632f32d3d284b820022c95fb11d55003ac2ade5))





## [0.62.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.62.0...@robotandkid/urielavalos@0.62.1) (2024-04-08)


### Bug Fixes

* **sprite-moton:** marked dims in Canvas as optional ([8ffaa25](https://gitlab.com/robotandkid/consulting/commit/8ffaa250ce2def0a01558ee64313722d265db11e))
* **urielavalos:** fixed self-portrait ([5f24c57](https://gitlab.com/robotandkid/consulting/commit/5f24c5791153d520b225406acb57f6e4c16a69d4))
* **urielavalos:** fixed self-portrait ([f5d0cb7](https://gitlab.com/robotandkid/consulting/commit/f5d0cb7c9503ec71314701bec3689ef4fe39479d))





# [0.62.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.61.1...@robotandkid/urielavalos@0.62.0) (2024-04-06)


### Features

* **sprite-motion:** ascii supports custom bg colors ([c2513ec](https://gitlab.com/robotandkid/consulting/commit/c2513ecbc99ac587909a9c775a77f8a96de17187))
* **urielavalos:** added latest comic + chronos-canvas ([48c6a10](https://gitlab.com/robotandkid/consulting/commit/48c6a101f764a3f83a575d443d511e9567b45568))
* **urielavalos:** added more chrono canvas ([a59ac52](https://gitlab.com/robotandkid/consulting/commit/a59ac52bdace2b2b4149fe958414d1e0abb41f65))
* **urielavalos:** added next chrono canvas ([da5af19](https://gitlab.com/robotandkid/consulting/commit/da5af1975c2b5fd142e8f0461a5c0027be8e4169))





## [0.61.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.61.0...@robotandkid/urielavalos@0.61.1) (2024-03-31)


### Bug Fixes

* **urielavalos:** compiles again ([8c0b5b0](https://gitlab.com/robotandkid/consulting/commit/8c0b5b08b0c17f4bd2055cda4421d24070afbd9e))





# [0.61.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.60.0...@robotandkid/urielavalos@0.61.0) (2024-03-31)


### Features

* **urielavalos:** added latest comic ([6450ea4](https://gitlab.com/robotandkid/consulting/commit/6450ea47d8c52a5b5f897e9d140447e44cd0c231))
* **urielavalos:** added self-portrait ([9fc64f3](https://gitlab.com/robotandkid/consulting/commit/9fc64f3d8433a65d376d9dd9b5334a2da0aa869b))
* **urielavalos:** added The Portal ([fe045ed](https://gitlab.com/robotandkid/consulting/commit/fe045edde12369fa18368f399f4235c7523ab11a))





# [0.60.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.59.0...@robotandkid/urielavalos@0.60.0) (2024-03-05)


### Features

* **urielavalos:** added latest comic Welcome to the technocracy ([41c777b](https://gitlab.com/robotandkid/consulting/commit/41c777b95f48d3269b60d23347ae641154c32730))





# [0.59.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.58.0...@robotandkid/urielavalos@0.59.0) (2024-03-04)


### Features

* **sprite-motion:** added Noise ([59c8a89](https://gitlab.com/robotandkid/consulting/commit/59c8a8962c112049e5cfe280a419e0bcb63776a4))
* **urielavalos:** added latest comic ([ff38c55](https://gitlab.com/robotandkid/consulting/commit/ff38c557c8f5dc1a488e3b73a5c2aaeb99bf1ce8))





# [0.58.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.57.0...@robotandkid/urielavalos@0.58.0) (2024-02-24)


### Bug Fixes

* **sprite-motion:** fixed SpriteSheet onLoopEnd bug ([b9e9213](https://gitlab.com/robotandkid/consulting/commit/b9e9213cf0a7879e92a907a1f729b05de369f189))
* **urielavalos:** compile error ([2224bd3](https://gitlab.com/robotandkid/consulting/commit/2224bd3a6ada3f63efbf7a2e02b466ef0cfc362e))


### Features

* **urielavalos:** added Do we live in a simulation? ([a886c5b](https://gitlab.com/robotandkid/consulting/commit/a886c5b15a170e7eaa338e6b5ae35e9b1fe1eaf8))
* **urielavalos:** added engineering simulator ([b589c10](https://gitlab.com/robotandkid/consulting/commit/b589c108ea580d2b915f8904d2058454caed0665))
* **urielavalos:** added I have visual snow ([64a4997](https://gitlab.com/robotandkid/consulting/commit/64a4997c928052f6381ae5509d77d8c11e99f675))
* **urielavalos:** added quine ([536d156](https://gitlab.com/robotandkid/consulting/commit/536d1561b15dac2288401ffd673fcba319008aee))
* **urielavalos:** added Sometimes you just have to rest ([1eebdb5](https://gitlab.com/robotandkid/consulting/commit/1eebdb5a2bedfa1a6d30adf887a0401a60a7a094))





# [0.57.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.56.0...@robotandkid/urielavalos@0.57.0) (2024-02-01)


### Features

* **urielavalos:** added Achievement Unlocked ([64cbc47](https://gitlab.com/robotandkid/consulting/commit/64cbc47bdf2ae55328116d0d911b21b01bf8507e))
* **urielavalos:** added Bot Followers ([9a9f90b](https://gitlab.com/robotandkid/consulting/commit/9a9f90b6f8fdf807bfb63593a18a93a99b082b94))





# [0.56.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.55.0...@robotandkid/urielavalos@0.56.0) (2024-01-27)


### Features

* **urielavalos:** added Bigmac with a side of murder ([a0dbcff](https://gitlab.com/robotandkid/consulting/commit/a0dbcff97fb0b6687b74503a9a86c60fc5e17b11))
* **urielavalos:** added panda support ([20f6040](https://gitlab.com/robotandkid/consulting/commit/20f6040b3ab2ccae0b398b0766db5a3459d3fd74))





# [0.55.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.54.0...@robotandkid/urielavalos@0.55.0) (2024-01-14)


### Features

* **urieavalos:** made "burger with extra murder" deployable ([4a04bf7](https://gitlab.com/robotandkid/consulting/commit/4a04bf70bcda79aa9076e05cac1f211061960dc3))
* **urielavalos:** added "A Big Mac with extra murder" ([8b37cc3](https://gitlab.com/robotandkid/consulting/commit/8b37cc3bca5cf5e546fa88d74e55e65745092500))





# [0.54.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.53.2...@robotandkid/urielavalos@0.54.0) (2023-12-21)


### Bug Fixes

* **urielavalos:** dropped padding in gate container ([938262b](https://gitlab.com/robotandkid/consulting/commit/938262b1a54f29ca02b50fdaf4bccdbe2c5af661))
* **urielavalos:** fixed post ([ce09a9f](https://gitlab.com/robotandkid/consulting/commit/ce09a9fba7b26780582dd71ffd75c82c203ac52a))


### Features

* **urielavalos:** 1st pass Street Fighter ([850aab3](https://gitlab.com/robotandkid/consulting/commit/850aab309fce08067073167df50f5bbee59ae2e3))
* **urielavalos:** added 12/17 post ([21f4c7b](https://gitlab.com/robotandkid/consulting/commit/21f4c7be150d60d5a0c8a190b60f48caa0ce9cd7))
* **urielavalos:** gateBehindButton now supports custom button ([33a8698](https://gitlab.com/robotandkid/consulting/commit/33a8698acac4372c369bae12c82c96755e2fecbb))





## [0.53.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.53.1...@robotandkid/urielavalos@0.53.2) (2023-12-06)


### Bug Fixes

* **urielavalos:** comic had wrong title ([251b05a](https://gitlab.com/robotandkid/consulting/commit/251b05a4c2fe61e2170c4c4e1b6d748e1a481340))





## [0.53.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.53.0...@robotandkid/urielavalos@0.53.1) (2023-12-06)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.53.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.52.0...@robotandkid/urielavalos@0.53.0) (2023-12-06)


### Bug Fixes

* compiles again ([5e10200](https://gitlab.com/robotandkid/consulting/commit/5e10200ad98982598452bd2f593b21de63e618b3))


### Features

* **urielavalos:** added Movie Night ([85147da](https://gitlab.com/robotandkid/consulting/commit/85147da75b83e07717dab49b351ac9b69fd16cd7))
* **urielavalos:** made progress on StreetFighter ([db8c143](https://gitlab.com/robotandkid/consulting/commit/db8c1439f4ed3c6f3f868d8630e3d36bd0ecb241))
* **urielavalos:** published Alli Venden Drogas ([a963b95](https://gitlab.com/robotandkid/consulting/commit/a963b95fc42c3e94b2757cd2c0ff577953e96d29))
* **urielavalos:** typewriter component accepts props ([3bb79be](https://gitlab.com/robotandkid/consulting/commit/3bb79beb45a3b8a9f9bd70c71ce6800387e7a274))





# [0.52.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.51.0...@robotandkid/urielavalos@0.52.0) (2023-11-21)


### Features

* **urielavalos:** draft of Street Fighter ([e8cd9ce](https://gitlab.com/robotandkid/consulting/commit/e8cd9ce59185fdf47d85cca563fb78b4825876d1))





# [0.51.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.50.0...@robotandkid/urielavalos@0.51.0) (2023-11-07)


### Features

* **urielavalos:** added full screen mode ([8d27d38](https://gitlab.com/robotandkid/consulting/commit/8d27d38083bccd22f8bc70257dc15871013b77df))
* **urielavalos:** added latest comic ([b2d33cf](https://gitlab.com/robotandkid/consulting/commit/b2d33cf21555306bb398fb03f2fdf94402815e3a))
* **urielavalos:** added useCaleToFit ([ad03a64](https://gitlab.com/robotandkid/consulting/commit/ad03a64fa9d4e69a4c93d110b1b4a6f45dc57bb6))





# [0.50.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.49.2...@robotandkid/urielavalos@0.50.0) (2023-10-14)


### Bug Fixes

* **urielavalos:** disabled manual navigation in some comics ([9d85e34](https://gitlab.com/robotandkid/consulting/commit/9d85e34102dd67f0f5bd8f624e821d249cc2eb18))


### Features

* **urielavalos:** added 10/4 comic ([934ac1d](https://gitlab.com/robotandkid/consulting/commit/934ac1d96bb3f1b12e89cdf4f2491289a55b1d7f))
* **urielavalos:** added when yoga goes bad ([86c9465](https://gitlab.com/robotandkid/consulting/commit/86c94652a05ca2be08479b5956ff6ecb8ce49c3b))





## [0.49.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.49.1...@robotandkid/urielavalos@0.49.2) (2023-09-30)


### Bug Fixes

* **urielavalos:** tOC works in blog/comics posts ([9b4c909](https://gitlab.com/robotandkid/consulting/commit/9b4c9095615132a38513e354861e0f62b68c9bef))





## [0.49.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.49.0...@robotandkid/urielavalos@0.49.1) (2023-09-24)


### Bug Fixes

* **urielavalos:** useScaleToFit works on mobile ([cd88f4f](https://gitlab.com/robotandkid/consulting/commit/cd88f4f33fbceca39172f5001c9229e71ddac12c))
* **urielavalos:** visual bug in press start button ([d519e62](https://gitlab.com/robotandkid/consulting/commit/d519e62f790d56b90022d6809cb2166a13f192ac))





# [0.49.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.48.1...@robotandkid/urielavalos@0.49.0) (2023-09-24)


### Bug Fixes

* **sprite-motion:** action block works more gooder ([9c74741](https://gitlab.com/robotandkid/consulting/commit/9c74741b86c5f6a27ad08f6454f29552af2160db))


### Features

* **urielavalos:** added next comic ([7577b36](https://gitlab.com/robotandkid/consulting/commit/7577b36f758257dd266ba2cbdddbdea3a5f69198))
* **urielavalos:** ok i think i finally finished the blog post ([231e2eb](https://gitlab.com/robotandkid/consulting/commit/231e2eb396204de61cee1078514fd9d33dced059))





## [0.48.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.48.0...@robotandkid/urielavalos@0.48.1) (2023-09-20)


### Bug Fixes

* **urielavalo:** cleanup useScaleToFit + some comics were cropped incorrectly ([e30ccc3](https://gitlab.com/robotandkid/consulting/commit/e30ccc363150f697514b93541dc2eaacae7f5e4e))





# [0.48.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.47.0...@robotandkid/urielavalos@0.48.0) (2023-09-20)


### Features

* **urielavalos:** really awesome comic ([00e4ffb](https://gitlab.com/robotandkid/consulting/commit/00e4ffbdd9e03ec6ec21294572ff0880cb880215))





# [0.47.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.46.5...@robotandkid/urielavalos@0.47.0) (2023-09-19)


### Bug Fixes

* **urielavalos:** compile errors ([7ca2c87](https://gitlab.com/robotandkid/consulting/commit/7ca2c87d0e0a80274a819d800c8a16c69ce4078d))


### Features

* **urielavalos:** added sound effects to a comic ([4881484](https://gitlab.com/robotandkid/consulting/commit/488148479cd650161c94c29d9c6c48cc360900a6))
* **urielavalos:** added sound effects to a comic ([cddddd2](https://gitlab.com/robotandkid/consulting/commit/cddddd25d12e315db8dcecf5943d0b9f84197299))





## [0.46.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.46.4...@robotandkid/urielavalos@0.46.5) (2023-09-19)


### Bug Fixes

* **urielavalos:** hiding video game in all pages except the main page ([5331de4](https://gitlab.com/robotandkid/consulting/commit/5331de43cf4e236ddee091a71e92b7a3c33eed6b))





## [0.46.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.46.3...@robotandkid/urielavalos@0.46.4) (2023-09-19)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.46.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.46.2...@robotandkid/urielavalos@0.46.3) (2023-09-18)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.46.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.46.1...@robotandkid/urielavalos@0.46.2) (2023-09-18)


### Bug Fixes

* **urielavalos:** added latest comic ([3929347](https://gitlab.com/robotandkid/consulting/commit/3929347b93bf280617b1d3775d83734d87845efe))





## [0.46.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.46.0...@robotandkid/urielavalos@0.46.1) (2023-09-17)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.46.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.45.0...@robotandkid/urielavalos@0.46.0) (2023-09-16)


### Features

* **urielavalos:** styled Button ([74562b3](https://gitlab.com/robotandkid/consulting/commit/74562b37f3a2fe462faf0ac94fffa4fec0f5e86c))





# [0.45.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.44.0...@robotandkid/urielavalos@0.45.0) (2023-09-10)


### Features

* **sprite-motion:** did a lot ([f10006e](https://gitlab.com/robotandkid/consulting/commit/f10006e7bb9479ae2cc80db38e27bfdeaf5cce5e))





# [0.44.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.43.1...@robotandkid/urielavalos@0.44.0) (2023-09-10)


### Features

* **sprite-motion:** added simpler sound API ([1c4e57d](https://gitlab.com/robotandkid/consulting/commit/1c4e57d20c8f9e6a30df6088fd87cb822287712a))
* **urielavalos:** added comic mobile support ([868e062](https://gitlab.com/robotandkid/consulting/commit/868e062f07743ff8d1fac2b04b95c2c126a02cc5))





## [0.43.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.43.0...@robotandkid/urielavalos@0.43.1) (2023-09-07)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.43.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.42.0...@robotandkid/urielavalos@0.43.0) (2023-09-04)


### Bug Fixes

* enabled stricer array access in TS ([8b89be7](https://gitlab.com/robotandkid/consulting/commit/8b89be793edef75156025f9b3ee4a7944a308dc0))


### Features

* **urielavalos:** added latest comic ([33f7533](https://gitlab.com/robotandkid/consulting/commit/33f7533a70c757732dabbe7b491eb178947c40b2))
* **urielavalos:** added latest comic (1st pass) ([486ef69](https://gitlab.com/robotandkid/consulting/commit/486ef69c63c061a1cd03180625e771c469692b49))





# [0.42.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.41.0...@robotandkid/urielavalos@0.42.0) (2023-08-20)


### Features

* **urielavalos:** various bug fixes + improvements ([1d2bc58](https://gitlab.com/robotandkid/consulting/commit/1d2bc583c4a5d75819b675c39949635bddfafdb3))





# [0.41.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.40.0...@robotandkid/urielavalos@0.41.0) (2023-08-19)


### Features

* **urielavalos:** added comic 2023-08-16 ([11ca69f](https://gitlab.com/robotandkid/consulting/commit/11ca69f6f456f29effa38b70f2f50b1ca0a26fa5))





# [0.40.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.39.0...@robotandkid/urielavalos@0.40.0) (2023-08-07)


### Bug Fixes

* goodbye dead code ([6762adf](https://gitlab.com/robotandkid/consulting/commit/6762adfe99415c1e21399757e8aa3324440da3ef))
* **urielavalos:** removed broken posts ([784da89](https://gitlab.com/robotandkid/consulting/commit/784da898ca7a8c53a50a6f3085054fad1cce2917))


### Features

* upgraded node, pixi.js ([c06a0b4](https://gitlab.com/robotandkid/consulting/commit/c06a0b470991876a2763336d76a4272ca536af75))





# [0.39.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.38.2...@robotandkid/urielavalos@0.39.0) (2023-08-07)


### Bug Fixes

* **components:** image blur works correctly ([0b3bd56](https://gitlab.com/robotandkid/consulting/commit/0b3bd5687ff7cdbb6123d91b20b75e8f56fb012a))
* **urielavalos:** hamburger spins correctly ([0af905b](https://gitlab.com/robotandkid/consulting/commit/0af905b78bb3d33379d960d9d4d01085b862b42b))


### Features

* **digital-apps:** eli garcia published ([331476e](https://gitlab.com/robotandkid/consulting/commit/331476ecc8574bc6371c17268e674f68a6919c1e))
* migrated to use BodyUnderHamburger copmonent ([2c9c74a](https://gitlab.com/robotandkid/consulting/commit/2c9c74a91577db6c73d0f457b8058b10d1b3daaa))
* **sprite-motion:** added blog post and finished 1st pass sprite-motion ([6011664](https://gitlab.com/robotandkid/consulting/commit/601166446fc6be5e1b56e9efc2c7cecaff25e1f4))





## [0.38.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.38.1...@robotandkid/urielavalos@0.38.2) (2022-11-30)


### Bug Fixes

* **leandertile:** fixed call button flickering bug on desktop ([b946b6a](https://gitlab.com/robotandkid/consulting/commit/b946b6a1dfc3a3c59b6083e24b283eb9b28bb0dd))





## [0.38.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.38.0...@robotandkid/urielavalos@0.38.1) (2022-11-28)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.38.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.37.2...@robotandkid/urielavalos@0.38.0) (2022-11-27)


### Bug Fixes

* **urielavalos:** cI script has typo ([8fd09a5](https://gitlab.com/robotandkid/consulting/commit/8fd09a5f30eac3fedc4f608ae2ef888d48e0aecc))
* **urielavalos:** fixed landing bg animation ([ed4daa7](https://gitlab.com/robotandkid/consulting/commit/ed4daa724deef6ebb36f83390e8d9c4d814dafe4))
* **urielavalos:** landing bg had layoutId typo ([e253020](https://gitlab.com/robotandkid/consulting/commit/e253020936bcc5a523a3eccf8d9146d51b5d9cb6))
* **urielavalos:** using transient props ([c15c6d3](https://gitlab.com/robotandkid/consulting/commit/c15c6d39c053d8f2ce14983ca561231abc80dd56))


### Features

* added bundle analyzer ([89cb42b](https://gitlab.com/robotandkid/consulting/commit/89cb42bc0fd79ad72f9204e47f8ae943e108a682))
* **cypress:** updated task to add latest cypress ([e2a00a7](https://gitlab.com/robotandkid/consulting/commit/e2a00a74f2402b014c9a606ea0b7294ec3f160b1))
* **urielavalos:** reduced bundle size ([2964056](https://gitlab.com/robotandkid/consulting/commit/2964056f73168b847f811150bc536b30e86c3e46))





## [0.37.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.37.1...@robotandkid/urielavalos@0.37.2) (2022-11-23)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.37.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.37.0...@robotandkid/urielavalos@0.37.1) (2022-11-23)


### Bug Fixes

* **urielavalos:** correctly hiding game in mobile ([ecb7565](https://gitlab.com/robotandkid/consulting/commit/ecb7565896824c2979500d142c0eba9ab700f6a1))





# [0.36.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.35.2...@robotandkid/urielavalos@0.36.0) (2022-02-02)


### Features

* **urielavalos:** glow comic now has sound ([75d3dc4](https://gitlab.com/robotandkid/consulting/commit/75d3dc493a6d00b8f0cd92ed5d48dd4038286f82))





## [0.35.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.35.1...@robotandkid/urielavalos@0.35.2) (2022-02-01)


### Bug Fixes

* **urielavalos:** added touchstart event to Glow ([b21cd6a](https://gitlab.com/robotandkid/consulting/commit/b21cd6ae222e62978d2df49bfb091f049a8e3989))





## [0.35.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.35.0...@robotandkid/urielavalos@0.35.1) (2022-02-01)


### Bug Fixes

* **urielavalos:** glow comic TSX compiles again ([23e1dea](https://gitlab.com/robotandkid/consulting/commit/23e1dea7510f76463953682a9024103020ba0e4f))





# [0.35.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.34.6...@robotandkid/urielavalos@0.35.0) (2022-02-01)


### Features

* **urielavalos:** added Glow comic ([7eb316a](https://gitlab.com/robotandkid/consulting/commit/7eb316a2cce9566178a749dc655eabe1fe339c9a))





## [0.34.6](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.34.5...@robotandkid/urielavalos@0.34.6) (2022-01-24)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.34.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.34.4...@robotandkid/urielavalos@0.34.5) (2022-01-23)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.34.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.34.3...@robotandkid/urielavalos@0.34.4) (2022-01-23)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.34.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.34.2...@robotandkid/urielavalos@0.34.3) (2022-01-13)


### Bug Fixes

* **urielavalos:** made summer's poems a11y ([469376e](https://gitlab.com/robotandkid/consulting/commit/469376ecc2d07f19b208225161fcfc2232f7f359))
* **urielavalos:** made summer's poems a11y ([34df2ba](https://gitlab.com/robotandkid/consulting/commit/34df2bada7bee0d0f85f870147a24961e9c30bac))





## [0.34.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.34.1...@robotandkid/urielavalos@0.34.2) (2022-01-13)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.34.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.34.0...@robotandkid/urielavalos@0.34.1) (2022-01-13)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.34.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.33.0...@robotandkid/urielavalos@0.34.0) (2022-01-13)


### Features

* **urielavalos:** added more summer stuff ([58d5a02](https://gitlab.com/robotandkid/consulting/commit/58d5a027a8aa557d508b42551c238fd6009cfddb))





# [0.33.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.32.1...@robotandkid/urielavalos@0.33.0) (2022-01-09)


### Bug Fixes

* **urielavalos:** footer had a spacing issue ([355167e](https://gitlab.com/robotandkid/consulting/commit/355167ef26897fc36799668a702b1c21b5f63e07))


### Features

* **urielavalos:** added click to open container in summer's poems ([2b93b34](https://gitlab.com/robotandkid/consulting/commit/2b93b3465b7757ee7fc5d6098da9903df486d38d))
* **urielavalos:** added new comics ([37a715b](https://gitlab.com/robotandkid/consulting/commit/37a715b681fe33529ceffa72a876b5b584f65062))
* **urielavalos:** consuming pixi-components via dynamic components ([6fed0ba](https://gitlab.com/robotandkid/consulting/commit/6fed0ba4e0955d4cac756425226d7b348aaada38))





## [0.32.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.32.0...@robotandkid/urielavalos@0.32.1) (2021-12-14)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.32.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.31.0...@robotandkid/urielavalos@0.32.0) (2021-12-06)


### Features

* **urielavalos:** added updated sitemap ([a9c8bee](https://gitlab.com/robotandkid/consulting/commit/a9c8beeeb168cbab35e66937131f9cf7e2ebb7a7))





# [0.31.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.30.3...@robotandkid/urielavalos@0.31.0) (2021-12-03)


### Features

* **pixi:** migrated awesome library ([7fa73f2](https://gitlab.com/robotandkid/consulting/commit/7fa73f21f500d48b1505bfbbab5ba08775e824bd))
* **urielavalos:** added crypto tips ([a821c9f](https://gitlab.com/robotandkid/consulting/commit/a821c9f48668d641a37319cb103fb81c7fdd660e))
* **urielavalos:** added crypto tips ([fa142a9](https://gitlab.com/robotandkid/consulting/commit/fa142a9c9decc09008d1af66b0652f43cb3ec931))





## [0.30.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.30.2...@robotandkid/urielavalos@0.30.3) (2021-09-29)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.30.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.30.1...@robotandkid/urielavalos@0.30.2) (2021-09-29)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.30.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.30.0...@robotandkid/urielavalos@0.30.1) (2021-09-27)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.30.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.29.0...@robotandkid/urielavalos@0.30.0) (2021-09-24)


### Features

* **urielavalos:** added react matter demo4 ([cae1cb5](https://gitlab.com/robotandkid/consulting/commit/cae1cb51b97ae4f858db6521504f72864f533032))





# [0.29.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.28.0...@robotandkid/urielavalos@0.29.0) (2021-09-23)


### Features

* **urielavalos:** added google analytics ([048f252](https://gitlab.com/robotandkid/consulting/commit/048f2529b7239fa2499ada8cd87bd2affd354d73))





# [0.28.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.27.0...@robotandkid/urielavalos@0.28.0) (2021-09-18)


### Features

* **urielavalos:** created MadeWithAwesome footer ([0fc5cc2](https://gitlab.com/robotandkid/consulting/commit/0fc5cc2509d30c10e09aa6bacf67a42ec347fac7))





# [0.27.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.26.0...@robotandkid/urielavalos@0.27.0) (2021-09-18)


### Features

* **urielavalos:** added 9/17 comic ([86621f9](https://gitlab.com/robotandkid/consulting/commit/86621f90c552162bfff36e85e65a48ae8c8a31a1))
* **urielavalos:** added 9/17 comic ([1d0a6f5](https://gitlab.com/robotandkid/consulting/commit/1d0a6f5d8d65b26df5f6339b6e4dbbf0a8d4f1cb))





# [0.26.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.25.0...@robotandkid/urielavalos@0.26.0) (2021-09-18)


### Features

* **urielavalos:** added 07-21 comic ([2dd91d6](https://gitlab.com/robotandkid/consulting/commit/2dd91d67adce54ee5904881200db8dcdf253c67e))





# [0.25.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.24.0...@robotandkid/urielavalos@0.25.0) (2021-09-18)


### Features

* **urielavalos:** added 2 SummerDuran ([c1f06d5](https://gitlab.com/robotandkid/consulting/commit/c1f06d5c2c94c159f3550bf983173fce213775f9))
* **urielavalos:** added 2 SummerDuran ([93bd525](https://gitlab.com/robotandkid/consulting/commit/93bd525d626bacb525c6b9260cc0430877cde6a2))





# [0.24.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.23.2...@robotandkid/urielavalos@0.24.0) (2021-09-14)


### Features

* **urielavalos:** added 3rd press-start blog post ([2cf92cc](https://gitlab.com/robotandkid/consulting/commit/2cf92cc0edab8721636b5ade271fa97928b1da25))





## [0.23.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.23.1...@robotandkid/urielavalos@0.23.2) (2021-09-14)


### Bug Fixes

* **urielavalos:** demo centered properly and matter.js starts properly ([27caf1b](https://gitlab.com/robotandkid/consulting/commit/27caf1b5cf8c3bcba4c16236a8f538152ff4a046))





## [0.23.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.23.0...@robotandkid/urielavalos@0.23.1) (2021-09-02)


### Bug Fixes

* fixed double BODY tag bug ([4416d82](https://gitlab.com/robotandkid/consulting/commit/4416d82153252ae34c9369500253b5b6f15e53ba))





# [0.23.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.22.0...@robotandkid/urielavalos@0.23.0) (2021-09-02)


### Features

* **urielavalos:** added 2nd blog post on matter.js ([9a2a0da](https://gitlab.com/robotandkid/consulting/commit/9a2a0daff662d8be57dbe42094afb42160ac92e0))





# [0.22.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.7...@robotandkid/urielavalos@0.22.0) (2021-08-25)


### Features

* **urielavalos:** 1st passcanvas landinghero ([77a0377](https://gitlab.com/robotandkid/consulting/commit/77a037784f1f1f80e059ed3be3ce47f0a30bde89))
* **urielavalos:** added matter.js ([74cbd61](https://gitlab.com/robotandkid/consulting/commit/74cbd610875a556c027557214b279ab47d65c457))
* **urielavalos:** added react-matter-demo ([f7a79f9](https://gitlab.com/robotandkid/consulting/commit/f7a79f970096d98503b50900279e79665ac0dbf1))





## [0.21.7](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.6...@robotandkid/urielavalos@0.21.7) (2021-08-04)


### Bug Fixes

* **urielavalos:** fixed a wierd framer-motion quirk ([96c0b6b](https://gitlab.com/robotandkid/consulting/commit/96c0b6beba7dac6601f9d6c5856818dcfc5bab49))





## [0.21.6](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.5...@robotandkid/urielavalos@0.21.6) (2021-07-29)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.21.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.4...@robotandkid/urielavalos@0.21.5) (2021-07-29)


### Bug Fixes

* **urielavalos:** typo in meta description ([bca58b5](https://gitlab.com/robotandkid/consulting/commit/bca58b5ae197c6e6ad89cecb35d5d61e9c7c1028))





## [0.21.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.3...@robotandkid/urielavalos@0.21.4) (2021-07-29)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.21.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.2...@robotandkid/urielavalos@0.21.3) (2021-07-29)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.21.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.1...@robotandkid/urielavalos@0.21.2) (2021-07-29)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.21.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.21.0...@robotandkid/urielavalos@0.21.1) (2021-07-28)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.21.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.20.2...@robotandkid/urielavalos@0.21.0) (2021-07-27)


### Features

* added robots.txt ([06493ee](https://gitlab.com/robotandkid/consulting/commit/06493eebb8ca980a6e588b8d88e632422167540e))





## [0.20.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.20.1...@robotandkid/urielavalos@0.20.2) (2021-07-27)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.20.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.20.0...@robotandkid/urielavalos@0.20.1) (2021-07-23)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.20.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.19.0...@robotandkid/urielavalos@0.20.0) (2021-07-22)


### Features

* **urielavalos:** added faviicon ([c2976d4](https://gitlab.com/robotandkid/consulting/commit/c2976d4d91a92989cd0b5fc007a820324b5c2d5d))





# [0.19.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.18.0...@robotandkid/urielavalos@0.19.0) (2021-07-22)


### Bug Fixes

* **urielavalos:** fixed heading / added Row ([17a3fbf](https://gitlab.com/robotandkid/consulting/commit/17a3fbfe06488d319043f04c962247569474e274))
* **urielavalos:** style fixes ([5e8282e](https://gitlab.com/robotandkid/consulting/commit/5e8282eb4b77a48545181882271466cb2ab7cf7b))


### Features

* **urielavalos:** added About header ([5416ce5](https://gitlab.com/robotandkid/consulting/commit/5416ce56f79d2f02155c56d10defcf1c1d97b340))
* **urielavalos:** added Contact and h2 headings ([8e865d8](https://gitlab.com/robotandkid/consulting/commit/8e865d8534a9ef76606b09a6e49c3869104040b3))
* **urielavalos:** added heading to gallery section ([12bd084](https://gitlab.com/robotandkid/consulting/commit/12bd0840ba359a184e2d047c964be2b6cce1b159))
* **urielavalos:** added headings ([1f1fe66](https://gitlab.com/robotandkid/consulting/commit/1f1fe660f23b38fe63023eda74a16233be921176))
* **urielavalos:** added media queries helper ([1d88c04](https://gitlab.com/robotandkid/consulting/commit/1d88c0458ccd206e230b48dd3f95efc5902ea4a1))





# [0.18.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.17.2...@robotandkid/urielavalos@0.18.0) (2021-07-15)


### Bug Fixes

* **urielavalos:** sitemap pointed to localhost! ([38a7749](https://gitlab.com/robotandkid/consulting/commit/38a77490c0736534fdc33f0a59c5d30d97f2a642))
* **urielavalos:** structured data bad ([b53c337](https://gitlab.com/robotandkid/consulting/commit/b53c3378c73c5cc36d2237d8f67bf616234326d7))
* **urielavalos:** structured data should pass google check ([53b1c9f](https://gitlab.com/robotandkid/consulting/commit/53b1c9f9db69cc438e1cf5a2f5aa379ef0a33586))


### Features

* **urielavalos:** added gallery to TOC ([a7f0480](https://gitlab.com/robotandkid/consulting/commit/a7f04805fde4a65b8b1b7b3c2fa8d082c77caf8a))





## [0.17.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.17.1...@robotandkid/urielavalos@0.17.2) (2021-07-15)


### Bug Fixes

* **urielavalos:** compiles again ([b25f05c](https://gitlab.com/robotandkid/consulting/commit/b25f05c4d5161f8a9a7ab61851decb9ce1710d83))





## [0.17.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.17.0...@robotandkid/urielavalos@0.17.1) (2021-07-15)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.17.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.16.0...@robotandkid/urielavalos@0.17.0) (2021-07-14)


### Features

* **components:** added TitleSEO ([3f6a86c](https://gitlab.com/robotandkid/consulting/commit/3f6a86c58e8f96dcfe46a01fdd63a6b9b9973d2a))





# [0.16.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.15.2...@robotandkid/urielavalos@0.16.0) (2021-07-13)


### Bug Fixes

* **urielavalos:** fixed SEO issues ([f46a512](https://gitlab.com/robotandkid/consulting/commit/f46a512f341d0cd943581c62e48258d93b18ff99))


### Features

* added semantic HTML ([0a0203c](https://gitlab.com/robotandkid/consulting/commit/0a0203c9909b5f9a4cd2b546556240855c9fd6d2))
* added sitemaps ([1d1b58a](https://gitlab.com/robotandkid/consulting/commit/1d1b58a8d7392c303c992beeca84104e52a6f2e1))





## [0.15.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.15.1...@robotandkid/urielavalos@0.15.2) (2021-07-01)


### Bug Fixes

* **urielavalos:** clicking home scrolls to top ([7ecafcd](https://gitlab.com/robotandkid/consulting/commit/7ecafcd0ec385a952b082470f12bd9cec1454d82))
* **urielavalos:** contact form opens w/o a hitch ([2bc488a](https://gitlab.com/robotandkid/consulting/commit/2bc488aee9c206aa8b9459f48c8c211043717e5e))
* **urielavalos:** title has correct zindex ([d0dce6b](https://gitlab.com/robotandkid/consulting/commit/d0dce6b7306b2ba9d79068fa904a93c1055ef081))





## [0.15.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.15.0...@robotandkid/urielavalos@0.15.1) (2021-06-23)


### Bug Fixes

* **urielavalos:** better SEO description ([18fe667](https://gitlab.com/robotandkid/consulting/commit/18fe6676b7dd7bef8a6b8d6020b47f29e79309e7))





# [0.15.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.14.1...@robotandkid/urielavalos@0.15.0) (2021-06-22)


### Bug Fixes

* **urielavalos:** fixed SEO: added document title ([4d02ea7](https://gitlab.com/robotandkid/consulting/commit/4d02ea7da8bcd47e4309c16ed137aa3002d5ea4e))


### Features

* **urielavalos:** added google site verification ([aa0b6d3](https://gitlab.com/robotandkid/consulting/commit/aa0b6d36fc6530431f52d7194aadc16a00c92372))
* **urielavalos:** added SEO description ([aefd18a](https://gitlab.com/robotandkid/consulting/commit/aefd18ac6a9df5255646600043bde9d2e9516112))





## [0.14.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.14.0...@robotandkid/urielavalos@0.14.1) (2021-06-21)


### Bug Fixes

* **nextjs:** default image loader not disabled for exporting ([b2fe045](https://gitlab.com/robotandkid/consulting/commit/b2fe0454484c10886868c113a01c2c2a0e814549))





# [0.14.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.7...@robotandkid/urielavalos@0.14.0) (2021-06-21)


### Features

* **urielavalos:** added Art gallery item type ([8fefcaf](https://gitlab.com/robotandkid/consulting/commit/8fefcaf906f10c2faa80d5862540b49e03102844))
* **urielavalos:** added styles for Oscilloscope ([52733c2](https://gitlab.com/robotandkid/consulting/commit/52733c2acf9de7cf29272ba14e004d370ead0c10))
* **urielavalos:** styled Art gallery item ([efad1e6](https://gitlab.com/robotandkid/consulting/commit/efad1e6089639c3229a165599b4754f8082902b2))
* **urielavalos:** styled summer poem ([a1da28a](https://gitlab.com/robotandkid/consulting/commit/a1da28a07b6b4cfd3e5f15c94f20b5f0b402c20e))





## [0.13.7](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.6...@robotandkid/urielavalos@0.13.7) (2021-06-19)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.13.6](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.5...@robotandkid/urielavalos@0.13.6) (2021-06-19)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.13.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.4...@robotandkid/urielavalos@0.13.5) (2021-06-18)


### Bug Fixes

* **urielavalos:** compiles again ([c9baf55](https://gitlab.com/robotandkid/consulting/commit/c9baf55d745e363a2254c1f424dcc5f88d67eb8a))





## [0.13.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.3...@robotandkid/urielavalos@0.13.4) (2021-06-17)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.13.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.2...@robotandkid/urielavalos@0.13.3) (2021-06-16)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.13.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.1...@robotandkid/urielavalos@0.13.2) (2021-06-15)

**Note:** Version bump only for package @robotandkid/urielavalos





## [0.13.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.13.0...@robotandkid/urielavalos@0.13.1) (2021-06-15)

**Note:** Version bump only for package @robotandkid/urielavalos





# [0.13.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.12.1...@robotandkid/urielavalos@0.13.0) (2021-06-03)


### Features

* **urielavalos:** added more content ([45d409f](https://gitlab.com/robotandkid/consulting/commit/45d409fe9482bbac9671bdbbd02bde59e4aeea79))





## [0.12.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.12.0...@robotandkid/urielavalos@0.12.1) (2021-05-28)


### Bug Fixes

* **urielavalos:** properly using textscroll sound property ([52e7822](https://gitlab.com/robotandkid/consulting/commit/52e7822e8f4a9b057a42b8f57f5a34f81c40d491))





# [0.12.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.11.0...@robotandkid/urielavalos@0.12.0) (2021-05-27)


### Bug Fixes

* **urielavalos:** better codeBlock styling in small screens ([69b6b15](https://gitlab.com/robotandkid/consulting/commit/69b6b159e3648ad3cfd976752608b0903d5eecdd))
* **urielavalos:** title break in gallery grid ([aa04c55](https://gitlab.com/robotandkid/consulting/commit/aa04c55cce2b864c08f610df0659c22a2bfac3d8))
* **urielavalos:** updated grid styles ([90433e3](https://gitlab.com/robotandkid/consulting/commit/90433e37bdb9bd4d3866cb4c60b691fd8b17005f))


### Features

* **urielavalos:** added dynamic component for AwesomeRetroCutscens ([15d92ca](https://gitlab.com/robotandkid/consulting/commit/15d92caf75a447520afa95436d91c2f789a37577))
* **urielavalos:** added InternalLink ([8e191b4](https://gitlab.com/robotandkid/consulting/commit/8e191b4b1b63f8078fd6963a79b151ddf94101d1))
* **urielavalos:** better codeBlock styles ([4abbcf5](https://gitlab.com/robotandkid/consulting/commit/4abbcf5c52238534d70506c63be772877ee2151f))
* **urielavalos:** updated blog content ([4f12137](https://gitlab.com/robotandkid/consulting/commit/4f1213723f206543454ea46c25562c562c79ffc8))





# [0.11.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.10.0...@robotandkid/urielavalos@0.11.0) (2021-05-24)


### Features

* **urielavalos:** better codeBlock theme ([eab9955](https://gitlab.com/robotandkid/consulting/commit/eab9955f61366e0f88c7318c3ebc5487a67e9a28))





# [0.10.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.9.0...@robotandkid/urielavalos@0.10.0) (2021-05-17)


### Bug Fixes

* **scripts:** added missing dependency to cmdAppMDXCodeBlocks ([39b4fe7](https://gitlab.com/robotandkid/consulting/commit/39b4fe75924be2e8e90de63d1c67a3e28f2ff162))
* **urielavalos:** compiles again ([fe0ec41](https://gitlab.com/robotandkid/consulting/commit/fe0ec41060f7cbb226cc24650c124ef52ea817c9))
* **urielavalos:** compiles again ([bf1214c](https://gitlab.com/robotandkid/consulting/commit/bf1214cd0491b9687bb5571a1f59038d85380508))
* **urielavalos:** page has proper width ([4dee56e](https://gitlab.com/robotandkid/consulting/commit/4dee56e18a7bc714f85c3cba21f36aea3e9a9e72))
* **urielavalos:** properly center page ([bc43d4f](https://gitlab.com/robotandkid/consulting/commit/bc43d4f0d2dc86933cb403d11cb897dd82ad4f1f))


### Features

* **urielavalos:** 1st pass code block theme ([7d8b17a](https://gitlab.com/robotandkid/consulting/commit/7d8b17a203efef1dc481ae2d737a751cfcf65103))





# [0.9.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.8.1...@robotandkid/urielavalos@0.9.0) (2021-05-15)


### Features

* **urielavalos:** added H2 styles ([3e1b567](https://gitlab.com/robotandkid/consulting/commit/3e1b5676e0bcbc4d8d62287fd1a445e0f534f57f))
* **urielavalos:** added h3,p,a,li styles + colors ([7619629](https://gitlab.com/robotandkid/consulting/commit/76196296825d508293eb031e18e8b24f39db0ceb))
* **urielavalos:** added mdx wrapper ([33c2d21](https://gitlab.com/robotandkid/consulting/commit/33c2d21de08901eddb289e6a33a19e0ff52ca61e))
* **urielavalos:** added mdx wrapper ([86475ba](https://gitlab.com/robotandkid/consulting/commit/86475ba3c4551be0f875e68df01fc761b156a5ce))
* **urielavalos:** added theme types ([5f97db3](https://gitlab.com/robotandkid/consulting/commit/5f97db3584477929292169fc08d74bb72d525a5c))
* **urielavalos:** styled code blocks ([0322f74](https://gitlab.com/robotandkid/consulting/commit/0322f741ef136b3466bc5f6daf5bf4f76123f315))





## [0.8.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.8.0...@robotandkid/urielavalos@0.8.1) (2021-05-05)


### Bug Fixes

* lints again ([244cb09](https://gitlab.com/robotandkid/consulting/commit/244cb092058fad3e8fa096d3eaf7cdf91c1e9732))





# [0.8.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.7.1...@robotandkid/urielavalos@0.8.0) (2021-05-03)


### Features

* filtering out draft posts ([4b76fd0](https://gitlab.com/robotandkid/consulting/commit/4b76fd027994b8f95147e3bf0dbb4f14132eeae1))





## [0.7.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.7.0...@robotandkid/urielavalos@0.7.1) (2021-04-20)


### Bug Fixes

* downgraded react --- causing styled-components SSR bug ([7c6fc16](https://gitlab.com/robotandkid/consulting/commit/7c6fc16233d001207816be6da2567f45fba72104))


### Reverts

* **react:** upgraded back to 17.0.1 ([f194805](https://gitlab.com/robotandkid/consulting/commit/f19480562935d936b42a1b204c6c104f87cadd97))





# [0.7.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.6.1...@robotandkid/urielavalos@0.7.0) (2021-04-16)


### Bug Fixes

* downgraded packages to fix styled-components SSR bug ([ae008cc](https://gitlab.com/robotandkid/consulting/commit/ae008ccd4bac666f113b7a710b1ca8b4327f2981))


### Features

* **apps/urielavalos:** added 1st cypress test ([8093a0c](https://gitlab.com/robotandkid/consulting/commit/8093a0c52cffbd60f88efa6c3b9f0b9b014dc1d9))
* added cypress task ([a82f278](https://gitlab.com/robotandkid/consulting/commit/a82f2787d61cef8b4f3ca6c44e9d9e99c8331b95))





## [0.6.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.6.0...@robotandkid/urielavalos@0.6.1) (2021-04-02)

### Bug Fixes

- **urielavalos:** removing browser warning ([d848595](https://gitlab.com/robotandkid/consulting/commit/d848595c80460a116eb22f5d6d01e836a364bafc))

# [0.6.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.5.1...@robotandkid/urielavalos@0.6.0) (2021-03-30)

### Features

- **components/statictoc:** fully working ([dc1d9a7](https://gitlab.com/robotandkid/consulting/commit/dc1d9a79eedc6b27d327a2ef1901cbc4748a3aef))
- **urielavalos:** added header section ([0365f73](https://gitlab.com/robotandkid/consulting/commit/0365f73e950978fe486affc949d540054f2b9046))

## [0.5.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.5.0...@robotandkid/urielavalos@0.5.1) (2021-03-26)

### Bug Fixes

- **urielavalos/link:** now supports multiple TOC link selection ([3897613](https://gitlab.com/robotandkid/consulting/commit/389761302f367a3ac715ca760c1fa5e66bcd5a5a))
- lints again ([deb4427](https://gitlab.com/robotandkid/consulting/commit/deb44276affe2dd5990a953af61a272ca5d1a5ed))

# [0.5.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.4.0...@robotandkid/urielavalos@0.5.0) (2021-03-10)

### Bug Fixes

- **urielavalos:** disabled landing hero when not on landing page ([32128fc](https://gitlab.com/robotandkid/consulting/commit/32128fc01dea88bb74ec21f7963972322d665e77))

### Features

- **urielavalos:** passing href in GalleryGrid ([bd15a2f](https://gitlab.com/robotandkid/consulting/commit/bd15a2f8bfded81f3870319fc7e4227586399e5e))

# [0.4.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.3.0...@robotandkid/urielavalos@0.4.0) (2021-02-27)

### Features

- **components:** added typescript/Unpromisify ([8dc5c8c](https://gitlab.com/robotandkid/consulting/commit/8dc5c8c367fc7e5fe0b0a6736d3b11bb92edf376))

# [0.3.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.2.5...@robotandkid/urielavalos@0.3.0) (2021-02-26)

### Bug Fixes

- **urielavalos:** loading gallery grid data properly ([6ae3887](https://gitlab.com/robotandkid/consulting/commit/6ae38876c2c87dc88a695491b6ba89fd90f10875))

### Features

- **urielavalos:** added JSON schema validation ([d8e85bb](https://gitlab.com/robotandkid/consulting/commit/d8e85bb535fb5d10367af39c818d08091edb8ed5))
- **urielavalos:** added MDX support ([e546147](https://gitlab.com/robotandkid/consulting/commit/e546147ab6c3149d56249abfff49dad10e29cffe))
- **urielavalos:** updated About component ([42aa135](https://gitlab.com/robotandkid/consulting/commit/42aa1357e313ac978bc1f2b4032f072976f9a244))

## [0.2.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.2.4...@robotandkid/urielavalos@0.2.5) (2021-02-07)

**Note:** Version bump only for package @robotandkid/urielavalos

## [0.2.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.2.3...@robotandkid/urielavalos@0.2.4) (2021-02-06)

**Note:** Version bump only for package @robotandkid/urielavalos

## [0.2.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.2.2...@robotandkid/urielavalos@0.2.3) (2021-02-06)

**Note:** Version bump only for package @robotandkid/urielavalos

## [0.2.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.2.1...@robotandkid/urielavalos@0.2.2) (2021-02-06)

**Note:** Version bump only for package @robotandkid/urielavalos

## [0.2.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.2.0...@robotandkid/urielavalos@0.2.1) (2021-02-05)

**Note:** Version bump only for package @robotandkid/urielavalos

# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/urielavalos@0.1.0...@robotandkid/urielavalos@0.2.0) (2021-02-05)

### Features

- **urielavalos:** added old code ([7b4ebd0](https://gitlab.com/robotandkid/consulting/commit/7b4ebd0cd58565d01eeed23c873b8c37bff04a76))

# 0.1.0 (2021-02-04)

### Features

- added urielavalos site ([30a4c3c](https://gitlab.com/robotandkid/consulting/commit/30a4c3cc1aebf53e7bbadd7e491f1c827383a9ba))
