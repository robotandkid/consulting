import type { GlobalProvider } from "@ladle/react";
import React from "react";
import { LazyMotion, domMax } from "framer-motion";
import { GlobalStyle } from "../src/styles/GlobalStyle";
import { AppThemeProvider } from "../src/styles/theme";

const loadFramerFeatures = () =>
  import("framer-motion").then((module) => module.domMax);

export const Provider: GlobalProvider = (props) => (
  <>
    <GlobalStyle />
    <LazyMotion strict features={loadFramerFeatures}>
      <AppThemeProvider>{props.children}</AppThemeProvider>
    </LazyMotion>
  </>
);
