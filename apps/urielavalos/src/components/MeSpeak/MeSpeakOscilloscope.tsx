import { OscilloscopeOpts } from "@robotandkid/pixi-components";
import React, { useEffect, useState } from "react";
import { DynamicOscilloscope as Oscilloscope } from "../../dynamic/DynamicPixiComponents";
import { getMeSpeak } from "./MeSpeak";

type OscilloscopeProps = Pick<
  OscilloscopeOpts,
  "style" | "lineStyle" | "parent"
> & { children?: React.ReactNode };

export function MeSpeakOscilloscope(props: OscilloscopeProps) {
  const { children, ...rest } = props;
  const [analyser, setAnalyser] = useState<AnalyserNode>();

  useEffect(() => {
    setAnalyser(getMeSpeak()?.getAudioAnalyser());
  }, []);

  return (
    <>
      <Oscilloscope analyser={analyser} {...rest} />
      {!!analyser && children}
    </>
  );
}
