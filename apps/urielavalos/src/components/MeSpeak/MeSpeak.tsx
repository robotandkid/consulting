import React, { ReactNode, useEffect, useState } from "react";
import { ComicPlaceholder } from "../../styles/styles";

interface InternalMeSpeak {
  canPlay(): boolean;
  loadVoice(path: string): void;
  getAudioAnalyser(): AnalyserNode;

  speak(
    text: string,
    /**
     * Options.capitals:
     *
     * 1. Use a click sound to indicate when a word starts with a capital letter,
     *    or double click if word is all capitals.
     * 2. Speak the word "capital" before a word which begins with a capital
     *    letter.
     * 3. Increases the pitch for words which begin with a capital letter. The
     *    greater the value, the greater the increase in pitch. (eg.: 20)
     */
    options?: { capitals: 1 | 2 | 3 },
    callback?: (success: boolean, id: number) => void
  ): number;

  stop(id?: number): void;
}

interface MeSpeak {
  canPlay(): boolean;

  loadVoice(path: string): void;

  getAudioAnalyser(): AnalyserNode;

  /** Promise resolve after meSpeak has finished talking */
  speak(
    text: string,
    opts?: {
      /** Default: 100 */
      amplitude?: number;
      /** Default: 50 */
      pitch?: number;
      /** Default: 175 */
      speed?: number;
      /**
       * Default: 0
       *
       * In units of 10 ms
       */
      wordgap?: number;
      capitals?: 1 | 2 | 3;
    }
  ): Promise<boolean>;

  stop(id?: number): void;
}

const isMeSpeak = (win?: any): win is InternalMeSpeak => {
  return !!(win as InternalMeSpeak)?.loadVoice;
};

export function getMeSpeak(): MeSpeak | undefined {
  const meSpeak = (window as any).meSpeak;

  if (isMeSpeak(meSpeak)) {
    type SpeakArgs = Parameters<MeSpeak["speak"]>;

    const speak = (...args: SpeakArgs) => {
      return new Promise<boolean>((resolve) => {
        meSpeak.speak(args[0], { capitals: 3, ...args[1] }, (isSuccess) => {
          resolve(isSuccess);
        });
      });
    };

    return {
      ...meSpeak,
      speak,
    };
  }
}

/**
 * The children will be rendered when mespeak has finished loading. Children can
 * then safely invoke getMeSpeak.
 */
export function WrapMeSpeak(props: { children?: ReactNode }) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const script = document.createElement("script");
    script.src = "/mespeak/mespeak.js";
    script.onload = () => {
      if (getMeSpeak()?.canPlay()) {
        getMeSpeak()?.loadVoice("/mespeak/voices/en/en-us.json");
        setLoaded(true);
      }
    };
    document.head.append(script);
  }, []);

  return loaded ? (
    <>{props.children}</>
  ) : (
    <ComicPlaceholder>Loading...</ComicPlaceholder>
  );
}
