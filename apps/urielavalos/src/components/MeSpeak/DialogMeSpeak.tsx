import { useEffect, useRef, useState } from "react";
import { getMeSpeak } from "./MeSpeak";
import {
  UiDialogPanel,
  sleep,
  usePagination,
} from "@robotandkid/sprite-motion";

const DialogMeSpeakInner = (props: {
  dialog: string;
  visibleLines: number;
  paginate: ReturnType<typeof usePagination>;
  Dialog: (
    props: {
      children: string;
    } & Pick<DialogProps, "wordSpeed" | "onDone" | "visibleLines">
  ) => JSX.Element;
}) => {
  const { visibleLines, dialog, Dialog, paginate } = props;
  const meSpeak = getMeSpeak();

  const done = useRef({
    speech: false,
    speechTime: 0,
    text: false,
    textTime: 0,
    called: false,
  });

  const pageRef = useRef(paginate);
  pageRef.current = paginate;

  const checkDone = useRef(async () => {
    const stats = done.current;
    if (stats.speech && stats.text && !stats.called) {
      stats.called = true;
      const time = Math.min(stats.speechTime, stats.textTime);
      if (time < 3_000) await sleep(3_000 - time);
      else await sleep(500);
      pageRef.current?.("next-fade");
    }
  });

  const mounted = useRef(false);

  useEffect(() => {
    mounted.current = true;
    return () => {
      mounted.current = false;
    };
  });

  const meSpeakRef = useRef(meSpeak);
  meSpeakRef.current = meSpeak;

  useEffect(() => {
    if (!meSpeakRef.current) return;

    const time = Date.now();

    const go = async () => {
      const lines = dialog.trim().split("\n");

      for (let lineNum = 0; lineNum < lines.length; lineNum += 1) {
        if (!mounted.current) return;
        const line = lines[lineNum];
        if (lineNum === lines.length - 1) done.current.text = true;
        await meSpeakRef.current?.speak(line ?? "", { speed: 225 });
      }

      if (!mounted.current) return;
      done.current.speech = true;
      done.current.speechTime = Date.now() - time;
      await checkDone.current();
    };

    go();
  }, [dialog]);

  const time = Date.now();

  return (
    <Dialog
      visibleLines={visibleLines}
      wordSpeed={10}
      onDone={async () => {
        done.current.text = true;
        done.current.textTime = Date.now() - time;
        await checkDone.current();
      }}
    >
      {dialog.trim()}
    </Dialog>
  );
};

type DialogProps = Parameters<typeof UiDialogPanel>[0];

export const DialogMeSpeak = (props: {
  dialog: string;
  stop?: boolean;
  visibleLines: number;
  Dialog: (
    props: {
      children: string;
    } & Pick<DialogProps, "wordSpeed" | "onDone" | "visibleLines">
  ) => JSX.Element;
}) => {
  const { dialog, visibleLines } = props;
  const [location, setLocation] = useState(0);
  const lines = dialog.trim().split("\n");

  const currentDialog = lines
    .filter((_, index) => {
      return location <= index && index < location + visibleLines;
    })
    .join("\n");

  const updateLocation = () => setLocation((l) => l + visibleLines);
  const paginate = usePagination();
  const realPaginate = props.stop ? () => {} : paginate;

  return (
    <DialogMeSpeakInner
      key={currentDialog}
      {...props}
      dialog={currentDialog}
      paginate={
        location + visibleLines < lines.length ? updateLocation : realPaginate
      }
    />
  );
};
