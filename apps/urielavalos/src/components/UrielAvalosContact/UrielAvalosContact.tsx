import { LinkTarget, SetStaticTOC } from "@robotandkid/components";
import { GithubLogo, GitlabLogo, LinkedinLogo } from "phosphor-react";
import styled from "styled-components";
import { SectionContainer, Text } from "../../styles/styles";
import { SectionExternalLink } from "../Section/SectionAnchor";
import { UrielAvalosH2 } from "../Section/SectionHeadings";

const GithubIcon = styled(GithubLogo)`
  width: 1.5em;
  height: 1.5em;
  outline: none;
  border: none;
  ${(p) => p.theme.entities.section.anchor()}
`;

const GitlabIcon = styled(GitlabLogo)`
  width: 1.5em;
  height: 1.5em;
  outline: none;
  border: none;
  ${(p) => p.theme.entities.section.anchor()}
`;

const LinkedinIcon = styled(LinkedinLogo)`
  width: 1.5em;
  height: 1.5em;
  outline: none;
  border: none;
  ${(p) => p.theme.entities.section.anchor()}
`;

const Link = styled(Text)`
  display: flex;
  align-items: center;

  & > svg {
    margin-right: 0.5em;
  }
`;

const Links = styled.ul`
  list-style-type: none;
  padding-bottom: 2rem;
  margin-left: 0;
  padding-left: 0;
`;

const href = "#contact";
const name = "contact";

export function UrielAvalosContact() {
  return (
    <>
      <SectionContainer as="section">
        <LinkTarget href={href}>
          <UrielAvalosH2>{name}</UrielAvalosH2>
        </LinkTarget>
        <Links>
          <Link as="li">
            <GithubIcon />
            <SectionExternalLink href="https://github.com/frankandrobot">
              github.com/frankandrobot
            </SectionExternalLink>
          </Link>
          <Link as="li">
            <GitlabIcon />
            <SectionExternalLink href="https://gitlab.com/robotandkid">
              gitlab.com/robotandkid
            </SectionExternalLink>
          </Link>
          <Link as="li">
            <LinkedinIcon />
            <SectionExternalLink href="https://www.linkedin.com/in/uriel-avalos/">
              linkedin/in/uriel-avalos
            </SectionExternalLink>
          </Link>
        </Links>
      </SectionContainer>
      <SetStaticTOC href={href} name={name} index={3} />
    </>
  );
}
