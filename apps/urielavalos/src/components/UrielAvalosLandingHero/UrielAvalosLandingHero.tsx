import {
  LandingHero,
  LinkTarget,
  SetStaticTOC,
  shrinkLandingHero,
} from "@robotandkid/components";
import { m } from "framer-motion";
import { useRouter } from "next/router";
import styled, { css } from "styled-components";
import { media } from "../../styles/media";
import { sharedTitleStyle } from "../../styles/styles";
import { UrielAvalosGameFixFocus } from "../UrielAvalosGame/UrielAvalosGame";

const Title = styled(m.h1)`
  ${sharedTitleStyle}

  ${(p) => p.theme.entities.landing.title}
  text-align: center;
  font-size: 8rem;
  font-weight: normal;

  text-shadow: 0.5rem 0.5rem black;

  ${media("tablet")(css`
    font-size: 13rem;
  `)}

  ${media("desktop")(css`
    font-size: 16rem;
    text-shadow: 0.5rem 0.5rem black;
  `)}
`;

const SmallTitle = styled(m.h1)`
  ${sharedTitleStyle}
  ${(p) => p.theme.entities.landing.title}
  font-size: 2rem;
`;

const Letter = styled.span<{ index: number }>`
  color: ${(p) => p.theme.entities.landing.letterColor(p.index)};
`;

const Letters = (props: { children: string }) => {
  return (
    <>
      {props.children.split("").map((char, index) => (
        <Letter key={index} index={index}>
          {char}
        </Letter>
      ))}
    </>
  );
};

const href = "#title";
const name = "home";

export function UrielAvalosLandingHero() {
  const { route } = useRouter() || {};

  return (
    <>
      <UrielAvalosGameFixFocus callback={shrinkLandingHero} />
      <LandingHero disabled={route !== "/"}>
        <Title>
          <LinkTarget href={href}>
            <section aria-label="uriel avalos">
              <Letters>uriel</Letters>
              <br />
              <Letters>avalos</Letters>
            </section>
          </LinkTarget>
        </Title>
        <SmallTitle>
          <section aria-label="uriel avalos">
            <Letters>uriel avalos</Letters>
          </section>
        </SmallTitle>
      </LandingHero>
      <SetStaticTOC index={0} href={href} name={name} />
    </>
  );
}
