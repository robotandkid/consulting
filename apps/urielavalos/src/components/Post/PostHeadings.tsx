import styled, { css } from "styled-components";
import { media } from "../../styles/media";
import { sharedTitleStyle } from "../../styles/styles";

export const PostH2 = styled.h2`
  ${sharedTitleStyle}
  ${(p) => p.theme.entities.post.h2()}
  font-weight: bold;
  font-size: 2rem;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;
  margin-top: 0.5em;

  ${media("tablet")(css`
    font-size: 3rem;
  `)}
`;

export const PostH3 = styled.h3`
  ${sharedTitleStyle}
  font-weight: bold;
  ${(p) => p.theme.entities.post.h3()}
  font-size: 1.5rem;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;

  ${media("tablet")(css`
    font-size: 2.5rem;
  `)}
`;
