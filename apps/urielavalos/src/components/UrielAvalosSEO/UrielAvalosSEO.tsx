import { StructuredDataSEO, TitleSEO } from "@robotandkid/components";
import React from "react";

const structuredData = {
  "@id": "https://www.urielavalos.com",
  address: {
    "@type": "PostalAddress",
    addressLocality: "Austin",
    addressRegion: "TX",
    addressCountry: "US",
  },
  familyName: "Avalos",
  givenName: "Uriel",
  name: "Uriel Avalos",
  contactPoint: [
    {
      "@type": "ContactPoint",
      name: "Github",
      identifier: "https://github.com/frankandrobot",
      contactType: "github URL",
    },
    {
      "@type": "ContactPoint",
      name: "Gitlab",
      identifier: "https://gitlab.com/robotandkid",
      contactType: "gitlab URL",
    },
    {
      "@type": "ContactPoint",
      name: "Linkedin",
      identifier: "https://www.linkedin.com/in/uriel-avalos/",
      contactType: "linkedin URL",
    },
  ],
  hasOccupation: [
    {
      "@type": "Occupation",
      name: "Dad",
      occupationLocation: [{ "@type": "City", name: "The Universe" }],
      estimatedSalary: [
        {
          "@type": "MonetaryAmountDistribution",
          name: "base",
          currency: "USD",
          duration: "P1Y",
          percentile10: "0",
          percentile25: "0",
          median: "0",
          percentile75: "0",
          percentile90: "0",
        },
      ],
    },
    {
      "@type": "Occupation",
      name: "Software Engineer",
      occupationLocation: [{ "@type": "City", name: "The Universe" }],
      estimatedSalary: [
        {
          "@type": "MonetaryAmountDistribution",
          name: "base",
          currency: "USD",
          duration: "P1Y",
          percentile10: "100000",
          percentile25: "125000",
          median: "150000",
          percentile75: "175000",
          percentile90: "250000",
        },
      ],
    },
    {
      "@type": "Occupation",
      name: "Husband",
      occupationLocation: [{ "@type": "City", name: "The Universe" }],
      estimatedSalary: [
        {
          "@type": "MonetaryAmountDistribution",
          name: "base",
          currency: "USD",
          duration: "P1Y",
          percentile10: "0",
          percentile25: "0",
          median: "0",
          percentile75: "0",
          percentile90: "0",
        },
      ],
    },
    {
      "@type": "Occupation",
      name: "Programmatic Illustrator",
      occupationLocation: [{ "@type": "City", name: "The Universe" }],
      estimatedSalary: [
        {
          "@type": "MonetaryAmountDistribution",
          name: "base",
          currency: "USD",
          duration: "P1Y",
          percentile10: "0",
          percentile25: "0",
          median: "0",
          percentile75: "0",
          percentile90: "0",
        },
      ],
    },
  ],
  "@context": "https://schema.org",
  "@type": "Person",
  url: "https://www.urielavalos.com",
};

const title = `Uriel Avalos, the Software Engineer - Comics, Art, Tech Articles`;

const description = `The website of Uriel Avalos, the Software Engineer. He's also a dad, husband, and programmatic illustrator. He's passionate about technology and people.`;

export function UrielAvalosSEO() {
  return (
    <>
      <TitleSEO>
        {title}
        {description}
      </TitleSEO>
      <StructuredDataSEO structuredData={structuredData} />;
    </>
  );
}
