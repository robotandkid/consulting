import dynamic from "next/dynamic";
import React from "react";
import { ComicPlaceholder } from "../../styles/styles";
import { theme } from "../../styles/theme";
import { AwesomeRetroCutscenesProps } from "awesome-retro-video-game-cutscenes/dist/types/src/AwesomeRetroCutscenes/AwesomeRetroCutscenes.types";

export const awesomeRetroCutscenes = (title: string) =>
  dynamic<AwesomeRetroCutscenesProps>(
    () =>
      import("awesome-retro-video-game-cutscenes").then(
        (module) => module.AwesomeRetroCutscenes
        // TODO why does this not compile?
      ) as any,
    {
      ssr: false,
      loading() {
        return <ComicPlaceholder theme={theme}>{title}</ComicPlaceholder>;
      },
    }
  );
