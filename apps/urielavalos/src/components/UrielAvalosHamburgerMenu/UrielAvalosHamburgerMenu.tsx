import {
  HamburgerMenu,
  HamburgerMenuItem,
  InternalLink,
  StaticTOCContext,
} from "@robotandkid/components";
import { List, XCircle } from "phosphor-react";
import React, { useContext } from "react";
import styled, { css } from "styled-components";
import { glitchKeyframe } from "../../styles/keyframes";

const Container = styled.div`
  position: relative;
`;

const MenuButtonIcon = styled(List)`
  width: 3.5em;
  height: 3.5em;
  outline: none;
  border: none;
`;

const XButtonIcon = styled(XCircle)`
  width: 3.5em;
  height: 3.5em;
  outline: none;
  border: none;
`;

const glitch = css<{ children: string; pressed?: boolean }>`
  :before,
  :after {
    display: block;
    content: "${(p) => p.children}";
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    opacity: 0.8;
  }

  :after {
    color: ${(p) => p.theme.color.secondary};
    z-index: -2;
  }

  :before {
    color: ${(p) => p.theme.color.tertiary};
    z-index: -1;
  }

  :hover,
  :focus {
    &:before {
      animation: ${glitchKeyframe} 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94)
        both infinite;
    }
    &:after {
      animation: ${glitchKeyframe} 0.3s cubic-bezier(0.25, 0.46, 0.45, 0.94)
        reverse both infinite;
    }
  }
`;

const StyledMenuItem = styled(HamburgerMenuItem)`
  ${glitch}

  color: ${(p) => p.theme.entities.menu.color} !important;
  font-family: ${(p) => p.theme.entities.menu.font};
  font-size: 3rem;
  margin-left: 2rem;
  margin-top: 2rem;
  outline: none;
  position: relative;

  &:visited {
    color: ${(p) => p.theme.entities.menu.color} !important;
  }
`;

function MenuItem(props: { href: `#${string}`; name: string }) {
  const { href, name } = props;

  return (
    <InternalLink href={href}>
      <StyledMenuItem
        onClick={
          href === "#title"
            ? () => {
                window.scrollTo({
                  top: 0,
                  left: 0,
                  // not supported by IE or Safari
                  behavior: "smooth",
                });
              }
            : undefined
        }
      >
        {name}
      </StyledMenuItem>
    </InternalLink>
  );
}

export function UrielAvalosHamburgerMenu() {
  const { items } = useContext(StaticTOCContext);

  return (
    <Container>
      <HamburgerMenu>
        <MenuButtonIcon weight="bold" />
        <XButtonIcon weight="bold" />
        {items.map((item) => {
          return <MenuItem key={item.name} href={item.href} name={item.name} />;
        })}
      </HamburgerMenu>
    </Container>
  );
}
