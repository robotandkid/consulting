import { media } from "@robotandkid/components";
import styled, { css } from "styled-components";
import { H2 } from "../../styles/headings";

export const UrielAvalosH2 = styled(H2)`
  ${(p) => p.theme.entities.section.h2()}
  font-size: 4rem;
  text-transform: lowercase;
  text-align: left;
  margin-bottom: 2rem;
  margin-top: 0;

  ${media("tablet")(css`
    font-size: 4rem;
  `)}
`;
