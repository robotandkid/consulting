import styled from "styled-components";
import { ExternalLink } from "../../styles/ExternalLink/ExternalLink";

export const SectionExternalLink = styled(ExternalLink)`
  ${(p) => p.theme.entities.section.anchor()}
`;
