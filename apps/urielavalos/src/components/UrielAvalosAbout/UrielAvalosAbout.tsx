import { LinkTarget, SetStaticTOC } from "@robotandkid/components";
import React from "react";
import styled from "styled-components";
import { SectionContainer, Text } from "../../styles/styles";
import { SectionExternalLink } from "../Section/SectionAnchor";
import { UrielAvalosH2 } from "../Section/SectionHeadings";

const HeartFont = styled.span`
  ${(p) => p.theme.entities.section.highlight()}
`;

function Heart() {
  return <HeartFont>&hearts;</HeartFont>;
}

const AboutText = styled(Text)`
  text-align: left;
`;

const href = "#about";
const name = "about";

export function UrielAvalosAbout() {
  return (
    <>
      <SectionContainer as="section">
        <LinkTarget href={href}>
          <UrielAvalosH2>about</UrielAvalosH2>
        </LinkTarget>
        <AboutText>
          I&apos;m Uriel Avalos&mdash;dad, husband, software engineer,
          programmatic illustrator. I strive to combine software engineering
          excellence with emotional empathy. I&apos;m also passionate about art
          and illustration.
        </AboutText>
        <AboutText>
          Made with{" "}
          <SectionExternalLink href="https://reactjs.org/">
            React
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://styled-components.com/">
            Styled Components
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://nextjs.org/">
            Next.js
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://www.pixijs.com/">
            PixiJS
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://mdxjs.com/">
            MDX
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://www.framer.com/motion/">
            Framer Motion
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://brm.io/matter-js/">
            Matter.js
          </SectionExternalLink>
          , and <Heart />.
        </AboutText>
        <AboutText>
          The site design is inspired by the{" "}
          <SectionExternalLink href="https://brutalistwebsites.com/">
            Brutalist Web Design movement
          </SectionExternalLink>
          , that is, <em>raw</em>, <em>free-form</em> style. Some people would
          call it ugly, but <em>ugliness</em> is a construct :-) This site aims
          to be accessible (but is a work in progress).
        </AboutText>
      </SectionContainer>
      <SetStaticTOC href={href} name={name} index={1} />
    </>
  );
}
