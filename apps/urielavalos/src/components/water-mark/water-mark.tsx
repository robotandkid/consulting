import {
  ResourceVideo,
  Video,
  useCanvasRegistration,
  useKeyRegistration,
  usePanelCanvasCtx2D,
} from "@robotandkid/sprite-motion";

type Color = `#${string}`;

type Props = Parameters<typeof WaterMark>[0];

const defaults = (props: Props): Required<Props> => ({
  borderThickness: props.width * 0.02,
  fontSize: props.width * 0.3,
  fontColor: "#000",
  paddingTop: props.width * 0.06,
  paddingLeft: props.width * 0.06,
  bgColor: "#fff",
  borderColor: "#000000",
  ...props,
});

const fullDefaults = (props: Props) => {
  const d = defaults(props);
  const rowHeight = d.width * 0.3;
  const videoWidth = d.width - d.borderThickness;
  const videoHeight = (9 / 16) * d.width;

  return {
    ...d,
    rowHeight,
    videoWidth,
    videoHeight,
    height: 3 * rowHeight + 2 * videoHeight,
  };
};

export const WaterMark = (props: {
  top: number;
  left: number;
  width: number;
  bgColor?: Color;
  borderColor?: Color;
  borderThickness?: number;
  fontSize?: number;
  fontColor?: Color;
  paddingTop?: number;
  paddingLeft?: number;
  kid: ResourceVideo;
  robot: ResourceVideo;
}) => {
  const render = useCanvasRegistration();
  const [keyChanged, key] = useKeyRegistration("urielavalos-watermark", props);
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && render && ctx) {
    render(() => {
      const {
        top,
        left,
        width,
        height,
        bgColor,
        borderColor,
        borderThickness,
        fontSize,
        fontColor,
        paddingLeft,
        paddingTop,
      } = fullDefaults(props);

      ctx.save();
      {
        ctx.translate(left, top);
        ctx.fillStyle = bgColor;
        ctx.fillRect(0, 0, width, height);

        ctx.strokeStyle = borderColor;
        ctx.lineWidth = borderThickness;
        ctx.strokeRect(0, 0, width, height);
      }
      ctx.restore();

      ctx.save();
      {
        ctx.font = `${fontSize}px mrpixel`;
        ctx.fillStyle = fontColor;
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.translate(left, top);
        ctx.translate(paddingLeft, paddingTop);
        ctx.fillText("ROBOT", 0, 0, width);
        ctx.fillText("AND", 0, fontSize, width);
        ctx.fillText("KID", 0, 2 * fontSize, width);
      }
      ctx.restore();
      return key;
    });
  }

  const { rowHeight, videoWidth, videoHeight, left, borderThickness } =
    fullDefaults(props);

  const videoLeft = left + borderThickness / 2;

  return (
    <>
      <Video
        src={props.robot}
        loop
        renderTop={3 * rowHeight + borderThickness / 2}
        renderLeft={videoLeft}
        renderWidth={videoWidth}
        renderHeight={videoHeight}
      />
      <Video
        src={props.kid}
        loop
        renderTop={3 * rowHeight + videoHeight + borderThickness / 2}
        renderLeft={videoLeft}
        renderWidth={videoWidth}
        renderHeight={videoHeight}
      />
    </>
  );
};
