import { GalleryPager, GalleryPagerInfo } from "@robotandkid/components";
import styled, { css } from "styled-components";
import { media } from "../../styles/media";

const PagerContainer = styled(GalleryPager)`
  display: flex;
  justify-content: space-around;
  align-items: baseline;
`;

const PagerButton = styled.button`
  font-family: ${(p) => p.theme.title.font};
  border: 3px solid black;
  border-radius: 5px;
  background: inherit;
  width: 3rem;
  font-size: 2.5rem;

  ${media("tablet")(css`
    width: 3.5rem;
    font-size: 2rem;
  `)}

  ${media("desktop")(css`
    width: 4rem;
    font-size: 3.5rem;
  `)}

  user-select: none;

  transition: all 0.3s;

  &:disabled {
    border-color: ${(p) => p.theme.color.disabled};
    color: ${(p) => p.theme.color.disabled};
  }

  &:hover {
    border-color: ${(p) => p.theme.color.hover};
    background: ${(p) => p.theme.color.hover};
  }

  &:focus {
    outline-color: ${(p) => p.theme.color.tertiary};
  }

  &:active {
    background: black;
    color: white;
    transform: scale(1.3);
  }
`;

const Pager = styled(GalleryPagerInfo)`
  letter-spacing: -0.1rem;
  margin-left: 1rem;
  margin-right: 1rem;
`;

const leftButton = "<";
const rightButton = ">";

/**
 * - GalleryPager will always show the latest page number
 * - But...
 * - It the call to load the current page (onPagingCommit), is debounced.
 * - Fires `onSwipeRight`/`onSwipeLeft` synchronously after every update
 * - Fires `onPagingCommit` after the `debounce` timeout.
 */

export const UrielAvalosGalleryPager = (
  <PagerContainer>
    <PagerButton>{leftButton}</PagerButton>
    <Pager />
    <PagerButton>{rightButton}</PagerButton>
  </PagerContainer>
);
