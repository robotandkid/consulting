import {
  defaultBasicGalleryGrid,
  DefaultBasicGalleryGrid,
  GalleryItem,
  InMemoryGalleryGrid,
  LinkTarget,
  NormalizedAnchor,
  SetStaticTOC,
} from "@robotandkid/components";
import Link from "next/link";
import React, { useMemo } from "react";
import styled, { css } from "styled-components";
import { media } from "../../styles/media";
import { SectionContainer, Row } from "../../styles/styles";
import { UrielAvalosH2 } from "../Section/SectionHeadings";
import { UrielAvalosGalleryPager } from "./UrielAvalosGalleryPager";

export const galleryTypography = css`
  font-family: ${(p) => p.theme.header.font};
  font-size: 2rem;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: -0.01em;

  ${media("mobileL")(css`
    font-size: 2.5rem;
  `)}

  ${media("tablet")(css`
    font-size: 2rem;
  `)}

  ${media("desktop")(css`
    font-size: 3.5rem;
  `)}
`;

export const pageType = ["comic", "blog", "art"] as const;

export interface UrielAvalosGalleryItem {
  title: string;
  type: typeof pageType[number];
  slug: string;
  href: string;
}

function Item(props: Parameters<typeof GalleryItem>[0]) {
  return (
    <GalleryItem
      {...props}
      whileHover={{
        rotate: "5deg",
        transition: { duration: 0.5 },
      }}
    />
  );
}

export const ComicsItem = styled(Item)`
  ${galleryTypography}
  background: ${(p) => p.theme.entities.gallery.comicItemColor};
`;

export const BlogItem = styled(Item)`
  ${galleryTypography}
  background: ${(p) => p.theme.entities.gallery.blogItemColor};
`;

export const ArtItem = styled(Item)`
  ${galleryTypography}
  background: ${(p) => p.theme.entities.gallery.artItemColor};
`;

export const Title = styled(NormalizedAnchor)`
  cursor: pointer;
`;

function CustomGalleryItem(props: {
  type: UrielAvalosGalleryItem["type"];
  href: string;
  children: string;
}) {
  let Item: undefined | typeof GalleryItem;

  switch (props.type) {
    case "comic":
      Item = ComicsItem;
      break;
    case "blog":
      Item = BlogItem;
      break;
    case "art":
      Item = ArtItem;
      break;
    default: {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const x: never = props.type;
    }
  }

  // should never happen but needed for compilation
  if (!Item) return null;

  return (
    <Item>
      <Link href={props.href}>
        <Title
          tabIndex={0}
          href={props.href}
          dangerouslySetInnerHTML={{ __html: props.children }}
        />
      </Link>
    </Item>
  );
}

const Header = styled(UrielAvalosH2)`
  background-color: inherit !important;
`;

const Section = styled(SectionContainer)`
  background-color: inherit;
`;

const GalleryContainer = styled(Row)``;

const href = "#gallery";
const name = "gallery";

const Grid = styled(DefaultBasicGalleryGrid)`
  ${defaultBasicGalleryGrid}
`;

export function UrielAvalosGalleryGrid(props: {
  children: UrielAvalosGalleryItem[];
}) {
  const items = useMemo(
    () =>
      props.children.map((card) => (
        <CustomGalleryItem key={card.slug} type={card.type} href={card.href}>
          {card.title}
        </CustomGalleryItem>
      )),
    [props.children]
  );

  return (
    <>
      <Row as="section">
        <Section>
          <LinkTarget href={href}>
            <Header>{name}</Header>
          </LinkTarget>
        </Section>
        <GalleryContainer>
          <InMemoryGalleryGrid pageSize={20}>
            {UrielAvalosGalleryPager}
            <Grid>{items}</Grid>
          </InMemoryGalleryGrid>
        </GalleryContainer>
      </Row>
      <SetStaticTOC href={href} name={name} index={2} />
    </>
  );
}
