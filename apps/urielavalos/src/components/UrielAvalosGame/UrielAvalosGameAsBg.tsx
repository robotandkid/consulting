import { useDetectLandingScrolled } from "@robotandkid/components";
import { LayoutGroup, m } from "framer-motion";
import { useRouter } from "next/router";
import styled from "styled-components";
import { UrielAvalosGame } from "./UrielAvalosGame";

const BgParentContainer = styled(m.div)`
  position: relative;
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
`;

const DisabledBg = styled(m.canvas)`
  width: 100%;
  height: 100%;
  background-color: #dfdfdf;
`;

export function UrielAvalosGameAsBg(props: { disabled?: boolean }) {
  const { disabled = false } = props;

  const scrolled = useDetectLandingScrolled();
  const showBg = !(scrolled || disabled);
  const { route } = useRouter() || {};

  if (route !== "/") return null;

  return (
    <BgParentContainer>
      <LayoutGroup id="landing-bg">
        {showBg && (
          <m.div
            layoutId="landing-bg"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1, transition: { duration: 1 } }}
          >
            <UrielAvalosGame />
          </m.div>
        )}
        {!showBg && (
          <DisabledBg
            layoutId="landing-bg"
            initial={{ opacity: 1 }}
            animate={{ opacity: 0, scale: 0, transition: { duration: 1 } }}
          />
        )}
      </LayoutGroup>
    </BgParentContainer>
  );
}
