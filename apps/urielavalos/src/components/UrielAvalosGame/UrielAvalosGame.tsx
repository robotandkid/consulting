import dynamic from "next/dynamic";
import { UseFixFocus } from "./UrielAvalosGame.dynamic";

export const UrielAvalosGame = dynamic(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports don't work
    import("./UrielAvalosGame.dynamic").then(
      (module) => module.UrielAvalosGame
    ) as any,
  {
    ssr: false,
    loading() {
      return null;
    },
  }
);

export const UrielAvalosGameFixFocus = dynamic<
  Parameters<typeof UseFixFocus>[0]
>(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports don't work
    import("./UrielAvalosGame.dynamic").then(
      (module) => module.UseFixFocus
    ) as any,
  {
    ssr: false,
    loading() {
      return null;
    },
  }
);
