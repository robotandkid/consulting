import { isRight } from "fp-ts/lib/Either";
import fs from "fs";
import matter from "gray-matter";
import { MdxRemote } from "next-mdx-remote/types";
import { MetaData, metaDataDecoder } from "./metaDataDecoder";

interface PageOpts {
  path: "./pages/comics" | "./pages/blog" | "./pages/art";
}

export interface MDXPageSource {
  /** Source is currently unused right now */
  source: MdxRemote.Source;
  metaData: MetaData;
}

const rawPageFilenames = async (path: PageOpts["path"]): Promise<string[]> =>
  new Promise((resolve, reject) => {
    fs.readdir(path, (err, files) => {
      if (err) {
        reject(err);
      }

      resolve(files.filter((file) => /.*\.mdx/.test(file)));
    });
  });

const loadRawPage = async (path: PageOpts["path"]) => {
  const filenames = await rawPageFilenames(path);

  return filenames
    .map((filename) => `${path}/${filename}`)
    .map((filename) => ({
      url: filename.replace(/.\/.+?\//, "/").replace(".mdx", ""),
      page: fs.readFileSync(filename),
    }));
};

const mdxLoadFrontMatter = async (path: PageOpts["path"]) => {
  const files = await loadRawPage(path);

  const allFrontMatter = files.map(({ page, url }) => ({
    url,
    metaData: matter(page),
  }));

  return allFrontMatter
    .map((frontMatter, index) => {
      const result = metaDataDecoder.decode({
        url: frontMatter.url,
        ...frontMatter.metaData.data,
      });

      if (!isRight(result)) {
        throw new Error(
          `${files[index]?.url} is missing front matter: ${JSON.stringify(
            result,
            null,
            " "
          )}`
        );
      }

      return { metaData: result.right };
    })
    .filter((source) => !source.metaData.draft);
};

export type FrontMatter = ReturnType<typeof mdxLoadFrontMatter> extends Promise<
  infer Result
>
  ? Result
  : never;

export const loadFrontMatterComics = () => mdxLoadFrontMatter("./pages/comics");
export const loadFrontMatterBlog = () => mdxLoadFrontMatter("./pages/blog");
export const loadFrontMatterArt = () => mdxLoadFrontMatter("./pages/art");
