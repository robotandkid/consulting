import * as t from "io-ts";
import {
  UrielAvalosGalleryItem,
  pageType,
} from "../components/UrielAvalosGalleryGrid/UrielAvalosGalleryGrid";

type DateType = string;
const dateRegex = /^\d\d\d\d-\d\d-\d\d$/;
const dateDecoder = new t.Type<DateType, string, unknown>(
  "date",
  (input: unknown): input is DateType => dateRegex.test(input as any),
  (input, context) =>
    dateRegex.test(input as any)
      ? t.success(input as DateType)
      : t.failure(input, context),
  t.identity
);

type PageType = UrielAvalosGalleryItem["type"];
const pageTypeDecoder = new t.Type<PageType, string, unknown>(
  "pageType",
  (input: unknown): input is PageType => pageType.includes(input as any),
  (input, context) =>
    pageType.includes(input as any)
      ? t.success(input as PageType)
      : t.failure(input, context),
  t.identity
);

type UrlType = `/${PageType}/${string}`;
const urlRegex = /^\/[^/]*\/[^/]*$/;
const urlTypeDecoder = new t.Type<UrlType, string, unknown>(
  "UrlType",
  (input: unknown): input is UrlType => urlRegex.test(input as any),
  (input, context) =>
    urlRegex.test(input as any)
      ? t.success(input as UrlType)
      : t.failure(input, context),
  t.identity
);

export const metaDataDecoder = t.type({
  title: t.string,
  url: urlTypeDecoder,
  created: dateDecoder,
  type: pageTypeDecoder,
  draft: t.union([t.undefined, t.boolean]),
});

export type MetaData = t.TypeOf<typeof metaDataDecoder>;
