import dynamic from "next/dynamic";

export const comicsModules = (filenames: string[]) => {
  return filenames.map((filename) => {
    return dynamic(() => {
      return import(`../../pages/comics/${filename}`);
    });
  });
};
