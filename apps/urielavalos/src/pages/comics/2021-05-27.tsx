import { awesomeRetroCutscenes } from "../../components/AwesomeRetroCutscenes/AwesomeRetroCutscenes";

const AwesomeRetroCutscenes = awesomeRetroCutscenes(
  "A case of mistaken identity"
);

export function Comic_2021_05_27() {
  return (
    <AwesomeRetroCutscenes
      config={{
        background: {
          containerWidth: 128,
          containerHeight: 128,
        },
        dialog: {
          containerHeight: 50,
          containerWidth: 128,
          textScrollSoundPath: "/sounds/textscroll.wav",
        },
      }}
      scenes={[
        {
          title: "start",
          commands: [
            {
              "draw-bg": "/images/2021-05-27/01.png",
            },
            {
              say: "GET OUT OF MY HOUSE!!!",
              actor: "mom",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "02",
            },
          ],
        },
        {
          title: "02",
          commands: [
            {
              "draw-bg": "/images/2021-05-27/02.png",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "03",
            },
          ],
        },
        {
          title: "03",
          commands: [
            {
              "draw-bg": "/images/2021-05-27/03.png",
            },
            {
              say: "Not you sweetie...",
              actor: "mom",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "04",
            },
          ],
        },
        {
          title: "04",
          commands: [
            {
              "draw-bg": "/images/2021-05-27/04.png",
            },
            {
              say: "...the puppy!",
              actor: "mom",
            },
          ],
        },
      ]}
    />
  );
}
