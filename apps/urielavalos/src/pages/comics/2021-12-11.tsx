import { awesomeRetroCutscenes } from "../../components/AwesomeRetroCutscenes/AwesomeRetroCutscenes";

const AwesomeRetroCutscenes = awesomeRetroCutscenes("Based pn a true story");

export function Comic_2021_12_11() {
  return (
    <AwesomeRetroCutscenes
      config={{
        background: {
          containerWidth: 256,
          containerHeight: 256,
        },
        dialog: {
          containerHeight: 50,
          containerWidth: 256,
          textScrollSoundPath: "/sounds/textscroll.wav",
        },
      }}
      scenes={[
        {
          title: "start",
          commands: [
            {
              "draw-bg": "/images/2021-12-11/01-0.png",
            },
            {
              say: "Let's get that chocolate cake",
              actor: "spouse",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "02",
            },
          ],
        },
        {
          title: "02",
          commands: [
            {
              "draw-bg": "/images/2021-12-11/02-0.png",
            },
            {
              actor: "spouse",
              say: "mmm... this is good",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "03",
            },
          ],
        },
        {
          title: "03",
          commands: [
            {
              "draw-bg": "/images/2021-12-11/03-0.png",
            },
            {
              actor: "spouse",
              say: "what?",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "04",
            },
          ],
        },
        {
          title: "04",
          commands: [
            {
              "draw-bg": "/images/2021-12-11/04-0.png",
            },
            {
              actor: "me",
              say: "uh...",
            },
            {
              delayInMs: 2000,
            },
            {
              actor: "me",
              say: "uh...",
            },
            {
              delayInMs: 2000,
            },
            {
              actor: "me",
              say: "uh...",
            },
          ],
        },
      ]}
    />
  );
}
