import { awesomeRetroCutscenes } from "../../components/AwesomeRetroCutscenes/AwesomeRetroCutscenes";

const AwesomeRetroCutscenes = awesomeRetroCutscenes("The road not taken");

export function Comic_2021_11_12() {
  return (
    <AwesomeRetroCutscenes
      config={{
        background: {
          containerWidth: 256,
          containerHeight: 256,
        },
        dialog: {
          containerHeight: 50,
          containerWidth: 256,
          textScrollSoundPath: "/sounds/textscroll.wav",
        },
      }}
      scenes={[
        {
          title: "start",
          commands: [
            {
              "draw-bg": "/images/2021-11-12/01-0.png",
            },
            {
              say: "Sometimes when I look at paintings",
              actor: "me",
            },
            {
              delayInMs: 2000,
            },
            {
              say: "I feel that could have been me",
              actor: "me",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "02",
            },
          ],
        },
        {
          title: "02",
          commands: [
            {
              "draw-bg": "/images/2021-11-12/03-0.png",
            },
            {
              actor: "me",
              say: "But I've learned that it's better to be grateful...",
            },
            {
              delayInMs: 2000,
            },
            {
              actor: "me",
              say: "...for what you have",
            },
          ],
        },
      ]}
    />
  );
}
