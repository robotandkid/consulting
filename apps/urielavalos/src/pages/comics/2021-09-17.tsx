import { awesomeRetroCutscenes } from "../../components/AwesomeRetroCutscenes/AwesomeRetroCutscenes";

const AwesomeRetroCutscenes = awesomeRetroCutscenes("They grow up so fast");

export function Comic_2021_09_17() {
  return (
    <AwesomeRetroCutscenes
      config={{
        background: {
          containerWidth: 256,
          containerHeight: 256,
        },
        dialog: {
          containerHeight: 50,
          containerWidth: 256,
          textScrollSoundPath: "/sounds/textscroll.wav",
        },
      }}
      scenes={[
        {
          title: "start",
          commands: [
            {
              "draw-bg": "/images/2021-09-17/01-0.png",
            },
            {
              say: "You guys make me lose my patience.",
              actor: "son",
            },
            {
              delayInMs: 2000,
            },
            {
              say: "I'm moving out when I turn 8.",
              actor: "son",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "02",
            },
          ],
        },
        {
          title: "02",
          commands: [
            {
              "draw-bg": "/images/2021-09-17/02-0.png",
            },
            {
              actor: "Dad",
              say: "OK, I'll hold you to that.",
            },
            {
              delayInMs: 2000,
            },
            {
              actor: "Dad",
              say: "Alexa, set a reminder in 3 years...",
            },
            {
              delayInMs: 2000,
            },
            {
              actor: "Dad",
              say: "My son is moving out.",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "03",
            },
          ],
        },
        {
          title: "03",
          commands: [
            {
              "draw-bg": "/images/2021-09-17/03-0.png",
            },
            {
              actor: "Alexa",
              say: "OK, I'll remind you in 3 years.",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "04",
            },
          ],
        },
        {
          title: "04",
          commands: [
            {
              "draw-bg": "/images/2021-09-17/04-0.png",
            },
            {
              actor: "son",
              say: "I was just kidding Daddy.",
            },
          ],
        },
      ]}
    />
  );
}
