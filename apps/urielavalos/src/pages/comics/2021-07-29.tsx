import { awesomeRetroCutscenes } from "../../components/AwesomeRetroCutscenes/AwesomeRetroCutscenes";

const AwesomeRetroCutscenes = awesomeRetroCutscenes(
  "Grandma is gonna punch me in the face"
);

export function Comic_2021_07_29() {
  return (
    <AwesomeRetroCutscenes
      config={{
        background: {
          containerWidth: 128,
          containerHeight: 128,
        },
        dialog: {
          containerHeight: 50,
          containerWidth: 128,
          textScrollSoundPath: "/sounds/textscroll.wav",
        },
      }}
      scenes={[
        {
          title: "start",
          commands: [
            {
              "draw-bg": "/images/2021-07-29/01-0.png",
            },
            {
              say: "Grandma, are you feeding Greg again?",
              actor: "me",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "02",
            },
          ],
        },
        {
          title: "02",
          commands: [
            {
              "draw-bg": "/images/2021-07-29/02-0.png",
            },
            {
              say: "Of course not sweetie.",
              actor: "Grandma",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "03",
            },
          ],
        },
        {
          title: "03",
          commands: [
            {
              "draw-bg": "/images/2021-07-29/03-0.png",
            },
            {
              say: "Grandma, what are you talking about?",
              actor: "me",
            },
            {
              delayInMs: 2000,
            },
            {
              say: "I see you feeding him right now.",
              actor: "me",
            },
            {
              delayInMs: 2000,
            },
            {
              jump: "04",
            },
          ],
        },
        {
          title: "04",
          commands: [
            {
              "draw-bg": "/images/2021-07-29/04-0.png",
            },
            {
              say: "Stop lying sweetie.",
              actor: "Grandma",
            },
          ],
        },
      ]}
    />
  );
}
