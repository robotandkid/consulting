import { awesomeRetroCutscenes } from "../../components/AwesomeRetroCutscenes/AwesomeRetroCutscenes";

const AwesomeRetroCutscenes = awesomeRetroCutscenes("Destructuring");

export function Art_2021_05_02() {
  return (
    <AwesomeRetroCutscenes
      config={{
        background: {
          containerWidth: 200,
          containerHeight: 200,
        },
        dialog: {
          containerHeight: 0,
          containerWidth: 0,
        },
      }}
      scenes={[
        {
          title: "start",
          commands: [
            {
              "draw-bg": "/images/2021-05-03/destructured.png",
            },
            {
              filter: "glitch",
            },
            {
              delayInMs: 30000,
            },
          ],
        },
      ]}
    />
  );
}
