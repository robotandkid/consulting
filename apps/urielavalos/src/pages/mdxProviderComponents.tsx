import Link from "next/link";
import { ReactNode } from "react";
import { SummerDuran1 } from "../../pages/art/summer-duran/SummerDuran/SummerDuran1";
import { SummerDuran2 } from "../../pages/art/summer-duran/SummerDuran/SummerDuran2";
import {
  SummerDuran3,
  SummerDuran4,
  SummerDuran5,
  SummerDuran6,
} from "../../pages/art/summer-duran/SummerDuran/SummerDuranRest";
import { AiBook2024_05_09 } from "../../pages/art/ai-book/cwm";
import { AiBook2024_05_02 } from "../../pages/art/ai-book/wwa";
import { Comic2023_12_26 } from "../../pages/art/big-mac-murder/index";
import { ChronoCanvas2024_04_02 } from "../../pages/art/chrono-canvas/04/02/index";
import { ChronoCanvas2024_04_03 } from "../../pages/art/chrono-canvas/04/03/index";
import { ChronoCanvas2024_04_05 } from "../../pages/art/chrono-canvas/04/05/index";
import { ChronoCanvas2024_04_06 } from "../../pages/art/chrono-canvas/04/06/index";
import { ChronoCanvas2024_04_11 } from "../../pages/art/chrono-canvas/04/11/index";
import { ChronoCanvas2024_04_17 } from "../../pages/art/chrono-canvas/04/17/index";
import { ChronoCanvas2024_04_18 } from "../../pages/art/chrono-canvas/04/18/index";
import { ChronoCanvas2024_04_20 } from "../../pages/art/chrono-canvas/04/20/index";
import { ChronoCanvas2024_04_22 } from "../../pages/art/chrono-canvas/04/22";
import { ChronoCanvas2024_04_23 } from "../../pages/art/chrono-canvas/04/23";
import { ChronoCanvas2024_04_24 } from "../../pages/art/chrono-canvas/04/24";
import { ChronoCanvas2024_04_28 } from "../../pages/art/chrono-canvas/04/28";
import { ChronoCanvas2024_04_29 } from "../../pages/art/chrono-canvas/04/29";
import { ChronoCanvas2024_04_30 } from "../../pages/art/chrono-canvas/04/30";
import { ChronoCanvas2024_05_01 } from "../../pages/art/chrono-canvas/05/01";
import { ChronoCanvas2024_05_02 } from "../../pages/art/chrono-canvas/05/02";
import { ChronoCanvas2024_05_04 } from "../../pages/art/chrono-canvas/05/04";
import { ChronoCanvas2024_05_06 } from "../../pages/art/chrono-canvas/05/06";
import { ChronoCanvas2024_05_07 } from "../../pages/art/chrono-canvas/05/07";
import { ChronoCanvas2024_05_09 } from "../../pages/art/chrono-canvas/05/09";
import { ChronoCanvas2024_05_10 } from "../../pages/art/chrono-canvas/05/10";
import { ChronoCanvas2024_05_12 } from "../../pages/art/chrono-canvas/05/12";
import { ChronoCanvas2024_05_15 } from "../../pages/art/chrono-canvas/05/15";
import { ChronoCanvas2024_05_16 } from "../../pages/art/chrono-canvas/05/16";
import { ChronoCanvas2024_05_17 } from "../../pages/art/chrono-canvas/05/17";
import { ChronoCanvas2024_05_20 } from "../../pages/art/chrono-canvas/05/20";
import { ChronoCanvas2024_05_21 } from "../../pages/art/chrono-canvas/05/21";
import { ChronoCanvas2024_05_23 } from "../../pages/art/chrono-canvas/05/23";
import { ChronoCanvas2024_06_10 } from "../../pages/art/chrono-canvas/06/10";
import { ChronoCanvas2024_06_12 } from "../../pages/art/chrono-canvas/06/12";
import { ChronoCanvas2024_06_14 } from "../../pages/art/chrono-canvas/06/14";
import { ChronoCanvasIntro } from "../../pages/art/chrono-canvas/intro/index";
import { EngineeringSimulator } from "../../pages/art/engineering-simulator/index";
import { LoveInMotion2024_05_14 } from "../../pages/art/love-in-motion/2024/05/12";
import { LoveInMotion2024_05_16 } from "../../pages/art/love-in-motion/2024/05/16";
import { LoveInMotion2024_05_17 } from "../../pages/art/love-in-motion/2024/05/17";
import { LoveInMotion2024_05_20 } from "../../pages/art/love-in-motion/2024/05/20";
import { LoveInMotion2024_06_06 } from "../../pages/art/love-in-motion/2024/06/06";
import { LoveInMotion2024_06_11 } from "../../pages/art/love-in-motion/2024/06/11";
import { LoveInMotion2024_06_13 } from "../../pages/art/love-in-motion/2024/06/13";
import { LoveInMotion2024_06_17 } from "../../pages/art/love-in-motion/2024/06/17";
import { Random2024_05_02 } from "../../pages/art/random/2024/05-02";
import { Random_2024_06_11 } from "../../pages/art/random/2024/06-11";
import { Random_2024_06_15 } from "../../pages/art/random/2024/06-15";
import {
  ReactMatterDemo,
  ReactMatterDemo2,
} from "../../pages/blog/ReactMatterDemo";
import { TearsInTheRain } from "../../pages/blog/SpriteMotionDemo/TearsInTheRain";
import { Comic_2022_02_06 } from "../../pages/blog/awesome-retro-content/2022-02-06";
import { Comic_2022_01_30 } from "../../pages/comics/2022-01-30/2022-01-30";
import Comic2023_08_16 from "../../pages/comics/2023-08-16/2023-08-16.dynamic";
import { Comic2023_08_20 } from "../../pages/comics/2023-08-20/index";
import { Comic2023_08_26 } from "../../pages/comics/2023-08-26/index";
import { Comic2023_09_10 } from "../../pages/comics/2023-09-10/index";
import { Comic2023_09_18 } from "../../pages/comics/2023-09-18/index";
import { Comic2023_09_23 } from "../../pages/comics/2023-09-23/index";
import { Comic2023_10_04 } from "../../pages/comics/2023-10-04/index";
import { Comic2023_11_04 } from "../../pages/comics/2023-11-04/index";
import { Comic2023_11_15 } from "../../pages/comics/2023-11-15/index";
import { Comic2023_11_25 } from "../../pages/comics/2023-11-25/index";
import { Comic2023_12_03 } from "../../pages/comics/2023-12-03/index";
import { Comic2023_12_17 } from "../../pages/comics/2023-12-17/index";
import { Comic2024_01_14 } from "../../pages/comics/2024/01-14/index";
import { Comic2024_01_26 } from "../../pages/comics/2024/01-26/index";
import { Comic2024_01_30 } from "../../pages/comics/2024/01-30/index";
import { Comic2024_02_13 } from "../../pages/comics/2024/02-13/index";
import { Comic2024_02_16 } from "../../pages/comics/2024/02-16/index";
import { Comic2024_02_22 } from "../../pages/comics/2024/02-22/index";
import { Comic2024_02_28 } from "../../pages/comics/2024/02-28/index";
import { Comic2024_03_04 } from "../../pages/comics/2024/03/04/index";
import { Comic2024_03_22 } from "../../pages/comics/2024/03/22/index";
import { Comic2024_03_26 } from "../../pages/comics/2024/03/26/index";
import { Comic2024_03_30 } from "../../pages/comics/2024/03/30/index";
import { Comic2024_04_15 } from "../../pages/comics/2024/04/15/index";
import { Comic2024_04_26 } from "../../pages/comics/2024/04/26";
import { Comic2024_06_07 } from "../../pages/comics/2024/06/07";
import { Comic2024_06_09 } from "../../pages/comics/2024/06/09";
import {
  MadeWithAwesome,
  MadeWithPixiJS,
} from "../../pages/comics/footer/MadeWith";
import { awesomeRetroCutscenes } from "../components/AwesomeRetroCutscenes/AwesomeRetroCutscenes";
import { PostH2, PostH3 } from "../components/Post/PostHeadings";
import {
  UrielAvalosGame,
  UrielAvalosGameFixFocus,
} from "../components/UrielAvalosGame/UrielAvalosGame";
import {
  ExternalLink,
  InternalLink,
} from "../styles/ExternalLink/ExternalLink";
import { Pre } from "../styles/codeBlocks";
import {
  Blockquote,
  Button,
  CenteredColumn,
  ListItem,
  PageContainer,
  Text,
  Title,
} from "../styles/styles";
import { Art_2021_05_02 } from "./art/2021-05-02";
import { Comic_2021_05_27 } from "./comics/2021-05-27";
import { Comic_2021_07_29 } from "./comics/2021-07-29";
import { Comic_2021_09_17 } from "./comics/2021-09-17";
import { Comic_2021_11_12 } from "./comics/2021-11-12";
import { Comic_2021_12_11 } from "./comics/2021-12-11";
import { ChronoCanvas2024_06_19 } from "../../pages/art/chrono-canvas/06/19";
import { Comic2024_06_19 } from "../../pages/comics/2024/06/19";
import { ChronoCanvas2024_06_21 } from "../../pages/art/chrono-canvas/06/21";
import { Random_2024_06_23 } from "../../pages/art/random/2024/06-23";
import { Comic2024_06_28 } from "../../pages/comics/2024/06/28";
import { AiBook2024_06_30 } from "../../pages/art/ai-book/captain-jack";
import { ChronoCanvas2024_06_30 } from "../../pages/art/chrono-canvas/06/30";
import { Comic2024_07_03 } from "../../pages/comics/2024/07/03";
import { Comic2024_07_04 } from "../../pages/comics/2024/07/04";
import { Comic2024_07_20 } from "../../pages/comics/2024/07/20";
import { SummerDuranRedux01 } from "../../pages/art/summer-duran/01";
import { Comic2024_07_30 } from "../../pages/comics/2024/07/30";
import { Comic2024_08_01 } from "../../pages/comics/2024/08/01";
import { Comic2024_08_05 } from "../../pages/comics/2024/08/05";
import { Comic2024_08_07 } from "../../pages/comics/2024/08/07";
import { Comic2024_08_11 } from "../../pages/comics/2024/08/11";
import { Comic2024_08_16 } from "../../pages/comics/2024/08/16";
import { Comic2024_08_24 } from "../../pages/comics/2024/08/24";
import { Comic2024_09_01 } from "../../pages/comics/2024/09/01";
import { ShirtClub001 } from "../../pages/art/shirt-club/001";
import { Random2024_10_03 } from "../../pages/art/random/2024/10-03";
import { ThePortalStoryFramework } from "../../pages/art/the-portal/game-framework";
import { ThePortalStoryDemo } from "../../pages/art/the-portal/demo";
import { Watermark001 } from "../../pages/art/watermark/001";

/** @deprecated For now --- we're just using YAML */
export const mdxProviderComponents = (title?: string) => ({
  AwesomeCutScenes: awesomeRetroCutscenes(title ?? "Loading..."),
});

export const Wrapper = (props: { children?: ReactNode }) => {
  return (
    <CenteredColumn as="main">
      <PageContainer>{props.children}</PageContainer>
    </CenteredColumn>
  );
};

export const components = {
  wrapper: Wrapper,
  h1: (props: any) => <Title {...props} as="h1" />,
  h2: PostH2,
  h3: PostH3,
  p: Text,
  a: ExternalLink,
  li: ListItem,
  pre: Pre,
  Link: (props: any) => (
    <Link {...props} passHref>
      <InternalLink>{props.children}</InternalLink>
    </Link>
  ),
  blockquote: Blockquote,
  button: Button,
  AwesomeCutScenes: awesomeRetroCutscenes("Loading..."),
  Comic2021x05x27: Comic_2021_05_27,
  Comic2021x07x29: Comic_2021_07_29,
  Comic2021x09x17: Comic_2021_09_17,
  Comic2021x11x12: Comic_2021_11_12,
  Comic2021x12x11: Comic_2021_12_11,
  Comic2022x01x30: Comic_2022_01_30,
  Comic2022x02x06: Comic_2022_02_06,
  MadeWithAwesome,
  MadeWithPixiJS,
  Art2021x05x02: Art_2021_05_02,
  SummerDuran1,
  SummerDuran2,
  SummerDuran3,
  SummerDuran4,
  SummerDuran5,
  SummerDuran6,
  SummerDuranRedux01,
  ReactMatterDemo,
  ReactMatterDemo2,
  UrielAvalosGameFixFocus,
  UrielAvalosGame,
  TearsInTheRain,
  ChronoCanvasIntro,
  Randomx2024x05x02: Random2024_05_02,
  Randomx2024x06x11: Random_2024_06_11,
  Randomx2024x06x15: Random_2024_06_15,
  Randomx2024x06x23: Random_2024_06_23,
  Randomx2024x10x03: Random2024_10_03,
  ShirtClub001,
  ThePortalStoryFramework,
  ThePortalStoryDemo,
  AiBook2024x05x02: AiBook2024_05_02,
  AiBook2024x05x09: AiBook2024_05_09,
  AiBook2024x06x30: AiBook2024_06_30,
  ChronoCanvasx2024x04x02: ChronoCanvas2024_04_02,
  ChronoCanvasx2024x04x03: ChronoCanvas2024_04_03,
  ChronoCanvasx2024x04x05: ChronoCanvas2024_04_05,
  ChronoCanvasx2024x04x06: ChronoCanvas2024_04_06,
  ChronoCanvasx2024x04x11: ChronoCanvas2024_04_11,
  ChronoCanvasx2024x04x17: ChronoCanvas2024_04_17,
  ChronoCanvasx2024x04x18: ChronoCanvas2024_04_18,
  ChronoCanvasx2024x04x20: ChronoCanvas2024_04_20,
  ChronoCanvasx2024x04x22: ChronoCanvas2024_04_22,
  ChronoCanvasx2024x04x23: ChronoCanvas2024_04_23,
  ChronoCanvasx2024x04x24: ChronoCanvas2024_04_24,
  ChronoCanvasx2024x04x28: ChronoCanvas2024_04_28,
  ChronoCanvasx2024x04x29: ChronoCanvas2024_04_29,
  ChronoCanvasx2024x04x30: ChronoCanvas2024_04_30,
  ChronoCanvasx2024x05x01: ChronoCanvas2024_05_01,
  ChronoCanvasx2024x05x02: ChronoCanvas2024_05_02,
  ChronoCanvasx2024x05x04: ChronoCanvas2024_05_04,
  ChronoCanvasx2024x05x06: ChronoCanvas2024_05_06,
  ChronoCanvasx2024x05x07: ChronoCanvas2024_05_07,
  ChronoCanvasx2024x05x09: ChronoCanvas2024_05_09,
  ChronoCanvasx2024x05x10: ChronoCanvas2024_05_10,
  ChronoCanvasx2024x05x12: ChronoCanvas2024_05_12,
  ChronoCanvasx2024x05x15: ChronoCanvas2024_05_15,
  ChronoCanvasx2024x05x16: ChronoCanvas2024_05_16,
  ChronoCanvasx2024x05x17: ChronoCanvas2024_05_17,
  ChronoCanvasx2024x05x20: ChronoCanvas2024_05_20,
  ChronoCanvasx2024x05x23: ChronoCanvas2024_05_23,
  ChronoCanvasx2024x05x21: ChronoCanvas2024_05_21,
  ChronoCanvasx2024x06x10: ChronoCanvas2024_06_10,
  ChronoCanvasx2024x06x12: ChronoCanvas2024_06_12,
  ChronoCanvasx2024x06x14: ChronoCanvas2024_06_14,
  ChronoCanvasx2024x06x19: ChronoCanvas2024_06_19,
  ChronoCanvasx2024x06x21: ChronoCanvas2024_06_21,
  ChronoCanvasx2024x06x30: ChronoCanvas2024_06_30,
  LoveInMotionx2024x05x14: LoveInMotion2024_05_14,
  LoveInMotionx2024x05x16: LoveInMotion2024_05_16,
  LoveInMotionx2024x05x17: LoveInMotion2024_05_17,
  LoveInMotionx2024x05x20: LoveInMotion2024_05_20,
  LoveInMotionx2024x06x06: LoveInMotion2024_06_06,
  LoveInMotionx2024x06x11: LoveInMotion2024_06_11,
  LoveInMotionx2024x06x13: LoveInMotion2024_06_13,
  LoveInMotionx2024x06x17: LoveInMotion2024_06_17,
  Comic2023x08x16: Comic2023_08_16,
  Comic2023x08x20: Comic2023_08_20,
  Comic2023x08x26: Comic2023_08_26,
  Comic2023x09x10: Comic2023_09_10,
  Comic2023x09x18: Comic2023_09_18,
  Comic2023x09x23: Comic2023_09_23,
  Comic2023x10x04: Comic2023_10_04,
  Comic2023x11x04: Comic2023_11_04,
  Comic2023x11x15: Comic2023_11_15,
  Comic2023x11x25: Comic2023_11_25,
  Comic2023x12x03: Comic2023_12_03,
  Comic2023x12x17: Comic2023_12_17,
  Comic2023x12x26: Comic2023_12_26,
  Comic2024x01x14: Comic2024_01_14,
  Comic2024x01x26: Comic2024_01_26,
  Comic2024x01x30: Comic2024_01_30,
  Comic2024x02x13: Comic2024_02_13,
  Comic2024x02x16: Comic2024_02_16,
  Comic2024x02x22: Comic2024_02_22,
  Comic2024x02x28: Comic2024_02_28,
  Comic2024x03x04: Comic2024_03_04,
  Comic2024x03x22: Comic2024_03_22,
  Comic2024x03x26: Comic2024_03_26,
  Comic2024x03x30: Comic2024_03_30,
  Comic2024x04x15: Comic2024_04_15,
  Comic2024x04x26: Comic2024_04_26,
  Comic2024x06x07: Comic2024_06_07,
  Comic2024x06x09: Comic2024_06_09,
  Comic2024x06x19: Comic2024_06_19,
  Comic2024x06x28: Comic2024_06_28,
  Comic2024x07x03: Comic2024_07_03,
  Comic2024x07x04: Comic2024_07_04,
  Comic2024x07x20: Comic2024_07_20,
  Comic2024x07x30: Comic2024_07_30,
  Comic2024x08x01: Comic2024_08_01,
  Comic2024x08x05: Comic2024_08_05,
  Comic2024x08x07: Comic2024_08_07,
  Comic2024x08x11: Comic2024_08_11,
  Comic2024x08x16: Comic2024_08_16,
  Comic2024x08x24: Comic2024_08_24,
  Comic2024x09x01: Comic2024_09_01,
  EngineeringSimulator,
  Watermark001,
};
