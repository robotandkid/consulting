import { OscilloscopeOpts, PixiAppOpts } from "@robotandkid/pixi-components";
import dynamic from "next/dynamic";
import React from "react";
import { ComicPlaceholder } from "../styles/styles";
import { theme } from "../styles/theme";

export const DynamicPixiApp = dynamic<PixiAppOpts>(
  () =>
    import("@robotandkid/pixi-components").then(
      (module) => module.PixiApp
    ) as any,
  {
    ssr: false,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);

export const DynamicOscilloscope = dynamic<OscilloscopeOpts>(
  () =>
    import("@robotandkid/pixi-components").then(
      (module) => module.Oscilloscope
    ) as any,
  {
    ssr: false,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);
