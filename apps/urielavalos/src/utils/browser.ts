import { useEffect, useState } from "react";

export const isSafari =
  typeof navigator !== "undefined" &&
  !!navigator.vendor &&
  navigator.vendor.indexOf("Apple") > -1 &&
  !!navigator.userAgent &&
  navigator.userAgent.indexOf("CriOS") == -1 &&
  navigator.userAgent.indexOf("FxiOS") == -1;

const viewportWidth = () =>
  typeof window === "undefined"
    ? 0
    : Math.max(
        document.documentElement.clientWidth || 0,
        window.innerWidth || 0
      );

const viewportHeight = () =>
  typeof window === "undefined"
    ? 0
    : Math.max(
        document.documentElement.clientHeight || 0,
        window.innerHeight || 0
      );

export function useViewportDims() {
  const [dims, setDims] = useState({
    viewportWidth: viewportWidth(),
    viewportHeight: viewportHeight(),
  });

  useEffect(() => {
    function onResize() {
      setDims({
        viewportWidth: viewportWidth(),
        viewportHeight: viewportHeight(),
      });
    }

    window.addEventListener("resize", onResize);

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  return dims;
}
