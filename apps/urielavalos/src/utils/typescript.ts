export type Undefined<T> = { [K in keyof T]?: undefined };
export type Unpromisify<T> = T extends Promise<infer R> ? R : T;
