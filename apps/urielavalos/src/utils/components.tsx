import { ResourceSound } from "@robotandkid/sprite-motion";
import { m } from "framer-motion";
import { useEffect, useState } from "react";
import styled, { keyframes } from "styled-components";
import { useGuardedCallback } from "./hooks";

const blinkCaret = (caretColor: string) => keyframes`
/* The typewriter cursor effect */
  from, to { border-color: transparent; }
  50% { border-color: ${caretColor}; }
`;

/** https://css-tricks.com/snippets/css/typewriter-effect/ */
const TypewriterContainer = styled(m.div)<{ $textColor: string }>`
  position: absolute;
  font-family: monospace;
  font-weight: bold;
  font-size: 1rem;
  color: ${(p) => p.$textColor};
  width: 100%;
  letter-spacing: 0.15em;
  height: 75px;
`;

const TypeWriterText = styled(m.span)``;

const Cursor = styled(m.span)<{ $cursorColor: string }>`
  padding: 0;
  width: 1char;
  height: 1char;
  margin-bottom: -0.25rem;
  border-right: 0.5rem solid orange;
  animation: ${(p) => blinkCaret(p.$cursorColor)} 0.75s step-end infinite;
`;

export const Typewriter = (props: {
  children: string | string[];
  onDone?: () => void;
  speed?: number;
  sound: ResourceSound | undefined;
  className?: string;
  textColor?: string;
  cursorColor?: string;
  style?: React.HTMLAttributes<HTMLDivElement>["style"];
}) => {
  const {
    children,
    onDone,
    speed = 50,
    sound,
    style,
    textColor = "#008300",
    cursorColor = "orange",
  } = props;

  const [reveal, _setReveal] = useState(1);
  const fullText = Array.isArray(children) ? children.join("") : children;
  const setReveal = useGuardedCallback(_setReveal);

  useEffect(() => {
    sound?.resource.play({ loop: true });

    const interval = setInterval(() => {
      setReveal((c) => {
        if (c + 1 === fullText.length) {
          clearInterval(interval);
          sound?.resource.stop();
          onDone?.();
          return c + 1;
        }
        return c + 1;
      });
    }, speed);

    return () => {
      clearInterval(interval);
    };
  }, [fullText.length, onDone, setReveal, sound?.resource, speed]);

  const text = fullText.substring(0, reveal);

  return (
    <TypewriterContainer
      className={props.className}
      $textColor={textColor}
      style={style}
    >
      <TypeWriterText>{text}</TypeWriterText>
      <Cursor $cursorColor={cursorColor} />
    </TypewriterContainer>
  );
};
