import { RefObject, useEffect, useRef } from "react";

export function globalWindow(): typeof window | undefined {
  return typeof window !== "undefined" ? window : ({} as typeof window);
}

/**
 * @deprecated This is technically a hack because I'm moving away from styled
 *   components (don't want to return a prop)
 */
export const useDownscaleToFit = <Element extends HTMLElement>(
  ref: RefObject<Element>
) => {
  const anotherRef = useRef(ref);
  anotherRef.current = ref;

  useEffect(() => {
    const obs = new ResizeObserver(() => {
      if (!anotherRef.current.current) return;
      const viewportWidth = window.document.documentElement.clientWidth;

      const dims = anotherRef.current.current.getClientRects();
      const width = dims[0]?.width ?? 0;
      // Calculate the scale ratio to fit the viewport
      // extra for padding
      const scale = (0.9 * viewportWidth) / width;

      if (scale > 1) return;

      anotherRef.current.current.style.transform = `scale(${scale})`;
      anotherRef.current.current.style.transformOrigin = `0 50%`;
    });

    obs.observe(window.document.documentElement);

    return () => {
      obs.unobserve(window.document.documentElement);
    };
  }, []);

  return null;
};

/**
 * @deprecated This is technically a hack because I'm moving away from styled
 *   components (don't want to return a prop)
 */
export const useScaleToFit = <Element extends HTMLElement>(
  ref: RefObject<Element>
) => {
  const anotherRef = useRef(ref);
  anotherRef.current = ref;

  useEffect(() => {
    const obs = new ResizeObserver(() => {
      if (!anotherRef.current.current) return;
      const parent = anotherRef.current.current.parentElement;
      if (!parent) return;
      const parentWidth = parent.clientWidth;
      const parentHeight = parent.clientHeight;

      const dims = anotherRef.current.current.getClientRects();
      const width = dims[0]?.width ?? 0;
      const height = dims[0]?.height ?? 0;
      // Calculate the scale ratio to fit the parent
      // extra for padding
      const scaleX = (0.9 * parentWidth) / width;
      const scaleY = (0.9 * parentHeight) / height;
      const scale = Math.min(scaleX, scaleY);

      anotherRef.current.current.style.transform = `translate(-100px,0px) scale(${scale})`;
      anotherRef.current.current.style.transformOrigin = `0 0`;
    });

    obs.observe(window.document.documentElement);

    return () => {
      obs.unobserve(window.document.documentElement);
    };
  }, []);

  return null;
};
