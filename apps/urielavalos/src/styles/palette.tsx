export const palette = {
  gbColor: ["#081820", "#346856", "#88c070", "#e0f8d0"] as const,
  macPaint: ["#051b2c", "#8bc8fe"] as const,
  pixelInk: ["#3e232c", "#edf6d6"] as const,
  bitBee: ["#292b30", "#cfab4a"] as const,
  neutralGreen: ["#004c3d", "#ffeaf9"] as const,
  chasingLight: ["#000000", "#ffff02"] as const,
  ongBit: ["#151d24", "#ed8463"] as const,
  flowersAsbestos: ["#c62b69", "#edf4ff"] as const,
  peachyKeen: ["#242234", "#facab8"] as const,
  funkyJam: ["#920244", "#fec28c"] as const,
} as const;
