import { CenteredColumn } from "@robotandkid/components";
import styled, { css, keyframes } from "styled-components";

import React, { forwardRef, ReactNode, useRef, useState } from "react";
import { useDownscaleToFit, useScaleToFit } from "../utils/window";
import { media } from "./media";
import { theme } from "./theme";

export { CenteredColumn };

export const Row = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const ComicPlaceholder = styled.div`
  background: #ff0099;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-family: ${(p) => p.theme.title.font};
  font-size: 3rem;
  letter-spacing: -0.25rem;
`;

export const PageContainer = styled.div`
  width: 100%;

  ${media("tablet")(css`
    width: 37rem;
    overflow: visible;
  `)}
`;

export const SectionContainer = styled.div`
  ${(p) => p.theme.entities.section.text()}

  width: 100%;

  ${media("tablet")(css`
    width: 37rem;
  `)}

  margin-top: 5rem;
  padding-top: 0;
`;

export const sharedTitleStyle = css`
  font-family: ${(p) => p.theme.title.font};
  background: transparent;
  letter-spacing: -0.12rem;
  line-height: 1;
`;

export const Title = styled.div`
  ${sharedTitleStyle}
  font-weight: normal;
  font-size: 8em;
  padding: 0;
  margin: 0;
  margin-top: 1em;
  margin-bottom: 0.5em;
  background: transparent;

  ${media("tablet")(css`
    font-size: 10em;
    margin-top: 0;
  `)}
`;

export const text = css`
  font-family: sans-serif;
  font-size: 1.5rem;
  margin-top: 1em;
  margin-bottom: 1em;
  text-align: justify;

  &:first-child {
    margin-top: 0em;
  }

  &:last-child {
    margin-bottom: 0em;
  }
`;

export const Blockquote = styled.blockquote`
  ${text}
  margin-top: 1em;
  margin-bottom: 1em;
  margin-left: 0;
  margin-right: 0;
  padding: 1rem;
  text-align: left;
  background: rgb(213, 212, 212);
`;

export const Text = styled.p`
  font-family: sans-serif;
  font-size: 1.5rem;
  margin-top: 1em;
  margin-bottom: 1em;
  text-align: justify;

  &:first-child {
    margin-top: 0em;
  }

  &:last-child {
    margin-bottom: 0em;
  }
`;

const _ListItem = styled(Text)`
  margin-top: 0.5em;
  margin-bottom: 0.5em;
`;

export const ListItem = (props: any) => <_ListItem {...props} as="li" />;

export const Button = styled.button`
  ${text}
  background: ${theme.color.blue1};
  border-color: ${theme.color.blue1};
  padding: 0.5em;
  color: ${theme.color.white};
  &:hover,
  &:active {
    opacity: 0.5;
  }
`;

const blink = keyframes`
  50% {
    opacity: 0;
  }
`;

export const GateButton = styled.button<{
  $textColor?: string;
  $buttonColor?: string;
}>`
  ${text}
  background: ${(p) => p.$textColor ?? "gray"};
  border-color: none;
  padding: 0.5em;
  color: ${(p) => p.$buttonColor ?? "black"};
  &:hover,
  &:active {
    opacity: 0.5;
  }
  text-align: left;
  text-transform: uppercase;
  font-family: mrpixel, monospace;
  font-size: 2rem;
  animation: ${blink} 1s step-start infinite;
  outline: none;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
`;

export const GateButtonContainer = styled.section<{ $background?: string }>`
  border: 2pt solid gray;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0;
  box-sizing: border-box;
  ${(p) =>
    p.$background &&
    css`
      background: ${p.$background};
    `}
`;

const PressStartContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  justify-content: space-around;
`;

export const PressStart = () => (
  <PressStartContainer>
    <div style={{ lineHeight: 1 }}>➤</div>
    <div style={{ marginLeft: "0.5rem" }}>Press start</div>
  </PressStartContainer>
);

const Gate = forwardRef<
  HTMLElement,
  {
    children: ReactNode;
    button?: React.ReactElement<Pick<HTMLDivElement, "click">>;
    container?: React.ReactElement;
    width: number;
    height: number;
  }
>((props, ref) => {
  const [show, setShow] = useState(false);
  const {
    button: gateButton = (
      <GateButton>
        <PressStart />
      </GateButton>
    ),
    container: gateContainer = <GateButtonContainer />,
    width,
    height,
  } = props;
  const button = React.cloneElement(gateButton, {
    onClick: () => setShow(true),
  });

  const gate = React.cloneElement(gateContainer, {
    ref,
    style: { width, height },
    children: (
      <>
        {show && props.children}
        {!show && button}
      </>
    ),
  });

  return <>{gate}</>;
});

export const GateBehindButton = (props: Parameters<typeof Gate>[0]) => {
  const ref = useRef<HTMLElement>(null);
  useDownscaleToFit(ref);

  return <Gate ref={ref} {...props} />;
};

export const GateBehindButtonFull = (props: Parameters<typeof Gate>[0]) => {
  const ref = useRef<HTMLElement>(null);
  useScaleToFit(ref);

  return <Gate ref={ref} {...props} />;
};
