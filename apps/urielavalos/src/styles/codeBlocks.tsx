import { ReactNode } from "react";
import styled, { css } from "styled-components";
import { media } from "./media";
import { theme } from "./theme";

const codeTheme = {
  fonts: {
    mono: "monospace",
  },
  fontSizes: {
    1: "1.4rem",
    2: "1.5rem",
  },
  lineHeight: {
    1: 1.5,
  },
  color: {
    black: "rgba(19, 19, 21, 1)",
    white: "rgba(255, 255, 255, 1)",
    gray: "rgba(128, 128, 128, 1)",
    blue: "rgba(3, 136, 252, 1)",
    red: "rgba(249, 16, 74, 1)",
    yellow: "rgba(255, 221, 0, 1)",
    pink: "rgba(232, 141, 163, 1)",
    turq: "rgba(0, 245, 196, 1)",
    orange: "rgba(255, 135, 31, 1)",
    neonGreen: "#39F21D",
    neonYellow: "#ffea19",
  },
};

interface Style {
  text: string;
  background: string;
}

interface PreOpts {
  background: string;
  text: string;
  function: {
    definitions: Style;
    calls: Style;
  };
  string: Style;
  /**
   * keywords: const, new, etc
   */
  keywords: Style;
  /**
   * Also numbers
   */
  typescript: Style;
  comment: string;
  removed: string;
  added: string;
}

const colorBurnBg = css<{ background: string }>`
  background-color: ${(p) => p.background};
  background-image: radial-gradient(
    circle,
    rgba(255, 255, 255, 0.5) 0%,
    rgba(255, 0, 0, 0.5) 100%
  );
  background-repeat: repeat;
  background-blend-mode: color-burn;
`;

const _Pre = styled.pre<PreOpts>`
  padding: 1em;
  overflow: auto;
  font-family: ${codeTheme.fonts.mono};
  font-size: ${codeTheme.fontSizes[2]};
  line-height: ${codeTheme.lineHeight[1]};
  white-space: pre;
  position: relative;

  ${media("tablet")(css`
    left: max(calc(-20em + 48%), calc(-45vw + 48%));
    width: 40em;
    max-width: 90vw;
  `)}

  color: ${(p) => p.text};
  font-weight: bold;

  ${colorBurnBg}

  & > code {
    display: block;
  }

  .token.parameter {
    color: ${(p) => p.text};
  }

  /* function calls */
  .token.tag,
  .token.selector,
  .token.selector .class,
  .token.function :not(.property-access) {
    color: ${(p) => p.function.calls.text};
    background-color: ${(p) => p.function.calls.background};
    font-weight: normal;
    font-style: italic;
  }

  .token.attr-value,
  .token.class,
  .token.string,
  .token.unit,
  .token.color {
    color: ${(p) => p.string.text};
    background-color: ${(p) => p.string.background};
    font-style: normal;
    font-weight: normal;
  }

  /* function, new, return, const, etc */
  .token.attr-name,
  .token.keyword,
  .token.rule,
  .token.pseudo-class,
  .token.important {
    color: ${(p) => p.keywords.text};
    background: ${(p) => p.keywords.background};
    font-weight: bold;
    font-style: normal;
  }

  .token.operator {
    color: ${(p) => p.text};
  }

  .token.punctuation,
  .token.module,
  .token.property {
    color: ${(p) => p.text};
    font-weight: bold;
  }

  .token.number,
  .token.builtin {
    color: ${(p) => p.typescript.text};
    background-color: ${(p) => p.typescript.background} !important;
    font-style: normal;
    font-weight: normal;
    background-color: inherit;
    text-decoration: underline;
  }

  /* JSX Component functions  */
  .token.maybe-class-name,
  .token.function :not(.method) > .token.maybe-class-name {
    color: ${(p) => p.function.definitions.text};
    background-color: ${(p) => p.function.definitions.background};
    font-weight: normal;
    font-style: normal;
    width: 100%;
    height: 100%;
  }

  .token.class-name,
  .token.constant,
  .token.class-name > .token.constant {
    text-decoration: underline;
    font-style: normal;
    background-color: inherit;
  }

  .token.function.property-access {
    text-decoration: underline;
    font-style: italic;
  }

  .token.property-access > .token.maybe-class-name {
    color: ${(p) => p.text};
    background-color: inherit;
    font-weight: bold;
    font-style: normal;
  }

  .token.comment {
    color: ${(p) => p.comment};
  }

  .token.atapply .token:not(.rule):not(.important) {
    color: inherit;
  }

  .language-shell .token:not(.comment) {
    color: inherit;
  }

  .language-css .token.function {
    color: inherit;
  }

  .token.deleted:not(.prefix),
  .token.inserted:not(.prefix) {
  }

  .token.deleted:not(.prefix) {
    color: ${(p) => p.removed};
  }

  .token.inserted:not(.prefix) {
    color: ${(p) => p.added};
  }

  .token.deleted.prefix,
  .token.inserted.prefix {
    user-select: none;
  }
`;

export function Pre(props: { children?: ReactNode }) {
  return (
    <_Pre
      background={theme.color.blue1}
      text={codeTheme.color.black}
      /* JSX function definitions */
      function={{
        definitions: {
          text: theme.color.black,
          background: codeTheme.color.neonYellow,
        },
        calls: {
          text: theme.color.black,
          background: theme.color.green1,
        },
      }}
      string={{
        text: codeTheme.color.black,
        background: theme.color.gray4,
      }}
      keywords={{
        text: theme.color.orange1,
        background: theme.color.secondary,
      }}
      typescript={{
        text: codeTheme.color.white,
        background: theme.color.secondary,
      }}
      comment={codeTheme.color.black}
      removed={codeTheme.color.black}
      added={codeTheme.color.black}
    >
      {props.children}
    </_Pre>
  );
}
