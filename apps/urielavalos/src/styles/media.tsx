import { css } from "styled-components";

const size = {
  mobileL: "600px",
  tablet: "768px",
  desktop: "1920px",
};

type MediaSize = keyof typeof size;

export const media =
  (mediaSize: MediaSize) => (args: ReturnType<typeof css>) => {
    return css`
      @media only screen and (min-width: ${size[mediaSize]}) {
        ${args}
      }
    `;
  };
