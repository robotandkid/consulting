import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'mrpixel';
    src: url('/fonts/mrpixel.otf') format('opentype');
    font-display: 'swap';
  }

  @font-face {
    font-family: 'comic';
    src: url('/fonts/hey-comic.otf') format('opentype');
    font-display: 'swap';
  }

  body {
    font-size: 62.5%;
    margin: 0;
    padding: 0;
  }
`;
