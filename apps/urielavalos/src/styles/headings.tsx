import styled, { css } from "styled-components";
import { media } from "./media";

export const H2 = styled.h2`
  font-size: 5rem;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;
  margin-top: 0.5em;

  ${media("tablet")(css`
    font-size: 7rem;
  `)}
`;
