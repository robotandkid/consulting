import { normalizedAnchor } from "@robotandkid/components";
import { m } from "framer-motion";
import { forwardRef, useState } from "react";
import styled, { css, keyframes } from "styled-components";

const blendAnimation = keyframes`
  from {
    background-color:  green;
  }
  to {
   background-color: white;
  }
`;

const blend = css`
  color: white;
  padding: 0.1em;
  border: 0.01em solid transparent;
  border-radius: 0.25em;
  background-color: ${(p) => p.theme.color.gray1};
  background-image: linear-gradient(
    90deg,
    rgba(253, 33, 33, 1) 0%,
    rgba(240, 253, 33, 1) 25%,
    rgba(74, 227, 80, 1) 50%,
    rgba(0, 50, 255, 1) 75%,
    rgba(197, 25, 172, 1) 100%
  );
  background-blend-mode: difference, overlay;
  animation-duration: 1s;
  animation-name: ${blendAnimation};
  animation-iteration-count: infinite;
  animation-direction: alternate;
  animation-timing-function: ease-in-out;
`;

const StyledExternalLink = styled(m.a)<{ $showRainbow: boolean }>`
  ${normalizedAnchor}
  font-style: italic;
  color: ${(props) => props.theme.color.anchor};
  border-bottom: 1px dashed ${(props) => props.theme.color.anchor};

  &:visited {
    color: ${(props) => props.theme.color.anchor};
  }

  transition: background 1s, padding 1s, color 1s;

  ${(p) => p.$showRainbow && blend}
`;

export function ExternalLink(props: Parameters<typeof StyledExternalLink>[0]) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { ref, target, children, ...args } = props;
  const [showRainbow, setShowRainbow] = useState(false);

  /**
   * For some reason, `whileHover` doesn't work. It will leave the
   * background purple no how `initial` is set.
   */
  return (
    <StyledExternalLink
      tabIndex={0}
      {...args}
      target="_blank"
      $showRainbow={showRainbow}
      onHoverStart={() => setShowRainbow(true)}
      onHoverEnd={() => setShowRainbow(false)}
    >
      {children}
    </StyledExternalLink>
  );
}

export const InternalLink = forwardRef(
  (props: Parameters<typeof StyledExternalLink>[0], ref) => {
    const { children, ...args } = props;
    const [showRainbow, setShowRainbow] = useState(false);

    /**
     * For some reason, `whileHover` doesn't work. It will leave the
     * background purple no how `initial` is set.
     */
    return (
      <StyledExternalLink
        {...args}
        tabIndex={0}
        ref={ref}
        $showRainbow={showRainbow}
        onHoverStart={() => setShowRainbow(true)}
        onHoverEnd={() => setShowRainbow(false)}
      >
        {children}
      </StyledExternalLink>
    );
  }
);
