import { css, ThemeProvider } from "styled-components";

const colors = {
  black1: "black",
  fuchsia1: "#FF00B9",
  teal1: "#00FEFF",
  yellow1: "#FFD70D",
  anchor: "#6F685D",
  disabled: "#91AA9D",
  hover: "#D1DBBD",
  gray1: "#6F685D",
  gray2: "#6E6460",
  gray3: "#8B8885",
  gray4: "#c6c6c6",
  black: "black",
  white: "white",
  blue1: "#2179fd",
  orange1: "#ffb779",
  orange2: "#E8742B",
  green1: "#39F21D",
  green2: "green",
  red1: "#ff0000",
  mario1: "#049DD9",
  mario2: "#268C6F",
  mario3: "#30BF45",
  mario4: "#F2AC57",
  mario5: "#F24130",
};

const legacyConcepts = {
  main: {
    font: {
      bundled: "mrpixel",
      header: "sans-serif",
    },
    /**
     * @deprecated give them names like above
     */
    color: {
      1: colors.black,
      oneOnDarkBg: colors.white,
      2: colors.fuchsia1,
      3: colors.teal1,
      4: colors.green2,
      6: colors.green1,
    },
    bgColor: {
      1: colors.white,
      2: colors.fuchsia1,
      3: colors.teal1,
      4: colors.blue1,
      5: colors.green2,
      6: colors.red1,
    },
    gray: {
      1: colors.gray1,
      2: colors.gray2,
      3: colors.gray3,
      4: colors.gray4,
    },
  },
};

const concepts = {
  title: {
    font: "mrpixel",
    letterColor1: "#049DD9",
    letterColor2: "#268C6F",
    letterColor3: "#30BF45",
    letterColor4: "#F2AC57",
    letterColor5: "#F24130",
  },
  smallHeader: "Helvetica, Arial",
  main: {
    color: {
      inset: colors.black,
      insetBg: colors.white,
    },
  },
  secondary: {},
};

const entities = {
  landing: {
    title: () => css`
      font-family: ${concepts.title.font};
      background: transparent;
    `,
    letterColor: (index: number) => {
      const mod = ((index % 5) + 1) as 1 | 2 | 3 | 4 | 5;
      return concepts.title[`letterColor${mod}`];
    },
  },
  section: {
    h2: () => css`
      font-family: ${legacyConcepts.main.font.bundled};
      font-weight: normal;
      color: ${legacyConcepts.main.color[1]};
      background-color: ${legacyConcepts.main.bgColor[5]};
    `,
    text: () => css`
      background-color: ${legacyConcepts.main.bgColor[6]};
    `,
    highlight: () => css`
      color: ${legacyConcepts.main.color[6]};
    `,
    anchor: () => css`
      color: ${legacyConcepts.main.color[3]};
      border-bottom-color: ${legacyConcepts.main.color[3]};

      &:visited {
        color: ${legacyConcepts.main.color[3]};
      }
    `,
  },
  post: {
    h2: () => css`
      font-family: ${legacyConcepts.main.font.header};
      color: ${legacyConcepts.main.color["2"]};
    `,
    h3: () => css`
      font-family: ${legacyConcepts.main.font.header};
      color: ${legacyConcepts.main.color["3"]};
      background: ${legacyConcepts.main.gray["3"]};
    `,
  },
  menu: {
    color: legacyConcepts.main.color.oneOnDarkBg,
    font: legacyConcepts.main.font.bundled,
  },
  oscilloscope: {
    background: legacyConcepts.main.gray["4"],
    stroke: legacyConcepts.main.gray["3"],
  },
  gallery: {
    comicItemColor: legacyConcepts.main.bgColor["2"],
    blogItemColor: legacyConcepts.main.bgColor["3"],
    artItemColor: legacyConcepts.main.bgColor["4"],
  },
  comicPortrait: {
    portrait: () => css`
      background-color: transparent;
    `,
    inset: () => css`
      font-family: ${concepts.smallHeader};
      color: ${concepts.main.color.inset};
      background-color: ${concepts.main.color.insetBg};
    `,
  },
};

export const theme = {
  header: {
    font: "sans-serif",
  },
  title: {
    font: "mrpixel",
  },
  color: {
    primary: "black",
    background: "white",
    secondary: "#FF00B9",
    tertiary: "#00FEFF",
    quartile: "#E8742B",
    quintile: "#FFD70D",
    anchor: "#6F685D",
    disabled: "#91AA9D",
    hover: "#D1DBBD",
    gray1: "#6F685D",
    gray2: "#6E6460",
    gray3: "#8B8885",
    gray4: "#c6c6c6",
    black: "black",
    white: "white",
    blue1: "#2179fd",
    orange1: "#ffb779",
    green1: "#39F21D",
  },
  entities,
};

export interface AppThemeProps {
  theme: typeof theme;
}

export function AppThemeProvider(props: { children?: React.ReactNode }) {
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
}
