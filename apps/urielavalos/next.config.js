const rehypePrism = require("@mapbox/rehype-prism");
const path = require("path");

const extension = /\.mdx?$/;

const withMDX = require("@next/mdx")({
  extension,

  options: {
    rehypePlugins: [rehypePrism],
  },
});

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

/** @type {import("next").NextConfig} */
const config = {
  async headers() {
    return [
      {
        source: "/fonts/mrpixel.otf",
        headers: [
          {
            key: "Cache-Control",
            value: "public, immutable, max-age=31536000",
          },
        ],
      },
      {
        source: "/fonts/hey-comic.otf",
        headers: [
          {
            key: "Cache-Control",
            value: "public, immutable, max-age=31536000",
          },
        ],
      },
    ];
  },
  pageExtensions: ["page.tsx", "mdx"],
  webpack(config) {
    const mdxRule = config.module.rules.find(
      (rule) => `${rule.test}` === `${extension}`
    );
    if (mdxRule) {
      mdxRule.use.push(path.join(__dirname, "./mdx-fm-loader"));
    }

    return config;
  },
  images: {
    loader: "imgix",
    path: "",
  },
};

module.exports = withBundleAnalyzer(withMDX(config));
