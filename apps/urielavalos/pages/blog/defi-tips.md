---
title: "Uriel's Crypto Tips for Beginners"
type: blog
draft: true
created: "2021-11-17"
---

It all started when I bought a small amount of bitcoin a few months ago. And I
kept at it---every month I'd buy the same amount. Then roughly six months later
I realized I had not only made more than the interest on my entire emergency
fund but that I was up 30%.

If you have played the stock market before, you know that these returns are not
only great---they are freakin' amazing! And so that started my journey into
crypto.

Here's what I've learned so far:

## Uriel's Crypto Survival Rules:

- Start off by investing only what you don't mind losing. Crypto is highly
  volatile. Also, because it is so new, the road is filled with landmines (and
  I'm not necessarily talking about scams).
- Be safe bro. Minimum safety is to use one browser for buying/selling. And a
  different browser to surf the web.
- At least for now, stay away from [dapps](https://decrypt.co/resources/dapps)
  that live on the the layer 1 Etherium chain. That chain is freakin' expensive.
  (Like
  [gas fees](https://coinmarketcap.com/alexandria/article/what-are-gas-fees)
  north of $80 just to send $10.) I understand this rule may use words that make
  no sense now.

## A Note About Money-Making Strategies

In legacy finance, the benchmark is the S&P 500 (also called _the market_),
which averages a 10% yearly return. That means if you can find a strategy that
beats "the market", you are considered a financial genius. Seriously! On
average, when the pros try to pick winning stocks, they do _worse_ than the
S&P 500. _Remember that now_

In crypto, the equivalent "S&P 500" is Bitcoin and Etherium. We could possibly
include other coins, but this is the minimum benchmark. Currently, the 1 yr
return for [Bitcoin is 202.3%](https://www.coingecko.com/en/coins/bitcoin); the
1 yr return for
[Etherium is 671.5%](https://www.coingecko.com/en/coins/ethereum).

These amazing returns are the benchmark, that is, any money making strategy is
considered a "success" when it beats these returns. The benchmark is even more
amazing when you consider other blockchains and their primary coins. These are
the 1 yr returns for various blockchains:

- Solvana/SOL - 11,280.2%
- Binance/BNB - 1,960.5%
- Polygon/MATIC - 11,151.4%
- Fantom/FTM - 10,136.2%

However, I'm not sure (yet) about the long term sustainability of some of these
chains. The reason is because it's not clear (to me) whether these chains
complement or compete with each other.

In any case, as a noob, you can't go wrong with the simple, dumb strategy of
just buying Bitcoin/Etherium (and possibly other "blue chip" coins) and holding
(that is, _HODLing_)

### Defi 2.0 and Beyond

I also got into
[staking and yield farming](https://www.coinbureau.com/guides/yield-farming/https://beincrypto.com/learn/yield-farming-vs-staking/)
but that is for another post!

In theory, you could, for example, stake a _stable coin_ (or a coin pegged
against the US dollar), and earn interest north of 10%. In other words,
basically open the equivalent of a savings account with an interest rate better
than the legacy stock market! (However, I haven't tried this yet since stable
coins are infamous for _not_ being stable, and well see Rule #1.)

A better strategy is to use yield farming or staking to try to beat the crypto
market. There are dapps that give APYs as high as 90,000%! Yea, you read that
right. The catch is that the interest-bearing coin may be highly volatile. So in
practice, the yield may be smaller. How much smaller? Some quick math:

- You buy a 1 coin that's worth $1 USD.
- Say the APY is 3000%.
- So after 1 year, you have about 1 x 30, or 30 coins.
- However, the coin drops in price to $0.50 USD.
- But your investment is now worth 30 x $0.50, or $15.
- So the effective APY is about 1500%, which is amazing!

So in other words, with astronomically high APYs, it is possible to beat the
market, even if the underlying coin drops in price.

I realize all this is pretty abstract, and you're dying to get started. But
there are some things you need to understand before you jump in:

1. Gas fees are the cost to do anything on a blockchain, including staking and
   farming.
2. Every blockchain has its own coin to pay for gas fees. Ex: on the Etherium
   Layer 1, it's $ETH; on the Binance chain, it's $BNB.
3. Always have at least $10 worth of the gas-fee coin, so that your money
   doesn't get stuck in a dApp or blockchain. (The exception is the Etherium
   Layer 1 chain, where you need several hundred dollars. See Rule #3.)
4. You'll need to figure out a way to buy this coin, which may not be simple.
   Ex: in certain parts of the US, it is currently not possible to buy $BNB in a
   centralized exchange. So that may entail using a swap service, like
   [changelly.com](https://changelly.com/). And that's it's own side quest,
   since it is totally possible to send money to the wrong blockchain (ex:
   there's a version of $MATIC that lives on Etherium, and not on Polygon, where
   you probably intended.)
5. Yield farming uses _pairs_ of coins. On the dApp (ex:
   [pancake swap](https://pancakeswap.finance/farms)), you swap the gas-fee coin
   for these pairs.
6. Staking uses a a single coin.
7. The staking/farming dApps usually have good docs. Make sure to read them.
8. With staking/farming, the yield is all about the underlying coins. Ensure
   they're not obscure and/or super volatile.
