import {
  defaultTextStyle,
  PixiApp,
  PixiAppContext,
  TypewriterText,
  TypewriterTextOpts,
  useAddPixiFilter2,
  useLoadPixiSprite,
} from "@robotandkid/pixi-components";
import { Bodies, Composite, Engine, Runner } from "matter-js";
import { PixelateFilter } from "pixi-filters";
import {
  Children,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { makeReadOnlyContext } from "../../../src/utils/context";

const [SceneNextProvider, SceneNextContext] =
  makeReadOnlyContext<() => void>("SceneNext");

function useNext() {
  const next = useContext(SceneNextContext);
  return next;
}

/**
 * - Renders its children one at a time
 * - It will advance to the next child as it calls the SceneNextContext
 * - Children will be rendered on top of each other, so if a child needs to disappear,
 *   it can return null before calling onNext
 */
function Scene(props: { children?: ReactNode; name: string }) {
  const children = Children.toArray(props.children);
  const length = children.length;
  const [current, setCurrent] = useState(0);
  const parentNext = useNext();

  const next = useCallback(() => {
    setCurrent((c) => {
      if (c < length - 1) return c + 1;
      if (typeof parentNext === "function") parentNext();
      return c;
    });
  }, [length, parentNext]);

  return (
    <SceneNextProvider value={next}>
      {children.map((child, index) => {
        return index <= current ? child : null;
      })}
    </SceneNextProvider>
  );
}

const [PhysicsProvider, PhysicsContext] = makeReadOnlyContext<{
  engine: ReturnType<typeof Engine.create>;
  runner: ReturnType<typeof Runner.create>;
}>("Physics");

interface SceneProps {
  name: string;
}

function SceneProvider(props: {
  children: React.ReactElement<SceneProps>[] | React.ReactElement<SceneProps>;
}) {
  const physicsContext = useMemo(
    () => ({
      engine: Engine.create(),
      runner: Runner.create(),
    }),
    []
  );

  useEffect(
    function setupPhysics() {
      Runner.run(physicsContext.runner, physicsContext.engine);
    },
    [physicsContext]
  );

  return (
    <PhysicsProvider value={physicsContext}>
      <Scene name="root">{props.children}</Scene>
    </PhysicsProvider>
  );
}

const width = 150;
const height = 200;

function Background(props: { path: string; width: number; height: number }) {
  const { width, height } = props;
  const next = useNext();

  const sprite = useLoadPixiSprite({
    path: props.path,
    width,
    height,
    parentType: "app",
  });

  useEffect(() => {
    if (sprite) {
      next?.();
    }
  }, [next, sprite]);

  return null;
}

function Delay(props: { timeout: number }) {
  const { timeout } = props;
  const next = useNext();

  useEffect(() => {
    const _timeout = setTimeout(() => next?.(), timeout);

    return () => {
      clearTimeout(_timeout);
    };
  }, [next, timeout]);

  return null;
}

function PhysicsSprite(props: { path: string; x: number; y: number }) {
  const { path, x, y } = props;
  const next = useNext();
  const app = useContext(PixiAppContext);

  const spriteData = useLoadPixiSprite({
    path: path,
    width: app?.renderer.width ?? 0,
    height: app?.renderer.height ?? 0,
    parentType: "app",
  });

  const sprite = spriteData?.sprite;
  const physics = useContext(PhysicsContext);

  useEffect(
    function createPhysicsBody() {
      if (!sprite) return;

      const spriteBody = Bodies.rectangle(x, y, sprite.width, sprite.height);

      Composite.add(physics.engine.world, spriteBody);

      return () => {
        Composite.remove(physics.engine.world, spriteBody);
      };
    },
    [physics.engine.world, sprite, x, y]
  );

  useEffect(() => {
    if (sprite) {
      next?.();
    }
  }, [sprite, next]);

  useEffect(
    function syncPosition() {
      function animate() {
        for (const dot of physics.engine.world.bodies) {
          if (dot.isStatic || !sprite) continue;

          const { x, y } = dot.position;

          sprite.position.x = x;
          sprite.position.y = y;

          break;
        }
      }

      if (!app || !sprite) return;

      app.ticker.add(animate);

      return () => {
        app.ticker.remove(animate);
      };
    },
    [app, physics.engine.world.bodies, sprite]
  );

  return null;
}

function Say(
  props: {
    actor: string;
    hideActor?: boolean;
    children: string | string[];
  } & Pick<TypewriterTextOpts, "style">
) {
  const { children, style, hideActor } = props;
  const next = useNext();
  const app = useContext(PixiAppContext);
  const lines = Array.isArray(children) ? children : [children];
  const [lineNumber, setLineNumber] = useState(0);
  const line = lines[lineNumber] as string | undefined;
  const lineWithActor = `${props.actor}: ${line}`;
  const done = lineNumber === lines.length - 1;
  const text = !line ? undefined : hideActor ? line : lineWithActor;

  const onLineDone = useCallback(() => {
    setLineNumber((l) => l + 1);
    if (done) next?.();
  }, [done, next]);

  return (
    <TypewriterText
      lineId={text ?? ""}
      style={style}
      soundPath="/sounds/textscroll.wav"
      typingSpeedInMs={20}
      onLineDone={onLineDone}
      onLineDoneDelayMs={500}
      parent={app}
    >
      {text ?? ""}
    </TypewriterText>
  );
}

type Filter = Parameters<typeof useAddPixiFilter2>[0]["filters"];

function End(props: { transition: Filter }) {
  const app = useContext(PixiAppContext);
  const next = useNext();

  const added = useAddPixiFilter2({
    comp: app,
    filters: props.transition,
  });

  useEffect(() => {
    if (added) next?.();
  }, [added, next]);

  return null;
}

function Test() {
  const dialogStyle = {
    style: {
      ...defaultTextStyle(),
      fontSize: "1em",
      height: height / 2,
      top: height / 2,
    },
  };

  return (
    <SceneProvider>
      <Scene name="scene1">
        <Background
          path="/comics/2022-01-30/bg.png"
          width={width}
          height={height}
        />
        <Delay timeout={1000} />
        <PhysicsSprite
          path="/comics/2022-01-30/jump.png"
          x={width / 2}
          y={height / 2}
        />
        <Delay timeout={1000} />
        <Say actor="Grandma" hideActor {...dialogStyle}>
          mmmm.... this is good!
        </Say>
        <Delay timeout={1000} />
        <Say actor="Me" {...dialogStyle}>
          Wow, Grandma! I didn&apos;t know you had fake teeth!
        </Say>
        <Delay timeout={1000} />
        <End
          transition={{
            filter: new PixelateFilter(0),
            animation(time, filter) {
              (filter as PixelateFilter).size = time;
            },
          }}
        />
      </Scene>
    </SceneProvider>
  );
}

export function Comic_2022_02_06() {
  return (
    <PixiApp
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/comics/2022-01-30/jump.png",
        },
        {
          type: "image",
          path: "/comics/2022-01-30/bg.png",
        },
        {
          type: "sound",
          path: "/comics/2022-01-30/jump.mp3",
        },
      ]}
    >
      <Test />
    </PixiApp>
  );
}
