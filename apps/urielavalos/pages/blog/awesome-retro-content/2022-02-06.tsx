import dynamic from "next/dynamic";
import { ComicPlaceholder } from "../../../src/styles/styles";
import { theme } from "../../../src/styles/theme";
import { Comic_2022_02_06 as Type } from "./2022-02-06.part2";

export const Comic_2022_02_06 = dynamic<Parameters<typeof Type>>(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports not working
    import("./2022-02-06.part2").then(
      (module) => module.Comic_2022_02_06
    ) as any,
  {
    ssr: false,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);
