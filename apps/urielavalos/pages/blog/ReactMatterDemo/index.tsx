import dynamic from "next/dynamic";

export const ReactMatterDemo = dynamic(
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore dynamic imports don't work
  () => import("./ReactMatterDemo").then((mod) => mod.ReactMatterDemo) as any,
  {
    loading() {
      return <div>Loading...</div>;
    },
  }
);

export const ReactMatterDemo2 = dynamic(
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore dynamic imports don't work
  () => import("./ReactMatterDemo2").then((mod) => mod.ReactMatterDemo2) as any,
  {
    loading() {
      return <div>Loading...</div>;
    },
  }
);
