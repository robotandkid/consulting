import { World, MotionDiv } from "@robotandkid/sprite-motion";
import { useEffect, useState } from "react";

const random = (scale: number) =>
  Math.random() * scale * (Math.random() > 0.5 ? -1 : 1);

const rightEye = { startCx: 125, startCy: 125 };
const leftEye = { startCx: 55, startCy: 125 };

export const TearsInTheRainInner = () => {
  return (
    <World>
      <img src="/images/2023-07-29/tears-in-the-rain.png" />
      {Array.from({ length: 100 }).map((_, index) => {
        const eye = index % 2 ? rightEye : leftEye;
        return (
          <MotionDiv
            key={Math.random()}
            height={5}
            width={5}
            {...eye}
            vx={random(3)}
            vy={random(3)}
            style={{
              borderRadius: "5px",
              borderColor: "blue",
              background: "blue",
            }}
          />
        );
      })}
    </World>
  );
};

export const TearsInTheRain = () => {
  const [key, setKey] = useState(Math.random());

  useEffect(() => {
    const tears = () => setKey(Math.random());
    const unsubscribe = setInterval(tears, 800);
    return () => {
      clearInterval(unsubscribe);
    };
  });

  return <TearsInTheRainInner key={key} />;
};
