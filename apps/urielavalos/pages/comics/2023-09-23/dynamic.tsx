import {
  ActionBlock,
  MotionDiv,
  Panel,
  StaticImg,
  StoryBoard,
  useDownloadsCtx,
  useEngineContext,
  World,
} from "@robotandkid/sprite-motion";
import { useCallback, useEffect, useRef, useState } from "react";
import { GateBehindButton } from "../../../src/styles/styles";
import { sleep } from "../../../src/utils/promises";

const resources = {
  love: {
    type: "image",
    url: "/comics/2023-09-23/inlove.png",
  },
  loveEyes: {
    type: "image",
    url: "/comics/2023-09-23/inlove-eyes.png",
  },
  fart: {
    type: "sound",
    url: "/comics/2023-09-23/fart.mp3",
  },
  song: {
    type: "sound",
    url: "/comics/2023-09-23/love.mp3",
  },
} as const;

const width = 256;
const height = 312;

const useGuardedCallback = <FuncParams = void,>(
  cb: undefined | ((...args: FuncParams[]) => void)
): typeof cb extends undefined
  ? undefined
  : (...args: FuncParams[]) => void => {
  const mounted = useRef(false);

  useEffect(function checkMount() {
    mounted.current = true;
    return () => {
      mounted.current = false;
    };
  }, []);

  const cbRef = useRef(cb);
  cbRef.current = cb;

  const callback = useCallback((...args: FuncParams[]) => {
    if (mounted.current) cbRef.current?.(...args);
  }, []);

  return (cb === undefined ? undefined : callback) as any;
};

const random = (scale: number) =>
  Math.random() * scale * (Math.random() > 0.5 ? -1 : 1);

const Zs = () => {
  const [, _tick] = useState(0);
  const setTick = useGuardedCallback(_tick);

  useEffect(() => {
    const interval = setInterval(() => {
      setTick((c) => c + 1);
    }, 1500);
    return () => {
      clearInterval(interval);
    };
  }, [setTick]);

  return (
    <>
      {Array.from({ length: 5 }).map(() => {
        return (
          <MotionDiv
            key={Math.random()}
            height={5}
            width={5}
            startCx={100}
            startCy={100}
            vx={random(2)}
            vy={random(2)}
            style={{
              fontFamily: "mrpixel, monospace",
              fontSize: "1.5rem",
            }}
          >
            z
          </MotionDiv>
        );
      })}
    </>
  );
};

const Fart = () => {
  return (
    <>
      {Array.from({ length: 2 }).map((_, index) => {
        return (
          <MotionDiv
            key={index}
            height={5}
            width={5}
            startCx={130}
            startCy={120}
            vx={random(2)}
            vy={random(2)}
            style={{
              fontFamily: "mrpixel, monospace",
              fontSize: "2rem",
            }}
          >
            &#9729;
          </MotionDiv>
        );
      })}
    </>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const engine = useEngineContext();
  const [openEyes, setOpenEyes] = useState(false);
  const [fart, setFart] = useState(false);

  return (
    <>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            downloads.song.resource.play();
            if (engine) {
              engine.gravity.x = 0;
              engine.gravity.y = -0.5;
            }
            await sleep(10000);
            downloads.song.resource.stop();
            downloads.fart.resource.play();
            setFart(true);
            await sleep(1000);
            setOpenEyes(true);
            await sleep(5000);
            opts.paginate?.("next-wipe");
          }}
        </ActionBlock>
        <StaticImg
          aria-label="couple sleeping on couch"
          width={256}
          height={256}
          src={downloads.love}
          style={{
            bottom: 0,
            position: "absolute",
          }}
        />
        {openEyes && (
          <StaticImg
            aria-label="couple sleeping on couch and spouse awoke"
            width={256}
            height={256}
            src={downloads.loveEyes}
            style={{
              bottom: 0,
              position: "absolute",
            }}
          />
        )}
        <Zs />
        {fart && <Fart />}
      </Panel>
    </>
  );
};

const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{ overflow: "hidden", padding: "10px", boxSizing: "border-box" }}
      width={width}
      height={height}
    >
      <StoryBoard disableManualNavigation>
        <ComponentInner />
      </StoryBoard>
    </World>
  );
};

const Component = () => (
  <GateBehindButton width={width} height={height}>
    <ComponentWorld />
  </GateBehindButton>
);

export default Component;
