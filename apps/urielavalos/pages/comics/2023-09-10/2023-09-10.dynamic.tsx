import {
  StaticImg,
  MotionImg,
  Downloads,
  StaticDiv,
  useResources,
  World,
} from "@robotandkid/sprite-motion";
import { Engine, Sleeping } from "matter-js";
import { useRef, useState } from "react";
import styled from "styled-components";
import { Blockquote, GateBehindButton } from "../../../src/styles/styles";
import { sleep } from "../../../src/utils/promises";
import { useDownscaleToFit } from "../../../src/utils/window";

const metaResources = {
  me: {
    type: "image",
    url: "/comics/2023-09-10/me.png",
  },
  hands: {
    type: "image",
    url: "/comics/2023-09-10/hands.png",
  },
  swoosh: {
    type: "sound",
    url: "/comics/2023-08-26/swoosh.mp3",
  },
  chuckle: {
    type: "sound",
    url: "/comics/2023-09-10/chuckle.mp3",
  },
} as const;

const StyledWorld = styled(World)`
  overflow: hidden;
`;

const StyledScene = styled.div`
  width: 100%;
  height: 100%;
`;

const width = 256;
const height = 256;

const ComponentInner = (props: {
  onDone: () => void;
  resources: Downloads<typeof metaResources>;
}) => {
  const engine = useRef<Engine>();

  const ref = useRef<HTMLDivElement>(null);
  useDownscaleToFit(ref);

  return (
    <StyledWorld
      ref={ref}
      width={width}
      height={height}
      onInit={(thisEngine) => {
        engine.current = thisEngine;
        engine.current.gravity.x = 0;
        engine.current.gravity.y = -0.5;
        props.resources.downloads?.swoosh.resource.play();
      }}
    >
      <StyledScene>
        <StaticDiv
          aria-label="invisible top wall"
          width={1000}
          height={5}
          cx={128}
          cy={5}
          onCollisionStart={() => {
            const go = async () => {
              // disable all bodies
              for (const body of engine.current?.world.bodies ?? []) {
                Sleeping.set(body, true);
              }
              await sleep(3000);
              props.onDone();
            };

            go();
          }}
        />
        <MotionImg
          aria-label="me"
          src={props.resources.downloads?.me}
          width={256}
          height={256}
          startLeft={0}
          startTop={128}
          style={{
            zIndex: -1,
          }}
        />
        <StaticImg
          aria-label="hands"
          src={props.resources.downloads?.hands}
          width={256}
          height={256}
        />
      </StyledScene>
    </StyledWorld>
  );
};

type UseResources = typeof useResources<typeof metaResources>;

const GateBehindResource = (props: {
  downloaded: ReturnType<UseResources>;
}) => {
  const { downloaded } = props;
  const [key, setKey] = useState(0);
  const canShow = downloaded !== "loading";

  return (
    <>
      {downloaded === "loading" && <div>Loading...</div>}
      {canShow && (
        <>
          <ComponentInner
            key={key}
            onDone={() => setKey((c) => c + 1)}
            resources={downloaded}
          />
          <Blockquote>
            Been spending a lot of time reading manga. I&apos;m loving how it
            recaptures the spirit of comics when I was kid.
          </Blockquote>
        </>
      )}
    </>
  );
};

const Component = () => {
  const downloaded = useResources(metaResources);

  return (
    <GateBehindButton width={width * 2} height={height * 1.75}>
      <GateBehindResource downloaded={downloaded} />
    </GateBehindButton>
  );
};

export default Component;
