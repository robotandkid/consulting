import {
  ActionBlock,
  Panel,
  StaticImg,
  StoryBoard,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import styled, { keyframes } from "styled-components";
import {
  GateBehindButton as GateBehindButton2,
  GateBehindButtonFull,
} from "../../../src/styles/styles";
import { Typewriter as TypewriterOriginal } from "../../../src/utils/components";
import { sleep } from "../../../src/utils/promises";

const resources = {
  Scene01: {
    type: "image",
    url: "/comics/2023-12-03/adult-01-bw.png",
  },
  Scene02: {
    type: "image",
    url: "/comics/2023-12-03/adult-02-bw.png",
  },
  Scene03: {
    type: "image",
    url: "/comics/2023-12-03/adult-03-bw.png",
  },
  Scene04: {
    type: "image",
    url: "/comics/2023-12-03/adult-04-bw.png",
  },
  Scene04Head: {
    type: "image",
    url: "/comics/2023-12-03/adult-head.png",
  },
  music: {
    type: "sound",
    url: "/comics/2023-12-03/adult-music.mp3",
  },
} as const;

const width = 256;
const height = 256;

const oneBit = ["#222323", "#f0f6f0"];

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Typewriter = styled(TypewriterOriginal)`
  font-family: mrpixel, monospace;
  font-size: 1rem;
  letter-spacing: 0rem;
`;
const Screen = () => (
  <>
    <div
      style={{
        background: oneBit[1],
        position: "absolute",
        top: 170,
        width,
        height: 45,
      }}
    />
  </>
);

const bounce = keyframes`
  0%, 100% {
    transform: translateY(0);
  }
  10%, 30%, 50%, 70%, 90% {
    transform: translateY(-10%);
  }
  20%, 50%, 80% {
    transform: translateY(5%);
  }
`;

const BounceHead = styled(StaticImg)`
  animation: ${bounce} 1s infinite;
  animation-delay: 1s;
`;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const year = `${new Date().getFullYear()}`;

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            downloads.music.resource.play();
            await sleep(5000);
            if (opts.isBlockCancelled) return;
            opts.paginate("next-fade");
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.Scene01}
          style={{ position: "absolute", top: -35 }}
        />
        <Screen />
        <Typewriter
          sound={undefined}
          textColor={oneBit[0]}
          cursorColor={oneBit[1]}
          style={{ position: "absolute", top: 180, left: 10 }}
        >
          It&apos;s Friday night. Your pizza is ready.
        </Typewriter>
      </Panel>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            await sleep(5000);
            if (opts.isBlockCancelled) return;
            opts.paginate("next-fade");
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.Scene02}
          style={{ position: "absolute", top: -50 }}
        />
        <Screen />
        <Typewriter
          sound={undefined}
          textColor={oneBit[0]}
          cursorColor={oneBit[1]}
          style={{ position: "absolute", top: 180, left: 20 }}
        >
          Your favorite movie is on.
        </Typewriter>
      </Panel>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            await sleep(5000);
            if (opts.isBlockCancelled) return;
            opts.paginate("next-fade");
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.Scene03}
          style={{ position: "absolute", top: -50 }}
        />
        <Screen />
        <Typewriter
          sound={undefined}
          textColor={oneBit[0]}
          cursorColor={oneBit[1]}
          style={{ position: "absolute", top: 180, left: 0 }}
        >
          No school tomorrow and you have permission to stay up late.
        </Typewriter>
      </Panel>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            await sleep(6_000);
            if (opts.isBlockCancelled) return;
            opts.paginate("next-fade");
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.Scene04}
          style={{ position: "absolute", top: 0 }}
        />
        <BounceHead
          src={downloads.Scene04Head}
          style={{ position: "absolute", top: 12, left: 110 }}
        />
        <Screen />
        <Typewriter
          sound={undefined}
          textColor={oneBit[0]}
          cursorColor={oneBit[1]}
          style={{ position: "absolute", top: 180, left: 10 }}
        >
          You&apos;re an adult and the year is {year}.
        </Typewriter>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        overflow: "hidden",
        padding: "10px",
        boxSizing: "border-box",
        justifyContent: "flex-start",
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

const Component = () => (
  <GateBehindButton2 width={width} height={height}>
    <ComponentWorld />
  </GateBehindButton2>
);

export const ComponentFull = () => (
  <GateBehindButtonFull width={width} height={height}>
    <ComponentWorld />
  </GateBehindButtonFull>
);

export default Component;
