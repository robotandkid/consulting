import Link from "next/link";
import React from "react";
import {
  ExternalLink,
  InternalLink,
} from "../../../src/styles/ExternalLink/ExternalLink";

import { Text } from "../../../src/styles/styles";

export function MadeWithAwesome() {
  return (
    <Text>
      Made with{" "}
      <Link href="/blog/awesome-retro-video-game-cutscenes">
        <InternalLink>Awesome Retro Video Game Cutscenes</InternalLink>
      </Link>
      .
    </Text>
  );
}

export function MadeWithPixiJS() {
  return (
    <Text>
      Made with{" "}
      <ExternalLink href="https://brm.io/matter-js/">Matter.js</ExternalLink>{" "}
      and <ExternalLink href="https://www.pixijs.com/">PixiJS</ExternalLink>.
    </Text>
  );
}

/*
      <SectionExternalLink href="https://www.pixijs.com/">
            PixiJS
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://mdxjs.com/">
            MDX
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://www.framer.com/motion/">
            Framer Motion
          </SectionExternalLink>
          ,{" "}
          <SectionExternalLink href="https://brm.io/matter-js/">
            Matter.js
          </SectionExternalLink>
          */
