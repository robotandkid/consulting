import {
  ExplodingImage,
  matterBodyByLabel,
  MotionDiv,
  useEngineContext,
} from "@robotandkid/sprite-motion";
import {
  ActionBlock,
  Panel,
  StaticImg,
  StoryBoard,
  useDownloadsCtx,
  usePagination,
  World,
  StaticDiv,
} from "@robotandkid/sprite-motion";
import { m } from "framer-motion";
import { Body, Engine, Sleeping } from "matter-js";
import React, { HTMLAttributes, useReducer } from "react";
import { useRef } from "react";
import styled, { keyframes } from "styled-components";
import {
  GateBehindButton as GateBehindButton2,
  GateBehindButtonFull,
} from "../../../src/styles/styles";
import { Typewriter } from "../../../src/utils/components";
import { sleep } from "../../../src/utils/promises";

const resources = {
  chair: {
    type: "image",
    url: "/comics/2023-10-04/yoga-chair.png",
  },
  body: {
    type: "image",
    url: "/comics/2023-10-04/yoga-body.png",
  },
  head: {
    type: "image",
    url: "/comics/2023-10-04/yoga-head.png",
  },
  arm: {
    type: "image",
    url: "/comics/2023-10-04/yoga-arm.png",
  },
  cane: {
    type: "image",
    url: "/comics/2023-10-04/yoga-cane.png",
  },
  halo: {
    type: "image",
    url: "/comics/2023-10-04/yoga-halo.png",
  },
  type: {
    type: "sound",
    url: "/sounds/textscroll.wav",
  },
  grumpy: {
    type: "sound",
    url: "/comics/2023-10-04/grumpy.mp3",
  },
  twinkle: {
    type: "sound",
    url: "/comics/2023-10-04/twinkle.mp3",
  },
  chuckle: {
    type: "sound",
    url: "/comics/2023-10-04/chuckle.mp3",
  },
} as const;

const width = 312;
const height = 312;

const useDisableDrag = () => {
  const callbacks: Pick<HTMLAttributes<Element>, "onDragStart"> = {
    onDragStart: (e) => {
      e.preventDefault();
    },
  };
  return callbacks;
};

const NeighborBody = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <StaticImg
        aria-label="a wheelchair"
        src={downloads.chair}
        style={{
          top: 55,
          left: 0,
          position: "absolute",
        }}
      />
      <StaticImg
        {...useDisableDrag()}
        aria-label="neighbor's body sitting on a wheelchair"
        src={downloads.body}
        style={{
          top: 55,
          left: 50,
          position: "absolute",
        }}
      />
    </>
  );
};

const neighborHeadLabel = "neighbor's head";

const Neighbor = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <NeighborBody />
      <StaticImg
        aria-label={neighborHeadLabel}
        src={downloads.head}
        style={{
          top: 10,
          left: 75,
          position: "absolute",
        }}
      />
      <StaticImg
        aria-label="a cane"
        src={downloads.cane}
        style={{
          top: 65,
          left: 220,
          position: "absolute",
        }}
      />
      <StaticImg
        aria-label="an arm"
        src={downloads.arm}
        style={{
          top: 87,
          left: 172,
          position: "absolute",
        }}
      />
    </>
  );
};

const armLabel = "an arm";
const armBarrierLabel = "arm barrier";

const SwingyArm = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      {/* <Joint
        startTop={87}
        startLeft={168}
        hingeX="5%"
        hingeY="50%"
        width={76}
        height={19}
      > */}
      <StaticImg
        aria-label={armLabel}
        src={downloads.arm}
        style={{ position: "absolute", top: 87, left: 168 }}
      />
      {/* </Joint>
      <StaticDiv
        aria-label={armBarrierLabel}
        cx={160}
        cy={150}
        width={200}
        height={5}
      /> */}
    </>
  );
};

const disappearing = keyframes`
  0% { opacity: 1; }
  100% { opacity: 0; }
`;

const DisappearingImg = styled(StaticImg)`
  animation: ${disappearing} 2s forwards;
  position: absolute;
`;

const appearing = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`;

const AppearingImg = styled(StaticImg)`
  animation: ${appearing} 2s forwards;
  position: absolute;
`;

const NeighborWell = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <NeighborBody />
      <StaticImg
        aria-label={neighborHeadLabel}
        src={downloads.head}
        style={{
          top: 10,
          left: 75,
          position: "absolute",
        }}
      />
      <SwingyArm />
      <DisappearingImg
        aria-label="a cane"
        src={downloads.cane}
        style={{
          top: 65,
          left: 220,
          position: "absolute",
        }}
      />
    </>
  );
};

const NeighborAtPeace = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <NeighborBody />
      <BouncyHead
        aria-label={neighborHeadLabel}
        src={downloads.head}
        style={{
          top: 10,
          left: 75,
          position: "absolute",
        }}
      />
      <SwingyArm />
      <AppearingImg
        src={downloads.halo}
        style={{
          position: "absolute",
          top: 0,
          left: 60,
        }}
      />
    </>
  );
};

const NeighborNoHead = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <NeighborBody />
      <StaticImg
        aria-label={armLabel}
        src={downloads.arm}
        style={{ top: 87, left: 168, position: "absolute" }}
      />
    </>
  );
};

/**
 * These don't get removed before the next panel is visible so it makes it act
 * glitchy.
 *
 * TODO This is might a bug in the lifecycle, or at least there should be a
 * built-in helper
 */
const stopEngine = (engine: ReturnType<typeof useEngineContext>) => {
  engine?.world.bodies.forEach((b) => {
    Sleeping.set(b, true);
  });

  engine?.world.composites.forEach((c) => {
    c.bodies.forEach((b) => {
      Sleeping.set(b, true);
    });
  });
};

const bounce = keyframes`
  0%, 20%, 50%, 80%, 100% {
    transform: translateY(0);
  }
  40% {
    transform: translateY(-40%);
  }
  60% {
    transform: translateY(-20%);
  }
`;

const DownArrowInner = styled(m.div)`
  animation: ${bounce} 2s infinite;
  font-size: 4rem;
`;

const DownArrow = (props: {
  style: React.HTMLAttributes<HTMLDivElement>["style"];
}) => <DownArrowInner style={props.style}>⇣</DownArrowInner>;

const bouncyHead = keyframes`
  0%, 20%, 40%, 60%, 80%, 100% {
    transform: translateY(0);
  }
  10%, 30%, 50%, 70%, 90%  {
    transform: translateY(-10%);
  }
`;

const BouncyHead = styled(StaticImg)`
  animation: ${bouncyHead} 2s infinite;
`;

const Walls = () => {
  return (
    <>
      <StaticDiv
        aria-label="top wall"
        cx={width / 2}
        cy={-5}
        width={width}
        height={5}
      />
      <StaticDiv
        aria-label="bottom wall"
        cx={width / 2}
        cy={height}
        width={width}
        height={5}
        style={{
          background: "red",
          color: "red",
        }}
      />
      <StaticDiv
        aria-label="left wall"
        cx={0}
        cy={height / 2}
        width={5}
        height={height}
      />
      <StaticDiv
        aria-label="right wall"
        cx={width}
        cy={height / 2}
        width={5}
        height={height}
      />
    </>
  );
};

const disableBodyByLabel = (engine: Engine | undefined, label: string) => {
  const body = matterBodyByLabel(engine, label);
  if (body) Sleeping.set(body, true);
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const enableBodyByLabel = (engine: Engine | undefined, label: string) => {
  const body = matterBodyByLabel(engine, label);
  if (body) Sleeping.set(body, false);
};

type GameState = "game-init" | "game-prompt" | "game-mode" | "explode-head";

interface GameAction {
  type: GameState;
}

const gameTriggerLabel = "game-prompt";

interface Vector {
  x: number;
  y: number;
}

const pixelRatio = () => 1 / (window.devicePixelRatio || 1);

const toVector = (
  v:
    | {
        clientX: number;
        clientY: number;
      }
    | undefined
) => ({ x: v?.clientX ?? 0, y: v?.clientY ?? 0 });

const isMobile = () =>
  typeof window !== "undefined" && "ontouchstart" in window;

const useDrag = (opts?: {
  scale?: number;
  onDragStart?: (startPos: Vector) => void;
  onDrag?: (velocity: Vector, curPos: Vector, startPos: Vector) => void;
}) => {
  const { scale = 1, onDrag, onDragStart } = opts ?? {};
  const start = useRef<Vector | undefined>(undefined);

  const callbacks: Pick<
    HTMLAttributes<Element>,
    | "onDragStart"
    | "onMouseDown"
    | "onMouseMove"
    | "onTouchStart"
    | "onTouchMove"
  > = {
    onDragStart: (e) => {
      e.preventDefault();
    },
    onMouseDown: (e) => {
      e.preventDefault();
      start.current = toVector(e);
      onDragStart?.(start.current);
    },
    onTouchStart: (e) => {
      e.preventDefault();
      start.current = toVector(e.touches[0]);
      onDragStart?.(start.current);
    },
    onMouseMove: (e) => {
      if (!start.current) return;
      e.preventDefault();
      const cur = toVector(e);

      const velocity = {
        x: scale * pixelRatio() * (cur.x - start.current.x),
        y: scale * pixelRatio() * (cur.y - start.current.y),
      };

      onDrag?.(velocity, cur, start.current);
    },
    onTouchMove: (e) => {
      if (!start.current) return;
      e.preventDefault();
      const cur = toVector(e.touches[0]);

      const velocity = {
        x: scale * pixelRatio() * (cur.x - start.current.x),
        y: scale * pixelRatio() * (cur.y - start.current.y),
      };

      onDrag?.(velocity, cur, start.current);
    },
  };

  return [callbacks] as const;
};

const NeighborWhoIsLoved = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const engine = useEngineContext();

  const gameReducer = (state: GameState, action: GameAction) => {
    switch (action.type) {
      case "game-init":
        return "game-init";
      case "game-prompt": {
        disableBodyByLabel(engine, gameTriggerLabel);
        disableBodyByLabel(engine, neighborHeadLabel);
        return "game-prompt";
      }
      case "game-mode":
        {
          const head = matterBodyByLabel(engine, neighborHeadLabel);
          if (!head) return "game-mode";
          Sleeping.set(head, false);
        }
        return "game-mode";
      case "explode-head":
        return "explode-head";
    }
  };

  const [status, setStatus] = useReducer(gameReducer, "game-init");

  const pagination = usePagination();

  const head = matterBodyByLabel(engine, neighborHeadLabel);

  const [dragCallbacks] = useDrag({
    scale: isMobile() ? 0.75 : 1,
    onDragStart: () => {
      if (status === "game-prompt") setStatus({ type: "game-mode" });
    },
    onDrag: (velocity) => {
      if (!head) return;
      Body.setVelocity(head, velocity);
    },
  });

  return (
    <>
      <ActionBlock>
        {() => {
          disableBodyByLabel(engine, armLabel);
          disableBodyByLabel(engine, armBarrierLabel);
        }}
      </ActionBlock>
      <NeighborNoHead />
      {status === "game-prompt" && (
        <DownArrow
          style={{
            position: "absolute",
            top: 5,
            left: 85,
            color: "red",
          }}
        />
      )}
      <MotionDiv
        aria-label={neighborHeadLabel}
        startCy={60}
        startCx={110}
        width={92}
        height={85}
        vx={2}
        vy={2}
        onCollisionStart={() => {
          const go = async () => {
            if (status === "game-init") setStatus({ type: "game-prompt" });
            else if (status === "game-mode") {
              setStatus({ type: "explode-head" });
              await sleep(4000);
              pagination?.("next-wipe");
            }
          };

          go();
        }}
      >
        <div style={{ position: "relative" }}>
          {status !== "explode-head" && (
            <StaticImg src={downloads.head} {...dragCallbacks} />
          )}
          {status === "game-prompt" && (
            <DownArrow
              style={{
                position: "absolute",
                top: "-60%",
                left: "30%",
                color: "greenyellow",
              }}
            />
          )}
          {status === "explode-head" && (
            <ExplodingImage
              xNumParticles={10}
              yNumParticles={10}
              explosionFactor={10}
              delayInMs={300}
              src={downloads.head}
            />
          )}
        </div>
      </MotionDiv>
      {status === "game-init" && (
        <StaticDiv
          aria-label={gameTriggerLabel}
          cx={width / 2}
          cy={200}
          width={width}
          height={5}
        />
      )}
      <Walls />
    </>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const pagination = usePagination();
  const engine = useEngineContext();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-wipe");
            };

            go();
          }}
        >
          Start by finding a comfortable position...
        </Typewriter>
      </Panel>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-wipe");
            };

            go();
          }}
        >
          Visualize someone you don&apos;t know very well...
        </Typewriter>
      </Panel>
      <Panel>
        <Neighbor />
        <ActionBlock>
          {async ({ isBlockCancelled }) => {
            downloads.grumpy.resource.play();
            await sleep(3000);
            if (isBlockCancelled) return;
            pagination?.("next-wipe");
          }}
        </ActionBlock>
      </Panel>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-wipe");
            };

            go();
          }}
        >
          Direct loving kindness towards this person...
        </Typewriter>
      </Panel>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          May you be well...
        </Typewriter>
      </Panel>
      <Panel>
        <NeighborWell />
        <ActionBlock>
          {async (opts) => {
            downloads.twinkle.resource.play();
            await sleep(3000);
            if (opts.isBlockCancelled) return;
            stopEngine(engine);
            pagination?.("next-fade");
          }}
        </ActionBlock>
      </Panel>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              stopEngine(engine);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          May you be happy...
        </Typewriter>
      </Panel>
      <Panel aria-label="happy neighbor">
        <NeighborBody />
        <BouncyHead
          aria-label={neighborHeadLabel}
          src={downloads.head}
          style={{
            top: 10,
            left: 75,
            position: "absolute",
          }}
        />
        <SwingyArm />
        <ActionBlock>
          {async (opts) => {
            downloads.chuckle.resource.play();
            await sleep(4000);
            if (opts.isBlockCancelled) return;
            stopEngine(engine);
            pagination?.("next-fade");
          }}
        </ActionBlock>
      </Panel>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              stopEngine(engine);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          May you be at peace...
        </Typewriter>
      </Panel>
      <Panel aria-label="neighbor at peace">
        <NeighborAtPeace />
        <ActionBlock>
          {async () => {
            downloads.twinkle.resource.play();
            await sleep(4000);
            stopEngine(engine);
            pagination?.("next-fade");
          }}
        </ActionBlock>
      </Panel>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(1000);
              stopEngine(engine);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          May you be lov...
        </Typewriter>
      </Panel>
      <Panel>
        <NeighborWhoIsLoved />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{ overflow: "hidden", padding: "10px", boxSizing: "border-box" }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

const Component = () => (
  <GateBehindButton2 width={width} height={height}>
    <ComponentWorld />
  </GateBehindButton2>
);

export const ComponentFull = () => (
  <GateBehindButtonFull width={width} height={height}>
    <ComponentWorld />
  </GateBehindButtonFull>
);

export default Component;
