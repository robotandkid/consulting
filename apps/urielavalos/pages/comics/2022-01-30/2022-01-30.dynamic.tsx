import {
  PixiAppContext,
  PixiAppWrapper,
  PixiFilter,
  PixiLoaderResourcesContext,
  useLoadPixiSprite,
} from "@robotandkid/pixi-components";
import { Bodies, Body, Composite, Engine, Runner } from "matter-js";
import { useContext, useEffect, useState } from "react";
import "pixi-sound";

const engine = Engine.create();
const runner = Runner.create();

Runner.run(runner, engine);

const width = 150;
const height = 300;

export function Comic_2022_01_30() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/comics/2022-01-30/jump.png",
        },
        {
          type: "image",
          path: "/comics/2022-01-30/bg.png",
        },
        {
          type: "sound",
          path: "/comics/2022-01-30/jump.mp3",
        },
      ]}
    >
      {function useApp() {
        const app = useContext(PixiAppContext);
        const [spriteBody, setSpriteBody] = useState<Body>();

        useEffect(
          function init() {
            if (!app) return;

            const width = app.screen.width;
            const height = app.screen.height;

            const ground = Bodies.rectangle(width / 2, height, width, 25, {
              isStatic: true,
            });
            const ceiling = Bodies.rectangle(width / 2, 0, width, 25, {
              isStatic: true,
            });
            const wallL = Bodies.rectangle(0, height / 2, 25, height, {
              isStatic: true,
            });
            const wallR = Bodies.rectangle(width, height / 2, 25, height, {
              isStatic: true,
            });

            Composite.add(engine.world, [ground, ceiling, wallL, wallR]);
          },
          [app]
        );

        useLoadPixiSprite({
          path: "/comics/2022-01-30/bg.png",
          width,
          height,
          parentType: "app",
        });

        const spriteData = useLoadPixiSprite({
          path: "/comics/2022-01-30/jump.png",
          width: width * 0.5,
          height: (width * 0.5 * 330) / 141,
          parentType: "app",
        });

        const sprite = spriteData?.sprite;

        useEffect(
          function addSpriteToMatter() {
            if (!app || !sprite) return;

            const spriteBody = Bodies.rectangle(
              width / 2,
              height / 2,
              sprite.width,
              sprite.height
            );

            // spriteBody.friction = 0.05;
            // spriteBody.frictionAir = 0.00005;
            // spriteBody.restitution = 0.25;

            Composite.add(engine.world, spriteBody);
            setSpriteBody(spriteBody);

            return () => {
              Composite.remove(engine.world, spriteBody);
            };
          },
          [app, sprite]
        );

        useEffect(
          function syncPosition() {
            function animate() {
              for (const dot of engine.world.bodies) {
                if (dot.isStatic || !sprite) continue;

                const { x, y } = dot.position;

                sprite.position.x = x;
                sprite.position.y = y;

                break;
              }
            }

            if (!app || !sprite) return;

            app.ticker.add(animate);

            return () => {
              app.ticker.remove(animate);
            };
          },
          [app, sprite]
        );

        const resources = useContext(PixiLoaderResourcesContext);

        useEffect(() => {
          if (!app || !sprite || !resources) return;

          function jump() {
            if (!app || !spriteBody || !resources) return;

            const resource = resources["sound:/comics/2022-01-30/jump.mp3"];

            if (!resource) return;

            const { sound } = resource as any;

            if (!sound?.isPlaying) sound.play();

            Body.applyForce(
              spriteBody,
              { x: spriteBody.position.x, y: spriteBody.position.y },
              { x: 0, y: -app.renderer.height / 2500 }
            );
          }

          app.view.addEventListener("click", jump);
          app.view.addEventListener("touchstart", jump);

          return () => {
            app.view.removeEventListener("click", jump);
            app.view.removeEventListener("touchstart", jump);
          };
        }, [app, resources, sprite, spriteBody]);

        return (
          <PixiFilter comp={spriteData?.sprite} filters={{ filter: "glow" }} />
        );
      }}
    </PixiAppWrapper>
  );
}
