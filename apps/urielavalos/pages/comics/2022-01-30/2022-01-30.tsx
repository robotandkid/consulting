import dynamic from "next/dynamic";
import { ComicPlaceholder } from "../../../src/styles/styles";
import { theme } from "../../../src/styles/theme";
import { Comic_2022_01_30 as Type } from "./2022-01-30.dynamic";

export const Comic_2022_01_30 = dynamic<Parameters<typeof Type>>(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports don't work
    import("./2022-01-30.dynamic").then(
      (module) => module.Comic_2022_01_30
    ) as any,
  {
    ssr: false,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);
