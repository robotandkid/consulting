import { CenteredColumn } from "@robotandkid/components";
import { Comic2023_11_25Full } from "./index";

export default function () {
  return (
    <CenteredColumn
      as="main"
      style={{
        justifyContent: "flex-start",
        width: "100%",
        height: "100vh",
        paddingTop: "2rem",
      }}
    >
      <Comic2023_11_25Full />
    </CenteredColumn>
  );
}
