import {
  ActionBlock,
  Panel,
  StaticImg,
  StoryBoard,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import styled from "styled-components";
import {
  GateBehindButton as GateBehindButton2,
  GateBehindButtonFull,
} from "../../../src/styles/styles";
import { Typewriter as TypewriterOriginal } from "../../../src/utils/components";
import { sleep } from "../../../src/utils/promises";

const resources = {
  mom: {
    type: "image",
    url: "/comics/2023-11-25/mom.gif",
  },
  type: {
    type: "sound",
    url: "/sounds/textscroll.wav",
  },
} as const;

const width = 256;
const height = 256;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Typewriter = styled(TypewriterOriginal)`
  font-family: mrpixel, monospace;
  font-size: 1.5rem;
  letter-spacing: 0rem;
`;

const Screen = () => (
  <>
    <div
      style={{
        background: gbColor[0],
        position: "absolute",
        top: 0,
        width,
        height: 45,
      }}
    />
    <div
      style={{
        background: gbColor[0],
        position: "absolute",
        top: 170,
        width,
        height: 45,
      }}
    />
  </>
);

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            await sleep(4500);
            if (opts.isBlockCancelled) return;
            opts.paginate("next-fade");
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.mom}
          style={{ position: "absolute", top: 0 }}
        />
        <Screen />
        <Typewriter
          sound={undefined}
          textColor={gbColor[3]}
          cursorColor={gbColor[3]}
          style={{ position: "absolute", top: 180, left: 20 }}
        >
          Alli venden drogas!
        </Typewriter>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        overflow: "hidden",
        padding: "10px",
        boxSizing: "border-box",
        justifyContent: "flex-start",
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

const Component = () => (
  <GateBehindButton2 width={width} height={height}>
    <ComponentWorld />
  </GateBehindButton2>
);

export const ComponentFull = () => (
  <GateBehindButtonFull width={width} height={height}>
    <ComponentWorld />
  </GateBehindButtonFull>
);

export default Component;
