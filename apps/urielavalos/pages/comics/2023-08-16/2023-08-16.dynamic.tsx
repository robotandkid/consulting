import "@robotandkid/sprite-motion";
import { MotionDiv, World } from "@robotandkid/sprite-motion";
import { useEffect, useState } from "react";

const random = (scale: number) =>
  Math.random() * scale * (Math.random() > 0.5 ? -1 : 1);

const rightEye = { startCx: 200, startCy: 120 };
const leftEye = { startCx: 140, startCy: 115 };

const Tears = () => (
  <>
    {Array.from({ length: 100 }).map((_, index) => {
      const eye = index % 2 ? rightEye : leftEye;
      return (
        <MotionDiv
          key={Math.random()}
          height={5}
          width={5}
          {...eye}
          vx={random(3)}
          vy={random(3)}
          style={{
            borderRadius: "5px",
            borderColor: "black",
            background: "black",
          }}
        />
      );
    })}
  </>
);

const Comic2023_08_16 = () => {
  const [key, setKey] = useState(Math.random());

  useEffect(() => {
    const tears = () => setKey(Math.random());
    const unsubscribe = setInterval(tears, 800);
    return () => {
      clearInterval(unsubscribe);
    };
  });

  return (
    <World>
      <img src="/comics/2023-08-16/2023-08-16.png" />
      <Tears key={key} />
    </World>
  );
};

export default Comic2023_08_16;
