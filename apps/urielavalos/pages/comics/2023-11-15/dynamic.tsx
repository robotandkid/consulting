import {
  ActionBlock,
  Canvas,
  MotionImg,
  Panel,
  SpriteSheet,
  StaticImg,
  StoryBoard,
  useAbortCtrlCtx,
  useDownloadsCtx,
  usePagination,
  World,
} from "@robotandkid/sprite-motion";
import { Composite, Sleeping } from "matter-js";
import { useEffect, useState } from "react";
import styled from "styled-components";
import {
  GateBehindButton as GateBehindButton2,
  GateBehindButtonFull,
  GateButton,
  GateButtonContainer,
  PressStart,
} from "../../../src/styles/styles";
import { Typewriter as TypewriterOriginal } from "../../../src/utils/components";
import { useGuardedCallback } from "../../../src/utils/hooks";
import { sleep } from "../../../src/utils/promises";

const resources = {
  scene1ninja: {
    type: "image",
    url: "/comics/2023-11-15/ninja-01.png",
  },
  scene1cloud: {
    type: "image",
    url: "/comics/2023-11-15/ninja-cloud.png",
  },
  scene2ninja: {
    type: "image",
    url: "/comics/2023-11-15/ninja-right.png",
  },
  scene3ninja: {
    type: "image",
    url: "/comics/2023-11-15/ninja-left.png",
  },
  grass: {
    type: "image",
    url: "/comics/2023-11-15/grass.png",
  },
  skyLines: {
    type: "image",
    url: "/comics/2023-11-15/ninja-lines.png",
  },
  scene4ninja: {
    type: "image",
    url: "/comics/2023-11-15/ninja-running-left.png",
  },
  scene5ninja: {
    type: "image",
    url: "/comics/2023-11-15/ninja-running-right.png",
  },
  scene6bg: {
    type: "image",
    url: "/comics/2023-11-15/ninja-bg.png",
  },
  scene6ninjaR: {
    type: "image",
    url: "/comics/2023-11-15/ninja-jumping-right.png",
  },
  scene6ninjaL: {
    type: "image",
    url: "/comics/2023-11-15/ninja-jumping-left.png",
  },
  scene7ninja: {
    type: "image",
    url: "/comics/2023-11-15/ninja-falling.png",
  },
  scene8: {
    type: "image",
    url: "/comics/2023-11-15/ninja-final-01.png",
  },
  scene9: {
    type: "image",
    url: "/comics/2023-11-15/ninja-final-02.png",
  },
  hit: {
    type: "image",
    url: "/comics/2023-11-15/ninja-hit.png",
  },
  type: {
    type: "sound",
    url: "/sounds/textscroll.wav",
  },
  theme: {
    type: "sound",
    url: "/comics/2023-11-15/ninja-theme.mp3",
  },
  running: {
    type: "sound",
    url: "/comics/2023-11-15/ninja-running.mp3",
  },
  hitSound: {
    type: "sound",
    url: "/comics/2023-11-15/ninja-punch.mp3",
  },
  grunt: {
    type: "sound",
    url: "/comics/2023-11-15/ninja-grunt.mp3",
  },
  gameOver: {
    type: "sound",
    url: "/comics/2023-11-15/ninja-game-over.mp3",
  },
  swoosh: {
    type: "sound",
    url: "/comics/2023-08-26/swoosh.mp3",
  },
} as const;

const width = 256;
const height = 256;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Typewriter = styled(TypewriterOriginal)`
  font-family: mrpixel, monospace;
  font-size: 1.5rem;
  letter-spacing: 0rem;
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Clouds = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <>
      {Array.from({ length: 100 }).map((_, index) => (
        <MotionImg
          key={index}
          src={downloads.scene1cloud}
          startLeft={Math.random() * -10000 - 10}
          startTop={Math.random() * 75}
          disableCollisions
        />
      ))}
    </>
  );
};

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Screen = () => (
  <>
    <div
      style={{
        background: gbColor[3],
        position: "absolute",
        top: 0,
        width,
        height: height * 0.25,
      }}
    />
    <div
      style={{
        background: gbColor[3],
        position: "absolute",
        top: 2 * height * 0.25,
        width,
        height: height * 0.5,
      }}
    />
  </>
);

const Grass = (props: { direction: "left" | "right" }) => {
  const { direction } = props;
  const { downloads } = useDownloadsCtx<typeof resources>();

  const numGrass = 1_000;
  const totalWidth = width * numGrass;

  const [left, _setLeft] = useState(
    direction === "left" ? -totalWidth + width : 0
  );
  const setLeft = useGuardedCallback(_setLeft);
  useEffect(() => {
    const clear = setInterval(
      () => setLeft((c) => (direction === "left" ? c + 5 : c - 5)),
      10
    );
    return () => {
      clearInterval(clear);
    };
  }, [direction, setLeft]);

  return (
    <div
      style={{
        overflow: "hidden",
        display: "flex",
        width: totalWidth,
        height: 30,
        position: "absolute",
        top: 105,
        left,
      }}
    >
      {Array.from({ length: numGrass }).map((_, key) => (
        <StaticImg key={key} src={downloads.grass} style={{ width }} />
      ))}
    </div>
  );
};

const SkyLines = (props: { direction: "left" | "right" | "down" }) => {
  const { direction } = props;
  const { downloads } = useDownloadsCtx<typeof resources>();

  const numBlocks = 1_000;
  const totalWidth = 90 * numBlocks;

  const [left, _setLeft] = useState(
    direction === "left" ? -totalWidth + 256 : 0
  );
  const setLeft = useGuardedCallback(_setLeft);
  useEffect(() => {
    const clear = setInterval(
      () => setLeft((c) => (direction === "left" ? c + 3 : c - 3)),
      10
    );
    return () => {
      clearInterval(clear);
    };
  }, [direction, setLeft]);

  return (
    <div
      style={{
        overflow: "hidden",
        display: "flex",
        width: totalWidth,
        height: 80,
        position: "absolute",
        top: 50,
        left,
      }}
    >
      {Array.from({ length: numBlocks }).map((_, key) => (
        <StaticImg
          key={key}
          src={downloads.skyLines}
          style={{ width: 90, height: 90 }}
        />
      ))}
    </div>
  );
};

const VerticalSkyLines = (props: { direction: "up" | "down" }) => {
  const { direction } = props;
  const { downloads } = useDownloadsCtx<typeof resources>();

  const numBlocks = 1_000;
  const totalHeight = 90 * numBlocks;

  const [top, _setTop] = useState(direction === "up" ? -totalHeight + 256 : 0);
  const setLeft = useGuardedCallback(_setTop);
  useEffect(() => {
    const clear = setInterval(
      () => setLeft((c) => (direction === "up" ? c + 2 : c - 2)),
      10
    );
    return () => {
      clearInterval(clear);
    };
  }, [direction, setLeft]);

  return (
    <div
      style={{
        overflow: "hidden",
        display: "flex",
        flexDirection: "column",
        width: 256,
        height: totalHeight,
        position: "absolute",
        top,
        left: 0,
      }}
    >
      {Array.from({ length: numBlocks }).map((_, key) => (
        <StaticImg
          key={key}
          src={downloads.skyLines}
          style={{ width: 256, height: 256, transform: "rotate(90deg)" }}
        />
      ))}
    </div>
  );
};

const NinjaJumpingRight = (props: {
  showHit: (point: { x: number; y: number }) => void;
}) => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <MotionImg
      aria-label="ninja-01"
      src={downloads.scene6ninjaR}
      startLeft={0}
      startTop={150}
      vx={2.5}
      vy={-2}
      onCollisionStart={(thisBody) => {
        Sleeping.set(thisBody, true);
        props.showHit({
          x: thisBody.position.x - 10,
          y: thisBody.position.y - 70,
        });
      }}
    />
  );
};

const NinjaJumpingLeft = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <MotionImg
      aria-label="ninja-02"
      src={downloads.scene6ninjaL}
      startLeft={175}
      startTop={100}
      vx={-2.5}
      vy={0}
      onCollisionStart={(thisBody) => {
        Sleeping.set(thisBody, true);
      }}
    />
  );
};

const ComponentInner = (props: { onDone: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  const [hit, setHit] = useState<{ x: number; y: number } | undefined>(
    undefined
  );

  const ctrl = useAbortCtrlCtx();
  const paginate = usePagination();

  return (
    <StoryBoard disableManualNavigation={false}>
      <Panel aria-label="Scene 1: ninja's facing each other">
        <ActionBlock>
          {({ engine, isBlockCancelled, paginate }) => {
            downloads.theme.resource.play();
            engine.gravity.x = 0.5;
            engine.gravity.y = 0;
            const go = async () => {
              await sleep(4000);
              if (!isBlockCancelled) paginate("next-fade");
            };
            go();
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.scene1ninja}
          style={{
            position: "absolute",
            top: 0,
          }}
        />
        <Clouds />
      </Panel>
      <Panel aria-label="Scene2: ninja looking left">
        <ActionBlock>
          {({ isBlockCancelled, paginate }) => {
            const go = async () => {
              await sleep(3000);
              if (!isBlockCancelled) paginate("next-fade");
            };
            go();
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.scene2ninja}
          style={{
            position: "absolute",
            top: -5,
          }}
        />
        <Screen />
      </Panel>
      <Panel aria-label="Scene 3: ninja looking right">
        <ActionBlock>
          {({ isBlockCancelled, paginate }) => {
            const go = async () => {
              await sleep(3000);
              if (!isBlockCancelled) paginate("next-fade");
            };
            go();
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.scene3ninja}
          style={{
            position: "absolute",
            top: -10,
          }}
        />
        <Screen />
      </Panel>
      <Panel aria-label="Scene 4: ninja running left">
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async ({ isBlockCancelled, paginate }) => {
              downloads.running.resource.play();
              await sleep(3900);
              if (!isBlockCancelled) paginate("next-fade");
            }}
          </ActionBlock>
          <div
            style={{
              background: gbColor[0],
              position: "absolute",
              top: 0,
              height: "100%",
              width: "100%",
            }}
          ></div>
          <SkyLines direction="left" />
          <Grass direction="left" />
          <SpriteSheet
            src={downloads.scene4ninja}
            renderLeft={135}
            renderTop={65}
            renderWidth={99}
            renderHeight={70}
            frameWidth={99}
            frameHeight={70}
            fps={15}
          />
          <Screen />
        </Canvas>
      </Panel>
      <Panel aria-label="Scene 5: ninja running right">
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async ({ isBlockCancelled, paginate }) => {
              await sleep(3900);
              if (!isBlockCancelled) paginate("next-fade");
            }}
          </ActionBlock>
          <div
            style={{
              background: gbColor[0],
              position: "absolute",
              top: 0,
              height: "100%",
              width: "100%",
            }}
          ></div>
          <SkyLines direction="right" />
          <Grass direction="right" />
          <SpriteSheet
            src={downloads.scene5ninja}
            renderLeft={10}
            renderTop={65}
            renderWidth={99}
            renderHeight={70}
            frameWidth={99}
            frameHeight={70}
            fps={15}
          />
          <Screen />
        </Canvas>
      </Panel>
      <Panel aria-label="Scene 6: ninjas jumping">
        <ActionBlock>
          {async ({ engine }) => {
            downloads.running.resource.stop();
            downloads.swoosh.resource.play();
            engine.gravity.x = 0;
            engine.gravity.y = -0.05;
            // dunno why there are bodies
            for (const body of engine.world.bodies) {
              Composite.remove(engine.world, body);
            }
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.scene6bg}
          style={{
            position: "absolute",
            top: 0,
          }}
        />
        <NinjaJumpingLeft />
        {hit && (
          <Canvas width={width} height={height}>
            <SpriteSheet
              src={downloads.hit}
              renderLeft={hit.x}
              renderTop={hit.y}
              renderWidth={95}
              renderHeight={125}
              frameWidth={95}
              frameHeight={125}
              fps={12}
              loop={false}
              onLoopEnd={async () => {
                await sleep(1000);
                if (ctrl?.signal.aborted) return;
                paginate?.("next-fade");
              }}
            />
          </Canvas>
        )}
        <NinjaJumpingRight
          showHit={(point) => {
            downloads.hitSound.resource.play();
            setHit(point);
          }}
        />
      </Panel>
      <Panel aria-falling="ninja falling">
        <ActionBlock>
          {async (opts) => {
            opts.engine.gravity.x = 0;
            opts.engine.gravity.y = 0.35;
            downloads.grunt.resource.play();
            await sleep(3000);
            if (opts.isBlockCancelled) return;
            // dunno why there are bodies
            for (const body of opts.engine.world.bodies) {
              Composite.remove(opts.engine.world, body);
            }
            opts.paginate("next-fade");
          }}
        </ActionBlock>
        <VerticalSkyLines direction="down" />
        <MotionImg
          src={downloads.scene7ninja}
          startLeft={0}
          startTop={0}
          width={width}
          height={height}
          disableCollisions
        />
      </Panel>
      <Panel arial-label="dad on floor">
        <ActionBlock>
          {async () => {
            downloads.theme.resource.stop();
            downloads.gameOver.resource.play();
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.scene8}
          style={{
            position: "absolute",
            top: -35,
          }}
        />
        <div
          style={{
            position: "absolute",
            background: gbColor[3],
            width,
            height: 100,
            top: height - 100,
          }}
        />
        <Typewriter
          textColor={gbColor[1]}
          cursorColor={gbColor[1]}
          sound={undefined}
          style={{ top: height - 100, width: 240, background: gbColor[3] }}
          onDone={async () => {
            await sleep(2000);
            if (ctrl?.signal.aborted) return;
            paginate?.("next-fade");
          }}
        >
          Ouch! You punched me in the face for real, little buddy
        </Typewriter>
      </Panel>
      <Panel arial-label="son apologizing">
        <StaticImg
          src={downloads.scene9}
          style={{
            position: "absolute",
            top: -25,
          }}
        />
        <div
          style={{
            position: "absolute",
            background: gbColor[3],
            width,
            height: 70,
            top: height - 70,
          }}
        />
        <Typewriter
          textColor={gbColor[1]}
          cursorColor={gbColor[1]}
          sound={undefined}
          style={{ top: height - 70 + 10, width: 150, background: gbColor[3] }}
          onDone={async () => {
            await sleep(5000);
            if (ctrl?.signal.aborted) return;
            paginate?.("next-fade");
            props.onDone();
          }}
        >
          Sorry dad!
        </Typewriter>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  // there's some wierd bug in the matter-js engine
  const [key, setKey] = useState(0);
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        overflow: "hidden",
        justifyContent: "flex-start",
        background: gbColor[3],
      }}
      width={width}
      height={height}
      key={key}
    >
      <ComponentInner onDone={() => setKey((k) => k + 1)} />
    </World>
  );
};

const button = (
  <GateButton $buttonColor={gbColor[1]} $textColor={gbColor[2]}>
    <PressStart />
  </GateButton>
);

const bg = <GateButtonContainer $background={gbColor[3]}></GateButtonContainer>;

const Component = () => (
  <GateBehindButton2
    container={bg}
    button={button}
    width={width}
    height={height}
  >
    <ComponentWorld />
  </GateBehindButton2>
);

export const ComponentFull = () => (
  <GateBehindButtonFull
    container={bg}
    button={button}
    width={width}
    height={height}
  >
    <ComponentWorld />
  </GateBehindButtonFull>
);

export default Component;
