import {
  ActionBlock,
  MotionImg,
  Panel,
  StaticDiv,
  StaticImg,
  StoryBoard,
  useDownloadsCtx,
  usePagination,
  World,
} from "@robotandkid/sprite-motion";
import styled from "styled-components";
import {
  GateBehindButton as GateBehindButton2,
  GateBehindButtonFull,
} from "../../../src/styles/styles";
import { Typewriter as TypewriterOriginal } from "../../../src/utils/components";
import { sleep } from "../../../src/utils/promises";

const resources = {
  screen: {
    type: "image",
    url: "/comics/2023-11-04/legos-01.png",
  },
  buy: {
    type: "image",
    url: "/comics/2023-11-04/legos-02.png",
  },
  finger: {
    type: "image",
    url: "/comics/2023-11-04/legos-02-finger.png",
  },
  click: {
    type: "sound",
    url: "/comics/2023-11-04/coin2.mp3",
  },
  desk: {
    type: "image",
    url: "/comics/2023-11-04/legos-03.png",
  },
  desk2: {
    type: "image",
    url: "/comics/2023-11-04/legos-03-fingers.png",
  },
  popup: {
    type: "sound",
    url: "/comics/2023-11-04/huh.mp3",
  },
  type: {
    type: "sound",
    url: "/sounds/textscroll.wav",
  },
  boy: {
    type: "image",
    url: "/comics/2023-11-04/legos-03-boy.png",
  },
  final: {
    type: "image",
    url: "/comics/2023-11-04/legos-04.png",
  },
  theme: {
    type: "sound",
    url: "/comics/2023-11-04/theme.mp3",
  },
} as const;

const width = 256;
const height = 406;

const Typewriter = styled(TypewriterOriginal)`
  font-family: mrpixel, monospace;
  font-size: 1.75rem;
  letter-spacing: 0rem;
`;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const pagination = usePagination();

  return (
    <StoryBoard disableManualNavigation={false}>
      <Panel>
        <ActionBlock>
          {({ engine }) => {
            downloads.theme.resource.play();
            engine.gravity.y = 1;
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.screen}
          style={{
            position: "absolute",
            top: 0,
          }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          Look what I found...
        </Typewriter>
      </Panel>
      <Panel>
        <StaticImg
          src={downloads.screen}
          style={{
            position: "absolute",
            top: 0,
          }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          Isn&apos;t that cool?
        </Typewriter>
      </Panel>
      <Panel>
        <StaticImg
          src={downloads.screen}
          style={{
            position: "absolute",
            top: 0,
          }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-wipe");
            };

            go();
          }}
        >
          Son: meh
        </Typewriter>
      </Panel>
      <Panel>
        <StaticImg
          src={downloads.buy}
          style={{
            position: "absolute",
            top: 0,
          }}
        />
        <MotionImg src={downloads.finger} startLeft={78} startTop={-128} />
        <StaticDiv
          cx={128}
          cy={128}
          width={256}
          height={5}
          onCollisionStart={() => {
            const go = async () => {
              downloads.click.resource.play();
              await sleep(1000);
              pagination?.("next-wipe");
            };

            go();
          }}
        />
      </Panel>
      <Panel>
        <Typewriter
          style={{ top: "50%" }}
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-wipe");
            };

            go();
          }}
        >
          1 week later...
        </Typewriter>
      </Panel>
      <Panel>
        <StaticImg
          src={downloads.desk}
          style={{ position: "absolute", top: 0 }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          Look what came in!
        </Typewriter>
      </Panel>
      <Panel>
        <ActionBlock>
          {({ engine }) => {
            downloads.popup.resource.play();
            engine.gravity.y = -1;
          }}
        </ActionBlock>
        <StaticDiv cx={128} cy={0} width={256} height={5} />
        <MotionImg
          src={downloads.boy}
          startLeft={-5}
          startTop={150}
          onCollisionStart={() => {
            const go = async () => {
              await sleep(3000);
              pagination?.("next-wipe");
            };

            go();
          }}
        />
        <StaticImg
          src={downloads.desk2}
          style={{ position: "absolute", top: 0 }}
        />
      </Panel>
      <Panel>
        <StaticImg
          src={downloads.final}
          style={{ position: "absolute", top: 0 }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(10_000);
              pagination?.("next-fade");
            };

            go();
          }}
        >
          Fin...
        </Typewriter>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        overflow: "hidden",
        padding: "10px",
        boxSizing: "border-box",
        justifyContent: "flex-start",
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

const Component = () => (
  <GateBehindButton2 width={width} height={height}>
    <ComponentWorld />
  </GateBehindButton2>
);

export const ComponentFull = () => (
  <GateBehindButtonFull width={width} height={height}>
    <ComponentWorld />
  </GateBehindButtonFull>
);

export default Component;
