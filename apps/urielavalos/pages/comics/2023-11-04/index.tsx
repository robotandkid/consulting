import dynamic from "next/dynamic";
import { ComicPlaceholder } from "../../../src/styles/styles";
import { theme } from "../../../src/styles/theme";

export const Comic2023_11_04 = dynamic(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports don't work
    import("./dynamic"),
  {
    ssr: true,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);

export const Comic2023_11_04Full = dynamic(
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore dynamic imports don't work
  () => import("./dynamic").then((module) => module.ComponentFull),
  {
    ssr: true,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);
