import {
  defaultTextStyle,
  PixiApp,
  PixiAppContext,
  TypewriterText,
  TypewriterTextOpts,
  useAddPixiFilter2,
  useLoadPixiSprite,
} from "@robotandkid/pixi-components";
import { Bodies, Composite, Engine, Runner } from "matter-js";
import { PixelateFilter } from "pixi-filters";
import {
  Children,
  cloneElement,
  createContext,
  ReactElement,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";

const width = 150;
const height = 200;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const scenes = [
  {
    name: "scene1",
    commands: [
      {
        type: "draw-bg",
        path: "/comics/2022-02-06/grandma1.png",
      },
      {
        type: "delay",
        timeout: 1000,
      },
      // make fork go up
      {
        type: "draw-physics-sprite",
        path: "/comics/2022-02-06/fork.png",
        // add initial gravity velocity
      },
      {
        type: "delay",
        timeout: 1000,
      },
      {
        type: "say",
        actor: "Grandma",
        text: "mmmm.... this is good!",
      },
      {
        type: "delay",
        timeout: 1000,
      },
      {
        type: "draw-bg",
        path: "/comics/2022-02-06/grandma-knockout-teeth.png",
      },
      {
        type: "draw-physics-sprite",
        path: "/comics/2022-02-06/grandma-teeth.png",
        // add initial gravity velocity
      },
      {
        type: "delay",
        timeout: 1000,
      },
      {
        type: "say",
        actor: "Me",
        text: "Wow, Grandma! I didn't know you had fake teeth!",
      },
      {
        type: "delay",
        timeout: 1000,
      },
      {
        type: "jump",
        target: "scene2",
        filter: "pixelate",
      },
    ],
  },
  {
    name: "scene2",
    commands: [
      {
        type: "draw-background",
        path: "/comics/2022-02-06/grandma-angry.png",
      },
      {
        type: "say",
        actor: "Me",
        text: "Aw man grandma, you look kinda angry.",
      },
      {
        type: "delay",
        timeout: 1000,
      },
      {
        // fire particle effect
      },
    ],
  },
];

interface PhysicsContextType {
  engine: ReturnType<typeof Engine.create>;
  runner: ReturnType<typeof Runner.create>;
}

const PhysicsContext = createContext<PhysicsContextType>({} as any);

interface OnNext {
  onNext?: () => void;
}

/**
 * Reveals children one at a time, depending on when then previous child calls
 * onNext.
 */
function useRevealChildren(props: {
  onNext?: () => void;
  children: ReactElement<OnNext> | ReactElement<OnNext>[];
}) {
  const array = useMemo(
    () => Children.toArray(props.children) as React.ReactElement<SceneProps>[],
    [props.children]
  );

  const [index, setIndex] = useState(0);
  const [childToRender, setChildToRender] = useState<React.ReactElement[]>();
  const child = array[index];

  const onNext = useCallback(() => setIndex((c) => c + 1), []);

  useEffect(() => {
    if (child) {
      setChildToRender((c) => {
        if (c) return [...c, cloneElement(child, { onNext })];
        else return [cloneElement(child, { onNext })];
      });
    }
  }, [child, onNext]);

  return { children: childToRender, index };
}

interface SceneProps {
  name: string;
  onNext?: () => void;
}

function SceneProvider(props: {
  children: React.ReactElement<SceneProps>[] | React.ReactElement<SceneProps>;
}) {
  const physicsContext = useMemo(
    () => ({
      engine: Engine.create(),
      runner: Runner.create(),
    }),
    []
  );

  useEffect(
    function setupPhysics() {
      Runner.run(physicsContext.runner, physicsContext.engine);
    },
    [physicsContext]
  );

  const { children: scene } = useRevealChildren({ children: props.children });

  return (
    <PhysicsContext.Provider value={physicsContext}>
      {scene}
    </PhysicsContext.Provider>
  );
}

function Scene(
  props: {
    children: React.ReactElement<OnNext>[];
  } & SceneProps
) {
  const { children: original, onNext } = props;
  const { children: commands, index } = useRevealChildren(props);

  const nextCommands = useMemo(() => {
    if (!commands || !original) return null;
    if (index < original.length - 1) return commands;
    if (!commands[index]) return commands;

    const _commands = [...commands];

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore it's probably not null 🤷
    _commands[index] = cloneElement(commands[index], { onNext });

    return _commands;
  }, [commands, index, onNext, original]);

  return <>{nextCommands}</>;
}

function Background(
  props: { path: string; width: number; height: number } & OnNext
) {
  const { onNext, width, height } = props;

  const sprite = useLoadPixiSprite({
    path: props.path,
    width,
    height,
    parentType: "app",
  });

  useEffect(() => {
    if (sprite) {
      onNext?.();
    }
  }, [onNext, sprite]);

  return null;
}

function Delay(props: { timeout: number } & OnNext) {
  const { onNext, timeout } = props;

  useEffect(() => {
    const _timeout = setTimeout(() => onNext?.(), timeout);

    return () => {
      clearTimeout(_timeout);
    };
  }, [onNext, timeout]);

  return null;
}

function PhysicsSprite(props: { path: string; x: number; y: number } & OnNext) {
  const { onNext, path, x, y } = props;
  const app = useContext(PixiAppContext);

  const spriteData = useLoadPixiSprite({
    path: path,
    width: app?.renderer.width ?? 0,
    height: app?.renderer.height ?? 0,
    parentType: "app",
  });

  const sprite = spriteData?.sprite;
  const physics = useContext(PhysicsContext);

  useEffect(
    function createPhysicsBody() {
      if (!sprite) return;

      const spriteBody = Bodies.rectangle(x, y, sprite.width, sprite.height);

      Composite.add(physics.engine.world, spriteBody);

      return () => {
        Composite.remove(physics.engine.world, spriteBody);
      };
    },
    [physics.engine.world, sprite, x, y]
  );

  useEffect(() => {
    if (sprite) {
      onNext?.();
    }
  }, [sprite, onNext]);

  useEffect(
    function syncPosition() {
      function animate() {
        for (const dot of physics.engine.world.bodies) {
          if (dot.isStatic || !sprite) continue;

          const { x, y } = dot.position;

          sprite.position.x = x;
          sprite.position.y = y;

          break;
        }
      }

      if (!app || !sprite) return;

      app.ticker.add(animate);

      return () => {
        app.ticker.remove(animate);
      };
    },
    [app, physics.engine.world.bodies, sprite]
  );

  return null;
}

function Say(
  props: {
    actor: string;
    hideActor?: boolean;
    children: string | string[];
  } & OnNext &
    Pick<TypewriterTextOpts, "style">
) {
  const { children, onNext, style, hideActor } = props;
  const app = useContext(PixiAppContext);
  const lines = Array.isArray(children) ? children : [children];
  const [lineNumber, setLineNumber] = useState(0);
  const line = lines[lineNumber] as string | undefined;
  const lineWithActor = `${props.actor}: ${line}`;
  const done = lineNumber === lines.length - 1;
  const text = !line ? undefined : hideActor ? line : lineWithActor;

  const onLineDone = useCallback(() => {
    setLineNumber((l) => l + 1);
    if (done) onNext?.();
  }, [done, onNext]);

  return (
    <TypewriterText
      lineId={text ?? ""}
      style={style}
      soundPath="/sounds/textscroll.wav"
      typingSpeedInMs={20}
      onLineDone={onLineDone}
      onLineDoneDelayMs={500}
      parent={app}
    >
      {text ?? ""}
    </TypewriterText>
  );
}

type Filter = Parameters<typeof useAddPixiFilter2>[0]["filters"];

function End(props: { transition: Filter } & OnNext) {
  const app = useContext(PixiAppContext);

  useAddPixiFilter2({
    comp: app,
    filters: props.transition,
  });

  return null;
}

function Test() {
  const dialogStyle = {
    style: {
      ...defaultTextStyle(),
      fontSize: "1em",
      height: height / 2,
      top: height / 2,
    },
  };

  return (
    <SceneProvider>
      <Scene name="scene1">
        <Background
          path="/comics/2022-01-30/bg.png"
          width={width}
          height={height}
        />
        <Delay timeout={1000} />
        <PhysicsSprite
          path="/comics/2022-01-30/jump.png"
          x={width / 2}
          y={height / 2}
        />
        <Delay timeout={1000} />
        <Say actor="Grandma" hideActor {...dialogStyle}>
          mmmm.... this is good!
        </Say>
        <Delay timeout={1000} />
        <Say actor="Me" {...dialogStyle}>
          Wow, Grandma! I didn&apos;t know you had fake teeth!
        </Say>
        <Delay timeout={1000} />
        <End
          transition={{
            filter: new PixelateFilter(0),
            animation(time, filter) {
              (filter as PixelateFilter).size = time;
            },
          }}
        />
      </Scene>
    </SceneProvider>
  );
}

export function Comic_2022_02_06() {
  return (
    <PixiApp
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/comics/2022-01-30/jump.png",
        },
        {
          type: "image",
          path: "/comics/2022-01-30/bg.png",
        },
        {
          type: "sound",
          path: "/comics/2022-01-30/jump.mp3",
        },
      ]}
    >
      <Test />
    </PixiApp>
  );
}
