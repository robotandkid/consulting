import {
  ActionBlock,
  ExplodingImage,
  Panel,
  StaticImg,
  StoryBoard,
  useDownloadsCtx,
  usePagination,
  World,
} from "@robotandkid/sprite-motion";
import styled, { keyframes } from "styled-components";
import { GateBehindButton } from "../../../src/styles/styles";
import { Typewriter } from "../../../src/utils/components";
import { sleep } from "../../../src/utils/promises";

const resources = {
  me: {
    type: "image",
    url: "/comics/2023-09-18/me.png",
  },
  boss: {
    type: "image",
    url: "/comics/2023-09-18/boss.png",
  },
  troll: {
    type: "image",
    url: "/comics/2023-09-18/troll2.png",
  },
  type: {
    type: "sound",
    url: "/sounds/textscroll.wav",
  },
  background: {
    type: "sound",
    url: "/comics/2023-09-18/8-bit-game-music.mp3",
  },
  anger: {
    type: "sound",
    url: "/comics/2023-09-18/wut.mp3",
  },
} as const;

const width = 332;
const height = 376;

const appearing = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`;

const AppearingBg = styled.div`
  animation: ${appearing} 2s forwards;
  background: red;
  position: absolute;
  top: 0;
  height: 256px;
  width: 100%;
`;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const paginate = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {async () => {
            downloads.background.resource.play();
          }}
        </ActionBlock>
        <StaticImg
          aria-label="sick worker"
          width={256}
          height={256}
          src={downloads.me}
          style={{
            top: 0,
            left: 0,
            position: "absolute",
          }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              paginate?.("next-wipe");
            };

            go();
          }}
        >
          This COVID is kicking my butt... I&apos;m one step away from the ER.
        </Typewriter>
      </Panel>
      <Panel>
        <StaticImg
          aria-label="corporate"
          width={256}
          height={256}
          src={downloads.boss}
          style={{
            top: 0,
            left: 0,
            position: "absolute",
          }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(3000);
              paginate?.("next-wipe");
            };

            go();
          }}
        >
          You&apos;re still coming to the office tomorrow, right? (no pressure)
        </Typewriter>
      </Panel>
      <Panel>
        <ActionBlock>
          {async () => {
            await sleep(2000);
            downloads.background.resource.stop();
            downloads.anger.resource.play();
          }}
        </ActionBlock>
        <AppearingBg />
        <ExplodingImage
          aria-label="troll face"
          width={169}
          height={164}
          src={downloads.troll}
          style={{
            top: "20%",
            left: "20%",
            position: "absolute",
          }}
          xNumParticles={4}
          yNumParticles={4}
          delayInMs={2000}
          explosionFactor={6}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(4000);
              paginate?.("next-wipe");
            };

            go();
          }}
        >
          ...
        </Typewriter>
      </Panel>
      <Panel>
        <StaticImg
          aria-label="corporate"
          width={256}
          height={256}
          src={downloads.boss}
          style={{
            top: 0,
            left: 0,
            position: "absolute",
          }}
        />
        <Typewriter
          sound={downloads.type}
          onDone={() => {
            const go = async () => {
              await sleep(5000);
              paginate?.("next-wipe");
            };

            go();
          }}
        >
          You OK? I said no pressure.❤️
        </Typewriter>
      </Panel>
    </StoryBoard>
  );
};

const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{ overflow: "hidden", padding: "10px", boxSizing: "border-box" }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

const Component = () => (
  <GateBehindButton width={width} height={height}>
    <ComponentWorld />
  </GateBehindButton>
);

export default Component;
