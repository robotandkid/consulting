import {
  ActionBlock,
  MotionImg,
  Panel,
  StaticDiv,
  StaticImg,
  StoryBoard,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import { Sleeping } from "matter-js";
import { useState } from "react";
import styled from "styled-components";
import {
  GateBehindButton as GateBehindButton2,
  GateBehindButtonFull,
  GateButton,
  GateButtonContainer,
  PressStart,
} from "../../../src/styles/styles";
import { Typewriter as TypewriterOriginal } from "../../../src/utils/components";
import { sleep } from "../../../src/utils/promises";

const resources = {
  fence: {
    type: "image",
    url: "/comics/2023-12-17/greg05.png",
  },
  paws: {
    type: "image",
    url: "/comics/2023-12-17/greg02.png",
  },
  doberman: {
    type: "image",
    url: "/comics/2023-12-17/greg01.png",
  },
  greg: {
    type: "image",
    url: "/comics/2023-12-17/greg03.png",
  },
  us: {
    type: "image",
    url: "/comics/2023-12-17/greg04.png",
  },
  dobermanAngry: {
    type: "sound",
    url: "/comics/2023-12-17/angry-dog-sm.mp3",
  },
  gregCrying: {
    type: "sound",
    url: "/comics/2023-12-17/whining-dog.mp3",
  },
  theme: {
    type: "sound",
    url: "/comics/2023-12-17/greg-theme.mp3",
  },
} as const;

const width = 256;
const height = 256;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Typewriter = styled(TypewriterOriginal)`
  font-family: mrpixel, monospace;
  font-size: 1.5rem;
  letter-spacing: 0rem;
`;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;

const ComponentInner = (props: { onDone: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard disableManualNavigation={false}>
      <Panel aria-label="Scene 1: angry dog">
        <ActionBlock>
          {async ({ engine, isBlockCancelled, paginate }) => {
            downloads.theme.resource.play();
            downloads.dobermanAngry.resource.play();
            engine.gravity.x = 0;
            engine.gravity.y = -1;
            await sleep(4000);
            if (!isBlockCancelled) paginate("next-fade");
          }}
        </ActionBlock>
        <MotionImg
          src={downloads.doberman}
          startLeft={0}
          startTop={256}
          onCollisionStart={(thisBody) => {
            Sleeping.set(thisBody, true);
          }}
        />
        <StaticDiv cx={128} cy={10} width={256} height={5} />
        <StaticImg
          aria-label="fence"
          src={downloads.fence}
          style={{
            position: "absolute",
            bottom: -50,
          }}
        />
        <StaticImg
          aria-label="doberman paws"
          src={downloads.paws}
          style={{
            position: "absolute",
            bottom: 30,
          }}
        />
      </Panel>
      <Panel aria-label="Greg crying">
        <ActionBlock>
          {async ({ isBlockCancelled, paginate }) => {
            downloads.gregCrying.resource.play();
            await sleep(5000);
            if (!isBlockCancelled) paginate("next-fade");
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.greg}
          style={{
            position: "absolute",
            top: -20,
          }}
        />
        <div
          style={{
            position: "absolute",
            background: gbColor[3],
            width: "100%",
            top: 190,
            height: 256 - 190,
          }}
        >
          <Typewriter
            sound={undefined}
            style={{
              position: "relative",
              width: "90%",
              padding: 10,
              background: gbColor[3],
            }}
          >
            Greg, he doesn&apos;t want to play with you!
          </Typewriter>
        </div>
      </Panel>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            downloads.gregCrying.resource.play();
            await sleep(8000);
            if (opts.isBlockCancelled) return;
            opts.paginate("next-fade");
            props.onDone();
          }}
        </ActionBlock>
        <StaticImg
          src={downloads.us}
          style={{ position: "absolute", top: -5 }}
        />
        <div
          style={{
            position: "absolute",
            background: gbColor[3],
            width: "100%",
            top: 215,
            height: 256 - 215,
          }}
        >
          <Typewriter
            sound={undefined}
            style={{
              background: gbColor[3],
              width: "90%",
              padding: 5,
            }}
          >
            Come on dude let&apos;s go!
          </Typewriter>
        </div>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  // there's some wierd bug in the matter-js engine
  const [key, setKey] = useState(0);
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        overflow: "hidden",
        justifyContent: "flex-start",
        background: gbColor[3],
      }}
      width={width}
      height={height}
      key={key}
    >
      <ComponentInner onDone={() => setKey((k) => k + 1)} />
    </World>
  );
};

const button = (
  <GateButton $buttonColor={gbColor[1]} $textColor={gbColor[2]}>
    <PressStart />
  </GateButton>
);

const bg = <GateButtonContainer $background={gbColor[3]}></GateButtonContainer>;

const Component = () => (
  <GateBehindButton2
    container={bg}
    button={button}
    width={width}
    height={height}
  >
    <ComponentWorld />
  </GateBehindButton2>
);

export const ComponentFull = () => (
  <GateBehindButtonFull
    container={bg}
    button={button}
    width={width}
    height={height}
  >
    <ComponentWorld />
  </GateBehindButtonFull>
);

export default Component;
