import {
  MotionImg,
  Downloads,
  StaticDiv,
  useResources,
  World,
} from "@robotandkid/sprite-motion";
import { Body, Engine, Sleeping } from "matter-js";
import { useEffect, useRef, useState } from "react";
import styled, { css, keyframes } from "styled-components";
import { Blockquote, GateBehindButton } from "../../../src/styles/styles";

const metaResources = {
  dad: {
    type: "image",
    url: "/comics/2023-08-26/dad.png",
  },
  mom: {
    type: "image",
    url: "/comics/2023-08-26/mom.png",
  },
  momSound: {
    type: "sound",
    url: "/comics/2023-08-26/mom.mp3",
  },
  whoosh: {
    type: "sound",
    url: "/comics/2023-08-26/swoosh.mp3",
  },
} as const;

const darkenKeyframe = keyframes`
  from {background-color: white;}
  to {background-color: black;}
`;

const StyledWorld = styled(World)`
  overflow: hidden;
`;

const StyledScene = styled.div<{ $darken: boolean }>`
  width: 100%;
  height: 100%;
  ${(p) =>
    p.$darken &&
    css`
      animation: 3s ${darkenKeyframe} linear forwards;
    `}
`;

const width = 512;
const height = 256;

const ComponentInner = (props: {
  onDone: () => void;
  resources: Downloads<typeof metaResources>;
}) => {
  const engine = useRef<Engine>();
  const [darken, setDarken] = useState(false);
  const [dad, setDad] = useState(false);

  const showDad = () => {
    const go = async () => {
      // disable all bodies
      for (const body of engine.current?.world.bodies ?? []) {
        Sleeping.set(body, true);
      }
      // invert gravity
      if (engine.current) {
        engine.current.gravity.x = 0;
        engine.current.gravity.y = -1;
      }
      await new Promise((resolve) => setTimeout(resolve, 3000));
      setDad(true);
      await new Promise((resolve) => setTimeout(resolve, 500));
      props.resources.downloads?.whoosh.resource.play();
    };

    go();
  };

  // had to do these globally because sometimes they get stuck
  useEffect(
    function velocities() {
      const setVelocities = () => {
        for (const body of engine.current?.world.bodies ?? []) {
          if (!dad) Body.setVelocity(body, { x: 20, y: 0 });
          else Body.setVelocity(body, { x: 0, y: -10 });
        }
      };
      setInterval(setVelocities, 1000);
    },
    [dad]
  );

  return (
    <StyledWorld
      width={width}
      height={height}
      onInit={(thisEngine) => (engine.current = thisEngine)}
    >
      <StyledScene $darken={darken}>
        {!dad && (
          <StaticDiv
            aria-label="invisible bottom wall"
            width={1000}
            height={5}
            cx={128}
            cy={256}
          />
        )}
        <StaticDiv
          aria-label="invisible right wall"
          width={5}
          height={256}
          cx={256}
          cy={128}
          onCollisionStart={() => {
            props.resources.downloads?.momSound.resource.play();
            setDarken(true);
            showDad();
          }}
        />
        <MotionImg
          aria-label="mom"
          src={props.resources.downloads?.mom}
          width={256}
          height={256}
          startLeft={-256}
          startTop={100}
        />
        {dad && (
          <MotionImg
            aria-label="dad"
            src={props.resources.downloads?.dad}
            width={217}
            height={226}
            startLeft={270}
            startTop={200}
            style={{ zIndex: 100 }}
          />
        )}
        {dad && (
          <StaticDiv
            aria-label="invisible top wall"
            width={1000}
            height={5}
            cx={128}
            cy={20}
            onCollisionStart={() => {
              const go = async () => {
                // disable all bodies
                for (const body of engine.current?.world.bodies ?? []) {
                  Sleeping.set(body, true);
                }
                await new Promise((resolve) => setTimeout(resolve, 3000));
                props.onDone();
              };

              go();
            }}
          />
        )}
      </StyledScene>
    </StyledWorld>
  );
};

type UseResources = typeof useResources<typeof metaResources>;

const GateBehindDownload = (props: {
  downloaded: ReturnType<UseResources>;
}) => {
  const { downloaded } = props;
  const [key, setKey] = useState(0);
  const canShow = downloaded !== "loading";

  return (
    <>
      {downloaded === "loading" && <div>Loading...</div>}
      {canShow && (
        <>
          <ComponentInner
            key={key}
            onDone={() => setKey((c) => c + 1)}
            resources={downloaded}
          />
          <Blockquote>
            I hate that in social networks you can&apos;t always be real. In
            normal couples, things aren&apos;t always so rosy. My spouse and I
            fight sometimes.
          </Blockquote>
        </>
      )}
    </>
  );
};

const Component = () => {
  const downloaded = useResources(metaResources);

  return (
    <GateBehindButton width={width} height={height + 200}>
      <GateBehindDownload downloaded={downloaded} />
    </GateBehindButton>
  );
};

export default Component;
