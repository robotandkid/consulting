import dynamic from "next/dynamic";
import { ComicPlaceholder } from "../../../src/styles/styles";
import { theme } from "../../../src/styles/theme";

export const Comic2023_08_26 = dynamic(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports don't work
    import("./2023-08-26.dynamic"),
  {
    ssr: false,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);
