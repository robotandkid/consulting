import "@robotandkid/sprite-motion";
import { Joint, useEngineContext, World } from "@robotandkid/sprite-motion";
import { Body } from "matter-js";
import { useEffect } from "react";

const ComponentInner = () => {
  const engine = useEngineContext();

  if (engine) {
    engine.gravity.x = 0;
    engine.gravity.y = -1;
  }

  useEffect(() => {
    const wave = () => {
      engine?.world.composites.forEach((body) => {
        if (body.bodies[0])
          Body.setVelocity(body.bodies[0], { x: Math.random() + 3, y: 0 });
      });
    };

    const interval = setInterval(wave, 1500);

    return () => {
      clearInterval(interval);
    };
  }, [engine?.world.composites]);

  return (
    <>
      <img src="/comics/2023-08-20/body.png" />
      <Joint
        startLeft={15}
        startTop={25}
        hingeX="25%"
        hingeY="100%"
        width={51}
        height={127}
        style={{
          background: `url(/comics/2023-08-20/arm1.png)`,
        }}
      />
      <Joint
        startLeft={190}
        startTop={95}
        hingeX="0%"
        hingeY="100%"
        width={70}
        height={108}
        style={{
          background: `url(/comics/2023-08-20/arm2.png)`,
        }}
      />
    </>
  );
};

const Component = () => {
  return (
    <World>
      <ComponentInner />
    </World>
  );
};

export default Component;
