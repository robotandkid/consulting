import {
  ActionBlock,
  Canvas,
  Panel,
  StaticImg,
  StoryBoard,
  UiCommandPanel,
  UiDialogPanel,
  UiPanel,
  UiPlayerInfoPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  wife: {
    type: "video",
    url: "/comics/2024/01-30/robot2.mp4",
  },
  scream: {
    type: "sound",
    url: "/comics/2024/01-30/static_scream.mp3",
  },
  robot: {
    type: "image",
    url: "/comics/2024/01-30/robot.nes.png",
  },
  theme: {
    type: "sound",
    url: "/comics/2024/01-30/theme.mp3",
  },
  change: {
    type: "sound",
    url: "/comics/2024/01-14/coin-collect.mp3",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [state, setState] = useState<"init" | "command" | "translate">("init");

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {() => {
              downloads.theme.resource.play();
            }}
          </ActionBlock>
          <StaticImg src={downloads.robot} width={width} />
          <UiPlayerInfoPanel
            info={[
              { key: "Name", value: "Robot" },
              { key: "Lvl", value: `1` },
              { key: "HP", value: "80" },
              { key: "Exp", value: "30" },
            ]}
            zIndex={2}
            width="100%"
            height="4rem"
            position="absolute"
            top={0}
          />
          <UiDialogPanel
            visibleLines={3}
            zIndex={2}
            width="100%"
            height="8rem"
            position="absolute"
            bottom="0"
            onDone={async () => {
              await sleep(1000);
              page?.("next-fade");
            }}
          >
            {`Everyone seems to be getting 
kind bot followers.
But... 
my followers seem to be all sexy 
porn bots who do nothing but get me
in trouble with my spouse unit.`}
          </UiDialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {() => {
              downloads.theme.resource.stop();
              downloads.scream.resource.play();
            }}
          </ActionBlock>
          <Video src={downloads.wife} loop />
          <UiPlayerInfoPanel
            info={[
              { key: "Name", value: "Spouse Bot" },
              { key: "Lvl", value: `99` },
              { key: "HP", value: "800" },
              { key: "Exp", value: "3000" },
            ]}
            zIndex={2}
            width="100%"
            height="4rem"
            position="absolute"
            top={0}
          />
          {["init", "command"].includes(state) && (
            <UiDialogPanel
              visibleLines={3}
              zIndex={2}
              width="100%"
              height="8rem"
              position="absolute"
              bottom="0"
              onDone={() => setState("command")}
            >
              {`01010111 01101000 01101111 00100000
01110011 00100000 01001011 01100001`}
            </UiDialogPanel>
          )}
          {state === "command" && (
            <div
              style={{
                position: "absolute",
                bottom: "6rem",
                zIndex: 2,
                width: "22rem",
              }}
            >
              <UiCommandPanel
                commands={["Translate"]}
                subCommands={{
                  Translate: {
                    type: "cmd",
                    commands: ["Spouse"],
                  },
                }}
                onCommandChange={() => downloads.change.resource.play()}
                onSubCommandChange={() => downloads.change.resource.play()}
                onDone={() => {
                  downloads.change.resource.play();
                  downloads.scream.resource.play();
                  setState("translate");
                }}
                zIndex={2}
                width="100%"
              />
            </div>
          )}
          {state === "translate" && (
            <UiDialogPanel
              visibleLines={3}
              zIndex={2}
              width="100%"
              height="8rem"
              position="absolute"
              bottom="0"
            >
              {`WHO IS KARISSA?
WHY DID THEY ❤️ YOUR POSTS?`}
            </UiDialogPanel>
          )}
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  // there's some wierd bug in the matter-js engine
  const [key] = useState(0);
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
      key={key}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: "gb.50",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <UiPanel title="GAME (1)" onClick={() => setShow(true)} width="25rem">
          <VanillaButton>Start</VanillaButton>
        </UiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
