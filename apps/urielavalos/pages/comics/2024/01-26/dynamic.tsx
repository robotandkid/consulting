import {
  ActionBlock,
  Canvas,
  Panel,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  link: {
    type: "video",
    url: "/comics/2024/01-26/link.mp4",
  },
  itemAcquired: {
    type: "sound",
    url: "/comics/2024/01-14/item.mp3",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;

const Dialog = pandaStyled(UiDialogPanel, {
  base: {
    background: "black",
    padding: 0,
    "& div": {
      border: "none",
      borderColor: "black",
      background: "black",
      alignItems: "center",
      fontSize: "32px",
    },
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [state, setState] = useState<"init" | "final">("init");

  return (
    <StoryBoard disableManualNavigation>
      <Panel aria-label="start">
        <Canvas width={width} height={height}>
          <>
            <ActionBlock>{() => {}}</ActionBlock>
            <Video src={downloads.link} loop={false} />
          </>
          {state === "init" && (
            <Dialog
              visibleLines={1}
              wordSpeed={150}
              width="450px"
              zIndex={2}
              position="absolute"
              top={200}
              onDone={async () => {
                await sleep(1000);
                setState("final");
              }}
            >
              {`IT'S DANGEROUS TO GO ALONE!
TAKE THIS...`}
            </Dialog>
          )}
          {state === "final" && (
            <Dialog
              visibleLines={1}
              width="450px"
              zIndex={2}
              position="absolute"
              top={200}
            >
              {`YOU GOT COVID!!!`}
            </Dialog>
          )}
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  // there's some wierd bug in the matter-js engine
  const [key] = useState(0);
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
      key={key}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: "gb.50",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <UiPanel title="GAME (1)" onClick={() => setShow(true)} width="25rem">
          <VanillaButton>Start</VanillaButton>
        </UiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
