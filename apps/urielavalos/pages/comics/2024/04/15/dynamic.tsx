import {
  ActionBlock,
  Canvas,
  CanvasImage,
  Code,
  OneBit,
  Panel,
  StoryBoard,
  UiCommandPanel,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  useInterpolate,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useEffect, useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  ring: {
    type: "sound",
    url: "/comics/2024/04/15/effect-ring.mp3",
  },
  click: {
    type: "sound",
    url: "/comics/2024/03/26/click-effect.mp3",
  },
  explosion: {
    type: "sound",
    url: "/comics/2024/03/26/explosion-effect.mp3",
  },
  scene0201: {
    type: "image",
    url: "/comics/2024/04/15/scene02-01.png",
  },
  scene0202: {
    type: "video",
    url: "/comics/2024/04/15/scene02-02.mp4",
  },
  scene0203laptop: {
    type: "image",
    url: "/comics/2024/04/15/scene02-03-laptop.png",
  },
  scene0203video: {
    type: "video",
    url: "/comics/2024/04/15/scene02-03-video.mp4",
  },
  scene0106: {
    type: "video",
    url: "/comics/2024/03/26/scene-01-06.1bit.mp4",
  },
  scene01: {
    type: "sound",
    url: "/comics/2024/03/26/scene01.mp3",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
  variants: {
    size: {
      large: {
        "& > div": {
          backgroundColor: oneBitColor[0],
          borderColor: oneBitColor[1],
          color: oneBitColor[1],
          fontSize: "28px",
        },
      },
      normal: {},
    },
  },
});

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const CommandPanelContainer = pandaStyled("div", {
  base: {
    position: "absolute",
    zIndex: 2,
    width: "65%",
    top: "14rem !important",
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& div, & h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
  },
});

const CommandPanel = pandaStyled(UiCommandPanel, {
  base: {
    position: "absolute",
  },
});

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Scene02a = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  const x = useInterpolate(-1500, -750, { delta: 10 });

  return (
    <>
      <CanvasImage
        src={downloads.scene0201}
        renderTop={0}
        renderLeft={x}
        renderWidth={2048}
        renderHeight={height}
      />
      <OneBit threshold={175} />
      <Code
        left={10}
        top={375}
        speed={10}
        style={{
          font: "64px mrpixel",
          lineHeightAsPx: 64,
          glitch: false,
        }}
        onDone={async () => {
          await sleep(4_000);
          page?.("next-fade");
        }}
      >{`Somewhere in 
the present...`}</Code>
    </>
  );
};

// const useStateMachine = () => {
//   const states = ["init", "answer"] as const;
//   type State = typeof states[number];
//   const [state, setState] = useState<State>(states[0]);
//   const checkState = (expected: State, actual: State): actual is State => {};
//   return [state, setState] as const;
// };

const Scene02b = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [state, setState] = useState<"init" | "answer">("init");
  const [key, setKey] = useState(Math.random());

  return (
    <Canvas width={width} height={height}>
      <ActionBlock>
        {async (opts) => {
          downloads.ring.resource.play();
          await sleep(2_000);
          if (opts.isBlockCancelled) return;
          setState("answer");
        }}
      </ActionBlock>
      <Video src={downloads.scene0202} loop />
      <OneBit threshold={175} />
      <DialogPanel key={key} visibleLines={2} width="100%" size="large">
        * You have an incoming call
      </DialogPanel>
      {state === "answer" && (
        <CommandPanelContainer top={300} height={100}>
          <CommandPanel
            width="100%"
            height="100%"
            commands={["Answer"]}
            subCommands={{
              Answer: { type: "cmd", commands: ["Yes", "No"] },
            }}
            onCommandChange={() => downloads.click.resource.play()}
            onSubCommandChange={() => downloads.click.resource.play()}
            onDone={(_, subCommand) => {
              downloads.ring.resource.stop();
              if (subCommand === "Yes") page?.("next-fade");
              else {
                setKey(Math.random());
                downloads.ring.resource.play();
              }
            }}
          />
        </CommandPanelContainer>
      )}
    </Canvas>
  );
};

const Scene01c = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [state, setState] = useState<"init" | "answer">("init");

  const dialog = [
    `Hello doctor.
We'd like you to recover some very
expensive equipment in the Amazon,
and complete the research.
Everything will be explained if 
you choose to accept.`,
    `You are one of the few
scientists in the world who can 
complete this research. We won't 
be able to protect you from other
unsavory organizations who may 
be more forceful than we are...
if you catch my drift. 
Have you changed your mind yet?`,
    `I'm gonna level with you...
it's in our best interest to
eliminate you if you do not
cooperate, since we can't have
you joining the competition. 
So you ARE joining us. 
This conversation is over!`,
  ];

  const [current, setCurrent] = useState(0);

  return (
    <Canvas width={width} height={height}>
      <CanvasImage
        src={downloads.scene0203laptop}
        renderTop={0}
        renderLeft={0}
        renderHeight={height}
        renderWidth={width}
      />
      <Video
        src={downloads.scene0203video}
        loop
        renderLeft={125}
        renderTop={100}
        renderHeight={250}
        renderWidth={250}
      />
      <OneBit threshold={100} />
      <DialogPanel
        key={current}
        visibleLines={4}
        width="100%"
        size="large"
        minHeight={200}
        height={200}
        onDone={async () => {
          await sleep(1_000);
          setState("answer");
          //   page?.("next-fade");
        }}
      >
        {dialog[current] ?? ""}
      </DialogPanel>
      {state === "answer" && (
        <CommandPanelContainer top={300} height={100}>
          <CommandPanel
            width="100%"
            height="100%"
            commands={["Reply"]}
            subCommands={{
              Reply: { type: "cmd", commands: ["Yes", "No"] },
            }}
            onCommandChange={() => downloads.click.resource.play()}
            onSubCommandChange={() => downloads.click.resource.play()}
            onDone={(_, subCommand) => {
              setState("init");
              if (subCommand === "Yes") page?.("next-fade");
              else {
                setCurrent((c) => c + 1);
              }
            }}
          />
        </CommandPanelContainer>
      )}
    </Canvas>
  );
};

const Scene01d = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.explosion.resource.play();
        }}
      </ActionBlock>
      <DialogPanel
        visibleLines={2}
        width="100%"
        size="large"
        onDone={async () => {
          await sleep(1_000);
          page?.("next-fade");
        }}
      >{`They were left with only one 
option: the doomsday device...`}</DialogPanel>
    </Canvas>
  );
};

const Scene01End = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const y = useInterpolate(-1000, -250, { delta: 10 });

  useEffect(() => {
    if (y === -250) {
      const timeout = setTimeout(() => {
        page?.("next-wipe");
      }, 5_000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [page, y]);

  return (
    <Canvas>
      <Video
        src={downloads.scene0106}
        renderLeft={0}
        renderTop={y}
        renderWidth={512}
        renderHeight={2560}
      />
      <DialogPanel
        visibleLines={2}
        width="100%"
        size="large"
      >{`Unfortunately, while the device 
destroyed their civilization... 
the explosion did not destroy 
the portal...`}</DialogPanel>
    </Canvas>
  );
};

const ComponentInner = () => {
  // const { downloads } = useDownloadsCtx<typeof resources>();
  // const page = usePagination();
  // const [state, setState] = React.useState<"init" | "final" | "blur" | "end">(
  //   "init"
  // );

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <Scene02a />
        </Canvas>
      </Panel>
      <Panel>
        <Scene02b />
      </Panel>
      <Panel>
        <Scene01c />
      </Panel>
      <Panel>
        <Scene01d />
      </Panel>
      <Panel>
        <Scene01End />
      </Panel>
      <Panel>
        <DialogPanel
          visibleLines={2}
          width="100%"
          size="large"
        >{`The end?`}</DialogPanel>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: gbColor[0],
    // backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
