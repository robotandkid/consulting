import {
  ActionBlock,
  Canvas,
  OneBit,
  Panel,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/04/26/theme.wav.mp3",
  },
  chewing: {
    type: "sound",
    url: "/comics/2024/04/26/chewing.mp3",
  },
  barfing: {
    type: "sound",
    url: "/comics/2024/04/26/vomit.mp3",
  },
  scene01: {
    type: "video",
    url: "/comics/2024/04/26/scene01.mp4",
  },
  scene02: {
    type: "video",
    url: "/comics/2024/04/26/scene02.mp4",
  },
  scene03: {
    type: "video",
    url: "/comics/2024/04/26/scene03.mp4",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
  variants: {
    size: {
      large: {
        "& > div": {
          backgroundColor: oneBitColor[0],
          borderColor: oneBitColor[1],
          color: oneBitColor[1],
          fontSize: "28px",
        },
      },
      normal: {},
    },
  },
});

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     top: "14rem !important",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& div, & h1": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Scene01 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <>
      <ActionBlock>
        {() => {
          downloads.theme.resource.stop();
          downloads.theme.resource.play();
        }}
      </ActionBlock>
      <Video
        src={downloads.scene01}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <OneBit threshold={250} />
      <DialogPanel
        visibleLines={3}
        width="100%"
        size="large"
        onDone={async () => {
          await sleep(1000);
          page?.("next-fade");
        }}
      >
        {`Dad: Why so serious?
You gotta eat more than just nuggets.
You have to try at least one.`}
      </DialogPanel>
    </>
  );
};

const Scene02 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <>
      <ActionBlock>
        {() => {
          downloads.chewing.resource.play();
        }}
      </ActionBlock>
      <Video
        src={downloads.scene02}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
        loop={false}
        onEnd={async () => {
          await sleep(2_000);
          downloads.chewing.resource.stop();
          page?.("next-fade");
        }}
      />
      <OneBit threshold={250} />
      <DialogPanel visibleLines={3} width="100%" size="large">
        {`(chewing)`}
      </DialogPanel>
    </>
  );
};

const Scene03 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <>
      <ActionBlock>
        {async (opts) => {
          await sleep(2_000);
          if (opts.isBlockCancelled) return;
          downloads.barfing.resource.play();
        }}
      </ActionBlock>
      <Video
        src={downloads.scene03}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
        loop={false}
        onEnd={async () => {
          await sleep(5_000);
          page?.("next-fade");
        }}
      />
      <OneBit threshold={250} />
      <DialogPanel visibleLines={3} width="100%" size="large">
        {`Kid: (Barf)
Dad: Aw come on dude, 
it's just veggies!`}
      </DialogPanel>
    </>
  );
};

const ComponentInner = () => {
  // const { downloads } = useDownloadsCtx<typeof resources>();
  // const page = usePagination();
  // const [state, setState] = React.useState<"init" | "final" | "blur" | "end">(
  //   "init"
  // );

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <Scene01 />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <Scene02 />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <Scene03 />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: gbColor[0],
    // backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
