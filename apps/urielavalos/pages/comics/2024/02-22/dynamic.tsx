import {
  ActionBlock,
  Canvas,
  OvalIris,
  Panel,
  StoryBoard,
  UiCommandPanel,
  UiDialogPanel,
  UiPanel,
  UiPlayerInfoPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  change: {
    type: "sound",
    url: "/comics/2024/01-14/coin-collect.mp3",
  },
  alarm: {
    type: "sound",
    url: "/comics/2024/02-22/alarm.mp3",
  },
  zombies: {
    type: "video",
    url: "/comics/2024/02-22/zombies.mp4",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
  base: {
    zIndex: 2,
    position: "absolute",
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
  },
});

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const CommandPanelContainer = pandaStyled("div", {
  base: {
    position: "absolute",
    zIndex: 2,
    width: "65%",
    height: "8rem",
    top: "14rem !important",
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& div, & h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
  },
});

const CommandPanel = pandaStyled(UiCommandPanel, {
  base: {
    position: "absolute",
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  const [state, setState] = React.useState<
    "init" | "commands" | "nope" | "shutdown"
  >("init");

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              downloads.alarm.resource.play();
              await sleep(15_000);
              if (!opts.isBlockCancelled) return;
              downloads.alarm.resource.play();
              await sleep(15_1000);
              if (!opts.isBlockCancelled) return;
              downloads.alarm.resource.play();
            }}
          </ActionBlock>
          <Video src={downloads.zombies} />
          <PlayerInfoPanel
            info={[
              { key: "Name", value: "Robot" },
              { key: "HP", value: "1/100" },
              { key: "Lvl", value: `33` },
            ]}
            width="100%"
            height="4rem"
            top={0}
          />
          {["init", "commands"].includes(state) && (
            <DialogPanel
              visibleLines={3}
              width="100%"
              height="8rem"
              bottom="0"
              onDone={async () => {
                await sleep(1000);
                setState("commands");
              }}
            >
              {`You have little health.
Monsters are up ahead.
What do you wish to do?`}
            </DialogPanel>
          )}
          {state === "commands" && (
            <CommandPanelContainer>
              <CommandPanel
                width="100%"
                height="100%"
                commands={["Attack", "Run", "Use", "Check", "Auto", "Guard"]}
                subCommands={{
                  Attack: { type: "cmd", commands: ["Zombies"] },
                  Run: undefined,
                  Use: undefined,
                  Check: undefined,
                  Auto: undefined,
                  Guard: undefined,
                }}
                onCommandChange={() => downloads.change.resource.play()}
                onSubCommandChange={() => downloads.change.resource.play()}
                onDone={(value) => {
                  if (value === "Attack") setState("nope");
                }}
              />
            </CommandPanelContainer>
          )}
          {["nope", "shutdown"].includes(state) && (
            <DialogPanel
              visibleLines={3}
              width="100%"
              height="8rem"
              bottom="0"
              onDone={async () => {
                await sleep(1000);
                setState("shutdown");
              }}
            >
              {`Nope.
Shutting down.`}
            </DialogPanel>
          )}
          {state === "shutdown" && (
            <OvalIris durationInMs={1000} onEnd={() => page?.("next-fade")} />
          )}
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>{() => downloads.alarm.resource.stop()}</ActionBlock>
          <div
            style={{
              width: "100%",
              height: "100%",
              background: oneBitColor[0],
            }}
          ></div>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <VanillaButton>Start</VanillaButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
