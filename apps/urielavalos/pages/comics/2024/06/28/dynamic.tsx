import {
  ActionBlock,
  Canvas,
  CanvasDithering,
  CanvasSpeechBubble,
  funkyFuture8Palette,
  Panel,
  ShowAfterDelay,
  sleep,
  StoryBoard,
  UiPanel,
  useCanvasRegistration,
  useDownloadsCtx,
  useKeyRegistration,
  usePagination,
  usePanelCanvasCtx2D,
  useStateSteps,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/06/19/theme.wav.mp3",
  },
  laugh: {
    type: "sound",
    url: "/comics/2024/06/28/laugh.mp3",
  },
  water: {
    type: "sound",
    url: "/comics/2024/06/28/water.mp3",
  },
  scene1Talk: {
    type: "sound",
    url: "/comics/2024/06/28/scene1.mp3",
  },
  scene2TalkA: {
    type: "sound",
    url: "/comics/2024/06/28/scene2.mp3",
  },
  scene2TalkB: {
    type: "sound",
    url: "/comics/2024/06/28/scene2b.mp3",
  },
  scene1: {
    type: "video",
    url: "/comics/2024/06/28/scene1.mp4",
  },
  scene2: {
    type: "video",
    url: "/comics/2024/06/28/scene2.mp4",
  },
  scene3: {
    type: "video",
    url: "/comics/2024/06/28/scene3.mp4",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["#0D0D0D", "white"] as const;

const panelBg = "#fff7f8";
const panelFg = "#000000";
const palette = funkyFuture8Palette();

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Blur = (props: {
  size: number;
  duration: number;
  direction: "in" | "out";
}) => {
  const [keyChanged, key] = useKeyRegistration("blur", props);
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && ctx && render) {
    if (props.direction === "out") {
      let time = 0;
      let size = props.size;

      render((_, timeElapsedBetweenFrames) => {
        if (time + timeElapsedBetweenFrames >= props.duration) return key;
        time += timeElapsedBetweenFrames;
        size -= (props.size * timeElapsedBetweenFrames) / props.duration;
        size = Math.max(0, size);
        ctx.filter = `blur(${size}px)`;
        return key;
      });
    } else {
      let time = 0;
      let size = 0;

      render((_, timeElapsedBetweenFrames) => {
        if (time + timeElapsedBetweenFrames >= props.duration) return key;
        time += timeElapsedBetweenFrames;
        size += (props.size * timeElapsedBetweenFrames) / props.duration;
        size = Math.min(props.size, size);
        ctx.filter = `blur(${size}px)`;
        return key;
      });
    }
  }

  return null;
};

const Scene1 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          downloads.water.resource.stop();
          downloads.water.resource.play({ loop: true });
          downloads.laugh.resource.play();
        }}
      </ActionBlock>
      <Video
        src={downloads.scene1}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <Blur size={8} duration={5000} direction="out" />
      <CanvasDithering palette={palette} />
      <ShowAfterDelay timeout={5000}>
        <ActionBlock>
          {() => {
            downloads.scene1Talk.resource.play();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{
            left: 10,
            top: 175,
            width: 485,
            height: 80,
            radius: 1,
          }}
          textStyle={{
            font: "italic 24px comic",
            lineHeightAsPx: 28,
            fillStyle: panelFg,
            glitch: false,
          }}
          style={{
            fillColor: panelBg,
          }}
          text={{ rx: 15, ry: 15, speed: 10 }}
          onDone={async () => {
            await sleep(2_000);
            page?.("next-fade");
          }}
        >
          {`
KID: If I press here, you lift your legs
like a bridge. And I'll go under...
      `.toUpperCase()}
        </CanvasSpeechBubble>
      </ShowAfterDelay>
    </Canvas>
  );
};

const Scene2 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [Step, , nextStep] = useStateSteps({ steps: ["start", "ok"] });

  return (
    <Canvas>
      <ActionBlock>{async () => {}}</ActionBlock>
      <Video
        src={downloads.scene2}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <CanvasDithering palette={palette} />
      <Step step="start">
        <ActionBlock>
          {() => {
            downloads.scene2TalkA.resource.play();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{
            left: 10,
            top: 120,
            height: 125,
            width: 485,
            radius: 10,
          }}
          textStyle={{
            font: "italic 24px comic",
            lineHeightAsPx: 28,
            fillStyle: "#000000",
            glitch: false,
          }}
          style={{
            fillColor: panelBg,
          }}
          text={{ rx: 20, ry: 20, speed: 30 }}
          onDone={async () => {
            await sleep(2_000);
            nextStep();
          }}
        >
          {`
KID: And if I press here, you lower 
the bridge. And squish anyone that
doesn't press the button.
`.toUpperCase()}
        </CanvasSpeechBubble>
      </Step>
      <Step step="ok">
        <ActionBlock>
          {() => {
            downloads.scene2TalkB.resource.play();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{
            left: 260,
            top: 350,
            height: 60,
            width: 170,
            radius: 10,
          }}
          textStyle={{
            font: "italic 24px comic",
            lineHeightAsPx: 28,
            fillStyle: "#000000",
            glitch: false,
          }}
          style={{
            fillColor: panelBg,
          }}
          text={{ rx: 20, ry: 20, speed: 30 }}
          onDone={async () => {
            await sleep(2_000);
            page?.("next-fade");
          }}
        >
          {`
KID: OK, GO!
`}
        </CanvasSpeechBubble>
      </Step>
    </Canvas>
  );
};

const Scene3 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.laugh.resource.play();
        }}
      </ActionBlock>
      <Video
        src={downloads.scene3}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
        loop={false}
        onEnd={async () => {
          await sleep(15_000);
          page?.("next");
        }}
      />
      <CanvasDithering palette={palette} />
      <ShowAfterDelay timeout={5000}>
        <Blur size={32} duration={8000} direction="in" />
      </ShowAfterDelay>
    </Canvas>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Scene1 />
      </Panel>
      <Panel>
        <Scene2 />
      </Panel>
      <Panel>
        <Scene3 />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
