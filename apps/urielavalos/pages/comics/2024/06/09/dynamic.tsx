import {
  ActionBlock,
  Canvas,
  CanvasDithering,
  CanvasImage,
  CanvasSpeechBubble,
  cybergumsPalette,
  Panel,
  sleep,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  useInterpolate,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useEffect, useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/06/09/theme.wav.mp3",
  },
  pop: {
    type: "sound",
    url: "/comics/2024/06/09/pop.mp3",
  },
  scene01: {
    type: "image",
    url: "/comics/2024/06/09/01.png",
  },
  lens: {
    type: "image",
    url: "/comics/2024/06/09/lens.png",
  },
  boy: {
    type: "image",
    url: "/comics/2024/06/09/boy.png",
  },
  exploding: {
    type: "video",
    url: "/comics/2024/06/09/exploding.mp4",
  },
  teen: {
    type: "image",
    url: "/comics/2024/06/09/teen.png",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["#0D0D0D", "white"] as const;

const panelBg = "#3a2b3b";
const panelFg = "#ffffff";

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const MagnifyingLens = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  const [dx, setDx] = useState<[number, number]>([0, 50]);
  const x = useInterpolate(dx[0], dx[1], {
    delta: 5,
    onDone: async () => {
      setDx((c) => [c[0] + 50, c[1] + 50]);
    },
  });

  const [dy, setDy] = useState<[number, number]>([250, 25]);
  const y = useInterpolate(dy[0], dy[1], {
    delta: 5,
    onDone: async () => {
      setDy((c) => [c[0] - 50, c[1] + 50]);
    },
  });

  useEffect(() => {
    if (x === 100 && y === 25) page?.("next-fade");
  }, [page, x, y]);

  return (
    <>
      <ActionBlock>
        {() => {
          downloads.pop.resource.play();
        }}
      </ActionBlock>
      <CanvasImage
        src={downloads.lens}
        renderTop={y}
        renderLeft={x}
        renderWidth={width / 2}
        renderHeight={height / 2}
      />
    </>
  );
};

const Scene1 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [inspect, setInspect] = useState(false);

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          await sleep(4_000);
          setInspect(true);
        }}
      </ActionBlock>
      <CanvasImage
        src={downloads.scene01}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      {inspect && <MagnifyingLens />}
      <CanvasDithering palette={cybergumsPalette()} />
    </Canvas>
  );
};

const Scene2 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <CanvasImage
        src={downloads.boy}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <CanvasDithering palette={cybergumsPalette()} />
      <CanvasSpeechBubble
        pos={{
          left: 100,
          top: 300,
          width: 300,
          height: 110,
          pLeft: 250,
          pTop: 275,
          radius: 20,
        }}
        text={{
          rx: 25,
          ry: 30,
          speed: 10,
        }}
        style={{
          fillColor: panelBg,
          strokeColor: panelFg,
        }}
        textStyle={{
          font: "italic 24px comic",
          lineHeightAsPx: 28,
          fillStyle: panelFg,
          glitch: false,
        }}
        onDone={async () => {
          await sleep(2_000);
          page?.("next-fade");
        }}
      >
        {`
Did you touch my
lego sword display?
      `.toUpperCase()}
      </CanvasSpeechBubble>
    </Canvas>
  );
};

const Scene3 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <CanvasImage
        src={downloads.teen}
        renderLeft={0}
        renderTop={0}
        renderWidth={width}
        renderHeight={height}
      />
      <CanvasDithering palette={cybergumsPalette()} />
      <CanvasSpeechBubble
        pos={{
          left: 25,
          top: 375,
          width: 300,
          height: 110,
          pLeft: 250,
          pTop: 350,
          radius: 20,
        }}
        text={{
          rx: 25,
          ry: 30,
          speed: 10,
        }}
        style={{
          fillColor: panelBg,
          strokeColor: panelFg,
        }}
        textStyle={{
          font: "italic 24px comic",
          lineHeightAsPx: 28,
          fillStyle: panelFg,
          glitch: false,
        }}
        onDone={async () => {
          await sleep(2_000);
          page?.("next-fade");
        }}
      >
        {`
Uh... 
Maybe by accident?
      `.toUpperCase()}
      </CanvasSpeechBubble>
    </Canvas>
  );
};

const Scene4 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Canvas>
      <Video
        src={downloads.exploding}
        renderLeft={(-width * 2) / 2}
        renderTop={-height * 0.9}
        renderWidth={width * 3}
        renderHeight={height * 3}
      />
      <CanvasDithering palette={cybergumsPalette()} />
      <CanvasSpeechBubble
        pos={{
          left: 25,
          top: 400,
          width: 425,
          height: 75,
          pLeft: 250,
          pTop: 350,
          radius: 20,
        }}
        text={{
          rx: 25,
          ry: 30,
          speed: 50,
        }}
        style={{
          fillColor: panelBg,
          strokeColor: panelFg,
        }}
        textStyle={{
          font: "italic 24px comic",
          lineHeightAsPx: 28,
          fillStyle: panelFg,
          glitch: false,
        }}
        onDone={async () => {
          await sleep(2_000);
          //  page?.("next-fade");
        }}
      >
        {`
AGGGGGGHHHHHHHHHHHHHHHHH!!!
      `.toUpperCase()}
      </CanvasSpeechBubble>
    </Canvas>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {() => {
            downloads.theme.resource.stop();
            downloads.theme.resource.play({ loop: true });
          }}
        </ActionBlock>
        <Scene1 />
      </Panel>
      <Panel>
        <Scene2 />
      </Panel>
      <Panel>
        <Scene3 />
      </Panel>
      <Panel>
        <Scene4 />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
