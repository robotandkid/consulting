import {
  ActionBlock,
  Canvas,
  CanvasDithering,
  CanvasSpeechBubble,
  clement8Palette,
  Panel,
  sleep,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/06/19/theme.wav.mp3",
  },
  scream: {
    type: "sound",
    url: "/comics/2024/06/19/scream.mp3",
  },
  lawn: {
    type: "video",
    url: "/comics/2024/06/19/lawn.mp4",
  },
  fire: {
    type: "video",
    url: "/comics/2024/06/19/fire.mp4",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["#0D0D0D", "white"] as const;

const panelBg = "#ff79ae";
const panelFg = "#000000";

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Scene1 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.theme.resource.stop();
          downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <Video
        src={downloads.lawn}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <CanvasSpeechBubble
        pos={{
          left: 10,
          top: 300,
          width: 200,
          height: 80,
          pLeft: 10,
          pTop: 300,
          radius: 1,
        }}
        textStyle={{
          font: "italic 24px comic",
          lineHeightAsPx: 28,
          fillStyle: panelFg,
          glitch: false,
        }}
        style={{
          fillColor: panelBg,
        }}
        text={{ rx: 10, ry: 10 }}
      >
        {`
Anywhere,
Central Texas...
      `}
      </CanvasSpeechBubble>
      <CanvasSpeechBubble
        pos={{
          left: 10,
          top: 400,
          height: 100,
          width: 380,
          pLeft: 10,
          pTop: 400,
          radius: 1,
        }}
        textStyle={{
          font: "italic 24px comic",
          lineHeightAsPx: 28,
          fillStyle: panelFg,
          glitch: false,
        }}
        style={{
          fillColor: panelBg,
        }}
        text={{ rx: 10, ry: 10, speed: 10 }}
        onDone={async () => {
          await sleep(2_000);
          page?.("next-fade");
        }}
      >
        {`
When you look at the window,
it looks like a Disney movie.
But step outside...
`}
      </CanvasSpeechBubble>
      <CanvasDithering palette={clement8Palette()} />
    </Canvas>
  );
};

const Scene2 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [show, setShow] = useState(false);

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          downloads.scream.resource.play();
          await sleep(1_000);
          setShow(true);
        }}
      </ActionBlock>
      <Video
        src={downloads.fire}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <CanvasDithering palette={clement8Palette()} />
      {show && (
        <CanvasSpeechBubble
          pos={{
            left: 300,
            top: 120,
            height: 60,
            width: 160,
            pLeft: 290,
            pTop: 170,
            radius: 10,
          }}
          textStyle={{
            font: "italic 24px comic",
            lineHeightAsPx: 28,
            fillStyle: "#000000",
            glitch: false,
          }}
          style={{
            fillColor: "#fff982",
          }}
          text={{ rx: 20, ry: 20, speed: 10 }}
          onDone={async () => {
            await sleep(12_000);
            page?.("next-fade");
          }}
        >
          {`
IT BURNS!!!
`}
        </CanvasSpeechBubble>
      )}
    </Canvas>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {() => {
            downloads.theme.resource.stop();
            downloads.theme.resource.play({ loop: true });
          }}
        </ActionBlock>
        <Scene1 />
      </Panel>
      <Panel>
        <Scene2 />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
