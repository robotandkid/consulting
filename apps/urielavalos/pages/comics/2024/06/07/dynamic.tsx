import {
  ActionBlock,
  Canvas,
  CanvasImage,
  CanvasSpeechBubble,
  marioNesPalette,
  Panel,
  sleep,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  useInterpolate,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import React, { useState } from "react";
import { CanvasDithering } from "@robotandkid/sprite-motion";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/06/07/theme.mp3",
  },
  scene01: {
    type: "video",
    url: "/comics/2024/06/07/01.mp4",
  },
  scene02: {
    type: "image",
    url: "/comics/2024/06/07/02.png",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["#0D0D0D", "white"] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const SceneA = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <CanvasImage
        src={downloads.scene02}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <CanvasDithering palette={marioNesPalette()} />
      <CanvasSpeechBubble
        pos={{
          left: 10,
          top: 200,
          width: 470,
          height: 140,
          pLeft: 225,
          pTop: 160,
          radius: 20,
        }}
        text={{
          rx: 25,
          ry: 30,
          speed: 10,
        }}
        style={{
          fillColor: "#e8ff00",
        }}
        textStyle={{
          font: "italic 24px comic",
          lineHeightAsPx: 28,
          fillStyle: "#000",
          glitch: false,
        }}
        onDone={async () => {
          await sleep(2_000);
          page?.("next-fade");
        }}
      >
        {`
She has always been beautiful
but her constant sour disposition
robs me of enjoying her presence.
      `.toUpperCase()}
      </CanvasSpeechBubble>
    </Canvas>
  );
};

const SceneB = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const y = useInterpolate(-100, 0, {
    delta: 5,
    onDone: async () => {
      await sleep(5_000);
      page?.("next-fade");
    },
  });
  return (
    <Canvas>
      <Video
        src={downloads.scene01}
        renderTop={y}
        renderLeft={-150}
        renderWidth={2 * width}
        renderHeight={2 * height}
        loop={false}
      />
      <CanvasDithering palette={marioNesPalette()} />
    </Canvas>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {() => {
            downloads.theme.resource.stop();
            downloads.theme.resource.play({ loop: true });
          }}
        </ActionBlock>
        <SceneA />
      </Panel>
      <Panel>
        <SceneB />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
