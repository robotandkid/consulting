import {
  ActionBlock,
  Canvas,
  Panel,
  StaticImg,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  UiPlayerInfoPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";
import { Noise } from "../../../art/big-mac-murder/noise";

const resources = {
  robot1: {
    type: "image",
    url: "/comics/2024/02-28/robot1.nes.png",
  },
  synth1: {
    type: "sound",
    url: "/comics/2024/02-28/synth1.mp3",
  },
  robot2: {
    type: "video",
    url: "/comics/2024/02-28/robot2.mp4",
  },
  morning2: {
    type: "sound",
    url: "/comics/2024/02-28/morning2.mp3",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
// const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    // backgroundColor: oneBitColor[0],
    // color: oneBitColor[1],
    // "& > div": {
    //   backgroundColor: oneBitColor[0],
    //   borderColor: oneBitColor[1],
    //   color: oneBitColor[1],
    // },
    zIndex: 2,
    position: "absolute",
  },
});

const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
  base: {
    zIndex: 2,
    position: "absolute",
    // backgroundColor: oneBitColor[0],
    // color: oneBitColor[1],
    // "& > div": {
    //   backgroundColor: oneBitColor[0],
    //   borderColor: oneBitColor[1],
    //   color: oneBitColor[1],
    // },
  },
});

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    // backgroundColor: oneBitColor[0],
    // color: oneBitColor[1],
    // "& > div, & > h1": {
    //   backgroundColor: oneBitColor[0],
    //   borderColor: oneBitColor[1],
    //   color: oneBitColor[1],
    // },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  const [state, setState] = React.useState<
    "init" | "wipe" | "wipe2" | "dark-side" | "end"
  >("init");

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>{() => downloads.synth1.resource.play()}</ActionBlock>
          <Video
            loop={false}
            src={downloads.robot2}
            onEnd={() => setState("dark-side")}
          />
          <PlayerInfoPanel
            info={[
              { key: "Name", value: "Robot" },
              { key: "HP", value: "???/???" },
              { key: "Lvl", value: `???` },
            ]}
            width="100%"
            height="4rem"
            top={0}
          />
          {["dark-side"].includes(state) && (
            <DialogPanel
              visibleLines={3}
              width="100%"
              height="8rem"
              bottom="0"
              onDone={async () => {
                await sleep(1000);
                page?.("next-wipe");
              }}
            >
              {`RESISTANCE IS FUTILE HUMAN.
YOU WILL BE DESTROYED!`}
            </DialogPanel>
          )}
          <Noise time={3000} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <div
            style={{
              width: "100%",
              height: "100%",
              background: gbColor[0],
              zIndex: 2,
            }}
          ></div>
          {state !== "wipe" && (
            <DialogPanel
              visibleLines={3}
              width="60%"
              height="6rem"
              top="35%"
              onDone={async () => {
                await sleep(2000);
                page?.("next-fade");
              }}
            >
              {`12 hours earlier...`}
            </DialogPanel>
          )}
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async () => {
              downloads.synth1.resource.stop();
              downloads.morning2.resource.play();
            }}
          </ActionBlock>
          <StaticImg src={downloads.robot1} style={{ height: "90%" }} />
          <PlayerInfoPanel
            info={[
              { key: "Name", value: "Robot" },
              { key: "HP", value: "100/100" },
              { key: "Lvl", value: `45` },
            ]}
            width="100%"
            height="4rem"
            top={0}
          />
          <DialogPanel
            visibleLines={3}
            width="100%"
            height="8rem"
            bottom="0"
            onDone={async () => {
              await sleep(10_000);
              downloads.morning2.resource.stop();
            }}
          >
            {`(Time: 6 A.M.)
Hello human.
How can I help you?`}
          </DialogPanel>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: gbColor[0],
    // backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <VanillaButton>Start</VanillaButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
