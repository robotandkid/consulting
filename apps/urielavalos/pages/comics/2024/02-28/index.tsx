import dynamic from "next/dynamic";
import { ComicPlaceholder } from "../../../../src/styles/styles";
import { theme } from "../../../../src/styles/theme";

export const Comic2024_02_28 = dynamic(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports don't work
    import("./dynamic"),
  {
    ssr: true,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);
