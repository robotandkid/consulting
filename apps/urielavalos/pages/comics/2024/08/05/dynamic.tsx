import {
  CanvasDithering,
  CanvasSpeechBubble,
  Code,
  gameboyPalette,
  sleep,
} from "@robotandkid/sprite-motion";
import { usePagination } from "@robotandkid/sprite-motion";
import {
  ActionBlock,
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/08/05/theme.wav.mp3",
  },
  stop: {
    type: "sound",
    url: "/comics/2024/08/05/stop.mp3",
  },
  scream: {
    type: "sound",
    url: "/comics/2024/08/05/screaming.mp3",
  },
  video01: {
    type: "video",
    url: "/comics/2024/08/05/video01.mp4",
  },
  video02: {
    type: "video",
    url: "/comics/2024/08/05/video02.mp4",
  },
  video03: {
    type: "video",
    url: "/comics/2024/08/05/video03.mp4",
  },
  video04: {
    type: "video",
    url: "/comics/2024/08/05/video04.mp4",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = [gameboyPalette()[4], gameboyPalette()[0]] as const;

// const panelBg = "#fff7f8";
// const panelFg = "#000000";
// const palette = funkyFuture8Palette();

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const robotStyle = {
  fillColor: "#fff",
  strokeColor: "#000",
  strokeWidth: 5,
} as const;

const robotTextStyle = {
  fillStyle: "#000",
  font: "34px mrpixel,monorepo",
  lineHeightAsPx: 38,
  glitch: false,
} as const;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async () => {
              downloads.scream.resource.stop();
              downloads.theme.resource.stop();
              downloads.theme.resource.play({ loop: true });
            }}
          </ActionBlock>
          <Video
            src={downloads.video01}
            renderWidth={width}
            renderHeight={height}
            renderTop={0}
            renderLeft={0}
            loop={false}
            onEnd={() => page?.("next-fade")}
          />
          <CanvasSpeechBubble
            pos={{
              left: 25,
              top: 25,
              height: 115,
              radius: 25,
              width: 325,
              pLeft: 175,
              pTop: 175,
            }}
            text={{
              rx: 25,
              ry: 25,
            }}
            style={robotStyle}
            textStyle={robotTextStyle}
          >
            {`
We're gonna
record a tutorial...
          `}
          </CanvasSpeechBubble>
          <CanvasDithering palette={gameboyPalette()} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              await sleep(6_000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <Video
            src={downloads.video02}
            renderWidth={width}
            renderHeight={height}
            renderTop={0}
            renderLeft={0}
            loop
          />
          <CanvasSpeechBubble
            pos={{
              left: 25,
              top: 25,
              height: 150,
              radius: 25,
              width: 300,
              pLeft: 175,
              pTop: 200,
            }}
            text={{
              rx: 25,
              ry: 25,
            }}
            style={robotStyle}
            textStyle={robotTextStyle}
          >
            {`
When I give this 
hand signal, you
stop recording.
          `}
          </CanvasSpeechBubble>
          <CanvasSpeechBubble
            pos={{
              left: 250,
              top: 400,
              height: 75,
              radius: 25,
              width: 200,
              pLeft: 275,
              pTop: 375,
            }}
            text={{
              rx: 25,
              ry: 25,
            }}
            style={robotStyle}
            textStyle={robotTextStyle}
          >
            {`
OK? Go!
          `}
          </CanvasSpeechBubble>

          <CanvasDithering palette={gameboyPalette()} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasSpeechBubble
            pos={{
              left: 0,
              top: 0,
              height,
              radius: 0,
              width,
            }}
            text={{
              rx: 50,
              ry: 100,
              speed: 100,
            }}
            style={{ ...robotStyle, fillColor: gameboyPalette()[4] }}
            textStyle={{
              ...robotTextStyle,
              fillStyle: gameboyPalette()[0],
              font: "bold 92px mrpixel,monorepo",
              lineHeightAsPx: 128,
            }}
            onDone={() => page?.("next-fade")}
          >
            {`
A Few
Moments
Later...
          `}
          </CanvasSpeechBubble>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              await sleep(4_000);
              if (opts.isBlockCancelled) return;
              downloads.theme.resource.stop();
              downloads.stop.resource.play();
              page?.("next-fade");
            }}
          </ActionBlock>
          <Video
            src={downloads.video03}
            renderWidth={width}
            renderHeight={height}
            renderTop={0}
            renderLeft={0}
            loop={false}
          />
          <CanvasSpeechBubble
            pos={{
              left: 3,
              top: 3,
              height: 125,
              radius: 25,
              width: 425,
              pLeft: 175,
              pTop: 165,
            }}
            text={{
              rx: 25,
              ry: 25,
            }}
            style={robotStyle}
            textStyle={robotTextStyle}
          >
            {`
Thanks for watching!
Don't forget to subscribe.

          `}
          </CanvasSpeechBubble>
          <CanvasDithering palette={gameboyPalette()} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              downloads.scream.resource.play();
              await sleep(4_000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <Video
            src={downloads.video04}
            renderWidth={width}
            renderHeight={height}
            renderTop={0}
            renderLeft={0}
            loop={false}
          />
          <CanvasSpeechBubble
            pos={{
              left: 3,
              top: 3,
              height: 75,
              radius: 25,
              width: 450,
              pLeft: 175,
              pTop: 90,
            }}
            text={{
              rx: 25,
              ry: 25,
            }}
            style={robotStyle}
            textStyle={robotTextStyle}
          >
            {`
WHY DIDN'T YOU STOP?!!!
          `}
          </CanvasSpeechBubble>
          <CanvasSpeechBubble
            pos={{
              left: 20,
              top: 425,
              height: 75,
              radius: 0,
              width: 460,
            }}
            text={{
              rx: 25,
              ry: 25,
            }}
            style={robotStyle}
            textStyle={robotTextStyle}
          >
            {`
Relax, we can always edit...
          `}
          </CanvasSpeechBubble>
          <CanvasDithering palette={gameboyPalette()} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {() => {
              downloads.scream.resource.play();
            }}
          </ActionBlock>
          <Video
            src={downloads.video04}
            renderWidth={width * 1.5}
            renderHeight={height * 1.5}
            renderTop={0}
            renderLeft={-125}
            loop={false}
            onEnd={() => page?.("next-fade")}
          />
          <Code
            left={125}
            top={0}
            style={{
              ...robotTextStyle,
              fillStyle: gameboyPalette()[4],
              font: "bold 42px mrpixel,monorepo",
            }}
          >
            {`
ARGHHHH!!!
          `}
          </Code>
          <CanvasDithering palette={gameboyPalette()} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {() => {
              downloads.scream.resource.play();
            }}
          </ActionBlock>
          <Video
            src={downloads.video04}
            renderWidth={width * 2}
            renderHeight={height * 2}
            renderTop={-175}
            renderLeft={-250}
            loop={false}
            onEnd={() => page?.("next-fade")}
          />
          <Code
            left={125}
            top={0}
            style={{
              ...robotTextStyle,
              fillStyle: gameboyPalette()[0],
              font: "bold 42px mrpixel,monorepo",
            }}
          >
            {`
ARGHHHH!!!
          `}
          </Code>
          <CanvasDithering palette={gameboyPalette()} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: "black",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recognize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
