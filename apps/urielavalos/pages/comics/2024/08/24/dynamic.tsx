import {
  ActionBlock,
  Canvas,
  CanvasSpeechBubble,
  Panel,
  StoryBoard,
  UiPanel,
  Video,
  World,
  sleep,
  useDownloadsCtx,
  usePagination,
  useStateSteps,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { ComponentProps, useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/08/24/theme.wav.mp3",
  },
  video01: {
    type: "video",
    url: "/comics/2024/08/24/video01.s.mp4",
  },
  video02: {
    type: "video",
    url: "/comics/2024/08/24/video02.s.mp4",
  },
  video03: {
    type: "video",
    url: "/comics/2024/08/24/video03.s.mp4",
  },
  video04: {
    type: "video",
    url: "/comics/2024/08/24/video04.s.mp4",
  },
  video05: {
    type: "video",
    url: "/comics/2024/08/24/video05.s.mp4",
  },
  video06: {
    type: "video",
    url: "/comics/2024/08/24/video06.s.mp4",
  },
  video07: {
    type: "video",
    url: "/comics/2024/08/24/video07.s.mp4",
  },
} as const;

const height = 400;
const width = (height * 1280) / 768;

const dark = "#ff0000";
const light = "#fff";
const oneBitColor = [dark, light] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const darkTextStyle: Pick<
  ComponentProps<typeof CanvasSpeechBubble>,
  "text" | "style" | "textStyle"
> = {
  text: {
    rx: 25,
    ry: 25,
  },
  style: {
    fillColor: "#ffffff00",
    strokeColor: "#ffffff00",
    strokeWidth: 5,
  },
  textStyle: {
    fillStyle: dark,
    font: "bold 36px helvetica,arial,sans-serif",
    lineHeightAsPx: 40,
    glitch: false,
    shadowColor: light,
    shadowBlur: 0,
    shadowOffsetX: 1,
    shadowOffsetY: 1,
  },
} as const;

const lightTextStyle: Pick<
  ComponentProps<typeof CanvasSpeechBubble>,
  "text" | "style" | "textStyle"
> = {
  text: {
    rx: 25,
    ry: 25,
  },
  style: {
    fillColor: "#ffffff00",
    strokeColor: "#ffffff00",
    strokeWidth: 5,
  },
  textStyle: {
    fillStyle: light,
    font: "bold 36px helvetica,arial,sans-serif",
    lineHeightAsPx: 40,
    glitch: false,
    shadowColor: dark,
    shadowBlur: 2,
    shadowOffsetX: 2,
    shadowOffsetY: 4,
  },
} as const;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [Step, , nextStep, , reset] = useStateSteps({
    steps: ["1", "2", "3", "4", "5", "6", "7", "8"],
  });

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas disableClearCanvasBeforeUpdate>
          <ActionBlock>
            {async () => {
              downloads.theme.resource.stop();
              downloads.theme.resource.play({ loop: true });
            }}
          </ActionBlock>
          <Step step="1">
            <Video
              src={downloads.video01}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
              onEnd={nextStep}
            />
          </Step>
          <Step step="2">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                nextStep();
              }}
            </ActionBlock>
            <Video
              src={downloads.video02}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 375,
                top: 250,
                height: 100,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...darkTextStyle}
            >
              {`
Hey Pooper, 
do you want to
play with me?
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="3">
            <Video
              src={downloads.video03}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
              onEnd={nextStep}
            />
          </Step>
          <Step step="4">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                nextStep();
              }}
            </ActionBlock>
            <CanvasSpeechBubble
              pos={{
                left: 100,
                top: 50,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...lightTextStyle}
              textStyle={{
                ...lightTextStyle.textStyle,
                font: "bold 102px helvetica,arial,sans-serif",
                lineHeightAsPx: 102 * 1.2,
              }}
            >
              {`
30 years
later...
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="5">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                nextStep();
              }}
            </ActionBlock>
            <Video
              src={downloads.video04}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 120,
                top: 300,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...lightTextStyle}
            >
              {`
I miss the little boy.
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="6">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                nextStep();
              }}
            </ActionBlock>
            <Video
              src={downloads.video05}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 120,
                top: 290,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...lightTextStyle}
            >
              {`
Pooper got on a plane and
went to visit the little boy.
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="7">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                nextStep();
              }}
            </ActionBlock>
            <Video
              src={downloads.video06}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 400,
                top: 25,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...darkTextStyle}
            >
              {`
Hey pooper,
I have a 
family now!
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="8">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                reset();
                page?.("next-fade");
              }}
            </ActionBlock>
            <Video
              src={downloads.video07}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 40,
                top: 325,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...lightTextStyle}
            >
              {`
My son has a little pooper too!
          `}
            </CanvasSpeechBubble>
          </Step>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: "black",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recognize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
