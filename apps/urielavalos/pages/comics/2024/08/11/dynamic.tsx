import {
  ActionBlock,
  Canvas,
  CanvasDithering,
  CanvasSpeechBubble,
  Panel,
  StoryBoard,
  UiPanel,
  Video,
  World,
  palette5origamiClouds,
  sleep,
  useDownloadsCtx,
  usePagination,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { ComponentProps, useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/08/11/theme.wav.mp3",
  },
  video01: {
    type: "video",
    url: "/comics/2024/08/11/video01.mp4",
  },
  video02: {
    type: "video",
    url: "/comics/2024/08/11/video02.mp4",
  },
  video03: {
    type: "video",
    url: "/comics/2024/08/11/video03.mp4",
  },
} as const;

const width = 512;
const height = 512;

const palette = palette5origamiClouds();
const dark = palette[4];
const light = palette[1];
const oneBitColor = [dark, light] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

// const darkTextStyle: Pick<
//   ComponentProps<typeof CanvasSpeechBubble>,
//   "text" | "style" | "textStyle"
// > = {
//   text: {
//     rx: 25,
//     ry: 25,
//   },
//   style: {
//     fillColor: "#ffffff00",
//     strokeColor: "#ffffff00",
//     strokeWidth: 5,
//   },
//   textStyle: {
//     fillStyle: dark,
//     font: "36px mrpixel,monorepo",
//     lineHeightAsPx: 40,
//     glitch: false,
//     shadowColor: light,
//     shadowBlur: 0,
//     shadowOffsetX: 1,
//     shadowOffsetY: 1,
//   },
// } as const;

const lightTextStyle: Pick<
  ComponentProps<typeof CanvasSpeechBubble>,
  "text" | "style" | "textStyle"
> = {
  text: {
    rx: 25,
    ry: 25,
  },
  style: {
    fillColor: "#ffffff00",
    strokeColor: "#ffffff00",
    strokeWidth: 5,
  },
  textStyle: {
    fillStyle: light,
    font: "36px mrpixel,monorepo",
    lineHeightAsPx: 40,
    glitch: false,
    shadowColor: dark,
    shadowBlur: 2,
    shadowOffsetX: 2,
    shadowOffsetY: 4,
  },
} as const;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async () => {
              downloads.theme.resource.stop();
              downloads.theme.resource.play({ loop: true });
            }}
          </ActionBlock>
          <Video
            src={downloads.video01}
            renderWidth={width}
            renderHeight={height}
            renderTop={0}
            renderLeft={0}
            loop={false}
            onEnd={() => page?.("next-fade")}
          />
          <CanvasSpeechBubble
            pos={{
              left: 0,
              top: 300,
              height: 115,
              radius: 25,
              width: 325,
            }}
            {...lightTextStyle}
          >
            {`
Look at what the 
birds are doing...
          `}
          </CanvasSpeechBubble>
          <CanvasDithering palette={palette} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              await sleep(4_000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <Video
            src={downloads.video02}
            renderWidth={width}
            renderHeight={height}
            renderTop={0}
            renderLeft={0}
            loop
          />
          <CanvasSpeechBubble
            pos={{
              left: 150,
              top: 450,
              height: 150,
              radius: 25,
              width: 300,
              pLeft: 175,
              pTop: 200,
            }}
            {...lightTextStyle}
          >
            {`
CRASH!!!
          `}
          </CanvasSpeechBubble>
          <CanvasDithering palette={palette} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              await sleep(6_000);
              if (opts.isBlockCancelled) return;
              // page?.("next-fade");
            }}
          </ActionBlock>
          <Video
            src={downloads.video03}
            renderWidth={width * 1.1}
            renderHeight={height * 1.1}
            renderTop={-25}
            renderLeft={-25}
            loop={false}
          />
          <CanvasSpeechBubble
            pos={{
              left: 250,
              top: -20,
              height: 150,
              radius: 25,
              width: 300,
              pLeft: 175,
              pTop: 200,
            }}
            {...lightTextStyle}
          >
            {`
There was 
a door there...
I knew that.
          `}
          </CanvasSpeechBubble>
          <CanvasDithering palette={palette} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: "black",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recognize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
