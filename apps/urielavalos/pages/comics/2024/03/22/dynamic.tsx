import {
  ActionBlock,
  Canvas,
  Noise,
  Panel,
  StaticImg,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/03/22/down-theme.mp3",
  },
  scene01: {
    type: "video",
    url: "/comics/2024/03/22/scene-01-nes.mp4",
  },
  scene02a: {
    type: "image",
    url: "/comics/2024/03/22/scene-02a.nes.png",
  },
  scene02b: {
    type: "image",
    url: "/comics/2024/03/22/scene-02b.nes.png",
  },
  scene02c: {
    type: "image",
    url: "/comics/2024/03/22/scene-02c.nes.png",
  },
  scene02d: {
    type: "image",
    url: "/comics/2024/03/22/scene-02d.nes.png",
  },
  scene02e: {
    type: "image",
    url: "/comics/2024/03/22/scene-02e.nes.png",
  },
  scene02f: {
    type: "image",
    url: "/comics/2024/03/22/scene-02f.nes.png",
  },
  scene03a: {
    type: "video",
    url: "/comics/2024/03/22/scene-03a.nes.mp4",
  },
  scene03b: {
    type: "video",
    url: "/comics/2024/03/22/scene-03b.nes.mp4",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [state, setState] = React.useState<"init" | "final" | "blur" | "end">(
    "init"
  );

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>{() => downloads.theme.resource.play()}</ActionBlock>
          <Video src={downloads.scene01} />
          <DialogPanel
            visibleLines={3}
            width="100%"
            height="8rem"
            bottom="0"
            onDone={async () => {
              await sleep(1000);
              page?.("next-fade");
            }}
          >
            {`You stupid idiot
why did I marry a child
I swear you are an idiot...`}
          </DialogPanel>
          <Noise time={3000} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <StaticImg src={downloads.scene02a} width={width} height={height} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <StaticImg src={downloads.scene02b} width={width} height={height} />
          <DialogPanel visibleLines={3} width="100%" height="8rem" bottom="0">
            {`Many...`}
          </DialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <StaticImg src={downloads.scene02c} width={width} height={height} />
          <DialogPanel visibleLines={3} width="100%" height="8rem" bottom="0">
            {`years...`}
          </DialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <StaticImg src={downloads.scene02d} width={width} height={height} />
          <DialogPanel visibleLines={3} width="100%" height="8rem" bottom="0">
            {`later...`}
          </DialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <StaticImg src={downloads.scene02e} width={width} height={height} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <StaticImg src={downloads.scene02f} width={width} height={height} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          {state === "init" && (
            <Video
              src={downloads.scene03a}
              loop={false}
              onEnd={() => setState("final")}
            />
          )}
          {["final", "blur", "end"].includes(state) && (
            <>
              <Video
                src={downloads.scene03b}
                loop={false}
                onEnd={() => setState("end")}
              />
              <Noise time={11_000} effect="fadeOut" />
            </>
          )}
          {state === "final" && (
            <DialogPanel visibleLines={3} width="100%" height="8rem" bottom="0">
              {`oh yea boi...
I'm finally alone`}
            </DialogPanel>
          )}
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: gbColor[0],
    // backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <VanillaButton>Start</VanillaButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
