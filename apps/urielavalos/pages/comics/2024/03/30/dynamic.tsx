import {
  ActionBlock,
  Ascii,
  Canvas,
  CanvasImage,
  Code,
  Noise,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/03/30/self-portrait.mp3",
  },
  portrait1: {
    type: "video",
    url: "/comics/2024/03/30/portrait-01.mp4",
  },
  portrait2: {
    type: "video",
    url: "/comics/2024/03/30/portrait-02.mp4",
  },
  portrait3: {
    type: "image",
    url: "/comics/2024/03/30/portrait-03.jpg",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  // const [state, setState] = React.useState<"init" | "final" | "blur" | "end">(
  //   "init"
  // );

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <Code
            left={10}
            top={50}
            speed={10}
            style={{
              font: "bold 16px monospace",
              fillStyle: "#80ff00",
              textBaseline: "top",
              lineHeightAsPx: 16,
            }}
            onDone={async () => {
              await sleep(2_500);
              page?.("next-fade");
            }}
          >
            {`$ ls life_stream

drwx------@  8 robot   256B Mar 30 08:45 .
drwxr-xr-x  18 robot   576B Mar 30 08:42 ..
-rw-rw-r--@  1 robot   100PB Mar 26 08:34 01.life
-rw-rw-r--@  1 robot   105PB Mar 30 08:34 02.life
-rw-rw-r--@  1 robot   102PB Mar 30 08:34 03.life

$ open life_stream/01.life`}
          </Code>
          <Noise time={1000} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async () => {
              downloads.theme.resource.play();
            }}
          </ActionBlock>
          <Video
            src={downloads.portrait1}
            loop={false}
            onEnd={() => {
              page?.("next-fade");
            }}
          />
          <Ascii
            resolutionX={240}
            resolutionY={120}
            style={{
              fontFamily: "monospace",
              fillStyle: "#80ff00",
            }}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <Code
            left={10}
            top={50}
            speed={10}
            style={{
              font: "bold 16px monospace",
              fillStyle: "#80ff00",
              textBaseline: "top",
              lineHeightAsPx: 16,
            }}
            onDone={async () => {
              await sleep(2_500);
              page?.("next-fade");
            }}
          >
            {`$ open life_stream/02.life`}
          </Code>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <Video
            src={downloads.portrait2}
            loop={false}
            onEnd={() => {
              page?.("next-fade");
            }}
          />
          <Ascii
            resolutionX={240}
            resolutionY={120}
            style={{
              fontFamily: "monospace",
              fillStyle: "#80ff00",
            }}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <Code
            left={10}
            top={50}
            speed={10}
            style={{
              font: "bold 16px monospace",
              fillStyle: "#80ff00",
              textBaseline: "top",
              lineHeightAsPx: 16,
            }}
            onDone={async () => {
              await sleep(2_500);
              page?.("next-fade");
            }}
          >
            {`$ open life_stream/03.life`}
          </Code>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              await sleep(4000);
              if (opts.isBlockCancelled) return;
              page?.("next-fade");
            }}
          </ActionBlock>
          <CanvasImage
            src={downloads.portrait3}
            renderLeft={50}
            renderTop={0}
            renderWidth={384}
            renderHeight={512}
          />
          <Ascii
            resolutionX={240}
            resolutionY={120}
            style={{
              fontFamily: "monospace",
              fillStyle: "#80ff00",
            }}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: gbColor[0],
    // backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
