import {
  ActionBlock,
  Canvas,
  Noise,
  Panel,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  chant: {
    type: "sound",
    url: "/comics/2024/03/04/chant.mp3",
  },
  levelUp: {
    type: "sound",
    url: "/comics/2024/03/04/level-up.mp3",
  },
  theme: {
    type: "sound",
    url: "/comics/2024/03/04/theme.mp3",
  },
  start: {
    type: "video",
    url: "/comics/2024/03/04/start-02-bw.mp4",
  },
  middle: {
    type: "video",
    url: "/comics/2024/03/04/middle-bw.mp4",
  },
  end: {
    type: "video",
    url: "/comics/2024/03/04/end-bw.mp4",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const Start = (props: { onEnd: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Video
      src={downloads.start}
      renderWidth={512}
      renderHeight={512}
      renderLeft={0}
      renderTop={0}
      playbackRate={0.5}
      loop={false}
      onEnd={() => {
        props.onEnd();
      }}
    />
  );
};

const End = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [y, setY] = React.useState(0);

  React.useEffect(() => {
    const update = setInterval(
      () =>
        setY((x) => {
          if (x > 550) {
            clearInterval(update);
            return x;
          }
          return x + 10;
        }),
      100
    );

    return () => {
      clearInterval(update);
    };
  }, []);

  return (
    <Video
      src={downloads.end}
      renderWidth={512}
      renderHeight={1536}
      renderLeft={0}
      renderTop={-900 + y}
    />
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>{() => downloads.chant.resource.play()}</ActionBlock>
          <Start
            onEnd={() => {
              downloads.levelUp.resource.play();
            }}
          />
          <DialogPanel
            visibleLines={3}
            width="100%"
            height="8rem"
            bottom="0"
            onDone={async () => {
              await sleep(1000);
              page?.("next-fade");
            }}
          >
            {`Those who worship technology
will be ruled by a false AI god...`}
          </DialogPanel>
          <Noise time={3000} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {() => {
              downloads.chant.resource.stop();
              downloads.theme.resource.play();
            }}
          </ActionBlock>

          <Video
            loop={false}
            src={downloads.middle}
            onEnd={() => {
              page?.("next-fade");
            }}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <End />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: gbColor[0],
    // backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <VanillaButton>Start</VanillaButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
