import {
  ActionBlock,
  Canvas,
  CanvasImage,
  Noise,
  Panel,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  useInterpolate,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useEffect, useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  click: {
    type: "sound",
    url: "/comics/2024/03/26/click-effect.mp3",
  },
  explosion: {
    type: "sound",
    url: "/comics/2024/03/26/explosion-effect.mp3",
  },
  introTheme: {
    type: "sound",
    url: "/comics/2024/03/26/intro.mp3",
  },
  intro: {
    type: "video",
    url: "/comics/2024/03/26/intro.1bit.mp4",
  },
  scene0101: {
    type: "image",
    url: "/comics/2024/03/26/scene-01-01.1bit.png",
  },
  scene0102: {
    type: "video",
    url: "/comics/2024/03/26/scene-01-02.1bit.mp4",
  },
  scene0103: {
    type: "video",
    url: "/comics/2024/03/26/scene-01-03.1bit.mp4",
  },
  scene0104: {
    type: "video",
    url: "/comics/2024/03/26/scene-01-04.1bit.mp4",
  },
  scene0105: {
    type: "video",
    url: "/comics/2024/03/26/scene-01-05.1bit.mp4",
  },
  scene0106: {
    type: "video",
    url: "/comics/2024/03/26/scene-01-06.1bit.mp4",
  },
  scene01: {
    type: "sound",
    url: "/comics/2024/03/26/scene01.mp3",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["black", "white"] as const;
//const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
  variants: {
    size: {
      large: {
        "& > div": {
          backgroundColor: oneBitColor[0],
          borderColor: oneBitColor[1],
          color: oneBitColor[1],
          fontSize: "28px",
        },
      },
      normal: {},
    },
  },
});

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
  variants: {
    size: {
      default: {},
      intro: {
        "& > div, & > h1": {
          padding: "0.5rem",
          width: "12rem",
          fontSize: "40px",
        },
      },
    },
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Intro = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [state, setState] = React.useState<"init" | "title">("init");
  const page = usePagination();
  return (
    <>
      <ActionBlock>{() => downloads.introTheme.resource.play()}</ActionBlock>
      <Video
        src={downloads.intro}
        loop={false}
        onEnd={() => setState("title")}
        renderTop={-5}
        renderLeft={-5}
        renderWidth={width + 20}
        renderHeight={height + 20}
      />
      {state === "title" && (
        <>
          <div
            style={{
              color: oneBitColor[1],
              fontFamily: "mrpixel",
              fontSize: "75px",
              fontWeight: "bold",
              zIndex: 2,
              position: "absolute",
              top: 175,
              left: 15,
            }}
          >
            The Portal
          </div>
          <CustomUiPanel
            top={250}
            left={95}
            title=""
            onClick={() => {
              downloads.click.resource.play();
              page?.("next-fade");
            }}
            size="intro"
          >
            <StartButton anim="blink">Press Start</StartButton>
          </CustomUiPanel>
        </>
      )}
    </>
  );
};

const Scene01a = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [y, setY] = useState(0);
  const page = usePagination();

  React.useEffect(() => {
    const down = setInterval(
      () =>
        setY((c) => {
          if (c - 5 < -(768 - 512)) {
            page?.("next-fade");
            clearInterval(down);
            return c;
          }
          return c - 5;
        }),
      100
    );
    return () => {
      clearInterval(down);
    };
  }, [page]);

  return (
    <>
      <CanvasImage
        src={downloads.scene0101}
        renderTop={y}
        renderLeft={0}
        renderWidth={width}
        renderHeight={(width * 768) / 512}
      />
      <DialogPanel
        visibleLines={3}
        width="100%"
        size="large"
      >{`Eons ago...`}</DialogPanel>
    </>
  );
};

const Scene01b = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas width={width} height={height}>
      <Video
        src={downloads.scene0102}
        loop={false}
        onEnd={async () => {
          await sleep(1_000);
          page?.("next-fade");
        }}
      />
      <DialogPanel
        visibleLines={2}
        width="100%"
        size="large"
      >{`...a long forgotten civilization
opened a portal to the beyond...`}</DialogPanel>
    </Canvas>
  );
};

const Scene01c = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas width={width} height={height}>
      <Video src={downloads.scene0103} loop={false} />
      <DialogPanel
        visibleLines={2}
        width="100%"
        size="large"
        onDone={async () => {
          await sleep(1_000);
          page?.("next-fade");
        }}
      >{`...but they unleashed a horror...`}</DialogPanel>
    </Canvas>
  );
};

const Scene01d = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.explosion.resource.play();
        }}
      </ActionBlock>
      <Video src={downloads.scene0105} loop={false} />
      <DialogPanel
        visibleLines={2}
        width="100%"
        size="large"
        onDone={async () => {
          await sleep(1_000);
          page?.("next-fade");
        }}
      >{`They were left with only one 
option: the doomsday device...`}</DialogPanel>
    </Canvas>
  );
};

const Scene01End = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const y = useInterpolate(-1000, -250, { delta: 10 });

  useEffect(() => {
    if (y === -250) {
      const timeout = setTimeout(() => {
        page?.("next-wipe");
      }, 5_000);
      return () => {
        clearTimeout(timeout);
      };
    }
  }, [page, y]);

  return (
    <Canvas>
      <Video
        src={downloads.scene0106}
        renderLeft={0}
        renderTop={y}
        renderWidth={512}
        renderHeight={2560}
      />
      <DialogPanel
        visibleLines={2}
        width="100%"
        size="large"
      >{`Unfortunately, while the device 
destroyed their civilization... 
the explosion did not destroy 
the portal...`}</DialogPanel>
    </Canvas>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  // const page = usePagination();
  // const [state, setState] = React.useState<"init" | "final" | "blur" | "end">(
  //   "init"
  // );

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <Intro />
          <Noise time={1000} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>{() => downloads.scene01.resource.play()}</ActionBlock>
          <Scene01a />
        </Canvas>
      </Panel>
      <Panel>
        <Scene01b />
      </Panel>
      <Panel>
        <Scene01c />
      </Panel>
      <Panel>
        <Scene01d />
      </Panel>
      <Panel>
        <Scene01End />
      </Panel>
      <Panel>
        <DialogPanel
          visibleLines={2}
          width="100%"
          size="large"
        >{`The end?`}</DialogPanel>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: gbColor[0],
    // backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
