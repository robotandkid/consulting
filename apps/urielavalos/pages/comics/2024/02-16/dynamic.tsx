import {
  ActionBlock,
  Canvas,
  OvalIris,
  Panel,
  SpriteSheet,
  StaticImg,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  useKeyListeners,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useRef } from "react";
import { useEffect, useState } from "react";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  dad: {
    type: "image",
    url: "/comics/2024/02-16/robot-dad.png",
  },
  kid: {
    type: "image",
    url: "/comics/2024/02-16/robot-kid.png",
  },
  room: {
    type: "image",
    url: "/comics/2024/02-16/room-1bit.png",
  },
  theme: {
    type: "sound",
    url: "/comics/2024/02-16/theme.mp3",
  },
  night: {
    type: "video",
    url: "/comics/2024/02-13/starry-night.mp4",
  },
  furby: {
    type: "video",
    url: "/comics/2024/02-16/furby.mp4",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
    },
  },
});

const CustomPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
    },
  },
});

const Kid = (props: { onTouchDad: () => void }) => {
  const { onTouchDad } = props;
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [x, setX] = useState(100);

  useKeyListeners({
    disabled: x > 255,
    onKeyDown(key) {
      if (key === "ArrowLeft") setX((x) => x - 12);
      if (key === "ArrowRight") setX((x) => x + 12);
    },
  });

  const done = useRef(false);
  useEffect(() => {
    if (done.current) return;

    if (x > 255) {
      onTouchDad();
      done.current = true;
    }
  }, [onTouchDad, x]);

  return (
    <SpriteSheet
      src={downloads.kid}
      renderLeft={x}
      renderTop={240}
      frameWidth={24}
      frameHeight={23}
      renderWidth={75}
      renderHeight={75}
      fps={12}
      loop
    />
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [state, setState] = React.useState<"init" | "talk" | "wipe">("init");

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {() => {
              downloads.theme.resource.play();
            }}
          </ActionBlock>
          <StaticImg src={downloads.room} width={width} height={height} />
          <Kid onTouchDad={() => setState("talk")} />
          <SpriteSheet
            src={downloads.dad}
            renderLeft={300}
            renderTop={215}
            renderWidth={90}
            renderHeight={95}
            frameWidth={24}
            frameHeight={25}
            fps={12}
          />
          {["talk"].includes(state) && (
            <DialogPanel
              visibleLines={3}
              zIndex={2}
              width="100%"
              height="8rem"
              position="absolute"
              bottom="0"
              onDone={async () => {
                await sleep(1000);
                setState("wipe");
              }}
            >
              {`* Kid: Wanna check out my world?
  It took me all afternoon to make.              
* Dad: sure`}
            </DialogPanel>
          )}
          {state === "wipe" && (
            <OvalIris
              color={oneBitColor[0]}
              durationInMs={1000}
              onEnd={async () => {
                page?.("next-fade");
              }}
            />
          )}
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <Video src={downloads.night} loop />
          <DialogPanel
            visibleLines={3}
            zIndex={2}
            width="100%"
            height="8rem"
            position="absolute"
            bottom="0"
            onDone={async () => {
              await sleep(2000);
              page?.("next-fade");
            }}
          >
            {`* Dad: Wow, this level is so realistic!
* Kid: It's not a level it's a world.
* Dad: What's it about?`}
          </DialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <Video src={downloads.furby} loop />
          <DialogPanel
            visibleLines={3}
            zIndex={2}
            width="100%"
            height="8rem"
            position="absolute"
            bottom="0"
          >
            {`* Kid: You're an artist trying to
sell art on Farcaster.`}
          </DialogPanel>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomPanel
          title="GAME (1)"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <VanillaButton>Start</VanillaButton>
        </CustomPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
