import {
  ActionBlock,
  Canvas,
  Panel,
  StoryBoard,
  UiCommandPanel,
  UiDialogPanel,
  UiPanel,
  UiPlayerInfoPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  man: {
    type: "video",
    url: "/comics/2024/01-14/01-robot-gb.mp4",
  },
  nurse: {
    type: "video",
    url: "/comics/2024/01-14/02-nurse-gb.mp4",
  },
  pouringWater: {
    type: "video",
    url: "/comics/2024/01-14/03-pouring-water-gb.mp4",
  },
  nurseSprayed: {
    type: "video",
    url: "/comics/2024/01-14/04-nurse-sprayed-gb.mp4",
  },
  change: {
    type: "sound",
    url: "/comics/2024/01-14/coin-collect.mp3",
  },
  itemAcquired: {
    type: "sound",
    url: "/comics/2024/01-14/item.mp3",
  },
  alarmSound: {
    type: "sound",
    url: "/comics/2024/01-14/alarm.mp3",
  },
  overflowSound: {
    type: "sound",
    url: "/comics/2024/01-14/overflow.mp3",
  },
  loseLevel: {
    type: "sound",
    url: "/comics/2024/01-14/lose-level.mp3",
  },
  theme: {
    type: "sound",
    url: "/comics/2024/01-14/theme.mp3",
  },
  ending: {
    type: "sound",
    url: "/comics/2024/01-14/ending.mp3",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;

const PlayerInfoPanel = (props: { level: number }) => (
  <UiPlayerInfoPanel
    info={[
      { key: "Name", value: "Robot" },
      { key: "Lvl", value: `${props.level}` },
      { key: "HP", value: "80" },
      { key: "Exp", value: "30" },
    ]}
    zIndex={2}
    width="100%"
    height="4rem"
    position="absolute"
    bottom={0}
  />
);

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [state, setState] = useState<
    "start" | "wait-for-command" | "nurse-sprayed" | "lose-level"
  >();
  const navigation = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel aria-label="start">
        <Canvas width={width} height={height}>
          <>
            <ActionBlock>
              {() => {
                downloads.theme.resource.play();
              }}
            </ActionBlock>
            <Video
              src={downloads.man}
              loop={false}
              onEnd={() => setState("start")}
            />
            <PlayerInfoPanel level={1} />
            {["start", "wait-for-command"].includes(state ?? "") && (
              <UiDialogPanel
                visibleLines={3}
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore 🤷
                width="100%"
                height="8rem"
                zIndex={2}
                position="absolute"
                bottom="5rem"
                onDone={() => setState("wait-for-command")}
              >
                You need to use the bathroom.
              </UiDialogPanel>
            )}
            {["wait-for-command"].includes(state ?? "") && (
              <div
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  zIndex: 2,
                  width: "60%",
                  height: "8rem",
                }}
              >
                <UiCommandPanel
                  width="100%"
                  height="100%"
                  commands={["Talk", "Use", "Check", "Auto", "Guard", "Run"]}
                  subCommands={{
                    Talk: { type: "cmd", commands: ["Nurse"] },
                    Run: { type: "dialog", text: "You can't leave the bed." },
                    Use: undefined,
                    Check: undefined,
                    Auto: undefined,
                    Guard: undefined,
                  }}
                  onCommandChange={() => downloads.change.resource.play()}
                  onSubCommandChange={() => downloads.change.resource.play()}
                  onDone={(value) => {
                    if (value === "Talk") navigation?.("next-fade");
                  }}
                />
              </div>
            )}
          </>
        </Canvas>
      </Panel>
      <Panel aria-label="Say hello to the nurse">
        <Canvas width={width} height={height}>
          <Video
            src={downloads.nurse}
            loop={false}
            // onEnd={() => setState("nurse")}
          />
          <UiPlayerInfoPanel
            info={[
              { key: "Name", value: "Nurse" },
              { key: "Lvl", value: "1" },
              { key: "HP", value: "100" },
              { key: "Exp", value: "90" },
            ]}
            zIndex={2}
            width="100%"
            height="4rem"
            position="absolute"
            bottom={0}
          />
          <UiDialogPanel
            visibleLines={3}
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore 🤷
            width="100%"
            height="8rem"
            zIndex={2}
            position="absolute"
            bottom="5rem"
            onDone={async () => {
              downloads.itemAcquired.resource.play();
              await sleep(1000);
              navigation?.("next-fade");
            }}
          >
            {`* If you need to use the bathroom, 
  use this portable urinal.

Nurse gives you a PORTABLE URINAL.`}
          </UiDialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <>
            <Video
              src={downloads.man}
              loop={false}
              onEnd={() => setState("wait-for-command")}
            />
            <PlayerInfoPanel level={1} />
            {["wait-for-command"].includes(state ?? "") && (
              <div
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  zIndex: 2,
                  width: "60%",
                  height: "8rem",
                }}
              >
                <UiCommandPanel
                  width="100%"
                  height="100%"
                  commands={["Talk", "Use", "Check", "Auto", "Guard", "Run"]}
                  subCommands={{
                    Talk: undefined,
                    Run: { type: "dialog", text: "You can't leave the bed." },
                    Use: { type: "cmd", commands: ["Portable Urinal"] },
                    Check: undefined,
                    Auto: undefined,
                    Guard: undefined,
                  }}
                  onCommandChange={() => downloads.change.resource.play()}
                  onSubCommandChange={() => downloads.change.resource.play()}
                  onDone={(value) => {
                    if (value === "Use") navigation?.("next-fade");
                  }}
                />
              </div>
            )}
          </>
        </Canvas>
      </Panel>
      <Panel aria-label="using the portable urinal">
        <Canvas width={width} height={height}>
          <>
            <ActionBlock>
              {async (opts) => {
                downloads.overflowSound.resource.play();
                await sleep(7000);
                if (opts.isBlockCancelled) return;
                downloads.alarmSound.resource.play();
                await sleep(1500);
                if (opts.isBlockCancelled) return;
                downloads.alarmSound.resource.play();
                navigation?.("next-fade");
              }}
            </ActionBlock>
            <Video src={downloads.pouringWater} loop={false} />
            <UiDialogPanel
              visibleLines={1}
              position="absolute"
              zIndex={2}
              width="100%"
              bottom="4rem"
            >
              You use the portable urinal.
            </UiDialogPanel>
            <PlayerInfoPanel level={1} />
          </>
        </Canvas>
      </Panel>
      <Panel aria-label="something goes horribly wrong">
        <Canvas width={width} height={height}>
          <>
            <ActionBlock>
              {async (opts) => {
                await sleep(1500);
                if (opts.isBlockCancelled) return;
                downloads.alarmSound.resource.play();
                await sleep(1500);
                if (opts.isBlockCancelled) return;
                downloads.alarmSound.resource.play();
              }}
            </ActionBlock>
            <Video
              src={downloads.nurseSprayed}
              loop={false}
              onEnd={() => {
                setState("nurse-sprayed");
              }}
            />
            {["nurse-sprayed"].includes(state ?? "") && (
              <UiDialogPanel
                visibleLines={3}
                width="100%"
                height="8rem"
                position="absolute"
                zIndex={2}
                bottom="4rem"
                onDone={async () => {
                  await sleep(1000);
                  downloads.overflowSound.resource.stop();
                  navigation?.("next-fade");
                }}
              >
                {`Something went horribly wrong! 

YOU SPRAYED THE NURSE!`}
              </UiDialogPanel>
            )}
            <PlayerInfoPanel level={1} />
          </>
        </Canvas>
      </Panel>
      <Panel aria-label="end">
        <Canvas width={width} height={height}>
          <>
            <ActionBlock>
              {() => {
                downloads.theme.resource.stop();
                downloads.ending.resource.play();
              }}
            </ActionBlock>
            <Video src={downloads.man} loop={false} />
            <UiDialogPanel
              visibleLines={3}
              width="100%"
              height="8rem"
              position="absolute"
              zIndex={2}
              bottom="4rem"
              onDone={() => {
                downloads.itemAcquired.resource.play();
                setState("lose-level");
              }}
            >
              {`* Oops

Robot loses a level`}
            </UiDialogPanel>
            <PlayerInfoPanel level={state === "lose-level" ? 0 : 1} />
          </>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  // there's some wierd bug in the matter-js engine
  const [key] = useState(0);
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
      key={key}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: "gb.50",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <UiPanel title="GAME (1)" onClick={() => setShow(true)} width="25rem">
          <VanillaButton>Start</VanillaButton>
        </UiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
