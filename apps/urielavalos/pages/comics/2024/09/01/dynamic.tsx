import {
  ActionBlock,
  Canvas,
  CanvasSpeechBubble,
  Panel,
  StoryBoard,
  UiPanel,
  Video,
  World,
  sleep,
  useDownloadsCtx,
  useStateSteps,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { ComponentProps, useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/09/01/theme.wav.mp3",
  },
  video01: {
    type: "video",
    url: "/comics/2024/09/01/video01.mp4",
  },
  video02: {
    type: "video",
    url: "/comics/2024/09/01/video02.mp4",
  },
  video03: {
    type: "video",
    url: "/comics/2024/09/01/video03.mp4",
  },
  video04: {
    type: "video",
    url: "/comics/2024/09/01/video04.mp4",
  },
  video05: {
    type: "video",
    url: "/comics/2024/09/01/video05.mp4",
  },
} as const;

const height = 400;
const width = (height * 1280) / 768;

const dark = "#000";
const light = "#fff";
const oneBitColor = [dark, light] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const darkTextStyle: Pick<
  ComponentProps<typeof CanvasSpeechBubble>,
  "text" | "style" | "textStyle"
> = {
  text: {
    rx: 25,
    ry: 25,
  },
  style: {
    fillColor: "#ffffff00",
    strokeColor: "#ffffff00",
    strokeWidth: 5,
  },
  textStyle: {
    fillStyle: dark,
    font: "bold 36px helvetica,arial,sans-serif",
    lineHeightAsPx: 40,
    glitch: false,
    shadowColor: light,
    shadowBlur: 0,
    shadowOffsetX: 1,
    shadowOffsetY: 1,
  },
} as const;

// const lightTextStyle: Pick<
//   ComponentProps<typeof CanvasSpeechBubble>,
//   "text" | "style" | "textStyle"
// > = {
//   text: {
//     rx: 25,
//     ry: 25,
//   },
//   style: {
//     fillColor: "#ffffff00",
//     strokeColor: "#ffffff00",
//     strokeWidth: 5,
//   },
//   textStyle: {
//     fillStyle: light,
//     font: "bold 36px helvetica,arial,sans-serif",
//     lineHeightAsPx: 40,
//     glitch: false,
//     shadowColor: dark,
//     shadowBlur: 2,
//     shadowOffsetX: 2,
//     shadowOffsetY: 4,
//   },
// } as const;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  // const page = usePagination();
  const [Step, , nextStep, , reset] = useStateSteps({
    steps: ["1", "2", "3", "4", "5"],
  });

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas disableClearCanvasBeforeUpdate>
          <ActionBlock>
            {async () => {
              downloads.theme.resource.stop();
              downloads.theme.resource.play({ loop: true });
            }}
          </ActionBlock>
          <Step step="1">
            <Video
              src={downloads.video01}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
              onEnd={nextStep}
            />
            <CanvasSpeechBubble
              pos={{
                left: 90,
                top: 290,
                height: 100,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...darkTextStyle}
            >
              {`
Hey Cyborg Boy, do you 
want to go swimming?
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="2">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                nextStep();
              }}
            </ActionBlock>
            <Video
              src={downloads.video02}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 375,
                top: 250,
                height: 100,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...darkTextStyle}
            >
              {`
Heck yea!
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="3">
            <Video
              src={downloads.video03}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
              onEnd={nextStep}
            />
            <CanvasSpeechBubble
              pos={{
                left: 350,
                top: 175,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...darkTextStyle}
            >
              {`
Let's go!
`}
            </CanvasSpeechBubble>
          </Step>
          <Step step="4">
            <ActionBlock>
              {async (opts) => {
                await sleep(6_000);
                if (opts.isBlockCancelled) return;
                nextStep();
              }}
            </ActionBlock>
            <Video
              src={downloads.video04}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 120,
                top: 290,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...darkTextStyle}
            >
              {`
Let me dive in!
I'm also amphibious.
          `}
            </CanvasSpeechBubble>
          </Step>
          <Step step="5">
            <ActionBlock>
              {async (opts) => {
                await sleep(10_000);
                if (opts.isBlockCancelled) return;
                reset();
              }}
            </ActionBlock>
            <Video
              src={downloads.video05}
              renderWidth={width}
              renderHeight={height}
              renderTop={0}
              renderLeft={0}
              loop={false}
            />
            <CanvasSpeechBubble
              pos={{
                left: 120,
                top: 290,
                height: 150,
                radius: 25,
                width: 300,
                pLeft: 175,
                pTop: 200,
              }}
              {...darkTextStyle}
            >
              {`
Or maybe not
          `}
            </CanvasSpeechBubble>
          </Step>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: "black",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recognize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
