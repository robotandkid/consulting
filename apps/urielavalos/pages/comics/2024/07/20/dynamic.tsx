import {
  ActionBlock,
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  useStateSteps,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/07/20/theme.mp3",
  },
  crash: {
    type: "sound",
    url: "/comics/2024/07/20/crash.mp3",
  },
  laughing: {
    type: "sound",
    url: "/comics/2024/07/20/laughing.mp3",
  },
  fluttering: {
    type: "sound",
    url: "/comics/2024/07/20/fluttering.mp3",
  },
  intro: {
    type: "video",
    url: "/comics/2024/07/20/video01.mp4",
  },
  coffee: {
    type: "video",
    url: "/comics/2024/07/20/coffee.mp4",
  },
  video02: {
    type: "video",
    url: "/comics/2024/07/20/video04b.mp4",
  },
  video03: {
    type: "video",
    url: "/comics/2024/07/20/video03b.mp4",
  },
  video04: {
    type: "video",
    url: "/comics/2024/07/20/video02b.mp4",
  },
  video05: {
    type: "video",
    url: "/comics/2024/07/20/video05b.mp4",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["#0D0D0D", "white"] as const;

// const panelBg = "#fff7f8";
// const panelFg = "#000000";
// const palette = funkyFuture8Palette();

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Scene = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [Steps, , nextStep] = useStateSteps({
    steps: [
      "scene1",
      "scene2",
      "coffee",
      "scene3",
      "coffee2",
      "scene4",
      "coffee3",
      "scene5",
      "coffee4",
    ],
  });

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          downloads.theme.resource.stop();
          downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <Steps step="scene1">
        <Video
          src={downloads.intro}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="scene2">
        <ActionBlock>
          {async () => {
            downloads.crash.resource.play();
          }}
        </ActionBlock>
        <Video
          src={downloads.video02}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="coffee">
        <Video
          src={downloads.coffee}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="scene3">
        <ActionBlock>
          {async () => {
            downloads.laughing.resource.play();
          }}
        </ActionBlock>
        <Video
          src={downloads.video03}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="coffee2">
        <Video
          src={downloads.coffee}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="scene4">
        <ActionBlock>
          {async () => {
            downloads.laughing.resource.play();
          }}
        </ActionBlock>
        <Video
          src={downloads.video04}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="coffee3">
        <Video
          src={downloads.coffee}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="scene5">
        <ActionBlock>
          {async () => {
            downloads.fluttering.resource.play();
          }}
        </ActionBlock>
        <Video
          src={downloads.video05}
          renderWidth={1024}
          renderHeight={(1024 * 752) / 1360}
          renderTop={0}
          renderLeft={-250}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
      <Steps step="coffee4">
        <Video
          src={downloads.coffee}
          renderWidth={512}
          renderHeight={512}
          renderTop={0}
          renderLeft={0}
          loop={false}
          onEnd={nextStep}
        />
      </Steps>
    </Canvas>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Scene />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
