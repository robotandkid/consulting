import {
  ActionBlock,
  Canvas,
  CanvasImage,
  Panel,
  StoryBoard,
  UiPanel,
  useCanvasRegistration,
  useDownloadsCtx,
  useKeyRegistration,
  usePanelCanvasCtx2D,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/07/04/theme.wav.mp3",
  },
  static: {
    type: "image",
    url: "/comics/2024/07/04/canvas.png",
  },
} as const;

const width = 512;
const height = 512;

const videoRatio = 512 / 896;

const oneBitColor = ["#0D0D0D", "white"] as const;

// const panelBg = "#fff7f8";
// const panelFg = "#000000";
// const palette = funkyFuture8Palette();

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const toRadians = (degrees: number) => (degrees * Math.PI) / 180;
const toDegrees = (radians: number) => (radians * 180) / Math.PI;

const timeOffset = (deltaTime: number, degreeOffset: number) => {
  let prev = 0;
  let time = 0;
  return (elapsedTime: number) => {
    time += elapsedTime;
    if (time > deltaTime) {
      prev += degreeOffset;
      prev %= 360;
      time = 0;
    }
    return prev;
  };
};

const Spiral = (props: {
  deltaInRadians: number;
  radius: number;
  cx: number;
  cy: number;
  palette: Array<`#${string}`>;
}) => {
  const [keyChanged, key] = useKeyRegistration("spiral", props);
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && ctx && render) {
    const time = timeOffset(100, 0.5);

    render((_, t) => {
      const { cx, cy, radius, deltaInRadians, palette } = props;
      const offset = time(t);
      for (
        let degree = 0;
        degree < 360 / toDegrees(deltaInRadians);
        degree += 1
      ) {
        const radian1 = (degree + offset) * deltaInRadians;
        const radian2 = (degree + offset + 1) * deltaInRadians;
        ctx.beginPath();
        ctx.fillStyle = palette[degree % palette.length] ?? "white";
        ctx.moveTo(cx, cy);
        ctx.lineTo(
          cx + radius * Math.cos(radian1),
          cy + radius * Math.sin(radian1)
        );
        ctx.lineTo(
          cx + radius * Math.cos(radian2),
          cy + radius * Math.sin(radian2)
        );
        ctx.closePath();
        ctx.fill();
      }
      return key;
    });
  }

  return null;
};

const Scene1 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          downloads.theme.resource.stop();
          downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <Spiral
        cx={210}
        cy={300}
        radius={450}
        deltaInRadians={toRadians(10)}
        palette={["#00ff00", "#ffffff"]}
      />
      <CanvasImage
        src={downloads.static}
        renderWidth={512}
        renderHeight={512 / videoRatio}
        renderTop={0}
        renderLeft={0}
      />
    </Canvas>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Scene1 />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
