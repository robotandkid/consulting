import {
  ActionBlock,
  Canvas,
  Code,
  Panel,
  ShowAfterDelay,
  sleep,
  StoryBoard,
  UiPanel,
  useCanvasRegistration,
  useDownloadsCtx,
  useKeyRegistration,
  usePagination,
  usePanelCanvasCtx2D,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/07/03/theme.wav.mp3",
  },
  canvas: {
    type: "video",
    url: "/comics/2024/07/03/canvas.mp4",
  },
} as const;

const width = 896;
const height = 640;

const oneBitColor = ["#0D0D0D", "white"] as const;

// const panelBg = "#fff7f8";
// const panelFg = "#000000";
// const palette = funkyFuture8Palette();

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Blur = (props: {
  size: number;
  duration: number;
  direction: "in" | "out";
}) => {
  const [keyChanged, key] = useKeyRegistration("blur", props);
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && ctx && render) {
    if (props.direction === "out") {
      let time = 0;
      let size = props.size;

      render((_, timeElapsedBetweenFrames) => {
        if (time + timeElapsedBetweenFrames >= props.duration) return key;
        time += timeElapsedBetweenFrames;
        size -= (props.size * timeElapsedBetweenFrames) / props.duration;
        size = Math.max(0, size);
        ctx.filter = `blur(${size}px)`;
        return key;
      });
    } else {
      let time = 0;
      let size = 0;

      render((_, timeElapsedBetweenFrames) => {
        if (time + timeElapsedBetweenFrames >= props.duration) return key;
        time += timeElapsedBetweenFrames;
        size += (props.size * timeElapsedBetweenFrames) / props.duration;
        size = Math.min(props.size, size);
        ctx.filter = `blur(${size}px)`;
        return key;
      });
    }
  }

  return null;
};

const Scene1 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          downloads.theme.resource.stop();
          downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <Video
        src={downloads.canvas}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
        loop={false}
        onEnd={async () => {
          await sleep(2_000);
          page?.("next-fade");
        }}
      />
      <Blur size={8} duration={5000} direction="out" />
      <ShowAfterDelay timeout={5000}>
        <Code
          left={width * 0.48}
          top={height * 0.4}
          speed={100}
          style={{
            font: "bold italic 64px helvetica",
            fillStyle: "#fff",
            lineHeightAsPx: 64,
            glitch: false,
          }}
        >
          {`
Be gentle
Take it easy
Allow life
to unfold
`
            .trim()
            .toUpperCase()}
        </Code>
      </ShowAfterDelay>
    </Canvas>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Scene1 />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
