import { CanvasSpeechBubble } from "@robotandkid/sprite-motion";
import { sleep } from "@robotandkid/sprite-motion";
import {
  ActionBlock,
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  useStateSteps,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/comics/2024/07/30/theme.wav.mp3",
  },
  video: {
    type: "video",
    url: "/comics/2024/07/30/video.mp4",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["#0D0D0D", "white"] as const;

// const panelBg = "#fff7f8";
// const panelFg = "#000000";
// const palette = funkyFuture8Palette();

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const robotStyle = {
  fillColor: "#fff",
  strokeColor: "#000",
  strokeWidth: 5,
} as const;

const robotTextStyle = {
  fillStyle: "#000",
  font: "italic 32px helvetica,arial,sans-serif",
  lineHeightAsPx: 32,
  glitch: false,
} as const;

const Scene = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [Steps, , nextStep] = useStateSteps({
    steps: ["1", "2", "3", "4", "5"],
  });

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          downloads.theme.resource.stop();
          downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <Video
        src={downloads.video}
        renderWidth={535}
        renderHeight={535}
        renderTop={-25}
        renderLeft={-25}
        loop={false}
      />
      <Steps step="1">
        <ActionBlock>
          {async () => {
            await sleep(2000);
            nextStep();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{ left: 5, top: 10, height: 90, radius: 0, width: 250 }}
          text={{
            rx: 10,
            ry: 10,
          }}
          style={robotStyle}
          textStyle={robotTextStyle}
        >
          {`
This is George.
He was very...
          `}
        </CanvasSpeechBubble>
      </Steps>
      <Steps step="2">
        <ActionBlock>
          {async () => {
            await sleep(2000);
            nextStep();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{ left: 50, top: 300, height: 50, radius: 10, width: 150 }}
          text={{
            rx: 10,
            ry: 10,
          }}
          style={robotStyle}
          textStyle={robotTextStyle}
        >
          {`
Serious!
          `}
        </CanvasSpeechBubble>
      </Steps>
      <Steps step="3">
        <ActionBlock>
          {async () => {
            await sleep(2000);
            nextStep();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{ left: 5, top: 10, height: 90, radius: 0, width: 250 }}
          text={{
            rx: 10,
            ry: 10,
          }}
          style={robotStyle}
          textStyle={robotTextStyle}
        >
          {`
This is George.
He was very...
          `}
        </CanvasSpeechBubble>
      </Steps>
      <Steps step="4">
        <ActionBlock>
          {async () => {
            await sleep(2000);
            nextStep();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{ left: 50, top: 300, height: 50, radius: 10, width: 150 }}
          text={{
            rx: 10,
            ry: 10,
          }}
          style={robotStyle}
          textStyle={robotTextStyle}
        >
          {`
Curious!
          `}
        </CanvasSpeechBubble>
      </Steps>
      <Steps step="5">
        <ActionBlock>
          {async () => {
            await sleep(2000);
            nextStep();
          }}
        </ActionBlock>
        <CanvasSpeechBubble
          pos={{ left: 50, top: 300, height: 50, radius: 10, width: 350 }}
          text={{
            rx: 15,
            ry: 10,
          }}
          style={robotStyle}
          textStyle={robotTextStyle}
        >
          {`
Dad, what is curious?
          `}
        </CanvasSpeechBubble>
      </Steps>
    </Canvas>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Scene />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
