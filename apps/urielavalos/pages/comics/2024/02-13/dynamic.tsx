import {
  ActionBlock,
  Canvas,
  FilmGrain,
  Panel,
  StaticImg,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  UiPlayerInfoPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  robot: {
    type: "image",
    url: "/comics/2024/01-30/robot.nes.png",
  },
  theme: {
    type: "sound",
    url: "/comics/2024/01-30/theme.mp3",
  },
  night: {
    type: "video",
    url: "/comics/2024/02-13/starry-night.mp4",
  },
} as const;

const width = 512;
const height = 512;

const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
// const oneBitColor = ["#2c1f33", "#f5eac2"] as const;

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {() => {
              downloads.theme.resource.play();
            }}
          </ActionBlock>
          <StaticImg src={downloads.robot} width={width} />
          <UiPlayerInfoPanel
            info={[
              { key: "Name", value: "Robot" },
              { key: "Lvl", value: `1` },
              { key: "HP", value: "80" },
              { key: "Exp", value: "30" },
            ]}
            zIndex={2}
            width="100%"
            height="4rem"
            position="absolute"
            top={0}
          />
          <UiDialogPanel
            visibleLines={3}
            zIndex={2}
            width="100%"
            height="8rem"
            position="absolute"
            bottom="0"
            onDone={async () => {
              await sleep(3000);
              page?.("next-fade");
            }}
          >
            {`I have visual snow syndrome...`}
          </UiDialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <Video src={downloads.night} loop />
          <UiDialogPanel
            visibleLines={3}
            zIndex={2}
            width="100%"
            height="8rem"
            position="absolute"
            bottom="0"
            onDone={async () => {
              await sleep(3000);
              page?.("next-fade");
            }}
          >
            {`While most people see
a beautiful night sky...`}
          </UiDialogPanel>
        </Canvas>
      </Panel>
      <Panel>
        <Canvas width={width} height={height}>
          <Video src={downloads.night} loop />
          <FilmGrain />
          <UiDialogPanel
            visibleLines={3}
            zIndex={2}
            width="100%"
            height="8rem"
            position="absolute"
            bottom="0"
          >
            {`This is what I see.
It's like my eyes have cheap
low-light sensors.`}
          </UiDialogPanel>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: gbColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: "gb.50",
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const VanillaButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
});

const Intro = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <UiPanel title="GAME (1)" onClick={() => setShow(true)} width="25rem">
          <VanillaButton>Start</VanillaButton>
        </UiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Intro;
