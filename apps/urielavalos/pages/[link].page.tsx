import { getStaticPathsForStaticTOC } from "@robotandkid/components";
import { GetStaticPaths } from "next";
import React from "react";
import { AppThemeProvider } from "../src/styles/theme";
import IndexPage, {
  getStaticProps as indexPageStaticProps,
  IndexContent,
} from "./index.page";

export default IndexPage;

export const getStaticProps = indexPageStaticProps;

export const getStaticPaths: GetStaticPaths = async () => {
  const TOCPaths = await getStaticPathsForStaticTOC(
    "link",
    <AppThemeProvider>
      <IndexContent cards={[]} />
    </AppThemeProvider>
  );

  return {
    ...TOCPaths,
    fallback: false,
  };
};
