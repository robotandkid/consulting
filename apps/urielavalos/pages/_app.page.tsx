import { MDXProvider } from "@mdx-js/react";
import {
  BodyUnderHamburger,
  GetStaticPropsStaticTOC,
  HamburgerMenuContextProvider,
  StaticTOCProvider,
} from "@robotandkid/components";
import { AppProps } from "next/app";
import { useEffect } from "react";

import { LazyMotion } from "framer-motion";
import { UrielAvalosGameAsBg } from "../src/components/UrielAvalosGame/UrielAvalosGameAsBg";
import { UrielAvalosHamburgerMenu } from "../src/components/UrielAvalosHamburgerMenu/UrielAvalosHamburgerMenu";
import { UrielAvalosLandingHero } from "../src/components/UrielAvalosLandingHero/UrielAvalosLandingHero";
import { components } from "../src/pages/mdxProviderComponents";
import { GlobalStyle } from "../src/styles/GlobalStyle";
import { AppThemeProvider } from "../src/styles/theme";

import "../styled-system/styles.css";
import "@robotandkid/sprite-motion/styled-system/styles.css";

const loadFramerFeatures = () =>
  import("framer-motion").then((module) => module.domMax);

const defaultToc = [
  { index: 0, href: "#title", name: "home" },
  { href: "#about", name: "about", index: 1 },
  { href: "#gallery", name: "gallery", index: 2 },
  { href: "#contact", name: "contact", index: 3 },
];

function ThisApp(props: AppProps) {
  const { Component } = props;
  const pageProps = props.pageProps as GetStaticPropsStaticTOC;

  useEffect(() => {
    const script = document.createElement("script");
    const script2 = document.createElement("script");

    script.src = "https://www.googletagmanager.com/gtag/js?id=G-YQHQVNWT4D";
    script.async = true;
    script2.src = `
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'G-YQHQVNWT4D');
    `;

    document.body.appendChild(script);
    document.body.appendChild(script2);

    return () => {
      document.body.removeChild(script);
      document.body.removeChild(script2);
    };
  }, []);

  return (
    <>
      <GlobalStyle />
      <LazyMotion strict features={loadFramerFeatures}>
        <AppThemeProvider>
          <MDXProvider components={components as any}>
            <StaticTOCProvider
              initialItemsFromSSR={pageProps.initialStaticTOC ?? defaultToc}
            >
              <HamburgerMenuContextProvider>
                <nav>
                  <UrielAvalosHamburgerMenu />
                </nav>
                <BodyUnderHamburger>
                  <header>
                    <UrielAvalosGameAsBg />
                    <UrielAvalosLandingHero />
                  </header>
                  <Component {...props.pageProps} />
                </BodyUnderHamburger>
              </HamburgerMenuContextProvider>
            </StaticTOCProvider>
          </MDXProvider>
        </AppThemeProvider>
      </LazyMotion>
    </>
  );
}

export default ThisApp;
