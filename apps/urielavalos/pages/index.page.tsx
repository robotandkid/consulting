import {
  getStaticPropsStaticTOC,
  StaticTOCItem,
} from "@robotandkid/components";
import { GetStaticProps } from "next";
import Head from "next/head";
import { useMemo } from "react";
import { UrielAvalosAbout } from "../src/components/UrielAvalosAbout/UrielAvalosAbout";
import { UrielAvalosContact } from "../src/components/UrielAvalosContact/UrielAvalosContact";
import {
  UrielAvalosGalleryGrid,
  UrielAvalosGalleryItem,
} from "../src/components/UrielAvalosGalleryGrid/UrielAvalosGalleryGrid";
import { UrielAvalosSEO } from "../src/components/UrielAvalosSEO/UrielAvalosSEO";
import {
  FrontMatter,
  loadFrontMatterArt,
  loadFrontMatterBlog,
  loadFrontMatterComics,
} from "../src/pages/mdxLoadFrontmatter";
import { CenteredColumn, Row } from "../src/styles/styles";
import { AppThemeProvider } from "../src/styles/theme";

export function IndexContent(props: { cards: UrielAvalosGalleryItem[] }) {
  return (
    <CenteredColumn as="main">
      <Row as="article">
        <UrielAvalosAbout />
        <UrielAvalosGalleryGrid>{props.cards}</UrielAvalosGalleryGrid>
        <UrielAvalosContact />
      </Row>
    </CenteredColumn>
  );
}

export default function IndexPage(props: {
  initialTableOfContents?: StaticTOCItem[];
  all: FrontMatter;
}) {
  const cards = useMemo(
    () =>
      props.all.map((article) => ({
        title: article.metaData.title,
        type: article.metaData.type,
        slug: article.metaData.title,
        href: article.metaData.url,
      })),
    [props.all]
  );

  return (
    <>
      <Head>
        <meta
          name="google-site-verification"
          content="MhOBHTlv28AmrDes3M8nYWrPVUDh-Y6mQUryFOdJbg4"
        />
      </Head>
      <UrielAvalosSEO />
      <IndexContent cards={cards} />
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const comics = await loadFrontMatterComics();
  const posts = await loadFrontMatterBlog();
  const art = await loadFrontMatterArt();
  const all = [...comics, ...posts, ...art];

  all.sort((a, b) => (a.metaData.created <= b.metaData.created ? 1 : -1));

  const toc = await getStaticPropsStaticTOC(
    <AppThemeProvider>
      <IndexContent cards={[]} />
    </AppThemeProvider>
  )();

  const tocProps = toc.props;

  return {
    props: {
      all,
      ...tocProps,
    },
  };
};
