import {
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";
import { WaterMark } from "../../../../src/components/water-mark/water-mark";
import { RecordCanvas } from "../../big-mac-murder/record-canvas";

const resources = {
  robot: {
    type: "video",
    url: "/images/water-mark/robot.mp4",
  },
  kid: {
    type: "video",
    url: "/images/water-mark/kid.mp4",
  },
} as const;

const width = 350;
const height = 750;

const oneBitColor = ["#fff", "#000"] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

// const textPalette = palette8funkyFuture();

const Frame = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <WaterMark
        top={0}
        left={0}
        width={width}
        kid={downloads.kid}
        robot={downloads.robot}
        fontSize={98}
        borderColor="#000"
        borderThickness={15}
      />
      <RecordCanvas time={10000} />
    </>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas disableClearCanvasBeforeUpdate>
          <Frame />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
