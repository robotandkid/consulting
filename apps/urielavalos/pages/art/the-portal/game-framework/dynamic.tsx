import {
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";
import { Choice, CYOAStory, StoryNode } from "./story-framework";

const resources = {} as const;

const width = 512;
const height = 512;

const oneBitColor = ["#fff", "#000"] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

interface State {
  travel?: "submarine" | "boat";
  explore: Record<"logs" | "power-room" | "medical-bay", true>;
  portal?: "open";
}

// Sample Story Setup with State
const story = new CYOAStory<State>(
  {
    explore: {} as State["explore"],
  },
  new StoryNode(
    "introNode",
    `Eons ago...
  a long forgotten civilization opened a portal to the beyond...
  but they unleashed a horror...
  they were left with only one option---deploy the doomsday device.
  but while it destroyed their civilization, it did not destroy the portal.
  The end?
  `,
    { choices: [new Choice("start", "startNode")] }
  ),
  new StoryNode(
    "startNode",
    `Location: Somewhere in the present.

    Frame 1: wide shot of the board room, dimly lit with a cold corporate aesthetic. Oboro sits at the head of a long, metallic table.

    Frame 2: close-up of the holo-screen, showing a rotating 3D model of the Antartctic base. 

    Frame 3: over-the-shoulder shot from the professor's perspective, showing Oboro's face.

    Oboro: Professor, you're one of the few people who understands the significance of the artifact. I need someone with your expertise. That’s why I’m giving you this mission.

    Frame 4: medium shot of the professor seated at the table with arms crossed

    You: What’s this really about, Oboro? You already sent a team in to investigate. What happened to them?

    Frame 5: close-up of Oboro's face.
    
    Oboro: They’re missing. All of them. We lost contact three days ago. (His voice remains cool, detached.)
    
    Oboro: Your task is simple: recover the portal artifact and, if possible, bring back the missing team. You’re the only one with the knowledge to get the job done.

    Frame 6: close up of the Professor's eyes. 

    You: (You lean back, considering your options, the base schematics still glowing on the screen)
    
    You (thinking): Something about the mission feels wrong—missing information, things unsaid.
    But the portal is too dangerous in the wrong hands, and if anyone else goes, they’ll mishandle it. Maybe they already have.
    
    Frame 7: close-up of the holo-screen displaying two transport options: the submarine and the ship.
    `,
    {
      choices: [
        new Choice(
          "Take the submarine. Faster but the depths bring higher risks.",
          "arriveBaseNode",
          (state) => {
            state.travel = "submarine";
          }
        ),
        new Choice(
          "Take the boat. Slower but more stable.",
          "arriveBaseNode",
          (state) => {
            state.travel = "boat";
          }
        ),
      ],
    }
  ),
  new StoryNode(
    "arriveBaseNode",
    `The base, once a thriving outpost of cutting-edge research, now looks like a crumbling, forgotten relic. Thick layers of ice and snow cling to the buildings, some windows shattered, others frosted over. The towers and communication arrays are buried under snowdrifts, barely visible through the white fog that rolls in with the wind. Rusted metal and faded insignias from the corporation are all that mark this place as once being inhabited. It’s silent, hauntingly still.
    `,
    {
      choices: [
        new Choice("Read the logs", "logNode"),
        new Choice("Investigate the power room", "powerRoomNode"),
        new Choice("Investigate the medical bay", "medicalBay"),
        new Choice("Go straight for the portal artifact", "portalNode"),
      ],
    }
  ),
  new StoryNode(
    "logNode",
    `You read the base logs. 
    Excerpt: "Day 12: We believe the artifact could be an inter-dimensional gateway..."
    Excerpt: "Day 47: First portal opened successfully. Sending in drones for a deeper look.
    Excerpt: "Day 89: Something came through... Dr Avalos is gone."
    Excerpt: "Day 109: We should have never touched it. They won't stop coming."
    `,
    {
      updateState: (state) => (state.explore.logs = true),
      choices: [
        new Choice("Investigate the power room", "powerRoomNode"),
        new Choice("Investigate the medical bay", "medicalBay"),
        new Choice("Go straight for the portal artifact", "portalNode"),
      ],
    }
  ),
  new StoryNode(
    "powerRoomNode",
    "The core that powers the portal hums violently, sparks flying as coolant tubes leak vapor. Read warning lights pulse rhythmically, casting the room in a sinister glow.",
    {
      updateState: (state) => (state.explore["power-room"] = true),
      choices: [
        new Choice("Read the logs", "logNode"),
        new Choice("Investigate the medical bay", "medicalBay"),
        new Choice("Go straight for the portal artifact", "portalNode"),
      ],
    }
  ),
  new StoryNode(
    "medicalBay",
    "Three medical beds are occupied. The first crew member's skin is blistered and peeling. The others are barely recognizable as human. They look as if their last action was to try to scream.",
    {
      updateState: (state) => (state.explore["medical-bay"] = true),
      choices: [
        new Choice("Read the logs", "logNode"),
        new Choice("Investigate the power room", "powerRoomNode"),
        new Choice("Go straight for the portal artifact", "portalNode"),
      ],
    }
  ),
  new StoryNode(
    "portalNode",
    `A wall of blinking monitors flickers with static, casting an eerie blue glow across the room. You tap the console, opening the secure line to headquarters. 
    The face of your boss, Oboro, appears on the largest screen. 
    "Turn it on, Professor. Now. The corporation’s invested too much for you to back out now. The research team is still in there, waiting to be rescued."
    `,
    {
      choices: [
        new Choice("Ignore Oboro", "cyborgNode"),
        new Choice("Open the portal", "openPortalNode"),
      ],
    }
  ),
  new StoryNode(
    "openPortalNode",
    `You power on the portal. 
    Soon the air feels heavy, thick with the stench of something unnatural.
    There is a lone figure appearing out of the portal. His breathing is shallow, rasping.
    As you step closer, he says, "Shut...it...down...We didn't know...we unleashed...something..."
    You instinctively take a step back, as his body starts to melt in front of you.`,
    {
      choices: [
        new Choice(
          "Shut the portal",
          "cyborgNode",
          (state) => (state.portal = "open")
        ),
      ],
    }
  ),
  new StoryNode(
    "cyborgNode",
    (state) =>
      `The soldier arrives, ${
        state.portal === "open"
          ? `Just as you reach for the controls, a loud crash reverberates through the room. A figure emerges from the shadows—a hulking presence clad in a sleek, black suit of cybernetic armor.
          "Wait—what are you doing?" you shout, taking a step forward, but he ignores you completely.
          His fingers continue tapping commands into the console.
          Suddenly, the portal surges with violent energy, its surface boiling and expanding. Alarms blare throughout the room, a shrill, deafening sound.
          "What the hell did you do?" you yell, rushing to the controls.
          The cybernetic soldier accidentally jammed the portal open.
          `
          : `A loud crash reverberates through the room. A figure emerges from the shadows—a hulking presence clad in a sleek, black suit of cybernetic armor.
          "Wait—what are you doing?" you shout, taking a step forward, but he ignores you completely.
          His fingers continue tapping commands into the console.
          Suddenly, the portal surges with violent energy, its surface boiling and expanding. Alarms blare throughout the room, a shrill, deafening sound.
          "What the hell did you do?" you yell, rushing to the controls.
          The cybernetic soldier accidentally jammed the portal open`
      }`,
    {
      choices: [
        new Choice("Fight", "fightSoldier"),
        new Choice("Negotiate", "negotiateSoldier"),
      ],
    }
  ),
  new StoryNode(
    "fightSoldier",
    `
    The professor's hand instinctively reaches for a nearby wrench, her only available weapon. Without hesitation, the professor swings the wrench with all her strength, aiming for his head. But in one swift motion, the soldier catches her wrist mid-swing, his mechanical hand closing around her arm with a vise-like grip.
    
    The professor winces as pain shoots up her arm, struggling to free herself, but it’s no use. The soldier's grip tightens. "You're outmatched," he says coldly, barely reacting to her futile attempts to resist.
    
    Gasping for air, the professor’s eyes dart around the room, searching for a way out. Her fingers brush against a lever on the control panel, her last hope. Summoning the last of her strength, she manages to pull the switch. A loud mechanical groan reverberates through the room as machinery sputters to life, sending a shower of sparks cascading from the ceiling.
    
    The distraction works—the soldier loosens his grip for a split second, giving the professor just enough time to drop to the ground, gasping for breath. She scrambles backward, crawling away from him, her vision still blurred from the lack of oxygen. The room continues to rumble, and the soldier glances toward the glitching portal controls. His focus shifts momentarily, giving the professor the chance to pull herself up and stagger toward the nearest exit.
  `,
    {
      choices: [],
    }
  ),
  new StoryNode(
    "negotiateSoldier",
    `
    “Wait,” the professor says, “We don’t have to fight.”

    The soldier stops, his expression unchanged. 
    
    “I know what you want,” she continues, lowering her hands slowly but keeping her eyes on him. “You’re here for the portal. So am I. But if you jam it open, we’re both dead—everything here will be lost, including you.”

    "Fine," he mutters. "But if you try anything, I’ll kill you."
    `,
    {
      choices: [],
    }
  )
);

story.start("introNode");

const ComponentInner = () => {
  const [, setRender] = useState(0);

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas disableClearCanvasBeforeUpdate>
          <div
            style={{
              height: height * 0.8,
              overflow: "scroll",
              overflowY: "scroll",
              top: 0,
              left: 0,
              width: width * 0.8,
              position: "absolute",
              zIndex: 24,
              fontFamily: "mrpixel",
            }}
          >
            <ul
              style={{
                listStyleType: "none",
              }}
            >
              {story.render().text.map((line) => (
                <li key={line} style={{ color: "#000" }}>
                  {line}
                </li>
              ))}
              {story.render().choices.map((line) => (
                <li key={line} style={{ color: "#000" }}>
                  {line}
                </li>
              ))}
            </ul>
          </div>
          <label
            style={{
              position: "absolute",
              bottom: "0em",
              zIndex: 999,
            }}
          >
            Selection:{" "}
            <input
              name="selection"
              type="text"
              onKeyDown={(e) => {
                if (e.key === "Enter" && e.currentTarget.value) {
                  story.choose(+e.currentTarget.value);
                  setRender((c) => c + 1);
                  e.currentTarget.value = "";
                }
              }}
            />
          </label>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
