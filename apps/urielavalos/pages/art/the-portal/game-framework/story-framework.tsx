type PredicateStateFn<State> = (state: State) => boolean;
type UpdateStateFn<State> = (state: State) => void;

export class StoryNode<State, Id extends string> {
  id: Id;
  text: string | ((state: State) => string);
  choices: Choice<State, Id>[];
  checkState: PredicateStateFn<State> | undefined;
  updateState: UpdateStateFn<State> | undefined;

  constructor(
    id: Id,
    text: string | ((state: State) => string),
    opts: {
      choices: Choice<State, Id>[];
      checkState?: PredicateStateFn<State>;
      updateState?: UpdateStateFn<State>;
    }
  ) {
    this.id = id; // Unique identifier for the story node
    this.text = text; // The story content displayed at this node
    this.choices = opts.choices;
    this.checkState = opts.checkState;
    this.updateState = opts.updateState;
  }
}

export class Choice<State, Id extends string> {
  text: string;
  nextNodeId: Id;
  action: UpdateStateFn<State> | undefined;
  condition: PredicateStateFn<State> | undefined;

  constructor(
    text: string,
    nextNodeId: Id,
    action: undefined | ((state: State) => void) = undefined,
    condition: undefined | PredicateStateFn<State> = undefined
  ) {
    this.text = text; // The text of the choice
    this.nextNodeId = nextNodeId; // The ID of the next StoryNode
    this.action = action; // Optional function to modify state when this choice is made
    this.condition = condition; // Optional condition to show the choice (based on state)
  }
}

interface Render {
  text: string[];
  choices: string[];
}

export class CYOAStory<State, Id extends string = string> {
  storyNodes: Record<Id, StoryNode<State, Id>>;
  currentNode: StoryNode<State, Id> | undefined;
  state: State;

  constructor(initialState: State, ...nodes: StoryNode<State, Id>[]) {
    // @ts-expect-error dunno 🤷
    this.storyNodes = {}; // To store story nodes by their ID
    this.currentNode = undefined; // Track the current story node
    this.state = initialState; // State object to hold player's current state (e.g., health, inventory)
    nodes.forEach((node) => (this.storyNodes[node.id] = node));
  }

  addNode(node: StoryNode<State, Id>) {
    this.storyNodes[node.id] = node; // Add node to the story
  }

  start(nodeId: Id) {
    this.currentNode = this.storyNodes[nodeId]; // Set the starting node
  }

  choose(optionIndex: number): Render {
    const selectedChoice = this.currentNode?.choices[optionIndex];

    if (!selectedChoice) return { text: [], choices: [] };

    // Check if the choice has a condition based on the state
    if (!selectedChoice.condition || selectedChoice.condition(this.state)) {
      // Apply any state modifications from the choice
      if (selectedChoice.action) {
        selectedChoice.action(this.state);
      }
      // Move to the next story node
      this.currentNode = this.storyNodes[selectedChoice.nextNodeId];
      this.currentNode?.updateState?.(this.state);

      return this.render();
    } else {
      return { text: ["Invalid choice or condition not met!"], choices: [] };
    }
  }

  render(): Render {
    if (
      this.currentNode?.checkState &&
      !this.currentNode.checkState(this.state)
    ) {
      return {
        text: [
          "You cannot proceed further. Condition not met for this part of the story.",
        ],
        choices: [],
      };
    }

    const message =
      typeof this.currentNode?.text === "function"
        ? this.currentNode.text(this.state)
        : this.currentNode?.text;

    const response: Render = {
      text: message?.split("\n") ?? [],
      choices: [],
    };

    // List the available choices, checking any conditions
    this.currentNode?.choices.forEach((choice) => {
      if (!choice.condition || choice.condition(this.state)) {
        response.choices.push(choice.text);
      }
    });

    return response;
  }
}
