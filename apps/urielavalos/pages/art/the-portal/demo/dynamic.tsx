import {
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";
import {
  Choice,
  CYOAStory,
  StoryNode,
} from "../game-framework/story-framework";
import { Video } from "@robotandkid/sprite-motion";
import { useDownloadsCtx } from "@robotandkid/sprite-motion";
import { UiDialogWithCommands } from "@robotandkid/sprite-motion";
import { ActionBlock } from "@robotandkid/sprite-motion";
import { sleep } from "@robotandkid/sprite-motion";
import { usePanelCanvas } from "@robotandkid/sprite-motion";
import { useStateSteps } from "@robotandkid/sprite-motion";
import { UiDialog } from "@robotandkid/sprite-motion";

const resources = {
  scene01: {
    type: "video",
    url: "/art/the-portal/demo/scene01.mp4",
  },
  scene02: {
    type: "video",
    url: "/art/the-portal/demo/scene02.mp4",
  },
  scene03: {
    type: "video",
    url: "/art/the-portal/demo/scene03.mp4",
  },
  wind: {
    type: "sound",
    url: "/art/the-portal/demo/wind.mp3",
  },
  keyClick: {
    type: "sound",
    url: "/art/the-portal/demo/key-click.mp3",
  },
  keySelect: {
    type: "sound",
    url: "/art/the-portal/demo/key-select.mp3",
  },
  metallicHit: {
    type: "sound",
    url: "/art/the-portal/demo/metalic-hit.mp3",
  },
} as const;

const width = 900;
const height = (width * 768) / 1280;

const oneBitColor = ["#fff", "#000"] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

interface State {
  travel?: "submarine" | "boat";
  explore: Record<"logs" | "power-room" | "medical-bay", true>;
  portal?: "open";
}

// Sample Story Setup with State
const story = new CYOAStory<State>(
  {
    explore: {} as State["explore"],
  },
  new StoryNode(
    "startNode",
    `You have arrived at the base.
    `,
    {
      choices: [
        new Choice("Read the logs", "logNode"),
        new Choice("Investigate the power room", "powerRoomNode"),
        new Choice("Investigate the medical bay", "medicalBay"),
        new Choice("Go straight for the portal artifact", "portalNode"),
      ],
    }
  ),
  new StoryNode(
    "logNode",
    `You read the base logs. 
    Excerpt: "Day 12: We believe the artifact could be an inter-dimensional gateway..."
    Excerpt: "Day 47: First portal opened successfully. Sending in drones for a deeper look.
    Excerpt: "Day 89: Something came through... Dr Avalos is gone."
    Excerpt: "Day 109: We should have never touched it. They won't stop coming."
    `,
    {
      updateState: (state) => (state.explore.logs = true),
      choices: [
        new Choice("Investigate the power room", "powerRoomNode"),
        new Choice("Investigate the medical bay", "medicalBay"),
        new Choice("Go straight for the portal artifact", "portalNode"),
      ],
    }
  ),
  new StoryNode(
    "powerRoomNode",
    "The core that powers the portal hums violently, sparks flying as coolant tubes leak vapor. Read warning lights pulse rhythmically, casting the room in a sinister glow.",
    {
      updateState: (state) => (state.explore["power-room"] = true),
      choices: [
        new Choice("Read the logs", "logNode"),
        new Choice("Investigate the medical bay", "medicalBay"),
        new Choice("Go straight for the portal artifact", "portalNode"),
      ],
    }
  ),
  new StoryNode("medicalBay", "Oh no! What happened here...?", {
    updateState: (state) => (state.explore["medical-bay"] = true),
    choices: [
      new Choice("Read the logs", "logNode"),
      new Choice("Investigate the power room", "powerRoomNode"),
      new Choice("Go straight for the portal artifact", "portalNode"),
    ],
  }),
  new StoryNode(
    "portalNode",
    `A wall of blinking monitors flickers with static, casting an eerie blue glow across the room. You tap the console, opening the secure line to headquarters. 
    The face of your boss, Oboro, appears on the largest screen. 
    "Turn it on, Professor. Now. The corporation’s invested too much for you to back out now. The research team is still in there, waiting to be rescued."
    `,
    {
      choices: [
        new Choice("Ignore Oboro", "cyborgNode"),
        new Choice("Open the portal", "openPortalNode"),
      ],
    }
  ),
  new StoryNode(
    "openPortalNode",
    `You power on the portal. 
    Soon the air feels heavy, thick with the stench of something unnatural.
    There is a lone figure appearing out of the portal. His breathing is shallow, rasping.
    As you step closer, he says, "Shut...it...down...We didn't know...we unleashed...something..."
    You instinctively take a step back, as his body starts to melt in front of you.`,
    {
      choices: [
        new Choice(
          "Shut the portal",
          "cyborgNode",
          (state) => (state.portal = "open")
        ),
      ],
    }
  ),
  new StoryNode(
    "cyborgNode",
    (state) =>
      `The soldier arrives, ${
        state.portal === "open"
          ? `Just as you reach for the controls, a loud crash reverberates through the room. A figure emerges from the shadows—a hulking presence clad in a sleek, black suit of cybernetic armor.
          "Wait—what are you doing?" you shout, taking a step forward, but he ignores you completely.
          His fingers continue tapping commands into the console.
          Suddenly, the portal surges with violent energy, its surface boiling and expanding. Alarms blare throughout the room, a shrill, deafening sound.
          "What the hell did you do?" you yell, rushing to the controls.
          The cybernetic soldier accidentally jammed the portal open.
          `
          : `A loud crash reverberates through the room. A figure emerges from the shadows—a hulking presence clad in a sleek, black suit of cybernetic armor.
          "Wait—what are you doing?" you shout, taking a step forward, but he ignores you completely.
          His fingers continue tapping commands into the console.
          Suddenly, the portal surges with violent energy, its surface boiling and expanding. Alarms blare throughout the room, a shrill, deafening sound.
          "What the hell did you do?" you yell, rushing to the controls.
          The cybernetic soldier accidentally jammed the portal open`
      }`,
    {
      choices: [
        new Choice("Fight", "fightSoldier"),
        new Choice("Negotiate", "negotiateSoldier"),
      ],
    }
  )
);

story.start("startNode");

const FullScreen = () => {
  const canvas = usePanelCanvas();

  const goFullScreen = () => {
    if (!canvas) return;
    if (canvas.requestFullscreen) canvas.requestFullscreen();
  };

  return (
    <button
      onClick={goFullScreen}
      style={{ position: "absolute", left: 0, top: 0, zIndex: 999 }}
    >
      Fullscreen
    </button>
  );
};

const Scene1 = (props: { nextStep: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [show, setShow] = useState(false);

  return (
    <>
      <ActionBlock>
        {async (opts) => {
          downloads.wind.resource.play({ loop: true });
          await sleep(5000);
          if (opts.isBlockCancelled) return;
          downloads.metallicHit.resource.play();
          setShow(true);
        }}
      </ActionBlock>
      <Video
        src={downloads.scene01}
        renderLeft={0}
        renderTop={0}
        renderWidth={width}
        renderHeight={height}
        loop={false}
      />
      {show && (
        <UiDialogWithCommands
          dialogPos={{
            left: 50,
            top: 100,
            width: 435,
            height: 115,
            textOffsetX: 30,
            textOffsetY: 60,
            speed: 50,
            radius: 10,
          }}
          commands={story.render().choices}
          commandPos={{
            left: 300,
            top: 200,
            textOffsetX: 30,
            textOffsetY: 30,
            height: 250,
            width: 500,
            radius: 10,
            speed: 50,
          }}
          textStyles={{
            font: "28px mrpixel",
            lineHeightAsPx: 28 * 1.2,
            glitch: false,
            fillStyle: "#fff",
          }}
          containerStyles={{
            fillColor: "#000",
            strokeColor: "#fff",
            strokeWidth: 15,
          }}
          onChange={() => {
            downloads.keyClick.resource.play();
          }}
          onSelect={() => {
            downloads.keySelect.resource.play();
            props.nextStep();
          }}
        >
          {story.render().text.join("\n")}
        </UiDialogWithCommands>
      )}
      <FullScreen />
    </>
  );
};

const Scene2 = (props: { nextStep: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [show, setShow] = useState(false);

  return (
    <>
      <ActionBlock>
        {async () => {
          story.choose(2);
          downloads.wind.resource.stop();
        }}
      </ActionBlock>
      <Video
        src={downloads.scene02}
        renderLeft={0}
        renderTop={0}
        renderWidth={width}
        renderHeight={height}
        loop={false}
        onEnd={async () => {
          setShow(true);
          downloads.metallicHit.resource.play();
          await sleep(10_000);
          props.nextStep();
        }}
        muted={false}
      />
      {show && (
        <UiDialog
          dialogPos={{
            left: 50,
            top: 100,
            width: 435,
            height: 115,
            textOffsetX: 30,
            textOffsetY: 60,
            speed: 50,
            radius: 10,
          }}
          textStyles={{
            font: "28px mrpixel",
            lineHeightAsPx: 28 * 1.2,
            glitch: false,
            fillStyle: "#fff",
          }}
          containerStyles={{
            fillColor: "#000",
            strokeColor: "#fff",
            strokeWidth: 15,
          }}
        >
          {story.render().text.join("\n")}
        </UiDialog>
      )}
    </>
  );
};

const Scene3 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <ActionBlock>
        {async () => {
          downloads.wind.resource.play({ loop: true });
        }}
      </ActionBlock>
      <Video
        src={downloads.scene03}
        renderLeft={0}
        renderTop={0}
        renderWidth={width}
        renderHeight={height}
        loop
      />
    </>
  );
};

const ComponentInner = () => {
  const [Step, , nextStep] = useStateSteps({
    steps: ["scene1", "scene2", "loop"],
  });
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas disableClearCanvasBeforeUpdate>
          <Step step="scene1">
            <Scene1 nextStep={nextStep} />
          </Step>
          <Step step="scene2">
            <Scene2 nextStep={nextStep} />
          </Step>
          <Step step="loop">
            <Scene3 />
          </Step>
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
