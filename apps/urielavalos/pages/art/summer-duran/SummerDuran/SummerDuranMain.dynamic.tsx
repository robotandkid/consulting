import { onClickA11y } from "@robotandkid/components";
import { PixiFilter } from "@robotandkid/pixi-components";
import {
  defaultTextStyle,
  PixiApp,
  TypewriterText,
} from "@robotandkid/pixi-components";
import { LayoutGroup, motion } from "framer-motion";
import { XCircle } from "phosphor-react";
import { Application } from "pixi.js";
import React, { useCallback, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import {
  getMeSpeak,
  WrapMeSpeak,
} from "../../../../src/components/MeSpeak/MeSpeak";
import { MeSpeakOscilloscope } from "../../../../src/components/MeSpeak/MeSpeakOscilloscope";
import {
  ExternalLink,
  InternalLink,
} from "../../../../src/styles/ExternalLink/ExternalLink";
import { Text, text } from "../../../../src/styles/styles";

function useWaitToStart(timeout: number) {
  const [start, setStart] = useState(false);

  useEffect(() => {
    const interval = setTimeout(() => {
      setStart(true);
    }, timeout);
    return () => clearTimeout(interval);
  }, [timeout]);

  return start;
}

const ClickHereContainer = styled(motion.div)`
  ${text}
  text-align: center;
  font-style: italic;
`;

const ClickHere = styled(InternalLink)``;

function useReadPoem(props: { lines: string[] }) {
  const { lines } = props;
  const [current, setCurrent] = useState<{ line: string; index: number }>({
    line: ".",
    index: -1,
  });
  const start = useWaitToStart(2000);
  const done = useRef(false);

  useEffect(() => {
    const meSpeak = getMeSpeak();

    if (!start || !meSpeak) return;

    lines
      .map((line, index) => () => {
        if (done.current) return;
        setCurrent({ line, index });
        return meSpeak.speak(line);
      })
      .reduce(
        (cur, next) => cur.then(next),
        Promise.resolve<boolean | undefined>(true)
      )
      .then(() => setCurrent({ line: ".........", index: -1 }));

    return () => {
      done.current = true;
      meSpeak.stop();
    };
  }, [lines, start]);

  return current;
}

const MadeBySummerContainer = styled(Text)`
  margin-bottom: 4rem;
`;

function MadeBySummer() {
  return (
    <MadeBySummerContainer>
      Original poem by <em>Summer Duran</em>, a special young woman who recently
      started writing poems and happens to be cognitively challenged.
    </MadeBySummerContainer>
  );
}

const MadeWithMeSpeakContainer = styled(Text)`
  margin-top: 4rem;
`;

function MadeWithMeSpeak() {
  return (
    <MadeWithMeSpeakContainer>
      Made with{" "}
      <ExternalLink href="https://masswerk.at/mespeak/">
        meSpeak.js
      </ExternalLink>{" "}
      and <ExternalLink href="https://pixijs.com/">Pixi.js</ExternalLink>.
    </MadeWithMeSpeakContainer>
  );
}

export const openSpring = { type: "spring", stiffness: 200, damping: 30 };
export const closeSpring = { type: "spring", stiffness: 300, damping: 35 };

const Page = styled(motion.div)`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ffffff;
  z-index: 10000;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const XButtonIcon = styled(XCircle)`
  position: fixed;
  top: 1rem;
  right: 1rem;
  width: 3.5em;
  height: 3.5em;
  outline: none;
  border: none;
`;

const width = 480;
const height = (width * 3) / 4;
const tvContentColor = "#CBF23D";
const voiceWaveColor = "#F2622E";

const TvBlip = styled(motion.div)`
  position: relative;
  width: 1px;
  height: 1px;
  margin: auto;
  border: 2px ${tvContentColor} solid;
  border-top-width: 2px;
  border-bottom-width: 2px;
  border-radius: 1px;
`;

const TV = styled(motion.div)`
  position: relative;
  width: ${width}px;
  height: ${height}px;
  margin: auto;
  border: 16px black solid;
  border-top-width: ${height / 10}px;
  border-bottom-width: ${height / 10}px;
  border-radius: 36px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #a6a6a6;

  /* The horizontal line on the top of the device */
  /* &:before {
    content: "";
    display: block;
    width: 60px;
    height: 5px;
    position: absolute;
    top: -30px;
    left: 50%;
    transform: translate(-50%, -50%);
    background: #333;
    border-radius: 10px;
  } */

  /* The circle on the bottom of the device */
  /* &:after {
    content: "";
    display: block;
    width: 35px;
    height: 35px;
    position: absolute;
    left: 50%;
    bottom: -65px;
    transform: translate(-50%, -50%);
    background: #333;
    border-radius: 50%;
  } */

  /* The screen (or content) of the device */
  & canvas {
    width: ${width}px;
    height: ${height}px;
    margin: -1px;
  }
`;

function SummerDuranPixiApp(props: { children: string[] }) {
  const [app, setApp] = useState<Application>();
  const poem = useReadPoem({ lines: props.children });
  const turnOnTV = useWaitToStart(1000);

  return (
    <TV>
      {!turnOnTV && <TvBlip layoutId="TV" />}
      {turnOnTV && (
        <PixiApp
          width={width}
          height={height}
          resolution={window.devicePixelRatio || 1}
          backgroundColor={tvContentColor}
          onAppInit={setApp}
          layoutId="TV"
        >
          <PixiFilter comp={app} filters={[{ filter: "crt" }]} />
          <MeSpeakOscilloscope
            style={{ height: height / 2, backgroundColor: tvContentColor }}
            lineStyle={{ width: 2, color: voiceWaveColor }}
            parent={app}
          />
          {poem?.line && (
            <TypewriterText
              parent={app}
              lineId={`${poem.line}`}
              typingSpeedInMs={30}
              style={{
                ...defaultTextStyle(),
                fontSize: 24,
                wordWrapWidth: width,
                fill: ["#ffffff", "#00ff99"], // gradient
                stroke: "#4a1850",
                strokeThickness: 5,
                dropShadow: true,
                dropShadowColor: "#000000",
                dropShadowBlur: 4,
                dropShadowAngle: Math.PI / 6,
                dropShadowDistance: 6,
                left: 0,
                top: height / 2,
                height: height / 2,
                backgroundColor: tvContentColor,
              }}
            >
              {poem.line}
            </TypewriterText>
          )}
        </PixiApp>
      )}
    </TV>
  );
}

export function InternalSummerDuranMain(props: { children: string[] }) {
  const [start, setStart] = useState(false);

  useEffect(() => {
    const close = (ev: KeyboardEvent) => {
      ev.key === "Escape" && setStart(false);
    };
    window.addEventListener("keydown", close);

    return () => {
      window.removeEventListener("keydown", close);
    };
  });

  const startCb = useCallback(() => setStart(true), []);

  return (
    <WrapMeSpeak>
      <LayoutGroup>
        {!start && (
          <>
            <MadeBySummer />
            <ClickHereContainer layoutId="poem">
              <ClickHere {...onClickA11y(startCb)}>Click to start!</ClickHere>
            </ClickHereContainer>
            <MadeWithMeSpeak />
          </>
        )}
        {start && (
          <Page layoutId="poem">
            <XButtonIcon onClick={() => setStart(false)} />
            <SummerDuranPixiApp>{props.children}</SummerDuranPixiApp>
          </Page>
        )}
      </LayoutGroup>
    </WrapMeSpeak>
  );
}
