import { SummerDuranMain } from "./SummerDuranMain";

const poem = `
  Oh. yes but first first you must
  where you're meant to be.
  we make our nets fom the
  the water is sweet inside.
  we use the leaves to build.
  we cook up the meat inside
  well. you have to pay.
  for your pizza. sir.
  where those tree tops top
  may our days.
  be merry and bright.
`.split("\n");

export function SummerDuran1() {
  return <SummerDuranMain>{poem}</SummerDuranMain>;
}
