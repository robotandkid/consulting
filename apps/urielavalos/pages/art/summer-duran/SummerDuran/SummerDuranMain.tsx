import dynamic from "next/dynamic";
import { ComicPlaceholder } from "../../../../src/styles/styles";
import { theme } from "../../../../src/styles/theme";
import { InternalSummerDuranMain } from "./SummerDuranMain.dynamic";

export const SummerDuranMain = dynamic<
  Parameters<typeof InternalSummerDuranMain>[0]
>(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore dynamic imports not working
    import("./SummerDuranMain.dynamic").then(
      (module) => module.InternalSummerDuranMain
    ) as any,
  {
    ssr: false,
    loading() {
      return <ComicPlaceholder theme={theme}>Loading...</ComicPlaceholder>;
    },
  }
);
