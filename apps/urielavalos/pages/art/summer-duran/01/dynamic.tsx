import {
  ActionBlock,
  Canvas,
  CanvasBlur,
  CanvasDithering,
  CanvasImage,
  Code,
  Panel,
  sleep,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  useStateSteps,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import React, { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/art/summer-duran/01/theme.wav.mp3",
  },
  summer01: {
    type: "image",
    url: "/art/summer-duran/01/summer-01.jpg",
  },
  summer02: {
    type: "image",
    url: "/art/summer-duran/01/summer-02.jpg",
  },
  summer03: {
    type: "image",
    url: "/art/summer-duran/01/summer-03.jpg",
  },
  summer04: {
    type: "image",
    url: "/art/summer-duran/01/summer-04.jpg",
  },
  summer05: {
    type: "image",
    url: "/art/summer-duran/01/summer-05.jpg",
  },
  summer06: {
    type: "image",
    url: "/art/summer-duran/01/summer-06.jpg",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["black", "white"] as const;
const palette = [
  "#5ca8c2",
  "#055d31",
  "#86d86a",
  "#fdfdf0",
  "#faa3ce",
  "#a4573d",
  "#02020f",
  "#7a2796",
] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const poem = [
  `
Oh. yes but first first you
must
`,
  `
where you're meant to be.
`,
  `
we make our nets fom the
the water is sweet inside.
`,
  `
we
use
the
leaves
to
build.
`,
  `
we
cook
up
the
meat
inside
`,
  `
well. you have to pay.
for your pizza. sir.
`,
  `where those tree tops top`,
  `
may our days.
be merry and bright.
`,
].map((str) => str.trim());

const Poem = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [Step, , nextStep] = useStateSteps({
    steps: ["1", "2", "3", "4", "5", "6"],
  });

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          downloads.theme.resource.stop();
          downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <Step step="1">
        <CanvasImage src={downloads.summer01} />
        <CanvasDithering palette={palette} />
        <Code
          left={-10}
          top={40}
          style={{
            textBaseline: "bottom",
            fillStyle: "#fff",
            font: "bold 46px helvetica",
            glitch: false,
            lineHeightAsPx: 46 * 1.2,
          }}
          onDone={async () => {
            await sleep(1500 + 4000);
            nextStep();
          }}
        >
          {`
          ${poem[0]}
        `.trim()}
        </Code>
        <CanvasBlur duration={1500} direction="out" size={100} />
      </Step>
      <Step step="2">
        <CanvasImage src={downloads.summer02} />
        <CanvasDithering palette={palette} />
        <Code
          left={-10}
          top={475}
          style={{
            fillStyle: "#fff",
            font: "46px helvetica,arial",
            glitch: false,
            lineHeightAsPx: 46 * 1.2,
          }}
          onDone={async () => {
            await sleep(7_000);
            nextStep();
          }}
        >
          {`
          ${poem[1]}
        `.trim()}
        </Code>
      </Step>
      <Step step="3">
        <CanvasImage src={downloads.summer03} />
        <CanvasDithering palette={palette} />
        <Code
          left={-10}
          top={425}
          style={{
            fillStyle: "#fff",
            font: "46px helvetica,arial",
            glitch: false,
            lineHeightAsPx: 46 * 1.2,
          }}
          onDone={async () => {
            await sleep(7_000);
            nextStep();
          }}
        >
          {`
          ${poem[2]}
        `.trim()}
        </Code>
      </Step>
      <Step step="4">
        <CanvasImage src={downloads.summer04} />
        <CanvasDithering palette={palette} />
        <Code
          left={-5}
          top={205}
          style={{
            fillStyle: "#fff",
            font: "46px helvetica,arial",
            glitch: false,
            lineHeightAsPx: 46 * 1.2,
          }}
          onDone={async () => {
            await sleep(7_000);
            nextStep();
          }}
        >
          {`
          ${poem[3]}
        `.trim()}
        </Code>
      </Step>
      <Step step="5">
        <CanvasImage src={downloads.summer05} />
        <CanvasDithering palette={palette} />
        <Code
          left={435}
          top={105}
          style={{
            fillStyle: "#fff",
            font: "46px helvetica,arial",
            glitch: false,
            lineHeightAsPx: 46 * 1.2,
          }}
          onDone={async () => {
            await sleep(7_000);
            nextStep();
          }}
        >
          {`
          ${poem[4]}
        `.trim()}
        </Code>
      </Step>
      <Step step="6">
        <CanvasImage src={downloads.summer06} />
        <CanvasDithering palette={palette} />
        <Code
          left={-10}
          top={23}
          style={{
            fillStyle: "#fff",
            font: "46px helvetica,arial",
            glitch: false,
            lineHeightAsPx: 46 * 1.2,
          }}
          onDone={async () => {
            await sleep(7_000);
            // nextStep();
          }}
        >
          {`
          ${poem[5]}
        `.trim()}
        </Code>
      </Step>
    </Canvas>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Poem />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
