import {
  ActionBlock,
  Canvas,
  OneBit,
  Panel,
  StaticDiv,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  intro: {
    type: "sound",
    url: "/art/random/2024-05-02/intro.mp3",
  },
  ticking: {
    type: "sound",
    url: "/art/random/2024-05-02/ticking.mp3",
  },
  laughing: {
    type: "sound",
    url: "/art/random/2024-05-02/laughing.mp3",
  },
  cricket: {
    type: "sound",
    url: "/art/random/2024-05-02/cricket.mp3",
  },
  scene01: {
    type: "video",
    url: "/art/random/2024-05-02/01.mp4",
  },
  scene02: {
    type: "video",
    url: "/art/random/2024-05-02/02.mp4",
  },
  scene03: {
    type: "video",
    url: "/art/random/2024-05-02/03.mp4",
  },
} as const;

const width = 512;
const height = 512;

// const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
// const macPaintPalette = ["#051b2c", "#8bc8fe"] as const;
// const pixelInkPalette = ["#3e232c", "#edf6d6"] as const;
// const bitBeePalette = ["#292b30", "#cfab4a"] as const;
// const neutralGreen = ["#004c3d", "#ffeaf9"] as const;
// const chasingLight = ["#000000", "#ffff02"] as const;
// const ongBit = ["#151d24", "#ed8463"] as const;
// const flowersAsbestosPalette = ["#c62b69", "#edf4ff"] as const;
// const peachyKeenPalette = ["#242234", "#facab8"] as const;
// const funkyJamPalette = ["#920244", "#fec28c"] as const;
// const macao12Palette = ["#fff2df", "#ef243a"] as const;
// const yPopItPalette = ["#00d4ff", "#d61406"] as const;
// const yPostApolalytpicSunsetPalette = ["#1d0f44", "#f44e38"] as const;
const oneBitColor = ["#073823", "white"] as const;

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const FirstPass = (props: { onDone: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async () => {
              downloads.ticking.resource.play();
              downloads.intro.resource.play();
            }}
          </ActionBlock>
          <Video
            src={downloads.scene01}
            loop={false}
            renderTop={0}
            renderLeft={0}
            renderWidth={width}
            renderHeight={height}
            onEnd={() => {
              page?.("next");
            }}
          />
          <OneBit threshold={175} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async () => {
              downloads.laughing.resource.play();
            }}
          </ActionBlock>
          <Video
            src={downloads.scene02}
            loop={false}
            renderTop={0}
            renderLeft={0}
            renderWidth={width}
            renderHeight={height}
            onEnd={() => {
              page?.("next");
            }}
          />
          <OneBit threshold={175} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async () => {
              downloads.cricket.resource.play();
            }}
          </ActionBlock>
          <Video
            src={downloads.scene03}
            loop={false}
            renderTop={0}
            renderLeft={0}
            renderWidth={width}
            renderHeight={height}
            onEnd={() => {
              page?.("next");
              props.onDone();
            }}
          />
          <OneBit threshold={175} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

const SecondPass = (props: { onDone: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();
  const [timer, setTimer] = useState(4_000);

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              downloads.ticking.resource.play();
              downloads.intro.resource.play();
              await sleep(timer);
              if (opts.isBlockCancelled) return;
              setTimer((c) => Math.max(0, c - 250));
              page?.("next");
            }}
          </ActionBlock>
          <Video
            src={downloads.scene01}
            loop={false}
            renderTop={0}
            renderLeft={0}
            renderWidth={width}
            renderHeight={height}
          />
          <OneBit threshold={175} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              downloads.laughing.resource.play();
              await sleep(timer);
              if (opts.isBlockCancelled) return;
              setTimer((c) => Math.max(0, c - 250));
              page?.("next");
            }}
          </ActionBlock>
          <Video
            src={downloads.scene02}
            loop={false}
            renderTop={0}
            renderLeft={0}
            renderWidth={width}
            renderHeight={height}
          />
          <OneBit threshold={175} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              downloads.cricket.resource.play();
              await sleep(timer);
              if (opts.isBlockCancelled) return;
              if (timer === 0) props.onDone();
              else setTimer((c) => c - 500);
              page?.("next");
            }}
          </ActionBlock>
          <Video
            src={downloads.scene03}
            loop={false}
            renderTop={0}
            renderLeft={0}
            renderWidth={width}
            renderHeight={height}
          />
          <OneBit threshold={175} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

const ComponentInner = () => {
  const [state, setState] = React.useState(0);
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      {state === 0 && <FirstPass onDone={() => setState(1)} />}
      {state === 1 && <SecondPass onDone={() => setState(2)} />}
      {state === 2 && (
        <StoryBoard disableManualNavigation>
          <Panel>
            <ActionBlock>
              {async () => {
                downloads.cricket.resource.stop();
                downloads.intro.resource.stop();
                downloads.laughing.resource.stop();
              }}
            </ActionBlock>
            <StaticDiv
              cx={width / 2}
              cy={height / 2}
              width={width}
              height={height}
              style={{
                zIndex: 4,
                width: "100%",
                height: "100%",
                background: "black",
              }}
            />
          </Panel>
        </StoryBoard>
      )}
    </>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
