import {
  AsciiFullColor,
  Canvas,
  CanvasDithering,
  CanvasImage,
  CanvasSaveImage,
  CanvasSaveVideoProvider,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  canvas: {
    type: "image",
    url: "./random/2024/06-23/canvas.png",
  },
  canvas2: {
    type: "image",
    url: "./random/2024/06-23/canvas2.png",
  },
} as const;

const width = 512;
//const height = 1024;
const height = (512 * 22) / 8.5;
const imageWidth = (width * 7.5) / 8.5;
const imageHeight = (width * 20) / 9;

const oneBitColor = ["black", "white"] as const;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const red1 = [
  /* Color Theme Swatches in Hex */
  "#D90D1E",
  "#BF0426",
  "#8C031C",
  "#F20F38",
  "#D9ADAD",
] as const;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const red2 = ["#a6242f", "#d93d4a", "#215ba6", "#2484bf", "#f2f2f2"] as const;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const red3 = [
  /* Color Theme Swatches in Hex */
  "#49C2F2",
  "#BF0426",
  "#8C031C",
  "#F20F38",
  "#D9ADAD",
] as const;

const red = red2;

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.canvas}
            renderTop={35}
            renderLeft={25}
            renderWidth={imageWidth}
            renderHeight={imageHeight}
          />
          <AsciiFullColor
            resolutionX={64}
            resolutionY={64 * 1.9}
            characterMap={"01234567"}
            style={{ bgColor: "#fff" }}
          />
          <CanvasSaveImage Button={<button>save</button>} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.canvas2}
            renderTop={0}
            renderLeft={0}
            renderWidth={width}
            renderHeight={height}
          />
          <CanvasDithering palette={red} />
          <CanvasSaveImage Button={<button>save</button>} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <CanvasSaveVideoProvider>
        <ComponentInner />
      </CanvasSaveVideoProvider>
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
