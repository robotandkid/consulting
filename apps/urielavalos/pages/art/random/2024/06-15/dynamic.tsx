import {
  Canvas,
  CanvasSaveVideo,
  CanvasSaveVideoProvider,
  Panel,
  sleep,
  SpriteSheetFromMany,
  StoryBoard,
  UiPanel,
  useCanvasSaveVideoCtx,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  a01: {
    type: "image",
    url: "./random/2024/06-15/a/01.png",
  },
  a02: {
    type: "image",
    url: "./random/2024/06-15/a/02.png",
  },
  a03: {
    type: "image",
    url: "./random/2024/06-15/a/03.png",
  },
  a04: {
    type: "image",
    url: "./random/2024/06-15/a/04.png",
  },
  a05: {
    type: "image",
    url: "./random/2024/06-15/a/05.png",
  },
  a06: {
    type: "image",
    url: "./random/2024/06-15/a/06.png",
  },
  a07: {
    type: "image",
    url: "./random/2024/06-15/a/07.png",
  },
  b01: {
    type: "image",
    url: "./random/2024/06-15/b/01.png",
  },
  b02: {
    type: "image",
    url: "./random/2024/06-15/b/02.png",
  },
  b03: {
    type: "image",
    url: "./random/2024/06-15/b/03.png",
  },
  b04: {
    type: "image",
    url: "./random/2024/06-15/b/04.png",
  },
  b05: {
    type: "image",
    url: "./random/2024/06-15/b/05.png",
  },
  c01: {
    type: "image",
    url: "./random/2024/06-15/c/01.png",
  },
  c02: {
    type: "image",
    url: "./random/2024/06-15/c/02.png",
  },
  c03: {
    type: "image",
    url: "./random/2024/06-15/c/03.png",
  },
  c04: {
    type: "image",
    url: "./random/2024/06-15/c/04.png",
  },
  c05: {
    type: "image",
    url: "./random/2024/06-15/c/05.png",
  },
  c06: {
    type: "image",
    url: "./random/2024/06-15/c/06.png",
  },
} as const;

const width = 512;
const height = (width * 14) / 10;

const oneBitColor = ["black", "white"] as const;

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const video = useCanvasSaveVideoCtx();

  return (
    <StoryBoard>
      <Panel>
        <Canvas>
          <SpriteSheetFromMany
            src={[
              downloads.a01,
              downloads.a02,
              downloads.a03,
              downloads.a04,
              downloads.a05,
              downloads.a06,
              downloads.a07,
            ]}
            renderLeft={-50}
            renderTop={0}
            renderWidth={width * 1.2}
            renderHeight={height}
            fps={4}
            onLoopEnd={async () => {
              await sleep(4_000);
              video?.stop();
            }}
          />
          <CanvasSaveVideo autoStart />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <SpriteSheetFromMany
            src={[
              downloads.b01,
              downloads.b02,
              downloads.b03,
              downloads.b04,
              downloads.b05,
            ]}
            renderLeft={-50}
            renderTop={0}
            renderWidth={(width * 14) / 10}
            renderHeight={height}
            fps={4}
            onLoopEnd={async () => {
              await sleep(4_000);
              video?.stop();
            }}
          />
          {/* <CanvasSaveVideo /> */}
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <SpriteSheetFromMany
            src={[
              downloads.c01,
              downloads.c02,
              downloads.c03,
              downloads.c04,
              downloads.c05,
              downloads.c06,
            ]}
            renderLeft={-100}
            renderTop={0}
            renderWidth={(width * 14) / 10}
            renderHeight={height}
            fps={4}
            onLoopEnd={async () => {
              await sleep(4_000);
              video?.stop();
            }}
          />
          <CanvasSaveVideo autoStart />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <CanvasSaveVideoProvider>
        <ComponentInner />
      </CanvasSaveVideoProvider>
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
