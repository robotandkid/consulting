import {
  ActionBlock,
  Canvas,
  CanvasDivAlignLeft,
  CanvasImage,
  CanvasSaveImage,
  CanvasSpan,
  palette8spazbol,
  //  palette9smokey09,
  Panel,
  pickAccessibleColor,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/art/love-in-motion/2024/06/17/theme.wav",
  },
  canvas: {
    type: "image",
    url: "/art/random/2024/10-03/canvas.png",
  },
} as const;

const aspectRatio = 16 / 9;
const width = 512;
const height = width * aspectRatio;

const oneBitColor = ["#fff", "#000"] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

// const quotes = [
//   `It’s amazing how we came from different countries and different backgrounds, and we were able to work together and accomplish so much.`,
//   `We need to continue our legacy of being a beacon of hope for people from all over the world who come here seeking freedom and opportunity.`,
// ].map((q) => q.replace(/ /g, "").replace(/\./g, " ")) as [string, string];

// const formatQuote = (quote: string) =>
//   quote
//     .split(".")
//     .flatMap((quote) => quote.split(","))
//     .flatMap((quote) => quote.split("?"))
//     .map((text) => text.replace(/ /g, ""));

const rawQuotes = [
  "You never tell me what you want",
  "You always blame others",
  "You have fits of rage",
  "You withhold affection",
  "You constantly criticize me",
  "You send me on guilt trips",
  "You give me the silent treatment",
  "You make hostile comments",
  "You give me anxiety",
];

const textPalette = palette8spazbol();

const randomTextColor = () => {
  const textColor =
    textPalette[Math.floor(Math.random() * textPalette.length)]!;
  const bgColor = pickAccessibleColor(textColor, textPalette);
  return [bgColor, textColor] as const;
};

const quotes = rawQuotes.map((quote) => {
  const color = randomTextColor();
  return { quote, textColor: color[0], bgColor: color[1] };
});

const Frame = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <>
      <ActionBlock>
        {async () => {
          downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <CanvasImage src={downloads.canvas} />
      {quotes.map((quote, index) => {
        const neg = index % 2 === 0 ? 1 : -1;
        const rotations = [0, 1, 2, 3];
        const rotation =
          rotations[Math.floor(Math.random() * rotations.length)] ?? 0;

        return (
          <CanvasDivAlignLeft
            key={quote.quote}
            pos={{
              left: width * 0.02,
              top: width * 0.1 + width * 0.15 * index,
              width: width,
              height: height,
            }}
            style={{
              lineHeightMultiple: 1.6,
            }}
          >
            <CanvasSpan
              style={{
                colorStyle: quote.textColor,
                bgStyle: quote.bgColor,
                font: "32px mrpixel",
              }}
              transform={{ rotate: neg * rotation }}
            >
              {quote.quote}
            </CanvasSpan>
          </CanvasDivAlignLeft>
        );
      })}

      <CanvasSaveImage Button={<button>Save</button>} />
    </>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas disableClearCanvasBeforeUpdate>
          <Frame />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
