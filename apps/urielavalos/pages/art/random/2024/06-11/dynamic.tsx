import {
  AsciiFullColor,
  AsciiLightColor,
  Canvas,
  CanvasDithering,
  CanvasImage,
  CanvasSaveImage,
  Code,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  useStateSteps,
  World,
} from "@robotandkid/sprite-motion";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  front: {
    type: "image",
    url: "/art/random/2024/06-11/front.png",
  },
  back: {
    type: "image",
    url: "/art/random/2024/06-11/back.png",
  },
  front3: {
    type: "image",
    url: "/art/random/2024/06-11/front4.png",
  },
  back3: {
    type: "image",
    url: "/art/random/2024/06-11/back4.png",
  },
  lily: {
    type: "image",
    url: "/art/random/2024/06-11/lily.png",
  },
  melding: {
    type: "image",
    url: "/art/random/2024/06-11/melding.png",
  },
  heartRiver: {
    type: "image",
    url: "/art/random/2024/06-11/river.png",
  },
  starWars: {
    type: "image",
    url: "/art/random/2024/06-11/star-wars.png",
  },
} as const;

const tshirt = {
  blue: "#c1d6ea",
  darkBlue: "#004b8d",
} as const;

const oneBitColor = [tshirt.darkBlue, "black"] as const;

// Be happy for this moment. This moment is your life

// Only the present moment is real.

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const frontWidth1 = 1000;
const frontHeight1 = (frontWidth1 * 10) / 6;

const frontWidth2 = 4000;
const frontHeight2 = 4000;
// const fontSize2 = (72 / 1000) * frontWidth2;

const Design1Inner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.front}
            renderTop={-50}
            renderLeft={0}
            renderWidth={frontWidth1}
            renderHeight={frontHeight1}
          />
          <AsciiFullColor
            resolutionX={55}
            resolutionY={55 * 1.1}
            style={{
              fontFamily: "monospace",
              bgColor: oneBitColor[0],
            }}
          />
          <Code
            left={frontWidth1 * 0.1}
            top={frontHeight1 * 0.51}
            style={{
              glitch: false,
              fillStyle: "#fff",
              font: "bold 72px baoli sc",
              lineHeightAsPx: 72,
            }}
          >
            {`
この瞬間を幸せに。
この瞬間があなたの人生
          `.trim()}
          </Code>
          <CanvasSaveImage Button={<button>Save </button>} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.back}
            renderTop={frontHeight1 * 0.25}
            renderLeft={0}
            renderWidth={frontWidth1}
            renderHeight={frontHeight1}
          />
          <AsciiFullColor
            resolutionX={55}
            resolutionY={55 * 1.1}
            style={{
              fontFamily: "monospace",
              bgColor: oneBitColor[0],
            }}
          />
          <Code
            left={frontWidth1 * 0.16}
            top={frontHeight1 * 0.1}
            style={{
              glitch: false,
              fillStyle: "#fff",
              font: "bold 92px baoli sc",
              lineHeightAsPx: 94,
            }}
          >
            {`
今の瞬間だけが
現実だ
            `.trim()}
          </Code>
          <CanvasSaveImage Button={<button>Save </button>} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

const JapaneseFlowers = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.front3}
            renderTop={0}
            renderLeft={0}
            renderWidth={frontWidth2}
            renderHeight={frontHeight2}
          />
          <AsciiLightColor
            resolutionX={75}
            resolutionY={45}
            lightPercentFactor={0.3}
            darkPercentFactor={0.5}
            style={{
              fontFamily: "monospace",
            }}
          />
          {/* <Code
            left={frontWidth2 * 0.15}
            top={frontHeight2 * 0.8}
            style={{
              glitch: false,
              fillStyle: "white",
              font: "bold 72px baoli sc",
              lineHeightAsPx: 72,
            }}
          >
            {`
この瞬間を幸せに。
この瞬間があなたの人生
          `.trim()}
          </Code> */}
          <CanvasSaveImage Button={<button>Save </button>} />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.back3}
            renderTop={frontHeight2 * 0.25}
            renderLeft={0}
            renderWidth={frontWidth2}
            renderHeight={frontHeight2}
          />
          <AsciiFullColor
            resolutionX={125}
            resolutionY={125 * 1.1}
            style={{
              fontFamily: "monospace",
              bgColor: oneBitColor[0],
            }}
          />
          {/* <Code
            left={frontWidth2 * 0.2}
            top={frontHeight2 * 0.22}
            style={{
              glitch: false,
              fillStyle: "white",
              font: `bold ${fontSize2}px baoli sc`,
              lineHeightAsPx: fontSize2 * 0.8,
            }}
          >
            {`
今の瞬間だけが
現実だ
            `.trim()}
          </Code> */}
          <CanvasSaveImage Button={<button>Save </button>} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

const meldingWidth = 4000;
const meldingHeight = meldingWidth;

const Melding = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.melding}
            renderTop={0}
            renderLeft={0}
            renderWidth={meldingWidth}
            renderHeight={meldingHeight}
          />
          <AsciiLightColor
            resolutionX={175}
            resolutionY={(175 * 74) / 125 + 2}
            lightPercentFactor={0.2}
            darkPercentFactor={0.2}
            style={{
              fontFamily: "monospace",
            }}
          />
          <CanvasSaveImage Button={<button>Save </button>} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const MeldingWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={meldingWidth}
      height={meldingHeight}
    >
      <Melding />
    </World>
  );
};

const heartRiverWidth = 4000;
const heartRiverHeight = 4000;

const HeartLikeARiver = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasImage
            src={downloads.heartRiver}
            renderTop={0}
            renderLeft={0}
            renderWidth={heartRiverWidth}
            renderHeight={heartRiverHeight}
          />
          <AsciiLightColor
            resolutionX={75}
            resolutionY={46}
            // resolutionY={(75 * 74) / 125 + 2}
            // resolutionX={175}
            // resolutionY={(175 * 74) / 125 + 2}
            lightPercentFactor={0.1}
            darkPercentFactor={0.5}
            style={{
              fontFamily: "monospace",
            }}
          />
          <CanvasSaveImage Button={<button>Save </button>} />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const HeartLikeRiverWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={heartRiverWidth}
      height={heartRiverHeight}
    >
      <HeartLikeARiver />
    </World>
  );
};

const starWarsFactory = () => {
  const width = 4000;
  const height = 4000;

  const Fixture = () => {
    const { downloads } = useDownloadsCtx<typeof resources>();

    return (
      <StoryBoard>
        <Panel>
          <Canvas>
            <CanvasImage
              src={downloads.starWars}
              renderTop={0}
              renderLeft={0}
              renderWidth={width}
              renderHeight={height}
            />
            <AsciiLightColor
              // resolutionX={75}
              // resolutionY={46}
              resolutionX={175}
              resolutionY={(175 * 74) / 125 + 2}
              lightPercentFactor={0.3}
              darkPercentFactor={0.5}
              style={{
                fontFamily: "monospace",
              }}
            />
            <CanvasSaveImage Button={<button>Save </button>} />
          </Canvas>
        </Panel>
      </StoryBoard>
    );
  };

  const FixtureWorld = () => {
    return (
      <World
        resources={resources}
        loading={<div>Loading...</div>}
        style={{
          background: oneBitColor[0],
        }}
        width={width}
        height={height}
      >
        <Fixture />
      </World>
    );
  };

  return FixtureWorld;
};

const StarWarsWorld = starWarsFactory();

const nextButton = (
  <button style={{ position: "absolute", zIndex: 999, top: 0 }}>Next</button>
);

const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width: 512,
    height: 512,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

export const FrontWorld1 = () => {
  return (
    <IntroContainer width={frontWidth1} height={frontHeight1}>
      <World
        resources={resources}
        loading={<div>Loading...</div>}
        style={{
          background: oneBitColor[0],
        }}
        width={frontWidth1}
        height={frontHeight1}
      >
        <Design1Inner />
      </World>
    </IntroContainer>
  );
};

export const FrontWorld2 = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={frontWidth2}
      height={frontHeight2}
    >
      <JapaneseFlowers />
    </World>
  );
};

const lilyWidth = 2000;
const lilyHeight = 2000;

const LilyInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Panel>
      <Canvas>
        <CanvasImage
          src={downloads.lily}
          renderLeft={0}
          renderTop={0}
          renderWidth={lilyWidth}
          renderHeight={lilyHeight}
        />
        <CanvasDithering palette={["#000", "#fff"]} />
        <CanvasSaveImage Button={<button>Save </button>} />
      </Canvas>
    </Panel>
  );
};

const LilyWorld = () => {
  return (
    <IntroContainer width={lilyWidth} height={lilyHeight}>
      <World
        resources={resources}
        loading={<div>Loading...</div>}
        style={{
          background: oneBitColor[0],
        }}
        width={lilyWidth}
        height={lilyHeight}
      >
        <StoryBoard>
          <LilyInner />
        </StoryBoard>
      </World>
    </IntroContainer>
  );
};

const Component = () => {
  const [StepShow, ConnectedStartButton] = useStateSteps({
    steps: ["hide", "show"],
    Button: (
      <CustomUiPanel title="Press" width="25rem">
        <StartButton>Start</StartButton>
      </CustomUiPanel>
    ),
  });

  const [Step, ConnectedNextButton] = useStateSteps({
    steps: ["star-wars", "heart-river", "melding", "design1", "front2", "lily"],
    Button: nextButton,
  });

  return (
    <div style={{ position: "relative" }}>
      <StepShow step="hide">
        <IntroContainer>
          <ConnectedStartButton />
        </IntroContainer>
      </StepShow>
      <StepShow step="show">
        <Step step="star-wars">
          <StarWarsWorld />
          <ConnectedNextButton />
        </Step>
        <Step step="heart-river">
          <HeartLikeRiverWorld />
          <ConnectedNextButton />
        </Step>
        <Step step="melding">
          <MeldingWorld />
          <ConnectedNextButton />
        </Step>
        <Step step="design1">
          <FrontWorld1 />
          <ConnectedNextButton />
        </Step>
        <Step step="front2">
          <MeldingWorld />
          <ConnectedNextButton />
        </Step>
        <Step step="lily">
          <LilyWorld />
          <ConnectedNextButton />
        </Step>
      </StepShow>
    </div>
  );
};

export default Component;
