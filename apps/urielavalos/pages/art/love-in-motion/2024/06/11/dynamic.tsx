import {
  ActionBlock,
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import React, { useState } from "react";
import { sleep } from "../../../../../../src/utils/promises";
import { Chromata } from "@robotandkid/sprite-motion";

const resources = {
  theme: {
    type: "sound",
    url: "/art/love-in-motion/2024/06/11/theme.mp3",
  },
  canvas: {
    type: "image",
    url: "/art/love-in-motion/2024/06/11/canvas.jpg",
  },
} as const;

const width = 512;
const height = (512 * 4) / 3;

const oneBitColor = ["#39AA59", "white"] as const;
const palette = [
  "#010501",
  "#FFBF0D",
  "#FF0000",
  "#F9FFFF",
  "#159DFF",
] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              downloads.theme.resource.play({ loop: true });
              await sleep(55_000);
              if (opts.isBlockCancelled) return;
              downloads.theme.resource.stop();
              page?.("next-fade");
            }}
          </ActionBlock>
          <Chromata
            options={{
              pathFinderCount: 50,
              speed: 10,
              turningAngle: Math.PI / 10,
              colorMode: "color",
              palette,
              lineWidth: 3,
              lineMode: "smooth",
              // hue
              // luminosity
              compositeOperation: "hard-light",
              origin: ["50% 50%"],
              key: "low",
            }}
            imageSrc={downloads.canvas}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
