import {
  ActionBlock,
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import React, { useState } from "react";
import { sleep } from "../../../../../../src/utils/promises";
import { Chromata } from "@robotandkid/sprite-motion";

const resources = {
  theme: {
    type: "sound",
    url: "/art/love-in-motion/2024/06/13/theme.wav.mp3",
  },
  canvas: {
    type: "image",
    url: "/art/love-in-motion/2024/06/13/canvas.png",
  },
} as const;

const width = 512;
const height = (width * 4) / 3;

const oneBitColor = ["#354333", "white"] as const;
// const palette = ["#ED5AC8", "#7F306B"] as const;
const palette = [
  "#ffffff",
  "#e557c1",
  "#c34aa4",
  "#ed5ac8",
  "#a13d88",
  "#7f306b",
] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <ActionBlock>
            {async (opts) => {
              downloads.theme.resource.play({ loop: true });
              await sleep(32_000);
              if (opts.isBlockCancelled) return;
              downloads.theme.resource.stop();
              page?.("next-fade");
            }}
          </ActionBlock>
          <Chromata
            options={{
              pathFinderCount: 50,
              speed: 10,
              turningAngle: Math.PI / 2,
              colorMode: "color",
              palette,
              lineWidth: 3,
              lineMode: "point",
              // color-burn
              compositeOperation: "hard-light",
              origin: ["40% 89%"], //, "60% 60%", "50% 80%"],
              key: "low",
            }}
            imageSrc={downloads.canvas}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
