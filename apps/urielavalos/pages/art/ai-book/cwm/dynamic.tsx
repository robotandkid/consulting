import {
  Canvas,
  CanvasImage,
  OneBit,
  Panel,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import React, { useEffect, useRef, useState } from "react";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";
import { WrapMeSpeak } from "../../../../src/components/MeSpeak/MeSpeak";
import { DialogMeSpeak } from "../../../../src/components/MeSpeak/DialogMeSpeak";
import { ResourceImg } from "@robotandkid/sprite-motion";
import { sleep } from "@robotandkid/sprite-motion";
import { useAbortCtrlCtx } from "@robotandkid/sprite-motion";
import { ActionBlock } from "@robotandkid/sprite-motion";

const resources = {
  theme: {
    type: "sound",
    url: "/art/ai-books/cwm/theme.mp3",
  },
  page01: {
    type: "image",
    url: "/art/ai-books/cwm/scene01.jpg",
  },
  page02: {
    type: "image",
    url: "/art/ai-books/cwm/scene02.jpg",
  },
  page03: {
    type: "image",
    url: "/art/ai-books/cwm/scene03.jpg",
  },
  page04a: {
    type: "image",
    url: "/art/ai-books/cwm/scene04.jpg",
  },
  page04b: {
    type: "image",
    url: "/art/ai-books/cwm/scene04b.jpg",
  },
  page05a: {
    type: "image",
    url: "/art/ai-books/cwm/scene05.jpg",
  },
  page05b: {
    type: "image",
    url: "/art/ai-books/cwm/scene05b.jpg",
  },
  page06: {
    type: "image",
    url: "/art/ai-books/cwm/scene06.jpg",
  },
  page07a: {
    type: "image",
    url: "/art/ai-books/cwm/scene07a.jpg",
  },
  page07b: {
    type: "image",
    url: "/art/ai-books/cwm/scene07b.jpg",
  },
  page07c: {
    type: "image",
    url: "/art/ai-books/cwm/scene07c.jpg",
  },
  page07d: {
    type: "image",
    url: "/art/ai-books/cwm/scene07d.jpg",
  },
  page07e: {
    type: "image",
    url: "/art/ai-books/cwm/scene07e.jpg",
  },
} as const;

const dialog = {
  page1: `
Across an ocean, over lots of huge bumpy
mountains, across three hot deserts, and
one smaller ocean...`,
  page2: `
...there lay the tiny town of
Chewandswallow.`,
  page3: `
In most ways, it was very much like any
other tiny town. It had a Main Street
lined with stores, houses with trees and
gardens.`,
  page4a: `
But there were no food stores in the
town of Chewandswallow. They did't need
any. The sky supplied all the food they
could possibly want.`,
  page4b: `
It came three times a day, at breakfast,
lunch, and dinner. Everything that 
everyone ate came from the sky.`,
  page5a: `
Whatever the weather served that was 
what they ate. But it never rained rain.
It never snowed snow.`,
  page5b: `
It rained things like soup and juice. It
snowed mashed potatoes and green peas.
And sometimes the wind blew in storms
of hamburgers.
  `,
  page6: `
The people could watch the weather report
on television in the morning. And they 
would even hear predictions for the next
day's food.`,
  page7: `
When the townspeople went outside, they
carried their plates, cups, glasses,
forks, spoons, knives and napkins with
them.
That way they would always be prepared for
any kind of weather. 


If there were leftovers, and there usually
were, the people took them home and put
them in their refrigerators in case they
got hungry between meals.
`,
};

const width = 512;
const height = 512;

const oneBitColor = ["black", "white"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
    width: "[100%]",
    height: "[7em]",
    bottom: "[0px]",
  },
});

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const CycleImages = (props: { images: ResourceImg[]; delay: number }) => {
  const { delay } = props;
  const [image, setImage] = useState<ResourceImg>();
  const ctrl = useAbortCtrlCtx();
  const images = useRef(props.images);
  images.current = props.images;

  useEffect(() => {
    const go = async () => {
      for (const cur of images.current) {
        setImage(cur);
        await sleep(delay);
        if (ctrl?.signal.aborted) return;
      }
    };
    go();
  }, [ctrl?.signal.aborted, delay]);

  if (!image) return null;

  return (
    <>
      <CanvasImage key={image.url} src={image} />
      <OneBit key={image.url} threshold={200} />
    </>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {() => {
            downloads.theme.resource.play({ loop: true });
          }}
        </ActionBlock>
        <Canvas>
          <CanvasImage src={downloads.page01} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page1}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page02} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page2}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page03} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page3}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page04a} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page4a}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page04b} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page4b}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page05a} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page5a}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page05b} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page5b}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page06} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page6}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CycleImages
            images={[
              downloads.page07a,
              downloads.page07b,
              downloads.page07c,
              downloads.page07d,
              downloads.page07e,
            ]}
            delay={4_500}
          />
          <DialogMeSpeak
            stop
            dialog={dialog.page7}
            Dialog={DialogPanel}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={(height * 4) / 3}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height: (height * 4) / 3,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <WrapMeSpeak>
      <IntroContainer>
        {!show && (
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore don't know why it doesn't recodnize the click handler
          <CustomUiPanel
            title="Press"
            onClick={() => setShow(true)}
            width="25rem"
          >
            <StartButton>Start</StartButton>
          </CustomUiPanel>
        )}
        {show && <ComponentWorld />}
      </IntroContainer>
    </WrapMeSpeak>
  );
};

export default Component;
