import {
  ActionBlock,
  Canvas,
  CanvasImage,
  CanvasSaveVideoProvider,
  OneBit,
  FilmGrain as Original,
  Panel,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  World,
  useDownloadsCtx,
} from "@robotandkid/sprite-motion";
import { useState } from "react";
import { DialogMeSpeak } from "../../../../src/components/MeSpeak/DialogMeSpeak";
import { WrapMeSpeak } from "../../../../src/components/MeSpeak/MeSpeak";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  theme: {
    type: "sound",
    url: "/art/ai-books/captain-jack/theme.mp3",
  },
  scene1: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene1a.jpg",
  },
  scene2: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene2b.jpg",
  },
  scene3: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene3a.jpg",
  },
  scene4: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene4b.jpg",
  },
  scene5: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene5b.jpg",
  },
  scene6: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene6b.jpg",
  },
  scene7: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene7b.jpg",
  },
  scene8: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene8b.jpg",
  },
  scene9: {
    type: "image",
    url: "/art/ai-books/captain-jack/scene9b.jpg",
  },
} as const;

const width = 512;
const height = 512;

const oneBitColor = ["black", "white"] as const;

const FilmGrain = (_: Parameters<typeof Original>[0]) => {
  return null;
};

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const dialog = {
  page1: `
Jack’s bedroom was cozy, filled with toys
and bathed in the soft morning light.
Today was a special day.
Jack was starting at a new school! 
He jumped out of bed, grabbed his 
backpack, and dashed downstairs.
`,
  page2: `
In the kitchen, Mom was making breakfast.
“Ready for your big day, Jack?” she asked.
Jack grinned. “Yes, Mom!”
`,
  page3: `
The schoolyard was big and bustling with 
children playing everywhere. Jack took a
deep breath and walked in. 

Suddenly, he spotted something amazing—a
treehouse in the corner of the yard! 
“Wow, a treehouse!” Jack whispered to
himself.
`,
  page4: `
In the classroom, Jack sat at a desk, 
surrounded by new faces. Mrs. Parker,
the kind teacher, smiled at him.

“Class, we have a new student today.
Everyone, this is Jack. Let’s make him
feel welcome,” she said.

Jack waved shyly. The bell rang for recess,
and everyone rushed outside.
`,
  page5: `
Jack explored the playground, his eyes
fixed on the treehouse. He climbed up and
found a group of kids playing pirates.
Lucas, the leader, looked at Jack.

“Ahoy! Who goes there?” Lucas called out.
“I’m Jack. Can I join your crew?” Jack asked.

“Only if you can find the hidden treasure!”
 Lucas said, pointing to a map on the wall.
`,
  page6: `
Jack followed the treasure map, leading him
on an exciting adventure around the
schoolyard. 

He crawled through tunnels, slid down 
slides, and finally dug in the sandbox.
Suddenly, he found a small treasure chest!
`,
  page7: `
Jack opened the chest to find candy and a
pirate hat. Excited, he raced back to the
treehouse, holding up the treasure for
all to see.


“I found it!” Jack shouted.

The kids cheered and placed the pirate 
hat on Jack’s head.
`,
  page8: `
“Welcome to the crew, Captain Jack!”
Lucas declared.


Jack grinned widely, feeling happy and
accepted. It was the start of many
wonderful adventures with his new friends.
`,
} as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
    width: "[100%]",
    height: "[7em]",
    bottom: "[0px]",
  },
});

const Scene1 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene1} />
      <OneBit threshold={100} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page1}
      />
    </Canvas>
  );
};

const Scene2 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene2} />
      <OneBit threshold={100} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page2.trim()}
      />
    </Canvas>
  );
};

const Scene3 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene3} />
      <OneBit threshold={100} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page3.trim()}
      />
    </Canvas>
  );
};

const Scene4 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene4} />
      <OneBit threshold={100} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page4.trim()}
      />
    </Canvas>
  );
};

const Scene5 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene5} />
      <OneBit threshold={100} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page5.trim()}
      />
    </Canvas>
  );
};

const Scene6 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene7} />
      <OneBit threshold={100} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page6.trim()}
      />
    </Canvas>
  );
};

const Scene7 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene8} />
      <OneBit threshold={50} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page7.trim()}
      />
    </Canvas>
  );
};

const Scene8 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <Canvas>
      <CanvasImage src={downloads.scene9} />
      <OneBit threshold={200} />
      <FilmGrain alpha={255} size={150} />
      <DialogMeSpeak
        Dialog={DialogPanel}
        visibleLines={4}
        dialog={dialog.page8.trim()}
      />
    </Canvas>
  );
};

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <ActionBlock>
          {() => {
            downloads.theme.resource.stop();
            downloads.theme.resource.play({ loop: true });
          }}
        </ActionBlock>
        <Scene1 />
      </Panel>
      <Panel>
        <Scene2 />
      </Panel>
      <Panel>
        <Scene3 />
      </Panel>
      <Panel>
        <Scene4 />
      </Panel>
      <Panel>
        <Scene5 />
      </Panel>
      <Panel>
        <Scene6 />
      </Panel>
      <Panel>
        <Scene7 />
      </Panel>
      <Panel>
        <Scene8 />
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <WrapMeSpeak>
      <World
        resources={resources}
        loading={<div>Loading...</div>}
        style={{
          background: oneBitColor[0],
        }}
        width={width}
        height={height + 128}
      >
        <CanvasSaveVideoProvider>
          <ComponentInner />
        </CanvasSaveVideoProvider>
      </World>
    </WrapMeSpeak>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height: 640,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
