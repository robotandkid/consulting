import {
  ActionBlock,
  Canvas,
  CanvasImage,
  OneBit,
  Panel,
  StoryBoard,
  UiDialogPanel,
  UiPanel,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";
import { WrapMeSpeak } from "../../../../src/components/MeSpeak/MeSpeak";
import { DialogMeSpeak } from "../../../../src/components/MeSpeak/DialogMeSpeak";

const resources = {
  theme: {
    type: "sound",
    url: "/art/ai-books/wwa/theme.mp3",
  },
  page01: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-page-01.jpg",
  },
  page02: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-02.jpg",
  },
  page03: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-03.jpg",
  },
  page04: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-04.jpg",
  },
  page05: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-05.jpg",
  },
  page06: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-06.jpg",
  },
  page07: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-07.jpg",
  },
  page08: {
    type: "image",
    url: "/art/ai-books/wwa/wwa-08.jpg",
  },
} as const;

const dialog = {
  page1: `
The night Max wore his wolf suit
and made mischief of one kind`,
  page2: "and another",
  page3: `
his mother called him "WILD THING!"
and Max said "I'LL EAT YOU UP!"
so he was sent to bed without
eating anything`,
  page4: `
That very night in Max's room
a forest grew`,
  page5: "and grew--",
  page6: `
and grew until his ceiling hung
with vines and the walls became
the world all around`,
  page7: `
and an ocean tumbled by with a
private boat for Max
and he sailed off through night
and day`,
  page8: `
and in and out of weeks
and almost over a year
to where the wild things are
`,
  page9: `
And when he came to the place
where the wild things are
they roared their terrible roars
and gnashed their terrible teeth
`,
  page10: `
till Max said "BE STILL!"
and tamed them with the magic
trick.`,
};

const width = 512;
const height = 512;

// const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
// const macPaintPalette = ["#051b2c", "#8bc8fe"] as const;
// const pixelInkPalette = ["#3e232c", "#edf6d6"] as const;
// const bitBeePalette = ["#292b30", "#cfab4a"] as const;
// const neutralGreen = ["#004c3d", "#ffeaf9"] as const;
// const chasingLight = ["#000000", "#ffff02"] as const;
// const ongBit = ["#151d24", "#ed8463"] as const;
// const flowersAsbestosPalette = ["#c62b69", "#edf4ff"] as const;
// const peachyKeenPalette = ["#242234", "#facab8"] as const;
// const funkyJamPalette = ["#920244", "#fec28c"] as const;
// const macao12Palette = ["#fff2df", "#ef243a"] as const;
// const yPopItPalette = ["#00d4ff", "#d61406"] as const;
// const yPostApolalytpicSunsetPalette = ["#1d0f44", "#f44e38"] as const;
const oneBitColor = ["#4F4126", "white"] as const;

const DialogPanel = pandaStyled(UiDialogPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
    width: "[100%]",
    height: "[8rem]",
    bottom: "[0px]",
  },
});

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const Dialog = (props: {
  onDone?: (() => void) | undefined;
  children: string;
}) => (
  <DialogPanel visibleLines={4} width="100%" height={175} onDone={props.onDone}>
    {props.children}
  </DialogPanel>
);

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <ActionBlock>
          {() => {
            downloads.theme.resource.play({ loop: true });
          }}
        </ActionBlock>
        <Canvas>
          <CanvasImage src={downloads.page01} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page1}
            Dialog={Dialog}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page02} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page2}
            Dialog={Dialog}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page03} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page3}
            Dialog={Dialog}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page04} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page4}
            Dialog={Dialog}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page05} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page5}
            Dialog={Dialog}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page06} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page6}
            Dialog={Dialog}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page07} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page7}
            Dialog={Dialog}
            visibleLines={4}
          />
        </Canvas>
      </Panel>
      <Panel>
        <Canvas>
          <CanvasImage src={downloads.page08} />
          <OneBit threshold={200} />
          <DialogMeSpeak
            dialog={dialog.page8}
            Dialog={Dialog}
            stop
            visibleLines={4}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <WrapMeSpeak>
      <IntroContainer>
        {!show && (
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore don't know why it doesn't recodnize the click handler
          <CustomUiPanel
            title="Press"
            onClick={() => setShow(true)}
            width="25rem"
          >
            <StartButton>Start</StartButton>
          </CustomUiPanel>
        )}
        {show && <ComponentWorld />}
      </IntroContainer>
    </WrapMeSpeak>
  );
};

export default Component;
