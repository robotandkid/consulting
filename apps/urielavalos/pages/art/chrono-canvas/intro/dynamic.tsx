import {
  ActionBlock,
  AsciiFullColor,
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  usePanelCanvasCtx2D,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useEffect, useState } from "react";
import { palette } from "../../../../src/styles/palette";
import { sleep } from "../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../styled-system/jsx/index";

const resources = {
  theme2: {
    type: "sound",
    url: "/art/chrono-canvas/2024/intro/theme2.mp3",
  },
  scene01d: {
    type: "video",
    url: "/art/chrono-canvas/2024/04/06/canvas.mp4",
  },
  scene02: {
    type: "video",
    url: "/art/chrono-canvas/2024/intro/scene02.mp4",
  },
  scene02Audio: {
    type: "sound",
    url: "/art/chrono-canvas/2024/intro/scene02.mp3",
  },
  scene03: {
    type: "video",
    url: "/art/chrono-canvas/2024/intro/scene03.mp4",
  },
  scene03Audio: {
    type: "sound",
    url: "/art/chrono-canvas/2024/intro/scene03.mp3",
  },
  scene03b: {
    type: "video",
    url: "/art/chrono-canvas/2024/intro/scene03b.mp4",
  },
  scene03bAudio: {
    type: "sound",
    url: "/art/chrono-canvas/2024/intro/scene03b.mp3",
  },
  scene04: {
    type: "video",
    url: "/art/chrono-canvas/2024/intro/scene04.mp4",
  },
} as const;

const width = 512;
const height = (512 * 4) / 3;

const oneBitColor = palette.funkyJam;

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const BaseTitle = pandaStyled("div", {
  base: {
    fontFamily: "helvetica, arial, sans-serif",
    position: "absolute",
    color: "white",
    zIndex: 2,
  },
});

const Scene01Title = pandaStyled(BaseTitle, {
  base: {
    paddingLeft: "0.5rem",
    paddingBottom: "0.5rem",
    "& > h1": {
      lineHeight: 1.2,
      fontFamily: "inherit",
      fontSize: "5rem",
      margin: 0,
      padding: 0,
    },
  },
});

const Scene01 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const ctx = usePanelCanvasCtx2D();
  const page = usePagination();

  const sceneWidth = width * 4;
  const sceneHeight = height;
  const [, setLeft] = React.useState(0);

  useEffect(() => {
    if (!ctx) return;
    ctx.canvas.width = sceneWidth;
    ctx.canvas.height = sceneHeight;
    // ctx.filter = "blur(5px)";

    const int = setInterval(() => {
      if (!ctx) return;

      setLeft((left) => {
        ctx.canvas.setAttribute(
          "style",
          `position: absolute; left: ${left - 5}px;`
        );
        return left - 5;
      });
    }, 100);
    return () => {
      clearInterval(int);
      ctx.canvas.width = width;
      ctx.canvas.height = height;
    };
  }, [ctx, sceneWidth, sceneHeight]);

  return (
    <Canvas>
      <ActionBlock>
        {async () => {
          // downloads.theme.resource.play();
          await sleep(3_000);
          page?.("next-fade");
        }}
      </ActionBlock>
      <Video
        src={downloads.scene01d}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      <Video
        src={downloads.scene01d}
        renderTop={0}
        renderLeft={width * 1}
        renderWidth={width}
        renderHeight={height}
      />
      <Video
        src={downloads.scene01d}
        renderTop={0}
        renderLeft={width * 2}
        renderWidth={width}
        renderHeight={height}
      />
      <Video
        src={downloads.scene01d}
        renderTop={0}
        renderLeft={width * 3}
        renderWidth={width}
        renderHeight={height}
      />
      <AsciiFullColor resolutionX={250} resolutionY={60} />
      <Scene01Title>
        <h1>robotandkid</h1>
        <h1>presents</h1>
        <h1>Chrono Canvas</h1>
      </Scene01Title>
    </Canvas>
  );
};

const ProfTitle = pandaStyled(Scene01Title, {
  base: {
    left: 0,
    bottom: "1rem",
    "& > h1": {
      fontSize: "3rem",
    },
    "& > h2": {
      fontSize: "2rem",
      margin: 0,
      padding: 0,
    },
  },
});

const Scene02 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.scene02Audio.resource.play();
        }}
      </ActionBlock>
      <Video
        src={downloads.scene02}
        loop={false}
        onEnd={async () => {
          await sleep(1000);
          page?.("next-fade");
        }}
      />
      <ProfTitle>
        <h1>Pfelton Sutton</h1>
        <h2>Art Collector</h2>
      </ProfTitle>
    </Canvas>
  );
};

const Scene03 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.scene03Audio.resource.play();
        }}
      </ActionBlock>

      <Video
        src={downloads.scene03}
        loop={false}
        onEnd={async () => {
          await sleep(1000);
          page?.("next-fade");
        }}
      />
      <ProfTitle>
        <h1>Sumiyo Iwasawa</h1>
        <h2>Art Professor</h2>
      </ProfTitle>
    </Canvas>
  );
};

const Scene03b = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.scene03bAudio.resource.play();
        }}
      </ActionBlock>

      <Video
        src={downloads.scene03b}
        loop={false}
        onEnd={async () => {
          await sleep(1000);
          page?.("next-fade");
        }}
      />
      <ProfTitle>
        <h1>John Bueno</h1>
        <h2>Homeless Person</h2>
      </ProfTitle>
    </Canvas>
  );
};

const Scene04OldPrice = pandaStyled(BaseTitle, {
  base: {
    top: "2rem",
    left: "1rem",
    "& > div:first-child": {
      fontSize: "2rem",
      textTransform: "uppercase",
    },
    "& > div:nth-child(2)": {
      fontSize: "3rem",
    },
    "& > div:nth-child(3)": {
      animation: "blink 1s steps(5, start) infinite",
      position: "absolute",
      top: "-4rem",
      left: "2.5rem",
      fontSize: "13rem",
      fontWeight: "lighter",
      color: "red",
      lineHeight: "1",
      textAlign: "center",
    },
    background: "rgba(0,0,0,.75)",
  },
});

const Scene04NewPrice = pandaStyled(BaseTitle, {
  base: {
    top: "10rem",
    right: "1rem",
    "& > div:first-child": {
      fontSize: "3rem",
      textTransform: "uppercase",
    },
    "& > div:last-child": {
      fontSize: "4rem",
    },
    background: "rgba(0,0,0,.75)",
  },
});

const Scene04CTA = pandaStyled(BaseTitle, {
  base: {
    bottom: 0,
    "& > div:first-child": {
      fontSize: "3rem",
      textTransform: "uppercase",
      fontStyle: "oblique",
    },
    "& > div:last-child": {
      fontSize: "3rem",
      fontStyle: "oblique",
      textTransform: "uppercase",
    },
  },
});

const Scene04 = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Canvas>
      <ActionBlock>
        {() => {
          downloads.theme2.resource.play();
        }}
      </ActionBlock>
      <Video src={downloads.scene04} />
      <Scene04OldPrice>
        <div>retail value</div>
        <div>0.01 ETH</div>
        <div>☓</div>
      </Scene04OldPrice>
      <Scene04NewPrice>
        <div>pay only</div>
        <div>MINT FEES</div>
      </Scene04NewPrice>
      <Scene04CTA>
        <div>chrono canvas</div>
        <div>Avail on Zora! Link below!</div>
      </Scene04CTA>
    </Canvas>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: "black",
      }}
      width={width}
      height={height}
    >
      <StoryBoard disableManualNavigation>
        <Panel>
          <Scene01 />
        </Panel>
        <Panel>
          <Scene02 />
        </Panel>
        <Panel>
          <Scene03 />
        </Panel>
        <Panel>
          <Scene03b />
        </Panel>
        <Panel>
          <Scene04 />
        </Panel>
      </StoryBoard>
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    // backgroundColor: oneBitColor[0],
    // backgroundColor: oneBitColor[0],
    backgroundColor: `black`,
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
