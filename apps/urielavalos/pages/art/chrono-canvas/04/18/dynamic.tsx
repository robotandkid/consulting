import {
  ActionBlock,
  AsciiFullColor,
  Canvas,
  Panel,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  usePagination,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import React, { useState } from "react";
import { sleep } from "../../../../../src/utils/promises";
import { styled as pandaStyled } from "../../../../../styled-system/jsx/index";

const resources = {
  theme: {
    type: "sound",
    url: "/art/chrono-canvas/2024/04/18/theme.mp3",
  },
  canvas: {
    type: "video",
    url: "/art/chrono-canvas/2024/04/18/canvas.mp4",
  },
} as const;

const width = 512;
const height = (512 * 4) / 3;

// const gbColor = ["#081820", "#346856", "#88c070", "#e0f8d0"] as const;
// const macPaintPalette = ["#051b2c", "#8bc8fe"] as const;
// const pixelInkPalette = ["#3e232c", "#edf6d6"] as const;
// const bitBeePalette = ["#292b30", "#cfab4a"] as const;
// const neutralGreen = ["#004c3d", "#ffeaf9"] as const;
// const chasingLight = ["#000000", "#ffff02"] as const;
// const ongBit = ["#151d24", "#ed8463"] as const;
// const flowersAsbestosPalette = ["#c62b69", "#edf4ff"] as const;
// const peachyKeenPalette = ["#242234", "#facab8"] as const;
// const funkyJamPalette = ["#920244", "#fec28c"] as const;
// const macao12Palette = ["#fff2df", "#ef243a"] as const;
const yPopItPalette = ["#00d4ff", "#d61406"] as const;
const oneBitColor = yPopItPalette;

// const DialogPanel = pandaStyled(UiDialogPanel, {
//   base: {
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//     zIndex: 2,
//     position: "absolute",
//   },
// });

// const PlayerInfoPanel = pandaStyled(UiPlayerInfoPanel, {
//   base: {
//     zIndex: 2,
//     position: "absolute",
//     backgroundColor: oneBitColor[0],
//     color: oneBitColor[1],
//     "& > div": {
//       backgroundColor: oneBitColor[0],
//       borderColor: oneBitColor[1],
//       color: oneBitColor[1],
//     },
//   },
// });

const CustomUiPanel = pandaStyled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

// const CommandPanelContainer = pandaStyled("div", {
//   base: {
//     position: "absolute",
//     zIndex: 2,
//     width: "65%",
//     height: "8rem",
//     top: "14rem !important",
//     // backgroundColor: oneBitColor[0],
//     // color: oneBitColor[1],
//     // "& div, & h1": {
//     //   backgroundColor: oneBitColor[0],
//     //   borderColor: oneBitColor[1],
//     //   color: oneBitColor[1],
//     // },
//   },
// });

// const CommandPanel = pandaStyled(UiCommandPanel, {
//   base: {
//     position: "absolute",
//   },
// });

const StartButton = pandaStyled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const ComponentInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const page = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={width} height={height}>
          <ActionBlock>
            {async (opts) => {
              downloads.theme.resource.play();
              await sleep(60_000);
              if (opts.isBlockCancelled) return;
              downloads.theme.resource.stop();
              page?.("next-fade");
            }}
          </ActionBlock>
          <Video
            src={downloads.canvas}
            loop={true}
            renderTop={-50}
            renderLeft={0}
            renderWidth={width}
            renderHeight={(width * 16) / 9}
          />
          <AsciiFullColor
            resolutionX={55}
            resolutionY={55 * 0.8}
            style={{
              fontFamily: "monospace",
              bgColor: oneBitColor[0],
            }}
          />
        </Canvas>
      </Panel>
      <Panel>
        <ActionBlock>
          {async (opts) => {
            await sleep(1_000);
            if (opts.isBlockCancelled) return;
            page?.("next-fade");
          }}
        </ActionBlock>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = pandaStyled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    width,
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
