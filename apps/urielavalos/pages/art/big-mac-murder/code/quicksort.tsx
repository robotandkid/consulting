export const quickSort = (
  arr: number[]
): number[] => {
  if (arr.length < 2) {
    return arr;
  }
  const pivotIndex = Math.floor(
    Math.random() * arr.length
  );

  const pivot = arr[pivotIndex]!;

  const left = [];
  const right = [];
  const equal = [];

  for (const val of arr) {
    if (val < pivot) {
      left.push(val);
    } else if (val > pivot) {
      right.push(val);
    } else {
      equal.push(val);
    }
  }
  return [
    ...quickSort(left),
    ...equal,
    ...quickSort(right),
  ];
};
