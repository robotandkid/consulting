type Level = {
  level: number;
  type: "end" | "left" | "pivot" | "right";
  arr: number[];
};

function* quickSortStep(arr: number[], level = 1): Generator<Level> {
  if (arr.length < 2) {
    if (arr.length === 1) yield { level, type: "end", arr };
    return;
  }

  const pivot = arr[Math.floor(Math.random() * arr.length)]!;

  const left = [];
  const right = [];
  const equal = [];

  for (const val of arr) {
    if (val < pivot) {
      left.push(val);
    } else if (val > pivot) {
      right.push(val);
    } else {
      equal.push(val);
    }
  }

  if (left.length > 0) yield { level, type: "left", arr: left };
  yield { level, type: "pivot", arr: equal };
  if (right.length > 0) yield { level, type: "right", arr: right };

  yield* quickSortStep(left, level + 1);
  yield* quickSortStep(right, level + 1);
}

export const quickSortLevels = (data: number[]) => {
  const steps: Record<string, Level[]> = {};

  for (const step of quickSortStep(data)) {
    steps[step.level] = steps[step.level] ?? [];
    steps[step.level]?.push(step);
  }

  for (const level of Object.keys(steps)) {
    const step = steps[level];
    // get previous pivot
    const levelNo = +level;
    const prevStep = levelNo > 1 ? steps[levelNo - 1] : undefined;
    const pivots =
      prevStep?.filter((s) => s.type === "pivot" || s.type === "end") ?? [];
    step?.push(...pivots);
    step?.sort((a, b) => {
      const aMax = Math.max(...a.arr);
      const bMin = Math.min(...b.arr);
      return aMax < bMin ? -1 : 1;
    });
  }

  return [[...data]].concat(
    Object.keys(steps).map((level) => {
      const step = steps[level];
      return step?.flatMap((s) => s.arr) ?? [];
    })
  );
};
