import {
  useCanvasRegistration,
  usePanelCanvasCtx2D,
} from "@robotandkid/sprite-motion";
import { useRef } from "react";

const bar = (
  ctx: CanvasRenderingContext2D,
  opts: {
    left: number;
    bottom: number;
    width: number;
    height: number;
    style: {
      lineWidth: number;
      strokeStyle: string;
      fill: boolean;
    };
  }
) => {
  ctx.beginPath();

  // left
  ctx.moveTo(opts.left, opts.bottom);
  ctx.lineTo(opts.left, opts.bottom - opts.height);
  // top
  ctx.lineTo(opts.left + opts.width, opts.bottom - opts.height);
  // right
  ctx.lineTo(opts.left + opts.width, opts.bottom);

  ctx.lineWidth = opts.style.lineWidth;
  ctx.strokeStyle = opts.style.strokeStyle;
  ctx.closePath();
  if (opts.style.fill) ctx.fill();
  else ctx.stroke();
};

const outline = (
  ctx: CanvasRenderingContext2D,
  opts: {
    left: number;
    bottom: number;
    width: number;
    height: number;
    style: {
      lineWidth: number;
      strokeStyle: string;
    };
  }
) => {
  ctx.beginPath();
  ctx.lineWidth = opts.style.lineWidth;
  ctx.strokeStyle = opts.style.strokeStyle;
  ctx.strokeRect(opts.left, opts.bottom - opts.height, opts.width, opts.height);
  ctx.closePath();
};

export const Chart = (props: {
  data: number[];
  top: number;
  left: number;
  width: number;
  height: number;
  style?: {
    lineWidth?: number;
    strokeStyle?: string;
    highlightLast?: boolean;
  };
}) => {
  const { data, top, left, width, height, style: rawStyle } = props;

  const render = useCanvasRegistration();
  const prevKey = useRef<string>();
  const key = `chart:${JSON.stringify(props)}`;

  const style = {
    lineWidth: 5,
    strokeStyle: "white",
    highlightLast: true,
    ...rawStyle,
  };

  const ctx = usePanelCanvasCtx2D();
  const heightMax = Math.max(...data);

  if (prevKey.current !== key && ctx && render) {
    prevKey.current = key;

    render(() => {
      for (let i = 0; i < data.length; i++) {
        const value = data[i]!;
        const offsetX = (width * (1 - 0.9)) / 2;
        const barHeight = (value / heightMax) * (height * 0.9);
        const barWidth = (width * 0.9) / (data.length || 1);

        const highlight = {
          fill: i === data.length - 1 && style.highlightLast ? true : false,
        };

        bar(ctx, {
          width: barWidth,
          height: barHeight,
          left: left + i * barWidth + offsetX,
          bottom: top + height,
          style: {
            ...style,
            ...highlight,
          },
        });
      }

      outline(ctx, {
        left: left,
        bottom: top + height,
        width,
        height,
        style,
      });
    });
  }

  return null;
};
