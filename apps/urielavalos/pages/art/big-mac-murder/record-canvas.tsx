import { usePanelCanvas } from "@robotandkid/sprite-motion";
import React from "react";
import { useEffect, useRef } from "react";

const InternalRecordCanvas = (props: { time: number }) => {
  const { time } = props;
  const canvas = usePanelCanvas();
  const mediaRecorder = useRef<MediaRecorder>();

  if (!mediaRecorder.current && canvas) {
    const videoStream = canvas.captureStream(60);

    mediaRecorder.current = new MediaRecorder(videoStream);
    const chunks: BlobEvent["data"][] = [];

    mediaRecorder.current.ondataavailable = (e) => {
      chunks.push(e.data);
    };

    mediaRecorder.current.onstop = () => {
      if (chunks.length === 0) return;
      const blob = new Blob(chunks, { type: "video/webm" });
      const videoURL = URL.createObjectURL(blob);
      window.open(videoURL);
    };

    mediaRecorder.current.start();
  }

  useEffect(() => {
    const timer = setTimeout(() => mediaRecorder.current?.stop(), time);
    return () => {
      //  mediaRecorder.current?.stop();
      clearTimeout(timer);
    };
  });

  return null;
};

export const RecordCanvas = React.memo(InternalRecordCanvas);
