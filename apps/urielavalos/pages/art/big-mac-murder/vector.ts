export interface Point {
  x: number;
  y: number;
}

const subtract = (a: Point, b: Point) => {
  return { x: a.x - b.x, y: a.y - b.y };
};

const add = (a: Point, b: Point) => {
  return { x: a.x + b.x, y: a.y + b.y };
};

const scale = (t: number, a: Point) => {
  return { x: t * a.x, y: t * a.y };
};

/** @returns A + t*(b - a), so when t = 0, returns a and when t = 1, returns b */
export const line = (a: Point, b: Point, t: number) => {
  const v = subtract(b, a);
  return add(a, scale(t, v));
};
