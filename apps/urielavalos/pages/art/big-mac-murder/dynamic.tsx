import {
  ActionBlock,
  Canvas,
  Code,
  Panel,
  StoryBoard,
  useDownloadsCtx,
  Video,
  World,
} from "@robotandkid/sprite-motion";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { GateButton, PressStart } from "../../../src/styles/styles";
import { useGuardedCallback } from "../../../src/utils/hooks";
import { sleep } from "../../../src/utils/promises";
import { Chart } from "./chart";
import { HUD } from "./hud";
import { Noise } from "./noise";
import { quickSortLevels } from "./quicksort";
import { RecordCanvas } from "./record-canvas";
import { line, Point } from "./vector";

const record = false;
const width = 1820;
const height = (width * 640) / 1152;

const resources = {
  video: {
    type: "video",
    url: `/art/2023/12-26/garbage-sm.mp4`,
  },
  code: {
    type: "text",
    url: "/art/2023/12-26/quicksort.tsx",
  },
  music: {
    type: "sound",
    url: `/art/2023/12-26/${record ? "ui.mp3" : "ui-sm.mp3"}`,
  },
} as const;

/** You have to adjust by the size of the target reticle. */
const randomCoord = () => Math.round(Math.random() * 800 + 100) / 1000;

const randomData = (length: number) =>
  Array.from({ length }).map(() => ({
    cxp: randomCoord(),
    cyp: randomCoord(),
  }));

interface PointP {
  cxp: number;
  cyp: number;
}

const lineP = (a: PointP, b: PointP, t: number) =>
  line({ x: a.cxp, y: a.cyp }, { x: b.cxp, y: b.cyp }, t);

/**
 * Well this got complicated really fast. All of these need to be memoized
 * properly.
 */
const useInterpolate = (props: {
  start: PointP;
  end: PointP;
  increment?: number;
  speed?: number;
  onStep?: (point: Point) => void;
  onDone?: (point: Point) => void;
}) => {
  const { onStep, onDone, increment = 0.08, speed = 50, start, end } = props;
  /** T is between 0 and 1 */
  const t = useRef(0);

  const key = useRef<string>(JSON.stringify({ start, end, increment, speed }));
  const newKey = JSON.stringify({ start, end, increment, speed });
  if (key.current !== newKey) {
    // reset
    key.current = newKey;
    t.current = 0;
  }

  useEffect(() => {
    const interval = setInterval(() => {
      const newT = t.current + increment;
      if (newT < 1.0) {
        onStep?.(lineP(start, end, newT));
        t.current = newT;
        return;
      }
      onDone?.(lineP(start, end, 1.0));
      t.current = 1;
      clearInterval(interval);
    }, speed);
    return () => {
      clearInterval(interval);
    };
  }, [increment, end, onDone, onStep, speed, start]);
};

const CollectData = (props: {
  data: PointP[];
  onStep?: (value: number) => void;
  onDone?: (value: number) => void;
}) => {
  const { onStep, onDone, data } = props;
  const [dataIndex, setDataIndex] = useState(0);

  const targetCoords = useMemo(() => {
    const start = { cxp: 0.1, cyp: 0.1 };
    return [start].concat(data);
  }, [data]);

  const p = {
    x: targetCoords[dataIndex + 1]!.cxp,
    y: targetCoords[dataIndex + 1]!.cyp,
  };

  const [point, setPoint] = useState<Point>(p);

  useInterpolate({
    start: targetCoords[dataIndex]!,
    end: targetCoords[dataIndex + 1]!,
    // speed: 50,
    increment: 0.15,
    onStep: setPoint,
    onDone: useCallback(
      (p: Point) => {
        setDataIndex((c) => {
          if (c + 2 < targetCoords.length) {
            // report to parent the current x value
            onStep?.(+p.x.toFixed(1));
            return c + 1;
          }
          // if we reached the end of all the data, so let 'em know
          onDone?.(+p.x.toFixed(1));
          return c;
        });
      },
      [onDone, onStep, targetCoords.length]
    ),
  });

  return (
    <HUD
      width={700}
      height={600}
      left={500}
      top={200}
      cxp={point.x}
      cyp={point.y}
      style={{ lineWidth: 4 }}
    />
  );
};

const DataAnalysis = (props: { onDone?: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [code, setCode] = useState<string>();

  if (!code) {
    downloads.code.resource.then((text) =>
      setCode(
        `
Analyzing data...
        
${text}`
      )
    );
  }

  return (
    <Code speed={1} top={100} left={50} onDone={props.onDone}>
      {code ?? ""}
    </Code>
  );
};

const Sort = (props: { rawData: number[]; onDone: () => void }) => {
  const { onDone, rawData } = props;
  const [data, _setData] = useState(props.rawData);
  const setData = useGuardedCallback(_setData);

  useEffect(() => {
    const levels = quickSortLevels(rawData);

    const go = async () => {
      for (const level of levels) {
        setData(level);
        await sleep(300);
      }
      onDone();
    };

    go();
  }, [onDone, rawData, setData]);

  return (
    <Chart
      data={data}
      width={400}
      height={600}
      left={1300}
      top={200}
      style={{ highlightLast: false, lineWidth: 4 }}
    />
  );
};

const ComponentInner = (props: { onDone: () => void }) => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [stage, setStage] = useState<"collect" | "analyze" | "sort" | "done">(
    "collect"
  );
  const [collectedData, setCollectedData] = useState<number[]>([]);

  const addToCollection = useCallback((value: number) => {
    setCollectedData((d) => d.concat([value]));
  }, []);

  const [rawData] = useState(randomData(15));

  const onCollectionDone = useCallback(
    (value: number) => {
      addToCollection(value);
      setStage("analyze");
    },
    [addToCollection]
  );

  const onDeployDone = useCallback(() => {
    setStage("sort");
  }, []);

  const onSortDone = useCallback(() => {
    setStage("done");
  }, []);

  return (
    <Canvas width={width} height={height}>
      <ActionBlock>
        {() => {
          downloads.music.resource.play();
        }}
      </ActionBlock>
      <Video src={downloads.video} tintColor="red" />
      <Code speed={10} top={75} left={50}>
        Collecting data...
      </Code>
      <CollectData
        data={rawData}
        onStep={addToCollection}
        onDone={onCollectionDone}
      />
      {(stage === "collect" || stage === "analyze") && (
        <Chart
          data={collectedData}
          width={400}
          height={600}
          left={1300}
          top={200}
          style={{ lineWidth: 4 }}
        />
      )}
      {(stage === "analyze" || stage === "sort" || stage === "done") && (
        <DataAnalysis onDone={onDeployDone} />
      )}
      {(stage === "sort" || stage === "done") && (
        <Sort rawData={collectedData} onDone={onSortDone} />
      )}
      {stage === "done" && (
        <Code
          speed={10}
          top={750}
          left={600}
          style={{
            font: "72px mrpixel",
            lineHeightAsPx: 76,
          }}
          onDone={async () => {
            await sleep(8000);
            props.onDone();
          }}
        >
          {`
Optimal path
to target found!!!`}
        </Code>
      )}
      <Noise time={2000} />
      {record && <RecordCanvas time={16000} />}
    </Canvas>
  );
};

const Component = () => {
  const [key, setKey] = useState(0);
  const [show, setShow] = useState(false);
  return (
    <>
      {!show && (
        <GateButton onClick={() => setShow(true)}>
          <PressStart />
        </GateButton>
      )}
      {show && (
        <World
          resources={resources}
          loading={<div>Loading...</div>}
          style={{
            overflow: "hidden",
            justifyContent: "flex-start",
            padding: 0,
            position: "relative",
          }}
          width={width}
          height={height}
        >
          <StoryBoard>
            <Panel>
              <ComponentInner key={key} onDone={() => setKey(Math.random())} />
            </Panel>
          </StoryBoard>
        </World>
      )}
    </>
  );
};

export default Component;
