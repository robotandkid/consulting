import {
  useCanvasRegistration,
  usePanelCanvasCtx2D,
} from "@robotandkid/sprite-motion";
import { useRef } from "react";

const line = (
  ctx: CanvasRenderingContext2D,
  props: {
    left: number;
    top: number;
    width: number;
    height: number;
    xProjection: number;
    yProjection: number;
    style: {
      lineWidth: number;
      strokeStyle: string;
    };
  }
) => {
  ctx.beginPath();
  // horizontal line
  ctx.moveTo(props.left, props.yProjection);
  ctx.lineTo(props.left + props.width, props.yProjection);
  // vertical line
  ctx.moveTo(props.xProjection, props.top);
  ctx.lineTo(props.xProjection, props.top + props.height);
  ctx.stroke();

  const arrowSize = 20;
  const triangle = () => {
    ctx.beginPath();
    ctx.moveTo(0, arrowSize);
    ctx.lineTo(arrowSize, 0);
    ctx.lineTo(0, -arrowSize);
    ctx.fill();
    ctx.closePath();
  };

  // triangles
  ctx.save();
  {
    ctx.translate(props.left, props.yProjection);
    triangle();
  }
  ctx.restore();
  ctx.save();
  {
    ctx.translate(props.left + props.width, props.yProjection);
    ctx.rotate((180 * Math.PI) / 180);
    triangle();
  }
  ctx.restore();
  ctx.save();
  {
    ctx.translate(props.xProjection, props.top);
    ctx.rotate((90 * Math.PI) / 180);
    triangle();
  }
  ctx.restore();
  ctx.save();
  {
    ctx.translate(props.xProjection, props.top + props.height);
    ctx.rotate((-90 * Math.PI) / 180);
    triangle();
  }
  ctx.restore();
  // target
  ctx.save();
  {
    ctx.translate(props.xProjection, props.yProjection);
    ctx.strokeRect(
      -((5 * arrowSize) / 2),
      -((3 * arrowSize) / 2),
      5 * arrowSize,
      3 * arrowSize
    );
  }
  ctx.restore();
};

export const HUD = (props: {
  left: number;
  top: number;
  width: number;
  height: number;
  cxp: number;
  cyp: number;
  style?: {
    lineWidth?: number;
    strokeStyle?: string;
  };
}) => {
  const { left, top, width, height, style: rawStyle } = props;

  const render = useCanvasRegistration();

  const ctx = usePanelCanvasCtx2D();
  const prevKey = useRef<string>();
  const key = `hud:${JSON.stringify(props)}`;

  const style = {
    lineWidth: 5,
    strokeStyle: "white",
    ...rawStyle,
  };

  if (prevKey.current !== key && ctx && render) {
    prevKey.current = key;

    render(() => {
      const xProjection = props.cxp * props.width + props.left;
      const yProjection = props.cyp * props.height + props.top;

      // apply styles

      ctx.lineWidth = style.lineWidth;
      ctx.strokeStyle = style.strokeStyle;
      ctx.fillStyle = style.strokeStyle;

      line(ctx, { left, top, width, height, xProjection, yProjection, style });

      ctx.strokeRect(left, top, width, height);
    });
  }

  return null;
};
