import {
  useCanvasRegistration,
  usePanelCanvas,
} from "@robotandkid/sprite-motion";
import { useRef } from "react";

const noise = (ctx: CanvasRenderingContext2D) => {
  const iData = ctx.createImageData(ctx.canvas.width, ctx.canvas.height);
  const buffer32 = new Uint32Array(iData.data.buffer); // get 32-bit view
  let len = buffer32.length - 1;
  while (len--) buffer32[len] = Math.random() < 0.5 ? 0 : -1 >> 0;
  ctx.putImageData(iData, 0, 0);
};

/** This is the default */
const fadeIn = (timeElapsed: number, totalTime: number) => {
  const alpha = Math.min(1, timeElapsed / totalTime);
  return 1 - alpha;
};

const fadeOut = (t: number, totalTime: number) => {
  const alpha = Math.min(1, t / totalTime);
  return alpha;
};

export const Noise = (props: {
  time: number;
  effect?: "fadeIn" | "fadeOut";
}) => {
  const render = useCanvasRegistration();

  const canvas = usePanelCanvas();
  const prevKey = useRef<string>();
  const key = `noise:${JSON.stringify(props)}`;

  if (prevKey.current !== key && canvas && render) {
    prevKey.current = key;

    const w = canvas.width;
    const h = canvas.height;
    const offCanvas = document.createElement("canvas"); // create off-screen canvas
    offCanvas.width = w << 1; // set offscreen canvas x2 size
    offCanvas.height = h << 1;
    const octx = offCanvas.getContext("2d", { alpha: false });
    // render noise once, to the offscreen-canvas
    if (octx) noise(octx);
    const ctx = canvas.getContext("2d", { alpha: false });

    let timeElapsed = 0;
    const effect = props.effect === "fadeOut" ? fadeOut : fadeIn;

    render((_, timeElapsedBetweenFrames) => {
      if (timeElapsed > props.time) return;

      const x = (w * Math.random()) | 0; // force integer values for position
      const y = (h * Math.random()) | 0;

      if (!ctx) return;
      ctx.save();
      ctx.globalAlpha = effect(timeElapsed, props.time);
      ctx.drawImage(offCanvas, -x, -y);
      ctx.restore();

      timeElapsed += timeElapsedBetweenFrames;
    });
  }

  return null;
};
