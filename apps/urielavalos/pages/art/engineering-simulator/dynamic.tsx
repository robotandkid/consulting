import {
  ActionBlock,
  Canvas,
  matterBodyByLabel,
  MotionDiv,
  Panel,
  StoryBoard,
  useDownloadsCtx,
  useEngineContext,
  World,
} from "@robotandkid/sprite-motion";
import { Body } from "matter-js";
import { useEffect } from "react";
import { useState } from "react";
import { GateButton, PressStart } from "../../../src/styles/styles";

const record = false;
const width = 1000;
const height = (width * 9) / 16;

const resources = {
  video: {
    type: "video",
    url: `/art/2023/12-26/garbage-sm.mp4`,
  },
  code: {
    type: "text",
    url: "/art/2023/12-26/quicksort.tsx",
  },
  music: {
    type: "sound",
    url: `/art/2023/12-26/${record ? "ui.mp3" : "ui-sm.mp3"}`,
  },
} as const;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const ComponentInner = (props: { onDone: () => void }) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { downloads } = useDownloadsCtx<typeof resources>();
  const engine = useEngineContext();

  useEffect(() => {
    if (!engine) return;

    const keys = (evt: KeyboardEvent) => {
      const body = matterBodyByLabel(engine, "palm");
      if (!body) return;
      let vec = { x: 0, y: 0 };
      switch (evt.code) {
        case "KeyA":
          vec = { x: -0.15, y: 0 };
          break;
        case "KeyD":
          vec = { x: 0.15, y: 0 };
          break;
        case "KeyW":
          vec = { x: 0, y: -0.15 };
          break;
        case "KeyS":
          vec = { x: 0, y: 0.15 };
          break;
      }
      Body.applyForce(body, body?.position, vec);
    };

    window.addEventListener("keypress", keys);
    return () => {
      window.removeEventListener("keypress", keys);
    };
  }, [engine]);

  return (
    <Canvas width={width} height={height}>
      <ActionBlock>
        {(opts) => {
          opts.engine.gravity.x = 0;
          opts.engine.gravity.y = 0;
        }}
      </ActionBlock>
      <MotionDiv
        width={100}
        height={100}
        startCx={width / 2}
        startCy={height / 2}
        style={{
          background: "red",
        }}
        aria-label="palm"
        options={{
          frictionAir: 0.05,
          density: 0.001 * 2,
        }}
      />
    </Canvas>
  );
};

const Component = () => {
  const [key, setKey] = useState(0);
  const [show, setShow] = useState(false);
  return (
    <>
      {!show && (
        <GateButton onClick={() => setShow(true)}>
          <PressStart />
        </GateButton>
      )}
      {show && (
        <World
          resources={resources}
          loading={<div>Loading...</div>}
          style={{
            overflow: "hidden",
            justifyContent: "flex-start",
            padding: 0,
            position: "relative",
          }}
          width={width}
          height={height}
        >
          <StoryBoard disableManualNavigation>
            <Panel>
              <ComponentInner key={key} onDone={() => setKey(Math.random())} />
            </Panel>
          </StoryBoard>
        </World>
      )}
    </>
  );
};

export default Component;
