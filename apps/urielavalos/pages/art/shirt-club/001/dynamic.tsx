import {
  ActionBlock,
  Canvas,
  CanvasDivJustifiedFull,
  CanvasImage,
  CanvasSaveImage,
  CanvasSpan,
  palette8funkyFuture,
  Panel,
  pickAccessibleColor,
  StoryBoard,
  UiPanel,
  useDownloadsCtx,
  World,
} from "@robotandkid/sprite-motion";
import { styled } from "@styled-system/jsx";
import { useState } from "react";

const resources = {
  theme: {
    type: "sound",
    url: "/art/love-in-motion/2024/06/17/theme.wav",
  },
  canvas: {
    type: "image",
    url: "/art/shirt-club/001/canvas.png",
  },
  bg: {
    type: "image",
    url: "/art/shirt-club/001/bg.png",
  },
  robot: {
    type: "video",
    url: "/images/water-mark/robot.mp4",
  },
  kid: {
    type: "video",
    url: "/images/water-mark/kid.mp4",
  },
} as const;

const closestDivisor = (W: number, n: number) => {
  // Step 1: Check if n is already a divisor
  if (W % n === 0) {
    return n;
  }

  // Step 2: Find the closest smaller divisor
  let smallerDivisor = n;
  while (smallerDivisor > 0) {
    if (W % smallerDivisor === 0) {
      break;
    }
    smallerDivisor--;
  }

  // Step 3: Find the closest larger divisor
  let largerDivisor = n;
  while (largerDivisor <= W) {
    if (W % largerDivisor === 0) {
      break;
    }
    largerDivisor++;
  }

  // Step 4: Return the closest divisor
  if (Math.abs(n - smallerDivisor) <= Math.abs(n - largerDivisor)) {
    return smallerDivisor;
  } else {
    return largerDivisor;
  }
};

const closestHeight = (
  width: number,
  aspectRatio: number,
  tileWidth: number
) => {
  // Step 1: Calculate the target height from the aspect ratio
  const targetHeight = width * aspectRatio;

  // Step 2: Check if the target height is already a multiple of the divisor
  if (Math.round(targetHeight) % tileWidth === 0) {
    return Math.round(targetHeight);
  }

  // Step 3: Find the closest smaller multiple of divisor
  let smallerHeight = Math.floor(targetHeight);
  while (smallerHeight % tileWidth !== 0 && smallerHeight > 0) {
    smallerHeight--;
  }

  // Step 4: Find the closest larger multiple of divisor
  let largerHeight = Math.ceil(targetHeight);
  while (largerHeight % tileWidth !== 0) {
    largerHeight++;
  }

  // Step 5: Return the height that is closest to the targetHeight
  if (
    Math.abs(targetHeight - smallerHeight) <=
    Math.abs(targetHeight - largerHeight)
  ) {
    return smallerHeight;
  } else {
    return largerHeight;
  }
};

// if we want the width of the image to be 12 in and DPI = 72,
// then x/72 = 12 => 900px
//
// one flower is roughly 200/1024 = 20% wide.
// so if we want one flower to be 1.5in,
// then x/72 = 1.5 => 108px
//
// it's 20% of the tile, so x*0.20 = 108 => 540
//
const aspectRatio = 3100 / 2500;
const width = 1800;
const tileWidth = closestDivisor(width, 540);
const tileHeight = tileWidth;
const height = closestHeight(width, aspectRatio, tileWidth);

const oneBitColor = ["#000", "#ffff00"] as const;

const CustomUiPanel = styled(UiPanel, {
  base: {
    backgroundColor: oneBitColor[0],
    color: oneBitColor[1],
    "& > div, & > h1": {
      backgroundColor: oneBitColor[0],
      borderColor: oneBitColor[1],
      color: oneBitColor[1],
    },
    zIndex: 2,
    position: "absolute",
  },
});

const StartButton = styled("button", {
  base: {
    display: "inline-block",
    appearance: "none",
    margin: 0,
    padding: 0,
    borderWidth: 0,
    backgroundColor: "inherit",
    color: "inherit",
    fontFamily: "inherit",
    fontWeight: "normal",
    fontSize: "inherit",
    textDecoration: "none",
    cursor: "pointer",
    outline: "none",
    _focus: {
      animation: "pulse 200ms",
    },
  },
  variants: {
    anim: {
      blink: {
        animation: "blink 1s steps(5, start) infinite",
      },
    },
  },
});

const rawQuotes: Array<Array<[string, number, number]>> = [
  [
    [
      "We need to continue our legacy of being a beacon of hope for people from all over the world who come here seeking freedom and opportunity",
      10,
      -1,
    ],
    ["When the whole world is silent, even one voice becomes powerful", 7, 10],
    [
      "A woman is the full circle. Within her is the power to create, nurture, and transform",
      8,
      -3,
    ],
    [
      "To be yourself in a world that is constantly trying to make you something else is the greatest accomplishment.",
      8,
      5,
    ],
  ],
  [
    [
      "This is what an American looks like. This is what we look like. And if you see yourself in us, I hope you step up.",
      8,
      3,
    ],
    ["Alone we can do so little; together we can do so much.", 8, -5],
    ["There is no force more powerful than a woman determined to rise.", 6, 5],
    ["It takes courage to grow up and become who you really are.", 8, -5],
  ],
  [
    [
      "We may have different religions, different languages, different colored skin, but we all belong to one human race.",
      4,
      -2,
    ],
    [
      "The question isn’t who’s going to let me; it’s who’s going to stop me",
      3,
      5,
    ],
    ["She believed she could, so she did.", 2, -10],
    [
      "Our diversity is our strength. What a dull and pointless life it would be if everyone was the same.",
      4,
      3,
    ],
  ],
  [
    [
      "Everywhere immigrants have enriched and strengthened the fabric of American life.",
      4,
      -5,
    ],
    ["We cannot all succeed when half of us are held back.", 4, 2],
    [
      "Love takes off the masks that we fear we cannot live without and know we cannot live within.",
      6,
      -3,
    ],
  ],
];

const formatQuote1 = (q: string) => q.replace(/ /g, "").replace(/[.,?;]/g, " ");

const allQuotes = rawQuotes.flatMap((c) =>
  c.map((c) => ({ quote: formatQuote1(c[0]), height: c[1], rotation: c[2] }))
);

const quotes = allQuotes.filter((_, index) => index < 4);
const quotes2 = allQuotes.filter((_, index) => index >= 4);

// const formatQuote = (quote: string) =>
//   quote
//     .split(".")
//     .flatMap((quote) => quote.split(","))
//     .flatMap((quote) => quote.split("?"))
//     .map((text) => text.replace(/ /g, ""));

// const mainQuote = formatQuote(
//   `No human being is illegal. That is a contradiction in terms. Human beings can be beautiful, or more beautiful, they can be fat or skinny, they can be right or wrong, but illegal? How can a human being be illegal?`
// );

// const secondaryQuote = formatQuote(
//   `We need to continue our legacy, of being a beacon of hope, for people from all over the world, who come here seeking,freedom,and opportunity.`
// );

const textPalette = palette8funkyFuture();

const shuffleArray = <Data,>(array: Readonly<Data[]>) => {
  // Make a copy of the array to avoid modifying the original
  const shuffledArray = array.slice();

  for (let i = shuffledArray.length - 1; i > 0; i--) {
    // Pick a random index from 0 to i
    const j = Math.floor(Math.random() * (i + 1));

    // Swap elements at index i and j
    // @ts-expect-error this should be fine
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }

  return shuffledArray;
};

const colors = shuffleArray(textPalette);

const randomTextColor = (index: number) => {
  const textColor = colors[index % colors.length];
  const bgColor = pickAccessibleColor(textColor ?? "#fff", textPalette);
  return [textColor, bgColor] as const;
};

const Frame = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const hWidth = width / 2;

  return (
    <>
      <ActionBlock>
        {async () => {
          // downloads.theme.resource.play({ loop: true });
        }}
      </ActionBlock>
      <CanvasImage
        src={downloads.bg}
        tile="repeat"
        tileWidth={tileWidth}
        tileHeight={tileHeight}
        renderTop={0}
        renderLeft={0}
        renderWidth={width}
        renderHeight={height}
      />
      {quotes.map((quote, index) => {
        const [textColor, bgColor] = randomTextColor(index);
        // fontSize/72 = height in inches
        const fontSize = 72;
        let top = hWidth * 0.05;
        let len = 0;
        while (len < index) {
          top += fontSize * (quotes[len]?.height ?? 1);
          len += 1;
        }

        return (
          <CanvasDivJustifiedFull
            key={index}
            pos={{
              left: hWidth * 0.05,
              top,
              width: hWidth * 0.9,
              height: height * 0.5,
            }}
            style={{
              lineHeightMultiple: 1.4,
            }}
            transform={{
              rotate: quote.rotation,
            }}
          >
            <CanvasSpan
              key={index}
              style={{
                colorStyle: textColor,
                bgStyle: bgColor,
                font: `bold ${fontSize}px helvetica`,
              }}
            >
              {quote.quote}
            </CanvasSpan>
          </CanvasDivJustifiedFull>
        );
      })}
      {quotes2.map((quote, index) => {
        const [textColor, bgColor] = randomTextColor(index + quotes.length);
        // fontSize/72 = height in inches
        const fontSize = 72;
        let top = hWidth * 0.4;
        let len = 0;
        while (len < index) {
          top += fontSize * (quotes2[len]?.height ?? 1);
          len += 1;
        }

        return (
          <CanvasDivJustifiedFull
            key={index}
            pos={{
              left: hWidth * 0.9,
              top,
              width: hWidth * 0.9,
              height: height * 0.5,
            }}
            style={{
              lineHeightMultiple: 1.4,
            }}
            transform={{
              rotate: quote.rotation,
            }}
          >
            <CanvasSpan
              key={index}
              style={{
                colorStyle: textColor,
                bgStyle: bgColor,
                font: `bold ${fontSize}px helvetica`,
              }}
            >
              {quote.quote}
            </CanvasSpan>
          </CanvasDivJustifiedFull>
        );
      })}
      {/* <WaterMark
        top={10}
        left={width * 0.7}
        width={width * 0.2}
        kid={downloads.kid}
        robot={downloads.robot}
        fontSize={width * 0.2 * 0.25}
        borderColor="#000"
        borderThickness={10}
      /> */}
      <CanvasSaveImage Button={<button>Save</button>} />
    </>
  );
};

const ComponentInner = () => {
  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas disableClearCanvasBeforeUpdate>
          <Frame />
        </Canvas>
      </Panel>
    </StoryBoard>
  );
};

export const ComponentWorld = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{
        background: oneBitColor[0],
      }}
      width={width}
      height={height}
    >
      <ComponentInner />
    </World>
  );
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const IntroContainer = styled("div", {
  base: {
    backgroundColor: oneBitColor[0],
    height,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "1.5rem",
    width: "[100vw]",
    marginLeft: "[-40vw]",
    marginRight: "[-40vw]",
  },
});

const Component = () => {
  const [show, setShow] = useState(false);
  return (
    <IntroContainer>
      {!show && (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore don't know why it doesn't recodnize the click handler
        <CustomUiPanel
          title="Press"
          onClick={() => setShow(true)}
          width="25rem"
        >
          <StartButton>Start</StartButton>
        </CustomUiPanel>
      )}
      {show && <ComponentWorld />}
    </IntroContainer>
  );
};

export default Component;
