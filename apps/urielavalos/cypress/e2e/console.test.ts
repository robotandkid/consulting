describe("console", () => {
  Cypress.on("window:before:load", (win) => {
    cy.spy(win.console, "error");
    cy.spy(win.console, "warn");
  });

  it("there should be no console warnings", () => {
    cy.visit("");

    cy.findByRole("region", { name: /uriel avalos/i }).should("be.visible");
    cy.findByRole("button", { name: /open main menu/i }).click();
    cy.findByRole("navigation", { name: /main menu/i }).should("be.visible");
    cy.window().then((win) => {
      expect(win.console.error).to.have.callCount(0);
      expect(win.console.warn).to.have.callCount(0);
    });
  });
});
