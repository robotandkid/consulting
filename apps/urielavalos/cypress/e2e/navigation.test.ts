const clickMenuAndAssert = (name: RegExp) => {
  cy.findByRole("button", { name: /open main menu/i }).click();
  cy.findByRole("navigation", { name: /main menu/i }).should("be.visible");
  cy.findByRole("link", { name }).click();
  // DOH
  cy.findByRole("link", { name }).click();
  cy.findByRole("navigation", { name: /main menu/i }).should("not.exist");
  cy.findByRole("heading", { name }).should("be.focused");
};

describe("navigation", () => {
  it(`can click "about"`, () => {
    cy.visit("");
    clickMenuAndAssert(/about/i);
  });

  it(`can click "gallery"`, () => {
    cy.visit("");
    clickMenuAndAssert(/gallery/i);
  });

  it(`can click "contact"`, () => {
    cy.visit("");
    clickMenuAndAssert(/contact/i);
  });
});
