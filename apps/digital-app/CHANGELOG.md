# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/digital-app@0.1.4...@robotandkid/digital-app@0.2.0) (2024-05-10)


### Features

* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





## [0.1.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/digital-app@0.1.3...@robotandkid/digital-app@0.1.4) (2023-09-19)

**Note:** Version bump only for package @robotandkid/digital-app





## [0.1.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/digital-app@0.1.2...@robotandkid/digital-app@0.1.3) (2023-09-18)

**Note:** Version bump only for package @robotandkid/digital-app





## [0.1.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/digital-app@0.1.1...@robotandkid/digital-app@0.1.2) (2023-09-04)


### Bug Fixes

* enabled stricer array access in TS ([8b89be7](https://gitlab.com/robotandkid/consulting/commit/8b89be793edef75156025f9b3ee4a7944a308dc0))





## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/digital-app@0.1.0...@robotandkid/digital-app@0.1.1) (2023-08-07)

**Note:** Version bump only for package @robotandkid/digital-app





# 0.1.0 (2023-08-07)


### Bug Fixes

* **digital-apps:** auto resizer no works correctly ([fe97d34](https://gitlab.com/robotandkid/consulting/commit/fe97d34d43ce9386ee2e17bbbc2aa9f05c00f879))
* **digital-apps:** updated 07.png ([68cf67e](https://gitlab.com/robotandkid/consulting/commit/68cf67e196b1d190a227f13d5f9c2206504c4771))


### Features

* **digital-app:** added story ([bc149b6](https://gitlab.com/robotandkid/consulting/commit/bc149b681e8e43d6e55c33652fb45ea001d45294))
* **digital-app:** added storyboard ([0830c2b](https://gitlab.com/robotandkid/consulting/commit/0830c2be58386825d29cd29c589aab57178d6045))
* **digital-apps:** almost done ([1acc357](https://gitlab.com/robotandkid/consulting/commit/1acc357d28fd368d1258ba3870a44678ea254512))
* **digital-apps:** eli garcia published ([331476e](https://gitlab.com/robotandkid/consulting/commit/331476ecc8574bc6371c17268e674f68a6919c1e))
* **digital-apps:** eli garcia, 1st pass ([bfee301](https://gitlab.com/robotandkid/consulting/commit/bfee3017677d91b44017276f507e28929e1e93be))
* **digital-app:** successfully updated digital-app ([bd7f334](https://gitlab.com/robotandkid/consulting/commit/bd7f3344d7a7a12f53d9e620ce7b99669b88e8e5))
* **digitalapps:** created ([8851cc6](https://gitlab.com/robotandkid/consulting/commit/8851cc621eaa4bff6407057454b251715f4d1f76))
