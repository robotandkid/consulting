/** @type {import("next").NextConfig} */
module.exports = {
  images: {
    unoptimized: true,
  },
  basePath: "/REPLACE_ME_IN_NODE_SCRIPT",
  async headers() {
    return [];
  },
  pageExtensions: ["page.tsx", "mdx"],
  webpack(config) {
    // netlify bug:
    // https://answers.netlify.com/t/next-js-internal-error-during-functions-bundling-on-build/43647/7
    Object.assign(
      config.externals,
      ["critters"].reduce(
        (externals, name) => ({ ...externals, [name]: `commonjs ${name}` }),
        {}
      )
    );

    return config;
  },
};
