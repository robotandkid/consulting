export default {
  stories: "**/*.fixture.{ts,tsx}",
  appendToHead: `
  <link
    rel="preload"
    href="/fonts/Righteous-Regular.ttf"
    as="font"
    crossOrigin="anonymous"
  />`,
};
