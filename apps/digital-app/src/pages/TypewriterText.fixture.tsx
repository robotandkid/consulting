import styled from "styled-components";
import { GlobalStyle } from "./GlobalStyles";
import { TypewriterText } from "./TypewriterText";

export const SimpleTypewriterText = () => {
  return (
    <>
      <GlobalStyle />
      <TypewriterText fontSize="2rem">Hello world</TypewriterText>
    </>
  );
};

const BigText = styled(TypewriterText)`
  color: red;
  background: pink;
  display: inline-block;
`;

export const BigTypewriterText = () => (
  <>
    <GlobalStyle />
    <BigText fontSize="5rem">At this seat, we&apos;re pleased to offer</BigText>
  </>
);

const BigText2 = styled(BigText)`
  width: 20rem;
  height: 12rem;
`;

export const BigTextLittleBox = () => (
  <>
    <GlobalStyle />
    <BigText2 fontSize="5rem">
      At this seat, we&apos;re pleased to offer
    </BigText2>
  </>
);
