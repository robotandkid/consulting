import React from "react";
import styled, { css } from "styled-components";
import { AnimatePresence, motion } from "framer-motion";
import { wrap } from "popmotion";
import { reset } from "./GlobalStyles";
import { useCallback } from "react";
import { useEffect } from "react";
import { createModuleState, useModuleState } from "@robotandkid/components";

export const storyZIndex = 0;

const fullBleed = css`
  width: 100vw;
  height: 100vh;
  left: 50%;
  right: 50%;
  margin-left: -50vw;
  margin-right: -50vw;
  margin-top: -1vh;
`;

/** Breakpoints: 480, 600, 768, 800, 992, 1024, 1200 */
const StoryBoardContainer = styled.section`
  ${reset}
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  box-sizing: border-box;
  ${fullBleed};
`;

const PanelContainer = styled(motion.div)`
  ${reset}
  width: 100%;
  height: 100%;
  /**
   * absolute needed for swipe
   */
  position: absolute;
  overflow: hidden;
`;

export const PanelBackground = styled(motion.img)`
  ${reset}
  width: 100%;
  height: 100%;
  position: absolute;
  pointer-events: none;
  object-fit: cover;
  @media screen and (orientation: landscape) {
    /* top: 20%;
    overflow: visible; */
  }
`;

export const Panel = styled(motion.div)`
  ${reset}
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
`;

const NavButton = styled.button<{ $pos: "prev" | "next" }>`
  top: calc(50% - 1.5rem);
  border-radius: 5rem;
  width: 5rem;
  height: 5rem;
  font-size: 3rem;
  ${(p) =>
    p.$pos === "prev"
      ? css`
          left: 10px;
        `
      : css`
          right: 10px;
        `}
  position: absolute;
  background: white;
  display: flex;
  justify-content: center;
  align-items: center;
  user-select: none;
  cursor: pointer;
  font-weight: bold;
  z-index: ${storyZIndex + 2};
`;

const panelContainerVariants = {
  enter: (direction: "prev" | "next") => {
    return {
      x: direction === "next" ? 1000 : -1000,
      opacity: 0,
    };
  },
  center: {
    zIndex: storyZIndex + 1,
    x: 0,
    opacity: 1,
  },
  exit: (direction: "prev" | "next") => {
    return {
      zIndex: storyZIndex,
      x: direction === "prev" ? 1000 : -1000,
      opacity: 0,
    };
  },
};

/**
 * Experimenting with distilling swipe offset and velocity into a single
 * variable, so the less distance a user has swiped, the more velocity they need
 * to register as a swipe. Should accomodate longer swipes and short flicks
 * without having binary checks on just distance thresholds and velocity > 0.
 */
const swipeConfidenceThreshold = 10000;
const swipePower = (offset: number, velocity: number) => {
  return Math.abs(offset) * velocity;
};

const curPage = createModuleState([0, "next"] as [number, "prev" | "next"]);

export const gotoPage = (page: number) => {
  // we don't care about direction because this is only used in fixtures
  const [, direction] = curPage.getState();
  curPage.setState([page, direction]);
};

export const StoryBoard = (props: {
  children?: React.ReactNode[] | React.ReactNode;
}) => {
  const { children } = props;
  const items = Array.isArray(children) ? children : [children];

  const [[page, direction], setPage] = useModuleState(curPage);

  // We only have 3 images, but we paginate them absolutely (ie 1, 2, 3, 4, 5...) and
  // then wrap that within 0-2 to find our image ID in the array below. By passing an
  // absolute page index as the `motion` component's `key` prop, `AnimatePresence` will
  // detect it as an entirely new image. So you can infinitely paginate as few as 1 images.
  const itemIndex = wrap(0, items.length, page);

  const paginate = useCallback(
    (newDirection: "prev" | "next") => {
      if (newDirection === "prev") setPage(([p]) => [p - 1, newDirection]);
      else setPage(([p]) => [p + 1, newDirection]);
    },
    [setPage]
  );

  useEffect(() => {
    const keys = (evt: KeyboardEvent) => {
      if (evt.key === "ArrowLeft") paginate("prev");
      else if (evt.key === "ArrowRight") paginate("next");
    };

    window.addEventListener("keydown", keys);

    return () => {
      window.removeEventListener("keydown", keys);
    };
  });

  return (
    <StoryBoardContainer>
      <AnimatePresence initial={false} custom={direction}>
        <PanelContainer
          key={page}
          custom={direction}
          variants={panelContainerVariants}
          initial="enter"
          animate="center"
          exit="exit"
          transition={{
            x: { type: "spring", stiffness: 300, damping: 30 },
            opacity: { duration: 0.2 },
          }}
          drag="x"
          dragConstraints={{ left: 0, right: 0 }}
          dragElastic={1}
          onDragEnd={(e, { offset, velocity }) => {
            const swipe = swipePower(offset.x, velocity.x);

            if (swipe < -swipeConfidenceThreshold) {
              paginate("next");
            } else if (swipe > swipeConfidenceThreshold) {
              paginate("prev");
            }
          }}
        >
          {items[itemIndex]}
        </PanelContainer>
      </AnimatePresence>
      <NavButton $pos="next" onClick={() => paginate("next")}>
        {"▶"}
      </NavButton>
      <NavButton $pos="prev" onClick={() => paginate("prev")}>
        {"◀"}
      </NavButton>
    </StoryBoardContainer>
  );
};
