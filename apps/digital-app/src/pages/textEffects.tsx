import { motion } from "framer-motion";
import React from "react";
import styled, { css, keyframes } from "styled-components";

type Color = `#${string}`;

export const neonText = (color: Color, glow: Color, heightEms: number) => css`
  color: ${color};
  text-shadow: 0 0 ${heightEms * 0.025}em ${color},
    0 0 ${heightEms * 0.0733}em ${color}, 0 0 ${heightEms * 0.1266}em ${color},
    0 0 ${heightEms * 0.266}em ${glow}, 0 0 ${heightEms * 0.533}em ${glow},
    0 0 ${heightEms * 0.6}em ${glow}, 0 0 ${heightEms * 0.6666}em ${glow},
    0 0 ${heightEms}em ${glow};
`;

export const neonFlicker = (
  color: Color,
  glow: Color,
  heightEms: number
) => keyframes`
  0%, 18%, 22%, 25%, 53%, 57%, 100% {
    ${neonText(color, glow, heightEms)}
  }
  20%, 24%, 55% {       
    text-shadow: none;
    color: ${color};
  }
`;

const textClip = keyframes`
  to {
    background-position: 200% center;
  }
`;

export const textGradientAnimation = (
  gradient: readonly [Color, Color, Color, Color, Color] = [
    "#231557",
    "#44107a",
    "#ff1361",
    "#fff800",
    "#fff",
  ]
) => css`
  background-image: linear-gradient(
    -225deg,
    ${gradient[0]} 0%,
    ${gradient[1]} 29%,
    ${gradient[2]} 67%,
    ${gradient[3]} 100%
  );
  color: ${gradient[4]};
  background-size: 200% auto;
  background-clip: text;
  -webkit-text-fill-color: transparent;
  animation: ${textClip} 2s linear infinite;
  display: inline-block;
`;

export const staticColorChangePalette = [
  ["#DF8453", "#3D8DAE", "#E4A9A8"],
  ["#DBAD4A", "#ACCFCB", "#fff4e0"],
  ["#ACCFCB", "#E4A9A8", "#ACCFCB"],
  ["#3D8DAE", "#DF8453", "#E4A9A8"],
] as const;

const staticColorChange = (colors: readonly [Color, Color, Color]) => keyframes`
  0%    {color: ${colors[0]}}
  32%   {color: ${colors[0]}}
  33%   {color: ${colors[1]}}
  65%   {color: ${colors[1]}}
  66%   {color: ${colors[2]}}
  99%   {color: ${colors[2]}}
  100%  {color: ${colors[0]}}
`;

const StaticColorChangeWord = styled(motion.div)<{
  $colors: readonly [Color, Color, Color];
}>`
  animation: ${(p) => staticColorChange(p.$colors)} 4s linear infinite;
  display: inline-block;
`;

const StaticColorChangeContainer = styled(motion.div)``;

export const StaticColorChange = (props: {
  className?: string;
  children: string;
  palette?: Array<[Color, Color, Color]>;
}) => {
  const { palette = staticColorChangePalette } = props;
  const words = props.children.split(/\s/);
  return (
    <StaticColorChangeContainer className={props.className}>
      {words.map((word, index) => {
        const colors = palette[index % palette.length];
        return (
          <React.Fragment key={index}>
            <StaticColorChangeWord key={index} $colors={colors}>
              {word}
            </StaticColorChangeWord>{" "}
          </React.Fragment>
        );
      })}
    </StaticColorChangeContainer>
  );
};

const wavy = keyframes`
  0%,40%,100% {
    transform: translateY(0)
  }
  20% {
    transform: translateY(-0.5em)
  }
`;

const WavyLetter = styled(motion.span)<{
  $index: number;
  $delay: `${number}s`;
}>`
  display: inline-block;
  color: inherit;
  text-transform: inherit;
  animation: ${wavy} 1s infinite;
  animation-delay: calc(${(p) => p.$delay} * ${(p) => p.$index});
`;

const WavyContainer = styled(motion.div)``;
const spaceRegex = /\s+/;

export const WavyLetters = (props: {
  className?: string;
  children: string;
  delay?: `${number}s`;
}) => {
  const { delay = "0.1s" } = props;
  const letters = props.children.split("");

  return (
    <WavyContainer className={props.className}>
      <section aria-label={props.children} />
      {letters.map((letter, index) => {
        if (spaceRegex.test(letter)) {
          return <span key={index}> </span>;
        }
        return (
          <WavyLetter key={index} $index={index} $delay={delay} aria-hidden>
            {letter}
          </WavyLetter>
        );
      })}
    </WavyContainer>
  );
};

const toHex = (x: number): Color => {
  const rawHex = x.toString(16);
  const rounded = rawHex.replace(/\..*/, "");
  if (rounded.length < 6) {
    const zeros = Array.from({ length: 6 - rounded.length })
      .map(() => 0)
      .join("");
    return `#${zeros}${rounded}`;
  }
  if (rounded.length > 7) {
    return `#${rounded.substring(0, 6)}`;
  }
  return `#${rounded}`;
};

const toNumber = (x: Color) => {
  const rawHex = x.replace("#", "");
  return parseInt(rawHex, 16);
};

export const wackyInterpolate = (
  time: number,
  startColor: Color,
  endColor: Color
) => {
  const start = toNumber(startColor);
  const end = toNumber(endColor);
  return toHex(time * (end - start));
};

const interpolateHexColor = (
  start: Color,
  end: Color,
  percent: number
): Color => {
  const _start = start.substring(1);
  const _end = end.substring(1);
  // Convert the HEX color strings to RGB values
  const r1 = parseInt(_start.substring(0, 2), 16);
  const g1 = parseInt(_start.substring(2, 4), 16);
  const b1 = parseInt(_start.substring(4, 6), 16);
  const r2 = parseInt(_end.substring(0, 2), 16);
  const g2 = parseInt(_end.substring(2, 4), 16);
  const b2 = parseInt(_end.substring(4, 6), 16);

  // Interpolate the RGB values based on the percentage
  const r = Math.round(r1 + (r2 - r1) * percent);
  const g = Math.round(g1 + (g2 - g1) * percent);
  const b = Math.round(b1 + (b2 - b1) * percent);

  // Convert the interpolated RGB values back to a HEX color string
  let hex = ((r << 16) + (g << 8) + b).toString(16);
  while (hex.length < 6) {
    hex = "0" + hex;
  }
  return `#${hex}`;
};

const elegantTextShadow = (spacingEm: number, index: number, gradient: Color) =>
  `${2 * spacingEm - spacingEm * index}em ${
    spacingEm * index * 2
  }em ${spacingEm}em ${gradient}`;

const createGradient = (
  steps = 28,
  stepScale = 1 / (steps - 1),
  start: Color = "#767676",
  end: Color = "#e4e3e2",
  shadowSpacingEm = 0.01
) => {
  return Array.from({ length: steps }).map((_, index) => {
    return {
      gradient: interpolateHexColor(start, end, index * stepScale),
      shadowSpacingEm,
      index,
    };
  });
};

export const elegantShadow = (
  gradients: Array<{
    gradient: Color;
    shadowSpacingEm: number;
    index: number;
  }> = createGradient(20)
) => css`
  letter-spacing: 0.15em;
  text-shadow: ${gradients
    .map(({ gradient, shadowSpacingEm: spacingEm, index }) =>
      elegantTextShadow(spacingEm, index, gradient)
    )
    .join(", ")};
`;

const mysteryBlink = keyframes`
  50% {
    text-shadow: 0 0 20px #00ffff, 0 0 40px #ff00ff, 0 0 60px #00ff00,
      0 0 80px #ffff00, 0 0 100px #ff0000, 0 0 120px #00ff00, 0 0 140px #ffff00;
  }
`;

export const Mystery = styled(motion.div)<{ $color?: Color }>`
  text-transform: uppercase;
  color: ${(p) => p.$color ?? "#fff"};
  text-shadow: 0 0 20px #ff00ff, 0 0 40px #00ffff, 0 0 60px #00ff00,
    0 0 80px #ffff00, 0 0 100px #ff0000, 0 0 120px #00ff00, 0 0 140px #ffff00;
  animation: ${mysteryBlink} 5s infinite;
`;
