import { motion } from "framer-motion";
import styled from "styled-components";
import { reset } from "./GlobalStyles";

const Base = styled(motion.section)<{ $fontSize?: `${number}rem` }>`
  box-sizing: border-box;
  position: relative;
  font-family: "righteous";
  ${(p) => p.$fontSize && `font-size: ${p.$fontSize};`};
`;

const sentenceVariants = {
  hidden: {
    opacity: 1,
  },
  visible: {
    opacity: 1,
    transition: {
      delay: 0.5,
      staggerChildren: 0.08,
    },
  },
};

const Sentence = styled(motion.div)`
  ${reset}
  width: 100%;
  height: 100%;
`;

const letterVariants = {
  hidden: { opacity: 0, y: 50 },
  visible: {
    opacity: 1,
    y: 0,
  },
};

const Letter = styled(motion.span)``;

/** Needed for auto size text div */
const Hidden = styled.div`
  ${reset}
  position: absolute;
  visibility: hidden;
`;

/**
 * Approach taken from:
 * https://brad-carter.medium.com/how-to-animate-a-text-reveal-effect-in-react-with-framer-motion-ae8ddd296f0d
 */
export const TypewriterText = (props: {
  children: string;
  className?: string;
  hidden?: boolean;
  fontSize?: `${number}rem`;
}) => {
  const { hidden = false, fontSize } = props;

  const letters = props.children.split("").map((letter, index) => (
    <Letter key={index} variants={letterVariants}>
      {letter}
    </Letter>
  ));

  if (letters.length > 50)
    sentenceVariants.visible.transition.staggerChildren = 0.01;
  else sentenceVariants.visible.transition.staggerChildren = 0.08;

  return (
    <Base
      className={props.className}
      $fontSize={fontSize}
      aria-label={props.children}
    >
      {hidden && <Hidden>{props.children}</Hidden>}
      {!hidden && (
        <Sentence
          variants={sentenceVariants}
          initial="hidden"
          animate="visible"
          aria-hidden
        >
          {letters}
        </Sentence>
      )}
    </Base>
  );
};
