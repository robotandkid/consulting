import { GlobalStyle } from "./GlobalStyles";

export default function MyApp(props: { Component: any; pageProps: any }) {
  const { Component, pageProps } = props;
  return (
    <>
      <GlobalStyle />
      <Component {...pageProps} />
    </>
  );
}
