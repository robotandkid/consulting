import { createGlobalStyle, css } from "styled-components";

export const reset = css`
  padding: 0;
  margin: 0;
`;

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'righteous';
    src: url('./fonts/Righteous-Regular.ttf') format('truetype');
    font-display: 'swap';
  }

  :root {
      font-size: 75%;
      @media only screen and (min-width: 480px) {
        font-size: 62.5%;
      }
      ${reset}
      overflow: hidden;
  }
`;
