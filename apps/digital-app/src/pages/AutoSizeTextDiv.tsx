import { motion } from "framer-motion";
import React, { useEffect, useRef, useState } from "react";
import { useCallback } from "react";
import styled from "styled-components";
import { reset } from "./GlobalStyles";
import { storyZIndex } from "./Storyboard";

const Container = styled.div`
  ${reset}
  overflow: hidden;
  z-index: ${storyZIndex + 2};
`;

/** When content overflows, this will be less than 1. */
const calculateOverflow = (div: HTMLElement) => {
  const overflowX = div.scrollWidth - div.clientWidth > 0;
  const overflowY = div.scrollHeight - div.clientHeight > 0;

  return overflowX || overflowY;
};

/**
 * Usage:
 *
 * - Set the width/height by wrapping turning this into a styled-component.
 * - Use EMs for font size on children
 * - Do _not_ use REMs
 * - Do _not_ set overflow: hidden on children
 */
export const AutoSizer = (props: {
  children: React.ReactElement<{
    hidden?: boolean;
    fontSize?: `${number}rem`;
  }>;
  className?: string;
  initialFontSize: `${number}rem`;
}) => {
  const { initialFontSize } = props;
  const ref = useRef<HTMLDivElement>(null);

  const [hidden, setHidden] = useState(true);
  const [fontSize, setFontSize] = useState(+initialFontSize.replace("rem", ""));

  const mounted = useRef(true);

  useEffect(function mountGuard() {
    return () => {
      mounted.current = false;
    };
  }, []);

  const observer = useRef<ResizeObserver>();
  const done = useRef(false);

  const checkForOverflow = useCallback(() => {
    const container = ref.current;
    if (!container) return;
    if (!mounted.current) return;

    if (calculateOverflow(container)) {
      setFontSize((s) => Math.max(0.5, s - 0.5));
    } else {
      setHidden(false);
      // cleanup
      container.style.overflow = "inherit";
      observer.current?.disconnect();
      done.current = true;
    }
  }, []);

  const setupObserver = useCallback(() => {
    const container = ref.current;
    if (!container) return;
    const child = container.firstElementChild;
    if (!child) return;

    observer.current = new ResizeObserver(checkForOverflow);
    observer.current.observe(child);
    done.current = false;
    container.style.overflow = "hidden";
  }, [checkForOverflow]);

  /**
   * The way this works is weird and totally caused by React observing the DOM
   * at the wrong time.
   *
   * # How it works
   *
   * 1. Setup a resize observer on the container's first child. Why? This is the
   *    wacky part.
   * 2. Decrease the container font size (if there's overflow). At this point the
   *    resize observer kicks in---if there's still overflow, it'll decrease the
   *    container font size... again... recursively until there's no more
   *    overflow.
   *
   * OK that wasn't so bad!
   */
  useEffect(
    function setup() {
      setupObserver();
      checkForOverflow();

      return () => {
        observer.current?.disconnect();
      };
    },
    [checkForOverflow, setupObserver]
  );

  useEffect(
    function restartOnResize() {
      const main = new ResizeObserver(() => {
        if (done.current) {
          //restart
          setupObserver();
          checkForOverflow();
        }
      });

      main.observe(document.body);

      return () => {
        main.disconnect();
      };
    },
    [checkForOverflow, setupObserver]
  );

  return (
    <Container ref={ref} className={props.className}>
      {React.cloneElement(props.children, {
        hidden,
        fontSize: `${fontSize}rem`,
      })}
    </Container>
  );
};

export const AutoH1 = (props: {
  className?: string;
  children: React.ReactNode;
  hidden?: boolean;
  fontSize?: `${number}rem`;
}) => {
  const { hidden, fontSize } = props;

  return (
    <motion.h1
      className={props.className}
      style={{ visibility: hidden ? "hidden" : "inherit", fontSize: fontSize }}
    >
      {props.children}
    </motion.h1>
  );
};

export const AutoDiv = (props: {
  className?: string;
  children: React.ReactNode;
  hidden?: boolean;
  fontSize?: `${number}rem`;
}) => {
  const { hidden, fontSize } = props;

  return (
    <motion.div
      className={props.className}
      style={{ visibility: hidden ? "hidden" : "inherit", fontSize: fontSize }}
    >
      {props.children}
    </motion.div>
  );
};
