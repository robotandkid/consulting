import { motion } from "framer-motion";
import styled, { css } from "styled-components";
import { AutoDiv, AutoSizer } from "./AutoSizeTextDiv";
import { reset } from "./GlobalStyles";
import { Panel, PanelBackground, StoryBoard, storyZIndex } from "./Storyboard";
import {
  elegantShadow,
  Mystery,
  neonFlicker,
  StaticColorChange,
  WavyLetters,
} from "./textEffects";
import { TypewriterText } from "./TypewriterText";

const Caption = styled(AutoSizer)`
  color: white;
  font-weight: bold;
  width: 100vw;
  height: 25vh;
  background: rgba(0, 0, 0, 0.5);
  box-sizing: border-box;
`;

const CaptionText = styled(TypewriterText)`
  padding: 0.25em;
  box-sizing: border-box;
`;

const Cover = styled(motion.section)`
  ${reset}
  z-index: ${storyZIndex + 2};
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  font-family: "righteous";
`;

const TitleContainer = styled(AutoSizer)`
  width: 100vw;
  height: 45vh;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Title = styled(motion.h1)`
  ${reset}
  animation: ${neonFlicker("#fff", "#fb4710", 0.5)} 3s infinite alternate;
  font-size: 1em;
  text-align: center;
  padding: 0.25em;
`;

const SubTitle = styled(motion.h2)`
  ${reset}
  ${elegantShadow()};
  color: #ffc219;
  font-size: 1em;
  text-align: center;
  padding: 0.25em;
`;

const CreditsContainer = styled(motion.div)`
  width: 100vw;
  height: 45vh;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const AuthorContainer = styled(AutoSizer)`
  width: 100vw;
  height: 15vh;
  text-align: center;
`;

const AuthorInner = styled(AutoDiv)`
  color: #fff;
`;

const IllustratorContainer = styled(AutoSizer)`
  width: 100vw;
  height: 15vh;
  text-align: center;
  color: white;
`;

const IllustratorInner = styled(AutoDiv)`
  background: rgba(0, 0, 0, 0.5);
`;

const End = styled(motion.section)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  font-family: righteous;
  line-height: 1;
  background: white;
  color: black;
  width: 100vw;
  height: 102vh;
  margin-top: -1vh;
  margin-bottom: -1vh;
`;

const EndHeaderContainer = styled(AutoSizer)`
  width: 100vw;
  height: 35vh;
  text-align: center;
  font-style: italic;
`;

const About = styled(motion.ul)`
  list-style: none;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  font-family: righteous;
  line-height: 1;
  background: black;
  color: white;
  width: 100vw;
  height: 102vh;
  margin-top: -1vh;
  margin-bottom: -1vh;
`;

const aboutHeight = 100 / 5 - 1;

const AboutHeaderContainer = styled(AutoSizer)`
  width: 90%;
  height: ${aboutHeight}vh;
`;

const AboutHeader = styled(Mystery)`
  ${reset};
  padding: 0.25em;
`;

const AboutItem = styled(motion.li)`
  width: 100vw;
  height: ${aboutHeight}vh;
  display: flex;
  flex-direction: row;
`;

const AboutItemTextContainer = styled(AutoSizer)`
  width: 50vw;
  height: 100%;
`;

const AboutItemText = styled(AutoDiv)`
  padding: 0.25em;
  margin-left: 0.5em;
`;

const AboutItemPicContainer = styled(motion.div)``;

const aboutItemPic = css`
  width: 100%;
  height: 100%;
  width: calc(min(${aboutHeight - 1}vw, ${aboutHeight - 1}vh));
  height: calc(min(${aboutHeight - 1}vw, ${aboutHeight - 1}vh));
  border-radius: 50%;
  overflow: hidden;
  background-size: cover;
`;

const AboutItemSvg = styled(motion.div)`
  width: calc(min(${aboutHeight - 1}vw, ${aboutHeight - 1}vh));
  height: calc(min(${aboutHeight - 1}vw, ${aboutHeight - 1}vh));
  & > svg {
    width: 95%;
    height: 95%;
    color: white;
  }
`;

const AboutItemPic = styled(motion.img)`
  ${aboutItemPic}
`;

const OpenAiSvg = () => (
  <svg viewBox="-0.002 0 29.133 29.53" width="29.133" height="29.53">
    <path
      fill="#fff"
      d="M27.21,12.08c.67-2.01,.44-4.21-.63-6.04-1.61-2.8-4.85-4.24-8.01-3.57C17.16,.89,15.14-.01,13.02,0c-3.23,0-6.1,2.08-7.1,5.15-2.08,.43-3.87,1.73-4.92,3.57-1.62,2.8-1.25,6.32,.92,8.72-.67,2.01-.44,4.21,.63,6.03,1.61,2.81,4.85,4.25,8.02,3.58,1.4,1.58,3.42,2.49,5.54,2.48,3.23,0,6.1-2.08,7.1-5.15,2.08-.43,3.87-1.73,4.91-3.57,1.63-2.8,1.26-6.32-.91-8.72Zm-2.3-5.07c.64,1.12,.88,2.43,.66,3.7-.04-.03-.12-.07-.17-.1l-5.88-3.4c-.3-.17-.67-.17-.97,0l-6.89,3.98v-2.92l5.69-3.29c2.65-1.53,6.03-.62,7.56,2.03Zm-13.25,6.07l2.9-1.68,2.9,1.68v3.35l-2.9,1.68-2.9-1.68v-3.35ZM13.01,1.93c1.3,0,2.55,.45,3.55,1.28-.04,.02-.12,.07-.18,.1l-5.88,3.39c-.3,.17-.48,.49-.48,.84v7.96l-2.53-1.46V7.46c0-3.06,2.47-5.53,5.53-5.54ZM2.68,9.69h0c.65-1.12,1.66-1.98,2.88-2.43v6.99c0,.35,.18,.66,.48,.84l6.88,3.97-2.54,1.47-5.68-3.28c-2.64-1.53-3.55-4.91-2.02-7.56Zm1.55,12.83h0c-.65-1.11-.88-2.43-.66-3.7,.04,.03,.12,.07,.17,.1l5.88,3.4c.3,.17,.67,.17,.97,0l6.88-3.98v2.92l-5.69,3.28c-2.65,1.52-6.03,.62-7.56-2.02Zm11.89,5.08c-1.29,0-2.55-.45-3.54-1.28,.04-.02,.13-.07,.18-.1l5.88-3.39c.3-.17,.49-.49,.48-.84v-7.95l2.53,1.46v6.57c0,3.06-2.48,5.54-5.53,5.54Zm10.34-7.76c-.65,1.12-1.67,1.98-2.88,2.42v-6.99c0-.35-.18-.67-.48-.84h0l-6.89-3.98,2.53-1.46,5.69,3.28c2.65,1.53,3.55,4.91,2.02,7.56Z"
    ></path>
  </svg>
);

const Panels = [
  {
    kind: "cover",
    image: "./images/eli-garcia/cover.png",
    title: "Eli Garcia",
    subTitle: "Space Explorer",
    illustrator: "Midjourney",
    author1: "RobotandKid",
    author2: "ChatGPT",
  },
  {
    kind: "panel",
    caption: "Say hello to Eli. He loved to go on adventures.",
    image: "./images/eli-garcia/01.png",
  },
  {
    kind: "panel",
    caption:
      "One day Eli decided he wanted to go to space! He built a rocket ship in his backyard, and gathered all the necessary things for his journey, like food and water.",
    image: "./images/eli-garcia/02.png",
  },
  {
    kind: "panel",
    caption: "Eli put on his spacesuit and blasted off into space.",
    image: "./images/eli-garcia/03.png",
  },
  {
    kind: "panel",
    caption:
      "As he journeyed through space, Eli saw all sorts of amazing things like stars and planets, and even a shooting star!",
    image: "./images/eli-garcia/04.png",
  },
  {
    kind: "panel",
    caption:
      "Finally, Eli reached the moon! He landed on its surface and got out of his rocket ship to explore. He saw craters and rocks and...",
    image: "./images/eli-garcia/05.png",
  },
  {
    kind: "panel",
    caption: "...Eli even met some friendly aliens!",
    image: "./images/eli-garcia/06.png",
  },
  {
    kind: "panel",
    caption:
      "Eli had so much fun on the moon, but it was time to go back home, so he got back into his rocket ship and blasted back to Earth.",
    image: "./images/eli-garcia/07.png",
  },
  {
    kind: "panel",
    caption:
      "When he returned home, Eli told his mother about his amazing adventure. She was so impressed and couldn't stop laughing at all the silly things Eli did on the moon.",
    image: "./images/eli-garcia/08.png",
  },
  {
    kind: "panel",
    caption:
      "Eli knew that he would always love to go on adventures, and that he would always have fun no matter where his adventures took him next.",
    image: "./images/eli-garcia/09.png",
  },
  {
    kind: "end",
    caption: "The End",
  },
  {
    kind: "about",
    author1Name: "Robot",
    author1Desc: "A digital being who creates digital content.",
    author1Pic: "./images/eli-garcia/about-robot.png",
    author2Name: "Kid",
    author2Desc: "Just a normal kid who sometimes helps Robot.",
    author2Pic: "./images/eli-garcia/about-kid.png",
    author3Name: "Midjourney",
    author3Desc: "An artificial intelligence that draws.",
    author3Pic: "./images/eli-garcia/about-midjourney.png",
    author4Name: "ChatGPT",
    author4Desc: "An artificial intelligence that writes.",
  },
] as const;

export default function Page() {
  return (
    <StoryBoard>
      {Panels.map((panel, key) => {
        switch (panel.kind) {
          case "cover": {
            const authors = `Written by ${panel.author1} and ${panel.author2}`;
            const illustrators = `Illustrated by ${panel.illustrator}`;
            return (
              <Cover key={key}>
                <PanelBackground src={panel.image} />
                <TitleContainer initialFontSize="10rem">
                  <AutoDiv>
                    <Title>{panel.title}</Title>
                    <SubTitle>{panel.subTitle}</SubTitle>
                  </AutoDiv>
                </TitleContainer>
                <CreditsContainer>
                  <AuthorContainer initialFontSize="5rem">
                    <AuthorInner>
                      <WavyLetters delay="0.04s">{authors}</WavyLetters>
                    </AuthorInner>
                  </AuthorContainer>
                  <IllustratorContainer initialFontSize="5rem">
                    <IllustratorInner>
                      <StaticColorChange>{illustrators}</StaticColorChange>
                    </IllustratorInner>
                  </IllustratorContainer>
                </CreditsContainer>
              </Cover>
            );
          }
          case "panel":
            return (
              <Panel key={key}>
                <PanelBackground src={panel.image} />
                <Caption initialFontSize="10rem">
                  <CaptionText>{panel.caption}</CaptionText>
                </Caption>
              </Panel>
            );
          case "end":
            return (
              <End key={key}>
                <EndHeaderContainer initialFontSize="10rem">
                  <AutoDiv>
                    <h2>{panel.caption}</h2>
                  </AutoDiv>
                </EndHeaderContainer>
              </End>
            );
          case "about":
            return (
              <About key={key}>
                <AboutHeaderContainer initialFontSize="10rem">
                  <AutoDiv>
                    <AboutHeader as="h1">About</AboutHeader>
                  </AutoDiv>
                </AboutHeaderContainer>

                <AboutItem>
                  <AboutItemPicContainer>
                    <AboutItemPic src={panel.author1Pic}></AboutItemPic>
                  </AboutItemPicContainer>
                  <AboutItemTextContainer initialFontSize="10rem">
                    <AboutItemText>
                      <div style={{ fontWeight: "bold", fontStyle: "italic" }}>
                        {panel.author1Name}
                      </div>
                      {panel.author1Desc}
                    </AboutItemText>
                  </AboutItemTextContainer>
                </AboutItem>
                <AboutItem>
                  <AboutItemPicContainer>
                    <AboutItemPic src={panel.author2Pic}></AboutItemPic>
                  </AboutItemPicContainer>
                  <AboutItemTextContainer initialFontSize="10rem">
                    <AboutItemText>
                      <div style={{ fontWeight: "bold", fontStyle: "italic" }}>
                        {panel.author2Name}
                      </div>
                      {panel.author2Desc}
                    </AboutItemText>
                  </AboutItemTextContainer>
                </AboutItem>
                <AboutItem>
                  <AboutItemPicContainer>
                    <AboutItemPic src={panel.author3Pic}></AboutItemPic>
                  </AboutItemPicContainer>
                  <AboutItemTextContainer initialFontSize="10rem">
                    <AboutItemText>
                      <div style={{ fontWeight: "bold", fontStyle: "italic" }}>
                        {panel.author3Name}
                      </div>
                      {panel.author3Desc}
                    </AboutItemText>
                  </AboutItemTextContainer>
                </AboutItem>
                <AboutItem>
                  <AboutItemPicContainer>
                    <AboutItemSvg>
                      <OpenAiSvg />
                    </AboutItemSvg>
                  </AboutItemPicContainer>
                  <AboutItemTextContainer initialFontSize="10rem">
                    <AboutItemText>
                      <div style={{ fontWeight: "bold", fontStyle: "italic" }}>
                        {panel.author4Name}
                      </div>
                      {panel.author4Desc}
                    </AboutItemText>
                  </AboutItemTextContainer>
                </AboutItem>
              </About>
            );
        }
      })}
    </StoryBoard>
  );
}
