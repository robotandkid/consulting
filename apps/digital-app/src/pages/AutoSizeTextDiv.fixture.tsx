import styled from "styled-components";
import { AutoSizer } from "./AutoSizeTextDiv";
import { GlobalStyle } from "./GlobalStyles";
import { TypewriterText } from "./TypewriterText";

const BigTypewriterText = styled(TypewriterText)``;

const SmallContainer = styled(AutoSizer)`
  width: 50px;
  height: 25px;
  background: pink;
`;

const LargeContainer = styled(AutoSizer)`
  width: 100vw;
  height: 25vh;
  background: #ffcf7d;
`;

export const WithSmallContainer = () => {
  return (
    <>
      <GlobalStyle />
      <BigTypewriterText fontSize="10rem">
        Meet Eli. He loved to go on adventures.
      </BigTypewriterText>
      <SmallContainer initialFontSize="20rem">
        <TypewriterText>Meet Eli. He loved to go on adventures.</TypewriterText>
      </SmallContainer>
    </>
  );
};

export const WithLargeContainer = () => {
  return (
    <>
      <GlobalStyle />
      <BigTypewriterText fontSize="10rem">
        Meet Eli. He loved to go on adventures.
      </BigTypewriterText>
      <LargeContainer initialFontSize="20rem">
        <TypewriterText>Meet Eli. He loved to go on adventures.</TypewriterText>
      </LargeContainer>
    </>
  );
};
