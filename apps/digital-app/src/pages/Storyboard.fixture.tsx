import styled from "styled-components";
import { AutoSizer } from "./AutoSizeTextDiv";
import { GlobalStyle } from "./GlobalStyles";
import { Panel, PanelBackground, StoryBoard } from "./Storyboard";
import { TypewriterText } from "./TypewriterText";

const Caption = styled(AutoSizer)`
  color: white;
  font-weight: bold;
  width: 100vw;
  height: 25vh;
  background: rgba(255, 255, 255, 0.187);
`;

export const StoryboardGallery = () => {
  return (
    <>
      <GlobalStyle />
      <StoryBoard>
        <Panel>
          <PanelBackground src="/images/eli-garcia/01.png" />
        </Panel>
        <Panel>
          <PanelBackground src="/images/eli-garcia/02.png" />
        </Panel>
      </StoryBoard>
    </>
  );
};

export const StoryboardWithText = () => {
  return (
    <>
      <GlobalStyle />
      <StoryBoard>
        <Panel>
          <PanelBackground src="/images/eli-garcia/01.png" />
          <Caption initialFontSize="25rem">
            <TypewriterText>Hello world</TypewriterText>
          </Caption>
        </Panel>
        <PanelBackground src="/images/eli-garcia/02.png" />
      </StoryBoard>
    </>
  );
};
