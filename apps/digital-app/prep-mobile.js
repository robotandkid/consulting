/* eslint-disable no-console */
const fs = require("fs");

console.log("Fixing the NextJS relative base path...");

const fileName = "./out/eli-garcia.html";
const searchValue = "/REPLACE_ME_IN_NODE_SCRIPT";
const replaceValue = ".";

const file = fs.readFileSync(fileName, "utf8");
const result = file.replace(new RegExp(searchValue, "g"), replaceValue);

fs.writeFileSync(fileName, result, "utf8");

const path = require("path");

console.log("Outputting the new pubspec asset paths");

const directory = "./out";

console.log("directories:");

function readDirectory(dir, parent = "") {
  const files = fs.readdirSync(dir);
  files.forEach((file) => {
    const filePath = path.join(dir, file);
    if (fs.lstatSync(filePath).isDirectory()) {
      console.log(`    - ${parent}${file}/`);
      readDirectory(filePath, `${parent}${file}/`);
    }
  });
}

console.log(`    - assets/out/`);

readDirectory(directory, "assets/out/");
