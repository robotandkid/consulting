# `@robotandkid/digital-app`

## Development

```
yarn workspace @robotandkid/digital-app dev
```

- This will run the dev version.
- Note: the baseUrl in the next.config.js!

## Mobile

```
yarn workspace @robotandkid/digital-app mobile
```

- Make sure to update the spec YAML in the flutter project.

## Troubleshooting

- Images must be relative to the current baseUrl! Otherwise, flutter will not
  load them properly.
