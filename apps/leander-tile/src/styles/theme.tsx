import { css, ThemeProvider } from "styled-components";
import React from "react";

const colors = {
  white: "white",
  black: "black",
  green1: "#91AA9D",
  green2: "#D1DBBD",
  gray1: "#6E6460",
  gray2: "#8B8885",
  gray4: "#6F685D",
  gray5: "#252424",
};

const fonts = {
  sansSerif: "sans-serif",
  monospace: "monospace",
  size: "1.5rem",
};

const concepts = {
  mainBrand: {
    color: colors.black,
    colorSecondary: colors.gray4,
    colorOnDarkBg: colors.white,
    colorBg: colors.gray4,
    buttonEnabledColor: colors.gray5,
    buttonDisabledColor: colors.green2,
    disabledColor: colors.green1,
    hoverColor: colors.green2,
    titleFont: fonts.sansSerif,
    textFont: fonts.sansSerif,
    textFontSize: fonts.size,
    headerFont: fonts.sansSerif,
    featureFont: fonts.sansSerif,
    featureFontSize: `calc(${fonts.size}*1.25)`,
    formFont: fonts.monospace,
  },
};

const entities = {
  landing: () => css`
    background-color: ${concepts.mainBrand.colorBg};
  `,
  nav: {
    primaryColor: concepts.mainBrand.color,
  },
  menu: {
    sidebarFont: concepts.mainBrand.headerFont,
    sidebarColor: concepts.mainBrand.colorOnDarkBg,
  },

  pager: {
    font: concepts.mainBrand.headerFont,
    disabledColor: concepts.mainBrand.disabledColor,
    hoverColor: concepts.mainBrand.hoverColor,
  },
  about: {
    titleFont: concepts.mainBrand.headerFont,
    textFont: concepts.mainBrand.featureFont,
    textSize: concepts.mainBrand.featureFontSize,
    color: concepts.mainBrand.colorOnDarkBg,
  },
  contact: {
    titleFont: concepts.mainBrand.headerFont,
    textFont: concepts.mainBrand.featureFont,
    textSize: concepts.mainBrand.textFontSize,
    formFont: concepts.mainBrand.formFont,
    color: concepts.mainBrand.color,
    buttonBgColor: concepts.mainBrand.buttonEnabledColor,
    buttonTextColor: concepts.mainBrand.colorOnDarkBg,
    inputBorderColor: concepts.mainBrand.buttonEnabledColor,
  },
  thanks: {
    titleFont: concepts.mainBrand.titleFont,
    titleColor: concepts.mainBrand.color,
    subtitleColor: concepts.mainBrand.colorSecondary,
  },
};

export const theme = {
  header: {
    font: "sans-serif",
  },
  text: {
    font: "sans-serif",
  },
  pager: {
    font: "sans-serif",
    disabledColor: "#91AA9D",
    hoverColor: "#D1DBBD",
  },
  color: {
    white: "white",
    black: "black",
    primary: "white",
    secondary: "black",
    secondaryComplement: "rgba(0,0,0,0.05)",
    tertiary: "black",
  },
  colors,
  concepts,
  entities,
};

export interface AppThemeProps {
  theme: typeof theme;
}

export function AppThemeProvider(props: { children?: React.ReactNode }) {
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
}
