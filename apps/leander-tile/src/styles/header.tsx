import { forwardRef, ReactNode } from "react";
import styled from "styled-components";
import { H2, H3 } from "./styles";

const H2Container = styled.div`
  width: 6rem;
  padding: 0;
  padding-bottom: 0.25rem;
  border-bottom: 3pt solid ${(p) => p.theme.color.secondary};
  display: flex;
  justify-content: center;
  margin-top: 5rem;
  margin-bottom: 3rem;
`;

const H2Inner = styled(H2)`
  padding: 0;
  margin: 0;
`;

export const LeanderH2 = forwardRef<
  HTMLDivElement,
  { children?: ReactNode; className?: string }
>((props, ref) => {
  return (
    <H2Container ref={ref} className={props.className}>
      <H2Inner className={props.className} tabIndex={0}>
        {props.children}
      </H2Inner>
    </H2Container>
  );
});

export const LeanderH3 = styled(H3)`
  font-style: italic;
  margin-top: 3rem;
  margin-bottom: 0rem;
  font-size: 2rem;

  @media only screen and (min-width: 768px) {
    font-size: 2.5rem;
  }
`;
