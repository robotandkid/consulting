import styled, { css } from "styled-components";
import {
  CenteredColumn,
  ResponsiveCenteredColumn,
  media,
} from "@robotandkid/components";

export { CenteredColumn, ResponsiveCenteredColumn };

export const galleryTypography = css`
  font-family: ${(p) => p.theme.header.font};
  font-size: 2.5rem;

  @media only screen and (min-width: 768px) {
    font-size: 2rem;
  }

  @media only screen and (min-width: 1920px) {
    font-size: 3.5rem;
  }
`;

export const PageContainer = styled.section`
  padding: 1rem;
  max-width: 100%;

  @media only screen and (min-width: 768px) {
    width: 37rem;
  }
`;

export const sharedTitleStyle = css`
  font-family: ${(p) => p.theme.header.font};
  background: transparent;
  letter-spacing: -0.12rem;
  line-height: 1;
`;

export const Title = styled.h1`
  color: ${(p) => p.theme.color.primary};
  font-family: ${(p) => p.theme.header.font};
  text-align: center;
  font-size: 5rem;
  width: min(40rem, 95vw);
  position: absolute;
  mix-blend-mode: color-dodge;
  font-weight: bold;
  letter-spacing: -0.05em;
  margin: 0;
  padding: 0;

  ${media("tablet")(css`
    font-size: 10rem;
  `)}
`;

export const H1 = styled.h1`
  ${sharedTitleStyle}
  font-weight: normal;
  font-size: 8em;
  padding: 0;
  margin: 0;
  margin-top: 1em;
  margin-bottom: 0.5em;
  background: transparent;

  @media only screen and (min-width: 768px) {
    font-size: 10em;
    margin-top: 0;
  }
`;

export const H2 = styled.h2`
  ${sharedTitleStyle}
  font-family: ${(p) => p.theme.header.font};
  font-weight: bold;
  color: ${(p) => p.theme.color.secondary};
  font-size: 5em;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;
  margin-top: 0.5em;

  @media only screen and (min-width: 768px) {
    font-size: 7em;
  }
`;

export const H3 = styled.h3`
  ${sharedTitleStyle}
  font-family: ${(p) => p.theme.header.font};
  font-weight: bold;
  color: ${(p) => p.theme.color.tertiary};
  font-size: 3em;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;

  @media only screen and (min-width: 768px) {
    font-size: 7em;
  }
`;

export const text = css`
  font-family: ${(p) => p.theme.text.font};
  font-size: 1.5rem;
  margin-top: 1em;
  margin-bottom: 1em;
  text-align: justify;

  &:first-child {
    margin-top: 0em;
  }

  &:last-child {
    margin-bottom: 0em;
  }
`;

export const Text = styled.p`
  ${text}
`;

const _ListItem = styled(Text)`
  margin-top: 0.5em;
  margin-bottom: 0.5em;
`;

export const ListItem = (props: any) => <_ListItem {...props} as="li" />;
