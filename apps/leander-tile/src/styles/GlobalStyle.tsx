import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    font-size: 62.5%;
    margin: 0;
    padding: 0;
  }
`;
