import styled from "styled-components";
import { FacebookLogo, InstagramLogo } from "phosphor-react";
import { ExternalLink } from "@robotandkid/components";

const FacebookIcon = styled(FacebookLogo)`
  width: 1em;
  height: 1em;
  outline: none;
  border: none;
  margin-bottom: -0.15em;
  margin-left: -0.15em;
`;

const InstagramIcon = styled(InstagramLogo)`
  width: 1em;
  height: 1em;
  outline: none;
  border: none;
  margin-bottom: -0.15em;
  margin-left: -0.15em;
`;

export const SocialMediaItem = ExternalLink;

const socialMediaLinks = [
  {
    text: "Instagram",
    href: "https://www.instagram.com/leandertile/",
    icon: <InstagramIcon />,
  },
  {
    text: "Facebook",
    href: "https://www.facebook.com/people/Leander-Tile/100013784509250/",
    icon: <FacebookIcon />,
  },
] as const;

export function SocialMediaLinks(props: { className?: string }) {
  return (
    <>
      {socialMediaLinks.map(({ text, href, icon }) => {
        return (
          <SocialMediaItem key={text} className={props.className} href={href}>
            {text} {icon}
          </SocialMediaItem>
        );
      })}
    </>
  );
}
