import { ImageKit, LinkTarget, SetStaticTOC } from "@robotandkid/components";
import styled from "styled-components";
import { useI18Context } from "../../locales/i18n.context";
import { LeanderH2, LeanderH3 } from "../../styles/header";
import {
  CenteredColumn,
  ResponsiveCenteredColumn,
  Text,
} from "../../styles/styles";

const ImageContainer = styled.div`
  border: 1pt solid transparent;
  border-radius: 50%;
  overflow: hidden;

  width: 60vw;
  height: 60vw;

  @media only screen and (min-width: 768px) {
    width: min(20vw, 26rem);
    height: min(20vw, 26rem);
  }
`;

function ServiceImage(props: { alt: string; src: `/${string}` }) {
  return (
    <ImageContainer>
      <ImageKit
        alt={props.alt}
        src={props.src}
        width={250}
        height={250}
        layout="responsive"
        app="leandertile"
      />
    </ImageContainer>
  );
}

const ServiceDescription = styled(Text)`
  color: ${(p) => p.theme.color.secondary};
  padding: 1rem;
  text-align: center;
`;

const ServiceColumn = styled(CenteredColumn)`
  padding-top: 1rem;
  padding-bottom: 1rem;
  width: 100%;
`;

function Service(props: {
  title: string;
  src: `/${string}`;
  children: string;
}) {
  return (
    <ServiceColumn as="li">
      <ServiceImage alt={props.title} src={props.src} />
      <LeanderH3>{props.title}</LeanderH3>
      <ServiceDescription>{props.children}</ServiceDescription>
    </ServiceColumn>
  );
}

const ServiceCenteredColumn = styled(CenteredColumn)`
  width: 90vw;
`;

const Content = styled(ResponsiveCenteredColumn)`
  padding: 0;
  margin: 0;
  background: ${(p) => p.theme.color.secondaryComplement};
  width: 100%;

  @media only screen and (min-width: 768px) {
    padding-top: 2rem;
    padding-bottom: 4rem;
  }
`;

const href = "#services";

export function LeanderServices() {
  const t = useI18Context();

  return (
    <>
      <ServiceCenteredColumn>
        <LinkTarget href={href}>
          <LeanderH2>{t.servicesTitle}</LeanderH2>
        </LinkTarget>
        <Content as="ol">
          <Service title={t.tileTitle} src="/leandertile-service-tile.jpg">
            {t.tileServiceDescription}
          </Service>
          <Service
            title={t.bathroomTitle}
            src="/leandertile-service-bathroom.jpeg"
          >
            {t.bathroomServiceDescription}
          </Service>
          <Service
            title={t.hardwoodTitle}
            src="/leandertile-service-hardwood.jpg"
          >
            {t.hardwoodServiceDescription}
          </Service>
        </Content>
      </ServiceCenteredColumn>
      <SetStaticTOC index={1} href={href} name={t.servicesTitle} />
    </>
  );
}
