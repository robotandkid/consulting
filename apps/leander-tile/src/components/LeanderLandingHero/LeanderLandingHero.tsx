import {
  ImageKit,
  LinkTarget,
  Parallax,
  SetStaticTOC,
} from "@robotandkid/components";
import { m } from "framer-motion";
import React from "react";
import styled from "styled-components";
import { useI18Context } from "../../locales/i18n.context";

import { Title } from "../../styles/styles";

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  overflow: hidden;
  /* Not sure why this is needed */
  margin-top: -0.75rem;
`;

const HeroTitle = styled(Title)`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ImageContainer = styled(m.div)`
  ${(p) => p.theme.entities.landing()}
  width: 100%;
  height: calc(100vh + 600px);
`;

const href = "#title";

export function LeanderLandingHero() {
  const t = useI18Context();

  return (
    <>
      <Container>
        <Parallax offset={300}>
          <ImageContainer>
            <ImageKit
              alt="image of landing page"
              src="/leandertile-landing.jpeg"
              useBlur
              layout="fill"
              objectFit="cover"
              objectPosition="center top"
              app="leandertile"
            />
          </ImageContainer>
        </Parallax>
        <LinkTarget href={href}>
          <HeroTitle>
            <span>Leander Tile</span>
          </HeroTitle>
        </LinkTarget>
      </Container>
      <SetStaticTOC index={0} href={href} name={t.homeTitle} />
    </>
  );
}
