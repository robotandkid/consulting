import { LinkTarget, media, SetStaticTOC } from "@robotandkid/components";
import { PhoneCall } from "phosphor-react";
import React, { useRef } from "react";
import styled, { css } from "styled-components";
import { useI18Context } from "../../locales/i18n.context";
import { LeanderH2 } from "../../styles/header";
import {
  CenteredColumn,
  ResponsiveCenteredColumn,
  text,
  Text,
} from "../../styles/styles";
import { SocialMediaLinks } from "../LeanderSocial/LeanderSocial";

export const ButtonContained = styled.a`
  ${text}

  text-decoration: none;
  padding: 0.5em;
  margin-top: 1rem;
  transition: opacity 0.1s;
  box-shadow: 0.2em 0.2em 0.5em 1px black;
  transition: background 0.1s, opacity 0.1s;

  &:hover {
    opacity: 0.92;
  }

  &:focus {
    opacity: 0.76;
  }

  &:active {
    opacity: 0.68;
  }
`;

export const ButtonOutline = styled(ButtonContained)`
  border: 1px solid black;
  background-color: white;
  box-shadow: none;
  transition: background 0.1s, opacity 0.1s;

  &:hover {
    opacity: 1;
    background-color: hsl(0, 0%, 96%);
  }

  &:focus {
    opacity: 1;
    background-color: hsl(0, 0%, 88%);
  }

  &:active {
    opacity: 1;
    background-color: hsl(0, 0%, 88%);
  }
`;

const Container = styled(CenteredColumn)`
  width: 90vw;
  margin-top: 2rem;
  color: ${(p) => p.theme.entities.contact.color};
`;

const ResponsiveContainer = styled(ResponsiveCenteredColumn)`
  width: min(100%, 120em);
`;

const Column = styled(CenteredColumn)`
  ${media("tablet")(css`
    justify-content: flex-start;
  `)}
`;

const StyledCallContained = styled(ButtonContained)`
  ${text}
  font-size: ${(p) => p.theme.entities.contact.textSize};
  color: ${(p) => p.theme.entities.contact.buttonTextColor};
  background-color: ${(p) => p.theme.entities.contact.buttonBgColor};
`;

const StyledCallOutline = styled(ButtonOutline)`
  ${text}
  color: ${(p) => p.theme.entities.nav.primaryColor};
  border-color: ${(p) => p.theme.entities.nav.primaryColor};
`;

const CallButtonIcon = styled(PhoneCall)`
  width: 1.5em;
  height: 1.5em;
  outline: none;
  border: none;
  margin-bottom: -0.5em;
`;

const Title = styled(LeanderH2)`
  color: ${(p) => p.theme.entities.contact.color};
  border-color: ${(p) => p.theme.entities.contact.color};
`;

export function CallButton(props: {
  type?: "contained" | "outline";
  short?: boolean;
  className?: string;
  ariaLabel: string;
}) {
  const { type = "contained", short, className } = props;
  const ref = useRef<HTMLAnchorElement>(null);
  const Call = type === "contained" ? StyledCallContained : StyledCallOutline;
  const weight = type === "contained" ? undefined : "fill";
  const t = useI18Context();

  return (
    <Call
      as="a"
      className={className}
      ref={ref}
      href="tel:+15126323169"
      tabIndex={0}
      onKeyPress={(e: any) => {
        if (e.key === " ") {
          ref.current?.click();
        }
      }}
      aria-label={props.ariaLabel}
    >
      <CallButtonIcon weight={weight} /> {!short && t.contactCallLabel}
    </Call>
  );
}

const Form = styled.form`
  ${text}
  font-size: ${(p) => p.theme.entities.contact.textSize};
  display: flex;
  flex-direction: column;
  width: min(20em, 80vw);

  & > input[type="text"],
  & > input[type="email"],
  & > input[type="tel"],
  & > textarea {
    padding: 0.5em;
    font-family: ${(p) => p.theme.entities.contact.formFont};
    font-size: ${(p) => p.theme.entities.contact.textSize};
    margin-top: 0.5em;
    margin-bottom: 1em;
    box-sizing: border-box;
    border: 1pt solid ${(p) => p.theme.entities.contact.inputBorderColor};
  }

  & > textarea {
    height: 10em;
  }

  & > input[type="submit"] {
    cursor: pointer;
    padding: 0.5em;
    font-size: ${(p) => p.theme.entities.contact.textSize};
    margin-top: 0.5em;
    margin-bottom: 1em;
    color: ${(p) => p.theme.entities.contact.buttonBgColor};
    width: 10em;
    align-self: center;
    background: transparent;
    border: 1pt solid ${(p) => p.theme.entities.contact.inputBorderColor};
    transition: background 0.1s;

    &:hover {
      background-color: rgba(0, 0, 0, 0.04);
    }

    &:focus {
      background-color: rgba(0, 0, 0, 0.12);
    }

    &:active {
      background-color: rgba(0, 0, 0, 0.12);
    }
  }
`;

const ContactText = styled(Text)`
  margin-top: 0;
  margin-bottom: 0;
`;

const Social = styled(SocialMediaLinks)`
  ${text}
  padding-top: 0.5em;
  padding-bottom: 0.5em;

  margin-top: 1rem;
  margin-bottom: 0;

  &:last-child {
    margin-top: 0;
    margin-bottom: 1rem;
  }
`;

const href = "#contact";

export function LeanderContact() {
  const t = useI18Context();

  return (
    <>
      <Container as="section">
        <LinkTarget href={href}>
          <Title>{t.contactTitle}</Title>
        </LinkTarget>
        <ResponsiveContainer>
          <Column>
            <CallButton ariaLabel="click to call" />
            <ContactText>{t.contactDays}</ContactText>
            <ContactText>9&ndash;5pm CST</ContactText>
            <Social />
          </Column>
          <Column>
            <Form
              name="contact"
              data-netlify="true"
              method="POST"
              action="/thanks"
            >
              <input type="hidden" name="form-name" value="contact" />
              <label htmlFor="contact-name">{t.contactNameLabel}</label>
              <input
                type="text"
                name="name"
                autoComplete="name"
                required
                id="contact-name"
              />
              <label htmlFor="contact-email">{t.contactEmailLabel}</label>
              <input
                type="email"
                name="email"
                autoComplete="email"
                required
                id="contact-email"
              />
              <label htmlFor="contact-phone">{t.contactPhoneLabel}</label>
              <input
                type="tel"
                name="phone"
                autoComplete="tel"
                maxLength={10}
                minLength={10}
                required
                id="contact-phone"
              />
              <label htmlFor="contact-message">{t.contactMessageLabel}</label>
              <textarea
                name="message"
                autoComplete="off"
                maxLength={1000}
                minLength={10}
                required
                id="contact-message"
              />
              <input type="submit" value={t.contactSubmitButton} />
            </Form>
          </Column>
        </ResponsiveContainer>
      </Container>
      <SetStaticTOC index={4} href={href} name={t.contactTitle} />
    </>
  );
}
