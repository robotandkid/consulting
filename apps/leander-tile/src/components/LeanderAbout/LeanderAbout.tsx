import {
  ImageKit,
  LinkTarget,
  Parallax,
  SetStaticTOC,
} from "@robotandkid/components";
import { m } from "framer-motion";
import React from "react";
import styled from "styled-components";
import { useI18Context } from "../../locales/i18n.context";
import { LeanderH2 } from "../../styles/header";
import { CenteredColumn, Text as RawText } from "../../styles/styles";

const Container = styled.div`
  width: 100vw;
  height: max(100vh, 60em);
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  overflow: hidden;
  margin-top: 8rem;
  padding-bottom: 1rem;
`;

const Column = styled(CenteredColumn)`
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const ImageContainer = styled(m.div)`
  width: 100%;
  height: calc(max(100vh, 60em) + 600px);
`;

const AboutTitle = styled(LeanderH2)`
  color: ${(p) => p.theme.entities.about.color};
  border-color: ${(p) => p.theme.entities.about.color};
`;

const Text = styled(RawText)`
  background: transparent;
  color: ${(p) => p.theme.entities.about.color};
  font-family: ${(p) => p.theme.entities.about.textFont};
  font-size: ${(p) => p.theme.concepts.mainBrand.textFontSize};
  padding-left: 2rem;
  padding-right: 2rem;
  max-width: 35em;
  text-align: center;

  @media only screen and (min-width: 600px) {
    font-size: ${(p) => p.theme.entities.about.textSize};
    padding-left: 4rem;
    padding-right: 4rem;
  }
`;

const href = "#about";

export function LeanderAbout() {
  const t = useI18Context();

  return (
    <>
      <Container as="section">
        <Parallax offset={200}>
          <ImageContainer>
            <ImageKit
              alt="about section background"
              src="/leandertile-abouta.jpg"
              layout="fill"
              objectFit="cover"
              objectPosition="center top"
              app="leandertile"
            />
          </ImageContainer>
        </Parallax>
        <Column>
          <LinkTarget href={href}>
            <AboutTitle>{t.aboutTitle}</AboutTitle>
          </LinkTarget>
          <Text>{t.aboutDescription}</Text>
          <Text>{t.aboutMore}</Text>
        </Column>
      </Container>
      <SetStaticTOC index={3} href={href} name={t.aboutTitle} />
    </>
  );
}
