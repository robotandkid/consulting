import { useIsAtMostMediaSize } from "@robotandkid/components";
import styled from "styled-components";
import { CallButton } from "../../LeanderContact/LeanderContact";

const Button = styled(CallButton)`
  position: fixed;
  top: 0.5rem;
  right: 1rem;
  z-index: 9999;
`;

export function LeanderCallButton() {
  const isMobile = useIsAtMostMediaSize("mobileL", false);

  return (
    <>{isMobile && <Button short type="outline" ariaLabel="Click to call" />}</>
  );
}
