import { LeanderCallButton } from "./LeanderCallButton";
import { FullLeanderMenu } from "./LeanderHamburgerMenu";

export function LeanderNav() {
  return (
    <nav>
      <FullLeanderMenu />
      <LeanderCallButton />
    </nav>
  );
}
