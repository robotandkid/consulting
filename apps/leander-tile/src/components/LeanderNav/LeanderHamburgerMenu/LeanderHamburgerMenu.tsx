import {
  HamburgerMenu,
  HamburgerMenuContext,
  HamburgerMenuItem,
  HamburgerMenuToggleContext,
  InternalLink,
  NormalizedAnchor,
  StaticTOCContext,
  useIsAtMostMediaSize,
  useOnClickA11y,
} from "@robotandkid/components";
import {
  LayoutGroup,
  m,
  MotionValue,
  useScroll,
  useTransform,
} from "framer-motion";
import { useRouter } from "next/dist/client/router";
import { List, XCircle } from "phosphor-react";
import React, { useContext, useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { Text } from "../../../styles/styles";
import { SocialMediaLinks } from "../../LeanderSocial/LeanderSocial";

const HamburgerContainer = styled(m.div)`
  position: relative;
`;

const MenuButtonIcon = styled(List)`
  width: 2.5rem;
  height: 2.5rem;
  outline: none;
  border: none;
`;

const XButtonIcon = styled(XCircle)`
  width: 2.5rem;
  height: 2.5rem;
  outline: none;
  border: none;
`;

const sideMenuItem = css`
  color: ${(p) => p.theme.entities.menu.sidebarColor} !important;
  font-family: ${(p) => p.theme.entities.menu.sidebarFont};
  font-size: 3rem;
  margin-left: 2rem;
  margin-top: 2rem;
  position: relative;

  &:visited {
    color: ${(p) => p.theme.entities.menu.sidebarColor} !important;
  }
  /**
  Note: there's no onhover because this is for mobile
   */
`;

const StyledSideMenuItem = styled(HamburgerMenuItem)`
  ${sideMenuItem}
`;

const SocialMediaHeader = styled(Text)`
  ${sideMenuItem}

  font-size: 1.5rem;
  margin-bottom: -0.5rem !important;
  padding-bottom: 0;
`;

const SocialMedia = styled(SocialMediaLinks)`
  ${sideMenuItem}

  &:last-child {
    margin-bottom: 2rem;
  }
`;

function SideMenuItem(props: { href: `#${string}`; name: string }) {
  const { href, name } = props;

  return (
    <InternalLink href={href}>
      <StyledSideMenuItem>{name}</StyledSideMenuItem>
    </InternalLink>
  );
}

function MobileMenu() {
  const { items: tocItems } = useContext(StaticTOCContext);

  const items = tocItems.map((item) => {
    return <SideMenuItem key={item.name} href={item.href} name={item.name} />;
  });

  return (
    <HamburgerContainer layoutId="menu">
      <HamburgerMenu>
        <MenuButtonIcon weight="bold" />
        <XButtonIcon weight="bold" />
        {items}
        <SocialMediaHeader>Follow</SocialMediaHeader>
        <SocialMedia />
      </HamburgerMenu>
    </HamburgerContainer>
  );
}

const NavContainer = styled(m.div)`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  z-index: 9999;
`;

const NavMenu = styled(m.div)`
  width: 80%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const NavItemContainer = styled(m.div)`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin-left: 1rem;
  margin-right: 1rem;
  margin-top: 0.5rem;
  line-height: 1;
`;

const StyledNavItem = styled(NormalizedAnchor)`
  font-family: ${(p) => p.theme.header.font};
  font-size: 2rem;

  outline: none;
`;

const NavItemUnderline = styled(m.div)`
  width: 4rem;
  height: 3pt;
  margin-top: 0.5rem;
  margin-bottom: 1rem;
`;

/**
 * Use this to set the NavItem colors as it moves over the landing hero.
 *
 * - As you scroll past the viewport height, it will make the bg black and
 *   the fg white... that is, it will invert the colors.
 *
 * TODO: can we do this w/o hardcoding the colors?
 */
function useNavItemColorsOverHero(): [
  MotionValue<string>,
  MotionValue<string>
] {
  const { scrollY } = useScroll();
  const viewportHeight = typeof window !== "undefined" ? window.innerHeight : 0;

  const bgColor = useTransform(
    scrollY,
    [0, 0.5 * viewportHeight, viewportHeight],
    ["rgba(255,255,255,0)", "rgba(255,255,255,1)", "rgba(255,255,255,1)"]
  );
  const fgColor = useTransform(
    scrollY,
    [0, 0.5 * viewportHeight, viewportHeight],
    ["rgba(255,255,255,1)", "rgba(0,0,0,1)", "rgba(0,0,0,1)"]
  );

  return [bgColor, fgColor];
}

function NavItem(props: {
  href: `#${string}`;
  name: string;
  active: boolean;
  onTap: (name: string) => void;
}) {
  const { href, name, active, onTap } = props;
  const [, fgColor] = useNavItemColorsOverHero();

  return (
    <InternalLink href={href}>
      <NavItemContainer {...useOnClickA11y(() => onTap(name))}>
        <StyledNavItem>{name}</StyledNavItem>
        {active && (
          <NavItemUnderline style={{ background: fgColor }} layoutId="active" />
        )}
      </NavItemContainer>
    </InternalLink>
  );
}

function DesktopMenu() {
  const { items } = useContext(StaticTOCContext);
  const { query } = useRouter() || {};

  const [active, setActive] = useState(() => {
    const lookupIndex = items?.findIndex(
      (item) => item.href === `#${query?.link}`
    );

    return lookupIndex >= 0 ? lookupIndex : items?.[0]?.index || 0;
  });

  const [bgColor, fgColor] = useNavItemColorsOverHero();

  return (
    <NavContainer
      layoutId="menu"
      style={{
        backgroundColor: bgColor,
        color: fgColor,
      }}
    >
      <NavMenu>
        {items.map((item) => {
          return (
            <NavItem
              active={active === item.index}
              onTap={(name: string) =>
                setActive(items.findIndex((item) => item.name === name))
              }
              key={item.name}
              href={item.href}
              name={item.name}
            />
          );
        })}
      </NavMenu>
    </NavContainer>
  );
}

export function FullLeanderMenu() {
  const isMobile = useIsAtMostMediaSize("tablet");
  const { toggle } = useContext(HamburgerMenuToggleContext);
  const { open } = useContext(HamburgerMenuContext);

  useEffect(
    function closeMenuOnResize() {
      if (!isMobile && open) {
        toggle();
      }
    },
    [isMobile, open, toggle]
  );

  return (
    <LayoutGroup>
      {isMobile && <MobileMenu />}
      {!isMobile && <DesktopMenu />}
    </LayoutGroup>
  );
}
