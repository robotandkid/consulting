import {
  FacebookSEO,
  StructuredDataSEO,
  TitleSEO,
} from "@robotandkid/components";
import React from "react";

const structuredData = {
  "@id": "https://www.leandertile.com",
  address: {
    "@type": "PostalAddress",
    addressLocality: "Leander",
    addressRegion: "TX",
    addressCountry: "US",
  },
  name: "Leander Tile",
  image: [
    "https://ik.imagekit.io/bxpwztngs0o/leandertile/leandertile-sd-1x1",
    "https://ik.imagekit.io/bxpwztngs0o/leandertile/leandertile-sd-4x3",
    "https://ik.imagekit.io/bxpwztngs0o/leandertile/leandertile-sd-16x9",
  ],
  "@context": "https://schema.org",
  "@type": "HomeAndConstructionBusiness",
  currenciesAccepted: "USD",
  paymentAccepted: "Cash, Credit Card",
  areaServed: {
    "@type": "AdministrativeArea",
    name: "Williamson County, TX",
    geoCrosses: "Travis County, TX",
  },
  telephone: "+15126323169",
  url: "https://www.leandertile.com",
  hasOfferCatalog: {
    "@type": "OfferCatalog",
    name: "Professional Interior/Exterior Designs",
    itemListElement: [
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Tile",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Bathroom",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Backsplash",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Hardwood",
        },
      },
    ],
  },
  openingHoursSpecification: [
    {
      "@type": "OpeningHoursSpecification",
      dayOfWeek: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
      opens: "09:00",
      closes: "17:00",
    },
  ],
};

const title = `Leander Tile - Interior/Exterior Designs - Tile, Bathroom, Backsplash,
Hardwood`;

const description = `Professional interior/exterior designs for residential and business. 
No middleman or flooring store. Serving Austin and the greater Austin area for over 20 years.`;

export function LeanderSEO() {
  return (
    <>
      <TitleSEO>
        {title}
        {description}
      </TitleSEO>
      <StructuredDataSEO structuredData={structuredData} />;
      <FacebookSEO
        url="https://www.leandertile.com"
        type="website"
        title={title}
        description={description}
        imageUrl="https://ik.imagekit.io/bxpwztngs0o/leandertile/leandertile-landing.jpeg"
        imageWidthPx={720}
        imageHeightPx={960}
      />
    </>
  );
}
