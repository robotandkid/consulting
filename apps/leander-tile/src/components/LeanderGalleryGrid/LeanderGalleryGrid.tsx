import {
  defaultBasicGalleryGrid,
  DefaultBasicGalleryGrid,
  GalleryItem,
  ImageKit,
  InMemoryGalleryGrid,
  LinkTarget,
  SetStaticTOC,
} from "@robotandkid/components";
import React from "react";

import styled, { css } from "styled-components";
import { useI18Context } from "../../locales/i18n.context";
import { LeanderH2 } from "../../styles/header";
import { CenteredColumn } from "../../styles/styles";
import { LeanderGalleryPager } from "./LeanderGalleryPager";

const galleryTypography = css`
  font-family: ${(p) => p.theme.header.font};
  font-size: 2rem;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: -0.01em;

  @media only screen and (min-width: 600px) {
    font-size: 2.5rem;
  }

  @media only screen and (min-width: 768px) {
    font-size: 2rem;
  }

  @media only screen and (min-width: 1920px) {
    font-size: 3.5rem;
  }
`;

const ImageItem = styled(GalleryItem)`
  ${galleryTypography}
  background: ${(p) => p.theme.color.secondaryComplement};
  position: relative;
`;

const Section = styled(CenteredColumn)`
  width: 100%;
  margin-top: 2rem;
`;

const GalleryGrid = styled(DefaultBasicGalleryGrid)`
  ${defaultBasicGalleryGrid}

  grid-template-rows: 50vh;
  grid-auto-rows: 50vh;
`;

const formatter = (x: number) => (x < 10 ? `0${x}` : `${x}`);

const href = "#gallery";

export function LeanderGalleryGrid() {
  const t = useI18Context();

  const itemSrc = t.extras.galleryItems.map((index) => ({
    src: `/leandertile-${formatter(+index)}.jpeg` as `/${string}`,
    index,
  }));
  const items = itemSrc.map(({ src, index }) => {
    const alt = t.extras[`galleryItemAltText-${index}`];
    return (
      <ImageItem key={index}>
        <ImageKit
          alt={alt}
          src={src}
          layout="fill"
          objectFit="cover"
          objectPosition="center top"
          app="leandertile"
        />
      </ImageItem>
    );
  });

  return (
    <Section as="section">
      <LinkTarget href={href}>
        <LeanderH2>{t.galleryTitle}</LeanderH2>
      </LinkTarget>
      <InMemoryGalleryGrid pageSize={20}>
        {LeanderGalleryPager}
        <GalleryGrid>{items}</GalleryGrid>
      </InMemoryGalleryGrid>
      <SetStaticTOC index={2} href={href} name={t.galleryTitle} />
    </Section>
  );
}
