export const messages = {
  homeTitle: "Inicio",
  servicesTitle: "Servicios",
  tileTitle: "Tile",
  tileServiceDescription: "Ceramica, azulejos, suelos, patios",
  bathroomTitle: "Baño",
  bathroomServiceDescription:
    "Alrededores de la tina, conversiones de tina a ducha, reubicación de desagües y grifos, duchas europeas, duchas de entrada a la playa",
  hardwoodTitle: "Madera dura",
  hardwoodServiceDescription:
    "Engineered hardwoods, snap together, nail down, glue down",
  contactTitle: "Contacto",
  contactCallLabel: "Llamar (512) 632-3169",
  contactDays: "Lunes–Viernes",
  contactNameLabel: "Nombre:",
  contactEmailLabel: "Email:",
  contactPhoneLabel: "Teléfono:",
  contactMessageLabel: "Mensaje:",
  contactSubmitButton: "¡Contáctenos!",
  galleryTitle: "Galería",
  aboutTitle: "Acerca",
  aboutDescription:
    "Leander Tile es una diferente tipo de empresa con atención al detalle ★ Sin intermediarios ★ Sin sala de exposición marcada ★ Respuestas honestas con el mejor interés del cliente en mente.",
  aboutMore:
    "Sirviendo a Austin y las ciudades circundantes por más de 20 años.",
};

export const extras = {
  galleryAltText: {
    "4": "Ventana de baño de mármol",
    "5": "Ducha de azulejos blancos con ventana",
    "6": "Ducha de azulejos hexagonales",
    "7": "Ducha de azulejos rectangulares blancos",
    "8": "Ducha de losa gris",
    "9": "Isla de cocina blanca",
    "10": "Bañera de azulejos blancos",
    "11": "Placa para salpicaduras de azulejos blancos en la cocina",
    "12": "Ducha de azulejos grises",
    "13": "Placa para salpicaduras de cocina con azulejos geométricos",
    "17": "Ducha de azulejos blancos",
    "18": "Pavimento de paneles geométricos",
    "19": "Ducha de mármol",
    "20": "Asistente de la abuela",
    "21": "Escaleras modernas de paneles",
    "22": "Escaleras de paneles rectangulares",
    "23": "Vista al lago",
    "24": "Baldosa rectangular blanca",
    "25": "Back splash con ventana",
    "26": "Ducha blanca con suelo de baldosas negras",
  },
};
