import { extras as enExtra, messages as en } from "./messages.en";
import { extras as esExtra, messages as es } from "./messages.es";

const { galleryAltText: enGallery } = enExtra;
const { galleryAltText: esGallery } = esExtra;

/**
 * https://stackoverflow.com/a/53808212
 */
type IfEquals<T, U, Y = unknown, N = never> = (<G>() => G extends T
  ? 1
  : 2) extends <G>() => G extends U ? 1 : 2
  ? Y
  : N;

const exactType = <T, U>(
  draft: T & IfEquals<T, U>,
  expected: U & IfEquals<T, U>
): IfEquals<T, U> => {
  return { draft, expected } as IfEquals<T, U>;
};

exactType(enGallery, esGallery);
exactType(en, es);

export type GalleryItemKey = keyof typeof enGallery;

type GalleryItemAltText = `galleryItemAltText-${GalleryItemKey}`;

const enGalleryAltText = Object.keys(enGallery).reduce(
  (total, key) => ({
    ...total,
    [`galleryItemAltText-${key}` as GalleryItemAltText]: (enGallery as any)[
      key
    ],
  }),
  {} as { [key in GalleryItemAltText]: string }
);

const esGalleryAltText = Object.keys(esGallery).reduce(
  (total, key) => ({
    ...total,
    [`galleryItemAltText-${key}` as GalleryItemAltText]: (esGallery as any)[
      key
    ],
  }),
  {} as { [key in GalleryItemAltText]: string }
);

const galleryItems = Object.keys(enGallery) as readonly GalleryItemKey[];
const enFull = { ...en, extras: { ...enGalleryAltText, galleryItems } };
const esFull = { ...es, extras: { ...esGalleryAltText, galleryItems } };

export type Translations = Readonly<typeof enFull>;

export const getTranslations = (locale: string | undefined): Translations => {
  switch (locale) {
    case "en":
      return enFull;
    case "es":
      return esFull;
    default:
      return enFull;
  }
};
