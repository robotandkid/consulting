import { createContext, useContext } from "react";
import { Translations } from "./i18n";

const I18Context = createContext<Translations>({} as any);

export const useI18Context = () => useContext(I18Context);

export const I18Provider = I18Context.Provider;
