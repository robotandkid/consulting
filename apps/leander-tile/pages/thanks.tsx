import styled from "styled-components";
import { CenteredColumn } from "../src/styles/styles";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 80rem;
`;

const ThanksTitle = styled.div`
  color: ${(p) => p.theme.entities.thanks.titleColor};
  font-family: ${(p) => p.theme.entities.thanks.titleFont};
  line-height: 1;
  text-align: left;
  font-weight: bold;
  font-size: 5rem;
  letter-spacing: -0.05em;
  position: relative;
  padding: 4rem;
  padding-bottom: 0;
  align-self: flex-start;

  @media only screen and (min-width: 768px) {
    font-size: 8rem;
  }
`;

const Subtitle = styled(ThanksTitle)`
  color: ${(p) => p.theme.entities.thanks.subtitleColor};
  font-size: 3rem;
  padding-top: 2rem;
  padding-bottom: 4rem;

  @media only screen and (min-width: 768px) {
    padding-top: 4rem;
    font-size: 5rem;
  }
`;

export default function ThanksPage() {
  return (
    <CenteredColumn>
      <Container>
        <ThanksTitle>Thank you for contacting us!</ThanksTitle>
        <Subtitle>We&apos;ll get back to you shortly</Subtitle>
      </Container>
    </CenteredColumn>
  );
}
