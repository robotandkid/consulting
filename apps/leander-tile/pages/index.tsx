import { getStaticPropsStaticTOC } from "@robotandkid/components";
import { GetStaticProps } from "next";
import React from "react";

import { LeanderAbout } from "../src/components/LeanderAbout/LeanderAbout";
import { LeanderContact } from "../src/components/LeanderContact/LeanderContact";
import { LeanderGalleryGrid } from "../src/components/LeanderGalleryGrid/LeanderGalleryGrid";
import { LeanderLandingHero } from "../src/components/LeanderLandingHero/LeanderLandingHero";
import { LeanderSEO } from "../src/components/LeanderSEO/LeanderSEO";
import { LeanderServices } from "../src/components/LeanderServices/LeanderServices";
import { getTranslations } from "../src/locales/i18n";
import { I18Provider } from "../src/locales/i18n.context";
import { AppThemeProvider } from "../src/styles/theme";

export default function Index() {
  return (
    <>
      <LeanderSEO />
      <header>
        <LeanderLandingHero />
      </header>
      <main>
        <article>
          <section>
            <LeanderServices />
          </section>
          <section>
            <LeanderGalleryGrid />
          </section>
          <section>
            <LeanderAbout />
          </section>
        </article>
      </main>
      <footer>
        <LeanderContact />
      </footer>
    </>
  );
}

export const getStaticProps: GetStaticProps = async (ctx) => {
  const translations = getTranslations(ctx.locale);

  const toc = await getStaticPropsStaticTOC(
    <I18Provider value={translations}>
      <AppThemeProvider>
        <Index />
      </AppThemeProvider>
    </I18Provider>
  )();

  const tocProps = toc.props;

  return {
    props: {
      ...tocProps,
      translations,
    },
  };
};
