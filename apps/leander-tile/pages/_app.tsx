import {
  BodyUnderHamburger,
  GetStaticPropsStaticTOC,
  HamburgerMenuContextProvider,
  StaticTOCProvider,
} from "@robotandkid/components";
import { LazyMotion } from "framer-motion";
import { AppProps as NextAppProps } from "next/app";
import { useRouter } from "next/dist/client/router";
import { LeanderNav } from "../src/components/LeanderNav/LeanderHamburgerMenu/LeanderNav";
import { Translations } from "../src/locales/i18n";
import { I18Provider } from "../src/locales/i18n.context";
import { GlobalStyle } from "../src/styles/GlobalStyle";
import { AppThemeProvider } from "../src/styles/theme";

const loadFramerFeatures = () =>
  import("framer-motion").then((module) => module.domMax);

export type AppProps = GetStaticPropsStaticTOC & { translations: Translations };

export default function ThisApp(props: NextAppProps<AppProps>) {
  const {
    Component,
    pageProps: { initialStaticTOC, translations },
  } = props;
  const { route } = useRouter();

  return (
    <LazyMotion strict features={loadFramerFeatures}>
      <GlobalStyle />
      <AppThemeProvider>
        <I18Provider value={translations}>
          <StaticTOCProvider initialItemsFromSSR={initialStaticTOC}>
            <HamburgerMenuContextProvider>
              {route !== "/thanks" && <LeanderNav />}
              <BodyUnderHamburger>
                <Component {...props.pageProps} />
              </BodyUnderHamburger>
            </HamburgerMenuContextProvider>
          </StaticTOCProvider>
        </I18Provider>
      </AppThemeProvider>
    </LazyMotion>
  );
}
