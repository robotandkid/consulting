const openMenuAndAssert = (forceClick?: boolean) => {
  cy.findByRole("button", { name: /open main menu/i }).click({
    force: forceClick,
  });
  // wait for it to stop moving
  cy.findByRole("navigation", { name: /main menu/i })
    .should("be.visible")
    .and(function animationDone($nav) {
      expect($nav.position().left).to.eq(0);
    });
};

const closeMenuAndAssert = () => {
  cy.findByRole("button", { name: /close main menu/i })
    .click()
    .should("not.be.visible");
  cy.findByRole("navigation", { name: /main menu/i }).should("not.exist");
};

const assertIsInViewport = ($elem: JQuery<HTMLElement>) => {
  const bottom = Cypress.config().viewportHeight;
  const width = Cypress.config().viewportWidth;
  const rect = $elem.get(0).getBoundingClientRect();

  expect(
    rect.top < bottom && rect.right <= width && rect.left >= 0,
    "expected to be in the viewport"
  );
};

const mobileNavigateAndAssert = (name: RegExp, headingName = name) => {
  openMenuAndAssert();
  cy.findByRole("link", { name }).click();
  cy.findByRole("navigation", { name: /main menu/i }).should("not.exist");
  cy.findByRole("heading", { name: headingName }).should(assertIsInViewport);
};

const desktopNavigateAndAssert = (name: RegExp, headingName = name) => {
  cy.findByRole("link", { name }).click();
  cy.findByRole("navigation", { name: /main menu/i }).should("not.exist");
  cy.findByRole("heading", { name: headingName }).should(assertIsInViewport);
};

describe("navigation", () => {
  Cypress.on("window:before:load", (win) => {
    cy.spy(win.console, "error");
    cy.spy(win.console, "warn");
  });

  describe("mobile", () => {
    it(`can click "services"`, () => {
      cy.viewport("iphone-6");
      cy.visit("");
      mobileNavigateAndAssert(/services/i);
    });

    it(`can click "gallery"`, () => {
      cy.viewport("iphone-6");
      cy.visit("");
      mobileNavigateAndAssert(/gallery/i);
    });

    it(`can click "about"`, () => {
      cy.viewport("iphone-6");
      cy.visit("");
      mobileNavigateAndAssert(/about/i);
    });

    it(`can click "contact"`, () => {
      cy.viewport("iphone-6");
      cy.visit("");
      mobileNavigateAndAssert(/contact/i);
    });

    it("can click all of them", () => {
      cy.viewport("iphone-6");
      cy.visit("");
      mobileNavigateAndAssert(/services/i);
      mobileNavigateAndAssert(/gallery/i);
      mobileNavigateAndAssert(/home/i, /leander tile/i);
    });

    it("can open close the menu and it has no console errors", () => {
      cy.viewport("iphone-6");
      cy.visit("");
      // first close by clicking the hamburger
      openMenuAndAssert();
      closeMenuAndAssert();
      // // close by clicking on the landing image
      // openMenuAndAssert(true);
      // cy.get('[aria-hidden="true"]').click();
      // cy.findByRole("navigation", { name: /main menu/i }).should("not.exist");
      cy.window().then((win) => {
        expect(win.console.error).to.have.callCount(0);
        expect(win.console.warn).to.have.callCount(0);
      });
    });
  });

  describe("desktop", () => {
    it(`can click "services"`, () => {
      cy.viewport("macbook-16");
      cy.visit("");
      desktopNavigateAndAssert(/services/i);
    });

    it(`can click "gallery"`, () => {
      cy.viewport("macbook-16");
      cy.visit("");
      desktopNavigateAndAssert(/gallery/i);
    });

    it(`can click "about"`, () => {
      cy.viewport("macbook-16");
      cy.visit("");
      desktopNavigateAndAssert(/about/i);
    });

    it(`can click "contact"`, () => {
      cy.viewport("macbook-16");
      cy.visit("");
      desktopNavigateAndAssert(/contact/i);
    });

    it("can click all of them", () => {
      cy.viewport("macbook-16");
      cy.visit("");
      desktopNavigateAndAssert(/services/i);
      desktopNavigateAndAssert(/gallery/i);
      desktopNavigateAndAssert(/home/i, /leander tile/i);
    });

    it("on scrolling down, nav colors are inverted", () => {
      cy.viewport("macbook-16");
      cy.visit("");

      cy.scrollTo(0, 960 * 0.4);
      cy.findByRole("navigation")
        .children()
        .should(($nav) => {
          // TODO this is so brittle. Need at least a lib to convert to a standard colors
          expect($nav.get(0).style.backgroundColor).to.match(
            /rgba\(255, 255, 255, 0.8\)/
          );
        });

      cy.scrollTo(0, 960 * 0.5);
      cy.findByRole("navigation")
        .children()
        .should(($nav) => {
          expect($nav.get(0).style.backgroundColor).to.match(
            /rgb\(255, 255, 255\)/
          );
          expect($nav.get(0).style.color).to.match(/rgb\(0, 0, 0\)/);
        });
    });
  });
});
