describe("contact", () => {
  it("can fill out contact form", () => {
    cy.findByRole("textbox", { name: /name/i }).type("uriel");
    cy.findByRole("textbox", { name: /email/i }).type("hello@net.com");
    cy.findByRole("textbox", { name: /phone/i }).type("8675309");
    cy.findByRole("textbox", { name: /message/i }).type("hello world");
    cy.findByRole("button", { name: /contact us/i }).click();
  });
});
