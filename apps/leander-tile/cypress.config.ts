import { defineConfig } from "cypress";

export default defineConfig({
  numTestsKeptInMemory: 3,
  e2e: {
    baseUrl: "http://localhost:3000/",
    specPattern: "cypress/e2e/**/*.test.*",
  },
});
