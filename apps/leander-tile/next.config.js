const path = require("path");

const extension = /\.mdx?$/;

const withMDX = require("@next/mdx")({
  extension,
});

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

/** @type {import('next').NextConfig} */
const config = {
  pageExtensions: ["js", "jsx", "ts", "tsx", "mdx"],
  webpack(config) {
    const mdxRule = config.module.rules.find(
      (rule) => `${rule.test}` === `${extension}`
    );
    if (mdxRule) {
      mdxRule.use.push(path.join(__dirname, "./mdx-fm-loader"));
    }

    // netlify bug:
    // https://answers.netlify.com/t/next-js-internal-error-during-functions-bundling-on-build/43647/7
    Object.assign(
      config.externals,
      ["critters"].reduce(
        (externals, name) => ({ ...externals, [name]: `commonjs ${name}` }),
        {}
      )
    );

    return config;
  },
  images: {
    loader: "imgix",
    path: "",
  },
  i18n: {
    defaultLocale: "en",
    locales: ["en", "es"],
  },
};

module.exports = withBundleAnalyzer(withMDX(config));
