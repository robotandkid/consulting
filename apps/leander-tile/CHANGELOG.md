# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.25.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.24.3...@robotandkid/leander-tile@0.25.0) (2024-05-10)


### Features

* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





## [0.24.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.24.2...@robotandkid/leander-tile@0.24.3) (2023-09-19)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.24.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.24.1...@robotandkid/leander-tile@0.24.2) (2023-09-18)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.24.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.24.0...@robotandkid/leander-tile@0.24.1) (2023-09-04)

**Note:** Version bump only for package @robotandkid/leander-tile





# [0.24.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.23.0...@robotandkid/leander-tile@0.24.0) (2023-08-07)


### Features

* upgraded node, pixi.js ([c06a0b4](https://gitlab.com/robotandkid/consulting/commit/c06a0b470991876a2763336d76a4272ca536af75))





# [0.23.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.18...@robotandkid/leander-tile@0.23.0) (2023-08-07)


### Bug Fixes

* **components:** image blur works correctly ([0b3bd56](https://gitlab.com/robotandkid/consulting/commit/0b3bd5687ff7cdbb6123d91b20b75e8f56fb012a))
* **leander-tile:** hamburger menu transitions correctly ([6c39162](https://gitlab.com/robotandkid/consulting/commit/6c39162c1133c439fcadde48b2ca093e22520f67))


### Features

* **masonrypro:** added the gallery ([c04a941](https://gitlab.com/robotandkid/consulting/commit/c04a941ecc2f4901904de7e03921d7859c978047))
* migrated to use BodyUnderHamburger copmonent ([2c9c74a](https://gitlab.com/robotandkid/consulting/commit/2c9c74a91577db6c73d0f457b8058b10d1b3daaa))





## [0.22.18](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.17...@robotandkid/leander-tile@0.22.18) (2022-11-30)


### Bug Fixes

* **leander-tile:** fixed bg offset ([889cd68](https://gitlab.com/robotandkid/consulting/commit/889cd688e4fc113167f3788e5b162a0af31ed1f3))
* **leandertile:** compiles again ([bee1ba2](https://gitlab.com/robotandkid/consulting/commit/bee1ba2afd1f20b11f8b4ba65420bf5119d128cc))
* **leandertile:** fixed call button flickering bug on desktop ([b946b6a](https://gitlab.com/robotandkid/consulting/commit/b946b6a1dfc3a3c59b6083e24b283eb9b28bb0dd))





## [0.22.17](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.16...@robotandkid/leander-tile@0.22.17) (2022-11-28)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.22.16](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.15...@robotandkid/leander-tile@0.22.16) (2022-11-27)


### Bug Fixes

* **leander-tile:** improved SEO ([b0c4669](https://gitlab.com/robotandkid/consulting/commit/b0c4669f13b5e8b7f0e862d47bf8b5ffa3318fbe))
* **leander-tile:** reduced bundle size ([eef28a6](https://gitlab.com/robotandkid/consulting/commit/eef28a613217398d844357c7e840e1a7d3016289))





## [0.22.14](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.13...@robotandkid/leander-tile@0.22.14) (2022-01-09)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.22.13](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.12...@robotandkid/leander-tile@0.22.13) (2021-12-14)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.22.12](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.11...@robotandkid/leander-tile@0.22.12) (2021-12-03)


### Bug Fixes

* **leandertile:** call button is an anchor (as it should be) ([010814f](https://gitlab.com/robotandkid/consulting/commit/010814f1af11b7eb4ea5671f34659883b48c5fdd))





## [0.22.11](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.10...@robotandkid/leander-tile@0.22.11) (2021-09-30)


### Bug Fixes

* **leandertile:** landing hero title ([5e87e3b](https://gitlab.com/robotandkid/consulting/commit/5e87e3b7730bfc7b159cb5372f053778190269c4))





## [0.22.10](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.9...@robotandkid/leander-tile@0.22.10) (2021-09-30)


### Bug Fixes

* **leandertile:** tOC now supports es language ([b495fd4](https://gitlab.com/robotandkid/consulting/commit/b495fd4639fcbb3fa0bf5c93d7c9774e3c228ce1))





## [0.22.9](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.8...@robotandkid/leander-tile@0.22.9) (2021-09-29)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.22.8](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.7...@robotandkid/leander-tile@0.22.8) (2021-09-29)


### Bug Fixes

* **leandertile:** added missing external dep ([ebb8b90](https://gitlab.com/robotandkid/consulting/commit/ebb8b90842274dcca655cbf00fce15a3d593e7fe))





## [0.22.7](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.6...@robotandkid/leander-tile@0.22.7) (2021-09-29)


### Bug Fixes

* **leandertile:** config.externals should be an object as per official docs ([93bbf77](https://gitlab.com/robotandkid/consulting/commit/93bbf77ef420ad8e6629b2f4d784d2ea5d3fa4a8))





## [0.22.6](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.5...@robotandkid/leander-tile@0.22.6) (2021-09-29)


### Bug Fixes

* **leandertile:** next.config now says that "critters" is an external commonjs dep ([94c319d](https://gitlab.com/robotandkid/consulting/commit/94c319d6a1680fccac68b3034271d91628f23ab7))





## [0.22.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.4...@robotandkid/leander-tile@0.22.5) (2021-09-29)


### Bug Fixes

* **third time's the charm:** found missing dependency ([040d7b7](https://gitlab.com/robotandkid/consulting/commit/040d7b72d3a03b6b6cd495c7eda2291ad8129f34))





## [0.22.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.3...@robotandkid/leander-tile@0.22.4) (2021-09-29)


### Bug Fixes

* **leandertile:** possible workaround ([c79475a](https://gitlab.com/robotandkid/consulting/commit/c79475a9db4d8380a07d8c15cecfc1b9275c3063))





## [0.22.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.2...@robotandkid/leander-tile@0.22.3) (2021-09-29)


### Bug Fixes

* netlify workaround to broken build ([4d1725a](https://gitlab.com/robotandkid/consulting/commit/4d1725a9fe91bff9be7fa54f9212f644407eeaca))





## [0.22.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.1...@robotandkid/leander-tile@0.22.2) (2021-09-29)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.22.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.22.0...@robotandkid/leander-tile@0.22.1) (2021-09-27)


### Bug Fixes

* **leandertile:** typo in spanish tile title ([acc1254](https://gitlab.com/robotandkid/consulting/commit/acc12544df60a4ed9db87091f30550885533c846))





# [0.22.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.21.0...@robotandkid/leander-tile@0.22.0) (2021-09-24)


### Bug Fixes

* **leandertile:** fixed i18n translations ([bb00b9b](https://gitlab.com/robotandkid/consulting/commit/bb00b9b79782438885c2fd3c7f39c846f7bc4f8a))


### Features

* **leandertile:** added gallery alt text translations ([cb66b00](https://gitlab.com/robotandkid/consulting/commit/cb66b000163088c55c5a0ec2e53ff105975fcab7))
* **leandertile:** moved contact labels to i18n ([335c8cb](https://gitlab.com/robotandkid/consulting/commit/335c8cb94e9b2c14a4b9646e7df5ebc782ed7218))





# [0.21.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.20.0...@robotandkid/leander-tile@0.21.0) (2021-09-24)


### Features

* **leandertile:** correct phone number ([b10a8e2](https://gitlab.com/robotandkid/consulting/commit/b10a8e2f695fa373986d4f40e2e6a2c5a6614eae))





# [0.20.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.19.0...@robotandkid/leander-tile@0.20.0) (2021-09-24)


### Features

* **leandertile:** correct phone number ([8beb488](https://gitlab.com/robotandkid/consulting/commit/8beb488b9dc0cfc19d3315ddc7ac2fa931f6cb3e))
* **leandertile:** correct phone number ([bf42ec8](https://gitlab.com/robotandkid/consulting/commit/bf42ec866f7dfdc7f032f14c02f354a0a4214b1f))





# [0.19.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.18.2...@robotandkid/leander-tile@0.19.0) (2021-09-24)


### Features

* **leandertile:** added latest copy ([cf7f75e](https://gitlab.com/robotandkid/consulting/commit/cf7f75e0d3329d7e15fa98861ba4ebb2578ecffd))





## [0.18.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.18.1...@robotandkid/leander-tile@0.18.2) (2021-09-23)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.18.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.18.0...@robotandkid/leander-tile@0.18.1) (2021-09-22)

**Note:** Version bump only for package @robotandkid/leander-tile





# [0.18.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.17.1...@robotandkid/leander-tile@0.18.0) (2021-09-02)


### Features

* **leandertile:** created i18n module ([e44ef65](https://gitlab.com/robotandkid/consulting/commit/e44ef6534bca6986c15696717295e731f2d8b9c8))
* **leandertile:** services is now fully translated ([496cd85](https://gitlab.com/robotandkid/consulting/commit/496cd852d016d0d2f8737711bc06cc22c38d1a41))





## [0.17.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.17.0...@robotandkid/leander-tile@0.17.1) (2021-09-02)


### Bug Fixes

* fixed double BODY tag bug ([4416d82](https://gitlab.com/robotandkid/consulting/commit/4416d82153252ae34c9369500253b5b6f15e53ba))





# [0.17.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.16.2...@robotandkid/leander-tile@0.17.0) (2021-09-02)


### Bug Fixes

* add lingui script adds script correctly ([6e69b9b](https://gitlab.com/robotandkid/consulting/commit/6e69b9bb61d18c249cc6844951581f97efc8935e))
* **leandertile:** compiled lingui messages ([00eca48](https://gitlab.com/robotandkid/consulting/commit/00eca48a9ab224aec7e6a37f6675deae8f27fdd7))


### Features

* **leandertile:** added i8n ([9f853a4](https://gitlab.com/robotandkid/consulting/commit/9f853a4a9967fa8dbed46b8fc4be0a552c9cec10))
* **leandertile:** added lingui ([0eb1a8d](https://gitlab.com/robotandkid/consulting/commit/0eb1a8da5e85896ca7b4b98eecb8ad4f44e95a9e))
* **leandertile:** added lingui ([2167c49](https://gitlab.com/robotandkid/consulting/commit/2167c499121fdf517b8b81016f1169be613e8153))
* **leandertile:** enabled i8n routing ([093c75d](https://gitlab.com/robotandkid/consulting/commit/093c75d37613ca9ed54eb1ea46e89ebf553cc717))





## [0.16.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.16.1...@robotandkid/leander-tile@0.16.2) (2021-08-25)


### Bug Fixes

* **leandertile:** compiles again ([0353b46](https://gitlab.com/robotandkid/consulting/commit/0353b466b5f32c689be4c29be2ec45a50265eed9))





## [0.16.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.16.0...@robotandkid/leander-tile@0.16.1) (2021-08-04)

**Note:** Version bump only for package @robotandkid/leander-tile





# [0.16.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.15.3...@robotandkid/leander-tile@0.16.0) (2021-07-29)


### Features

* **leandertile:** added LeanderNav ([6d8584d](https://gitlab.com/robotandkid/consulting/commit/6d8584de9253fcd89a7c940d35e68a1cf727a491))
* **leandertile:** added nav Call button ([67b15bc](https://gitlab.com/robotandkid/consulting/commit/67b15bc9e224873090473328a4a2d07c7c2bebc8))
* **leandertile:** can style call button ([ffaed21](https://gitlab.com/robotandkid/consulting/commit/ffaed21e686100ce6f6d563e0fd1b33864163e4f))





## [0.15.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.15.2...@robotandkid/leander-tile@0.15.3) (2021-07-29)


### Bug Fixes

* **components:** compiles again ([a141830](https://gitlab.com/robotandkid/consulting/commit/a1418308b7ac51c7a6dde9c3c8075d6bdd981f95))





## [0.15.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.15.1...@robotandkid/leander-tile@0.15.2) (2021-07-29)


### Bug Fixes

* **leandertile:** added landing hero placeholder ([e7ae6a4](https://gitlab.com/robotandkid/consulting/commit/e7ae6a4e48c3652a87597062d40c2d793aaaa5b6))





## [0.15.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.15.0...@robotandkid/leander-tile@0.15.1) (2021-07-29)


### Bug Fixes

* **components:** facebookSEO adds itself to the head ([c34e697](https://gitlab.com/robotandkid/consulting/commit/c34e69770dd92d5e33be26557a0e4cde07e5499e))





# [0.15.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.14.0...@robotandkid/leander-tile@0.15.0) (2021-07-28)


### Features

* **leandertile:** added facebook SEO ([a9e92d8](https://gitlab.com/robotandkid/consulting/commit/a9e92d89ecf450c7cc5a19421051c75cc3584d77))





# [0.14.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.13.2...@robotandkid/leander-tile@0.14.0) (2021-07-27)


### Features

* added robots.txt ([06493ee](https://gitlab.com/robotandkid/consulting/commit/06493eebb8ca980a6e588b8d88e632422167540e))





## [0.13.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.13.1...@robotandkid/leander-tile@0.13.2) (2021-07-27)

**Note:** Version bump only for package @robotandkid/leander-tile





## [0.13.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.13.0...@robotandkid/leander-tile@0.13.1) (2021-07-26)


### Bug Fixes

* **leandertile:** thanks page should not show menu ([d4c618c](https://gitlab.com/robotandkid/consulting/commit/d4c618cf0faba92759d880cc95f5c316d51fe397))





# [0.13.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.12.0...@robotandkid/leander-tile@0.13.0) (2021-07-26)


### Bug Fixes

* **leandertile:** fixed SEO ([cdb6437](https://gitlab.com/robotandkid/consulting/commit/cdb6437c3f63ee0fb4ad41ea5208b809443e34f2))
* **leandertile:** style tweaks ([10b3048](https://gitlab.com/robotandkid/consulting/commit/10b304881e7e9916484cb546f679087051c112b8))


### Features

* **leandertile:** added favicon ([d2f740f](https://gitlab.com/robotandkid/consulting/commit/d2f740fc3a4df97331a306686140dab5d8f0e343))





# [0.12.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.11.1...@robotandkid/leander-tile@0.12.0) (2021-07-23)


### Features

* **leandertile:** added opening hours ([259d00f](https://gitlab.com/robotandkid/consulting/commit/259d00fe2f80d67617a613564e77f53b8c1660cb))
* **leandertile:** updated working hours ([76ad463](https://gitlab.com/robotandkid/consulting/commit/76ad4639c8912060e0f7635e4989d0a3940fcc9b))





## [0.11.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.11.0...@robotandkid/leander-tile@0.11.1) (2021-07-22)

**Note:** Version bump only for package @robotandkid/leander-tile





# [0.11.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.10.0...@robotandkid/leander-tile@0.11.0) (2021-07-14)


### Bug Fixes

* **leandertile:** added missing image property to structured data (SEO) ([5822759](https://gitlab.com/robotandkid/consulting/commit/5822759ec3a56f3204cf04253f0331cbb0b343f3))


### Features

* **components:** added TitleSEO ([3f6a86c](https://gitlab.com/robotandkid/consulting/commit/3f6a86c58e8f96dcfe46a01fdd63a6b9b9973d2a))





# [0.10.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.9.0...@robotandkid/leander-tile@0.10.0) (2021-07-13)


### Bug Fixes

* **leandertile:** better alt text for gallery ([b963d9f](https://gitlab.com/robotandkid/consulting/commit/b963d9f30e42767b5427746ac06b6e09786a4875))
* **urielavalos:** fixed SEO issues ([f46a512](https://gitlab.com/robotandkid/consulting/commit/f46a512f341d0cd943581c62e48258d93b18ff99))


### Features

* **leandertile:** added services to SEO ([4a844c0](https://gitlab.com/robotandkid/consulting/commit/4a844c0a517f29dee918ea1e77d72dd8cbde4009))
* **leandertile:** added structured data ([f6c9108](https://gitlab.com/robotandkid/consulting/commit/f6c9108fdd1d61eda6b3bc46a2758216b83a3455))
* added semantic HTML ([0a0203c](https://gitlab.com/robotandkid/consulting/commit/0a0203c9909b5f9a4cd2b546556240855c9fd6d2))
* added sitemaps ([1d1b58a](https://gitlab.com/robotandkid/consulting/commit/1d1b58a8d7392c303c992beeca84104e52a6f2e1))
* **leandertile:** added LeanderSocial ([4a40b9a](https://gitlab.com/robotandkid/consulting/commit/4a40b9a5f8f707b22a3441b54ab2765363305ec7))





# [0.9.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.8.0...@robotandkid/leander-tile@0.9.0) (2021-07-01)


### Bug Fixes

* **leandertile:** form should submit ([1aa0a17](https://gitlab.com/robotandkid/consulting/commit/1aa0a1789fd682f74af35602bff8f4caf6a08136))
* **leandertile:** form should submit now ([96b7e5c](https://gitlab.com/robotandkid/consulting/commit/96b7e5c967cfae610f63d0fe334c785937a1ba95))
* **leandertile:** removed disabled attrib on form submission ([0a2fcaf](https://gitlab.com/robotandkid/consulting/commit/0a2fcaf037726c93545e8790b3e0face0bc54475))
* **leandertile:** using input for submit, renamed form labels ([fb8f4f5](https://gitlab.com/robotandkid/consulting/commit/fb8f4f5205fb5e651356910b2c8f3480a26a532c))
* **leandertile:** when closing hamburgermenu it restores position ([d7cf04e](https://gitlab.com/robotandkid/consulting/commit/d7cf04ec77e4259a588d7a66fd99f7edc80739c7))
* **urielavalos:** contact form opens w/o a hitch ([2bc488a](https://gitlab.com/robotandkid/consulting/commit/2bc488aee9c206aa8b9459f48c8c211043717e5e))


### Features

* **components:** added normalizedAnchor and ExternalLink ([56c98ce](https://gitlab.com/robotandkid/consulting/commit/56c98ce7a576ee25baf8e659b75c1821f6db876a))
* **leandertile:** added thank you page for form ([81253c9](https://gitlab.com/robotandkid/consulting/commit/81253c9cc8967c49c23083d50b325d813eb30ce3))
* **leandertile:** using latest hamburger menu ([cbf6822](https://gitlab.com/robotandkid/consulting/commit/cbf6822d2b487cba7c8214ccb8307e28f10aaae3))





# [0.8.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.7.0...@robotandkid/leander-tile@0.8.0) (2021-06-23)


### Bug Fixes

* **leandertile:** about has correct height ([effa5ce](https://gitlab.com/robotandkid/consulting/commit/effa5ce56d7f0506a20cc544ffa735fe2359c91e))
* **leandertile:** made H2 tabbable ([fea62e7](https://gitlab.com/robotandkid/consulting/commit/fea62e7276e0d36ac43185e39dab789fc8b936ad))


### Features

* **leandertile:** 1st pass Contact form ([318808b](https://gitlab.com/robotandkid/consulting/commit/318808b19bf3f2f81ed28f9592d256185322cd2e))
* **leandertile:** a11y: made header tabbale ([772f209](https://gitlab.com/robotandkid/consulting/commit/772f2090b39c82276ed1c7f1d99a65eb3321f553))





# [0.7.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.6.1...@robotandkid/leander-tile@0.7.0) (2021-06-19)


### Features

* **leandertile:** added nav menu ([cba10fa](https://gitlab.com/robotandkid/consulting/commit/cba10faab2e7870b1796b0b35c422e19443e6521))





## [0.6.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.6.0...@robotandkid/leander-tile@0.6.1) (2021-06-19)


### Bug Fixes

* **leandertile:** about section tweak ([00b2732](https://gitlab.com/robotandkid/consulting/commit/00b273229324f92694385a51181cfdbc28f5fcc0))
* **leandertile:** about text is correct size ([8e42bf9](https://gitlab.com/robotandkid/consulting/commit/8e42bf943ce3b769d52c5a4b6001601bfc04931f))
* **leandertile:** service has the right width so text centered in column ([32f307a](https://gitlab.com/robotandkid/consulting/commit/32f307a9d8d794b22c81e2fa96e97f95a78b25e0))





# [0.6.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.5.0...@robotandkid/leander-tile@0.6.0) (2021-06-18)


### Bug Fixes

* **leandertile:** gallery inserted in menu in correct place ([7ed1589](https://gitlab.com/robotandkid/consulting/commit/7ed1589756f25ce3eb0a9c5ce9a720d42a671c3b))
* **leandertile:** h2 header can be styled ([dc59595](https://gitlab.com/robotandkid/consulting/commit/dc5959569963cbceb15211713c1bd972b0934eb8))
* **leandertile:** skipping bad images ([58c30c6](https://gitlab.com/robotandkid/consulting/commit/58c30c618a6141cbb6cb6899698a4949b5bcfa6b))


### Features

* **leandertile:** added About ([67526e2](https://gitlab.com/robotandkid/consulting/commit/67526e22b5a4d35a6b8ba14f4b85c42b72e7c719))





# [0.5.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.4.0...@robotandkid/leander-tile@0.5.0) (2021-06-17)


### Bug Fixes

* **leandertile:** styling pager correctly ([65a03f8](https://gitlab.com/robotandkid/consulting/commit/65a03f81daa53d2f8f95f3f317e4c67055bf1f8a))


### Features

* **leandertile:** added gallery header, styled services correctly ([af39b1f](https://gitlab.com/robotandkid/consulting/commit/af39b1f02e13d92480802fc42954ab454b676efb))
* **leandertile:** render grid correctly ([cd4ef0d](https://gitlab.com/robotandkid/consulting/commit/cd4ef0de4e0221be81c403a69b2aad17b1b412aa))





# [0.4.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.3.0...@robotandkid/leander-tile@0.4.0) (2021-06-16)


### Bug Fixes

* **leandertile:** added proper styled-components support ([23cc00c](https://gitlab.com/robotandkid/consulting/commit/23cc00c04e24f42e42caaa9f2cc0ffb2a2381a19))
* **leandertile:** compiles again ([498584b](https://gitlab.com/robotandkid/consulting/commit/498584b8fb6343cb8a4fb324e5586fb52922ff6e))


### Features

* **leandertile:** added grid ([77ae931](https://gitlab.com/robotandkid/consulting/commit/77ae931e1e7258e9aa729db849e8c972594bdc79))
* **leandertile:** updated theme ([b5d7d76](https://gitlab.com/robotandkid/consulting/commit/b5d7d76542fdd5da4e59a422653b16efe64e5dd5))





# [0.3.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.2.0...@robotandkid/leander-tile@0.3.0) (2021-06-15)


### Features

* **leandertile:** integration with ImageKit works ([aabb5ce](https://gitlab.com/robotandkid/consulting/commit/aabb5ce46f66108b0244cdc119097662692b9ab5))





# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/leander-tile@0.1.0...@robotandkid/leander-tile@0.2.0) (2021-06-15)


### Features

* **leandertile:** added ImageKit support ([e89a1ad](https://gitlab.com/robotandkid/consulting/commit/e89a1adf2c0345f4b8bfca5ceda40d1e1bfa9e9e))





# 0.1.0 (2021-06-03)


### Bug Fixes

* **leander-tile:** hero uses LinkTarget correctly ([f4f990f](https://gitlab.com/robotandkid/consulting/commit/f4f990f42b2fcac2015f5bdf2ea0ba8cd5cc9875))


### Features

* **leander-tile:** 1st pass ([609eb2c](https://gitlab.com/robotandkid/consulting/commit/609eb2c30b97827f7188b9a9ba20874405318672))
* **leandertile:** created ([b8f508b](https://gitlab.com/robotandkid/consulting/commit/b8f508bb98f75c5465a3d02d294a7a1f0b75f4a7))
