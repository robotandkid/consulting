const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

/* @type { import("next").NextConfig } */
const config = {
  async headers() {
    return [
      {
        source: "/fonts/Righteous-Regular.ttf",
        headers: [
          {
            key: "Cache-Control",
            value: "public, immutable, max-age=31536000",
          },
        ],
      },
    ];
  },
  pageExtensions: ["page.tsx"],
  webpack(config) {
    // netlify bug:
    // https://answers.netlify.com/t/next-js-internal-error-during-functions-bundling-on-build/43647/7
    Object.assign(
      config.externals,
      ["critters"].reduce(
        (externals, name) => ({ ...externals, [name]: `commonjs ${name}` }),
        {}
      )
    );

    return config;
  },
};

module.exports = withBundleAnalyzer(config);
