# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/masonrypro@0.1.0...@robotandkid/masonrypro@0.1.1) (2023-09-04)


### Bug Fixes

* enabled stricer array access in TS ([8b89be7](https://gitlab.com/robotandkid/consulting/commit/8b89be793edef75156025f9b3ee4a7944a308dc0))





# 0.1.0 (2023-08-07)


### Bug Fixes

* **cli:** finished adding linaria task ([9ec704c](https://gitlab.com/robotandkid/consulting/commit/9ec704ce700f4a2dca5b868c1ac27ad940a9fc84))
* **masmasonrypro:** menu items scroll to section ([d9a0e67](https://gitlab.com/robotandkid/consulting/commit/d9a0e6700e120a1f9de858d145ce296ebd4e7d26))
* **masonrypro:** added SEO for google/fb ([f345d97](https://gitlab.com/robotandkid/consulting/commit/f345d97673a832795004a8d0e6685cd20bb26081))
* **masonrypro:** fixed button ([71cc53f](https://gitlab.com/robotandkid/consulting/commit/71cc53ff1d71ed9955984883bd9da8c9ed70cd1f))
* **masonrypro:** fixed call button ([94dfeed](https://gitlab.com/robotandkid/consulting/commit/94dfeed4a08a65d722efa83923aef27a2befbb5d))
* **masonrypro:** fixed gradient h1 ([d92b94a](https://gitlab.com/robotandkid/consulting/commit/d92b94a389d74386439b9911c4fcbbcca841678c))
* **masonrypro:** fixed orientation on images :( ([e4c183e](https://gitlab.com/robotandkid/consulting/commit/e4c183eb5747b45e8d6dae6ed90aa76ae8baf33a))
* **masonrypro:** sEO URLs pointed to wrong domain ([a5e2a5c](https://gitlab.com/robotandkid/consulting/commit/a5e2a5c7d7bb547d87e34563d3bc435e33757291))
* **masonrypro:** sitemap had wrong URL ([8386c30](https://gitlab.com/robotandkid/consulting/commit/8386c305e46a61bf53af78e307c335b0bfb2375e))


### Features

* **components:** debouncing listener added ([e92d91e](https://gitlab.com/robotandkid/consulting/commit/e92d91e52b71f4c2d32dfa53bf8d6c5b91acb812))
* **digital-apps:** eli garcia published ([331476e](https://gitlab.com/robotandkid/consulting/commit/331476ecc8574bc6371c17268e674f68a6919c1e))
* google search ([d3556cd](https://gitlab.com/robotandkid/consulting/commit/d3556cdea076f38c02d71c9954e0874b035c9aed))
* **mansonrypro:** added ([cdf8c8d](https://gitlab.com/robotandkid/consulting/commit/cdf8c8d259006c3786bc64ebcb296eb6c117960d))
* **masonrpypro:** added call button ([45498f7](https://gitlab.com/robotandkid/consulting/commit/45498f79ce296738881f96f06f5e77b47a9860c7))
* **masonry:** added gallery ([c30f74f](https://gitlab.com/robotandkid/consulting/commit/c30f74fb0fb09d061cc57c03f48f3e4be7a908f4))
* **masonrypro:** added a nifty cursor ([92930c0](https://gitlab.com/robotandkid/consulting/commit/92930c0d7dd1fed481a8b02baad95bc2de98dcd5))
* **masonrypro:** added about ([8ef4b29](https://gitlab.com/robotandkid/consulting/commit/8ef4b29b8a84a0f0d0df4c552d9474eccbb5e8b1))
* **masonrypro:** added component LandingHero to title ([50646ea](https://gitlab.com/robotandkid/consulting/commit/50646ea3ea2afb0f2d91aa8d9f80b64e7a6a9c93))
* **masonrypro:** added contact form ([37eea90](https://gitlab.com/robotandkid/consulting/commit/37eea907201482426798b3cf7cca1e9173de96c9))
* **masonrypro:** added cursor ([ebd6cdd](https://gitlab.com/robotandkid/consulting/commit/ebd6cdd85721e9ca757bf478584ff3f01f14db92))
* **masonrypro:** added favicons ([147bbfd](https://gitlab.com/robotandkid/consulting/commit/147bbfd34eaa09622a1d8da52e24ec48186ebca5))
* **masonrypro:** added get-quote buttons ([d6282d7](https://gitlab.com/robotandkid/consulting/commit/d6282d7a51119530c3dde68885e7ed4503b94133))
* **masonrypro:** added google analytics ([71963a2](https://gitlab.com/robotandkid/consulting/commit/71963a2fe8522d705f8e81ec4f38504ea3d16a3c))
* **masonrypro:** added hamburger icon color ([365289b](https://gitlab.com/robotandkid/consulting/commit/365289bbb9b0c00f2214afea4194da8821f7644a))
* **masonrypro:** added linaria ([8ed4fd5](https://gitlab.com/robotandkid/consulting/commit/8ed4fd5cb6fc2d7ec7968aa2902e1f912de80ad4))
* **masonrypro:** added linaria ([320d8cd](https://gitlab.com/robotandkid/consulting/commit/320d8cd6ae69013f7c40cdf1ce4daed0eaf3c6a2))
* **masonrypro:** added navigation for mobile ([45e9627](https://gitlab.com/robotandkid/consulting/commit/45e96270148b6a250c5a29ed88e3a58f9554bd40))
* **masonrypro:** added progress bar ([b79ed1b](https://gitlab.com/robotandkid/consulting/commit/b79ed1ba59ca57ad7cc3fa210b5d5d044b86e6d4))
* **masonrypro:** added progress bar ([c9f65b6](https://gitlab.com/robotandkid/consulting/commit/c9f65b6fd53d64f2b1299efac774e2fa95deb337))
* **masonrypro:** added service header ([927f95f](https://gitlab.com/robotandkid/consulting/commit/927f95fdf54db9a9a8d4a79b82b0081076205e70))
* **masonrypro:** added services ([b712e60](https://gitlab.com/robotandkid/consulting/commit/b712e60193641ad24fe29066333c34489f8ddd0c))
* **masonrypro:** added the gallery ([c04a941](https://gitlab.com/robotandkid/consulting/commit/c04a941ecc2f4901904de7e03921d7859c978047))
* **masonrypro:** added theme ([ed05fd8](https://gitlab.com/robotandkid/consulting/commit/ed05fd895a90741b8b1a0a9a04e5b7ff7a684407))
* **masonrypro:** adjusted desktop version ([f5b7ecd](https://gitlab.com/robotandkid/consulting/commit/f5b7ecd6d5d8512c4a70dbb79462542584425554))
* **masonrypro:** almost there ([e1fb9fa](https://gitlab.com/robotandkid/consulting/commit/e1fb9fa4530eee68eee7da8f678d02d0a5db74b7))
* **masonrypro:** getting there ([fbca453](https://gitlab.com/robotandkid/consulting/commit/fbca4533475f82819e50749e4341b09592553445))
* **masonrypro:** landing hero mostly works ([fc25692](https://gitlab.com/robotandkid/consulting/commit/fc25692ee92aa886c5ce0c86ae5774411cbb5f17))
* **masonrypro:** nav get quote button ([da5ab62](https://gitlab.com/robotandkid/consulting/commit/da5ab626709678960b6449ce32d9f52efa872024))
* **root:** added netlify nextjs plugin ([7d348bb](https://gitlab.com/robotandkid/consulting/commit/7d348bb3db7c27ec55e3aa33a94411ad49d8a8b0))
