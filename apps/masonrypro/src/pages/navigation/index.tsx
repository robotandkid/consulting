import { MasonryPropNavigationMobile } from "./MasonryProNavigation.mobile";

export const MasonryProNavigation = () => {
  return (
    <nav>
      <MasonryPropNavigationMobile />
    </nav>
  );
};
