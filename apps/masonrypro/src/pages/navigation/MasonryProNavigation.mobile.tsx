import {
  HamburgerMenu,
  HamburgerMenuItem,
  InternalLink,
  useScrollingY,
  useTOC,
  zIndexNavigation,
} from "@robotandkid/components";
import { m, useMotionValue, useTransform } from "framer-motion";
import { List, XCircle } from "phosphor-react";
import styled, { css } from "styled-components";
import { theme } from "../../styles/theme";

const HamburgerContainer = styled(m.div)`
  position: relative;
  z-index: ${zIndexNavigation};
`;

const MenuButtonIcon = styled(List)`
  width: 2.5rem;
  height: 2.5rem;
  outline: none;
  border: none;
`;

const XButtonIcon = styled(XCircle)`
  width: 2.5rem;
  height: 2.5rem;
  outline: none;
  border: none;
`;

const navigationItem = css`
  ${(p) => p.theme.navigation.item()}
  margin-left: 2rem;
  margin-top: 2rem;
  position: relative;
`;

const StyledSideMenuItem = styled(HamburgerMenuItem)`
  ${navigationItem}
`;

/**
 * All of this is just to change the color of the nav menu icon.
 *
 * Doh!
 *
 * Note: it adds 4kb to the bundle size.
 */
export const useNavIconColor = () => {
  // this is now a number between [0, 1]---1 is when it's past the viewport
  const scrollY = Math.min(1, useScrollingY() / (globalThis.innerHeight ?? 1));
  const mScrollY = useMotionValue(scrollY);
  mScrollY.set(scrollY);

  const bgColor = useTransform(
    mScrollY,
    [0, 1],
    ["rgba(255,255,255,1)", "rgba(0,0,0,1)"]
  );

  return [bgColor.get(), bgColor] as const;
};

function SideMenuItem(props: { href: `#${string}`; name: string }) {
  const { href, name } = props;

  return (
    <InternalLink href={href}>
      <StyledSideMenuItem>{name}</StyledSideMenuItem>
    </InternalLink>
  );
}

export function MasonryPropNavigationMobile() {
  const TOC = useTOC();

  const items = TOC.map((item) => {
    return <SideMenuItem key={item.name} href={item.href} name={item.name} />;
  });

  const [color] = useNavIconColor();

  return (
    <HamburgerContainer layoutId="menu">
      <HamburgerMenu bgColor={theme.navigation.bg}>
        <MenuButtonIcon weight="bold" color={color} />
        <XButtonIcon weight="bold" />
        {items}
      </HamburgerMenu>
    </HamburgerContainer>
  );
}
