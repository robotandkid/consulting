import { LinkTarget, setTOCItem } from "@robotandkid/components";
import styled from "styled-components";
import { pageMargins } from "../../styles/styles";
import { theme } from "../../styles/theme";

const Header = styled.h2`
  ${theme.shared.headers.h2()}
  ${theme.about.headerPrimary()}
  ${pageMargins}
  margin-bottom: 1rem;
`;

const Text = styled.div`
  ${theme.about.text}
  ${pageMargins}
  margin-bottom: 2rem;
`;

const tocItem = { href: "#about", name: "services", index: 3 } as const;
setTOCItem(tocItem);

export const MasonryProAbout = () => {
  return (
    <section>
      <LinkTarget href={tocItem.href}>
        <Header>About</Header>
      </LinkTarget>
      <Text>
        We have over 20 years experience. We are experts in all aspects of
        masonry, including bricklaying, stone work, and concrete work. We pride
        ourselves on our attention to detail and our commitment to customer
        satisfaction. Contact us today for a free estimate on your next project.
      </Text>
    </section>
  );
};
