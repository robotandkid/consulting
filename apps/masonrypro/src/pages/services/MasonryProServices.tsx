import { exactType, LinkTarget, setTOCItem } from "@robotandkid/components";
import styled from "styled-components";
import { pageMargins, pagePaddings } from "../../styles/styles";
import { theme } from "../../styles/theme";

const Header = styled.h2`
  ${theme.shared.headers.h2()}
  ${theme.services.headerPrimary()}
  ${pageMargins}
`;

const Container = styled.ul`
  list-style: none;
  width: 100%;
  padding: 0;
`;

const ServiceItem = styled.li`
  ${pagePaddings}
  ${theme.services.item()}
`;

// TODO integrate w/ translations
// TODO autogenerate this gradient using color on the backend
const services = [
  "All phases of masonry",
  "Brick",
  "Concrete",
  "Driveways",
  "Patios",
  "Stone",
  "Fencing",
  "Sidewalks",
  "Flower beds",
] as const;

const serviceColors = [
  ["#6dc1e5", "#000"],
  ["#00a1e4", "#000"],
  ["#008cec", "#000"],
  ["#027bee", "#fff"],
  ["#006aee", "#fff"],
  ["#194aeb", "#fff"],
  ["#2e38e8", "#fff"],
  ["#0c21e9", "#fff"],
  ["#4507d6", "#fff"],
] as const;

// make sure there are always enough colors
exactType(
  Object.keys(services) as unknown as keyof typeof services,
  Object.keys(serviceColors) as unknown as keyof typeof serviceColors
);

const servicesWithColors = services.map((service, index) => [
  service,
  ...(serviceColors[index] ?? []),
]);

const tocItem = { href: "#services", name: "services", index: 1 } as const;
setTOCItem(tocItem);

export const MasonryProServices = () => {
  return (
    <section>
      <LinkTarget href={tocItem.href}>
        <Header>Services</Header>
      </LinkTarget>
      <Container>
        {servicesWithColors.map(([service, bgColor, fgColor], index) => (
          <ServiceItem
            key={index}
            style={{ color: fgColor, background: bgColor }}
          >
            {service}
          </ServiceItem>
        ))}
      </Container>
    </section>
  );
};
