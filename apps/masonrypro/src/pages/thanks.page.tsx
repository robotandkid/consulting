import styled from "styled-components";
import {
  bottomPageMargin,
  pageMargins,
  PageTransition,
} from "../styles/styles";
import { theme } from "../styles/theme";

const Header = styled.h2`
  ${theme.shared.headers.h2()}
  ${theme.contact.headerPrimary()}
    ${pageMargins}
    padding-top: 2rem;
`;

const Section = styled.section`
  ${theme.contact.container()}
  ${bottomPageMargin}
    display: flex;
  flex-direction: column;
`;

export default function ThanksPage() {
  return (
    <>
      <Section>
        <Header>Thanks</Header>
      </Section>
      <PageTransition
        initial={{ scaleX: 1 }}
        animate={{ scaleX: 0, transition: { duration: 0.5, ease: "circOut" } }}
        exit={{ scaleX: 1, transition: { duration: 0.5, ease: "circIn" } }}
        style={{ originX: 0 }}
      />
    </>
  );
}
