import {
  FacebookSEO,
  StructuredDataSEO,
  TitleSEO,
} from "@robotandkid/components";
import React from "react";

const structuredData = {
  "@id": "https://getmasonry.pro",
  address: {
    "@type": "PostalAddress",
    addressLocality: "Leander",
    addressRegion: "TX",
    addressCountry: "US",
  },
  name: "Masonry Pro",
  image: [
    "https://ik.imagekit.io/bxpwztngs0o/masonrypro/seo-1x1.jpg",
    "https://ik.imagekit.io/bxpwztngs0o/masonrypro/seo-4x3.jpg",
    "https://ik.imagekit.io/bxpwztngs0o/masonrypro/seo-16x9.jpg",
  ],
  "@context": "https://schema.org",
  "@type": "HomeAndConstructionBusiness",
  currenciesAccepted: "USD",
  paymentAccepted: "Cash, Credit Card",
  areaServed: {
    "@type": "AdministrativeArea",
    name: "Williamson County, TX",
    geoCrosses: "Travis County, TX",
  },
  telephone: "+15164514769",
  url: "https://getmasonry.pro",
  hasOfferCatalog: {
    "@type": "OfferCatalog",
    name: "Professional Exterior Designs",
    itemListElement: [
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Brick",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Concrete",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Driveways",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Patios",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Stone",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Fencing",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Sidewalks",
        },
      },
      {
        "@type": "Offer",
        itemOffered: {
          "@type": "Service",
          name: "Flower beds",
        },
      },
    ],
  },
  openingHoursSpecification: [
    {
      "@type": "OpeningHoursSpecification",
      dayOfWeek: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
      opens: "09:00",
      closes: "17:00",
    },
  ],
};

const title = `Masonry Pro - Brick, Concrete, Driveways, Patios, Stone, Fencing, Sidewalks, Flower beds`;

const description = `We have over 20 years experience and are experts in all aspects of masonry, including bricklaying, stone work, and concrete work. We pride ourselves on our attention to detail and our commitment to customer satisfaction.`;

export function MasonryProSeo() {
  return (
    <>
      <TitleSEO>
        {title}
        {description}
      </TitleSEO>
      <StructuredDataSEO structuredData={structuredData} />;
      <FacebookSEO
        url="https://getmasonry.pro"
        type="website"
        title={title}
        description={description}
        imageUrl="https://ik.imagekit.io/bxpwztngs0o/masonrypro/gallery-11.jpg?ik-sdk-version=javascript-1.4.3&updatedAt=1672186450466&tr=w-1200%2Ch-630%2Cfo-auto"
        imageWidthPx={1200}
        imageHeightPx={630}
      />
    </>
  );
}
