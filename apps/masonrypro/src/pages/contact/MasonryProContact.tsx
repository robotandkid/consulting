import {
  buttonFilledHover,
  LinkTarget,
  normalizedAnchor,
  setTOCItem,
} from "@robotandkid/components";
import { m } from "framer-motion";
import { useRouter } from "next/router";
import { PhoneCall } from "phosphor-react";
import { FormEvent, useRef } from "react";
import styled from "styled-components";
import { bottomPageMargin, pageMargins, text } from "../../styles/styles";
import { theme } from "../../styles/theme";

const Header = styled.h2`
  ${theme.shared.headers.h2()}
  ${theme.contact.headerPrimary()}
  ${pageMargins}
  padding-top: 2rem;
`;

const ContactText = styled.div`
  ${theme.contact.text()}
  ${pageMargins}
`;

const Form = styled.form`
  ${text}
  ${pageMargins}
  ${theme.contact.form()}
  display: flex;
  flex-direction: column;
`;

const Section = styled.section`
  ${theme.contact.container()}
  ${bottomPageMargin}
  display: flex;
  flex-direction: column;
`;

export const masonryProContactToc = {
  href: "#contact",
  name: "Contact",
  index: 4,
} as const;

setTOCItem(masonryProContactToc);

const t = {
  contactNameLabel: "Name",
  contactEmailLabel: "Email",
  contactPhoneLabel: "Phone Number",
  contactMessageLabel: "Message",
  contactSubmitButton: "Submit",
  contactCallLabel: "Call (516)451-4769",
  contactDays: "Monday – Friday",
};

const CallFilledButton = styled(m.a)`
  ${normalizedAnchor}
  ${buttonFilledHover}
  ${pageMargins}
  ${theme.contact.button()}
`;

const CallButtonIcon = styled(PhoneCall)`
  width: 1.5em;
  height: 1.5em;
  outline: none;
  border: none;
  margin-bottom: -0.5em;
`;

export function CallButton(props: { className?: string; ariaLabel: string }) {
  const { className } = props;
  const ref = useRef<HTMLAnchorElement>(null);

  return (
    <CallFilledButton
      className={className}
      ref={ref}
      href="tel:+15164514769"
      tabIndex={0}
      onKeyPress={(e: any) => {
        if (e.key === " ") {
          ref.current?.click();
        }
      }}
      whileHover={{ scale: 1.05 }}
      whileTap={{ scale: 0.95 }}
      aria-label={props.ariaLabel}
    >
      <CallButtonIcon /> {t.contactCallLabel}
    </CallFilledButton>
  );
}

export function MasonryProContact() {
  const formRef = useRef<HTMLFormElement>(null);
  const router = useRouter();

  const handleSubmit = (e: FormEvent) => {
    if (!formRef.current) return;
    e.preventDefault();
    router.push("/thanks");
    fetch("/thanks", {
      method: "POST",
      body: new FormData(formRef.current),
    });
  };

  return (
    <Section>
      <LinkTarget href={masonryProContactToc.href}>
        <Header>Contact</Header>
      </LinkTarget>
      <CallButton ariaLabel="click to call" />
      <ContactText>{t.contactDays}</ContactText>
      <ContactText>9&ndash;5pm CST</ContactText>
      <Form
        ref={formRef}
        name="contact"
        data-netlify="true"
        method="POST"
        onSubmit={handleSubmit}
      >
        <input type="hidden" name="form-name" value="contact" />
        <label htmlFor="contact-name">{t.contactNameLabel}</label>
        <input
          type="text"
          name="name"
          autoComplete="name"
          required
          id="contact-name"
        />
        <label htmlFor="contact-email">{t.contactEmailLabel}</label>
        <input
          type="email"
          name="email"
          autoComplete="email"
          required
          id="contact-email"
        />
        <label htmlFor="contact-phone">{t.contactPhoneLabel}</label>
        <input
          type="tel"
          name="phone"
          autoComplete="tel"
          maxLength={10}
          minLength={10}
          required
          id="contact-phone"
        />
        <label htmlFor="contact-message">{t.contactMessageLabel}</label>
        <textarea
          name="message"
          autoComplete="off"
          maxLength={1000}
          minLength={10}
          required
          id="contact-message"
        />
        <m.input
          type="submit"
          value={t.contactSubmitButton}
          whileHover={{ scale: 1.05 }}
          whileTap={{ scale: 0.95 }}
        />
      </Form>
    </Section>
  );
}
