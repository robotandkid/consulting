import {
  globalWindow,
  InternalLink,
  LandingHeroContainer,
  LinkTarget,
  navigationLandingHeroTitle,
  setTOCItem,
  useScrollingY,
} from "@robotandkid/components";
import { AnimatePresence, m } from "framer-motion";
import styled, { css } from "styled-components";
import { pageMargins } from "../../styles/styles";
import { theme } from "../../styles/theme";
import { masonryProContactToc } from "../contact/MasonryProContact";

const Container = styled(LandingHeroContainer)`
  ${theme.landing.default()}
  width: 100%;
  align-items: flex-start;
`;

const sharedMargins = css`
  ${pageMargins}
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
`;

const Header = styled(m.div)`
  ${theme.landing.header()}
  ${sharedMargins}
`;

const Title = styled(m.div)`
  ${theme.landing.title()}
  ${sharedMargins}
`;

const SmallText = styled(m.div)`
  ${theme.landing.default()}
  ${sharedMargins}
  margin-top: 1rem;
  margin-bottom: 1rem;
`;

const GetQuoteButton = styled(m.button)`
  ${sharedMargins}
  ${theme.landing.default()}
  ${theme.landing.getQuoteButton()}
  margin-top: 1rem;
  margin-bottom: 1rem;
`;

const NavGetQuoteButton = styled(m.button)`
  ${navigationLandingHeroTitle}
  ${theme.landing.default()}
  ${theme.navigation.getQuoteButton()}
`;

const tocItem = { href: "#welcome", name: "Welcome", index: 0 } as const;
setTOCItem(tocItem);

const title = "The Masonry Pro";

export const MasonryProLandingHero = () => {
  const scrollY = useScrollingY();
  const landingHeight = globalWindow()?.innerHeight ?? Number.MAX_SAFE_INTEGER;
  const isLandingScrolled = scrollY >= landingHeight;
  // const { open } = useContext(HamburgerMenuContext);

  // // there's a gotcha here --- when the user opens the nav menu,
  // // we have to ignore changes to isLandingScrolled... because it will be incorrect
  // if (!open) {
  // }

  return (
    <Container>
      <Header>Improve Your Home With</Header>
      <Title>
        <LinkTarget href={tocItem.href} scrollToTop>
          <div>{title}</div>
        </LinkTarget>
      </Title>
      <SmallText>Serving leander, cedar park, austin</SmallText>
      <InternalLink href={masonryProContactToc.href}>
        <GetQuoteButton whileHover={{ scale: 1.05 }} whileTap={{ scale: 0.95 }}>
          Get Quote
        </GetQuoteButton>
      </InternalLink>
      <AnimatePresence>
        {isLandingScrolled && (
          <m.div>
            <InternalLink href={masonryProContactToc.href}>
              <NavGetQuoteButton
                initial={{ transform: "translate(0,-200%)" }}
                animate={{ transform: "translate(0,0%)" }}
                transition={{ ease: "easeInOut", duration: 1 }}
                exit={{ transform: "translate(0,-200%)" }}
              >
                Get Quote
              </NavGetQuoteButton>
            </InternalLink>
          </m.div>
        )}
      </AnimatePresence>
    </Container>
  );
};
