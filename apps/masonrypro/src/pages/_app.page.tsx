import {
  BodyUnderHamburger,
  CenteredColumn,
  HamburgerMenuContextProvider,
} from "@robotandkid/components";
import { AnimatePresence, LazyMotion } from "framer-motion";
import styled from "styled-components";
import { GlobalStyle } from "../styles/GlobalStyle";
import { AppThemeProvider } from "../styles/theme";
import { MasonryProCursor } from "../ui/cursor/MasonryProCursor";
import { MasonryProNavigation } from "./navigation/index";

const loadFramerFeatures = () =>
  import("framer-motion").then((module) => module.domMax);

const Column = styled(CenteredColumn)`
  width: 100vw;
  max-width: 2000px;
`;

export default function MyApp(props: { Component: any; pageProps: any }) {
  const { Component, pageProps } = props;
  return (
    <AppThemeProvider>
      <LazyMotion strict features={loadFramerFeatures}>
        <GlobalStyle />
        <AnimatePresence mode="wait" initial={false}>
          <HamburgerMenuContextProvider>
            <Column>
              <MasonryProNavigation />
              <BodyUnderHamburger>
                <Component {...pageProps} />
              </BodyUnderHamburger>
            </Column>
          </HamburgerMenuContextProvider>
        </AnimatePresence>
      </LazyMotion>
      <MasonryProCursor />
    </AppThemeProvider>
  );
}
