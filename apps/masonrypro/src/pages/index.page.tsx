import React from "react";
import Script from "next/script";
import { MasonryProFooter } from "../footer/MasonryProFooter";
import { MasonryProGallery } from "../gallery/MasonryProGallery";
import { PageTransition } from "../styles/styles";
import { MasonryProAbout } from "./about/MasonryProAbout";
import { MasonryProContact } from "./contact/MasonryProContact";
import { MasonryProLandingHero } from "./landing/MasonryProLandingHero";
import { MasonryProServices } from "./services/MasonryProServices";
import { MasonryProSeo } from "./SEO";

export default function MainPage() {
  return (
    <>
      <Script
        async
        src="https://www.googletagmanager.com/gtag/js?id=G-SSCF2XY757"
        onLoad={() => {
          (window as any).dataLayer = (window as any).dataLayer || [];
          function gtag() {
            // eslint-disable-next-line prefer-rest-params
            (window as any).dataLayer.push(arguments);
          }
          (gtag as any)("js", new Date());
          (gtag as any)("config", "G-SSCF2XY757");
        }}
      />
      <MasonryProSeo />
      <MasonryProLandingHero />
      <MasonryProServices />
      <MasonryProGallery />
      <MasonryProAbout />
      <MasonryProContact />
      <MasonryProFooter />
      <PageTransition
        initial={{ scaleX: 1 }}
        animate={{ scaleX: 0, transition: { duration: 0.5, ease: "circOut" } }}
        exit={{ scaleX: 1, transition: { duration: 0.5, ease: "circIn" } }}
        style={{ originX: 0 }}
      />
    </>
  );
}
