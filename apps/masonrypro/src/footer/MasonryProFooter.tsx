import { useDebouncedListener } from "@robotandkid/components";
import { m } from "framer-motion";
import { useRef } from "react";
import styled from "styled-components";
import { theme } from "../styles/theme";

const ProgressBar = styled(m.div)`
  ${theme.footer.progressBar()}
  position: fixed;
  bottom: 1rem;
  left: 0;
  right: 0;
  transform-origin: 0%;
  height: 5px;
`;

const ProgressBarContainer = () => {
  const progressDiv = useRef<HTMLDivElement>(null);

  useDebouncedListener("scroll", () => {
    const maxScroll =
      document.documentElement.offsetHeight - window.innerHeight;
    const progress = window.scrollY / maxScroll;

    if (progressDiv.current) {
      progressDiv.current.style.transform = `scaleX(${progress})`;
    }
  });

  return <ProgressBar ref={progressDiv} />;
};

export const MasonryProFooter = () => {
  return (
    <footer>
      <ProgressBarContainer />
    </footer>
  );
};
