import {
  isSSR,
  useDebouncedListener,
  zIndexNavigation,
} from "@robotandkid/components";
import { useRef } from "react";
import styled from "styled-components";

const Cursor = styled.div`
  ${(p) => p.theme.cursor()}
  z-index: ${zIndexNavigation};
  width: 4rem;
  height: 4rem;
  position: fixed;
  top: 0;
  left: 0;
  pointer-events: none;
  opacity: 0;
  transition: opacity 1s;
`;

/** Prior art: https://blog.logrocket.com/creating-custom-mouse-cursor-css/ */
const InternalMasonryProCursor = () => {
  const cursor = useRef<HTMLDivElement>(null);

  useDebouncedListener("mousemove", (mouseEvent) => {
    const mouseY = mouseEvent.clientY;
    const mouseX = mouseEvent.clientX;

    if (cursor.current) {
      cursor.current.style.opacity = "1";
      cursor.current.style.transform = `translate(${mouseX - 25}px, ${
        mouseY - 25
      }px)`;
    }
  });

  return <Cursor ref={cursor} />;
};

export const MasonryProCursor = () => {
  const isMobile = !isSSR() && "ontouchstart" in window.document;

  if (!isMobile) return <InternalMasonryProCursor />;

  return null;
};
