import { css } from "styled-components";

export const gradient = (startColor: string, endColor: string) => css`
  background: ${startColor};
  background: linear-gradient(to right, ${startColor}, ${endColor});
`;

export const textGradient = (startColor: string, endColor: string) => css`
  background: ${startColor};
  background: linear-gradient(to right, ${startColor}, ${endColor});
  background-clip: text;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

export const unsetTextGradient = css`
  background: transparent;
  background-clip: unset;
  -webkit-background-clip: unset;
  -webkit-text-fill-color: unset;
`;

export const rem = (x: number) => `${x.toFixed(1)}rem`;
