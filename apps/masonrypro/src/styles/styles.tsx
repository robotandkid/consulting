import {
  centeredColumn,
  media,
  zIndexNavigation,
} from "@robotandkid/components";
import { m } from "framer-motion";
import styled, { css } from "styled-components";
import { theme } from "./theme";

export const text = css`
  ${(p) => p.theme.body()}
  margin-top: 1rem;
  margin-bottom: 1rem;
  text-align: justify;
`;

export const pageMargins = css`
  margin-left: 1rem;
  margin-right: 1rem;

  ${media("tablet")(css`
    margin-left: 5rem;
    margin-right: 5rem;
  `)}
`;

/** Same as pageMargins but using paddings. */
export const pagePaddings = css`
  padding-left: 1rem;
  padding-right: 1rem;

  ${media("tablet")(css`
    padding-left: 5rem;
    padding-right: 5rem;
  `)}
`;

export const bottomPageMargin = css`
  margin-bottom: 1.5rem;
`;

export const Section = styled.div`
  ${pageMargins}
  ${centeredColumn}
`;

export const SectionInner = styled.div`
  width: 100%;
`;

export const PageTransition = styled(m.div)`
  ${theme.pageTransition()}
  z-index: ${zIndexNavigation + 3};
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;
