import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    ${(p) => p.theme.body()};
    font-size: 62.5%;
    margin: 0;
    padding: 0;
  }

  /**
   * workaround next/image bug: https://nextjs.org/docs/api-reference/next/image
   */
  @supports (font: -apple-system-body) and (-webkit-appearance: none) { 
    img[loading="lazy"] { 
      clip-path: inset(0.6px)
    }
  }
`;
