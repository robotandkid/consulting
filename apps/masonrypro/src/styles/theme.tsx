import {
  buildFontScaleMap,
  buttonFilledHover,
  fontScaleAugmentedFourth,
  media,
} from "@robotandkid/components";
import React from "react";
import { css, ThemeProvider } from "styled-components";
import { gradient, rem, textGradient } from "./helpers";

const colors = {
  blue1: "#0540F2",
  blue2: "#0554F2",
  darkBlue1: "#152659",
  lightBlue1: "#77A0F2",
  red1: "rgb(255, 54, 50)",
  red2: "rgb(253, 123, 121)",
  white1: "#F2F2F2",
  black1: "#000000",
  darkBlue2: "#4A02E0",
  darkBlue3: "#0E02EB",
  darkBlue4: "#0D38D4",
  blue3: "#0272EB",
  lightBlue2: "#02A7E0",
  lightBlue3: "#6dc1e5",
  lightBlue4: "#00a1e4",
  darkGray1: "#404040",
} as const;

const fonts = {
  sansSerif: "sans-serif",
  monospace: "monospace",
  ...buildFontScaleMap(fontScaleAugmentedFourth, "rem"),
} as const;

const concepts = {
  cursor: {
    color: colors.blue2,
  },
  body: {
    font: fonts.sansSerif,
    fontSize: fonts.fontSize1,
  },
  shared: {
    title: {
      font: fonts.sansSerif,
      color: colors.blue1,
      gradientColor: colors.lightBlue2,
    },
    headers: {
      h2: {
        fontFamily: fonts.sansSerif,
        fontSize: fonts.fontSize5,
      },
    },
  },
  navigation: {
    fontSize: fonts.fontSize5,
    menuBgColor: colors.lightBlue2,
    textColor: colors.black1,
    shadowColor: colors.black1,
    desktop: {
      navBarBgColor: colors.white1,
    },
    title: {
      fontSize: fonts.fontSize3,
    },
    button: {
      borderColor: colors.black1,
      bgColor: colors.white1,
      fontColor: colors.black1,
      fontFamily: fonts.monospace,
      fontSize: fonts.fontSize2,
    },
  },
  landing: {
    bgColor: colors.darkGray1,
    color: colors.white1,
    fontSize: fonts.fontSize3,
    header: {
      color: colors.white1,
      fontSize: fonts.fontSize4,
    },
    title: {
      fontSize: fonts.fontSize5,
    },
    shadowTitle: {
      fallbackColor: colors.white1,
      strokeColor: colors.white1,
    },
    button: {
      bgColor: colors.blue1,
      fontColor: colors.white1,
      fontFamily: fonts.sansSerif,
      hoverColor: colors.red1,
      activeColor: colors.darkBlue3,
    },
  },
  services: {
    bgColor: colors.white1,
    textColor1: colors.black1,
    textColor2: colors.lightBlue2,
    itemFontSize: fonts.fontSize3,
    fontFamily: fonts.monospace,
  },
  gallery: {
    textColor1: colors.black1,
    itemColor: colors.lightBlue1,
    pager: {
      textColor: colors.black1,
      disabledColor: colors.darkGray1,
      hoverColor: colors.darkBlue3,
      outlineColor: colors.darkBlue1,
    },
  },
  pageTransition: {
    bgColor: colors.lightBlue1,
  },
  contact: {
    headerColor: colors.lightBlue4,
    fontSize: fonts.fontSize3,
    fontColor: colors.white1,
    fontFamily: fonts.monospace,
    fontSizeRaw: fonts.fontSizeRaw3,
    inputBorderColor: colors.white1,
    submit: {
      buttonColor: colors.darkGray1,
      textColor: colors.white1,
      borderColor: colors.white1,
      hoverColor1: colors.lightBlue2,
      hoverColor2: colors.blue1,
      fontFamily: fonts.monospace,
    },
    bgColor: colors.darkGray1,
    button: {
      bgColor: colors.lightBlue3,
      fontColor: colors.black1,
      fontFamily: fonts.monospace,
      fontSize: fonts.fontSize2,
      fontSizeRaw: fonts.fontSizeRaw2,
      hoverColor: colors.white1,
      activeColor: colors.lightBlue1,
    },
  },
  about: {
    bgColor: colors.white1,
    textColor1: colors.black1,
    textColor2: colors.lightBlue2,
    textFontSize: fonts.fontSize3,
    fontFamily: fonts.monospace,
  },
} as const;

const entities = {
  cursor: () => css`
    border-radius: 50%;
    border: 3pt solid ${concepts.cursor.color};
  `,
  body: () => css`
    font-family: ${concepts.body.font};
    font-size: ${concepts.body.fontSize};
  `,
  shared: {
    headers: {
      h2: () => css`
        font-family: ${concepts.shared.headers.h2.fontFamily};
        font-weight: bold;
        font-size: ${concepts.shared.headers.h2.fontSize};
        letter-spacing: -0.1rem;
        text-transform: capitalize;
        margin: 0;
        margin-top: 2.5rem;
      `,
    },
  },
  navigation: {
    title: () => css`
      font-size: ${concepts.navigation.title.fontSize};
      text-transform: uppercase;
      line-height: 1;
      letter-spacing: -0.1rem;
      ${textGradient(
        concepts.shared.title.color,
        concepts.shared.title.gradientColor
      )}
    `,
    item: () => css`
      font-size: ${concepts.navigation.fontSize};
      font-weight: bold;
      text-transform: capitalize;
      color: ${concepts.navigation.textColor};
      &:visited {
        color: ${concepts.navigation.textColor} !important;
      }
    `,
    bg: concepts.navigation.menuBgColor,
    getQuoteButton: () => css`
      font-family: ${concepts.navigation.button.fontFamily};
      color: ${concepts.navigation.button.fontColor};
      background-color: ${concepts.navigation.button.bgColor};
      border-color: ${concepts.navigation.button.borderColor};

      border-width: 3px;
      border-style: solid;
      border-radius: 0.25rem;

      text-transform: capitalize;
      font-weight: normal;
      padding: 0.25rem;
      padding-left: 0.5rem;
      padding-right: 0.5rem;
      line-height: 1;

      transition: background 0.5s;

      &:hover {
        background: ${concepts.shared.title.gradientColor};
      }

      &:focus {
        background: ${concepts.shared.title.gradientColor};
      }

      &:active {
        background: ${concepts.shared.title.color};
      }
    `,
  },
  landing: {
    default: () => css`
      font-size: ${concepts.landing.fontSize};
      background: ${concepts.landing.bgColor};
      color: ${concepts.landing.color};
      text-transform: capitalize;
      font-weight: bold;
    `,
    header: () => css`
      font-size: ${concepts.landing.header.fontSize};
      color: ${concepts.landing.header.color};
      font-weight: normal;
      line-height: 1;
    `,
    title: () => css`
      font-size: ${concepts.landing.title.fontSize};
      line-height: 1;

      ${textGradient(
        concepts.shared.title.color,
        concepts.shared.title.gradientColor
      )};
    `,
    getQuoteButton: () => css`
      ${buttonFilledHover}
      color: ${concepts.landing.button.fontColor};
      background-color: ${concepts.landing.button.bgColor};
      border-color: ${concepts.landing.button.bgColor};

      border-width: 1px;
      border-style: solid;
      border-radius: 0.25rem;

      text-transform: uppercase;
      font-family: ${concepts.landing.button.fontFamily};
      font-weight: 400;
      padding: 1rem;
      line-height: 1;
    `,
  },
  services: {
    headerPrimary: () => css`
      color: ${concepts.services.textColor1};
    `,
    item: () => css`
      font-size: ${concepts.services.itemFontSize};
      font-family: ${concepts.services.fontFamily};
      line-height: 1;
      padding-top: 0.5rem;
      padding-bottom: 0.5rem;
    `,
  },
  gallery: {
    headerPrimary: () => css`
      color: ${concepts.services.textColor1};
    `,
    item: () => css``,
    pager: () => css`
      font-family: ${concepts.gallery.pager.textColor};
      border: 3px solid black;
      border-radius: 5px;
      background: inherit;
      width: 3rem;
      font-size: 2.5rem;

      ${media("laptop")(css`
        width: 3.5rem;
        font-size: 2rem;
      `)}

      ${media("laptop")(css`
        width: 4rem;
        font-size: 3.5rem;
      `)}

      user-select: none;

      transition: all 0.3s;

      &:disabled {
        border-color: ${concepts.gallery.pager.disabledColor};
        color: ${concepts.gallery.pager.disabledColor};
      }

      &:hover {
        border-color: ${concepts.gallery.pager.hoverColor};
        background: ${concepts.gallery.pager.hoverColor};
      }

      &:focus {
        outline-color: ${concepts.gallery.pager.outlineColor};
      }

      &:active {
        background: black;
        color: white;
        transform: scale(1.3);
      }
    `,
  },
  pageTransition: () => css`
    background-color: ${concepts.pageTransition.bgColor};
  `,
  contact: {
    headerPrimary: () => css`
      color: ${concepts.contact.headerColor};
      margin-top: 0;
    `,
    text: () => css`
      color: ${concepts.contact.fontColor};
      font-size: ${concepts.contact.fontSize};
    `,
    container: () => css`
      background-color: ${concepts.contact.bgColor};
      padding-top: 2rem;
    `,
    button: () => css`
      color: ${concepts.contact.button.fontColor};
      background-color: ${concepts.contact.button.bgColor};
      border-color: ${concepts.contact.button.bgColor};

      border-width: 1px;
      border-style: solid;
      border-radius: 0.25rem;

      text-transform: uppercase;
      font-family: ${concepts.contact.button.fontFamily};
      font-size: ${concepts.contact.button.fontSize};
      font-weight: 400;
      padding: ${concepts.contact.button.fontSizeRaw / 2}rem;
      margin-top: ${concepts.contact.button.fontSizeRaw * 0.75}rem;
      margin-bottom: ${concepts.contact.button.fontSizeRaw * 0.75}rem;
      line-height: 1.4;
      position: relative;

      box-sizing: border-box;
      width: min(${rem(concepts.contact.button.fontSizeRaw * 15)}, 90vw);
    `,
    form: () => css`
      font-size: ${concepts.contact.fontSize};
      font-family: ${concepts.contact.fontFamily};
      color: ${concepts.contact.fontColor};
      width: min(${rem(concepts.contact.fontSizeRaw * 18)}, 90vw);

      & > input[type="text"],
      & > input[type="email"],
      & > input[type="tel"],
      & > input[type="submit"],
      & > textarea {
        font-family: ${concepts.contact.fontFamily};
        color: inherit;
        font-size: inherit;
        border: 1px solid ${concepts.contact.inputBorderColor};
        padding: ${rem(concepts.contact.fontSizeRaw / 6)};
        margin-top: ${rem(concepts.contact.fontSizeRaw / 3)};
        margin-bottom: ${rem(concepts.contact.fontSizeRaw / 3)};
        box-sizing: border-box;
        background-color: inherit;
      }

      & > textarea {
        height: ${rem(concepts.contact.fontSizeRaw * 3)};
      }

      & > input[type="submit"] {
        font-family: ${concepts.contact.submit.fontFamily};
        cursor: pointer;
        color: ${concepts.contact.submit.textColor};
        text-transform: capitalize;
        border: 1px solid ${concepts.contact.submit.borderColor};
        width: ${rem(concepts.contact.fontSizeRaw * 5)};
        padding: ${rem(concepts.contact.fontSizeRaw / 5)};

        /* align-self: center; */
        background: ${concepts.contact.submit.buttonColor};

        transition: background 0.1s;

        &:hover {
          background-color: ${concepts.contact.submit.hoverColor1};
        }

        &:focus {
          background-color: ${concepts.contact.submit.hoverColor1};
        }

        &:active {
          background-color: ${concepts.contact.submit.hoverColor2};
        }
      }
    `,
  },
  footer: {
    progressBar: () => css`
      background-color: ${gradient(
        concepts.shared.title.color,
        concepts.shared.title.gradientColor
      )};
    `,
  },
  about: {
    headerPrimary: () => css`
      color: ${concepts.about.textColor1};
    `,
    text: () => css`
      font-size: ${concepts.about.textFontSize};
      font-family: ${concepts.about.fontFamily};
    `,
  },
} as const;

export const theme = {
  ...entities,
};

export interface AppThemeProps {
  theme: typeof theme;
}

export function AppThemeProvider(props: { children?: React.ReactNode }) {
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
}
