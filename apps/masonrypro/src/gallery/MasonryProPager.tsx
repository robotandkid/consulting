import { GalleryPager, GalleryPagerInfo } from "@robotandkid/components";
import styled from "styled-components";
import { text } from "../styles/styles";
import { theme } from "../styles/theme";

const PagerContainer = styled(GalleryPager)`
  display: flex;
  justify-content: space-around;
  align-items: baseline;
`;

const PagerButton = styled.button`
  ${theme.gallery.pager()}
`;

const Pager = styled(GalleryPagerInfo)`
  ${text}
  letter-spacing: -0.1rem;
  margin-left: 1rem;
  margin-right: 1rem;
`;

const leftButton = "<";
const rightButton = ">";

/**
 * - GalleryPager will always show the latest page number
 * - But...
 * - It the call to load the current page (onPagingCommit), is debounced.
 * - Fires `onSwipeRight`/`onSwipeLeft` synchronously after every update
 * - Fires `onPagingCommit` after the `debounce` timeout.
 */

export const MasonryProPager = (
  <PagerContainer>
    <PagerButton>{leftButton}</PagerButton>
    <Pager />
    <PagerButton>{rightButton}</PagerButton>
  </PagerContainer>
);
