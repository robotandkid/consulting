import {
  centeredColumn,
  defaultBasicGalleryGrid,
  DefaultBasicGalleryGrid,
  GalleryItem,
  ImageKit,
  InMemoryGalleryGrid,
  LinkTarget,
  nextImageSizes,
  setTOCItem,
} from "@robotandkid/components";
import styled from "styled-components";
import { pageMargins } from "../styles/styles";
import { theme } from "../styles/theme";
import { MasonryProPager } from "./MasonryProPager";

const Header = styled.h2`
  ${theme.shared.headers.h2()}
  ${theme.gallery.headerPrimary()}
  ${pageMargins}
`;

const GalleryContainer = styled.div`
  ${centeredColumn}
`;

const ImageItem = styled(GalleryItem)`
  ${theme.gallery.item()}
  position: relative;
`;

const GalleryGrid = styled(DefaultBasicGalleryGrid)`
  ${defaultBasicGalleryGrid}

  grid-template-rows: 50vh;
  grid-auto-rows: 50vh;
`;

const formatter = (x: number) => (x < 10 ? `0${x}` : `${x}`);

const galleryAlt = [
  { index: 1, alt: "sidewalk" },
  { index: 2, alt: "tile patio stairs" },
  { index: 3, alt: "brick wall" },
  { index: 4, alt: "fence" },
  { index: 5, alt: "brick patio" },
  { index: 6, alt: "brick patio" },
  { index: 7, alt: "driveway" },
  { index: 8, alt: "brick sidewalk" },
  { index: 9, alt: "tile patio" },
  { index: 10, alt: "brick driveway entry gate" },
  { index: 11, alt: "tile driveway" },
  { index: 12, alt: "driveway" },
] as const;

const imagesSrc = Array.from({ length: 12 })
  .map((_, index) => index + 1)
  .map((index) => {
    if (!galleryAlt[index]) console.warn(`Need alt description ${index}`);
    return {
      src: `/gallery-${formatter(index)}.jpg`,
      alt: galleryAlt[index]?.alt,
    } as const;
  });

const images = imagesSrc.map(({ alt, src }, index) => {
  return (
    <ImageItem key={index}>
      <ImageKit
        app="masonrypro"
        alt={alt}
        src={src}
        layout="fill"
        objectFit="cover"
        objectPosition="center top"
        useBlur
        sizes={nextImageSizes}
      />
    </ImageItem>
  );
});

const tocItem = { href: "#gallery", name: "gallery", index: 2 } as const;
setTOCItem(tocItem);

export function MasonryProGallery() {
  return (
    <section>
      <LinkTarget href={tocItem.href}>
        <Header>Gallery</Header>
      </LinkTarget>
      <GalleryContainer>
        <InMemoryGalleryGrid pageSize={20}>
          {MasonryProPager}
          <GalleryGrid>{images}</GalleryGrid>
        </InMemoryGalleryGrid>
      </GalleryContainer>
    </section>
  );
}
