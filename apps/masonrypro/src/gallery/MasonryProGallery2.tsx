import { LinkTarget, setTOCItem } from "@robotandkid/components";
import NextImage from "next/image";
import styled from "styled-components";
import { theme } from "../styles/theme";

const Header = styled.h2`
  ${theme.shared.headers.h2()}
  ${theme.services.headerPrimary()}
  margin: 0;
  margin-top: 1rem;
  margin-left: 1rem;
  margin-right: 1rem;
`;

const Container = styled.div`
  position: relative;
  max-width: 100vw;
  height: 100vh;
  overflow-x: auto;
`;

const ContainerInner = styled.div`
  display: flex;
  width: 1000vw;
  height: 100%;
  position: absolute;
`;

const formatter = (x: number) => (x < 10 ? `0${x}` : `${x}`);

const ImageContainer = styled.div`
  position: relative;
  width: 18rem;
  height: 18rem;
  margin-left: 1rem;
  margin-right: 1rem;
`;

const Image = (props: { url: string }) => {
  return (
    <ImageContainer>
      <NextImage src={props.url} layout="fill" objectFit="contain"></NextImage>
    </ImageContainer>
  );
};

const images = Array.from({ length: 12 })
  .map((_, index) => index + 1)
  .map((index) => `/images/gallery-${formatter(index)}.jpg`);

const tocItem = { href: "#gallery", name: "gallery", index: 2 } as const;
setTOCItem(tocItem);

export const MasonryProGallery = () => {
  return (
    <section>
      <LinkTarget href={tocItem.href}>
        <Header>Gallery</Header>
      </LinkTarget>
      <Container>
        <ContainerInner>
          {images.map((src, index) => (
            <Image key={index} url={src} />
          ))}
        </ContainerInner>
      </Container>
    </section>
  );
};
