const matter = require("gray-matter");

module.exports = async function extractFrontMatter(src) {
  const callback = this.async();
  const { content, data } = matter(src);
  const code = `# ${data.title}
  
${content}`;

  return callback(null, code);
};
