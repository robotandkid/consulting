# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.4.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.4.1...@robotandkid/nft@0.4.2) (2025-03-05)

**Note:** Version bump only for package @robotandkid/nft





## [0.4.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.4.0...@robotandkid/nft@0.4.1) (2025-03-05)

**Note:** Version bump only for package @robotandkid/nft





# [0.4.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.3.5...@robotandkid/nft@0.4.0) (2024-06-05)


### Features

* **chromata:** added more changes ([d942dca](https://gitlab.com/robotandkid/consulting/commit/d942dca9a99acf491813604f35f64648dfadaf4d))
* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





## [0.3.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.3.4...@robotandkid/nft@0.3.5) (2023-12-06)

**Note:** Version bump only for package @robotandkid/nft





## [0.3.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.3.3...@robotandkid/nft@0.3.4) (2023-11-07)

**Note:** Version bump only for package @robotandkid/nft





## [0.3.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.3.2...@robotandkid/nft@0.3.3) (2023-09-19)

**Note:** Version bump only for package @robotandkid/nft





## [0.3.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.3.1...@robotandkid/nft@0.3.2) (2023-09-18)

**Note:** Version bump only for package @robotandkid/nft





## [0.3.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.3.0...@robotandkid/nft@0.3.1) (2023-09-04)


### Bug Fixes

* enabled stricer array access in TS ([8b89be7](https://gitlab.com/robotandkid/consulting/commit/8b89be793edef75156025f9b3ee4a7944a308dc0))





# [0.3.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.2.0...@robotandkid/nft@0.3.0) (2023-08-07)


### Features

* upgraded node, pixi.js ([c06a0b4](https://gitlab.com/robotandkid/consulting/commit/c06a0b470991876a2763336d76a4272ca536af75))





# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.1.6...@robotandkid/nft@0.2.0) (2023-08-07)


### Features

* **digital-apps:** eli garcia published ([331476e](https://gitlab.com/robotandkid/consulting/commit/331476ecc8574bc6371c17268e674f68a6919c1e))


### Reverts

* **pixi:** downgraded to v5 ([5dd0831](https://gitlab.com/robotandkid/consulting/commit/5dd083158a5f0ced9bf7374f7087aff1eda5d002))





## [0.1.6](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.1.5...@robotandkid/nft@0.1.6) (2022-11-27)

**Note:** Version bump only for package @robotandkid/nft





## [0.1.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.1.3...@robotandkid/nft@0.1.4) (2022-02-02)

**Note:** Version bump only for package @robotandkid/nft





## [0.1.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.1.2...@robotandkid/nft@0.1.3) (2022-02-01)

**Note:** Version bump only for package @robotandkid/nft





## [0.1.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.1.1...@robotandkid/nft@0.1.2) (2022-01-24)

**Note:** Version bump only for package @robotandkid/nft





## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/nft@0.1.0...@robotandkid/nft@0.1.1) (2022-01-23)


### Bug Fixes

* **nft:** compiles again ([904e2d5](https://gitlab.com/robotandkid/consulting/commit/904e2d5f6fda191cf637110cb28717bf706e468c))





# 0.1.0 (2022-01-23)


### Features

* **@robotandkid/nft:** created ([2c69a3c](https://gitlab.com/robotandkid/consulting/commit/2c69a3c70f1903319e5c853b7dee1fb6eaa675b3))
