const path = require("path");

const extension = /\.mdx?$/;

const withMDX = require("@next/mdx")({
  extension,
});

module.exports = withMDX({
  async headers() {
    return [
      {
        source: "/fonts/mrpixel.otf",
        headers: [
          {
            key: "Cache-Control",
            value: "public, immutable, max-age=31536000",
          },
        ],
      },
    ];
  },
  pageExtensions: ["page.tsx", "mdx"],
  webpack(config) {
    const mdxRule = config.module.rules.find(
      (rule) => `${rule.test}` === `${extension}`
    );
    if (mdxRule) {
      mdxRule.use.push(path.join(__dirname, "./mdx-fm-loader"));
    }

    return config;
  },
});
