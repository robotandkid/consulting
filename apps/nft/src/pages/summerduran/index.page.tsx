import dynamic from "next/dynamic";
import React from "react";
import { OpenseaPage } from "./summerduran.dynamic";

const page = dynamic<typeof OpenseaPage>(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore wierd shit
    import("./summerduran.dynamic").then(
      (module) => module.OpenseaPage
      // TODO why does this not compile?
    ) as any,
  {
    ssr: false,
    loading() {
      return <div>Loading...</div>;
    },
  }
);

export default page;
