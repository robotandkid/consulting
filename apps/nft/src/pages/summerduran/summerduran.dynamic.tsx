import {
  PixiApp,
  PixiFilter,
  useLoadPixiSprite,
} from "@robotandkid/pixi-components";
import { useRef } from "react";
import styled from "styled-components";
import {
  Canvas2Screenshot,
  Canvas2Video,
  useHtml2Screenshot,
} from "../../util/screenshot";

const Banner = styled.div`
  width: 1400px;
  height: 400px;
  display: flex;
  justify-content: space-between;
  background: #d0fbf8;
`;

const BannerImage = styled.div<{ src: string }>`
  border: 5px solid #f0f0f0;
  border-bottom: 10px solid #f0f0f0;
  border-top: 10px solid #f0f0f0;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  background-image: url("/images/summer/${(p) => p.src}");
  background-position: center top;
  background-size: cover;
`;

function BannerDemo() {
  const ref = useRef<HTMLDivElement>(null);
  const Save = useHtml2Screenshot(ref);

  return (
    <>
      <Banner ref={ref}>
        <BannerImage src="01.jpg" />
        <BannerImage src="02.jpg" />
        <BannerImage src="03.jpg" />
        <BannerImage src="04.jpg" />
        <BannerImage src="05.jpg" />
        <BannerImage src="06.jpg" />
      </Banner>
      <Save />
    </>
  );
}

function BannerImage2() {
  const sprite = useLoadPixiSprite({
    path: "/images/summer/summer_banner.png",
    width: 1400,
    height: 400,
    parentType: "app",
  });

  return (
    <PixiFilter
      comp={sprite?.sprite}
      filters={[
        {
          filter: "dot",
          scale: 0.5,
        },
      ]}
    />
  );
}

function BannerDemo2() {
  return (
    <PixiApp
      width={1400}
      height={400}
      resolution={1}
      resources={[{ path: "/images/summer/summer_banner.png", type: "image" }]}
    >
      <BannerImage2 />
      <Canvas2Screenshot />
    </PixiApp>
  );
}

const featuredWidth = 600;
const featuredHeight = 400;

function FeaturedImage() {
  const sprite = useLoadPixiSprite({
    path: "/images/summer/featured.jpg",
    width: featuredWidth,
    height: featuredWidth * 3,
    parentType: "app",
  });

  if (sprite) sprite.sprite.position.y = 300;

  return (
    <PixiFilter
      comp={sprite?.sprite}
      filters={[
        {
          filter: "crt",
        },
        {
          filter: "pixelate",
          size: 20,
        },
      ]}
    />
  );
}

function FeaturedImageDemo() {
  return (
    <PixiApp
      width={featuredWidth}
      height={featuredHeight}
      backgroundColor="#605b5b"
      resolution={1}
      resources={[{ path: "/images/summer/featured.jpg", type: "image" }]}
    >
      <FeaturedImage />
      <Canvas2Video />
    </PixiApp>
  );
}

function LogoImage() {
  const sprite = useLoadPixiSprite({
    path: "/images/summer/featured.jpg",
    width: 350,
    height: 700,
    parentType: "app",
  });

  if (sprite) sprite.sprite.position.y = 250;

  return (
    <PixiFilter
      comp={sprite?.sprite}
      filters={[
        {
          filter: "pixelate",
          size: 1,
        },
      ]}
    />
  );
}

function LogoImageDemo() {
  return (
    <PixiApp
      width={350}
      height={350}
      resolution={1}
      resources={[{ path: "/images/summer/featured.jpg", type: "image" }]}
    >
      <LogoImage />
      <Canvas2Screenshot />
    </PixiApp>
  );
}

const Column = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const Row = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  padding: 1rem;
`;

export function OpenseaPage() {
  return (
    <Column>
      <Row>
        <BannerDemo />
      </Row>
      <Row>
        <BannerDemo2 />
      </Row>
      <Row>
        <FeaturedImageDemo />
      </Row>
      <Row>
        <LogoImageDemo />
      </Row>
    </Column>
  );
}
