import {
  defaultDotFilter,
  defaultGlitchFilter,
  defaultOldFilmFilter,
  defaultPixelateFilter,
  PixiAppContext,
  PixiAppWrapper,
  useAddFlock,
  useAddPixiFilter2,
  useAddToParent,
  useGlowingEyes,
  useLightningShader,
  useLoadPixiSprite,
} from "@robotandkid/pixi-components";
import { Bodies, Body, Composite, Engine, Runner } from "matter-js";
import { PixelateFilter } from "pixi-filters";
import { AnimatedSprite, Point, Sprite, Texture } from "pixi.js";
import { useContext, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { Canvas2Video } from "../../util/screenshot";

const engine = Engine.create();
const runner = Runner.create();

Runner.run(runner, engine);

const width = 300;
const height = 300 / 1.5;

const flyAnimation: Array<{ path: string; type: "image" }> = Array.from({
  length: 6,
}).map((_, index) => {
  return {
    path: `/images/charlie/fly1/fly1_${index}.png`,
    type: "image",
  };
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function NFT() {
  return (
    <>
      <PixiAppWrapper
        width={width}
        height={height}
        resolution={1}
        resources={flyAnimation.concat({
          path: "/images/charlie/charlie.png",
          type: "image",
        })}
      >
        {function useApp() {
          const app = useContext(PixiAppContext);
          const [birdShow, setBirdShow] = useState(false);

          const charlie = useLoadPixiSprite({
            width,
            height,
            path: "/images/charlie/charlie.png",
            parentType: "app",
          });

          useAddPixiFilter2({
            comp: charlie?.sprite,
            filters: [
              defaultPixelateFilter({ size: 5 }),
              defaultOldFilmFilter(),
            ],
          });

          const [bird, setBird] = useState<AnimatedSprite>();

          useAddPixiFilter2({
            comp: bird,
            filters: [
              {
                filter: new PixelateFilter(20),
                onStart(_, filter) {
                  const _filter = filter as PixelateFilter;
                  _filter.size = 20;
                },
                animation(_, filter) {
                  const _filter = filter as PixelateFilter;
                  const size = (_filter.size as number[])[0] ?? 0;
                  if (size > 1) _filter.size = size - 0.5;
                  else setBirdShow(true);
                },
              },
            ],
          });

          useEffect(
            function animation() {
              if (!app || !charlie) return;

              const textures = flyAnimation.map(({ path }) =>
                Texture.from(path)
              );

              const sprite = new AnimatedSprite(textures);
              sprite.x = app.screen.width * 0.1;
              sprite.y = app.screen.height * 0.6;
              sprite.scale.set(2);
              sprite.animationSpeed = 0.5;
              sprite.play();
              app.stage.addChild(sprite);

              setBird(sprite);

              return () => {
                app.stage.removeChild(sprite);
              };
            },
            [app, charlie]
          );

          useEffect(
            function addSpriteToMatter() {
              if (!app || !bird || !birdShow) return;

              const spriteBody = Bodies.rectangle(
                bird.x,
                bird.y,
                bird.width,
                bird.height
              );

              // spriteBody.friction = 0.05;
              // spriteBody.frictionAir = 0.00005;
              // spriteBody.restitution = 0.25;

              Composite.add(engine.world, spriteBody);

              return () => {
                Composite.remove(engine.world, spriteBody);
              };
            },
            [app, bird, birdShow]
          );

          useEffect(
            function syncPosition() {
              function animate() {
                for (const dot of engine.world.bodies) {
                  if (dot.isStatic || !bird) continue;

                  const { x, y } = dot.position;

                  bird.position.x = x;
                  bird.position.y = y;

                  break;
                }
              }

              if (!app || !bird) return;

              app.ticker.add(animate);

              return () => {
                app.ticker.remove(animate);
              };
            },
            [app, bird]
          );

          useEffect(() => {
            function animate() {
              for (const dot of engine.world.bodies) {
                if (dot.isStatic) continue;

                Body.applyForce(
                  dot,
                  { x: dot.position.x, y: dot.position.y },
                  { x: 0.01, y: -0.015 }
                );
              }
            }

            const interval = setInterval(
              () => requestAnimationFrame(animate),
              150
            );

            return () => {
              clearInterval(interval);
            };
          }, []);

          return <Canvas2Video autoStart />;
        }}
      </PixiAppWrapper>
    </>
  );
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function NFT2() {
  return (
    <>
      <PixiAppWrapper
        width={width}
        height={height}
        resolution={1}
        resources={flyAnimation.concat({
          path: "/images/charlie/charlie.png",
          type: "image",
        })}
      >
        {function useApp() {
          const charlie = useLoadPixiSprite({
            width,
            height,
            path: "/images/charlie/charlie.png",
            parentType: "app",
          });

          const charlieWithFilter = useAddPixiFilter2({
            comp: charlie?.sprite,
            filters: [
              defaultDotFilter(),
              // defaultPixelateFilter({ size: 5 }),
              // defaultOldFilmFilter(),
            ],
          });

          const glowingEyes = useGlowingEyes({
            eyeColor: "#fd957d",
            leftEye: new Point(115, 105),
            rightEye: new Point(164, 101),
            radius: 2,
            blurRadius: 20,
          });

          const start = useAddToParent({
            child: [charlieWithFilter, glowingEyes],
            parentType: "app",
          });

          useAddFlock({
            start,
            config: {
              flockColor: "#d8d8d8",
              alignmentWeight: 3,
              cohesionWeight: 3,
            },
          });

          return <Canvas2Video autoStart />;
        }}
      </PixiAppWrapper>
    </>
  );
}

const logoWidth = 350;
const logoHeight = 350;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function LogoImage() {
  return (
    <PixiAppWrapper width={logoWidth} height={logoHeight}>
      {function useApp() {
        const container = useRef(new Sprite());
        container.current.width = logoWidth;
        container.current.height = logoHeight;

        const comp = useLightningShader({
          comp: container.current,
          uniforms: {
            u_precision: "highp",
            u_time: 0,
            u_resolution: [logoWidth * 1.25, logoHeight],
            u_speed: 0.0025,
          },
        });

        const final = useAddPixiFilter2({
          comp: comp?.comp,
          filters: defaultDotFilter(),
        });

        useAddToParent({
          child: final,
          parentType: "app",
        });

        return <Canvas2Video />;
      }}
    </PixiAppWrapper>
  );
}

const featureWidth = 600;
const featureHeight = 400;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function FeatureImage() {
  return (
    <PixiAppWrapper width={featureWidth} height={featureHeight}>
      {function useApp() {
        const container = useRef(new Sprite());
        container.current.width = featureWidth;
        container.current.height = featureHeight;

        const comp = useLightningShader({
          comp: container.current,
          uniforms: {
            u_precision: "highp",
            u_time: 0,
            u_resolution: [featureWidth * 1.25, featureHeight],
            u_speed: 0.0025,
          },
        });

        const final = useAddPixiFilter2({
          comp: comp?.comp,
          filters: defaultGlitchFilter(),
        });

        useAddToParent({
          child: final,
          parentType: "app",
        });

        return <Canvas2Video />;
      }}
    </PixiAppWrapper>
  );
}

const bannerWidth = 1400;
const bannerHeight = 400;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function BannerImage() {
  return (
    <PixiAppWrapper width={bannerWidth} height={bannerHeight}>
      {function useApp() {
        const container = useRef(new Sprite());
        container.current.width = bannerWidth;
        container.current.height = bannerHeight;

        const comp = useLightningShader({
          comp: container.current,
          uniforms: {
            u_precision: "highp",
            u_time: 0,
            u_resolution: [bannerWidth * 1.25, bannerHeight],
            u_speed: 0.0025,
          },
        });

        const final = useAddPixiFilter2({
          comp: comp?.comp,
          filters: defaultOldFilmFilter({
            noise: 100,
            noiseSize: 5,
            scratchWidth: 1,
            vignetting: 0.3,
          }),
        });

        useAddToParent({
          child: final,
          parentType: "app",
        });

        return <Canvas2Video />;
      }}
    </PixiAppWrapper>
  );
}

const Column = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const Row = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  padding: 1rem;
`;

export function Page() {
  return (
    <Column>
      {/* <Row>
        <NFT />
      </Row>
      <Row>
        <NFT2 />
      </Row> */}
      <Row>
        <LogoImage />
      </Row>
    </Column>
  );
}
