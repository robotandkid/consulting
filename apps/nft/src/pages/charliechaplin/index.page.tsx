import dynamic from "next/dynamic";
import React from "react";
import { Page } from "./charliechaplin.dynamic";

const page = dynamic<typeof Page>(
  () =>
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore wierd shit
    import("./charliechaplin.dynamic").then(
      (module) => module.Page
      // TODO why does this not compile?
    ) as any,
  {
    ssr: false,
    loading() {
      return <div>Loading...</div>;
    },
  }
);

export default page;
