// @ts-check

const child = require("child_process");

const rootPath = process.env.PWD || "";

/**
 * @param {string} cwd
 * @returns
 */
const bash =
  (cwd = rootPath) =>
  /**
   * @param {TemplateStringsArray} command
   * @param {string[]} values
   * @returns
   */
  (command, ...values) => {
    const str = command.reduce(
      (total, cmd, index) => (total += `${cmd}${values[index] ?? ""}`),
      ""
    );

    return child
      .execSync(str, {
        cwd,
      })
      .toString();
  };

// const rootBash = bash();

/**
 *
 * @param {number} x
 */
const format = (x, precision = 2) => {
  if (x < 10) {
    const padding = Array.from({ length: precision })
      .map(() => `0`)
      .join("");
    return `${padding}${x}`;
  }
  if (x >= 10) {
    const padding = Array.from({ length: precision - 1 })
      .map(() => `0`)
      .join("");
    return `${padding}${x}`;
  }
  return `${x}`;
};

/**
 * @param {string} filename
 * @param {number} count
 *
 * @typedef {{
 *   format: {
 *     filename: string;
 *     nb_streams: number;
 *     nb_programs: number;
 *     format_name: string;
 *     format_long_name: string;
 *     start_time: string;
 *     duration: string;
 *     size: string;
 *     bit_rate: string;
 *     probe_score: number;
 *     tags: {
 *       major_brand: string;
 *       minor_version: string;
 *       compatible_brands: string;
 *       encoder: string;
 *     };
 *   };
 * }} Info
 */
export const video2pngByCount = async (filename, count) => {
  if (count < 2) throw new Error(`Count ${count} must be >= 2`);
  const infoRaw = bash()`ffprobe -v quiet -print_format json -show_format -show_streams -print_format json ${filename}`;
  const info = /** @type {Info} */ (JSON.parse(infoRaw));

  const {
    format: { duration },
  } = info;

  const durationInSec = Math.max(1, +duration - 1);
  const timeWidth = durationInSec / (count - 1);

  for (let len = 0; len < count; len += 1) {
    const t = (len * timeWidth).toFixed(1);
    const fileLen = format(len);
    bash()`ffmpeg -i ${filename} -ss ${t} -t 1 -r 1 -f image2 ${filename}-${fileLen}.png`;
  }
};

/**
 * @param {string} filename
 * @param  {string[]} frames
 */
export const video2pngByFrame = async (filename, ...frames) => {
  let len = 0;
  for (const frame of frames) {
    const fileLen = format(len++);
    bash()`ffmpeg -i ${filename} -ss ${frame} -t 1 -r 1 -f image2 ${filename}-${fileLen}.png`;
  }
};
