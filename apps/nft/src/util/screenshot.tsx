import { PixiAppContext } from "@robotandkid/pixi-components";
import html2canvas from "html2canvas";
import { Application } from "pixi.js";
import React, {
  useCallback,
  RefObject,
  useState,
  useEffect,
  useContext,
} from "react";

export const useHtml2Screenshot = (ref: RefObject<HTMLElement>) => {
  const save = useCallback(() => {
    if (!ref.current) return;

    html2canvas(ref.current).then((canvas) => {
      const img = canvas.toDataURL("image/png");
      const link = document.createElement("a");
      link.href = img;
      link.download = "screenshot.png";
      link.click();
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return () => <button onClick={save}>Save</button>;
};

export const Canvas2Screenshot = (_props: {
  app?: Application | undefined;
}) => {
  const app = useContext(PixiAppContext);

  const save = useCallback(() => {
    app?.view.toBlob((blob) => {
      if (blob) window.open(URL.createObjectURL(blob));
    });
  }, [app]);

  return <button onClick={save}>Save</button>;
};

function ConvertToVideoWorker(props: {
  canvas: HTMLCanvasElement;
  autoStart?: boolean;
}) {
  const { canvas, autoStart = false } = props;
  const [recorder, setRecorder] = useState<MediaRecorder>();
  const [status, setStatus] = useState<"init" | "recording" | "done">("init");

  useEffect(
    function initialize() {
      if (canvas) {
        setRecorder(new MediaRecorder(canvas.captureStream(25)));
      }
    },
    [canvas]
  );

  useEffect(
    function handleAutoStart() {
      if (autoStart && recorder) {
        setStatus("recording");
      }
    },
    [autoStart, recorder]
  );

  useEffect(
    function handleStart() {
      if (status === "recording") recorder?.start();
    },
    [recorder, status]
  );

  useEffect(
    function handleStop() {
      if (status === "done") recorder?.stop();
    },
    [recorder, status]
  );

  useEffect(
    function onDone() {
      recorder?.addEventListener("dataavailable", function (e) {
        const videoData = [e.data];
        const blob = new Blob(videoData, { type: "video/webm" });
        setStatus("init");
        window.open(URL.createObjectURL(blob));
      });
    },
    [recorder]
  );

  return (
    <>
      {status === "init" && (
        <button onClick={() => setStatus("recording")}>Start</button>
      )}
      {status === "recording" && (
        <button onClick={() => setStatus("done")}>Stop</button>
      )}
    </>
  );
}

export function Canvas2Video(props: {
  app?: Application;
  autoStart?: boolean;
}) {
  const { app: passedIn, autoStart } = props;
  const app = useContext(PixiAppContext);
  const canvas = (passedIn ?? app)?.view;

  const recorderAvailable =
    canvas?.captureStream &&
    typeof MediaRecorder !== "undefined" &&
    MediaRecorder.isTypeSupported("video/webm");

  return (
    <>
      {recorderAvailable && (
        <ConvertToVideoWorker canvas={canvas} autoStart={autoStart} />
      )}
    </>
  );
}
