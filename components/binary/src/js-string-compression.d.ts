declare module "js-string-compression" {
  class Hauffman {
    compress(msg: string): string;
  }

  export default {
    Hauffman,
  };
}
