// @ts-check

/** @param {string} str */
export const stringToBinary = (str) => {
  let binary = "";
  for (let i = 0; i < str.length; i++) {
    // convert each character to its Unicode code point
    const codePoint = str.codePointAt(i);

    // convert the code point to binary and pad with leading zeros
    const binaryCode = codePoint?.toString(2).padStart(8, "0");

    // append the binary code to the result string
    binary += binaryCode;
  }
  return binary;
};

/**
 * @param {string} binaryCode
 * @param {string} imageFile
 * @param {(url: string) => void} callback
 */
export function embedBinaryCodeInPNG1(binaryCode, imageFile, callback) {
  const img = new Image();

  img.onload = function () {
    // create canvas
    const canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext("2d");

    if (!ctx) return;
    ctx.drawImage(img, 0, 0);

    // get pixel data
    const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    const data = imageData.data;
    let binaryCodeIndex = 0;

    // loop through pixel data and embed binary code in the least significant bit of each pixel
    for (let i = 0; i < data.length; i += 4) {
      if (binaryCodeIndex >= binaryCode.length) {
        break;
      }
      const binaryDigit = parseInt(binaryCode[binaryCodeIndex]);
      data[i] = (data[i] & 254) | binaryDigit;
      binaryCodeIndex++;
    }

    // put the modified pixel data back into the canvas
    ctx.putImageData(imageData, 0, 0);

    // create a new image from the modified canvas and call the callback with the data URL of the new image
    const encodedImage = new Image();
    encodedImage.onload = function () {
      const canvas = document.createElement("canvas");
      canvas.width = encodedImage.width;
      canvas.height = encodedImage.height;
      const ctx = canvas.getContext("2d");
      if (!ctx) return;
      ctx.drawImage(encodedImage, 0, 0);
      callback(canvas.toDataURL());
    };
    encodedImage.src = canvas.toDataURL();
  };
  img.src = imageFile;
}

/**
 * @param {string} binaryCode
 * @param {string} imageSrc
 * @param {(url: string) => void} callback
 */
export function embedBinaryCodeInPNG2(binaryCode, imageSrc, callback) {
  // create a new Image object
  var img = new Image();

  // set the image source
  img.src = imageSrc;

  // wait for the image to load
  img.onload = function () {
    // create a new Canvas element
    var canvas = document.createElement("canvas");

    // set the canvas dimensions to match the image
    canvas.width = img.width;
    canvas.height = img.height;

    // get the 2D context of the canvas
    var context = canvas.getContext("2d");

    if (!context) return;
    // draw the image onto the canvas
    context.drawImage(img, 0, 0);

    // convert the binary code to a string
    var binaryString = binaryCode.toString();
    // let binarySpace = stringToBinary(" ");

    // loop through each pixel in the image
    for (var i = 0; i < canvas.width; i++) {
      for (var j = 0; j < canvas.height; j++) {
        // get the RGBA values of the pixel
        var pixelData = context.getImageData(i, j, 1, 1).data;

        // convert the RGBA values to binary
        var red = pixelData[0].toString(2).padStart(8, "0");
        var green = pixelData[1].toString(2).padStart(8, "0");
        var blue = pixelData[2].toString(2).padStart(8, "0");

        // combine the binary values into a single string
        var binaryValue = red + green + blue;

        if (binaryString.length === 0) {
          // repeat the message
          binaryString = binaryCode.toString();
        }
        // check if there is more binary code to embed
        if (binaryString.length > 0) {
          // get the next character from the binary code
          var binaryChar = binaryString.charAt(0);

          // set the least significant bit of the blue value
          if (binaryChar == "1") {
            binaryValue = binaryValue.slice(0, -1) + "1";
          } else {
            binaryValue = binaryValue.slice(0, -1) + "0";
          }

          // remove the processed character from the binary code
          binaryString = binaryString.substring(1);
          // } else {
          //   if (binarySpace.length === 0) binarySpace = stringToBinary(" ");

          //   const binaryChar = binarySpace.charAt(0);

          //   // set the least significant bit of the blue value
          //   if (binaryChar == "1") {
          //     binaryValue = binaryValue.slice(0, -1) + "1";
          //   } else {
          //     binaryValue = binaryValue.slice(0, -1) + "0";
          //   }

          //   binarySpace = binarySpace.substring(1);
        }

        // convert the binary value back to RGB
        var redValue = parseInt(binaryValue.slice(0, 8), 2);
        var greenValue = parseInt(binaryValue.slice(8, 16), 2);
        var blueValue = parseInt(binaryValue.slice(16, 24), 2);

        // set the new pixel values
        context.fillStyle =
          "rgb(" + redValue + ", " + greenValue + ", " + blueValue + ")";
        context.fillRect(i, j, 1, 1);
      }
    }

    // encode the canvas as a PNG image
    var encodedImage = canvas.toDataURL("image/png");

    // call the callback function with the encoded image
    callback(encodedImage);
  };
}

/**
 * @param {string} binaryCode
 * @param {string} imageSrc
 * @param {(url: string) => void} callback
 */
export function embedBinaryCodeInPNG3(binaryCode, imageSrc, callback) {
  // create a new Image object
  var img = new Image();

  // set the image source
  img.src = imageSrc;

  // wait for the image to load
  img.onload = function () {
    // create a new Canvas element
    var canvas = document.createElement("canvas");

    // set the canvas dimensions to match the image
    canvas.width = img.width;
    canvas.height = img.height;

    // get the 2D context of the canvas
    var context = canvas.getContext("2d");
    if (!context) return;

    // draw the image onto the canvas
    context.drawImage(img, 0, 0);

    // calculate the number of times to repeat the binary message in the image
    var numRepetitions = Math.ceil(
      (canvas.width * canvas.height * 3) / binaryCode.length
    );

    // repeat the binary message the necessary number of times
    var binary = binaryCode.repeat(numRepetitions);

    // loop through each pixel in the image
    for (var i = 0; i < canvas.width; i++) {
      for (var j = 0; j < canvas.height; j++) {
        // get the RGBA values of the pixel
        var pixelData = context.getImageData(i, j, 1, 1).data;

        // convert the RGBA values to binary
        var red = pixelData[0].toString(2).padStart(8, "0");
        var green = pixelData[1].toString(2).padStart(8, "0");
        var blue = pixelData[2].toString(2).padStart(8, "0");

        // get the next 3 bits of the binary code
        var nextBits = binary.substr(0, 3);

        // remove the next 3 bits from the binary code
        binary = binary.substr(3);

        // modify the least significant bit of the blue value
        blue = blue.slice(0, -1) + nextBits;

        // set the modified pixel data
        context.fillStyle =
          "rgba(" +
          parseInt(red, 2) +
          ", " +
          parseInt(green, 2) +
          ", " +
          parseInt(blue, 2) +
          ", " +
          pixelData[3] +
          ")";
        context.fillRect(i, j, 1, 1);
      }
    }

    // get the base64-encoded PNG image data
    var imageData = canvas.toDataURL("image/png");

    // call the callback function with the updated image data
    callback(imageData);
  };
}

/**
 * @param {string} imageSrc
 * @returns Promise<string>
 */
export function extractBinaryCodeFromPNG3(imageSrc) {
  // create a new Image object
  var img = new Image();

  // set the image source
  img.src = imageSrc;

  // create a new Canvas element
  var canvas = document.createElement("canvas");

  // wait for the image to load
  return new Promise(function (resolve, reject) {
    img.onload = function () {
      // set the canvas dimensions to match the image
      canvas.width = img.width;
      canvas.height = img.height;

      // get the 2D context of the canvas
      var context = canvas.getContext("2d");
      if (!context) return;

      // draw the image onto the canvas
      context.drawImage(img, 0, 0);

      // loop through each pixel in the image
      var binary = "";
      let binaryCode = "";
      for (var i = 0; i < canvas.width; i++) {
        for (var j = 0; j < canvas.height; j++) {
          // get the RGBA values of the pixel
          var pixelData = context.getImageData(i, j, 1, 1).data;

          // convert the RGBA values to binary
          //var red = pixelData[0].toString(2).padStart(8, "0");
          // var green = pixelData[1].toString(2).padStart(8, "0");
          var blue = pixelData[2].toString(2).padStart(8, "0");

          // get the least significant bit of the blue value
          var nextBit = blue.charAt(7);

          // add the bit to the binary code
          binary += nextBit;

          // check if we have read the entire message
          if (binary.length % 3 === 0) {
            // extract the next 3 bits from the binary code
            var message = binary.substr(0, 3);

            // remove the extracted bits from the binary code
            binary = binary.substr(3);

            // add the extracted bits to the message
            binaryCode += message;
          }
        }
      }

      // resolve the promise with the extracted binary code
      resolve(binaryCode);
    };

    // reject the promise if there is an error loading the image
    img.onerror = function () {
      reject(new Error("Error loading image"));
    };
  });
}

/**
 * @param {string} imageFile
 * @param {(binaryCode: string) => void} callback
 */
export function extractBinaryCodeFromPNG1(imageFile, callback) {
  const img = new Image();

  img.onload = function () {
    // create canvas
    const canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext("2d");
    if (!ctx) return;
    ctx.drawImage(img, 0, 0);

    // get pixel data
    const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    const data = imageData.data;
    let binaryCode = "";

    // loop through pixel data and extract binary code from the least significant bit of each pixel
    for (let i = 0; i < data.length; i += 4) {
      let binaryDigit = data[i] & 1;
      binaryCode += binaryDigit;
    }

    // call the callback with the extracted binary code
    callback(binaryCode);
  };

  img.src = imageFile;
}

/**
 * @param {string} imageSrc
 * @param {(msg: string) => void} callback
 */
export function extractBinaryCodeFromPNG2(imageSrc, callback) {
  // create a new Image object
  var img = new Image();

  // set the image source
  img.src = imageSrc;

  // wait for the image to load
  img.onload = function () {
    // create a new Canvas element
    var canvas = document.createElement("canvas");

    // set the canvas dimensions to match the image
    canvas.width = img.width;
    canvas.height = img.height;

    // get the 2D context of the canvas
    var context = canvas.getContext("2d");
    if (!context) return;

    // draw the image onto the canvas
    context.drawImage(img, 0, 0);

    // initialize the binary code
    var binaryCode = "";

    // loop through each pixel in the image
    for (let i = 0; i < canvas.width; i++) {
      for (let j = 0; j < canvas.height; j++) {
        // get the RGBA values of the pixel
        var pixelData = context.getImageData(i, j, 1, 1).data;

        // convert the RGBA values to binary
        var red = pixelData[0].toString(2).padStart(8, "0");
        var green = pixelData[1].toString(2).padStart(8, "0");
        var blue = pixelData[2].toString(2).padStart(8, "0");

        // combine the binary values into a single string
        var binaryValue = red + green + blue;

        // get the least significant bit of the blue value
        var lsb = binaryValue.slice(-1);

        // append the least significant bit to the binary code
        binaryCode += lsb;
      }
    }

    // convert the binary code to a string
    var binaryString = "";

    for (let i = 0; i < binaryCode.length; i += 8) {
      binaryString += String.fromCharCode(parseInt(binaryCode.substr(i, 8), 2));
    }

    // call the callback function with the extracted binary code
    callback(binaryString);
  };
}

/** @param {string} binary */
export function binaryToString(binary) {
  let result = "";
  for (let i = 0; i < binary.length; i += 8) {
    const byte = binary.substr(i, 8);
    result += String.fromCharCode(parseInt(byte, 2));
  }
  return result;
}
