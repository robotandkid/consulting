import React, { useCallback, useEffect, useState } from "react";
import {
  embedBinaryCodeInPNG2,
  extractBinaryCodeFromPNG2,
  stringToBinary,
} from "./index";
import * as compress from "js-string-compression";

export function ImageInput(props: { onChange?: (url: string) => void }) {
  const [imagePreview, setImagePreview] = useState<string | null>(null);

  function handleImageChange(e: any) {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        const { result } = reader;
        if (result) {
          setImagePreview(result.toString());
          props.onChange?.(result.toString());
        }
      };
    } else {
      setImagePreview(null);
    }
  }

  return (
    <div>
      {imagePreview ? (
        <img src={imagePreview} alt="Preview" />
      ) : (
        <div className="placeholder">Choose an image</div>
      )}
      <input type="file" accept="image/*" onChange={handleImageChange} />
    </div>
  );
}

export const Embed = () => {
  const [inputImageUrl, setInputImageUrl] = useState<string>();
  const [inputMessage, setInputMessage] = useState<string>();

  const [status, setStatus] = useState<string>();
  const [outputImageUrl, setOutputUrl] = useState<string>();
  const [outputMessage, setOutputMessage] = useState<string>();

  const encode = useCallback(
    (message: string | undefined) => {
      if (!inputImageUrl || !message) return;

      setStatus(`Encoding ${message}`);
      setOutputUrl(undefined);
      setOutputMessage(undefined);

      embedBinaryCodeInPNG2(
        stringToBinary(`<${message}>`),
        inputImageUrl,
        (str) => {
          setStatus(undefined);
          setOutputUrl(str);
        }
      );
    },
    [inputImageUrl]
  );

  useEffect(
    function decode() {
      if (!outputImageUrl) return;
      extractBinaryCodeFromPNG2(outputImageUrl, setOutputMessage);
    },
    [outputImageUrl]
  );

  return (
    <>
      <h1>Embed data in an image</h1>
      <div>This version works!</div>
      <input
        type="text"
        onChange={(evt) => setInputMessage(evt.target.value)}
      />
      <button
        disabled={!inputMessage || !inputImageUrl}
        onClick={() => encode(inputMessage)}
      >
        Go
      </button>
      {status && <div>{status}</div>}
      <hr />
      <div>Decoded message (from image): {outputMessage}</div>
      <h2>Original Image</h2>
      <ImageInput onChange={setInputImageUrl} />
      {outputImageUrl && <h2>New Image</h2>}
      {outputImageUrl && <img src={outputImageUrl} />}
    </>
  );
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore it does exist
const hmCompress = new compress.Hauffman();

export const EmbedWithCompression = () => {
  const [inputImageUrl, setInputImageUrl] = useState<string>();
  const [inputMessage, setInputMessage] = useState<string>();

  const [status, setStatus] = useState<string>();
  const [outputImageUrl, setOutputUrl] = useState<string>();
  const [outputMessage, setOutputMessage] = useState<string>();

  const [decompressedMessage, setDecompressedMessage] = useState<string>();

  const encode = useCallback(
    (message: string | undefined) => {
      if (!inputImageUrl || !message) return;

      setStatus(`Encoding ${message}`);
      setOutputUrl(undefined);
      setOutputMessage(undefined);
      setDecompressedMessage(undefined);

      embedBinaryCodeInPNG2(
        stringToBinary(`<${hmCompress.compress(message)}>`),
        inputImageUrl,
        (str) => {
          setStatus(undefined);
          setOutputUrl(str);
        }
      );
    },
    [inputImageUrl]
  );

  useEffect(
    function decode() {
      if (!outputImageUrl) return;
      extractBinaryCodeFromPNG2(outputImageUrl, setOutputMessage);
    },
    [outputImageUrl]
  );

  return (
    <>
      <h1>Embed data in an image</h1>
      <div>This version works!</div>

      <h2>Usage</h2>

      <h3>Embedding</h3>
      <div>Write a message</div>
      <div>Choose an image</div>
      <div>Press Go!</div>
      <div>
        How it works: it will compress the message, wrap it in angle brackets,
        then embed in image
      </div>
      <input
        type="text"
        onChange={(evt) => setInputMessage(evt.target.value)}
      />
      <button
        disabled={!inputMessage || !inputImageUrl}
        onClick={() => encode(inputMessage)}
      >
        Go
      </button>
      {status && <div>{status}</div>}
      <ImageInput onChange={setInputImageUrl} />

      <h3>Un-embedding</h3>
      <div>Copy the message between the angle brackets.</div>
      <div>Past the message (w/o angle brackets) in the input field.</div>
      <hr />
      <input
        type="text"
        onChange={(evt) =>
          setDecompressedMessage(hmCompress.decompress(evt.target.value))
        }
      />
      {decompressedMessage && (
        <div>Decompressed message: {decompressedMessage}</div>
      )}
      <div>Decoded message (from image): {outputMessage}</div>
      {outputImageUrl && <h2>New Image</h2>}
      {outputImageUrl && <img src={outputImageUrl} />}
    </>
  );
};

export const Extract = () => {
  const [inputImageUrl, setInputImageUrl] = useState<string>();
  const [status, setStatus] = useState<string>();
  const [outputMessage, setOutputMessage] = useState<string>();
  const [decompressedMessage, setDecompressedMessage] = useState<string>();

  useEffect(
    function decode() {
      if (!inputImageUrl) return;

      setStatus("Extracting...");
      setOutputMessage(undefined);

      extractBinaryCodeFromPNG2(inputImageUrl, (str) => {
        setStatus(undefined);
        setOutputMessage(str);
      });
    },
    [inputImageUrl]
  );

  return (
    <>
      <h1>Extract data from an image</h1>
      <div>This version works!</div>

      {status && <div>{status}</div>}

      <ImageInput onChange={setInputImageUrl} />

      <hr />

      <div>Copy the message between the angle brackets.</div>
      <div>Past the message (w/o angle brackets) in the input field.</div>
      <hr />
      <input
        type="text"
        onChange={(evt) =>
          setDecompressedMessage(hmCompress.decompress(evt.target.value))
        }
      />
      {decompressedMessage && (
        <div>Decompressed message: {decompressedMessage}</div>
      )}
      <div>Decoded message (from image): {outputMessage}</div>
    </>
  );
};
