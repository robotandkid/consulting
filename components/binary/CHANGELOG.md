# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/binary@0.1.2...@robotandkid/binary@0.1.3) (2023-09-19)


### Bug Fixes

* wierd TS compile errors ([88ad7d0](https://gitlab.com/robotandkid/consulting/commit/88ad7d04d91bc0a687a9273d00a50a8fab58162e))





## [0.1.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/binary@0.1.1...@robotandkid/binary@0.1.2) (2023-09-04)


### Bug Fixes

* enabled stricer array access in TS ([8b89be7](https://gitlab.com/robotandkid/consulting/commit/8b89be793edef75156025f9b3ee4a7944a308dc0))





## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/binary@0.1.0...@robotandkid/binary@0.1.1) (2023-08-07)


### Bug Fixes

* **binary:** compile error ([eefad1e](https://gitlab.com/robotandkid/consulting/commit/eefad1e1f746a7756ca4dfe63c424d708ffaf20b))





# 0.1.0 (2023-08-07)


### Features

* **binary:** added binary ([11978e6](https://gitlab.com/robotandkid/consulting/commit/11978e6fe8dc8b7571b9d57484e4db9ead734879))
