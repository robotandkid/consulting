/// <reference types="react" />
export declare function ImageInput(props: {
    onChange?: (url: string) => void;
}): JSX.Element;
export declare const Embed: () => JSX.Element;
export declare const EmbedWithCompression: () => JSX.Element;
