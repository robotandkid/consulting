import React, { Context, ReactNode, useContext, useState } from "react";

export function createContext<T>(initial?: T) {
  return React.createContext<T>(initial ?? ({} as T));
}

export function makeReadOnlyContext<MyContext>(name: string) {
  const context = createContext<MyContext | undefined>();

  const Provider = (props: { children?: ReactNode; value: MyContext }) => {
    const { children, value } = props;

    return <context.Provider value={value}>{children}</context.Provider>;
  };

  Provider.displayName = name;

  const useMyContext = () => {
    return useContext(context);
  };

  return [Provider, useMyContext, context] as const;
}

export function makeSimpleContext<MyContext>(name: string) {
  const context = createContext<MyContext | undefined>();
  const updateContext = createContext<(value: MyContext) => void>();

  const Provider = (props: {
    children?: ReactNode;
    initialValue: MyContext;
  }) => {
    const { children, initialValue } = props;
    const [state, setState] = useState(initialValue);

    return (
      <updateContext.Provider value={setState}>
        <context.Provider value={state}>{children}</context.Provider>
      </updateContext.Provider>
    );
  };

  type Provider = typeof Provider;

  Provider.displayName = name;

  return [Provider, context, updateContext] as [
    Provider,
    Context<MyContext>,
    Context<(value: MyContext) => void>
  ];
}
