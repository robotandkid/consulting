import React from "react";
import { defaultTextStyle, PixiAppWrapper } from "@robotandkid/pixi-components";
import { PixelateFilter } from "pixi-filters";
import {
  Background,
  Delay,
  Filter,
  PhysicsSprite,
  Say,
  Scene,
  SceneProvider,
} from "./AwesomeRetroContent";

const width = 300;
const height = 500;

export function SceneExample() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/images/jump.png",
        },
        {
          type: "image",
          path: "/images/bg.png",
        },
      ]}
    >
      {function Fixture() {
        const dialogStyle = {
          style: {
            ...defaultTextStyle(),
            fontSize: "1em",
            height: height / 2,
            top: height / 2,
          },
        };

        return (
          <SceneProvider>
            <Scene name="scene1">
              <Background path="/images/bg.png" width={width} height={height} />
              <Delay timeout={1000} />
              <PhysicsSprite
                path="/images/jump.png"
                x={width / 2}
                y={height / 2}
              />
              <Say actor="Grandma" hideActor {...dialogStyle}>
                mmmm.... this is good!
              </Say>
              <Say actor="Me" {...dialogStyle}>
                Wow, Grandma! I didn&apos;t know you had fake teeth!
              </Say>
              <Filter
                transition={{
                  filter: new PixelateFilter(0),
                  animation(next, time, filter) {
                    (filter as PixelateFilter).size = time;
                    if (time === 3_000) next();
                  },
                }}
              />
              <Filter
                transition={{
                  filter: new PixelateFilter(0),
                  animation(next, time, filter) {
                    (filter as PixelateFilter).size = time;
                    if (time === 3_000) next();
                  },
                }}
              />
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}

export function TypewriterExample() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/images/jump.png",
        },
        {
          type: "image",
          path: "/images/bg.png",
        },
      ]}
    >
      {function Fixture() {
        const dialogStyle = {
          style: {
            ...defaultTextStyle(),
            fontSize: "1em",
            height: height / 2,
            top: height / 2,
          },
        };

        return (
          <SceneProvider>
            <Scene name="scene1">
              <Say actor="Grandma" hideActor {...dialogStyle}>
                mmmm.... this is good!
              </Say>
              <div>Done</div>
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}

export function SimpleExample() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/images/jump.png",
        },
        {
          type: "image",
          path: "/images/bg.png",
        },
      ]}
    >
      {function Fixture() {
        return (
          <SceneProvider>
            <Scene name="scene1">
              <Background path="/images/bg.png" width={width} height={height} />
              <Delay timeout={3000} />
              <Background
                path="/images/jump.png"
                width={width}
                height={height}
              />
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}

export function DelayExample() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/images/jump.png",
        },
        {
          type: "image",
          path: "/images/bg.png",
        },
      ]}
    >
      {function Fixture() {
        return (
          <SceneProvider>
            <Scene name="scene1">
              <Delay timeout={1000} />
              <div>Done</div>
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}
