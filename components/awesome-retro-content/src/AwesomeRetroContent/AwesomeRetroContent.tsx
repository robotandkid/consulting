import {
  PixiAppContext,
  PixiEvents,
  RegisterPixiEventListeners,
  TypewriterText,
  TypewriterTextOpts,
  useAddPixiFilter2,
  useLoadPixiSprite,
} from "@robotandkid/pixi-components";
import { Bodies, Composite, Engine, Runner } from "matter-js";
import { Filter as PixiFilter } from "pixi.js";
import React, {
  Children,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { makeReadOnlyContext } from "../utils/context";

/**
 * # Overview
 *
 * The first problem is registering the SceneChild. This is solved by prop
 * drilling the index/current value by the Scene.
 *
 * The second problem is re-running the Transition the user navigates
 * back/forth. We can do this by completely re-mounting the SceneChild or by
 * making a "shouldRerender" context available.
 *
 * There are also two sub-problems:
 *
 * - The user goes back. We want to re-run the Transition.
 * - The user goes forward. We want to end the current Transition. This means
 *   different things depending on the transition. For example, a Delay should
 *   abort (the currently running delay timeout). Both a filter and a sprite
 *   should jump to the final render.
 *
 * Another observation:
 *
 * - We have PixiEvents for this already.
 * - the biggest problem is ending the Transition. See below.
 *
 * ## Remount the component
 *
 * All approaches work by re-rendering a slightly different version of the
 * component. Ex: changing the key. The gotcha is that once you remount the
 * component, you should *not* remount unnecessarily. Ex: suppose the DOM is:
 *
 * - Filter
 * - Say
 *
 * As the user navigates away from the Filter, we most likely do not want to
 * re-run the filter.
 *
 * All solutions require tracking the re-mounted component. This can be complex.
 *
 * The main benefit is that since the SceneChild is re-mounted, the underlying
 * Transition "should just work", that is, there is no extra code (inside the
 * Transition) needed to re-run/clean up the Transition (other than the usual
 * React lifecycle hooks).
 *
 * ## Re-render the component
 *
 * The difference between the above approach is that the component is
 * re-rendered, not re-mounted. So the _Transition_ itself needs to manually
 * cleanup/re-run itself. This code can be _in addition_ to the usual React
 * lifecycle hooks.
 *
 * IMO, this approach is *not* sustainable as the Transitions grow. It is very
 * easy to implement this pattern incorrectly.
 *
 * ## Ending the Transition
 *
 * Simplest solution is just to add an "end" DOM node. Ex:
 *
 * ```
 * const Say = ({ text: string }) => {
 *  return <Transition
 *    start={<TypewriterText text={text}} />}
 *    end={<TypewriterText text={text} end/>}
 * />
 * ```
 *
 * The only con is that the underlying component may not support an "end"
 * transition and so may need rework.
 *
 *
 * ### Playground
 *
 * const Filter
 */

export const [SceneNextProvider, useNextScene] =
  makeReadOnlyContext<() => void>("SceneNext");

const [ScenePrevProvider, usePrevScene] =
  makeReadOnlyContext<() => void>("ScenePrevious");

const [SceneIndexProvider, useSceneIndex] =
  makeReadOnlyContext<number>("SceneIndex");

const [SceneCurrentProvider, useSceneCurrent] =
  makeReadOnlyContext<number>("SceneMeta");

function Remount(props: { children: ReactNode }) {
  const { children } = props;
  const index = useSceneIndex() ?? -1;
  const current = useSceneCurrent() ?? 0;
  const [childToRemount, setChildToRemount] = useState<typeof children>(null);

  useEffect(
    function reRender() {
      if (index === current) {
        if (!React.isValidElement(children)) setChildToRemount(<>{children}</>);
        else
          setChildToRemount(
            React.cloneElement(children, { key: `${Math.random()}` })
          );
      }
    },
    [children, current, index]
  );

  if (index > current) return null;
  if (!React.isValidElement(childToRemount)) return <>{childToRemount}</>;
  return childToRemount;
}

function Cycle(props: { start: ReactNode; end: ReactNode }) {
  const { start, end } = props;
  const index = useSceneIndex() ?? -1;
  const current = useSceneCurrent() ?? 0;
  const [showEnd, setShowEnd] = useState(false);
  const parentNext = useNextScene();

  const goToEnd = useCallback(() => {
    setShowEnd(true);
  }, []);

  useEffect(() => {
    if (index > current) {
      setShowEnd(true);
    }
  }, [current, index]);

  useEffect(
    function edgeCase() {
      // when there is no end, call the parent next;
      if (showEnd && !end && index === current) parentNext?.();
    },
    [current, index, parentNext, showEnd, end]
  );

  return (
    <>
      {!showEnd && (
        <SceneNextProvider value={goToEnd}>{start}</SceneNextProvider>
      )}
      {showEnd && end}
    </>
  );
}

function Transition(props: { start: ReactNode; end: ReactNode }) {
  const { start, end } = props;

  return <Cycle start={start} end={end} />;
}

interface SceneProps {
  name: string;
  children: ReactNode;
}

/**
 * - Renders its children one at a time
 * - It will advance to the next child as it calls the SceneNextContext
 * - Children will be rendered on top of each other, so if a child needs to disappear,
 *   it can return null before calling onNext
 */
export function Scene(props: SceneProps) {
  const children = Children.toArray(props.children);

  const length = children.length;
  const [current, setCurrent] = useState(0);
  const parentNext = useNextScene();
  const parentPrev = usePrevScene();

  const next = useCallback(() => {
    setCurrent((c) => {
      if (c < length - 1) return c + 1;
      if (typeof parentNext === "function") parentNext();
      return c;
    });
  }, [length, parentNext]);

  const prev = useCallback(() => {
    setCurrent((c) => {
      if (c > 0) return 0;
      if (typeof parentPrev === "function") parentPrev();
      return c;
    });
  }, [parentPrev]);

  const childrenToRender = useMemo(() => {
    return children.map((child, index) => {
      return (
        <SceneIndexProvider value={index} key={index}>
          <SceneCurrentProvider value={current}>
            <Remount>{child}</Remount>
          </SceneCurrentProvider>
        </SceneIndexProvider>
      );
    });
  }, [children, current]);

  return (
    <>
      <RegisterPixiEventListeners onNextEvent={next} onPreviousEvent={prev} />
      <SceneNextProvider value={next}>
        <ScenePrevProvider value={prev}>{childrenToRender}</ScenePrevProvider>
      </SceneNextProvider>
    </>
  );
}

export const [PhysicsProvider, usePhysicsContext, PhysicsContext] =
  makeReadOnlyContext<{
    engine: ReturnType<typeof Engine.create>;
    runner: ReturnType<typeof Runner.create>;
  }>("Physics");

export function SceneProvider(props: {
  children:
    | React.ReactElement<SceneProps>
    | [React.ReactElement<SceneProps>, ...React.ReactElement<SceneProps>[]];
}) {
  const physicsContext = useMemo(
    () => ({
      engine: Engine.create(),
      runner: Runner.create(),
    }),
    []
  );

  useEffect(
    function setupPhysics() {
      Runner.run(physicsContext.runner, physicsContext.engine);
    },
    [physicsContext]
  );

  return (
    <PhysicsProvider value={physicsContext}>
      <PixiEvents>
        <Scene name="root">{props.children}</Scene>
      </PixiEvents>
    </PhysicsProvider>
  );
}

export const Background = (props: {
  path: string;
  width: number;
  height: number;
}) => {
  const { width, height } = props;
  const next = useNextScene();

  const sprite = useLoadPixiSprite({
    path: props.path,
    width,
    height,
    parentType: "app",
  });

  useEffect(() => {
    if (sprite) {
      next?.();
    }
  }, [next, sprite]);

  return null;
};

const DelayStart = (props: { timeout: number }) => {
  const { timeout } = props;
  const next = useNextScene();
  /**
   * Code hardening: technically, there could be a race condition between the timeout and unmount
   */
  const unmounted = useRef(false);

  useEffect(() => {
    unmounted.current = false;

    const _timeout = setTimeout(() => {
      if (unmounted.current) return;
      next?.();
    }, timeout);

    return () => {
      unmounted.current = true;
      clearTimeout(_timeout);
    };
  }, [next, timeout]);

  return null;
};

export const Delay = (props: { timeout: number }) => {
  return <Transition start={<DelayStart {...props} />} end={null} />;
};

export const PhysicsSprite = (props: {
  path: string;
  x: number;
  y: number;
}) => {
  const { path, x, y } = props;
  const next = useNextScene();
  const app = useContext(PixiAppContext);

  const spriteData = useLoadPixiSprite({
    path: path,
    width: app?.renderer.width ?? 0,
    height: app?.renderer.height ?? 0,
    parentType: "app",
  });

  const sprite = spriteData?.sprite;
  const physics = usePhysicsContext();

  useEffect(
    function createPhysicsBody() {
      if (!sprite || !physics) return;

      const spriteBody = Bodies.rectangle(x, y, sprite.width, sprite.height);

      Composite.add(physics.engine.world, spriteBody);

      return () => {
        Composite.remove(physics.engine.world, spriteBody);
      };
    },
    [physics, sprite, x, y]
  );

  useEffect(() => {
    if (sprite) {
      next?.();
    }
  }, [sprite, next]);

  useEffect(
    function syncPosition() {
      function animate() {
        if (!physics) return;

        for (const dot of physics.engine.world.bodies) {
          if (dot.isStatic || !sprite) continue;

          const { x, y } = dot.position;

          sprite.position.x = x;
          sprite.position.y = y;

          break;
        }
      }

      if (!app || !sprite) return;

      app.ticker.add(animate);

      return () => {
        app.ticker.remove(animate);
      };
    },
    [app, physics, sprite]
  );

  return null;
};

const SayStart = (
  props: {
    actor: string;
    hideActor?: boolean;
    children: string | string[];
  } & Pick<TypewriterTextOpts, "style">
) => {
  const { children, style, hideActor } = props;
  const next = useNextScene();
  const app = useContext(PixiAppContext);
  const lines = Array.isArray(children) ? children : [children];
  const [lineNumber, setLineNumber] = useState(0);
  const line = lines[lineNumber] as string | undefined;
  const lineWithActor = `${props.actor}: ${line}`;
  const done = lineNumber === lines.length - 1;
  const text = !line ? undefined : hideActor ? line : lineWithActor;

  const onLineDone = useCallback(() => {
    setLineNumber((l) => l + 1);
    if (done) next?.();
  }, [done, next]);

  return (
    <TypewriterText
      lineId={text ?? ""}
      style={style}
      soundPath="/sounds/textscroll.wav"
      typingSpeedInMs={20}
      onLineDone={onLineDone}
      onLineDoneDelayMs={500}
      parent={app}
    >
      {text ?? ""}
    </TypewriterText>
  );
};

export const Say = (props: Parameters<typeof SayStart>[0]) => {
  return <Transition start={<SayStart {...props} />} end={null} />;
};

interface ARCFilter {
  filter: PixiFilter;
  onStart?: (elapsedTimeMs: number, filter: PixiFilter) => void;
  animation?: (
    onEnd: () => void,
    elapsedTimeMs: number,
    filter: PixiFilter
  ) => void;
}

type RobotPixiFilter = Parameters<typeof useAddPixiFilter2>[0]["filters"];

export const Filter = (props: { transition: ARCFilter }) => {
  const app = useContext(PixiAppContext);
  const next = useNextScene();

  const transition: RobotPixiFilter = (
    Array.isArray(props.transition) ? props.transition : [props.transition]
  ).map(({ filter, onStart, animation }) => ({
    filter,
    onStart: (elapsedTimeMs, filter) => {
      onStart?.(elapsedTimeMs, filter);
    },
    animation: (elapsedTimeMs, filter) => {
      animation?.(next, elapsedTimeMs, filter);
    },
  }));

  const added = useAddPixiFilter2({
    comp: app,
    filters: transition,
  });

  useEffect(() => {
    if (added) next?.();
  }, [added, next]);

  return null;
};
