# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.2.0...@robotandkid/awesome-retro-content@0.2.1) (2025-03-05)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.8...@robotandkid/awesome-retro-content@0.2.0) (2025-03-05)


### Features

* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





## [0.1.8](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.7...@robotandkid/awesome-retro-content@0.1.8) (2023-12-06)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





## [0.1.7](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.6...@robotandkid/awesome-retro-content@0.1.7) (2023-11-07)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





## [0.1.6](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.5...@robotandkid/awesome-retro-content@0.1.6) (2023-09-19)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





## [0.1.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.4...@robotandkid/awesome-retro-content@0.1.5) (2023-09-18)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





## [0.1.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.3...@robotandkid/awesome-retro-content@0.1.4) (2023-09-04)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





## [0.1.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.2...@robotandkid/awesome-retro-content@0.1.3) (2023-08-07)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





## [0.1.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.1...@robotandkid/awesome-retro-content@0.1.2) (2023-08-07)

**Note:** Version bump only for package @robotandkid/awesome-retro-content





## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/awesome-retro-content@0.1.0...@robotandkid/awesome-retro-content@0.1.1) (2022-11-27)

**Note:** Version bump only for package @robotandkid/awesome-retro-content
