import {
  GalleryItem,
  GallerySkeletonItem,
  InMemoryGalleryGrid,
  suggestedGalleryItemTypography,
} from "./lib/GalleryGrid/GalleryGrid";
import { GalleryPager, GalleryPagerInfo } from "./lib/GalleryGrid/GalleryPager";
import { ImageKit } from "./lib/ImageKit/ImageKit";
import { Parallax } from "./lib/Parallax/Parallax";
import { FacebookSEO } from "./lib/SEO/FacebookSEO";
import { StructuredDataSEO } from "./lib/SEO/StructuredDataSEO";
import { TitleSEO } from "./lib/SEO/TitleSEO";
import {
  ExternalLink,
  InternalLink,
  LinkTarget,
} from "./lib/TableOfContents/Link/Link";
import {
  getStaticPathsForStaticTOC,
  getStaticPropsStaticTOC,
  GetStaticPropsStaticTOC as _GetStaticPropsStaticTOC,
  StaticTOCItem as _StaticTOCItem,
  SetStaticTOC,
  StaticTOCContext,
  StaticTOCProvider,
} from "./lib/TableOfContents/StaticTOC/StaticTOC";
import { H1, H2, H3 } from "./lib/Text/Header";
import { Text } from "./lib/Text/Text";
import { media, useIsAtMostMediaSize } from "./styles/media";

export type GetStaticPropsStaticTOC = _GetStaticPropsStaticTOC;
export type StaticTOCItem = _StaticTOCItem;

export * from "./lib";
export * from "./utils";
export * from "./styles";

export {
  InMemoryGalleryGrid,
  suggestedGalleryItemTypography,
  GalleryItem,
  GallerySkeletonItem,
  GalleryPager,
  GalleryPagerInfo,
  Text,
  H1,
  H2,
  H3,
  InternalLink,
  ExternalLink,
  LinkTarget,
  StaticTOCContext,
  StaticTOCProvider,
  SetStaticTOC,
  getStaticPropsStaticTOC,
  getStaticPathsForStaticTOC,
  Parallax,
  ImageKit,
  FacebookSEO,
  StructuredDataSEO,
  TitleSEO,
  media,
  useIsAtMostMediaSize,
};
