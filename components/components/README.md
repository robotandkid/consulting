# `@robotandkid/components`

## Usage

```bash
lerna add @robotandkid/components --scope app
```

```typescript
import {
  LandingHero,
  //...
} from "@robotandkid/components";
```

## Development

- Don't to forget to build the app!
- Run `lerna run build --scope "*/components"`
- Commit changes
- Then `yarn lerna:publish` to "publish" the changes
