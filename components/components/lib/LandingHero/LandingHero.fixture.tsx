import styled from "styled-components";
import React from "react";
import { LandingHero } from "./LandingHero";
import { m } from "framer-motion";

const FullTitle = styled(m.h1)`
  font-family: sans-serif;
  font-size: 3em;
  font-weight: bold;
`;

const ReducedTitle = styled(m.h1)`
  font-family: sans-serif;
  font-size: 1em;
  font-weight: bold;
`;

const dummyPageText = `I'm baby scenester umami marfa letterpress, leggings woke echo park flexitarian four dollar toast everyday carry DIY live-edge. Banh mi unicorn succulents forage, migas pok pok iceland literally vinyl meh. Migas gluten-free venmo chia polaroid. Trust fund williamsburg taiyaki XOXO cray franzen 3 wolf moon pitchfork sustainable celiac raclette flexitarian intelligentsia kinfolk. Mumblecore vice aesthetic keffiyeh +1. Church-key PBR&B yuccie put a bird on it. Kitsch pork belly stumptown fanny pack letterpress, chambray art party etsy.

Retro live-edge cloud bread everyday carry selvage vaporware, edison bulb hella +1 literally pickled crucifix. Meggings mlkshk keytar seitan, pug chillwave unicorn small batch brooklyn DIY wolf. Pickled asymmetrical pour-over, man bun pinterest jean shorts semiotics iceland. Brooklyn microdosing chicharrones next level cliche, hexagon coloring book kogi helvetica PBR&B. Salvia adaptogen beard affogato activated charcoal microdosing, af bespoke flexitarian four dollar toast artisan. Vape pour-over neutra sartorial hot chicken mumblecore +1 beard, 8-bit chia. Vice shabby chic vexillologist leggings hoodie hell of.

Health goth farm-to-table vape adaptogen hammock pabst. Air plant brunch microdosing tbh ennui, stumptown fashion axe lo-fi kombucha YOLO hexagon. Tacos tumeric selvage art party iceland church-key. Waistcoat kinfolk kickstarter, wayfarers typewriter mustache pitchfork hexagon truffaut schlitz. Swag salvia la croix, lyft raw denim taxidermy single-origin coffee actually iPhone squid seitan mustache deep v bicycle rights. Drinking vinegar ugh tote bag selfies.

Occupy yuccie PBR&B freegan, 8-bit schlitz activated charcoal vinyl disrupt selfies. Direct trade sriracha banh mi typewriter flexitarian iPhone, vegan adaptogen sustainable. Mlkshk offal vexillologist copper mug pitchfork master cleanse man braid fashion axe lyft. Jianbing lomo meh street art cold-pressed, twee raw denim distillery kitsch.

Cliche letterpress lyft gastropub DIY swag flexitarian whatever pickled everyday carry brooklyn. Leggings neutra chillwave pok pok prism snackwave. Snackwave shaman portland, readymade shoreditch narwhal marfa mumblecore echo park art party. Craft beer pitchfork pork belly ethical cold-pressed keytar gastropub artisan. Enamel pin street art freegan readymade edison bulb actually salvia. Activated charcoal cliche iceland copper mug. Viral vegan cardigan snackwave tumeric farm-to-table coloring book.`;

const DummyPage = styled.div`
  font-size: 3em;
  font-family: serif;
`;

export function Fixture() {
  return (
    <>
      <LandingHero>
        <FullTitle>Hello world</FullTitle>
        <ReducedTitle>Hello</ReducedTitle>
      </LandingHero>
      <DummyPage>{dummyPageText}</DummyPage>
    </>
  );
}
