import { LayoutGroup, m } from "framer-motion";
import React, { ReactElement } from "react";
import styled, { css } from "styled-components";
import {
  createModuleState,
  useModuleState,
} from "../../utils/microStateManagement";
import { useDetectScrolling } from "../../utils/scrolling";
import { globalWindow } from "../../utils/window";
import { zIndexNavigation } from "../../utils/zIndex";

export const LandingHeroContainer = styled(m.div)`
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  background: transparent;
`;

/**
 * Use this as the basis for the ReducedHero variant.
 *
 * Defaults to position fixed in the top-right.
 */
export const navigationLandingHeroTitle = css`
  position: fixed;
  top: 1rem;
  right: 1rem;
  width: auto;
  z-index: ${zIndexNavigation};
`;

export const NavigationLandingHeroTitle = styled(m.div)`
  ${navigationLandingHeroTitle}
`;

const BgContainer = styled(m.div)`
  position: relative;
`;

export const useDetectLandingScrolled = () =>
  useDetectScrolling({
    deltaY: (globalWindow()?.innerHeight || 0) * 0.25,
  });

const requestShrinkState = createModuleState(false);
/**
 * When users call this method, it will be permanently rendered at a
 * smaller size. Call #cancelShrinkLandingHero to cancel.
 */
export const shrinkLandingHero = () => requestShrinkState.setState(true);
/**
 * Note: this won't automatically grow the landing hero if it's
 * scrolled away.
 */
export const cancelShrinkLandingHero = () => requestShrinkState.setState(false);

type LargeElementProps = {
  layoutId?: string;
};

type SmallElementProps = {
  layoutId?: string;
};

/**
 * Usage:
 *
 * - The first child element is the title, large version. You must use
 *   the provided FullHero component as the basis for your component.
 * - The second child element is the title, small version. You must use
 *   the provided ReducedHero component as the basis for your
 *   component.
 * - To use in non-landing pages, set forceReducedHero to true
 */
export function LandingHero(props: {
  disabled?: boolean;
  children: [ReactElement<LargeElementProps>, ReactElement<SmallElementProps>];
}) {
  const [requestShrink] = useModuleState(requestShrinkState);
  const { children, disabled = false } = props;
  const [largeTitle, smallTitle] = children;

  const scrolled = useDetectLandingScrolled();
  const shrink = requestShrink || scrolled || disabled;

  return (
    <>
      <LayoutGroup id="landing-bg">
        <BgContainer>
          <LandingHeroContainer
            initial={false}
            animate={disabled ? { height: 0 } : { height: "100vh" }}
          >
            {!shrink && <m.div layoutId="landing-hero">{largeTitle}</m.div>}
          </LandingHeroContainer>
        </BgContainer>
        {shrink && (
          <NavigationLandingHeroTitle layoutId="landing-hero">
            {smallTitle}
          </NavigationLandingHeroTitle>
        )}
      </LayoutGroup>
    </>
  );
}
