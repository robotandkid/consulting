import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { dummyPhrases } from "../../fixtures/dummyText";
import { GlobalStyle } from "../../fixtures/GlobalStyle";
import { AppThemeProvider } from "../../styles/theme";
import { CenteredColumn } from "../Text/Grid";
import { BasicGalleryGrid } from "./BasicGalleryGrid";
import {
  GalleryItem,
  GallerySkeletonItem,
  suggestedGalleryItemTypography,
} from "./GalleryGrid";
import { GalleryPager, GalleryPagerInfo } from "./GalleryPager";

function getCards(current: number, pageSize: number) {
  return dummyPhrases.filter(
    (_, index) =>
      index >= (current - 1) * pageSize && index < current * pageSize
  );
}

const SkeletonCards = (props: { current: number; pageSize: number }) => {
  const { current, pageSize } = props;

  return (
    <>
      {getCards(current, pageSize).map((title, i) => (
        <GallerySkeletonItem key={`${title}:${i}`} />
      ))}
    </>
  );
};

const Item = styled(GalleryItem)`
  ${suggestedGalleryItemTypography}
  font-family: sans-serif;
  font-weight: bold;
  background: #c35;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;

const Cards = (props: { current: number; pageSize: number }) => {
  const { current, pageSize } = props;

  return (
    <>
      {getCards(current, pageSize).map((title, i) => (
        <Item key={`${title}:${i}`}>{title}</Item>
      ))}
    </>
  );
};

const LoadingCards = (props: { current: number; pageSize: number }) => {
  const { current, pageSize } = props;
  const [cards, setCards] = useState<string[]>();

  useEffect(() => {
    setCards(undefined);

    const unsubscribe = setTimeout(() => {
      setCards(getCards(current, pageSize));
    }, 1000 + Math.random() * 3000);

    return () => {
      clearTimeout(unsubscribe);
    };
  }, [current, pageSize]);

  return (
    <>
      {!cards &&
        Array.from({ length: pageSize }).map((_, index) => (
          <GallerySkeletonItem
            key={index}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          />
        ))}
      {!!cards &&
        cards.map((title, i) => (
          <Item
            key={`${title}:${i}`}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          >
            {title}
          </Item>
        ))}
    </>
  );
};

const pageSize = 4;
const totalPages = Math.ceil(dummyPhrases.length / pageSize);

const Column = styled(CenteredColumn)`
  padding-top: 1rem;
  padding-bottom: 1rem;
  position: relative;
`;

function GridFixtureWithSkeleton() {
  const [current, setCurrent] = useState(() => ({
    page: 1,
    direction: "none" as "L" | "R" | "none",
  }));

  return (
    <AppThemeProvider>
      <GlobalStyle />
      <Column>
        <BasicGalleryGrid
          pageNumber={current.page}
          swipeDirection={current.direction}
        >
          <SkeletonCards current={current.page} pageSize={pageSize} />
        </BasicGalleryGrid>
        <GalleryPager
          currentPage={current.page}
          totalPageCount={totalPages}
          onPrevious={() =>
            setCurrent((c) => ({
              page: c.page - 1,
              direction: "L",
            }))
          }
          onNext={() =>
            setCurrent((c) => ({
              page: c.page + 1,
              direction: "R",
            }))
          }
          onPagingCommit={() => undefined}
        >
          <button>Previous</button>
          <GalleryPagerInfo />
          <button>Next</button>
        </GalleryPager>
      </Column>
    </AppThemeProvider>
  );
}

function GridFixture() {
  const [current, setCurrent] = useState(() => ({
    page: 1,
    direction: "none" as "L" | "R" | "none",
  }));

  return (
    <AppThemeProvider>
      <GlobalStyle />
      <Column>
        <BasicGalleryGrid
          pageNumber={current.page}
          swipeDirection={current.direction}
        >
          <Cards current={current.page} pageSize={pageSize} />
        </BasicGalleryGrid>
        <GalleryPager
          currentPage={current.page}
          totalPageCount={totalPages}
          onPrevious={() =>
            setCurrent((c) => ({
              page: c.page - 1,
              direction: "L",
            }))
          }
          onNext={() =>
            setCurrent((c) => ({
              page: c.page + 1,
              direction: "R",
            }))
          }
          onPagingCommit={() => undefined}
        >
          <button>Previous</button>
          <GalleryPagerInfo />
          <button>Next</button>
        </GalleryPager>
      </Column>
    </AppThemeProvider>
  );
}

function GridLoadingFixture() {
  const [current, setCurrent] = useState(() => ({
    page: 1,
    direction: "none" as "L" | "R" | "none",
  }));

  return (
    <AppThemeProvider>
      <GlobalStyle />
      <Column>
        <BasicGalleryGrid
          pageNumber={current.page}
          swipeDirection={current.direction}
        >
          <LoadingCards current={current.page} pageSize={pageSize} />
        </BasicGalleryGrid>
        <GalleryPager
          currentPage={current.page}
          totalPageCount={totalPages}
          onPrevious={() =>
            setCurrent((c) => ({
              page: c.page - 1,
              direction: "L",
            }))
          }
          onNext={() =>
            setCurrent((c) => ({
              page: c.page + 1,
              direction: "R",
            }))
          }
          onPagingCommit={() => undefined}
        >
          <button>Previous</button>
          <GalleryPagerInfo />
          <button>Next</button>
        </GalleryPager>
      </Column>
    </AppThemeProvider>
  );
}

export default {
  basicGalleryWithSkeletonItems: <GridFixtureWithSkeleton />,
  basicGalleryWithItems: <GridFixture />,
  basicGalleryWithLoadingItems: <GridLoadingFixture />,
};
