import React, {
  ButtonHTMLAttributes,
  cloneElement,
  ReactElement,
  useCallback,
  useEffect,
  useState,
} from "react";
import styled from "styled-components";
import { useOnClickA11y } from "../../utils/events";
import { noop } from "../../utils/noop";
import { Undefined } from "../../utils/typescript";

const debounce = 200;

interface GalleryPagerCallbacks {
  totalPageCount: number;
  /**
   * The current page
   */
  currentPage: number;
  /**
   * Always called whenever the user presses the next button
   */
  onNext: () => void;
  /**
   * Always called whenever the user presses the previous button
   */
  onPrevious: () => void;
  /**
   * After pressing next/previous, this will eventually be called. But
   * note that this call is _debounced_.
   */
  onPagingCommit: (page: number) => void;
}

type PrevButton = ReactElement<ButtonHTMLAttributes<any>>;
type NextButton = ReactElement<ButtonHTMLAttributes<any>>;
/**
 * The component responsible for rendering "current / total"
 */
type PagerInfoComponent = ReactElement<{ current?: number; total?: number }>;

export type GalleryPagerOpts = {
  /**
   * This is just so styled-components can style this component
   */
  className?: string;
  /**
   * You can pass an optional PagerInfoComponent. This component is
   * responsible for rendering current / total counts.
   */
  children:
    | [PrevButton, NextButton]
    | [PrevButton, PagerInfoComponent, NextButton];
} & (GalleryPagerCallbacks | Undefined<GalleryPagerCallbacks>);

const StyledGalleryPagerInfo = styled.div``;

export function GalleryPagerInfo(props: {
  className?: string;
  total?: number;
  current?: number;
}) {
  return (
    <StyledGalleryPagerInfo className={props.className}>
      {props.current} / {props.total}
    </StyledGalleryPagerInfo>
  );
}

const StyledGalleryPager = styled.div``;

function extractChildComponents(
  children: GalleryPagerOpts["children"]
): [PrevButton, PagerInfoComponent | undefined, NextButton] {
  const [prevButton, pager, nextButton] = children;
  let actualPager: PagerInfoComponent | undefined;
  let actualNextButton: NextButton;

  if (nextButton) {
    actualPager = pager as PagerInfoComponent;
    actualNextButton = nextButton;
  } else {
    actualNextButton = pager as NextButton;
  }

  return [prevButton, actualPager, actualNextButton];
}

/**
 * The GalleryPager is responsible for:
 *
 * - Rendering the next/previous buttons that move the GalleryGrid to
 *   the next/previous page.
 * - Rendering an optional GalleryPagerInfo component which renders
 *   current/total counts.
 *
 * ## How It Works
 *
 * - Pressing the prev/next buttons always trigger the onPrevious/onNext
 *   handlers
 * - But onPagingCommit (which reports the final navigated page) is
 *   debounced
 * - This will prevent things like thrashing the UI
 */
export function GalleryPager(props: GalleryPagerOpts) {
  const {
    currentPage: source = 0,
    totalPageCount = 0,
    onNext = noop,
    onPrevious = noop,
    onPagingCommit = noop,
    children,
  } = props;

  const [prevButton, pager, nextButton] = extractChildComponents(children);
  const [current, setCurrent] = useState(source);

  useEffect(
    function deriveStateFromProps() {
      setCurrent(source);
    },
    [source]
  );

  useEffect(
    function update() {
      if (current === source) {
        return;
      }

      const unsubscribe = setTimeout(() => {
        onPagingCommit(current);
      }, debounce);

      return () => {
        clearTimeout(unsubscribe);
      };
    },
    [current, onPagingCommit, source]
  );

  const next = useCallback(() => {
    onNext();
    setCurrent((c) => Math.min(totalPageCount, c + 1));
  }, [onNext, totalPageCount]);

  const prev = useCallback(() => {
    onPrevious();
    setCurrent((c) => Math.max(0, c - 1));
  }, [onPrevious]);

  return (
    <StyledGalleryPager className={props.className}>
      {cloneElement(prevButton, {
        disabled: current === 1,
        ...useOnClickA11y(prev),
      })}
      {pager && cloneElement(pager, { current, total: totalPageCount })}
      {cloneElement(nextButton, {
        disabled: current === totalPageCount,
        ...useOnClickA11y(next),
      })}
    </StyledGalleryPager>
  );
}
