import React, { useState } from "react";
import styled from "styled-components";
import { GlobalStyle } from "../../fixtures/GlobalStyle";
import { AppThemeProvider } from "../../styles/theme";
import { CenteredColumn } from "../Text/Grid";
import { GalleryPager, GalleryPagerInfo } from "./GalleryPager";

const Column = styled(CenteredColumn)`
  padding-top: 1rem;
  padding-bottom: 1rem;
`;

const Button = styled.button``;

const Pager = styled(GalleryPagerInfo)`
  font-size: 3em;
`;

function PagerFixture() {
  const [current, setCurrent] = useState(1);

  return (
    <AppThemeProvider>
      <GlobalStyle />
      <Column>
        <GalleryPager
          currentPage={current}
          totalPageCount={20}
          onPrevious={() => undefined}
          onNext={() => undefined}
          onPagingCommit={setCurrent}
        >
          <Button>Previous</Button>
          <Pager />
          <Button>Next</Button>
        </GalleryPager>
        <div>Current page: {current}</div>
      </Column>
    </AppThemeProvider>
  );
}

export default {
  pager: <PagerFixture />,
};
