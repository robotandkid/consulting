import { m } from "framer-motion";
import React, {
  Children,
  cloneElement,
  ReactElement,
  ReactNode,
  useMemo,
  useState,
} from "react";
import styled, { css } from "styled-components";
import { simpleShimmer } from "../../styles/keyframes";
import { BasicGalleryGrid, BasicGalleryGridOpts } from "./BasicGalleryGrid";
import { GalleryPagerOpts } from "./GalleryPager";

type GalleryItems = ReactNode[];
type GalleryPager = ReactElement<
  Partial<GalleryPagerOpts> & Pick<GalleryPagerOpts, "children">
>;
type GalleryGrid = ReactElement<
  BasicGalleryGridOpts["children"] & {
    children: GalleryItems;
  }
>;

export type GalleryGridOpts = {
  children: [GalleryPager, GalleryGrid];
  pageSize: number;
} & Partial<Pick<GalleryPagerOpts, "onNext" | "onPrevious" | "onPagingCommit">>;

const itemAsGridItem = css`
  grid-column-start: auto;
  grid-column-end: auto;
  grid-row-start: auto;
  grid-row-end: auto;
`;

export const suggestedGalleryItemTypography = css`
  padding: 1rem;
  text-align: center;
  line-height: 1.2;

  font-size: 1.2rem;
  @media only screen and (min-width: 768px) {
    font-size: 1.5rem;
  }
  @media only screen and (min-width: 1920px) {
    font-size: 2.5rem;
  }
`;

const itemAsContainer = css`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  overflow: hidden;
`;

export const GalleryItem = styled(m.div)`
  ${itemAsGridItem}
  ${itemAsContainer}
`;

export const GallerySkeletonItem = styled(m.div)`
  ${itemAsGridItem}
  ${itemAsContainer}

  animation: 1s linear ${simpleShimmer} infinite;
`;

/**
 * Note that this drops the first item (since it's the pager)
 */
function getItemsForPage(
  rawItems: ReactNode[],
  current: number,
  pageSize: number
): [number, ReactNode[]] {
  const page: ReactNode[] = [];
  const items = Children.toArray(rawItems);
  const start = (current - 1) * pageSize;
  const end = Math.min(items.length, current * pageSize);

  for (let index = start; index < end; index++) {
    page.push(items[index]);
  }

  const totalPages = Math.ceil(items.length / pageSize);

  return [totalPages, page];
}

/**
 * Splitting this functionality since we can probably harden the
 * memoization
 */
function useMemoizedGetItemsForPage(
  rawItems: ReactNode[],
  current: number,
  pageSize: number
): [number, ReactNode[]] {
  return useMemo(() => {
    return getItemsForPage(rawItems, current, pageSize);
  }, [current, pageSize, rawItems]);
}

/**
 * - This component assumes that you pass the _entire_ list of items to
 *   render
 * - (Hence, the name "in memory")
 * - It will automatically paginate this list based on the pageSize
 * - You must pass in an instance of the GalleryPager as the first
 *   child.
 * - TODO Pagination is tracked in the given query param
 */
export function InMemoryGalleryGrid(
  props: GalleryGridOpts & { className?: string }
) {
  const {
    children: [pager, gridWithItems],
    pageSize,
    onNext,
    onPrevious,
    onPagingCommit,
  } = props;
  const [current, setCurrent] = useState(() => ({
    page: 1,
    direction: "none" as "L" | "R" | "none",
  }));
  const [totalPages, items] = useMemoizedGetItemsForPage(
    gridWithItems?.props?.children,
    current.page,
    pageSize
  );

  return (
    <>
      <BasicGalleryGrid
        pageNumber={current.page}
        swipeDirection={current.direction}
      >
        {cloneElement<any>(gridWithItems, undefined, items)}
      </BasicGalleryGrid>
      {totalPages > 1 &&
        cloneElement(pager, {
          currentPage: current.page,
          totalPageCount: totalPages,
          onPrevious() {
            onPrevious?.();
            setCurrent((c) => ({
              page: c.page - 1,
              direction: "L",
            }));
          },
          onNext() {
            onNext?.();
            setCurrent((c) => ({
              page: c.page + 1,
              direction: "R",
            }));
          },
          onPagingCommit,
        })}
    </>
  );
}
