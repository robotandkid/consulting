import { AnimatePresence, m } from "framer-motion";
import React, {
  cloneElement,
  forwardRef,
  ReactElement,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import styled, { css } from "styled-components";
import { media } from "../../styles/media";

type GalleryGridWithItems = ReactElement<
  Parameters<typeof DefaultBasicGalleryGrid>[0]
>;
export interface BasicGalleryGridOpts {
  children: GalleryGridWithItems;
  pageNumber: number;
  swipeDirection: "none" | "L" | "R";
  className?: string;
}

const GalleryGridSwipeContainer = styled(m.div)<{
  height: number;
}>`
  height: ${(p) => p.height}px;
  // Grid height can change. For example, the last page can have less items than
  // previous pages.
  transition: height 0.5s cubic-bezier(0.2, -0.58, 0.65, 1.81);
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const defaultBasicGalleryGrid = css`
  padding: 1rem;
  width: 100%;
  justify-items: stretch;
  align-items: stretch;
  display: grid;

  grid-template-columns: 100%;
  grid-template-rows: max(12rem, 25vh);
  grid-auto-rows: max(12rem, 25vh);

  column-gap: 1rem;
  row-gap: 1rem;

  ${media("mobileL")(css`
    grid-template-columns: 50% 50%;
    grid-template-rows: max(22rem, 50vh);
    grid-auto-rows: max(22rem, 50vh);
  `)}

  ${media("tablet")(css`
    grid-template-columns: 24% 24% 24% 24%;
    grid-template-rows: max(20rem, 25vh);
    grid-auto-rows: max(20rem, 25vh);

    width: 90%;
  `)}
`;

/**
 * Pass this as the sizes property to a next/image used inside a
 * gallery grid.
 */
export const nextImageSizes = `
  (max-width: 600px) 100vh,
  (max-width: 768px) 50vh,
  25vh
`;

export const DefaultBasicGalleryGrid = styled(m.div)<{
  $positionAbsolute?: boolean;
}>`
  ${(p) =>
    !!p.$positionAbsolute &&
    css`
      position: absolute;
    `}
`;

/**
 * There is most definitely a framer-motion quirk here:
 *
 * - For some reason, AnimatePresence does not allow the
 *   StyledGalleryGrid, which is positioned absolutely, to size itself
 *   properly!
 *
 * And so whenever the StyledGalleryGrid gets mounted, we render it
 * twice:
 *
 * - The first time we render it w/o AnimatePresence
 * - This allows it to calculate it's height
 * - Then we inject it's height into the second render---this time with
 *   AnimatePresence
 */
const InitialRender = forwardRef<
  HTMLDivElement,
  Pick<BasicGalleryGridOpts, "children">
>((props, ref) => {
  const { children } = props;

  return cloneElement(children, { ref, $positionAbsolute: false });
});

const SecondRender = (
  props: BasicGalleryGridOpts & { injectedHeight: number }
) => {
  const ref = useRef<HTMLElement>(null);
  const {
    pageNumber,
    children: galleryGridWithItems,
    swipeDirection,
    className,
    injectedHeight,
  } = props;
  const [height, setHeight] = useState(injectedHeight);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useLayoutEffect(() => {
    // const ref = typeof refOrFunction !== "function" ? refOrFunction : undefined;

    if (ref.current?.offsetHeight) {
      setHeight(ref.current.offsetHeight);
    }

    // so we need to update the height when it changes but there's no onresize
    // event for DIVs. The standard fallback is to use a timer. Alternatively,
    // there's this:
    // http://www.backalleycoder.com/2013/03/18/cross-browser-event-based-element-resize-detection/
    const unsubscribe = setInterval(() => {
      if (ref.current?.offsetHeight) {
        setHeight(ref.current.offsetHeight);
      }
    }, 400);

    return () => {
      clearInterval(unsubscribe);
    };
  });

  return (
    <GalleryGridSwipeContainer className={className} height={height}>
      <AnimatePresence initial={false}>
        {cloneElement(galleryGridWithItems, {
          ref,
          key: `${pageNumber}`,
          $positionAbsolute: true,
          initial:
            swipeDirection === "R"
              ? {
                  x: "-100vw",
                }
              : { x: "100vw" },
          animate: { x: 0 },
          exit: swipeDirection === "L" ? { x: "100vw" } : { x: "-100vw" },
        })}
      </AnimatePresence>
    </GalleryGridSwipeContainer>
  );
};

/**
 * **Note** you must pass a child element that accepts a
 * positionAbsolute property _and_ positions itself absolutely when
 * true.
 *
 * This is a basic component. You most likely do not want to use it
 * directly.
 *
 * - It's literally a grid responsible for the swipe animation.
 * - It renders the passed-in GalleryItems in a grid.
 * - (The GallerySkeletonItem can be used for the same purpose but to
 *   represent loading state.)
 *
 * **Note:** it has no pagination whatsoever
 */
export function BasicGalleryGrid(props: BasicGalleryGridOpts) {
  const gridRef = useRef<HTMLDivElement>(null);
  const height = useRef(0);
  const hasHeight = height.current > 0;

  useEffect(() => {
    if (gridRef.current?.offsetHeight) {
      height.current = gridRef.current?.offsetHeight;
    }
  });

  return (
    <>
      {!hasHeight && (
        <InitialRender ref={gridRef}>{props.children}</InitialRender>
      )}
      {hasHeight && (
        <SecondRender
          {...props}
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          injectedHeight={height.current}
        />
      )}
    </>
  );
}
