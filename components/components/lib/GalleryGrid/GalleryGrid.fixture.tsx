import React from "react";
import styled from "styled-components";
import { dummyPhrases, dummyWords } from "../../fixtures/dummyText";
import { GlobalStyle } from "../../fixtures/GlobalStyle";
import { AppThemeProvider } from "../../styles/theme";
import { CenteredColumn } from "../Text/Grid";
import {
  defaultBasicGalleryGrid,
  DefaultBasicGalleryGrid,
} from "./BasicGalleryGrid";
import {
  GalleryItem,
  InMemoryGalleryGrid,
  suggestedGalleryItemTypography,
} from "./GalleryGrid";
import { GalleryPager, GalleryPagerInfo } from "./GalleryPager";

const Item = styled(GalleryItem)`
  ${suggestedGalleryItemTypography}
  font-family: "mrpixel";
  font-weight: bold;
  background: #c35;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;

const Column = styled(CenteredColumn)`
  padding-top: 1rem;
  padding-bottom: 1rem;
  position: relative;
`;

const BasicGrid = styled(DefaultBasicGalleryGrid)`
  ${defaultBasicGalleryGrid}
`;

export function GalleryWithItems() {
  return (
    <AppThemeProvider>
      <GlobalStyle />
      <Column>
        <InMemoryGalleryGrid pageSize={4}>
          <GalleryPager>
            <button>Previous</button>
            <GalleryPagerInfo />
            <button>Next</button>
          </GalleryPager>
          <BasicGrid>
            {dummyWords.map((title, i) => (
              <Item key={`${title}:${i}`}>{title}</Item>
            ))}
          </BasicGrid>
        </InMemoryGalleryGrid>
      </Column>
    </AppThemeProvider>
  );
}

export function GalleryWithPhrases() {
  return (
    <AppThemeProvider>
      <GlobalStyle />
      <Column>
        <InMemoryGalleryGrid pageSize={4}>
          <GalleryPager>
            <button>Previous</button>
            <GalleryPagerInfo />
            <button>Next</button>
          </GalleryPager>
          <DefaultBasicGalleryGrid>
            {dummyPhrases.map((title, i) => (
              <Item key={`${title}:${i}`}>{title}</Item>
            ))}
          </DefaultBasicGalleryGrid>
        </InMemoryGalleryGrid>
      </Column>
    </AppThemeProvider>
  );
}
