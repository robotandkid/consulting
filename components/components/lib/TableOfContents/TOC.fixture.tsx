import { setTOCItem, useTOC } from "./TOC";
import React from "react";

setTOCItem({ href: "#second", name: "second", index: 1 });
setTOCItem({ href: "#first", name: "first", index: 0 });

export const TOCStory = () => {
  const TOC = useTOC();

  return (
    <>
      {TOC.map((item) => (
        <div key={item.href}>{JSON.stringify(item)}</div>
      ))}
    </>
  );
};
