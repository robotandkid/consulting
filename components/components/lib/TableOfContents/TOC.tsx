import { useMemo } from "react";
import {
  createModuleState,
  useModuleState,
} from "../../utils/microStateManagement";

export interface StaticTOCItem {
  /**
   * Must be globally unique
   */
  href: `#${string}`;
  name: string;
  index: number;
}

const tocState = createModuleState([] as ReadonlyArray<StaticTOCItem>);

export const useTOC = () => {
  const [TOC] = useModuleState(tocState);

  return useMemo(() => {
    return TOC.map((item) => item).sort((a, b) => {
      return a.index < b.index ? -1 : 1;
    });
  }, [TOC]);
};

export const setTOCItem = (newItem: StaticTOCItem) => {
  if (
    tocState
      .getState()
      .find(
        (item) => item.href === newItem.href && item.index !== newItem.index
      )
  ) {
    throw new Error(`Duplicate TOC item found ${JSON.stringify(newItem)}`);
  } else if (
    tocState
      .getState()
      .find(
        (item) => item.href === newItem.href && item.index === newItem.index
      )
  ) {
    // Most likely a nextJS quirk---it
    console.warn(`Duplicate TOC item found ${JSON.stringify(newItem)}`);
  }

  tocState.setState([...tocState.getState(), newItem]);
};

export const resetTOC = () => {
  tocState.setState([]);
};
