import { m } from "framer-motion";
import NextLink from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useRef } from "react";
import styled from "styled-components";

import { normalizedAnchor } from "../../../styles/styles";
import { useOnClickA11y } from "../../../utils/events";

function stripHrefHashtag(href: string | undefined) {
  return href?.charAt(0) === "#" ? href?.substring(1) : undefined;
}

interface InternalLinkWrapper {
  /**
   * Required for a11y
   */
  tabIndex?: number;
  /**
   * Required for a11y
   */
  onKeyUp: (e: React.KeyboardEvent<HTMLAnchorElement>) => void;
  href?: string;
  children?: React.ReactNode;
}

/**
 * - A simple Link component that injects the `href` into the URL as
 *   `[queryParamName]=href` when clicked.
 * - (The query param name is customizable.)
 * - Can be used in conjunction with the LinkTarget to automatically
 *   scroll the app to matching LinkTarget.
 *
 * **Note:** For a11y, you need to pass a child component that
 * implements tabIndex and onKeyUp, that is, it must be tab-able and
 * support the keyUp handler.
 */
export function InternalLink(props: {
  href: `#${string}`;
  queryParamName?: string;
  children: React.ReactElement<InternalLinkWrapper>;
}) {
  const { href, queryParamName = "link", children } = props;
  const target = stripHrefHashtag(href);
  const { route, push } = useRouter() || {};

  const { onKeyDown: onKeyUp } = useOnClickA11y<HTMLAnchorElement>((e) => {
    push({ pathname: route, query: { [queryParamName]: target } }, undefined, {
      shallow: true,
    });
    e?.preventDefault();
  });

  return (
    <NextLink href={`/?${queryParamName}=${target}`} shallow>
      {React.cloneElement(children, {
        tabIndex: 0,
        onKeyUp,
        href: href,
      })}
    </NextLink>
  );
}

interface LinkWrapper {
  /**
   * Needed in order to scroll to target
   */
  ref?: React.MutableRefObject<HTMLElement | null | undefined>;
  /**
   * Needed for a11y
   */
  tabIndex?: number;
  children?: React.ReactNode;
}

/**
 * Listens to changes in the URL:
 *
 * - When the specified query parameter (`queryParamName`) equals the
 *   `href`, the app will scroll to this component and focus the
 *   component. OR
 * - When the route equals the `href', the app will scroll to this
 *   component and focus the component.
 */
export const LinkTarget = (props: {
  href: `#${string}`;
  queryParamName?: string;
  scrollToTop?: boolean;
  children: React.ReactElement<LinkWrapper>;
}) => {
  const { href, queryParamName = "link", children, scrollToTop } = props;
  const { query = {}, replace, route } = useRouter() || {};
  const ref = useRef<HTMLElement | null>();

  useEffect(
    function scrollToTargetOnQueryParamChange() {
      const target = stripHrefHashtag(href);

      const shouldScroll =
        query[queryParamName] === target || route === `/${target}`;

      if (shouldScroll && scrollToTop) {
        window.scrollTo({
          top: 0,
          left: 0,
          // not supported by IE or Safari
          behavior: "smooth",
        });
      } else if (shouldScroll) {
        window.scrollBy({
          top: ref.current?.getBoundingClientRect().top,
          left: 0,
          // not supported by IE or Safari
          behavior: "smooth",
        });
      }

      let unsubscribe: ReturnType<typeof setTimeout> | undefined;
      let unsubscribe2: ReturnType<typeof setTimeout> | undefined;

      if (shouldScroll) {
        unsubscribe = setTimeout(function focusAfterScroll() {
          // If it wasn't for Safari (and IE), we could just pass
          // `preventScroll: false` and call it a day, that is,
          // we could remove the scrollBy above
          ref.current?.focus();
        }, 1000) as any;

        unsubscribe2 = setTimeout(function removeLinkForReselection() {
          delete query[queryParamName];
          replace(route, query, { shallow: true });
        }, 1000);

        return () => {
          unsubscribe && clearTimeout(unsubscribe);
          unsubscribe2 && clearTimeout(unsubscribe2);
        };
      }
    },
    [href, query, queryParamName, replace, route, scrollToTop]
  );

  return (
    <>
      {React.cloneElement(children, {
        ref,
        tabIndex: 0,
      })}
    </>
  );
};

const StyledExternalLink = styled(m.a)`
  ${normalizedAnchor}
`;

export function ExternalLink(props: Parameters<typeof StyledExternalLink>[0]) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { target, tabIndex, ...args } = props;

  return <StyledExternalLink tabIndex={0} {...args} target="_blank" />;
}
