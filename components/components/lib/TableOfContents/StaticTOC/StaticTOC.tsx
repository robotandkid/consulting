import React, { useContext, useMemo, useRef } from "react";
import { renderToString } from "react-dom/server";
import { Unpromisify } from "../../../utils/typescript";

export interface StaticTOCItem {
  /**
   * Must be globally unique
   */
  href: `#${string}`;
  name: string;
  index: number;
}

interface IStaticTOCContext {
  items: ReadonlyArray<StaticTOCItem>;
  setItem(item: StaticTOCItem): void;
}

export const StaticTOCContext = React.createContext<
  Pick<IStaticTOCContext, "items">
>({
  items: [],
});

const SetterContext = React.createContext<Pick<IStaticTOCContext, "setItem">>({
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setItem() {},
});

/**
 * Use this to create a static TOC. Under the hood, the TOC will be
 * set exactly once---during the initial render by the SetStaticTOC
 * component.
 *
 * - Use SetStaticTOC in components to set TOC items
 * - Use getStaticProps for SSR-initial-data population via
 *   react-dom/renderToString.
 *
 * **Notes:**
 *
 * 1. As it's name implies, this provider does _not_ support a
 *    dynamically-generated TOC.
 * 2. Do not use __setItemsForSSR - this is used internally to set the
 *    items in SSR.
 */
export const StaticTOCProvider = (props: {
  initialItemsFromSSR?: StaticTOCItem[];
  __setItemsForSSR?: (items: StaticTOCItem[]) => void;
  children?: React.ReactNode;
}) => {
  const items = useRef(props.initialItemsFromSSR || []);

  const { __setItemsForSSR } = props;

  const update = useMemo(
    () => ({
      setItem(item: StaticTOCItem) {
        if (!items.current.find((x) => x.href === item.href)) {
          items.current.push(item);
          items.current.sort((a, b) => a.index - b.index);
          __setItemsForSSR?.(items.current);
        }
      },
    }),
    [__setItemsForSSR]
  );

  const context = useMemo(() => ({ items: items.current }), []);

  return (
    <SetterContext.Provider value={update}>
      <StaticTOCContext.Provider value={context}>
        {props.children}
      </StaticTOCContext.Provider>
    </SetterContext.Provider>
  );
};

/**
 * As it's name implies, calls setItem during the initial render, so
 * can be used by renderToString to get the initial values.
 */
class SetDuringInitialRender extends React.Component<
  {
    setItem: IStaticTOCContext["setItem"];
    children: StaticTOCItem;
  },
  unknown
> {
  constructor(props: {
    setItem: IStaticTOCContext["setItem"];
    children: StaticTOCItem;
  }) {
    super(props);

    props.setItem(props.children);
  }

  render() {
    return null;
  }
}

/**
 * Use this to create a TOC item during initial render.
 *
 * Note: can be safely set exactly once. As it's name implies, dynamic
 * TOC items are _not_ supported.
 *
 * Exists so that a component can set the TOC without triggering an
 * unnecessary render on itself.
 */
export function SetStaticTOC(props: StaticTOCItem) {
  const { setItem } = useContext(SetterContext);

  return (
    <SetDuringInitialRender setItem={setItem}>{props}</SetDuringInitialRender>
  );
}

/**
 * Use this to inject the TOC from SSR.
 */
export const getStaticPropsStaticTOC = (Component: JSX.Element) => async () => {
  let initial: StaticTOCItem[] = [];

  const setItems = (items: StaticTOCItem[]) => {
    initial = [...items];
  };

  const app = () => (
    <StaticTOCProvider __setItemsForSSR={setItems}>
      {Component}
    </StaticTOCProvider>
  );

  renderToString(app());

  return {
    props: {
      initialStaticTOC: initial,
    },
  };
};

export type GetStaticPropsStaticTOC = Unpromisify<
  ReturnType<ReturnType<typeof getStaticPropsStaticTOC>>
>["props"];

/**
 * - Convenience helper for generating dynamic routes from the TOC.
 * - For example, suppose your website has `pages/[link].js` and TOC
 *   items: "home" and "about".
 * - Then `getStaticPathsForStaticTOC("link", <StaticTOCComponent/>)`
 *   will create dynamic paths for `/home` and `/about`.
 */
export const getStaticPathsForStaticTOC = async (
  dynamicRouteName: string,
  StaticTOCComponent: JSX.Element
) => {
  const getStaticProps = getStaticPropsStaticTOC(StaticTOCComponent);

  const {
    props: { initialStaticTOC },
  } = await getStaticProps();

  return {
    paths: initialStaticTOC.map((item) => ({
      params: { [dynamicRouteName]: item.href.substring(1) },
    })),
  };
};
