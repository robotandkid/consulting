import styled, { css } from "styled-components";

export const text = css`
  font-size: 1.5rem;
  margin-top: 1em;
  margin-bottom: 1em;
  text-align: justify;

  &:first-child {
    margin-top: 0em;
  }

  &:last-child {
    margin-bottom: 0em;
  }

  font-family: inherit;
  color: inherit;
`;

export const Text = styled.p`
  ${text}
`;
