import styled, { css } from "styled-components";

import { media } from "../../styles/media";

/**
 * On mobile, the scrollbar gets in the way, so you need to use this
 * to size it correctly.
 */
export const centeredColumn = css`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  padding: 0;

  width: 90vw;

  ${media("tablet")(css`
    width: 100%;
  `)}
`;

/**
 * Not sure what this does 🤷
 */
export const responsiveCenteredColumn = css`
  ${centeredColumn}

  ${media("tablet")(css`
    width: 100%;
    flex-direction: row;
    align-items: flex-start;
  `)}
`;

export const CenteredColumn = styled.div`
  ${centeredColumn}
`;

export const ResponsiveCenteredColumn = styled.div`
  ${responsiveCenteredColumn}
`;
