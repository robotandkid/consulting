import styled, { css } from "styled-components";

import { media } from "../../styles/media";

const sharedTitleStyle = css`
  background: transparent;
  letter-spacing: -0.12rem;
  line-height: 1;
  font-family: inherit;
  color: inherit;
`;

export const Title = styled.h1`
  text-align: center;
  font-size: 5rem;
  width: min(40rem, 95vw);
  position: absolute;
  mix-blend-mode: color-dodge;
  font-weight: bold;
  letter-spacing: -0.05em;
  margin: 0;
  padding: 0;

  ${media("tablet")(css`
    font-size: 10rem;
  `)}
`;

export const H1 = styled.h1`
  ${sharedTitleStyle}
  font-weight: normal;
  font-size: 4rem;
  padding: 0;
  margin: 0;
  margin-top: 1em;
  margin-bottom: 0.5em;
  background: transparent;

  ${media("tablet")(css`
    font-size: 8rem;
    margin-top: 0;
  `)}
`;

export const H2 = styled.h2`
  ${sharedTitleStyle}
  font-weight: bold;
  font-size: 3rem;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;
  margin-top: 0.5em;

  ${media("tablet")(css`
    font-size: 6rem;
  `)}
`;

export const H3 = styled.h3`
  ${sharedTitleStyle}
  font-weight: bold;
  font-size: 2rem;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;

  ${media("tablet")(css`
    font-size: 4rem;
  `)}
`;
