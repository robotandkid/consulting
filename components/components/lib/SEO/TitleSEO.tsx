import React from "react";
import Head from "next/head";

type Title = string;
type Description = string;

export function TitleSEO(props: { children: [Title, Description] }) {
  const {
    children: [title, description],
  } = props;

  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
    </Head>
  );
}
