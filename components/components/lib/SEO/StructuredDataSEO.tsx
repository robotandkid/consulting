import React from "react";
import Head from "next/head";

export function StructuredDataSEO(props: {
  structuredData: Record<string, any>;
}) {
  return (
    <Head>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(props.structuredData, null, " "),
        }}
      ></script>
    </Head>
  );
}
