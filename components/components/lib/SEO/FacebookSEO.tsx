import Head from "next/head";
import React from "react";

export function FacebookSEO(props: {
  url: string;
  type: "website" | "article";
  title: string;
  description: string;
  imageUrl: string;
  imageWidthPx: number;
  imageHeightPx: number;
  alternateLocale?: "es_US";
}) {
  return (
    <Head>
      <meta property="og:url" content={props.url} />
      <meta property="og:type" content={props.type} />
      <meta property="og:title" content={props.title} />
      <meta property="og:description" content={props.description} />
      <meta property="og:image" content={props.imageUrl} />
      <meta property="og:image:width" content={`${props.imageWidthPx}`} />
      <meta property="og:image:height" content={`${props.imageHeightPx}`} />
      {props.alternateLocale && (
        <meta property="og:locale:alternate" content={props.alternateLocale} />
      )}
    </Head>
  );
}
