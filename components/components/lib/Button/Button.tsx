import styled, { css } from "styled-components";

import { text } from "../Text/Text";

export const buttonFilledHover = css`
  transition: opacity 0.1s;

  &:hover {
    opacity: 0.86;
  }

  &:focus {
    opacity: 0.76;
  }

  &:active {
    opacity: 0.68;
  }
`;

/**
 * @deprecated  This does too much.
 *
 *   This is a link that looks like a button. If i'm not mistaken, it's
 *   needed because it's used for the call-button icon...and this
 *   looks like a button but needs a link's a href to trigger the call
 *   action. In other words, it's a button that behaves like a link.
 *   🤷
 *
 *   Make sure to set the color and background-color.
 *
 *   Further reading:
 *   https://m3.material.io/components/buttons/guidelines
 */
export const LinkButtonFilled = styled.a`
  ${text}

  text-decoration: none;
  padding: 0.5em;
  margin-top: 1rem;
  transition: opacity 0.1s;
  box-shadow: 0.2em 0.2em 0.5em 1px black;
  transition: background 0.1s, opacity 0.1s;

  ${buttonFilledHover}
`;

/**
 * This is a link that looks like a button. If i'm not mistaken, it's
 * needed because it's used for the call-button icon...and this looks
 * like a button but needs a link's a href to trigger the call action.
 * In other words, it's a button that behaves like a link. 🤷
 *
 * Make sure to set the color and border-color.
 *
 * Optional: background-color
 *
 * Further reading:
 * https://m3.material.io/components/buttons/guidelines
 */
export const LinkButtonOutline = styled(LinkButtonFilled)`
  border: 1px solid black;
  background-color: white;
  box-shadow: none;
  transition: background 0.1s, opacity 0.1s;

  &:hover {
    opacity: 1;
    background-color: hsl(0, 0%, 96%);
  }

  &:focus {
    opacity: 1;
    background-color: hsl(0, 0%, 88%);
  }

  &:active {
    opacity: 1;
    background-color: hsl(0, 0%, 88%);
  }
`;
