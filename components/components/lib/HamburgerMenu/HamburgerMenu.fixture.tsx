import { List, XCircle } from "phosphor-react";
import React, { useContext } from "react";
import styled from "styled-components";
import { largeText } from "../../fixtures/dummyText";
import {
  BodyUnderHamburger,
  HamburgerMenu,
  HamburgerMenuContext,
  HamburgerMenuContextProvider,
  HamburgerMenuItem,
} from "./HamburgerMenu";

const MenuButtonIcon = styled(List)`
  width: 3.5em;
  height: 3.5em;
  outline: none;
  border: none;
`;

const XButtonIcon = styled(XCircle)`
  width: 3.5em;
  height: 3.5em;
  outline: none;
  border: none;
`;

const StyledItem = styled(HamburgerMenuItem)`
  font-family: sans-serif;
  font-size: 3rem;
  margin-top: 1rem;
  padding-left: 1rem;
  display: block;
`;

function Item(props: { children: string }) {
  return <StyledItem>{props.children}</StyledItem>;
}

const items = [
  "Jelly beans",
  "brownie candy canes",
  "biscuit",
  "I love dragée lollipopfoobar",
  "Lets go",
  "awgwar",
];

const MenuState = () => {
  const { open } = useContext(HamburgerMenuContext);
  return <div>State: {`${open}`}</div>;
};

const Container = styled.div`
  margin-top: 5rem;
  margin-right: 5rem;
`;

const Fixture = () => {
  return (
    <HamburgerMenu bgColor="#fefedd">
      <MenuButtonIcon key="menu" />
      <XButtonIcon key="x" />
      {items.map((item) => (
        <Item key={item}>{item}</Item>
      ))}
    </HamburgerMenu>
  );
};

export const BasicStory = () => {
  return (
    <HamburgerMenuContextProvider>
      <Fixture />
    </HamburgerMenuContextProvider>
  );
};

const LargeBody = () => (
  <BodyUnderHamburger>
    <Container>
      <MenuState />
      <div>{largeText}</div>
    </Container>
  </BodyUnderHamburger>
);

export const WithFixedBackground = () => {
  return (
    <HamburgerMenuContextProvider>
      <Fixture />
      <LargeBody />
    </HamburgerMenuContextProvider>
  );
};
