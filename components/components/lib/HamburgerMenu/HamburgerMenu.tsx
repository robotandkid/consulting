import { AnimatePresence, m } from "framer-motion";
import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import styled, { css } from "styled-components";
import { media } from "../../styles/media";
import { NormalizedAnchor } from "../../styles/styles";
import { defaultTheme as theme } from "../../styles/theme";
import { useOnClickA11y } from "../../utils/events";
import { HamburgerMenuButton } from "./HamburgerMenuButton";

const Container = styled.div`
  position: relative;
`;

const defaultTheme = theme.components.menu;

type Color = `#${string}` | `rgba(${string}`;

const StyledMenuContents = styled(m.nav)<{ $bgColor?: Color }>`
  position: fixed;
  z-index: 2;
  display: flex;
  flex-direction: column;

  /* styles */
  background: ${(p) =>
    p.$bgColor ??
    p.theme.components?.menu?.backgroundColor ??
    defaultTheme.backgroundColor};

  top: 5rem;
  bottom: 0;
  width: 100%;

  ${media("mobileL")(css`
    width: min(60vw, 27rem);
  `)}

  overflow-y: scroll;

  // https://www.w3schools.com/howto/howto_css_hide_scrollbars.asp
  &::-webkit-scrollbar {
    display: none;
  }
  -ms-overflow-style: none;
  scrollbar-width: none;
`;

export const HamburgerMenuContext = createContext<{ open: boolean }>({
  open: false,
});

export const HamburgerMenuToggleContext = createContext<{
  toggle: () => void;
}>({} as any);

/**
 * This is needed to pass state to items. See HamburgerMenu for
 * details.
 */
export function HamburgerMenuContextProvider(props: { children?: ReactNode }) {
  const [open, setOpen] = useState(false);
  const prevOpenTime = useRef(0);

  const toggleValue = useMemo(() => {
    return {
      toggle() {
        const now = Date.now();

        if (now - prevOpenTime.current > 400) {
          prevOpenTime.current = now;
          setOpen((_open) => !_open);
        }
      },
    };
  }, []);

  return (
    <HamburgerMenuToggleContext.Provider value={toggleValue}>
      <HamburgerMenuContext.Provider value={{ open }}>
        {props.children}
      </HamburgerMenuContext.Provider>
    </HamburgerMenuToggleContext.Provider>
  );
}

type HamburgerMenuItems = ReactNode[];

function SideMenuContents(props: {
  children: HamburgerMenuItems;
  bgColor?: Color;
}) {
  const { children: items } = props;
  const { open } = useContext(HamburgerMenuContext);
  const elem = useRef<HTMLDivElement | null>(null);

  return (
    <AnimatePresence initial={false}>
      {open && (
        <StyledMenuContents
          ref={elem}
          key="menu"
          aria-label="Main menu"
          role="navigation"
          initial={{
            x: "-100vw",
          }}
          animate={{
            x: 0,
            transition: { staggerChildren: 0.07, delayChildren: 0.2 },
          }}
          exit={{
            x: "-100vw",
            transition: { staggerChildren: 0.05, staggerDirection: -1 },
          }}
          $bgColor={props.bgColor}
        >
          {items}
        </StyledMenuContents>
      )}
    </AnimatePresence>
  );
}

type ClosedStateIcon = ReactNode;
type OpenStateIcon = ReactNode;

/**
 * Usage:
 *
 * 1. Child elements:
 *
 *    - The first child is the icon shown when the menu is closed.
 *    - The second child is the icon shown when the menu us open.
 *    - The remaining elements are the menu items.
 * 2. Wrap the HamburgerMenu with the HamburgerMenuContextProvider
 * 3. Change the background color of the menu items by passing a bgColor
 *    prop or setting `theme.components.menu.backgroundColor` (in the
 *    styled-components theme).
 * 4. You should most likely should use the provided HamburgerMenuItem to
 *    style clickable menu items. This component does the "right
 *    thing" with respect to a11y. It's just an anchor under the
 *    hood.
 * 5. Wrap the main content with BodyUnderHamburger. This is to freeze
 *    the content in place under the hamburger while it is open. The
 *    hamburger must be placed in front of the BodyUnderHamburger.
 * 6. (Optional) Apply `overflow:hidden` to the `body` when the menu is
 *    open. See https://stackoverflow.com/a/45230674. This is so that
 *    on IOS devices, the menu can be scrolled w/o also scrolling the
 *    body. Use useIsoMenuScrollWorkaround.
 *
 * Note:
 *
 * 1. The *StateIcon components are rendered inside a button component.
 * 2. While the *StateIcon components are usually icons, they can be any
 *    React component.
 * 3. Anything that wishes to toggle the menu state must use the same
 *    Context.
 */
export function HamburgerMenu(props: {
  children: [ClosedStateIcon, OpenStateIcon, ...HamburgerMenuItems];
  bgColor?: Color;
}) {
  const { children } = props;
  const [closedButton, openButton, ...items] = children;

  const { open } = useContext(HamburgerMenuContext);
  const { toggle } = useContext(HamburgerMenuToggleContext);

  useEffect(
    function closeOnBlur() {
      if (open) {
        window.addEventListener("click", toggle);
      }

      return () => {
        window.removeEventListener("click", toggle);
      };
    },
    [open, toggle]
  );

  return (
    <Container>
      <HamburgerMenuButton openState={open ? "open" : "closed"} toggle={toggle}>
        {closedButton}
        {openButton}
      </HamburgerMenuButton>
      <SideMenuContents bgColor={props.bgColor}>{items}</SideMenuContents>
    </Container>
  );
}

const StyledHamburgerMenuItem = styled(NormalizedAnchor)``;

export function HamburgerMenuItem(
  props: Parameters<typeof StyledHamburgerMenuItem>[0]
) {
  const { onClick, ...rest } = props;
  const { toggle } = useContext(HamburgerMenuToggleContext);

  return (
    <m.div whileHover={{ scale: 1.05 }} whileTap={{ scale: 0.95 }}>
      <StyledHamburgerMenuItem
        {...rest}
        {...useOnClickA11y((e) => {
          toggle();
          onClick?.(e);
        })}
      />
    </m.div>
  );
}

const Body = styled.div<{
  scrollY: number | undefined;
}>`
  width: 100%;
  transition: opacity 0.5s;
  opacity: 1;

  ${(p) =>
    p.scrollY !== undefined &&
    css`
      position: fixed;
      top: -${p.scrollY}px;
      opacity: 0.5;
    `}
`;

/**
 * Prior art:
 * https://css-tricks.com/prevent-page-scrolling-when-a-modal-is-open/
 */
export const BodyUnderHamburger = (props: {
  children: ReactNode;
  /**
   * A fudge factor
   */
  offsetY?: number;
}) => {
  const { open } = useContext(HamburgerMenuContext);
  const positionOnOpen = useRef<number>();

  const opening = open && positionOnOpen.current === undefined;
  const closing = !open && positionOnOpen.current !== undefined;

  if (opening) positionOnOpen.current = window.scrollY + (props.offsetY ?? 0);
  else if (closing) {
    const scrollPosition = positionOnOpen.current ?? 0;
    // reset positionOnOpen to reset the Body
    positionOnOpen.current = undefined;
    // but delay the scroll restore to give time for the Body to reset
    setTimeout(() => globalThis.scrollTo?.(0, scrollPosition));
  }

  return (
    <Body scrollY={positionOnOpen.current} aria-hidden={open}>
      {props.children}
    </Body>
  );
};

/**
 * @deprecated  ???
 */
export const UseIosMenuScrollWorkaround = () => {
  const { open } = useContext(HamburgerMenuContext);
  const prevPosition = useRef<string>();
  const prevOverflow = useRef<string>();

  useEffect(() => {
    if (open) {
      prevPosition.current = window.document.body.style.position;
      window.document.body.style.position = "fixed";
      prevOverflow.current = window.document.body.style.overflow;
      window.document.body.style.overflow = "hidden";
    } else {
      if (prevPosition.current)
        window.document.body.style.position = prevPosition.current;
      if (prevOverflow.current)
        window.document.body.style.overflow = prevOverflow.current;
    }
  }, [open]);

  return null;
};
