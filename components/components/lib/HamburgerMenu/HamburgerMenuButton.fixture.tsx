import { List, XCircle } from "phosphor-react";
import React, { useCallback, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { HamburgerMenuButton } from "./HamburgerMenuButton";

const MenuButtonIcon = styled(List)`
  width: 3.5em;
  height: 3.5em;
  outline: none;
  border: none;

  &:active,
  &:focus-visible {
    border: 1pt solid blue;
  }
`;

const XButtonIcon = styled(XCircle)`
  width: 3.5em;
  height: 3.5em;
  outline: none;
  border: none;

  &:active,
  &:focus-visible {
    border: 1pt solid blue;
  }
`;

const Container = styled.div``;

export function BasicStory() {
  const [open, setOpen] = useState(false);
  const prevOpenTime = useRef(0);

  const debouncedToggle = useCallback(() => {
    const now = Date.now();

    if (now - prevOpenTime.current > 400) {
      prevOpenTime.current = now;
      setOpen((_open) => !_open);
    }
  }, []);

  useEffect(
    function closeOnBlur() {
      if (open) {
        window.addEventListener("click", debouncedToggle);
      }

      return () => {
        window.removeEventListener("click", debouncedToggle);
      };
    },
    [open, debouncedToggle]
  );

  return (
    <>
      <div style={{ position: "relative", height: "5rem" }}>
        <HamburgerMenuButton
          openState={open ? "open" : "closed"}
          toggle={debouncedToggle}
        >
          <MenuButtonIcon weight="bold" />
          <XButtonIcon weight="bold" />
        </HamburgerMenuButton>
      </div>
      <Container>
        <div>Note: this story is *NOT* accessible</div>
        <button type="button">Button to test onBlur</button>
      </Container>
    </>
  );
}
