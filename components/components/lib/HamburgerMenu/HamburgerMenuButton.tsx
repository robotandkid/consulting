import { AnimatePresence, m } from "framer-motion";
import React, { ReactNode } from "react";
import styled from "styled-components";
import { onClickA11y } from "../../utils/events";

import { zIndexNavigation } from "../../utils/zIndex";

const ButtonContainer = styled.div`
  position: fixed;
  top: 1rem;
  left: 0.5rem;
  z-index: ${zIndexNavigation};
`;

const Button = styled(m.button)`
  outline: none;
  border: none;
  background: transparent;
`;

/**
 * Usage:
 *
 * - The first child element is the button to show when the menu is
 *   closed
 * - The second child element is the button to show when the menu is
 *   open
 */
export function HamburgerMenuButton(props: {
  children: [ReactNode, ReactNode];
  openState: "open" | "closed";
  toggle: () => void;
}) {
  const { openState, toggle } = props;

  return (
    <ButtonContainer>
      <AnimatePresence mode="popLayout">
        <Button
          tabIndex={0}
          {...onClickA11y(toggle)}
          aria-label={
            openState === "open" ? "Close main menu" : "Open main menu"
          }
          aria-haspopup
          aria-expanded={openState === "open"}
          key={openState}
          initial={{ opacity: 0, transformOrigin: "center" }}
          animate={{ opacity: 1, transition: { duration: 2 } }}
          exit={{
            opacity: 0,
            rotate: openState === "open" ? -360 : 360,
            scale: 1,
            transition: { duration: 1 },
          }}
          whileHover={{
            scale: 1.5,
            transition: { ease: [0.15, 1.98, 0.39, 1.99] },
          }}
          whileFocus={{
            scale: 1.5,
            transition: { ease: [0.15, 1.98, 0.39, 1.99] },
          }}
        >
          {openState === "closed" && props.children[0]}
          {openState === "open" && props.children[1]}
        </Button>
      </AnimatePresence>
    </ButtonContainer>
  );
}
