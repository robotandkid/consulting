import React from "react";
import Image, { ImageLoader } from "next/image";

const baseUrl = "https://ik.imagekit.io/bxpwztngs0o/";

const app = ["leanderlawn", "leandertile", "masonrypro"] as const;

type App = typeof app[number];
type Src = `/${string}`;

const imageKitUrl = (app: App, src: Src) => `${baseUrl}${app}${src}`;

type ImageProps = Parameters<typeof Image>[0];

const imageKitLoader =
  (app: App) =>
  ({ src, width, quality }: { src: Src; width: number; quality?: number }) => {
    return `${imageKitUrl(app, src)}?tr=w-${width}&tr=q-${quality ?? 80}`;
  };

const imageKitBlurUrl = (app: App, src: Src) =>
  `${imageKitUrl(app, src)}?tr=bl-10`;

export function ImageKit(
  props: Omit<
    Parameters<typeof Image>[0],
    "loader" | "placeholder" | "blurDataURL" | "src"
  > & {
    app: App;
    useBlur?: boolean;
    src: Src;
  }
) {
  const { app, useBlur, ...rest } = props;
  const blur = useBlur
    ? ({
        placeholder: "blur",
        blurDataURL: `${imageKitBlurUrl(props.app, props.src)}`,
      } as Pick<ImageProps, "placeholder" | "blurDataURL">)
    : null;

  return (
    <Image {...rest} {...blur} loader={imageKitLoader(app) as ImageLoader} />
  );
}
