import {
  useReducedMotion,
  useSpring,
  useTransform,
  useScroll,
} from "framer-motion";
import React, {
  cloneElement,
  ReactElement,
  useEffect,
  useRef,
  useState,
} from "react";

type ParallaxProps = {
  children: ReactElement;
  offset?: number;
};

export const Parallax = ({
  children,
  offset = 50,
}: ParallaxProps): JSX.Element => {
  const prefersReducedMotion = useReducedMotion();
  const [elementTop, setElementTop] = useState(0);
  const [clientHeight, setClientHeight] = useState(0);
  const ref = useRef<HTMLDivElement>(null);

  const initial = elementTop - clientHeight;
  const final = elementTop + offset;

  const { scrollY } = useScroll();

  const yRange = useTransform(scrollY, [initial, final], [offset, -offset]);
  const y = useSpring(yRange, { stiffness: 400, damping: 90 });

  useEffect(() => {
    const element = ref.current;
    const onResize = () => {
      setElementTop(
        (element?.getBoundingClientRect()?.top || 0) + window.scrollY ||
          window.pageYOffset
      );
      setClientHeight(window.innerHeight);
    };
    onResize();
    window.addEventListener("resize", onResize);
    return () => window.removeEventListener("resize", onResize);
  }, [ref]);

  // Don't parallax if the user has "reduced motion" enabled
  if (prefersReducedMotion) {
    return <>{children}</>;
  }

  return cloneElement(children, { ref, style: { y } });
};
