import React from "react";
import Image from "next/image";
declare const app: readonly ["leanderlawn", "leandertile", "masonrypro"];
type App = (typeof app)[number];
type Src = `/${string}`;
export declare function ImageKit(props: Omit<Parameters<typeof Image>[0], "loader" | "placeholder" | "blurDataURL" | "src"> & {
    app: App;
    useBlur?: boolean;
    src: Src;
}): React.JSX.Element;
export {};
