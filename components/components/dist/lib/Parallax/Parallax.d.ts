import { ReactElement } from "react";
type ParallaxProps = {
    children: ReactElement;
    offset?: number;
};
export declare const Parallax: ({ children, offset, }: ParallaxProps) => JSX.Element;
export {};
