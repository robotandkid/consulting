export interface StaticTOCItem {
    /**
     * Must be globally unique
     */
    href: `#${string}`;
    name: string;
    index: number;
}
export declare const useTOC: () => StaticTOCItem[];
export declare const setTOCItem: (newItem: StaticTOCItem) => void;
export declare const resetTOC: () => void;
