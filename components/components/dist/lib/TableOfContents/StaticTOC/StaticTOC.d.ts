import React from "react";
import { Unpromisify } from "../../../utils/typescript";
export interface StaticTOCItem {
    /**
     * Must be globally unique
     */
    href: `#${string}`;
    name: string;
    index: number;
}
interface IStaticTOCContext {
    items: ReadonlyArray<StaticTOCItem>;
    setItem(item: StaticTOCItem): void;
}
export declare const StaticTOCContext: React.Context<Pick<IStaticTOCContext, "items">>;
/**
 * Use this to create a static TOC. Under the hood, the TOC will be
 * set exactly once---during the initial render by the SetStaticTOC
 * component.
 *
 * - Use SetStaticTOC in components to set TOC items
 * - Use getStaticProps for SSR-initial-data population via
 *   react-dom/renderToString.
 *
 * **Notes:**
 *
 * 1. As it's name implies, this provider does _not_ support a
 *    dynamically-generated TOC.
 * 2. Do not use __setItemsForSSR - this is used internally to set the
 *    items in SSR.
 */
export declare const StaticTOCProvider: (props: {
    initialItemsFromSSR?: StaticTOCItem[] | undefined;
    __setItemsForSSR?: ((items: StaticTOCItem[]) => void) | undefined;
    children?: React.ReactNode;
}) => React.JSX.Element;
/**
 * Use this to create a TOC item during initial render.
 *
 * Note: can be safely set exactly once. As it's name implies, dynamic
 * TOC items are _not_ supported.
 *
 * Exists so that a component can set the TOC without triggering an
 * unnecessary render on itself.
 */
export declare function SetStaticTOC(props: StaticTOCItem): React.JSX.Element;
/**
 * Use this to inject the TOC from SSR.
 */
export declare const getStaticPropsStaticTOC: (Component: JSX.Element) => () => Promise<{
    props: {
        initialStaticTOC: StaticTOCItem[];
    };
}>;
export type GetStaticPropsStaticTOC = Unpromisify<ReturnType<ReturnType<typeof getStaticPropsStaticTOC>>>["props"];
/**
 * - Convenience helper for generating dynamic routes from the TOC.
 * - For example, suppose your website has `pages/[link].js` and TOC
 *   items: "home" and "about".
 * - Then `getStaticPathsForStaticTOC("link", <StaticTOCComponent/>)`
 *   will create dynamic paths for `/home` and `/about`.
 */
export declare const getStaticPathsForStaticTOC: (dynamicRouteName: string, StaticTOCComponent: JSX.Element) => Promise<{
    paths: {
        params: {
            [x: string]: string;
        };
    }[];
}>;
export {};
