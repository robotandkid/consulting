import React from "react";
interface InternalLinkWrapper {
    /**
     * Required for a11y
     */
    tabIndex?: number;
    /**
     * Required for a11y
     */
    onKeyUp: (e: React.KeyboardEvent<HTMLAnchorElement>) => void;
    href?: string;
    children?: React.ReactNode;
}
/**
 * - A simple Link component that injects the `href` into the URL as
 *   `[queryParamName]=href` when clicked.
 * - (The query param name is customizable.)
 * - Can be used in conjunction with the LinkTarget to automatically
 *   scroll the app to matching LinkTarget.
 *
 * **Note:** For a11y, you need to pass a child component that
 * implements tabIndex and onKeyUp, that is, it must be tab-able and
 * support the keyUp handler.
 */
export declare function InternalLink(props: {
    href: `#${string}`;
    queryParamName?: string;
    children: React.ReactElement<InternalLinkWrapper>;
}): React.JSX.Element;
interface LinkWrapper {
    /**
     * Needed in order to scroll to target
     */
    ref?: React.MutableRefObject<HTMLElement | null | undefined>;
    /**
     * Needed for a11y
     */
    tabIndex?: number;
    children?: React.ReactNode;
}
/**
 * Listens to changes in the URL:
 *
 * - When the specified query parameter (`queryParamName`) equals the
 *   `href`, the app will scroll to this component and focus the
 *   component. OR
 * - When the route equals the `href', the app will scroll to this
 *   component and focus the component.
 */
export declare const LinkTarget: (props: {
    href: `#${string}`;
    queryParamName?: string;
    scrollToTop?: boolean;
    children: React.ReactElement<LinkWrapper>;
}) => React.JSX.Element;
declare const StyledExternalLink: import("styled-components").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLAnchorElement, import("framer-motion").HTMLMotionProps<"a">>, import("styled-components").DefaultTheme, {}, never>;
export declare function ExternalLink(props: Parameters<typeof StyledExternalLink>[0]): React.JSX.Element;
export {};
