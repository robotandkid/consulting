import React, { ReactElement } from "react";
export declare const LandingHeroContainer: import("styled-components").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLDivElement, import("framer-motion").HTMLMotionProps<"div">>, import("styled-components").DefaultTheme, {}, never>;
/**
 * Use this as the basis for the ReducedHero variant.
 *
 * Defaults to position fixed in the top-right.
 */
export declare const navigationLandingHeroTitle: import("styled-components").FlattenSimpleInterpolation;
export declare const NavigationLandingHeroTitle: import("styled-components").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLDivElement, import("framer-motion").HTMLMotionProps<"div">>, import("styled-components").DefaultTheme, {}, never>;
export declare const useDetectLandingScrolled: () => boolean;
/**
 * When users call this method, it will be permanently rendered at a
 * smaller size. Call #cancelShrinkLandingHero to cancel.
 */
export declare const shrinkLandingHero: () => void;
/**
 * Note: this won't automatically grow the landing hero if it's
 * scrolled away.
 */
export declare const cancelShrinkLandingHero: () => void;
type LargeElementProps = {
    layoutId?: string;
};
type SmallElementProps = {
    layoutId?: string;
};
/**
 * Usage:
 *
 * - The first child element is the title, large version. You must use
 *   the provided FullHero component as the basis for your component.
 * - The second child element is the title, small version. You must use
 *   the provided ReducedHero component as the basis for your
 *   component.
 * - To use in non-landing pages, set forceReducedHero to true
 */
export declare function LandingHero(props: {
    disabled?: boolean;
    children: [ReactElement<LargeElementProps>, ReactElement<SmallElementProps>];
}): React.JSX.Element;
export {};
