import React, { ReactNode } from "react";
/**
 * Usage:
 *
 * - The first child element is the button to show when the menu is
 *   closed
 * - The second child element is the button to show when the menu is
 *   open
 */
export declare function HamburgerMenuButton(props: {
    children: [ReactNode, ReactNode];
    openState: "open" | "closed";
    toggle: () => void;
}): React.JSX.Element;
