import React, { ReactNode } from "react";
type Color = `#${string}` | `rgba(${string}`;
export declare const HamburgerMenuContext: React.Context<{
    open: boolean;
}>;
export declare const HamburgerMenuToggleContext: React.Context<{
    toggle: () => void;
}>;
/**
 * This is needed to pass state to items. See HamburgerMenu for
 * details.
 */
export declare function HamburgerMenuContextProvider(props: {
    children?: ReactNode;
}): React.JSX.Element;
type HamburgerMenuItems = ReactNode[];
type ClosedStateIcon = ReactNode;
type OpenStateIcon = ReactNode;
/**
 * Usage:
 *
 * 1. Child elements:
 *
 *    - The first child is the icon shown when the menu is closed.
 *    - The second child is the icon shown when the menu us open.
 *    - The remaining elements are the menu items.
 * 2. Wrap the HamburgerMenu with the HamburgerMenuContextProvider
 * 3. Change the background color of the menu items by passing a bgColor
 *    prop or setting `theme.components.menu.backgroundColor` (in the
 *    styled-components theme).
 * 4. You should most likely should use the provided HamburgerMenuItem to
 *    style clickable menu items. This component does the "right
 *    thing" with respect to a11y. It's just an anchor under the
 *    hood.
 * 5. Wrap the main content with BodyUnderHamburger. This is to freeze
 *    the content in place under the hamburger while it is open. The
 *    hamburger must be placed in front of the BodyUnderHamburger.
 * 6. (Optional) Apply `overflow:hidden` to the `body` when the menu is
 *    open. See https://stackoverflow.com/a/45230674. This is so that
 *    on IOS devices, the menu can be scrolled w/o also scrolling the
 *    body. Use useIsoMenuScrollWorkaround.
 *
 * Note:
 *
 * 1. The *StateIcon components are rendered inside a button component.
 * 2. While the *StateIcon components are usually icons, they can be any
 *    React component.
 * 3. Anything that wishes to toggle the menu state must use the same
 *    Context.
 */
export declare function HamburgerMenu(props: {
    children: [ClosedStateIcon, OpenStateIcon, ...HamburgerMenuItems];
    bgColor?: Color;
}): React.JSX.Element;
declare const StyledHamburgerMenuItem: import("styled-components").StyledComponent<"a", import("styled-components").DefaultTheme, {}, never>;
export declare function HamburgerMenuItem(props: Parameters<typeof StyledHamburgerMenuItem>[0]): React.JSX.Element;
/**
 * Prior art:
 * https://css-tricks.com/prevent-page-scrolling-when-a-modal-is-open/
 */
export declare const BodyUnderHamburger: (props: {
    children: ReactNode;
    /**
     * A fudge factor
     */
    offsetY?: number;
}) => React.JSX.Element;
/**
 * @deprecated  ???
 */
export declare const UseIosMenuScrollWorkaround: () => null;
export {};
