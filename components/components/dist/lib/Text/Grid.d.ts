/**
 * On mobile, the scrollbar gets in the way, so you need to use this
 * to size it correctly.
 */
export declare const centeredColumn: import("styled-components").FlattenInterpolation<import("styled-components").ThemeProps<import("styled-components").DefaultTheme>>;
/**
 * Not sure what this does 🤷
 */
export declare const responsiveCenteredColumn: import("styled-components").FlattenInterpolation<import("styled-components").ThemeProps<import("styled-components").DefaultTheme>>;
export declare const CenteredColumn: import("styled-components").StyledComponent<"div", import("styled-components").DefaultTheme, {}, never>;
export declare const ResponsiveCenteredColumn: import("styled-components").StyledComponent<"div", import("styled-components").DefaultTheme, {}, never>;
