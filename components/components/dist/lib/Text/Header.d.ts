export declare const Title: import("styled-components").StyledComponent<"h1", import("styled-components").DefaultTheme, {}, never>;
export declare const H1: import("styled-components").StyledComponent<"h1", import("styled-components").DefaultTheme, {}, never>;
export declare const H2: import("styled-components").StyledComponent<"h2", import("styled-components").DefaultTheme, {}, never>;
export declare const H3: import("styled-components").StyledComponent<"h3", import("styled-components").DefaultTheme, {}, never>;
