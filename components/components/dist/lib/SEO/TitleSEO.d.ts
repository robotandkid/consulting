import React from "react";
type Title = string;
type Description = string;
export declare function TitleSEO(props: {
    children: [Title, Description];
}): React.JSX.Element;
export {};
