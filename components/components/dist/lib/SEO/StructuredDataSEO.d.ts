import React from "react";
export declare function StructuredDataSEO(props: {
    structuredData: Record<string, any>;
}): React.JSX.Element;
