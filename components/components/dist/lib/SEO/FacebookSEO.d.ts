import React from "react";
export declare function FacebookSEO(props: {
    url: string;
    type: "website" | "article";
    title: string;
    description: string;
    imageUrl: string;
    imageWidthPx: number;
    imageHeightPx: number;
    alternateLocale?: "es_US";
}): React.JSX.Element;
