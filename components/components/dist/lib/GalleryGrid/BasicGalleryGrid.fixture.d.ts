import React from "react";
declare const _default: {
    basicGalleryWithSkeletonItems: React.JSX.Element;
    basicGalleryWithItems: React.JSX.Element;
    basicGalleryWithLoadingItems: React.JSX.Element;
};
export default _default;
