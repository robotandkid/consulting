import React, { ReactElement, ReactNode } from "react";
import { BasicGalleryGridOpts } from "./BasicGalleryGrid";
import { GalleryPagerOpts } from "./GalleryPager";
type GalleryItems = ReactNode[];
type GalleryPager = ReactElement<Partial<GalleryPagerOpts> & Pick<GalleryPagerOpts, "children">>;
type GalleryGrid = ReactElement<BasicGalleryGridOpts["children"] & {
    children: GalleryItems;
}>;
export type GalleryGridOpts = {
    children: [GalleryPager, GalleryGrid];
    pageSize: number;
} & Partial<Pick<GalleryPagerOpts, "onNext" | "onPrevious" | "onPagingCommit">>;
export declare const suggestedGalleryItemTypography: import("styled-components").FlattenSimpleInterpolation;
export declare const GalleryItem: import("styled-components").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLDivElement, import("framer-motion").HTMLMotionProps<"div">>, import("styled-components").DefaultTheme, {}, never>;
export declare const GallerySkeletonItem: import("styled-components").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLDivElement, import("framer-motion").HTMLMotionProps<"div">>, import("styled-components").DefaultTheme, {}, never>;
/**
 * - This component assumes that you pass the _entire_ list of items to
 *   render
 * - (Hence, the name "in memory")
 * - It will automatically paginate this list based on the pageSize
 * - You must pass in an instance of the GalleryPager as the first
 *   child.
 * - TODO Pagination is tracked in the given query param
 */
export declare function InMemoryGalleryGrid(props: GalleryGridOpts & {
    className?: string;
}): React.JSX.Element;
export {};
