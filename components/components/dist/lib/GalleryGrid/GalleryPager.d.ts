import React, { ButtonHTMLAttributes, ReactElement } from "react";
import { Undefined } from "../../utils/typescript";
interface GalleryPagerCallbacks {
    totalPageCount: number;
    /**
     * The current page
     */
    currentPage: number;
    /**
     * Always called whenever the user presses the next button
     */
    onNext: () => void;
    /**
     * Always called whenever the user presses the previous button
     */
    onPrevious: () => void;
    /**
     * After pressing next/previous, this will eventually be called. But
     * note that this call is _debounced_.
     */
    onPagingCommit: (page: number) => void;
}
type PrevButton = ReactElement<ButtonHTMLAttributes<any>>;
type NextButton = ReactElement<ButtonHTMLAttributes<any>>;
/**
 * The component responsible for rendering "current / total"
 */
type PagerInfoComponent = ReactElement<{
    current?: number;
    total?: number;
}>;
export type GalleryPagerOpts = {
    /**
     * This is just so styled-components can style this component
     */
    className?: string;
    /**
     * You can pass an optional PagerInfoComponent. This component is
     * responsible for rendering current / total counts.
     */
    children: [PrevButton, NextButton] | [PrevButton, PagerInfoComponent, NextButton];
} & (GalleryPagerCallbacks | Undefined<GalleryPagerCallbacks>);
export declare function GalleryPagerInfo(props: {
    className?: string;
    total?: number;
    current?: number;
}): React.JSX.Element;
/**
 * The GalleryPager is responsible for:
 *
 * - Rendering the next/previous buttons that move the GalleryGrid to
 *   the next/previous page.
 * - Rendering an optional GalleryPagerInfo component which renders
 *   current/total counts.
 *
 * ## How It Works
 *
 * - Pressing the prev/next buttons always trigger the onPrevious/onNext
 *   handlers
 * - But onPagingCommit (which reports the final navigated page) is
 *   debounced
 * - This will prevent things like thrashing the UI
 */
export declare function GalleryPager(props: GalleryPagerOpts): React.JSX.Element;
export {};
