import React, { ReactElement } from "react";
type GalleryGridWithItems = ReactElement<Parameters<typeof DefaultBasicGalleryGrid>[0]>;
export interface BasicGalleryGridOpts {
    children: GalleryGridWithItems;
    pageNumber: number;
    swipeDirection: "none" | "L" | "R";
    className?: string;
}
export declare const defaultBasicGalleryGrid: import("styled-components").FlattenInterpolation<import("styled-components").ThemeProps<import("styled-components").DefaultTheme>>;
/**
 * Pass this as the sizes property to a next/image used inside a
 * gallery grid.
 */
export declare const nextImageSizes = "\n  (max-width: 600px) 100vh,\n  (max-width: 768px) 50vh,\n  25vh\n";
export declare const DefaultBasicGalleryGrid: import("styled-components").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLDivElement, import("framer-motion").HTMLMotionProps<"div">>, import("styled-components").DefaultTheme, {
    $positionAbsolute?: boolean | undefined;
}, never>;
/**
 * **Note** you must pass a child element that accepts a
 * positionAbsolute property _and_ positions itself absolutely when
 * true.
 *
 * This is a basic component. You most likely do not want to use it
 * directly.
 *
 * - It's literally a grid responsible for the swipe animation.
 * - It renders the passed-in GalleryItems in a grid.
 * - (The GallerySkeletonItem can be used for the same purpose but to
 *   represent loading state.)
 *
 * **Note:** it has no pagination whatsoever
 */
export declare function BasicGalleryGrid(props: BasicGalleryGridOpts): React.JSX.Element;
export {};
