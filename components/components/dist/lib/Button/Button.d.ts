export declare const buttonFilledHover: import("styled-components").FlattenSimpleInterpolation;
/**
 * @deprecated  This does too much.
 *
 *   This is a link that looks like a button. If i'm not mistaken, it's
 *   needed because it's used for the call-button icon...and this
 *   looks like a button but needs a link's a href to trigger the call
 *   action. In other words, it's a button that behaves like a link.
 *   🤷
 *
 *   Make sure to set the color and background-color.
 *
 *   Further reading:
 *   https://m3.material.io/components/buttons/guidelines
 */
export declare const LinkButtonFilled: import("styled-components").StyledComponent<"a", import("styled-components").DefaultTheme, {}, never>;
/**
 * This is a link that looks like a button. If i'm not mistaken, it's
 * needed because it's used for the call-button icon...and this looks
 * like a button but needs a link's a href to trigger the call action.
 * In other words, it's a button that behaves like a link. 🤷
 *
 * Make sure to set the color and border-color.
 *
 * Optional: background-color
 *
 * Further reading:
 * https://m3.material.io/components/buttons/guidelines
 */
export declare const LinkButtonOutline: import("styled-components").StyledComponent<"a", import("styled-components").DefaultTheme, {}, never>;
