export declare function parametrizedLine<Point extends Record<string, any>>(start: Point, end: Point, keyOfX: keyof Point, keyOfY: keyof Point): (t: number) => {
    [x: string]: number;
};
