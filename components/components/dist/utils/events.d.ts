import React from "react";
/**
 * Returns onClick handler and it's corresponding onKeyDown handler
 * (for a11y).
 *
 * Use this when the hook version cannot be used.
 */
export declare function onClickA11y<Element = HTMLDivElement>(cb: (e?: React.KeyboardEvent<Element> | React.MouseEvent<Element>) => any): {
    onKeyDown(e: React.KeyboardEvent<Element>): void;
    onClick(e: React.MouseEvent<Element>): void;
};
/**
 * Returns onClick handler and it's corresponding onKeyDown handler
 * (for a11y).
 */
export declare function useOnClickA11y<Element = HTMLDivElement>(cb: (e?: React.KeyboardEvent<Element> | React.MouseEvent<Element>) => any): {
    onKeyDown(e: React.KeyboardEvent<Element>): void;
    onClick(e: React.MouseEvent<Element>): void;
};
