export declare function useFreezeValue<T>(valueToFreeze: T, freeze: boolean): T;
