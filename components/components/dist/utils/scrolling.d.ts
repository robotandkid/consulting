export declare const useDebouncedListener: <EventType extends keyof WindowEventMap>(eventType: EventType, listener: (event: WindowEventMap[EventType]) => void) => void;
/**
 * Returns true once users have scrolled past the given delta.
 */
export declare function useDetectScrolling({ deltaY }: {
    deltaY?: number;
}): boolean;
/**
 * Too expensive?
 */
export declare function useScrollingY(): number;
