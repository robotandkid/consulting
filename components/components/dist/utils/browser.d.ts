/**
 * @deprecated  Most likely not needed
 */
export declare const isSafari: boolean;
export declare function useViewportDims(): {
    viewportWidth: number;
    viewportHeight: number;
};
export declare const isSSR: () => boolean;
