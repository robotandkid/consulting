import { m, AnimatePresence, useReducedMotion, useScroll, useTransform, useSpring, LayoutGroup } from 'framer-motion';
import React, { useState, useEffect, forwardRef, cloneElement, useRef, useLayoutEffect, useMemo, Children, useCallback, useContext, createContext } from 'react';
import styled, { keyframes, css } from 'styled-components';
import Image from 'next/image';
import Head from 'next/head';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { renderToString } from 'react-dom/server';

keyframes `
  10%, 90% {
    transform: translateY(-0.1rem);
  }
  20%, 80% {
    transform: translateY(0.15rem);
  }
  30%, 50%, 70% {
    transform: translateY(-0.35rem);
  }

  40%, 60% {
    transform: translateY(0.35rem);
  }
`;
keyframes `
  0% {
    transform: translate(0);
  }
  20% {
    transform: translate(-5px, 5px);
  }
  40% {
    transform: translate(-5px, -5px);
  }
  60% {
    transform: translate(5px, 5px);
  }
  80% {
    transform: translate(5px, -5px);
  }
  to {
    transform: translate(0);
  }
`;
keyframes `
  from {
    transform: rotate(0) scale(1.5);
    opacity: 1;
  }
  to {
    transform: rotate(360deg) scale(1.5);
    opacity: 0;
  }
`;
keyframes `
  from {
    transform: rotate(0);
    opacity: 1;
  }
  to {
    transform: rotate(-360deg);
    opacity: 0;
  }
`;
keyframes `
  from { opacity: 0 }
  to { opacity: 1}
`;
keyframes `
  from { opacity: 1 }
  to { opacity: 0 }
`;
const simpleShimmer = keyframes `
 0% {
    background: #d2d2d2;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  } 
  
  30% {
    background: #bfbfbf;
    background-repeat: no-repeat;
    background-size: 100%;
  }

  70% {
    background: #e7e7e7;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  }

  100% {
    background: #d2d2d2;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  } 
`;

const size = {
    mobileL: 600,
    tablet: 768,
    laptop: 1920,
};
const media = (mediaSize) => (args) => {
    return css `
      @media only screen and (min-width: ${size[mediaSize]}px) {
        ${args}
      }
    `;
};
const getViewportWidth = () => typeof window !== "undefined" ? window.innerWidth : 0;
function useIsAtMostMediaSize(s, ssrValue = true) {
    const [isSize, setIsSize] = useState(ssrValue);
    useEffect(() => {
        function update() {
            setIsSize(getViewportWidth() < size[s]);
        }
        update();
        window.addEventListener("resize", update);
        return () => {
            window.removeEventListener("resize", update);
        };
    }, [s]);
    return isSize;
}

const GalleryGridSwipeContainer = styled(m.div) `
  height: ${(p) => p.height}px;
  // Grid height can change. For example, the last page can have less items than
  // previous pages.
  transition: height 0.5s cubic-bezier(0.2, -0.58, 0.65, 1.81);
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
const defaultBasicGalleryGrid = css `
  padding: 1rem;
  width: 100%;
  justify-items: stretch;
  align-items: stretch;
  display: grid;

  grid-template-columns: 100%;
  grid-template-rows: max(12rem, 25vh);
  grid-auto-rows: max(12rem, 25vh);

  column-gap: 1rem;
  row-gap: 1rem;

  ${media("mobileL")(css `
    grid-template-columns: 50% 50%;
    grid-template-rows: max(22rem, 50vh);
    grid-auto-rows: max(22rem, 50vh);
  `)}

  ${media("tablet")(css `
    grid-template-columns: 24% 24% 24% 24%;
    grid-template-rows: max(20rem, 25vh);
    grid-auto-rows: max(20rem, 25vh);

    width: 90%;
  `)}
`;
/**
 * Pass this as the sizes property to a next/image used inside a
 * gallery grid.
 */
const nextImageSizes = `
  (max-width: 600px) 100vh,
  (max-width: 768px) 50vh,
  25vh
`;
const DefaultBasicGalleryGrid = styled(m.div) `
  ${(p) => !!p.$positionAbsolute &&
    css `
      position: absolute;
    `}
`;
/**
 * There is most definitely a framer-motion quirk here:
 *
 * - For some reason, AnimatePresence does not allow the
 *   StyledGalleryGrid, which is positioned absolutely, to size itself
 *   properly!
 *
 * And so whenever the StyledGalleryGrid gets mounted, we render it
 * twice:
 *
 * - The first time we render it w/o AnimatePresence
 * - This allows it to calculate it's height
 * - Then we inject it's height into the second render---this time with
 *   AnimatePresence
 */
const InitialRender = forwardRef((props, ref) => {
    const { children } = props;
    return cloneElement(children, { ref, $positionAbsolute: false });
});
const SecondRender = (props) => {
    const ref = useRef(null);
    const { pageNumber, children: galleryGridWithItems, swipeDirection, className, injectedHeight, } = props;
    const [height, setHeight] = useState(injectedHeight);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useLayoutEffect(() => {
        // const ref = typeof refOrFunction !== "function" ? refOrFunction : undefined;
        if (ref.current?.offsetHeight) {
            setHeight(ref.current.offsetHeight);
        }
        // so we need to update the height when it changes but there's no onresize
        // event for DIVs. The standard fallback is to use a timer. Alternatively,
        // there's this:
        // http://www.backalleycoder.com/2013/03/18/cross-browser-event-based-element-resize-detection/
        const unsubscribe = setInterval(() => {
            if (ref.current?.offsetHeight) {
                setHeight(ref.current.offsetHeight);
            }
        }, 400);
        return () => {
            clearInterval(unsubscribe);
        };
    });
    return (React.createElement(GalleryGridSwipeContainer, { className: className, height: height },
        React.createElement(AnimatePresence, { initial: false }, cloneElement(galleryGridWithItems, {
            ref,
            key: `${pageNumber}`,
            $positionAbsolute: true,
            initial: swipeDirection === "R"
                ? {
                    x: "-100vw",
                }
                : { x: "100vw" },
            animate: { x: 0 },
            exit: swipeDirection === "L" ? { x: "100vw" } : { x: "-100vw" },
        }))));
};
/**
 * **Note** you must pass a child element that accepts a
 * positionAbsolute property _and_ positions itself absolutely when
 * true.
 *
 * This is a basic component. You most likely do not want to use it
 * directly.
 *
 * - It's literally a grid responsible for the swipe animation.
 * - It renders the passed-in GalleryItems in a grid.
 * - (The GallerySkeletonItem can be used for the same purpose but to
 *   represent loading state.)
 *
 * **Note:** it has no pagination whatsoever
 */
function BasicGalleryGrid(props) {
    const gridRef = useRef(null);
    const height = useRef(0);
    const hasHeight = height.current > 0;
    useEffect(() => {
        if (gridRef.current?.offsetHeight) {
            height.current = gridRef.current?.offsetHeight;
        }
    });
    return (React.createElement(React.Fragment, null,
        !hasHeight && (React.createElement(InitialRender, { ref: gridRef }, props.children)),
        hasHeight && (React.createElement(SecondRender, { ...props, 
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            injectedHeight: height.current }))));
}

const itemAsGridItem = css `
  grid-column-start: auto;
  grid-column-end: auto;
  grid-row-start: auto;
  grid-row-end: auto;
`;
const suggestedGalleryItemTypography = css `
  padding: 1rem;
  text-align: center;
  line-height: 1.2;

  font-size: 1.2rem;
  @media only screen and (min-width: 768px) {
    font-size: 1.5rem;
  }
  @media only screen and (min-width: 1920px) {
    font-size: 2.5rem;
  }
`;
const itemAsContainer = css `
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  overflow: hidden;
`;
const GalleryItem = styled(m.div) `
  ${itemAsGridItem}
  ${itemAsContainer}
`;
const GallerySkeletonItem = styled(m.div) `
  ${itemAsGridItem}
  ${itemAsContainer}

  animation: 1s linear ${simpleShimmer} infinite;
`;
/**
 * Note that this drops the first item (since it's the pager)
 */
function getItemsForPage(rawItems, current, pageSize) {
    const page = [];
    const items = Children.toArray(rawItems);
    const start = (current - 1) * pageSize;
    const end = Math.min(items.length, current * pageSize);
    for (let index = start; index < end; index++) {
        page.push(items[index]);
    }
    const totalPages = Math.ceil(items.length / pageSize);
    return [totalPages, page];
}
/**
 * Splitting this functionality since we can probably harden the
 * memoization
 */
function useMemoizedGetItemsForPage(rawItems, current, pageSize) {
    return useMemo(() => {
        return getItemsForPage(rawItems, current, pageSize);
    }, [current, pageSize, rawItems]);
}
/**
 * - This component assumes that you pass the _entire_ list of items to
 *   render
 * - (Hence, the name "in memory")
 * - It will automatically paginate this list based on the pageSize
 * - You must pass in an instance of the GalleryPager as the first
 *   child.
 * - TODO Pagination is tracked in the given query param
 */
function InMemoryGalleryGrid(props) {
    const { children: [pager, gridWithItems], pageSize, onNext, onPrevious, onPagingCommit, } = props;
    const [current, setCurrent] = useState(() => ({
        page: 1,
        direction: "none",
    }));
    const [totalPages, items] = useMemoizedGetItemsForPage(gridWithItems?.props?.children, current.page, pageSize);
    return (React.createElement(React.Fragment, null,
        React.createElement(BasicGalleryGrid, { pageNumber: current.page, swipeDirection: current.direction }, cloneElement(gridWithItems, undefined, items)),
        totalPages > 1 &&
            cloneElement(pager, {
                currentPage: current.page,
                totalPageCount: totalPages,
                onPrevious() {
                    onPrevious?.();
                    setCurrent((c) => ({
                        page: c.page - 1,
                        direction: "L",
                    }));
                },
                onNext() {
                    onNext?.();
                    setCurrent((c) => ({
                        page: c.page + 1,
                        direction: "R",
                    }));
                },
                onPagingCommit,
            })));
}

/**
 * Returns onClick handler and it's corresponding onKeyDown handler
 * (for a11y).
 *
 * Use this when the hook version cannot be used.
 */
function onClickA11y(cb) {
    return {
        onKeyDown(e) {
            if (e.key === " " || e.key === "Enter") {
                cb?.(e);
            }
        },
        onClick(e) {
            cb?.(e);
        },
    };
}
/**
 * Returns onClick handler and it's corresponding onKeyDown handler
 * (for a11y).
 */
function useOnClickA11y(cb) {
    const _cb = useRef(cb);
    useEffect(() => {
        _cb.current = cb;
    });
    return useMemo(() => ({
        onKeyDown(e) {
            if (e.key === " " || e.key === "Enter") {
                _cb.current?.(e);
            }
        },
        onClick(e) {
            _cb.current?.(e);
        },
    }), []);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
function noop() { }

const debounce = 200;
const StyledGalleryPagerInfo = styled.div ``;
function GalleryPagerInfo(props) {
    return (React.createElement(StyledGalleryPagerInfo, { className: props.className },
        props.current,
        " / ",
        props.total));
}
const StyledGalleryPager = styled.div ``;
function extractChildComponents(children) {
    const [prevButton, pager, nextButton] = children;
    let actualPager;
    let actualNextButton;
    if (nextButton) {
        actualPager = pager;
        actualNextButton = nextButton;
    }
    else {
        actualNextButton = pager;
    }
    return [prevButton, actualPager, actualNextButton];
}
/**
 * The GalleryPager is responsible for:
 *
 * - Rendering the next/previous buttons that move the GalleryGrid to
 *   the next/previous page.
 * - Rendering an optional GalleryPagerInfo component which renders
 *   current/total counts.
 *
 * ## How It Works
 *
 * - Pressing the prev/next buttons always trigger the onPrevious/onNext
 *   handlers
 * - But onPagingCommit (which reports the final navigated page) is
 *   debounced
 * - This will prevent things like thrashing the UI
 */
function GalleryPager(props) {
    const { currentPage: source = 0, totalPageCount = 0, onNext = noop, onPrevious = noop, onPagingCommit = noop, children, } = props;
    const [prevButton, pager, nextButton] = extractChildComponents(children);
    const [current, setCurrent] = useState(source);
    useEffect(function deriveStateFromProps() {
        setCurrent(source);
    }, [source]);
    useEffect(function update() {
        if (current === source) {
            return;
        }
        const unsubscribe = setTimeout(() => {
            onPagingCommit(current);
        }, debounce);
        return () => {
            clearTimeout(unsubscribe);
        };
    }, [current, onPagingCommit, source]);
    const next = useCallback(() => {
        onNext();
        setCurrent((c) => Math.min(totalPageCount, c + 1));
    }, [onNext, totalPageCount]);
    const prev = useCallback(() => {
        onPrevious();
        setCurrent((c) => Math.max(0, c - 1));
    }, [onPrevious]);
    return (React.createElement(StyledGalleryPager, { className: props.className },
        cloneElement(prevButton, {
            disabled: current === 1,
            ...useOnClickA11y(prev),
        }),
        pager && cloneElement(pager, { current, total: totalPageCount }),
        cloneElement(nextButton, {
            disabled: current === totalPageCount,
            ...useOnClickA11y(next),
        })));
}

const baseUrl = "https://ik.imagekit.io/bxpwztngs0o/";
const imageKitUrl = (app, src) => `${baseUrl}${app}${src}`;
const imageKitLoader = (app) => ({ src, width, quality }) => {
    return `${imageKitUrl(app, src)}?tr=w-${width}&tr=q-${quality ?? 80}`;
};
const imageKitBlurUrl = (app, src) => `${imageKitUrl(app, src)}?tr=bl-10`;
function ImageKit(props) {
    const { app, useBlur, ...rest } = props;
    const blur = useBlur
        ? {
            placeholder: "blur",
            blurDataURL: `${imageKitBlurUrl(props.app, props.src)}`,
        }
        : null;
    return (React.createElement(Image, { ...rest, ...blur, loader: imageKitLoader(app) }));
}

const Parallax = ({ children, offset = 50, }) => {
    const prefersReducedMotion = useReducedMotion();
    const [elementTop, setElementTop] = useState(0);
    const [clientHeight, setClientHeight] = useState(0);
    const ref = useRef(null);
    const initial = elementTop - clientHeight;
    const final = elementTop + offset;
    const { scrollY } = useScroll();
    const yRange = useTransform(scrollY, [initial, final], [offset, -offset]);
    const y = useSpring(yRange, { stiffness: 400, damping: 90 });
    useEffect(() => {
        const element = ref.current;
        const onResize = () => {
            setElementTop((element?.getBoundingClientRect()?.top || 0) + window.scrollY ||
                window.pageYOffset);
            setClientHeight(window.innerHeight);
        };
        onResize();
        window.addEventListener("resize", onResize);
        return () => window.removeEventListener("resize", onResize);
    }, [ref]);
    // Don't parallax if the user has "reduced motion" enabled
    if (prefersReducedMotion) {
        return React.createElement(React.Fragment, null, children);
    }
    return cloneElement(children, { ref, style: { y } });
};

function FacebookSEO(props) {
    return (React.createElement(Head, null,
        React.createElement("meta", { property: "og:url", content: props.url }),
        React.createElement("meta", { property: "og:type", content: props.type }),
        React.createElement("meta", { property: "og:title", content: props.title }),
        React.createElement("meta", { property: "og:description", content: props.description }),
        React.createElement("meta", { property: "og:image", content: props.imageUrl }),
        React.createElement("meta", { property: "og:image:width", content: `${props.imageWidthPx}` }),
        React.createElement("meta", { property: "og:image:height", content: `${props.imageHeightPx}` }),
        props.alternateLocale && (React.createElement("meta", { property: "og:locale:alternate", content: props.alternateLocale }))));
}

function StructuredDataSEO(props) {
    return (React.createElement(Head, null,
        React.createElement("script", { type: "application/ld+json", dangerouslySetInnerHTML: {
                __html: JSON.stringify(props.structuredData, null, " "),
            } })));
}

function TitleSEO(props) {
    const { children: [title, description], } = props;
    return (React.createElement(Head, null,
        React.createElement("title", null, title),
        React.createElement("meta", { name: "description", content: description })));
}

const normalizedAnchor = css `
  text-decoration: none;
  color: inherit;
  cursor: pointer;

  &:visited {
    color: inherit;
  }
`;
const NormalizedAnchor = styled.a `
  ${normalizedAnchor}
`;
const normalizeButton = css `
  outline: none;
  border: none;
`;
/**
 * Prior art:
 * https://gregives.co.uk/blog/interpolating-colour-with-css/
 */
const bgColorInterpolation = (start, end) => css `
  --progress: 0;
  --h-start: ${start[0]};
  --s-start: ${start[1]};
  --l-start: ${start[2]};
  --h-end: ${end[0]}
  --s-end: ${end[1]}
  --l-end: ${end[2]};

  background-color: hsl(
    calc(
      var(--h-start) + (var(--h-end) - var(--h-start)) * var(--progress) / 100
    ),
    calc(
      var(--s-start) + (var(--s-end) - var(--s-start)) * var(--progress) / 100
    ),
    calc(
      var(--l-start) + (var(--l-end) - var(--l-start)) * var(--progress) / 100
    )
  );
`;
const colorInterpolation = (start, end) => css `
  --progress: 0;
  --h-start: ${start[0]};
  --s-start: ${start[1]};
  --l-start: ${start[2]};
  --h-end: ${end[0]}
  --s-end: ${end[1]}
  --l-end: ${end[2]};

  color: hsl(
    calc(
      var(--h-start) + (var(--h-end) - var(--h-start)) * var(--progress) / 100
    ),
    calc(
      var(--s-start) + (var(--s-end) - var(--s-start)) * var(--progress) / 100
    ),
    calc(
      var(--l-start) + (var(--l-end) - var(--l-start)) * var(--progress) / 100
    )
  );
`;

function stripHrefHashtag(href) {
    return href?.charAt(0) === "#" ? href?.substring(1) : undefined;
}
/**
 * - A simple Link component that injects the `href` into the URL as
 *   `[queryParamName]=href` when clicked.
 * - (The query param name is customizable.)
 * - Can be used in conjunction with the LinkTarget to automatically
 *   scroll the app to matching LinkTarget.
 *
 * **Note:** For a11y, you need to pass a child component that
 * implements tabIndex and onKeyUp, that is, it must be tab-able and
 * support the keyUp handler.
 */
function InternalLink(props) {
    const { href, queryParamName = "link", children } = props;
    const target = stripHrefHashtag(href);
    const { route, push } = useRouter() || {};
    const { onKeyDown: onKeyUp } = useOnClickA11y((e) => {
        push({ pathname: route, query: { [queryParamName]: target } }, undefined, {
            shallow: true,
        });
        e?.preventDefault();
    });
    return (React.createElement(NextLink, { href: `/?${queryParamName}=${target}`, shallow: true }, React.cloneElement(children, {
        tabIndex: 0,
        onKeyUp,
        href: href,
    })));
}
/**
 * Listens to changes in the URL:
 *
 * - When the specified query parameter (`queryParamName`) equals the
 *   `href`, the app will scroll to this component and focus the
 *   component. OR
 * - When the route equals the `href', the app will scroll to this
 *   component and focus the component.
 */
const LinkTarget = (props) => {
    const { href, queryParamName = "link", children, scrollToTop } = props;
    const { query = {}, replace, route } = useRouter() || {};
    const ref = useRef();
    useEffect(function scrollToTargetOnQueryParamChange() {
        const target = stripHrefHashtag(href);
        const shouldScroll = query[queryParamName] === target || route === `/${target}`;
        if (shouldScroll && scrollToTop) {
            window.scrollTo({
                top: 0,
                left: 0,
                // not supported by IE or Safari
                behavior: "smooth",
            });
        }
        else if (shouldScroll) {
            window.scrollBy({
                top: ref.current?.getBoundingClientRect().top,
                left: 0,
                // not supported by IE or Safari
                behavior: "smooth",
            });
        }
        let unsubscribe;
        let unsubscribe2;
        if (shouldScroll) {
            unsubscribe = setTimeout(function focusAfterScroll() {
                // If it wasn't for Safari (and IE), we could just pass
                // `preventScroll: false` and call it a day, that is,
                // we could remove the scrollBy above
                ref.current?.focus();
            }, 1000);
            unsubscribe2 = setTimeout(function removeLinkForReselection() {
                delete query[queryParamName];
                replace(route, query, { shallow: true });
            }, 1000);
            return () => {
                unsubscribe && clearTimeout(unsubscribe);
                unsubscribe2 && clearTimeout(unsubscribe2);
            };
        }
    }, [href, query, queryParamName, replace, route, scrollToTop]);
    return (React.createElement(React.Fragment, null, React.cloneElement(children, {
        ref,
        tabIndex: 0,
    })));
};
const StyledExternalLink = styled(m.a) `
  ${normalizedAnchor}
`;
function ExternalLink(props) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { target, tabIndex, ...args } = props;
    return React.createElement(StyledExternalLink, { tabIndex: 0, ...args, target: "_blank" });
}

const StaticTOCContext = React.createContext({
    items: [],
});
const SetterContext = React.createContext({
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setItem() { },
});
/**
 * Use this to create a static TOC. Under the hood, the TOC will be
 * set exactly once---during the initial render by the SetStaticTOC
 * component.
 *
 * - Use SetStaticTOC in components to set TOC items
 * - Use getStaticProps for SSR-initial-data population via
 *   react-dom/renderToString.
 *
 * **Notes:**
 *
 * 1. As it's name implies, this provider does _not_ support a
 *    dynamically-generated TOC.
 * 2. Do not use __setItemsForSSR - this is used internally to set the
 *    items in SSR.
 */
const StaticTOCProvider = (props) => {
    const items = useRef(props.initialItemsFromSSR || []);
    const { __setItemsForSSR } = props;
    const update = useMemo(() => ({
        setItem(item) {
            if (!items.current.find((x) => x.href === item.href)) {
                items.current.push(item);
                items.current.sort((a, b) => a.index - b.index);
                __setItemsForSSR?.(items.current);
            }
        },
    }), [__setItemsForSSR]);
    const context = useMemo(() => ({ items: items.current }), []);
    return (React.createElement(SetterContext.Provider, { value: update },
        React.createElement(StaticTOCContext.Provider, { value: context }, props.children)));
};
/**
 * As it's name implies, calls setItem during the initial render, so
 * can be used by renderToString to get the initial values.
 */
class SetDuringInitialRender extends React.Component {
    constructor(props) {
        super(props);
        props.setItem(props.children);
    }
    render() {
        return null;
    }
}
/**
 * Use this to create a TOC item during initial render.
 *
 * Note: can be safely set exactly once. As it's name implies, dynamic
 * TOC items are _not_ supported.
 *
 * Exists so that a component can set the TOC without triggering an
 * unnecessary render on itself.
 */
function SetStaticTOC(props) {
    const { setItem } = useContext(SetterContext);
    return (React.createElement(SetDuringInitialRender, { setItem: setItem }, props));
}
/**
 * Use this to inject the TOC from SSR.
 */
const getStaticPropsStaticTOC = (Component) => async () => {
    let initial = [];
    const setItems = (items) => {
        initial = [...items];
    };
    const app = () => (React.createElement(StaticTOCProvider, { __setItemsForSSR: setItems }, Component));
    renderToString(app());
    return {
        props: {
            initialStaticTOC: initial,
        },
    };
};
/**
 * - Convenience helper for generating dynamic routes from the TOC.
 * - For example, suppose your website has `pages/[link].js` and TOC
 *   items: "home" and "about".
 * - Then `getStaticPathsForStaticTOC("link", <StaticTOCComponent/>)`
 *   will create dynamic paths for `/home` and `/about`.
 */
const getStaticPathsForStaticTOC = async (dynamicRouteName, StaticTOCComponent) => {
    const getStaticProps = getStaticPropsStaticTOC(StaticTOCComponent);
    const { props: { initialStaticTOC }, } = await getStaticProps();
    return {
        paths: initialStaticTOC.map((item) => ({
            params: { [dynamicRouteName]: item.href.substring(1) },
        })),
    };
};

const sharedTitleStyle = css `
  background: transparent;
  letter-spacing: -0.12rem;
  line-height: 1;
  font-family: inherit;
  color: inherit;
`;
styled.h1 `
  text-align: center;
  font-size: 5rem;
  width: min(40rem, 95vw);
  position: absolute;
  mix-blend-mode: color-dodge;
  font-weight: bold;
  letter-spacing: -0.05em;
  margin: 0;
  padding: 0;

  ${media("tablet")(css `
    font-size: 10rem;
  `)}
`;
const H1 = styled.h1 `
  ${sharedTitleStyle}
  font-weight: normal;
  font-size: 4rem;
  padding: 0;
  margin: 0;
  margin-top: 1em;
  margin-bottom: 0.5em;
  background: transparent;

  ${media("tablet")(css `
    font-size: 8rem;
    margin-top: 0;
  `)}
`;
const H2 = styled.h2 `
  ${sharedTitleStyle}
  font-weight: bold;
  font-size: 3rem;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;
  margin-top: 0.5em;

  ${media("tablet")(css `
    font-size: 6rem;
  `)}
`;
const H3 = styled.h3 `
  ${sharedTitleStyle}
  font-weight: bold;
  font-size: 2rem;
  padding: 0;
  margin: 0;
  margin-bottom: 0.5em;

  ${media("tablet")(css `
    font-size: 4rem;
  `)}
`;

const text = css `
  font-size: 1.5rem;
  margin-top: 1em;
  margin-bottom: 1em;
  text-align: justify;

  &:first-child {
    margin-top: 0em;
  }

  &:last-child {
    margin-bottom: 0em;
  }

  font-family: inherit;
  color: inherit;
`;
const Text = styled.p `
  ${text}
`;

const createModuleState = (initial) => {
    let state = initial;
    const cbs = new Set();
    const getState = () => state;
    const setState = (next) => {
        if (typeof next === "function") {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore can't infer next
            state = next(state);
        }
        else {
            state = next;
        }
        cbs.forEach((cb) => cb());
    };
    const subscribe = (cb) => {
        cbs.add(cb);
        return () => {
            cbs.delete(cb);
        };
    };
    return { getState, setState, subscribe };
};
const useModuleState = (moduleState) => {
    const [state, setState] = useState(moduleState.getState());
    useEffect(() => {
        const unsubscribe = moduleState.subscribe(() => {
            setState(moduleState.getState());
        });
        setState(moduleState.getState());
        return unsubscribe;
    }, [moduleState]);
    return [state, moduleState.setState];
};

const useDebouncedListener = (eventType, listener) => {
    const ticking = useRef(false);
    const event = useRef();
    const eventTypeRef = useRef(eventType);
    eventTypeRef.current = eventType;
    const listenerRef = useRef(listener);
    listenerRef.current = listener;
    useEffect(() => {
        let timeout;
        /**
         * Track the latest event... but the listener may not necessarily
         * receive all events.
         */
        const onEventUpdate = (newEvent) => {
            event.current = newEvent;
            requestTick();
        };
        const requestTick = () => {
            if (!ticking.current && timeout)
                cancelAnimationFrame(timeout);
            if (!ticking.current)
                timeout = requestAnimationFrame(callListener);
            ticking.current = true;
        };
        const callListener = () => {
            ticking.current = false;
            if (event.current)
                listenerRef.current(event.current);
        };
        window.addEventListener(eventTypeRef.current, onEventUpdate);
        return () => {
            window.removeEventListener(eventTypeRef.current, onEventUpdate);
        };
    }, []);
};
/**
 * Returns true once users have scrolled past the given delta.
 */
function useDetectScrolling({ deltaY }) {
    const scrollInfo = useRef({ lastKnownScrollY: 0, ticking: false });
    const [scrolling, setScrolling] = useState(false);
    useEffect(() => {
        let timeout;
        function trackScrollY() {
            scrollInfo.current.lastKnownScrollY = window.pageYOffset;
            requestTick();
        }
        function requestTick() {
            if (!scrollInfo.current.ticking) {
                if (timeout) {
                    cancelAnimationFrame(timeout);
                }
                timeout = requestAnimationFrame(update);
            }
            scrollInfo.current.ticking = true;
        }
        function update() {
            scrollInfo.current.ticking = false;
            const cropValue = deltaY ?? window.innerHeight;
            setScrolling((prevScrolling) => {
                if (!prevScrolling &&
                    scrollInfo.current.lastKnownScrollY >= cropValue) {
                    return true;
                }
                else if (prevScrolling &&
                    scrollInfo.current.lastKnownScrollY < cropValue) {
                    return false;
                }
                return prevScrolling;
            });
        }
        window.addEventListener("scroll", trackScrollY);
        return () => {
            window.removeEventListener("scroll", trackScrollY);
        };
    }, [deltaY]);
    return scrolling;
}
/**
 * Too expensive?
 */
function useScrollingY() {
    const lastKnownScrollY = useRef(0);
    const [, updateState] = useState(0);
    useDebouncedListener("scroll", () => {
        lastKnownScrollY.current = window.scrollY;
        updateState((c) => c + 1);
    });
    return lastKnownScrollY.current;
}

function globalWindow() {
    return typeof window !== "undefined" ? window : {};
}

const zIndexNavigation = 9999;

const LandingHeroContainer = styled(m.div) `
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  background: transparent;
`;
/**
 * Use this as the basis for the ReducedHero variant.
 *
 * Defaults to position fixed in the top-right.
 */
const navigationLandingHeroTitle = css `
  position: fixed;
  top: 1rem;
  right: 1rem;
  width: auto;
  z-index: ${zIndexNavigation};
`;
const NavigationLandingHeroTitle = styled(m.div) `
  ${navigationLandingHeroTitle}
`;
const BgContainer = styled(m.div) `
  position: relative;
`;
const useDetectLandingScrolled = () => useDetectScrolling({
    deltaY: (globalWindow()?.innerHeight || 0) * 0.25,
});
const requestShrinkState = createModuleState(false);
/**
 * When users call this method, it will be permanently rendered at a
 * smaller size. Call #cancelShrinkLandingHero to cancel.
 */
const shrinkLandingHero = () => requestShrinkState.setState(true);
/**
 * Note: this won't automatically grow the landing hero if it's
 * scrolled away.
 */
const cancelShrinkLandingHero = () => requestShrinkState.setState(false);
/**
 * Usage:
 *
 * - The first child element is the title, large version. You must use
 *   the provided FullHero component as the basis for your component.
 * - The second child element is the title, small version. You must use
 *   the provided ReducedHero component as the basis for your
 *   component.
 * - To use in non-landing pages, set forceReducedHero to true
 */
function LandingHero(props) {
    const [requestShrink] = useModuleState(requestShrinkState);
    const { children, disabled = false } = props;
    const [largeTitle, smallTitle] = children;
    const scrolled = useDetectLandingScrolled();
    const shrink = requestShrink || scrolled || disabled;
    return (React.createElement(React.Fragment, null,
        React.createElement(LayoutGroup, { id: "landing-bg" },
            React.createElement(BgContainer, null,
                React.createElement(LandingHeroContainer, { initial: false, animate: disabled ? { height: 0 } : { height: "100vh" } }, !shrink && React.createElement(m.div, { layoutId: "landing-hero" }, largeTitle))),
            shrink && (React.createElement(NavigationLandingHeroTitle, { layoutId: "landing-hero" }, smallTitle)))));
}

const defaultTheme$1 = {
    components: {
        menu: {
            backgroundColor: "black",
        },
        externalAnchor: {
            color: "#6F685D",
            hover: "#6F685D",
        },
    },
};

const ButtonContainer = styled.div `
  position: fixed;
  top: 1rem;
  left: 0.5rem;
  z-index: ${zIndexNavigation};
`;
const Button = styled(m.button) `
  outline: none;
  border: none;
  background: transparent;
`;
/**
 * Usage:
 *
 * - The first child element is the button to show when the menu is
 *   closed
 * - The second child element is the button to show when the menu is
 *   open
 */
function HamburgerMenuButton(props) {
    const { openState, toggle } = props;
    return (React.createElement(ButtonContainer, null,
        React.createElement(AnimatePresence, { mode: "popLayout" },
            React.createElement(Button, { tabIndex: 0, ...onClickA11y(toggle), "aria-label": openState === "open" ? "Close main menu" : "Open main menu", "aria-haspopup": true, "aria-expanded": openState === "open", key: openState, initial: { opacity: 0, transformOrigin: "center" }, animate: { opacity: 1, transition: { duration: 2 } }, exit: {
                    opacity: 0,
                    rotate: openState === "open" ? -360 : 360,
                    scale: 1,
                    transition: { duration: 1 },
                }, whileHover: {
                    scale: 1.5,
                    transition: { ease: [0.15, 1.98, 0.39, 1.99] },
                }, whileFocus: {
                    scale: 1.5,
                    transition: { ease: [0.15, 1.98, 0.39, 1.99] },
                } },
                openState === "closed" && props.children[0],
                openState === "open" && props.children[1]))));
}

const Container = styled.div `
  position: relative;
`;
const defaultTheme = defaultTheme$1.components.menu;
const StyledMenuContents = styled(m.nav) `
  position: fixed;
  z-index: 2;
  display: flex;
  flex-direction: column;

  /* styles */
  background: ${(p) => p.$bgColor ??
    p.theme.components?.menu?.backgroundColor ??
    defaultTheme.backgroundColor};

  top: 5rem;
  bottom: 0;
  width: 100%;

  ${media("mobileL")(css `
    width: min(60vw, 27rem);
  `)}

  overflow-y: scroll;

  // https://www.w3schools.com/howto/howto_css_hide_scrollbars.asp
  &::-webkit-scrollbar {
    display: none;
  }
  -ms-overflow-style: none;
  scrollbar-width: none;
`;
const HamburgerMenuContext = createContext({
    open: false,
});
const HamburgerMenuToggleContext = createContext({});
/**
 * This is needed to pass state to items. See HamburgerMenu for
 * details.
 */
function HamburgerMenuContextProvider(props) {
    const [open, setOpen] = useState(false);
    const prevOpenTime = useRef(0);
    const toggleValue = useMemo(() => {
        return {
            toggle() {
                const now = Date.now();
                if (now - prevOpenTime.current > 400) {
                    prevOpenTime.current = now;
                    setOpen((_open) => !_open);
                }
            },
        };
    }, []);
    return (React.createElement(HamburgerMenuToggleContext.Provider, { value: toggleValue },
        React.createElement(HamburgerMenuContext.Provider, { value: { open } }, props.children)));
}
function SideMenuContents(props) {
    const { children: items } = props;
    const { open } = useContext(HamburgerMenuContext);
    const elem = useRef(null);
    return (React.createElement(AnimatePresence, { initial: false }, open && (React.createElement(StyledMenuContents, { ref: elem, key: "menu", "aria-label": "Main menu", role: "navigation", initial: {
            x: "-100vw",
        }, animate: {
            x: 0,
            transition: { staggerChildren: 0.07, delayChildren: 0.2 },
        }, exit: {
            x: "-100vw",
            transition: { staggerChildren: 0.05, staggerDirection: -1 },
        }, "$bgColor": props.bgColor }, items))));
}
/**
 * Usage:
 *
 * 1. Child elements:
 *
 *    - The first child is the icon shown when the menu is closed.
 *    - The second child is the icon shown when the menu us open.
 *    - The remaining elements are the menu items.
 * 2. Wrap the HamburgerMenu with the HamburgerMenuContextProvider
 * 3. Change the background color of the menu items by passing a bgColor
 *    prop or setting `theme.components.menu.backgroundColor` (in the
 *    styled-components theme).
 * 4. You should most likely should use the provided HamburgerMenuItem to
 *    style clickable menu items. This component does the "right
 *    thing" with respect to a11y. It's just an anchor under the
 *    hood.
 * 5. Wrap the main content with BodyUnderHamburger. This is to freeze
 *    the content in place under the hamburger while it is open. The
 *    hamburger must be placed in front of the BodyUnderHamburger.
 * 6. (Optional) Apply `overflow:hidden` to the `body` when the menu is
 *    open. See https://stackoverflow.com/a/45230674. This is so that
 *    on IOS devices, the menu can be scrolled w/o also scrolling the
 *    body. Use useIsoMenuScrollWorkaround.
 *
 * Note:
 *
 * 1. The *StateIcon components are rendered inside a button component.
 * 2. While the *StateIcon components are usually icons, they can be any
 *    React component.
 * 3. Anything that wishes to toggle the menu state must use the same
 *    Context.
 */
function HamburgerMenu(props) {
    const { children } = props;
    const [closedButton, openButton, ...items] = children;
    const { open } = useContext(HamburgerMenuContext);
    const { toggle } = useContext(HamburgerMenuToggleContext);
    useEffect(function closeOnBlur() {
        if (open) {
            window.addEventListener("click", toggle);
        }
        return () => {
            window.removeEventListener("click", toggle);
        };
    }, [open, toggle]);
    return (React.createElement(Container, null,
        React.createElement(HamburgerMenuButton, { openState: open ? "open" : "closed", toggle: toggle },
            closedButton,
            openButton),
        React.createElement(SideMenuContents, { bgColor: props.bgColor }, items)));
}
const StyledHamburgerMenuItem = styled(NormalizedAnchor) ``;
function HamburgerMenuItem(props) {
    const { onClick, ...rest } = props;
    const { toggle } = useContext(HamburgerMenuToggleContext);
    return (React.createElement(m.div, { whileHover: { scale: 1.05 }, whileTap: { scale: 0.95 } },
        React.createElement(StyledHamburgerMenuItem, { ...rest, ...useOnClickA11y((e) => {
                toggle();
                onClick?.(e);
            }) })));
}
const Body = styled.div `
  width: 100%;
  transition: opacity 0.5s;
  opacity: 1;

  ${(p) => p.scrollY !== undefined &&
    css `
      position: fixed;
      top: -${p.scrollY}px;
      opacity: 0.5;
    `}
`;
/**
 * Prior art:
 * https://css-tricks.com/prevent-page-scrolling-when-a-modal-is-open/
 */
const BodyUnderHamburger = (props) => {
    const { open } = useContext(HamburgerMenuContext);
    const positionOnOpen = useRef();
    const opening = open && positionOnOpen.current === undefined;
    const closing = !open && positionOnOpen.current !== undefined;
    if (opening)
        positionOnOpen.current = window.scrollY + (props.offsetY ?? 0);
    else if (closing) {
        const scrollPosition = positionOnOpen.current ?? 0;
        // reset positionOnOpen to reset the Body
        positionOnOpen.current = undefined;
        // but delay the scroll restore to give time for the Body to reset
        setTimeout(() => globalThis.scrollTo?.(0, scrollPosition));
    }
    return (React.createElement(Body, { scrollY: positionOnOpen.current, "aria-hidden": open }, props.children));
};
/**
 * @deprecated  ???
 */
const UseIosMenuScrollWorkaround = () => {
    const { open } = useContext(HamburgerMenuContext);
    const prevPosition = useRef();
    const prevOverflow = useRef();
    useEffect(() => {
        if (open) {
            prevPosition.current = window.document.body.style.position;
            window.document.body.style.position = "fixed";
            prevOverflow.current = window.document.body.style.overflow;
            window.document.body.style.overflow = "hidden";
        }
        else {
            if (prevPosition.current)
                window.document.body.style.position = prevPosition.current;
            if (prevOverflow.current)
                window.document.body.style.overflow = prevOverflow.current;
        }
    }, [open]);
    return null;
};

const tocState = createModuleState([]);
const useTOC = () => {
    const [TOC] = useModuleState(tocState);
    return useMemo(() => {
        return TOC.map((item) => item).sort((a, b) => {
            return a.index < b.index ? -1 : 1;
        });
    }, [TOC]);
};
const setTOCItem = (newItem) => {
    if (tocState
        .getState()
        .find((item) => item.href === newItem.href && item.index !== newItem.index)) {
        throw new Error(`Duplicate TOC item found ${JSON.stringify(newItem)}`);
    }
    else if (tocState
        .getState()
        .find((item) => item.href === newItem.href && item.index === newItem.index)) {
        // Most likely a nextJS quirk---it
        console.warn(`Duplicate TOC item found ${JSON.stringify(newItem)}`);
    }
    tocState.setState([...tocState.getState(), newItem]);
};

/**
 * On mobile, the scrollbar gets in the way, so you need to use this
 * to size it correctly.
 */
const centeredColumn = css `
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  padding: 0;

  width: 90vw;

  ${media("tablet")(css `
    width: 100%;
  `)}
`;
/**
 * Not sure what this does 🤷
 */
const responsiveCenteredColumn = css `
  ${centeredColumn}

  ${media("tablet")(css `
    width: 100%;
    flex-direction: row;
    align-items: flex-start;
  `)}
`;
const CenteredColumn = styled.div `
  ${centeredColumn}
`;
const ResponsiveCenteredColumn = styled.div `
  ${responsiveCenteredColumn}
`;

const buttonFilledHover = css `
  transition: opacity 0.1s;

  &:hover {
    opacity: 0.86;
  }

  &:focus {
    opacity: 0.76;
  }

  &:active {
    opacity: 0.68;
  }
`;
/**
 * @deprecated  This does too much.
 *
 *   This is a link that looks like a button. If i'm not mistaken, it's
 *   needed because it's used for the call-button icon...and this
 *   looks like a button but needs a link's a href to trigger the call
 *   action. In other words, it's a button that behaves like a link.
 *   🤷
 *
 *   Make sure to set the color and background-color.
 *
 *   Further reading:
 *   https://m3.material.io/components/buttons/guidelines
 */
const LinkButtonFilled = styled.a `
  ${text}

  text-decoration: none;
  padding: 0.5em;
  margin-top: 1rem;
  transition: opacity 0.1s;
  box-shadow: 0.2em 0.2em 0.5em 1px black;
  transition: background 0.1s, opacity 0.1s;

  ${buttonFilledHover}
`;
/**
 * This is a link that looks like a button. If i'm not mistaken, it's
 * needed because it's used for the call-button icon...and this looks
 * like a button but needs a link's a href to trigger the call action.
 * In other words, it's a button that behaves like a link. 🤷
 *
 * Make sure to set the color and border-color.
 *
 * Optional: background-color
 *
 * Further reading:
 * https://m3.material.io/components/buttons/guidelines
 */
const LinkButtonOutline = styled(LinkButtonFilled) `
  border: 1px solid black;
  background-color: white;
  box-shadow: none;
  transition: background 0.1s, opacity 0.1s;

  &:hover {
    opacity: 1;
    background-color: hsl(0, 0%, 96%);
  }

  &:focus {
    opacity: 1;
    background-color: hsl(0, 0%, 88%);
  }

  &:active {
    opacity: 1;
    background-color: hsl(0, 0%, 88%);
  }
`;

/**
 * @deprecated  Most likely not needed
 */
typeof navigator !== "undefined" &&
    !!navigator.vendor &&
    navigator.vendor.indexOf("Apple") > -1 &&
    !!navigator.userAgent &&
    navigator.userAgent.indexOf("CriOS") == -1 &&
    navigator.userAgent.indexOf("FxiOS") == -1;
const isSSR = () => typeof window === "undefined";

/**
 * Do nothing function that compiles iff the two types are the same.
 *
 * https://stackoverflow.com/a/53808212
 */
const exactType = (draft, expected) => {
    return { draft, expected };
};

/**
 * Prior art:
 * https://designshack.net/articles/typography/guide-to-responsive-typography-sizing-and-scales/
 */
const buildFontScale = (scale) => {
    const scales = [];
    for (let level = 0; level < 8; level++) {
        if (level === 0)
            scales.push(1);
        else
            scales.push(scales[level - 1] * scale);
    }
    return scales;
};
const buildFontScaleMap = (scale, unit) => {
    const scales = buildFontScale(scale);
    const name = "fontSize";
    const raw = {
        [`${name}Raw1`]: scales[0],
        [`${name}Raw2`]: scales[1],
        [`${name}Raw3`]: scales[2],
        [`${name}Raw4`]: scales[3],
        [`${name}raw5`]: scales[4],
        [`${name}Raw6`]: scales[5],
        [`${name}Raw7`]: scales[6],
        [`${name}Raw8`]: scales[7],
    };
    const toUnit = (x) => `${x}${unit}`;
    const units = {
        [`${name}1`]: toUnit(scales[0]),
        [`${name}2`]: toUnit(scales[1]),
        [`${name}3`]: toUnit(scales[2]),
        [`${name}4`]: toUnit(scales[3]),
        [`${name}5`]: toUnit(scales[4]),
        [`${name}6`]: toUnit(scales[5]),
        [`${name}7`]: toUnit(scales[6]),
        [`${name}8`]: toUnit(scales[7]),
    };
    return {
        ...raw,
        ...units,
    };
};
const fontScaleGoldenRatio = 1.68;
const fontScaleAugmentedFourth = 1.414;
const fontScalePerfectFifth = 1.5;

export { BasicGalleryGrid, BodyUnderHamburger, CenteredColumn, DefaultBasicGalleryGrid, ExternalLink, FacebookSEO, GalleryItem, GalleryPager, GalleryPagerInfo, GallerySkeletonItem, H1, H2, H3, HamburgerMenu, HamburgerMenuContext, HamburgerMenuContextProvider, HamburgerMenuItem, HamburgerMenuToggleContext, ImageKit, InMemoryGalleryGrid, InternalLink, LandingHero, LandingHeroContainer, LinkButtonFilled, LinkButtonOutline, LinkTarget, NavigationLandingHeroTitle, NormalizedAnchor, Parallax, ResponsiveCenteredColumn, SetStaticTOC, StaticTOCContext, StaticTOCProvider, StructuredDataSEO, Text, TitleSEO, UseIosMenuScrollWorkaround, bgColorInterpolation, buildFontScaleMap, buttonFilledHover, cancelShrinkLandingHero, centeredColumn, colorInterpolation, createModuleState, defaultBasicGalleryGrid, exactType, fontScaleAugmentedFourth, fontScaleGoldenRatio, fontScalePerfectFifth, getStaticPathsForStaticTOC, getStaticPropsStaticTOC, globalWindow, isSSR, media, navigationLandingHeroTitle, nextImageSizes, normalizeButton, normalizedAnchor, onClickA11y, responsiveCenteredColumn, setTOCItem, shrinkLandingHero, suggestedGalleryItemTypography, useDebouncedListener, useDetectLandingScrolled, useDetectScrolling, useIsAtMostMediaSize, useModuleState, useOnClickA11y, useScrollingY, useTOC, zIndexNavigation };
//# sourceMappingURL=index.js.map
