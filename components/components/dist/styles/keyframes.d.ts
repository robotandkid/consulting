export declare const shakeKeyframe: import("styled-components").Keyframes;
export declare const glitchKeyframe: import("styled-components").Keyframes;
export declare const rotate360ScaleFadeKeyframe: import("styled-components").Keyframes;
export declare const rotateMinus360FadeKeyFrame: import("styled-components").Keyframes;
export declare const fadeInKeyframe: import("styled-components").Keyframes;
export declare const fadeOutKeyframe: import("styled-components").Keyframes;
export declare const simpleShimmer: import("styled-components").Keyframes;
