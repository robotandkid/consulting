import { css } from "styled-components";
declare const size: {
    mobileL: number;
    tablet: number;
    laptop: number;
};
type MediaSize = keyof typeof size;
export declare const media: (mediaSize: MediaSize) => (args: ReturnType<typeof css>) => import("styled-components").FlattenInterpolation<import("styled-components").ThemeProps<import("styled-components").DefaultTheme>>;
export declare function useIsAtMostMediaSize(s: MediaSize, ssrValue?: boolean): boolean;
export {};
