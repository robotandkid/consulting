import React from "react";
export declare const defaultTheme: {
    components: {
        menu: {
            backgroundColor: string;
        };
        externalAnchor: {
            color: string;
            hover: string;
        };
    };
};
type AllPartial<T> = {
    [P in keyof T]?: Partial<T[P]>;
};
/**
 * Consumers must implement this interface
 */
export interface ComponentThemeProps {
    theme: AllPartial<typeof defaultTheme>;
}
/**
 * Used by component-library viewer (cosmos, storybook, etc)
 */
export declare function AppThemeProvider(props: {
    children?: React.ReactNode;
}): React.JSX.Element;
export {};
