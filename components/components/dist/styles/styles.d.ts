export declare const normalizedAnchor: import("styled-components").FlattenSimpleInterpolation;
export declare const NormalizedAnchor: import("styled-components").StyledComponent<"a", import("styled-components").DefaultTheme, {}, never>;
export declare const normalizeButton: import("styled-components").FlattenSimpleInterpolation;
type HSL = [number, number, number];
/**
 * Prior art:
 * https://gregives.co.uk/blog/interpolating-colour-with-css/
 */
export declare const bgColorInterpolation: (start: HSL, end: HSL) => import("styled-components").FlattenSimpleInterpolation;
export declare const colorInterpolation: (start: HSL, end: HSL) => import("styled-components").FlattenSimpleInterpolation;
export {};
