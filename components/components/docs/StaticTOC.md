# StaticTOC

## Basic Behavior

- LinkRoutes and LinkQueryParams have 1-1 mappings with TOC items.
- These register themselves on the initial render with a StaticTOCContext.
- When the URL is updated, the corresponding Link\* component receives focus and
  the app scrolls to this element.
- It would also be cool if when the Link\* component is in view that it updates
  the route. But right now it doesn't work like this.
