import { ComponentThemeProps } from "../styles/theme";
import {} from "styled-components";

declare module "styled-components" {
  type Theme = ComponentThemeProps["theme"];
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultTheme extends Theme {}
}
