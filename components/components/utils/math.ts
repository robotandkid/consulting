/**
 * I forget what this is called :-) But it's basically used to define
 * a function which return the line segment between the start and end
 * when the input is between 0 and 1.
 */
function parametrizedLine1D<Point extends Record<string, any>>(
  start: Point,
  end: Point,
  key: keyof Point
) {
  const s = typeof start[key] === "number" ? start[key] : 0;
  const e = typeof end[key] === "number" ? end[key] : 0;

  return (t: number) => (e - s) * t + s;
}

export function parametrizedLine<Point extends Record<string, any>>(
  start: Point,
  end: Point,
  keyOfX: keyof Point,
  keyOfY: keyof Point
) {
  const vecX = parametrizedLine1D(start, end, keyOfX);
  const vecY = parametrizedLine1D(start, end, keyOfY);

  return (t: number) => ({
    [keyOfX]: vecX(t),
    [keyOfY]: vecY(t),
  });
}
