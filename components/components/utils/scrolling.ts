import { useEffect, useRef, useState } from "react";

export const useDebouncedListener = <EventType extends keyof WindowEventMap>(
  eventType: EventType,
  listener: (event: WindowEventMap[EventType]) => void
) => {
  const ticking = useRef(false);
  const event = useRef<WindowEventMap[EventType]>();

  const eventTypeRef = useRef(eventType);
  eventTypeRef.current = eventType;

  const listenerRef = useRef(listener);
  listenerRef.current = listener;

  useEffect(() => {
    let timeout: number;

    /**
     * Track the latest event... but the listener may not necessarily
     * receive all events.
     */
    const onEventUpdate = (newEvent: WindowEventMap[EventType]) => {
      event.current = newEvent;
      requestTick();
    };

    const requestTick = () => {
      if (!ticking.current && timeout) cancelAnimationFrame(timeout);
      if (!ticking.current) timeout = requestAnimationFrame(callListener);

      ticking.current = true;
    };

    const callListener = () => {
      ticking.current = false;
      if (event.current) listenerRef.current(event.current);
    };

    window.addEventListener(eventTypeRef.current, onEventUpdate);

    return () => {
      window.removeEventListener(eventTypeRef.current, onEventUpdate);
    };
  }, []);
};

/**
 * Returns true once users have scrolled past the given delta.
 */
export function useDetectScrolling({ deltaY }: { deltaY?: number }) {
  const scrollInfo = useRef({ lastKnownScrollY: 0, ticking: false });
  const [scrolling, setScrolling] = useState(false);

  useEffect(() => {
    let timeout: number;

    function trackScrollY() {
      scrollInfo.current.lastKnownScrollY = window.pageYOffset;
      requestTick();
    }

    function requestTick() {
      if (!scrollInfo.current.ticking) {
        if (timeout) {
          cancelAnimationFrame(timeout);
        }

        timeout = requestAnimationFrame(update);
      }

      scrollInfo.current.ticking = true;
    }

    function update() {
      scrollInfo.current.ticking = false;

      const cropValue = deltaY ?? window.innerHeight;

      setScrolling((prevScrolling) => {
        if (
          !prevScrolling &&
          scrollInfo.current.lastKnownScrollY >= cropValue
        ) {
          return true;
        } else if (
          prevScrolling &&
          scrollInfo.current.lastKnownScrollY < cropValue
        ) {
          return false;
        }

        return prevScrolling;
      });
    }

    window.addEventListener("scroll", trackScrollY);

    return () => {
      window.removeEventListener("scroll", trackScrollY);
    };
  }, [deltaY]);

  return scrolling;
}

/**
 * Too expensive?
 */
export function useScrollingY() {
  const lastKnownScrollY = useRef(0);
  const [, updateState] = useState(0);

  useDebouncedListener("scroll", () => {
    lastKnownScrollY.current = window.scrollY;
    updateState((c) => c + 1);
  });

  return lastKnownScrollY.current;
}
