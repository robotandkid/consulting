import React, { useEffect, useMemo, useRef } from "react";

/**
 * Returns onClick handler and it's corresponding onKeyDown handler
 * (for a11y).
 *
 * Use this when the hook version cannot be used.
 */
export function onClickA11y<Element = HTMLDivElement>(
  cb: (e?: React.KeyboardEvent<Element> | React.MouseEvent<Element>) => any
) {
  return {
    onKeyDown(e: React.KeyboardEvent<Element>) {
      if (e.key === " " || e.key === "Enter") {
        cb?.(e);
      }
    },
    onClick(e: React.MouseEvent<Element>) {
      cb?.(e);
    },
  };
}

/**
 * Returns onClick handler and it's corresponding onKeyDown handler
 * (for a11y).
 */
export function useOnClickA11y<Element = HTMLDivElement>(
  cb: (e?: React.KeyboardEvent<Element> | React.MouseEvent<Element>) => any
) {
  const _cb = useRef(cb);

  useEffect(() => {
    _cb.current = cb;
  });

  return useMemo(
    () => ({
      onKeyDown(e: React.KeyboardEvent<Element>) {
        if (e.key === " " || e.key === "Enter") {
          _cb.current?.(e);
        }
      },
      onClick(e: React.MouseEvent<Element>) {
        _cb.current?.(e);
      },
    }),
    []
  );
}
