/**
 * Prior art:
 * https://designshack.net/articles/typography/guide-to-responsive-typography-sizing-and-scales/
 */
const buildFontScale = (scale: number) => {
  const scales: number[] = [];

  for (let level = 0; level < 8; level++) {
    if (level === 0) scales.push(1);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore it should be defined
    else scales.push(scales[level - 1] * scale);
  }

  return scales as [
    number,
    number,
    number,
    number,
    number,
    number, // 6
    number,
    number
  ];
};

export const buildFontScaleMap = <Unit extends "px" | "rem">(
  scale: number,
  unit: Unit
) => {
  const scales = buildFontScale(scale);
  const name = "fontSize" as const;

  const raw = {
    [`${name}Raw1` as const]: scales[0],
    [`${name}Raw2` as const]: scales[1],
    [`${name}Raw3` as const]: scales[2],
    [`${name}Raw4` as const]: scales[3],
    [`${name}raw5` as const]: scales[4],
    [`${name}Raw6` as const]: scales[5],
    [`${name}Raw7` as const]: scales[6],
    [`${name}Raw8` as const]: scales[7],
  } as const;

  type Scale = `${number}${Unit}`;
  const toUnit = (x: number) => `${x}${unit}` as Scale;

  const units = {
    [`${name}1` as const]: toUnit(scales[0]),
    [`${name}2` as const]: toUnit(scales[1]),
    [`${name}3` as const]: toUnit(scales[2]),
    [`${name}4` as const]: toUnit(scales[3]),
    [`${name}5` as const]: toUnit(scales[4]),
    [`${name}6` as const]: toUnit(scales[5]),
    [`${name}7` as const]: toUnit(scales[6]),
    [`${name}8` as const]: toUnit(scales[7]),
  } as const;

  return {
    ...raw,
    ...units,
  } as const;
};

export const fontScaleGoldenRatio = 1.68;
export const fontScaleAugmentedFourth = 1.414;
export const fontScalePerfectFifth = 1.5;
