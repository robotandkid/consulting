import { useEffect, useState } from "react";

export function useFreezeValue<T>(valueToFreeze: T, freeze: boolean): T {
  const [cache, setCache] = useState<T>();

  useEffect(
    function freezeValue() {
      setCache((c) => {
        if (typeof c === "undefined" && freeze) {
          return valueToFreeze;
        }

        return c;
      });
    },
    [valueToFreeze, freeze]
  );

  useEffect(
    function unfreezeValue() {
      setCache((c) => {
        if (!freeze) {
          return undefined;
        }
        return c;
      });
    },
    [freeze]
  );

  return typeof cache === "undefined" ? valueToFreeze : cache;
}
