export type Undefined<T> = { [K in keyof T]?: undefined };
export type Unpromisify<T> = T extends Promise<infer R> ? R : T;

/**
 * https://stackoverflow.com/a/53808212
 */
type IfEquals<T, U, Y = unknown, N = never> = (<G>() => G extends T
  ? 1
  : 2) extends <G>() => G extends U ? 1 : 2
  ? Y
  : N;

/**
 * Do nothing function that compiles iff the two types are the same.
 *
 * https://stackoverflow.com/a/53808212
 */
export const exactType = <T, U>(
  draft: T & IfEquals<T, U>,
  expected: U & IfEquals<T, U>
): IfEquals<T, U> => {
  return { draft, expected } as IfEquals<T, U>;
};
