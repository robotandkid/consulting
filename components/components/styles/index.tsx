export {
  bgColorInterpolation,
  colorInterpolation,
  normalizeButton,
  normalizedAnchor,
  NormalizedAnchor,
} from "./styles";
