import { keyframes } from "styled-components";

export const shakeKeyframe = keyframes`
  10%, 90% {
    transform: translateY(-0.1rem);
  }
  20%, 80% {
    transform: translateY(0.15rem);
  }
  30%, 50%, 70% {
    transform: translateY(-0.35rem);
  }

  40%, 60% {
    transform: translateY(0.35rem);
  }
`;

export const glitchKeyframe = keyframes`
  0% {
    transform: translate(0);
  }
  20% {
    transform: translate(-5px, 5px);
  }
  40% {
    transform: translate(-5px, -5px);
  }
  60% {
    transform: translate(5px, 5px);
  }
  80% {
    transform: translate(5px, -5px);
  }
  to {
    transform: translate(0);
  }
`;

export const rotate360ScaleFadeKeyframe = keyframes`
  from {
    transform: rotate(0) scale(1.5);
    opacity: 1;
  }
  to {
    transform: rotate(360deg) scale(1.5);
    opacity: 0;
  }
`;

export const rotateMinus360FadeKeyFrame = keyframes`
  from {
    transform: rotate(0);
    opacity: 1;
  }
  to {
    transform: rotate(-360deg);
    opacity: 0;
  }
`;

export const fadeInKeyframe = keyframes`
  from { opacity: 0 }
  to { opacity: 1}
`;

export const fadeOutKeyframe = keyframes`
  from { opacity: 1 }
  to { opacity: 0 }
`;

export const simpleShimmer = keyframes`
 0% {
    background: #d2d2d2;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  } 
  
  30% {
    background: #bfbfbf;
    background-repeat: no-repeat;
    background-size: 100%;
  }

  70% {
    background: #e7e7e7;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  }

  100% {
    background: #d2d2d2;
    background-repeat: no-repeat;
    background-size: 100% 100%;
  } 
`;
