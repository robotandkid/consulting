import { ThemeProvider } from "styled-components";
import React from "react";

export const defaultTheme = {
  components: {
    menu: {
      backgroundColor: "black",
    },
    externalAnchor: {
      color: "#6F685D",
      hover: "#6F685D",
    },
  },
};

type AllPartial<T> = { [P in keyof T]?: Partial<T[P]> };

/**
 * Consumers must implement this interface
 */
export interface ComponentThemeProps {
  theme: AllPartial<typeof defaultTheme>;
}

/**
 * Used by component-library viewer (cosmos, storybook, etc)
 */
export function AppThemeProvider(props: { children?: React.ReactNode }) {
  return <ThemeProvider theme={defaultTheme}>{props.children}</ThemeProvider>;
}
