import { useEffect, useState } from "react";
import { css } from "styled-components";

const size = {
  mobileL: 600,
  tablet: 768,
  laptop: 1920,
};

type MediaSize = keyof typeof size;

export const media =
  (mediaSize: MediaSize) => (args: ReturnType<typeof css>) => {
    return css`
      @media only screen and (min-width: ${size[mediaSize]}px) {
        ${args}
      }
    `;
  };

const getViewportWidth = () =>
  typeof window !== "undefined" ? window.innerWidth : 0;

export function useIsAtMostMediaSize(s: MediaSize, ssrValue = true) {
  const [isSize, setIsSize] = useState(ssrValue);

  useEffect(() => {
    function update() {
      setIsSize(getViewportWidth() < size[s]);
    }

    update();

    window.addEventListener("resize", update);

    return () => {
      window.removeEventListener("resize", update);
    };
  }, [s]);

  return isSize;
}
