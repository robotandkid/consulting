import styled, { css } from "styled-components";

export const normalizedAnchor = css`
  text-decoration: none;
  color: inherit;
  cursor: pointer;

  &:visited {
    color: inherit;
  }
`;

export const NormalizedAnchor = styled.a`
  ${normalizedAnchor}
`;

export const normalizeButton = css`
  outline: none;
  border: none;
`;

type HSL = [number, number, number];

/**
 * Prior art:
 * https://gregives.co.uk/blog/interpolating-colour-with-css/
 */
export const bgColorInterpolation = (start: HSL, end: HSL) => css`
  --progress: 0;
  --h-start: ${start[0]};
  --s-start: ${start[1]};
  --l-start: ${start[2]};
  --h-end: ${end[0]} --s-end: ${end[1]} --l-end: ${end[2]};

  background-color: hsl(
    calc(
      var(--h-start) + (var(--h-end) - var(--h-start)) * var(--progress) / 100
    ),
    calc(
      var(--s-start) + (var(--s-end) - var(--s-start)) * var(--progress) / 100
    ),
    calc(
      var(--l-start) + (var(--l-end) - var(--l-start)) * var(--progress) / 100
    )
  );
`;

export const colorInterpolation = (start: HSL, end: HSL) => css`
  --progress: 0;
  --h-start: ${start[0]};
  --s-start: ${start[1]};
  --l-start: ${start[2]};
  --h-end: ${end[0]} --s-end: ${end[1]} --l-end: ${end[2]};

  color: hsl(
    calc(
      var(--h-start) + (var(--h-end) - var(--h-start)) * var(--progress) / 100
    ),
    calc(
      var(--s-start) + (var(--s-end) - var(--s-start)) * var(--progress) / 100
    ),
    calc(
      var(--l-start) + (var(--l-end) - var(--l-start)) * var(--progress) / 100
    )
  );
`;
