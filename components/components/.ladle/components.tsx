import type { GlobalProvider } from "@ladle/react";
import { domAnimation, LazyMotion } from "framer-motion";
import React from "react";

export const Provider: GlobalProvider = ({ children, globalState }) => (
  <LazyMotion strict features={domAnimation}>
    {children}
  </LazyMotion>
);
