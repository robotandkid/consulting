export const dummyWords = [
  "worried",
  "spare",
  "cellar",
  "dress",
  "wish",
  "spurious",
  "writer",
  "grate",
  "frightening",
  "determined",
  "mysterious",
  "replace",
  "sea",
  "pizzas",
  "bawdy",
  "pink",
  "icky",
  "adventurous",
  "outstanding",
  "calculate",
  "ill",
  "squirrel",
  "ambiguous",
  "found",
  "tall",
  "kindly",
  "dashing",
  "earthquake",
  "deceive",
  "envious",
  "rampant",
  "flavor",
  "fortunate",
  "gun",
  "unable",
  "married",
  "powder",
  "thoughtful",
  "lip",
  "grey",
  "suffer",
  "mouth",
  "zephyr",
  "grab",
  "mean",
  "sweltering",
  "highfalutin",
  "bake",
  "overrated",
  "embarrassed",
] as const;

export const dummyPhrases = [
  `You Can't Teach an Old Dog New Tricks`,
  `It's Not All It's Cracked Up To Be`,
  `Mountain Out of a Molehill`,
  `Not the Sharpest Tool in the Shed`,
  `Wake Up Call`,
  `Wouldn't Harm a Fly`,
  `Scot-free`,
  `Cry Over Spilt Milk`,
  `Top Drawer`,
  `Son of a Gun`,
  `Two Down, One to Go`,
  `Quick and Dirty`,
  `Poke Fun At`,
  `Fish Out Of Water`,
  `Beating Around the Bush`,
  `Quality Time`,
  `Between a Rock and a Hard Place`,
  `Head Over Heels`,
  `Under the Weather`,
  `Happy as a Clam`,
  `Hard Pill to Swallow`,
  `A Piece of Cake`,
  `Greased Lightning`,
  `Lickety Split`,
  `Love Birds`,
  `Know the Ropes`,
  `No Ifs, Ands, or Buts`,
  `I Smell a Rat`,
  `Hear, Hear`,
  `My Cup of Tea`,
  `In the Red`,
  `Read 'Em and Weep`,
  `Elvis Has Left The Building`,
  `Tug of War`,
  `Tough It Out`,
  `Curiosity Killed The Cat`,
  `On the Ropes`,
  `You Can't Judge a Book By Its Cover`,
  `A Chip on Your Shoulder`,
  `Playing Possum`,
  `Foaming At The Mouth`,
  `Back to Square One`,
  `Jumping the Gun`,
  `Everything But The Kitchen Sink`,
  `Ugly Duckling`,
  `Knock Your Socks Off`,
  `All Greek To Me`,
  `Easy As Pie`,
  `Cry Wolf`,
  `Close But No Cigar`,
];

export const largeText = `
    Cupcake ipsum dolor sit amet chocolate bar muffin I love apple pie. Carrot
    cake tootsie roll lollipop dessert carrot cake gummi bears. Jelly beans
    toffee apple pie liquorice cotton candy cheesecake brownie I love croissant.
    Jujubes I love cake cupcake I love topping dessert I love sweet roll.
    Marshmallow macaroon sesame snaps croissant tootsie roll candy I love candy
    canes sweet. Cotton candy gingerbread cotton candy sesame snaps pie pastry
    donut sweet roll shortbread. Chocolate donut tiramisu soufflé candy canes
    marzipan. Bear claw I love sugar plum liquorice dessert bear claw. Liquorice
    sesame snaps halvah lollipop chupa chups oat cake donut dessert. Bonbon
    tiramisu powder I love tiramisu lemon drops. I love danish marshmallow
    pudding dessert lollipop jujubes. Bonbon I love halvah cookie pie chocolate
    bar ice cream. Tiramisu wafer cheesecake donut soufflé dragée icing sugar
    plum. Gummi bears cake bonbon chupa chups ice cream chocolate bar macaroon.
    Lemon drops icing macaroon wafer sesame snaps lollipop chocolate. Gummi
    bears pastry biscuit donut soufflé. Jelly-o gingerbread jelly cupcake oat
    cake danish carrot cake biscuit. Pastry I love chocolate cake cake chocolate
    bar tart croissant. I love danish jelly-o muffin shortbread cheesecake
    gummies cotton candy pie. Marzipan chocolate chocolate cake candy canes
    sweet. Jelly dessert jelly macaroon icing sugar plum chocolate cake sesame
    snaps. Icing dragée biscuit powder sugar plum dragée cookie I love
    croissant. Dragée donut fruitcake chocolate cake cake chocolate sugar plum.
    I love sugar plum liquorice tiramisu pastry muffin. Wafer marzipan cupcake
    liquorice candy canes lollipop I love. Gingerbread toffee fruitcake pastry
    cotton candy dessert toffee chupa chups. Brownie muffin jelly beans jujubes
    carrot cake I love icing halvah. Tiramisu candy sweet topping jelly-o donut.
    Powder cheesecake I love sweet lollipop chocolate cake jelly-o gummi bears.
    Oat cake gummi bears brownie shortbread halvah soufflé. Donut macaroon donut
    tiramisu icing carrot cake muffin sweet. Carrot cake fruitcake chupa chups
    cake chocolate lemon drops dessert. Croissant sweet sweet roll biscuit sweet
    macaroon cookie cake ice cream. Sweet roll shortbread halvah danish topping.
    I love pie cookie dessert I love chocolate bar apple pie. Candy I love
    biscuit bonbon cake. I love icing jujubes cupcake lemon drops. Candy canes
    cotton candy biscuit sweet roll jujubes muffin bonbon jelly beans macaroon.
    I love sesame snaps marshmallow gummies fruitcake fruitcake. Jelly sesame
    snaps caramels pastry marshmallow caramels croissant sesame snaps. Brownie
    powder lemon drops dessert sugar plum. Candy canes cupcake caramels lemon
    drops cupcake halvah cupcake cotton candy. I love chocolate cupcake
    chocolate bar pie. Sweet roll I love danish jujubes caramels. Chocolate cake
    oat cake I love I love dragée tootsie roll toffee croissant cake. Topping
    chocolate cake muffin chupa chups pudding. Bear claw jelly sesame snaps I
    love gummi bears marzipan I love tootsie roll fruitcake. Chupa chups
    chocolate bar gummies shortbread cupcake topping cotton candy gummies
    jujubes. Fruitcake wafer shortbread candy cotton candy. Liquorice I love pie
    I love sweet roll I love oat cake. Chocolate gummies croissant apple pie
    chocolate bar. Biscuit dessert gingerbread shortbread pie chupa chups.
    Soufflé powder cheesecake oat cake bonbon lemon drops muffin dessert
    brownie. Pastry candy carrot cake chocolate cake sesame snaps chocolate bar
    cake gingerbread. Biscuit I love halvah I love cake jelly. I love I love
    gummies pastry shortbread jelly-o sweet. Dragée croissant I love tart I love
    I love. Ice cream fruitcake jelly beans lemon drops gummies candy sweet
    cheesecake. Sugar plum I love soufflé topping icing apple pie. Icing oat
    cake oat cake chocolate chocolate bar. Candy canes fruitcake carrot cake
    donut brownie wafer bonbon I love biscuit. Icing topping cake cake chocolate
    I love. Sweet sugar plum sesame snaps pudding topping. Gummies jelly muffin
    I love brownie gummi bears fruitcake apple pie. Jelly tart biscuit ice cream
    donut pudding jelly-o cupcake. Chocolate lollipop pudding I love topping
    tart jelly beans. Candy canes I love jelly-o pastry pastry muffin
    gingerbread marshmallow donut. I love soufflé gummies halvah oat cake
    brownie chocolate marshmallow fruitcake.
`;
