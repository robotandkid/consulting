import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'mrpixel';
    src: url('/fonts/mrpixel.otf');
  }

  body {
    font-size: 62.5%;
    margin: 0;
    padding: 0;
  }
`;
