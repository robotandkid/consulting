# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.23.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.22.3...@robotandkid/components@0.23.0) (2024-05-10)


### Features

* **urielavalos:** added next chrono canvas ([8350f1f](https://gitlab.com/robotandkid/consulting/commit/8350f1fa6e8106d4e60a56a133890a82f7cd5f32))
* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





## [0.22.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.22.2...@robotandkid/components@0.22.3) (2023-09-19)

**Note:** Version bump only for package @robotandkid/components





## [0.22.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.22.1...@robotandkid/components@0.22.2) (2023-09-18)

**Note:** Version bump only for package @robotandkid/components





## [0.22.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.22.0...@robotandkid/components@0.22.1) (2023-09-04)


### Bug Fixes

* enabled stricer array access in TS ([8b89be7](https://gitlab.com/robotandkid/consulting/commit/8b89be793edef75156025f9b3ee4a7944a308dc0))





# [0.22.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.21.0...@robotandkid/components@0.22.0) (2023-08-07)


### Features

* upgraded node, pixi.js ([c06a0b4](https://gitlab.com/robotandkid/consulting/commit/c06a0b470991876a2763336d76a4272ca536af75))





# [0.21.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.20.2...@robotandkid/components@0.21.0) (2023-08-07)


### Bug Fixes

* **components:** exporting all files ([bf87a23](https://gitlab.com/robotandkid/consulting/commit/bf87a23262231be3966f2dbeaa707527cc6567f0))
* **components:** fixed ImageKit ([932bf65](https://gitlab.com/robotandkid/consulting/commit/932bf65af7f9c7647f81b7959ab16f41ab53c8c6))
* **components:** image blur works correctly ([0b3bd56](https://gitlab.com/robotandkid/consulting/commit/0b3bd5687ff7cdbb6123d91b20b75e8f56fb012a))
* **components:** no longer passing bgColor prop to DOM element ([71595a9](https://gitlab.com/robotandkid/consulting/commit/71595a9735449a5f1dc4b419ed67dc6250303c2f))


### Features

* **components:** added BodyUnderHamburger ([cae2015](https://gitlab.com/robotandkid/consulting/commit/cae20159ac4a6ec8575272a2baf0ddd7265bb3f3))
* **components:** added BodyUnderHamburger that works! ([4dabb8a](https://gitlab.com/robotandkid/consulting/commit/4dabb8a09e7c879e889e515e579753e614eebd52))
* **components:** added ladle ([0e1fd2d](https://gitlab.com/robotandkid/consulting/commit/0e1fd2d353162fcd76a3e3f9f7807c951d917ff5))
* **components:** added link scrollToTop ([8402256](https://gitlab.com/robotandkid/consulting/commit/84022567d1bf858772e3dbefdc28e20e16f3d47f))
* **components:** added normalize button css ([4ebf007](https://gitlab.com/robotandkid/consulting/commit/4ebf00703d81a216cc5905e2c41bf3967780c418))
* **components:** added offset workaround to BodyUnderHamburgerMenu ([36663c4](https://gitlab.com/robotandkid/consulting/commit/36663c43317abd490977b18cf4a40066890200ac))
* **components:** added useScrolling ([3859691](https://gitlab.com/robotandkid/consulting/commit/3859691ffc269c57951eba8569483525fd6d64f8))
* **components:** debouncing listener added ([e92d91e](https://gitlab.com/robotandkid/consulting/commit/e92d91e52b71f4c2d32dfa53bf8d6c5b91acb812))
* **components:** hamburger nav menu now has title support ([099770c](https://gitlab.com/robotandkid/consulting/commit/099770c543d666828bad1a8b64a3725dc9463030))
* **componetns:** added typography scales ([68928c3](https://gitlab.com/robotandkid/consulting/commit/68928c3cedd67c561307689e21b45e0d9cd74e7a))
* **digital-apps:** eli garcia published ([331476e](https://gitlab.com/robotandkid/consulting/commit/331476ecc8574bc6371c17268e674f68a6919c1e))
* **masonrpypro:** added call button ([45498f7](https://gitlab.com/robotandkid/consulting/commit/45498f79ce296738881f96f06f5e77b47a9860c7))
* **masonrypro:** added about ([8ef4b29](https://gitlab.com/robotandkid/consulting/commit/8ef4b29b8a84a0f0d0df4c552d9474eccbb5e8b1))
* **masonrypro:** added component LandingHero to title ([50646ea](https://gitlab.com/robotandkid/consulting/commit/50646ea3ea2afb0f2d91aa8d9f80b64e7a6a9c93))
* **masonrypro:** added hamburger icon color ([365289b](https://gitlab.com/robotandkid/consulting/commit/365289bbb9b0c00f2214afea4194da8821f7644a))
* **masonrypro:** added service header ([927f95f](https://gitlab.com/robotandkid/consulting/commit/927f95fdf54db9a9a8d4a79b82b0081076205e70))
* **masonrypro:** added services ([b712e60](https://gitlab.com/robotandkid/consulting/commit/b712e60193641ad24fe29066333c34489f8ddd0c))
* **masonrypro:** added the gallery ([c04a941](https://gitlab.com/robotandkid/consulting/commit/c04a941ecc2f4901904de7e03921d7859c978047))
* **masonrypro:** adjusted desktop version ([f5b7ecd](https://gitlab.com/robotandkid/consulting/commit/f5b7ecd6d5d8512c4a70dbb79462542584425554))
* **masonrypro:** getting there ([fbca453](https://gitlab.com/robotandkid/consulting/commit/fbca4533475f82819e50749e4341b09592553445))
* **masonrypro:** nav get quote button ([da5ab62](https://gitlab.com/robotandkid/consulting/commit/da5ab626709678960b6449ce32d9f52efa872024))
* **root:** added netlify nextjs plugin ([7d348bb](https://gitlab.com/robotandkid/consulting/commit/7d348bb3db7c27ec55e3aa33a94411ad49d8a8b0))
* **root:** added prettier jsdoc plugin ([a9c86fc](https://gitlab.com/robotandkid/consulting/commit/a9c86fc36756ee6398a6ea996c52c16846061cb5))





## [0.20.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.20.1...@robotandkid/components@0.20.2) (2022-11-30)


### Bug Fixes

* **leandertile:** fixed call button flickering bug on desktop ([b946b6a](https://gitlab.com/robotandkid/consulting/commit/b946b6a1dfc3a3c59b6083e24b283eb9b28bb0dd))





## [0.20.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.20.0...@robotandkid/components@0.20.1) (2022-11-28)

**Note:** Version bump only for package @robotandkid/components





# [0.20.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.19.0...@robotandkid/components@0.20.0) (2022-11-27)


### Bug Fixes

* **components:** useDetectScrolloing uses React state correctly ([fd93ce7](https://gitlab.com/robotandkid/consulting/commit/fd93ce7af802389b460c2cdbb6e79fdf85dfb96c))
* **root:** correctly building libs w/ TS ([2e793da](https://gitlab.com/robotandkid/consulting/commit/2e793da11f356607ca04e4579111c8f203d45fc9))
* **urielavalos:** correctly building component libraries ([2e2bf3f](https://gitlab.com/robotandkid/consulting/commit/2e2bf3fa85830cd4f16ad4de7196c70d500f0bc0))
* **urielavalos:** fixed landing bg animation ([ed4daa7](https://gitlab.com/robotandkid/consulting/commit/ed4daa724deef6ebb36f83390e8d9c4d814dafe4))
* **urielavalos:** using transient props ([c15c6d3](https://gitlab.com/robotandkid/consulting/commit/c15c6d39c053d8f2ce14983ca561231abc80dd56))


### Features

* **urielavalos:** reduced bundle size ([2964056](https://gitlab.com/robotandkid/consulting/commit/2964056f73168b847f811150bc536b30e86c3e46))





## [0.18.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.18.0...@robotandkid/components@0.18.1) (2022-01-09)


### Bug Fixes

* **components:** compiles again ([ca882ad](https://gitlab.com/robotandkid/consulting/commit/ca882ad1b2094b9d38d3114e7d171b2016930c5e))





# [0.18.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.17.0...@robotandkid/components@0.18.0) (2021-12-14)


### Features

* **components:** enabled tree-shaking ([2efc5fe](https://gitlab.com/robotandkid/consulting/commit/2efc5fe8256f5005ffd83c55c94a5d9c7ffdda03))





# [0.17.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.16.4...@robotandkid/components@0.17.0) (2021-12-03)


### Bug Fixes

* **components:** buton is an anchor, not a div ([1402283](https://gitlab.com/robotandkid/consulting/commit/140228371e798b38e9ae101af294e51fab552575))
* **components:** missing type ([30ae4e6](https://gitlab.com/robotandkid/consulting/commit/30ae4e69588ad2aa5cd90eaf0c1cc9455b0981c9))


### Features

* **components:** added button component ([fa6fe87](https://gitlab.com/robotandkid/consulting/commit/fa6fe8703f6cf3a428a4c8df05f62486603fd977))
* **components:** added new styles ([6ce682d](https://gitlab.com/robotandkid/consulting/commit/6ce682db054a58ae24c4679be09c9c451b73a1f7))
* **components:** fixed Button styles ([5b94613](https://gitlab.com/robotandkid/consulting/commit/5b9461368809f17255a2c459cf1e5f9295976766))
* **components:** updated ImageKit to accept endpoint folder ([f866a4b](https://gitlab.com/robotandkid/consulting/commit/f866a4bbd2b77c3dcd095c829780d456db9288c1))





## [0.16.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.16.3...@robotandkid/components@0.16.4) (2021-09-29)

**Note:** Version bump only for package @robotandkid/components





## [0.16.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.16.2...@robotandkid/components@0.16.3) (2021-09-29)

**Note:** Version bump only for package @robotandkid/components





## [0.16.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.16.1...@robotandkid/components@0.16.2) (2021-09-27)

**Note:** Version bump only for package @robotandkid/components





## [0.16.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.16.0...@robotandkid/components@0.16.1) (2021-08-04)

**Note:** Version bump only for package @robotandkid/components





# [0.16.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.15.3...@robotandkid/components@0.16.0) (2021-07-29)


### Features

* **components:** added useIsMobileOrTablet ([eee0f69](https://gitlab.com/robotandkid/consulting/commit/eee0f69d10773af4794cd046114be12edb0d5c9c))
* **leandertile:** added nav Call button ([67b15bc](https://gitlab.com/robotandkid/consulting/commit/67b15bc9e224873090473328a4a2d07c7c2bebc8))





## [0.15.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.15.2...@robotandkid/components@0.15.3) (2021-07-29)


### Bug Fixes

* **components:** compiles again ([64059bc](https://gitlab.com/robotandkid/consulting/commit/64059bcd6b7ea14d626c36ca6e04297dde1338e1))
* **components:** compiles again ([a141830](https://gitlab.com/robotandkid/consulting/commit/a1418308b7ac51c7a6dde9c3c8075d6bdd981f95))





## [0.15.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.15.1...@robotandkid/components@0.15.2) (2021-07-29)


### Bug Fixes

* **leandertile:** added landing hero placeholder ([e7ae6a4](https://gitlab.com/robotandkid/consulting/commit/e7ae6a4e48c3652a87597062d40c2d793aaaa5b6))





## [0.15.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.15.0...@robotandkid/components@0.15.1) (2021-07-29)


### Bug Fixes

* **components:** checks for React in scope ([5727036](https://gitlab.com/robotandkid/consulting/commit/57270363995bda5bacea413cad3780dab9c72531))
* **components:** facebookSEO adds itself to the head ([c34e697](https://gitlab.com/robotandkid/consulting/commit/c34e69770dd92d5e33be26557a0e4cde07e5499e))





# [0.15.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.14.1...@robotandkid/components@0.15.0) (2021-07-28)


### Features

* **components:** added FacebookSEO ([0538aac](https://gitlab.com/robotandkid/consulting/commit/0538aac09466f938fb90e60ad1b68d83786d60ba))





## [0.14.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.14.0...@robotandkid/components@0.14.1) (2021-07-27)

**Note:** Version bump only for package @robotandkid/components





# [0.14.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.13.0...@robotandkid/components@0.14.0) (2021-07-22)


### Bug Fixes

* **components:** removed silver background (accident) ([7233245](https://gitlab.com/robotandkid/consulting/commit/7233245ea5f2083d10b15dd5442aeb0a9c13d5e9))


### Features

* **components:** added media / updated grid styles ([99dd2d2](https://gitlab.com/robotandkid/consulting/commit/99dd2d276746ebb15fb7c83daa7bf264780ea9a1))
* **components:** added media / updated grid styles ([c6a308b](https://gitlab.com/robotandkid/consulting/commit/c6a308b4afaee1c8c4ca32040c1c9ccc4497886f))
* **urielavalos:** added Contact and h2 headings ([8e865d8](https://gitlab.com/robotandkid/consulting/commit/8e865d8534a9ef76606b09a6e49c3869104040b3))





# [0.13.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.12.0...@robotandkid/components@0.13.0) (2021-07-14)


### Features

* **components:** added TitleSEO ([3f6a86c](https://gitlab.com/robotandkid/consulting/commit/3f6a86c58e8f96dcfe46a01fdd63a6b9b9973d2a))





# [0.12.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.11.0...@robotandkid/components@0.12.0) (2021-07-13)


### Features

* **leandertile:** added structured data ([f6c9108](https://gitlab.com/robotandkid/consulting/commit/f6c9108fdd1d61eda6b3bc46a2758216b83a3455))





# [0.11.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.10.1...@robotandkid/components@0.11.0) (2021-07-01)


### Bug Fixes

* **component:** properly creating onA11y handler ([86ba00b](https://gitlab.com/robotandkid/consulting/commit/86ba00bf70e42976aa1485b0ee87a88cbffc2da9))
* **component:** properly creating onA11y handler ([caca8ef](https://gitlab.com/robotandkid/consulting/commit/caca8efb18be7d2770818b923a555341ed6b5089))


### Features

* **components:** added HamburgerMenuItems ([c952bd6](https://gitlab.com/robotandkid/consulting/commit/c952bd66d82b23365fad4b9df6e3ce6045669f15))
* **components:** added normalizedAnchor and ExternalLink ([56c98ce](https://gitlab.com/robotandkid/consulting/commit/56c98ce7a576ee25baf8e659b75c1821f6db876a))
* **components:** also added NormalizedAnchor component ([9559147](https://gitlab.com/robotandkid/consulting/commit/955914733d4fe64e7e1ce3322aa1a0282bdea65f))





## [0.10.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.10.0...@robotandkid/components@0.10.1) (2021-06-19)


### Bug Fixes

* **components:** hamburgerMenu navigation works on safari ([8b45a8a](https://gitlab.com/robotandkid/consulting/commit/8b45a8ab75edf8eaa6ff2f372e382d83405f05e9))





# [0.10.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.9.4...@robotandkid/components@0.10.0) (2021-06-19)


### Features

* **components:** hamburgerMenu uses framer motion for swipe anim ([36ac1c5](https://gitlab.com/robotandkid/consulting/commit/36ac1c5edfd1df3cb4bb8d94a8e2527e64a3552c))





## [0.9.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.9.3...@robotandkid/components@0.9.4) (2021-06-18)


### Bug Fixes

* **components:** fixed bug where skipping first item ([3837a9e](https://gitlab.com/robotandkid/consulting/commit/3837a9e34aab1964d38786df857542c9977dbf80))
* **components:** has the right type ([7a4c4ff](https://gitlab.com/robotandkid/consulting/commit/7a4c4ff6a16dad47def5027659f70ca0eaa55e16))





## [0.9.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.9.2...@robotandkid/components@0.9.3) (2021-06-17)


### Bug Fixes

* **components:** galleryGrid properly resizes on page swipe ([92582f5](https://gitlab.com/robotandkid/consulting/commit/92582f5afc5d3763811eb0f42de58e753838fd7b))





## [0.9.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.9.1...@robotandkid/components@0.9.2) (2021-06-16)


### Bug Fixes

* **components:** correctly generating dist ([3b23d69](https://gitlab.com/robotandkid/consulting/commit/3b23d695a1b38caf0a6b0936e7b29281ea0014af))





## [0.9.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.9.0...@robotandkid/components@0.9.1) (2021-06-15)


### Bug Fixes

* **components:** imageKit correctly uses endpoint ([cbfde6f](https://gitlab.com/robotandkid/consulting/commit/cbfde6f6c7646bb59f2d8f6f3a60bd50dab18299))





# [0.9.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.8.0...@robotandkid/components@0.9.0) (2021-06-15)


### Bug Fixes

* **components:** compile again ([3392bdb](https://gitlab.com/robotandkid/consulting/commit/3392bdb14d2fff34530dd35fe234b3801cb9e98b))


### Features

* **components:** added ImageKit ([c178f33](https://gitlab.com/robotandkid/consulting/commit/c178f338c2ddbf3651a08f7620fdcf3e2c9d0e6f))
* **components:** migrated to rollup ([ed0497f](https://gitlab.com/robotandkid/consulting/commit/ed0497feafc62dcb69d3ab7496fca1f1c01e15bf))





# [0.8.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.7.1...@robotandkid/components@0.8.0) (2021-06-03)


### Bug Fixes

* **components:** better theme style ([87eea58](https://gitlab.com/robotandkid/consulting/commit/87eea580e4c2755baed6d1a27a8b9eb1ba0c98c2))


### Features

* **components:** added parallax component ([d93d349](https://gitlab.com/robotandkid/consulting/commit/d93d34912734d697dfc50ce4f6089d6a4d180b90))





## [0.7.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.7.0...@robotandkid/components@0.7.1) (2021-05-17)


### Bug Fixes

* compiles again ([b3b01a4](https://gitlab.com/robotandkid/consulting/commit/b3b01a45cb3d0c230ef20e32a96d45fe8fc79c5a))





# [0.7.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.6.3...@robotandkid/components@0.7.0) (2021-05-15)


### Features

* **urielavalos:** added H2 styles ([3e1b567](https://gitlab.com/robotandkid/consulting/commit/3e1b5676e0bcbc4d8d62287fd1a445e0f534f57f))





## [0.6.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.6.2...@robotandkid/components@0.6.3) (2021-04-20)


### Bug Fixes

* downgraded react --- causing styled-components SSR bug ([7c6fc16](https://gitlab.com/robotandkid/consulting/commit/7c6fc16233d001207816be6da2567f45fba72104))





## [0.6.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.6.1...@robotandkid/components@0.6.2) (2021-04-16)


### Bug Fixes

* downgraded packages to fix styled-components SSR bug ([ae008cc](https://gitlab.com/robotandkid/consulting/commit/ae008ccd4bac666f113b7a710b1ca8b4327f2981))
* lints again ([eec46f1](https://gitlab.com/robotandkid/consulting/commit/eec46f155cafd2d4c5881cf62359482cb9186cd9))





## [0.6.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.6.0...@robotandkid/components@0.6.1) (2021-04-02)

### Bug Fixes

- **components:** compiles again ([7ec5859](https://gitlab.com/robotandkid/consulting/commit/7ec585902a960802d879273b52673d1335dea53e))

# [0.6.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.5.1...@robotandkid/components@0.6.0) (2021-03-30)

### Bug Fixes

- **components/basicgallerygrid:** grid bug where gallery didn't size properly on renders ([0a20ace](https://gitlab.com/robotandkid/consulting/commit/0a20ace30c0cc12e9b2683a2114c4874432aef30))
- **components/gallerygrid:** fixed initial render bug ([7eddd39](https://gitlab.com/robotandkid/consulting/commit/7eddd39e2599ffccb3c2deea09db7b6e52b000a5))

### Features

- **components/statictoc:** fully working ([dc1d9a7](https://gitlab.com/robotandkid/consulting/commit/dc1d9a79eedc6b27d327a2ef1901cbc4748a3aef))

## [0.5.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.5.0...@robotandkid/components@0.5.1) (2021-03-26)

### Bug Fixes

- lints again ([deb4427](https://gitlab.com/robotandkid/consulting/commit/deb44276affe2dd5990a953af61a272ca5d1a5ed))

# [0.5.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.4.0...@robotandkid/components@0.5.0) (2021-03-10)

### Bug Fixes

- **components:** landing component uses reduced hero when not on title ([f519585](https://gitlab.com/robotandkid/consulting/commit/f519585112b71c23c99fee0c92abb7070d6fc2c9))
- **components:** properly handling initial render ([770dea6](https://gitlab.com/robotandkid/consulting/commit/770dea6fdee413a0bd3e321af31b73ad02d0ad8f))
- **components:** use correct z-index in GalleryGrid ([0f70603](https://gitlab.com/robotandkid/consulting/commit/0f706036001dd8cde9bd45baada0c9afb923b16e))

### Features

- **components:** galleryGrid hides pager when not enough pages ([2790843](https://gitlab.com/robotandkid/consulting/commit/279084325b746de2de93d285a3a5a8135e4326bb))

# [0.4.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.3.0...@robotandkid/components@0.4.0) (2021-02-27)

### Bug Fixes

- **components:** exporting itemAsTypography ([e1042a8](https://gitlab.com/robotandkid/consulting/commit/e1042a8cd86e305ee61a10b656d7d4faddfc6c4f))
- **components:** properly style Pager component ([1cceb80](https://gitlab.com/robotandkid/consulting/commit/1cceb800d51329e33ba94c988b2c639753258fb0))

### Features

- **components:** added typescript/Unpromisify ([8dc5c8c](https://gitlab.com/robotandkid/consulting/commit/8dc5c8c367fc7e5fe0b0a6736d3b11bb92edf376))

# [0.3.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.2.1...@robotandkid/components@0.3.0) (2021-02-26)

### Bug Fixes

- **components:** also expored GalleryPagerInfo ([89dc9cd](https://gitlab.com/robotandkid/consulting/commit/89dc9cd52d9794d675e3463e5080c97f666f4e19))
- **components:** properly calculate the page window in the InMemoryGalleryPager ([7dd5e4e](https://gitlab.com/robotandkid/consulting/commit/7dd5e4e8d8d7aa37d45cef281bf9098011287ca9))
- **components:** properly using Fragment pragma ([23011e2](https://gitlab.com/robotandkid/consulting/commit/23011e2ebdd4d3a824fc1490066cbcff2b54966c))

### Features

- **components:** added utils/noop ([9470d9c](https://gitlab.com/robotandkid/consulting/commit/9470d9c3e61afed5e6e305c53fc09d7be7c4501c))
- **components:** created BasicGalleryGrid ([e738815](https://gitlab.com/robotandkid/consulting/commit/e7388150637bfba2cc31178fddb2382b66dd5f05))
- **components:** inMemoryGalleryPager ([64cde45](https://gitlab.com/robotandkid/consulting/commit/64cde45a43e751c383e7366dcfc2693885ea8234))

## [0.2.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.2.0...@robotandkid/components@0.2.1) (2021-02-07)

**Note:** Version bump only for package @robotandkid/components

# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.1.5...@robotandkid/components@0.2.0) (2021-02-06)

### Bug Fixes

- **components:** builds correctly ([32c8962](https://gitlab.com/robotandkid/consulting/commit/32c896208a2a2099e8fab04b0903ad42501cf107))

### Features

- **components:** exported HamburgerMenuButton + docs ([02b5210](https://gitlab.com/robotandkid/consulting/commit/02b5210bdc27abe6ac248d3ad774f55ecc36f063))

## [0.1.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.1.4...@robotandkid/components@0.1.5) (2021-02-06)

### Bug Fixes

- **components:** really fixed react issue ([d6bf827](https://gitlab.com/robotandkid/consulting/commit/d6bf8278c2d86532af12c0647e29ce6900fa68f9))

## [0.1.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.1.3...@robotandkid/components@0.1.4) (2021-02-06)

### Bug Fixes

- **components:** bundling correctly ([1c2099b](https://gitlab.com/robotandkid/consulting/commit/1c2099b6478a7932298fab13581261462120e879))

## [0.1.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.1.2...@robotandkid/components@0.1.3) (2021-02-05)

**Note:** Version bump only for package @robotandkid/components

## [0.1.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.1.1...@robotandkid/components@0.1.2) (2021-02-05)

**Note:** Version bump only for package @robotandkid/components

## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.1.0...@robotandkid/components@0.1.1) (2021-02-05)

**Note:** Version bump only for package @robotandkid/components

# [0.1.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/components@0.0.3...@robotandkid/components@0.1.0) (2021-02-04)

### Bug Fixes

- **gallerygrid:** resizing gallery correctly on item change ([1700824](https://gitlab.com/robotandkid/consulting/commit/170082409840757f2d5a27b27dcf0a27db9949f3))

### Features

- added framer motion ([4ad34cd](https://gitlab.com/robotandkid/consulting/commit/4ad34cd42296e0a415d5adcfb9dbfaa2154088a8))
- **a11y:** hamburgermenu ([e7b7188](https://gitlab.com/robotandkid/consulting/commit/e7b718857758fd859949ac7867d19e5fc447f3e5))
- **components/gallerypager:** added ([7fc2799](https://gitlab.com/robotandkid/consulting/commit/7fc27995f01f728b34d10007f1fcf29d46b6134f))
- cleaned up component and added item styling ([458b9bb](https://gitlab.com/robotandkid/consulting/commit/458b9bb94fce601b15b7d26f048536c69d2e33b9))
- **gallerpager:** working pager ([656243d](https://gitlab.com/robotandkid/consulting/commit/656243d33f3c9abf50ea94f96200569f2fea9079))
- **gallerygrid:** added ([43847d3](https://gitlab.com/robotandkid/consulting/commit/43847d3147e5d6fa20c5822928e9ae7c5689e0c9))
- **gallerygrid:** automatically tracks height of gallery ([579bc3b](https://gitlab.com/robotandkid/consulting/commit/579bc3bc387ee71e087c11ef9311b3abc336f6d4))
- **hamburgermenubutton:** using framer motion ([ed61f03](https://gitlab.com/robotandkid/consulting/commit/ed61f033e3e42393dd91609618096dae5be539dc))
- **hamburgermenuicon:** created ([e32f41d](https://gitlab.com/robotandkid/consulting/commit/e32f41d61a8db4647806a29ff0f725aa77d03d78))
