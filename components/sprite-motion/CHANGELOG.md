# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.45.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.44.0...@robotandkid/sprite-motion@0.45.0) (2024-10-20)


### Features

* **sprite-motion:** added ability for video to play sounds ([fcd5623](https://gitlab.com/robotandkid/consulting/commit/fcd56239b8b3b43990ec5ae58a6d8b840d7e9470))





# [0.44.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.43.1...@robotandkid/sprite-motion@0.44.0) (2024-10-05)


### Bug Fixes

* **sprite-motion:** canvas div properly rotating ([efb71f1](https://gitlab.com/robotandkid/consulting/commit/efb71f1aea11789f5d95c4f9dcf830896e322242))


### Features

* **sprite-motion:** add tile ability ([9655987](https://gitlab.com/robotandkid/consulting/commit/9655987690616a37bb1c8814b72622d4bcae1fe8))
* **sprite-motion:** added chasm palette ([c95ab81](https://gitlab.com/robotandkid/consulting/commit/c95ab81dd8d5319021bfd027e603addc4df62c6a))
* **sprite-motion:** gave div rotation powers + fixed a bug ([e234306](https://gitlab.com/robotandkid/consulting/commit/e2343068e69e8afa0341885f23b71acb95083ac8))





## [0.43.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.43.0...@robotandkid/sprite-motion@0.43.1) (2024-10-04)


### Bug Fixes

* **urielavalos:** compiles again ([c5fc228](https://gitlab.com/robotandkid/consulting/commit/c5fc228ea4ea01890f21eaeb19448a9178feb291))





# [0.43.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.42.1...@robotandkid/sprite-motion@0.43.0) (2024-10-03)


### Features

* **sprite-motion:** added DivAlignLeft with rotation ([f483dbb](https://gitlab.com/robotandkid/consulting/commit/f483dbb61cfea4de2f2d89672fbdb431d24dbf55))





## [0.42.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.42.0...@robotandkid/sprite-motion@0.42.1) (2024-10-01)


### Bug Fixes

* **sprite-motion:** chromata compiles again ([bbb0005](https://gitlab.com/robotandkid/consulting/commit/bbb0005754565236cfc8c1e32d5e945fd3f5fa17))





# [0.42.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.41.0...@robotandkid/sprite-motion@0.42.0) (2024-10-01)


### Bug Fixes

* **sprite-motion:** canvas div justified full works ([c7a72a9](https://gitlab.com/robotandkid/consulting/commit/c7a72a9577fbfb0df55015196b4e176163031670))
* **sprite-motion:** chromata cleans up itself when it finishes rendering ([10d6b65](https://gitlab.com/robotandkid/consulting/commit/10d6b654a31ada635f98fee4398f03f74f22ed1f))


### Features

* **sprite-motion:** chromata now supports onIterationDone ([fa20276](https://gitlab.com/robotandkid/consulting/commit/fa202767ddd9f13a75916bb11657f340622a1c69))





# [0.41.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.40.0...@robotandkid/sprite-motion@0.41.0) (2024-09-28)


### Features

* **sprite-motion:** added canvas-div ([12db897](https://gitlab.com/robotandkid/consulting/commit/12db89787e4141795cb6da35a4d5ca09d42e3889))





# [0.40.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.39.0...@robotandkid/sprite-motion@0.40.0) (2024-09-27)


### Features

* **sprite-motion:** added ability to disable auto-clearing of canvas ([4c0a0da](https://gitlab.com/robotandkid/consulting/commit/4c0a0da60d19ad2a665248ce0c10050f71207e60))
* **sprite-motion:** faster video loads ([e2bb933](https://gitlab.com/robotandkid/consulting/commit/e2bb933f61c1e4208255cfd3862275fee9ab1938))





# [0.39.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.38.2...@robotandkid/sprite-motion@0.39.0) (2024-08-17)


### Features

* **sprite-motion:** added more palettes ([4b09580](https://gitlab.com/robotandkid/consulting/commit/4b095807a14589a44fa60c3f00bea77706b7fb0f))





## [0.38.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.38.1...@robotandkid/sprite-motion@0.38.2) (2024-08-08)


### Bug Fixes

* **sprite-motion:** build lib ([1cd0c88](https://gitlab.com/robotandkid/consulting/commit/1cd0c88b8a874c587905bd9409bcf61e6ccd399b))
* **sprite-motion:** compiles again ([7f9241f](https://gitlab.com/robotandkid/consulting/commit/7f9241f4ac4abbe50ca8dfab8c4e161c7e6a1b0f))





## [0.38.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.38.0...@robotandkid/sprite-motion@0.38.1) (2024-08-08)


### Bug Fixes

* **sprite-motion:** compiles again ([fd697c1](https://gitlab.com/robotandkid/consulting/commit/fd697c1a857ee19425d4e9749e6d868089360443))





# [0.38.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.37.0...@robotandkid/sprite-motion@0.38.0) (2024-08-08)


### Features

* **sprite-motion:** added a new palette + better typings for Code ([ea0a4ca](https://gitlab.com/robotandkid/consulting/commit/ea0a4ca71bab0a6cf1ca9a91f5ffe36777e93d9d))





# [0.37.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.36.0...@robotandkid/sprite-motion@0.37.0) (2024-07-25)


### Features

* **sprite-motion:** added new ascii components ([2206df1](https://gitlab.com/robotandkid/consulting/commit/2206df106e808ddbfa49ff6edbce27b284734269))





# [0.36.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.35.0...@robotandkid/sprite-motion@0.36.0) (2024-07-16)


### Features

* **sprite-motion:** added blur ([0bcb37d](https://gitlab.com/robotandkid/consulting/commit/0bcb37d33615057a0bc333298894203565e2d977))





# [0.35.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.34.0...@robotandkid/sprite-motion@0.35.0) (2024-06-30)


### Features

* **sprite-motion:** made corner optional in speach bubble ([a58392f](https://gitlab.com/robotandkid/consulting/commit/a58392ffdcd1a1003d5a145914edeeeff26ab3f9))





# [0.34.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.33.0...@robotandkid/sprite-motion@0.34.0) (2024-06-28)


### Features

* **sprite-motion:** added another palette ([c0a70fc](https://gitlab.com/robotandkid/consulting/commit/c0a70fce31febad88ad7a3e5e209e28cb7a63dec))
* **sprite-motion:** added State Step component ([2a29c13](https://gitlab.com/robotandkid/consulting/commit/2a29c13abc323dae8eb7bf1851402b8e9dd41b47))
* **sprite-motion:** latest ([b263b60](https://gitlab.com/robotandkid/consulting/commit/b263b604ab92c1dd078ba45c21e4fa783e334525))
* **sprite-motion:** pulled in a very risky change from the portal ([82c8156](https://gitlab.com/robotandkid/consulting/commit/82c81568db1221d4147b3363489e54d67555c9d3))
* **urielavalos:** built the very risky version ([be5b0c4](https://gitlab.com/robotandkid/consulting/commit/be5b0c436ebf68707c0d2774795e47f254762459))





# [0.33.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.32.0...@robotandkid/sprite-motion@0.33.0) (2024-06-22)


### Features

* **urielavalos:** added Summer Traffic ([a5f7664](https://gitlab.com/robotandkid/consulting/commit/a5f76645633bee275866772061d4b51c11dcd7d4))





# [0.32.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.31.0...@robotandkid/sprite-motion@0.32.0) (2024-06-17)


### Bug Fixes

* **sprite-motion:** code properly checks for deps ([de82706](https://gitlab.com/robotandkid/consulting/commit/de827068dfe9659cc1f869683f2ac5c1a0b2d064))
* **sprite-motion:** sprite sheet from many properly renders height/width ([46e3cd5](https://gitlab.com/robotandkid/consulting/commit/46e3cd53c766f676f7e9cf40926be6e98989497c))


### Features

* **sprite-motion:** added auto start to save video ([152d54b](https://gitlab.com/robotandkid/consulting/commit/152d54b058dfc5d772e5a5cf49d4311080d0152b))
* **sprite-motion:** added save-video ([c20244a](https://gitlab.com/robotandkid/consulting/commit/c20244a9406c91fe37bb7f4b54c789e98e7bbf52))
* **sprite-motion:** added sprite sheet from many ([451642b](https://gitlab.com/robotandkid/consulting/commit/451642bb7dd0368a81fe02cf2fe53be709d4f376))





# [0.31.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.30.0...@robotandkid/sprite-motion@0.31.0) (2024-06-14)


### Features

* **sprite-motion:** added save image ([6bdc1f1](https://gitlab.com/robotandkid/consulting/commit/6bdc1f10542434673bf3862d77cbd1a5bf10504c))
* **urielavalos:** added t-shirt design [#1](https://gitlab.com/robotandkid/consulting/issues/1) ([2c755a0](https://gitlab.com/robotandkid/consulting/commit/2c755a00aca77f84b340c0ac9af3f9e05bc131cc))





# [0.30.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.29.0...@robotandkid/sprite-motion@0.30.0) (2024-06-09)


### Features

* **sprite-motion:** added dithering ([bd04fb6](https://gitlab.com/robotandkid/consulting/commit/bd04fb6f2b64dba5a4de4cf110c06e77d85e0bcf))
* **sprite-motion:** added speech-bubble ([5e1446f](https://gitlab.com/robotandkid/consulting/commit/5e1446fa216135966c89d9617ca88957fc3577df))





# [0.29.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.28.0...@robotandkid/sprite-motion@0.29.0) (2024-06-06)


### Features

* **urielavalos:** added anniversary flowers to love in motion ([41a271b](https://gitlab.com/robotandkid/consulting/commit/41a271bdbdc28688fc94e438adf496a68e481c66))





# [0.28.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.27.0...@robotandkid/sprite-motion@0.28.0) (2024-06-06)


### Bug Fixes

* **chromata:** compiles again ([d57668e](https://gitlab.com/robotandkid/consulting/commit/d57668e66f83c024132cef7b7991d029281d38ac))


### Features

* **chromata:** added full color support ([3ed52ea](https://gitlab.com/robotandkid/consulting/commit/3ed52ea242ab040c4329ade9468b9d9cfbfcd269))
* **sprite-motion:** added the beginnings of color support ([b6ceb05](https://gitlab.com/robotandkid/consulting/commit/b6ceb05cb664fbaebcf6da0bfe0b24c90eb718cd))





# [0.27.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.26.1...@robotandkid/sprite-motion@0.27.0) (2024-06-05)


### Features

* **chromata:** added more changes ([1b82359](https://gitlab.com/robotandkid/consulting/commit/1b823595371018c30715804fc7acfff19811abd1))
* **chromata:** added more changes ([d942dca](https://gitlab.com/robotandkid/consulting/commit/d942dca9a99acf491813604f35f64648dfadaf4d))
* **chromata:** latest ([2df18bb](https://gitlab.com/robotandkid/consulting/commit/2df18bb17ddfc9e18ca1a4fd27d9ae3c98d4ca46))
* **urielavalos:** added Summer Love ([dd18c0f](https://gitlab.com/robotandkid/consulting/commit/dd18c0f5d8653018c795110f0c78becad9caf0ac))





## [0.26.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.26.0...@robotandkid/sprite-motion@0.26.1) (2024-05-20)

**Note:** Version bump only for package @robotandkid/sprite-motion





# [0.26.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.23.0...@robotandkid/sprite-motion@0.26.0) (2024-05-17)


### Features

* **sprite-motion:** added staticimg ref support + chromata ([1b4d157](https://gitlab.com/robotandkid/consulting/commit/1b4d157d91526c4af2a48201a23900b7ea628839))
* **sprite-motion:** chromata => TS ([b4b6200](https://gitlab.com/robotandkid/consulting/commit/b4b620083ea2703fa2e9480c4f7e9e2e37489524))
* **sprite-motion:** chromata has less ts-ignores ([8ce5d7f](https://gitlab.com/robotandkid/consulting/commit/8ce5d7fcccd32f623227ac1694f39456417b878c))
* **sprite-motoin:** brought in various bug fixes/improvements from the other repo ([c7cc6ae](https://gitlab.com/robotandkid/consulting/commit/c7cc6aeb7f725ec55d7044167915030735b6aa86))
* **spritemotion:** added chromata ([eb9e60a](https://gitlab.com/robotandkid/consulting/commit/eb9e60a6fbf7fb4615ca8c3106ab9807160a8cb5))
* **urielavalos:** added a bunch of stuff ([d160ee5](https://gitlab.com/robotandkid/consulting/commit/d160ee54624fb17b1c2c47259c6d65cfd413a11f))





# [0.25.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.23.0...@robotandkid/sprite-motion@0.25.0) (2024-05-17)


### Features

* **sprite-motion:** added staticimg ref support + chromata ([1b4d157](https://gitlab.com/robotandkid/consulting/commit/1b4d157d91526c4af2a48201a23900b7ea628839))
* **sprite-motion:** chromata => TS ([b4b6200](https://gitlab.com/robotandkid/consulting/commit/b4b620083ea2703fa2e9480c4f7e9e2e37489524))
* **sprite-motion:** chromata has less ts-ignores ([8ce5d7f](https://gitlab.com/robotandkid/consulting/commit/8ce5d7fcccd32f623227ac1694f39456417b878c))
* **sprite-motoin:** brought in various bug fixes/improvements from the other repo ([c7cc6ae](https://gitlab.com/robotandkid/consulting/commit/c7cc6aeb7f725ec55d7044167915030735b6aa86))
* **spritemotion:** added chromata ([eb9e60a](https://gitlab.com/robotandkid/consulting/commit/eb9e60a6fbf7fb4615ca8c3106ab9807160a8cb5))
* **urielavalos:** added a bunch of stuff ([d160ee5](https://gitlab.com/robotandkid/consulting/commit/d160ee54624fb17b1c2c47259c6d65cfd413a11f))





# [0.24.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.23.0...@robotandkid/sprite-motion@0.24.0) (2024-05-15)


### Features

* **sprite-motion:** added staticimg ref support + chromata ([703fef2](https://gitlab.com/robotandkid/consulting/commit/703fef2ccec47bf75d96b0cb715c5841c810b439))
* **sprite-motion:** chromata has less ts-ignores ([b642f3a](https://gitlab.com/robotandkid/consulting/commit/b642f3a47161f4f185e9a3b93480fb49419f72c8))
* **sprite-motoin:** brought in various bug fixes/improvements from the other repo ([7eb0dbd](https://gitlab.com/robotandkid/consulting/commit/7eb0dbd2e515dc51268af088c0dab41d9fecf3ab))
* **spritemotion:** added chromata ([8adb9fa](https://gitlab.com/robotandkid/consulting/commit/8adb9fa98715b10437cf2076c4b022249c7c8e70))





# [0.23.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.22.0...@robotandkid/sprite-motion@0.23.0) (2024-05-10)


### Features

* **panda:** upgraded to newer version ([a12c630](https://gitlab.com/robotandkid/consulting/commit/a12c6308b8028227e7ecac8e82d282430543339d))
* **sprite-motion:** added simple transition support ([504578b](https://gitlab.com/robotandkid/consulting/commit/504578b38a549a8482e4439256bc94f7aeefea2e))
* **sprite-motion:** using latest ([662ffff](https://gitlab.com/robotandkid/consulting/commit/662ffff37fe605cdb84aa507c3d4d128f3c67d16))
* **urielavalos:** added wwa ([78b5337](https://gitlab.com/robotandkid/consulting/commit/78b5337cf6d2689a908d8b451c37b15d0dd7260f))
* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





# [0.22.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.21.1...@robotandkid/sprite-motion@0.22.0) (2024-04-22)


### Bug Fixes

* **sprite-motion:** ok reactive prop finally fixed ([f9ce64f](https://gitlab.com/robotandkid/consulting/commit/f9ce64fe080ef93beb2634f5859d50fd61d552cf))
* **sprite-motion:** ui dialog component now has a reactive property ([bf19753](https://gitlab.com/robotandkid/consulting/commit/bf19753a1b47d7c24d879654215978f0b8e9ef68))


### Features

* **sprite-motion:** added dynamic version of ui panel component ([0aa977b](https://gitlab.com/robotandkid/consulting/commit/0aa977b14522cbe908df73810c0aae3d7db24b8c))
* **sprite-motion:** updated timeouts ([e07aaff](https://gitlab.com/robotandkid/consulting/commit/e07aaffc195f3a3407589ded066d7a5862665e60))
* **spritemotion:** added 1bit component ([95b7d8c](https://gitlab.com/robotandkid/consulting/commit/95b7d8cc0ebe3bd400e2c54fa747db1a41bad0b5))
* **urielavalos:** updated Portal and more ([5632f32](https://gitlab.com/robotandkid/consulting/commit/5632f32d3d284b820022c95fb11d55003ac2ade5))





## [0.21.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.21.0...@robotandkid/sprite-motion@0.21.1) (2024-04-08)


### Bug Fixes

* **sprite-moton:** marked dims in Canvas as optional ([8ffaa25](https://gitlab.com/robotandkid/consulting/commit/8ffaa250ce2def0a01558ee64313722d265db11e))
* **urielavalos:** fixed self-portrait ([5f24c57](https://gitlab.com/robotandkid/consulting/commit/5f24c5791153d520b225406acb57f6e4c16a69d4))





# [0.21.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.20.0...@robotandkid/sprite-motion@0.21.0) (2024-04-06)


### Bug Fixes

* **sprite-motion:** improved ascii colors ([d8cde5b](https://gitlab.com/robotandkid/consulting/commit/d8cde5bc567cf11f883b653a9a5e2d1f6286583a))


### Features

* **sprite-motion:** added better color support for ascii ([7e2a011](https://gitlab.com/robotandkid/consulting/commit/7e2a0116f30acd26adba3989f80ea7bfadee38f8))
* **sprite-motion:** ascii supports custom bg colors ([c2513ec](https://gitlab.com/robotandkid/consulting/commit/c2513ecbc99ac587909a9c775a77f8a96de17187))





# [0.20.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.19.0...@robotandkid/sprite-motion@0.20.0) (2024-03-31)


### Features

* **sprite-motion:** added image/ascii components ([8844101](https://gitlab.com/robotandkid/consulting/commit/8844101321ceea8318d53d6c0798510adbed4fee))
* **urielavalos:** added latest comic ([6450ea4](https://gitlab.com/robotandkid/consulting/commit/6450ea47d8c52a5b5f897e9d140447e44cd0c231))
* **urielavalos:** added self-portrait ([9fc64f3](https://gitlab.com/robotandkid/consulting/commit/9fc64f3d8433a65d376d9dd9b5334a2da0aa869b))





# [0.19.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.18.0...@robotandkid/sprite-motion@0.19.0) (2024-03-05)


### Features

* **sprite-motion:** added complex video ([fba6831](https://gitlab.com/robotandkid/consulting/commit/fba683111ea4d9455428b40ad917a410b6c434d0))
* **urielavalos:** added latest comic Welcome to the technocracy ([41c777b](https://gitlab.com/robotandkid/consulting/commit/41c777b95f48d3269b60d23347ae641154c32730))





# [0.18.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.17.0...@robotandkid/sprite-motion@0.18.0) (2024-03-04)


### Features

* **sprite-motion:** added Noise ([59c8a89](https://gitlab.com/robotandkid/consulting/commit/59c8a8962c112049e5cfe280a419e0bcb63776a4))





# [0.17.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.16.0...@robotandkid/sprite-motion@0.17.0) (2024-02-24)


### Bug Fixes

* **sprite-motion:** fixed SpriteSheet onLoopEnd bug ([b9e9213](https://gitlab.com/robotandkid/consulting/commit/b9e9213cf0a7879e92a907a1f729b05de369f189))
* **sprite-motion:** sprite sheet component supports transparency ([6c8500c](https://gitlab.com/robotandkid/consulting/commit/6c8500c839dba0cc417077a005f9b9927a490d99))


### Features

* **sprite-motion:** added Code ([6f8dc3e](https://gitlab.com/robotandkid/consulting/commit/6f8dc3e7cba76f5cd261a7e9e338f14a9c937a49))
* **sprite-motion:** added film-grain ([5aedf08](https://gitlab.com/robotandkid/consulting/commit/5aedf08b58f8c9dc93da524d14eb454657384d42))
* **sprite-motion:** added key listener hook helper ([81e36e5](https://gitlab.com/robotandkid/consulting/commit/81e36e5f6d0dd14a06db52ca61d2b835981f1973))
* **sprite-motion:** added oval-iris effect ([36a4243](https://gitlab.com/robotandkid/consulting/commit/36a42437cad942a9c6b804a5bd6ba90248f49e9f))
* **sprite-motion:** added working game rendering engine ([1990926](https://gitlab.com/robotandkid/consulting/commit/1990926463d8b72c13c0636d277422205518782c))
* **sprite-motion:** motionDiv - added more matter-js options ([bd7dca1](https://gitlab.com/robotandkid/consulting/commit/bd7dca1a49ff9ca868db5e5cd418786a1bd574c5))
* **sprite-motion:** oval-iris supports color ([0310cff](https://gitlab.com/robotandkid/consulting/commit/0310cff5b5d726cfbebc013a4571fab17e2d9a3f))
* **urielavalos:** added Do we live in a simulation? ([a886c5b](https://gitlab.com/robotandkid/consulting/commit/a886c5b15a170e7eaa338e6b5ae35e9b1fe1eaf8))
* **urielavalos:** added I have visual snow ([64a4997](https://gitlab.com/robotandkid/consulting/commit/64a4997c928052f6381ae5509d77d8c11e99f675))
* **urielavalos:** added Sometimes you just have to rest ([1eebdb5](https://gitlab.com/robotandkid/consulting/commit/1eebdb5a2bedfa1a6d30adf887a0401a60a7a094))





# [0.16.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.15.0...@robotandkid/sprite-motion@0.16.0) (2024-01-27)


### Bug Fixes

* **sprite-motion:** dialog uses Pre to render lines ([0584a5b](https://gitlab.com/robotandkid/consulting/commit/0584a5b96aa3103f98158e7e36f61a5e726e8b9a))
* **sprite-motion:** fixed bug in dialog ui ([990002d](https://gitlab.com/robotandkid/consulting/commit/990002d127e9405e291e6ce045eb7c292910493c))


### Features

* **sprite-motion:** added command sub menu ([a97b10f](https://gitlab.com/robotandkid/consulting/commit/a97b10f1d409a122c831394d6d629c48d190db19))
* **sprite-motion:** added game ui ([6e9dc9a](https://gitlab.com/robotandkid/consulting/commit/6e9dc9a51a224e4209b319980072be41a56b66ab))
* **sprite-motion:** added player info panel ([13934e7](https://gitlab.com/robotandkid/consulting/commit/13934e7cd6c8f6c64f97deef9978cefa30e8bd1e))
* **sprite-motion:** added working ui dialog ([48339d4](https://gitlab.com/robotandkid/consulting/commit/48339d4dc2f84c2e675ffb76ee4df3511a890f77))
* **urielavalos:** added Bigmac with a side of murder ([a0dbcff](https://gitlab.com/robotandkid/consulting/commit/a0dbcff97fb0b6687b74503a9a86c60fc5e17b11))





# [0.15.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.14.0...@robotandkid/sprite-motion@0.15.0) (2024-01-14)


### Features

* **sprite-motion:** added video support ([a27534c](https://gitlab.com/robotandkid/consulting/commit/a27534c319bad108864b9fc6234681712a8fc916))
* **sprite-motion:** canvas ctx suppoert + raw text loader ([cba357f](https://gitlab.com/robotandkid/consulting/commit/cba357fbd59105277723cc07c60c12b3f2c74ba1))





# [0.14.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.13.1...@robotandkid/sprite-motion@0.14.0) (2023-12-21)


### Features

* **spritemotion:** spriteSheet now supports loop/onLoopEnd ([bc62ffd](https://gitlab.com/robotandkid/consulting/commit/bc62ffd5410957b6f3ff52ee73f4460c2096647d))





## [0.13.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.13.0...@robotandkid/sprite-motion@0.13.1) (2023-12-06)


### Bug Fixes

* **spritemotion:** compiles again ([ec5068d](https://gitlab.com/robotandkid/consulting/commit/ec5068de7326d7feb45e8b27cc2ca004ac919bea))





# [0.13.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.12.0...@robotandkid/sprite-motion@0.13.0) (2023-12-06)


### Bug Fixes

* **spritemotion:** panel has correct props + img comp accepts GIF ([3c9baeb](https://gitlab.com/robotandkid/consulting/commit/3c9baeb4b0c7ae269c870ba6b191eb49d156ca4c))


### Features

* **spritemotion:** exporting SpriteSheet ([a6632f8](https://gitlab.com/robotandkid/consulting/commit/a6632f8d3d2314c2c51bfc512f015f9b8ce2799f))





# [0.12.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.11.0...@robotandkid/sprite-motion@0.12.0) (2023-11-21)


### Features

* **spritemotion:** added ability to disable collision ([30411cf](https://gitlab.com/robotandkid/consulting/commit/30411cf90b3e8dbf943b01fded2d41890ee19254))
* **spritemotion:** added ability to disable collision ([6591bba](https://gitlab.com/robotandkid/consulting/commit/6591bbabf352085597ecb822aa3235bfc608cc4b))
* **spritemotion:** added sprite sheet functionality ([8177410](https://gitlab.com/robotandkid/consulting/commit/817741020ae00331154bac3eda31700bb96e5dcc))





# [0.11.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.10.0...@robotandkid/sprite-motion@0.11.0) (2023-10-14)


### Bug Fixes

* **sprite-motion:** added ability to completely disable manual navigation ([f72a287](https://gitlab.com/robotandkid/consulting/commit/f72a287df8b81aab5de3b1c1a9737ca25be4e765))
* **sprite-motion:** incorrectly stopping all sounds on transitions ([a3123ac](https://gitlab.com/robotandkid/consulting/commit/a3123ac19155d809c5e11625c655597fc57edb40))


### Features

* **sprite-motion:** added collision callbacks to motion div ([75570cf](https://gitlab.com/robotandkid/consulting/commit/75570cf6b66256a212904b3d15b0dc670a033306))
* **sprite-motion:** added collision callbacks to motion img + aria labels ([fe7bf1f](https://gitlab.com/robotandkid/consulting/commit/fe7bf1fd7fe8561582cba15ca7a153168d2d9eeb))
* **sprite-motion:** fixed disableManualNavigation + added fade transition ([5394edd](https://gitlab.com/robotandkid/consulting/commit/5394edd03975bcb8a21ebe44485285a4ff2e0c70))
* **sprite-motion:** motion div is now a forward ref ([53bc782](https://gitlab.com/robotandkid/consulting/commit/53bc782c2c9418d8c073afac7d8559867ecf6469))
* **sprite-motion:** static image receives className ([7f0587f](https://gitlab.com/robotandkid/consulting/commit/7f0587f737d8f3ccd101c7b3f58288a15393dbce))





# [0.10.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.9.1...@robotandkid/sprite-motion@0.10.0) (2023-09-30)


### Features

* **sprite-motion:** added ActionBlock ability to cancel tasks ([3e8a085](https://gitlab.com/robotandkid/consulting/commit/3e8a08565a6f825259f6a4d044b632fc223e1d45))





## [0.9.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.9.0...@robotandkid/sprite-motion@0.9.1) (2023-09-24)


### Bug Fixes

* **sprite-motion:** action block works with all contexts ([510c8ff](https://gitlab.com/robotandkid/consulting/commit/510c8ffc77aee97c25cf83b4d1c35d4a8883f060))





# [0.9.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.8.0...@robotandkid/sprite-motion@0.9.0) (2023-09-24)


### Bug Fixes

* **sprite-motion:** action block works more gooder ([9c74741](https://gitlab.com/robotandkid/consulting/commit/9c74741b86c5f6a27ad08f6454f29552af2160db))
* **sprite-motion:** exported refactored methods ([345753e](https://gitlab.com/robotandkid/consulting/commit/345753e67e407b61068ddaf1e5becd500677497e))
* **sprite-motion:** properly handling onload in motion img ([ee0386f](https://gitlab.com/robotandkid/consulting/commit/ee0386f62bab98ac823d96ff344cbda5e8775367))


### Features

* **sprite-motion:** added exploding image component ([4e5ec40](https://gitlab.com/robotandkid/consulting/commit/4e5ec4048bb6a1d6fb7f0b035f4de3a4a644cd65))





# [0.8.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.7.1...@robotandkid/sprite-motion@0.8.0) (2023-09-19)


### Features

* **sprite-motion:** added loop playblack + ActionBlock component ([5477804](https://gitlab.com/robotandkid/consulting/commit/5477804d741dc7091f435d98503026b317f0588d))
* **sprite-motion:** added stop cabilitiy ([afa54dd](https://gitlab.com/robotandkid/consulting/commit/afa54ddd6c403bcddecb0aa19bddafb8a9e5e0aa))
* **urielavalos:** added sound effects to a comic ([cddddd2](https://gitlab.com/robotandkid/consulting/commit/cddddd25d12e315db8dcecf5943d0b9f84197299))





## [0.7.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.7.0...@robotandkid/sprite-motion@0.7.1) (2023-09-19)


### Bug Fixes

* **sprite-motion:** no longer resetting css ([a503a0a](https://gitlab.com/robotandkid/consulting/commit/a503a0a3a9a03556e4203547e7041b5ae3877e49))
* **sprite-motion:** properly generating CSS ([60b3511](https://gitlab.com/robotandkid/consulting/commit/60b35112f7f5d710947fe16d94e52e06b846543b))





# [0.7.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.6.0...@robotandkid/sprite-motion@0.7.0) (2023-09-18)


### Bug Fixes

* **sprite-motion:** correct deps ([22eff8d](https://gitlab.com/robotandkid/consulting/commit/22eff8df722502a6e0e1c91adea2c9b4679b0ab2))
* **sprite-motion:** correctly forwarding ref in WorldWithResources ([5376ef5](https://gitlab.com/robotandkid/consulting/commit/5376ef52eeba7e43b7204fcb5201d202d7922e3d))
* **sprite-motion:** no setState in render ([7be62a4](https://gitlab.com/robotandkid/consulting/commit/7be62a4fcb1180547b85d60b18f2151cb42c3b6c))
* **sprite-motion:** use m instead of motion ([98a19e3](https://gitlab.com/robotandkid/consulting/commit/98a19e3a7c458088fcc853bb1a686b00a07a8c0e))
* **sprite-motion:** was incorrectly applying full bleed to sotryboard that is, to viewport dims ([f3959ab](https://gitlab.com/robotandkid/consulting/commit/f3959abb22dca26682d0f95952cec0dec8884ffb))


### Features

* **sprite-motion:** forgot to remove storyboard nav ([40b97a2](https://gitlab.com/robotandkid/consulting/commit/40b97a210e41907e145b4bce9b779649961c3fa9))





# [0.6.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.5.0...@robotandkid/sprite-motion@0.6.0) (2023-09-17)


### Bug Fixes

* **sprite-motion:** sound works completely again ([e9dffb3](https://gitlab.com/robotandkid/consulting/commit/e9dffb3caed354ae78d0d5e8fc5dd0bc977564ce))


### Features

* **sprite-motion:** 1st pass Storyboard + sound rewrite ([8cc5e46](https://gitlab.com/robotandkid/consulting/commit/8cc5e46715a90271cba014eb2afe9ebb31da71fc))
* **sprite-motion:** added sound cleanup ([5ab8a51](https://gitlab.com/robotandkid/consulting/commit/5ab8a5181658c88b12315ce2c0d50b9901aa8b74))





# [0.5.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.4.0...@robotandkid/sprite-motion@0.5.0) (2023-09-10)


### Bug Fixes

* **sprite-motion:** finally fixed the bug where you couldn't play two sounds at the same time ([5b857cc](https://gitlab.com/robotandkid/consulting/commit/5b857ccfce8024b5631f54feacd26db6c7fcb58c))
* **sprite-motion:** sSR support ([20d1349](https://gitlab.com/robotandkid/consulting/commit/20d1349f372152fdf47b7f398ff895087942e49f))
* **sprite-motion:** working sound play() ([d79b23f](https://gitlab.com/robotandkid/consulting/commit/d79b23f05497018d68c5c9dbe1f8d13855b9e68f))


### Features

* **sprite-motion:** added simpler sound API ([1c4e57d](https://gitlab.com/robotandkid/consulting/commit/1c4e57d20c8f9e6a30df6088fd87cb822287712a))
* **sprite-motion:** added sound playback capability ([29f52ac](https://gitlab.com/robotandkid/consulting/commit/29f52ac5d05b7f3f2eb8399e4d73f44541f469c1))
* **sprite-mtion:** world is now a forwardRef component ([bbe8857](https://gitlab.com/robotandkid/consulting/commit/bbe8857bee40a7859796a3ad6bde5d86a4da2bc6))





# [0.4.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.3.0...@robotandkid/sprite-motion@0.4.0) (2023-09-07)


### Bug Fixes

* **sprite-motion:** motionImg had choppy initial behavior ([8b71c92](https://gitlab.com/robotandkid/consulting/commit/8b71c92f1531fd54a8ffeceeddcad300470c68aa))


### Features

* **sprite-motion:** added resource support ([3473d2b](https://gitlab.com/robotandkid/consulting/commit/3473d2b5c59aa645a0a88b04bb5edc3cd0aa96f1))
* **sprite-motion:** exporting Resources type ([4b15ffd](https://gitlab.com/robotandkid/consulting/commit/4b15ffd460c0637e55a279614fc883df80b6761b))





# [0.3.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.2.0...@robotandkid/sprite-motion@0.3.0) (2023-09-04)


### Bug Fixes

* **sprite-motion:** added guarded callback ([d71ee3a](https://gitlab.com/robotandkid/consulting/commit/d71ee3a45e4e8d6afd17d9c4b030a3fe7a9c6ee0))
* **sprite-motion:** collision callbacks work correctly ([15bbf5a](https://gitlab.com/robotandkid/consulting/commit/15bbf5ababa422c43a3244ae01e4e75f88cd54aa))
* **sprite-motion:** fixed a null pointer ([7c01416](https://gitlab.com/robotandkid/consulting/commit/7c0141633cfb96793fcc8c31a7033b41f8e90b35))


### Features

* **sprite-motion:** added styled components support ([104e1c5](https://gitlab.com/robotandkid/consulting/commit/104e1c5097c9e10c8328e34064a21471c14206b7))
* **sprite-motion:** added support for collision callbacks ([6e2d956](https://gitlab.com/robotandkid/consulting/commit/6e2d9560e433eec7bad87572591c3600b5f3a1ba))
* **sprite-motion:** added World dimensions ([3685280](https://gitlab.com/robotandkid/consulting/commit/3685280efaaa044d2984cd8fc5636d9dd9612b01))
* **sprite-motion:** better optional types around world dimensions and child velocities ([3a41b99](https://gitlab.com/robotandkid/consulting/commit/3a41b99a4c92a08f379769ba2e99f6536d56519e))
* **sprite-motion:** static div now has collision callbacks ([cd2645e](https://gitlab.com/robotandkid/consulting/commit/cd2645e77c6034322aeedcbe35640656fe2d81a0))
* **sprite-motion:** updated collision callbacks ([945695e](https://gitlab.com/robotandkid/consulting/commit/945695ead4d9dc618a5b8708e99224077dd9f9ee))





# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.1.1...@robotandkid/sprite-motion@0.2.0) (2023-08-20)


### Features

* **sprite-motion:** added joint ([0937a0d](https://gitlab.com/robotandkid/consulting/commit/0937a0dfbf62051b062d2c65c488853524b74859))
* **sprite-motion:** joint now has a customizable hinge ([cf14fc3](https://gitlab.com/robotandkid/consulting/commit/cf14fc36d3cc3829530bd3d0c7c7913135ef9125))
* **urielavalos:** various bug fixes + improvements ([1d2bc58](https://gitlab.com/robotandkid/consulting/commit/1d2bc583c4a5d75819b675c39949635bddfafdb3))





## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/sprite-motion@0.1.0...@robotandkid/sprite-motion@0.1.1) (2023-08-07)

**Note:** Version bump only for package @robotandkid/sprite-motion





# 0.1.0 (2023-08-07)


### Features

* created sprite-motion ([a32f394](https://gitlab.com/robotandkid/consulting/commit/a32f394aba3d04c0466dc9cfd7d3a4050b01d607))
* **sprite-motion:** added blog post and finished 1st pass sprite-motion ([6011664](https://gitlab.com/robotandkid/consulting/commit/601166446fc6be5e1b56e9efc2c7cecaff25e1f4))
* **sprite-motion:** added sprite-motion 1st pass ([57989f5](https://gitlab.com/robotandkid/consulting/commit/57989f558ae580c207e933103749a38da130a1dd))
