const tokens = {
  "colors.black.100": {
    "value": "black",
    "variable": "var(--colors-black-100)"
  },
  "colors.customGray.100": {
    "value": "#a9a9a9",
    "variable": "var(--colors-custom-gray-100)"
  },
  "colors.dark": {
    "value": "#081820",
    "variable": "var(--colors-dark)"
  },
  "colors.sprite-motion-gb.50": {
    "value": "#081820",
    "variable": "var(--colors-sprite-motion-gb-50)"
  },
  "colors.sprite-motion-gb.51": {
    "value": "#346856",
    "variable": "var(--colors-sprite-motion-gb-51)"
  },
  "colors.sprite-motion-gb.52": {
    "value": "#88c070",
    "variable": "var(--colors-sprite-motion-gb-52)"
  },
  "colors.sprite-motion-gb.53": {
    "value": "#e0f8d0",
    "variable": "var(--colors-sprite-motion-gb-53)"
  },
  "fonts.sprite-motion": {
    "value": "mrpixel, 'var(--font-mrpixel)', monospace",
    "variable": "var(--fonts-sprite-motion)"
  },
  "colors.colorPalette.100": {
    "value": "var(--colors-color-palette-100)",
    "variable": "var(--colors-color-palette-100)"
  },
  "colors.colorPalette": {
    "value": "var(--colors-color-palette)",
    "variable": "var(--colors-color-palette)"
  },
  "colors.colorPalette.50": {
    "value": "var(--colors-color-palette-50)",
    "variable": "var(--colors-color-palette-50)"
  },
  "colors.colorPalette.51": {
    "value": "var(--colors-color-palette-51)",
    "variable": "var(--colors-color-palette-51)"
  },
  "colors.colorPalette.52": {
    "value": "var(--colors-color-palette-52)",
    "variable": "var(--colors-color-palette-52)"
  },
  "colors.colorPalette.53": {
    "value": "var(--colors-color-palette-53)",
    "variable": "var(--colors-color-palette-53)"
  }
}

export function token(path, fallback) {
  return tokens[path]?.value || fallback
}

function tokenVar(path, fallback) {
  return tokens[path]?.variable || fallback
}

token.var = tokenVar