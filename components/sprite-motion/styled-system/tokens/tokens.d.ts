/* eslint-disable */
export type Token =
  | "colors.black.100"
  | "colors.customGray.100"
  | "colors.dark"
  | "colors.sprite-motion-gb.50"
  | "colors.sprite-motion-gb.51"
  | "colors.sprite-motion-gb.52"
  | "colors.sprite-motion-gb.53"
  | "fonts.sprite-motion"
  | "colors.colorPalette.100"
  | "colors.colorPalette"
  | "colors.colorPalette.50"
  | "colors.colorPalette.51"
  | "colors.colorPalette.52"
  | "colors.colorPalette.53";

export type ColorPalette = "black" | "customGray" | "dark" | "sprite-motion-gb";

export type ColorToken =
  | "black.100"
  | "customGray.100"
  | "dark"
  | "sprite-motion-gb.50"
  | "sprite-motion-gb.51"
  | "sprite-motion-gb.52"
  | "sprite-motion-gb.53"
  | "colorPalette.100"
  | "colorPalette"
  | "colorPalette.50"
  | "colorPalette.51"
  | "colorPalette.52"
  | "colorPalette.53";

export type FontToken = "sprite-motion";

export type Tokens = {
  colors: ColorToken;
  fonts: FontToken;
} & { [token: string]: never };

export type TokenCategory =
  | "aspectRatios"
  | "zIndex"
  | "opacity"
  | "colors"
  | "fonts"
  | "fontSizes"
  | "fontWeights"
  | "lineHeights"
  | "letterSpacings"
  | "sizes"
  | "shadows"
  | "spacing"
  | "radii"
  | "borders"
  | "borderWidths"
  | "durations"
  | "easings"
  | "animations"
  | "blurs"
  | "gradients"
  | "breakpoints"
  | "assets";
