import { ChromataPixel } from "./shared.types";
/** Implementation of a queue of a fixed size. */
export default class PathQueue {
    queue: ChromataPixel[];
    size: number;
    constructor(size: number);
    /**
     * Put a new item in the queue. If this causes the queue to exceed its size
     * limit, the oldest item will be discarded.
     */
    put(item: ChromataPixel): void;
    /**
     * Get an item from the queue, specified by index. 0 gets the oldest item in
     * the queue, 1 the second oldest etc. -1 gets the newest item, -2 the second
     * newest etc.
     */
    get(index?: number): ChromataPixel | undefined;
    contains(item: ChromataPixel): boolean;
}
