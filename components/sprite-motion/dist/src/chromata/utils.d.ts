import { ChromataColor, ChromataOptions, RGBA } from "./shared.types";
type Options = {
    colorMode: "rgb";
    channel: "r" | "g" | "b";
} | {
    colorMode: "grayscale";
    channel?: undefined;
};
export declare const oneBitToRgb: (value: number, options: Options) => RGBA;
/** When the palette is defined (when colorMode = color), rgba will be the color */
export declare const indexToRgbString: (i: number, opts: ChromataOptions) => ChromataColor;
export {};
