import { ResourceImg } from "../resources";
import { ChromataOptions } from "./shared.types";
export declare const Chromata: (props: {
    imageSrc: ResourceImg;
    options: ChromataOptions;
}) => null;
