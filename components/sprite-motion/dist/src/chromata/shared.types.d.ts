type Velocity = [number, number];
export type HexColor = `#${string}`;
type ChromataSharedColorOpts = {
    colorMode: "rgb";
    channel: "r" | "g" | "b";
} | {
    colorMode: "color";
    channel?: undefined;
} | {
    colorMode: "grayscale";
    channel?: undefined;
};
interface BasePathFinderOpts {
    startingVelocity: Readonly<Velocity>;
    key: "low" | undefined;
    turningAngle: number;
    speed: number;
}
export type PathFinderOpts = BasePathFinderOpts & ChromataSharedColorOpts;
interface BasePathRendererOpts {
    lineMode: "smooth" | "square" | "point";
    speed: number;
    lineWidth: number;
}
export type PathRendererOpts = BasePathRendererOpts & ChromataSharedColorOpts;
export type RGBA = [number, number, number, number];
/** [x, y, rgba] */
export type ChromataPixel = [number, number, RGBA];
export type ChromataBaseOptions = {
    compositeOperation: CanvasRenderingContext2D["globalCompositeOperation"];
    lineWidth: number;
    lineMode: "smooth" | "square" | "point";
    origin: Array<"bottom" | "top" | "left" | "right" | `${number}% ${number}%`>;
    pathFinderCount: number;
    speed: number;
    turningAngle: number;
    key?: "low";
    dimensions?: {
        width: number | undefined;
        height: number | undefined;
    };
} & ({
    iterationLimit?: undefined;
    onIterationDone?: undefined;
} | {
    iterationLimit: number;
    onIterationDone: () => void;
});
export type ChromataBaseColorOpts = {
    colorMode: "rgb";
    palette?: undefined;
} | {
    colorMode: "color";
    palette: Readonly<HexColor[]>;
} | {
    colorMode: "grayscale";
    palette?: undefined;
};
export type ChromataOptions = ChromataBaseOptions & ChromataBaseColorOpts;
export interface ChromataColor {
    color: `#${string}`;
    channel: "r" | "g" | "b";
    rgba: RGBA;
}
export {};
