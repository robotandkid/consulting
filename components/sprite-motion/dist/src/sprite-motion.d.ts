import { Bodies, Body, Engine } from "matter-js";
import React, { ReactNode } from "react";
import { MetaResource, ResourceImg } from "./resources";
import { PanelTransition } from "./storyboard";
/**
 * Escape hatch to access the matter-js Engine
 */
export declare const EngineProvider: {
    (props: {
        children?: React.ReactNode;
        value: Engine;
    }): React.JSX.Element;
    displayName: string;
}, useEngineContext: () => Engine | undefined;
/**
 * **WARNING**:
 *
 * - All of these are called by the World, which is the global parent.
 * - You will get state-update warnings when you naively try to update
 *   state on a child from the parent.
 * - Instead, you have to check the component is still mounted before
 *   updating state.
 */
type BodyCallbacks = {
    onUpdateBodyState: (body: Body) => void;
    onCollisionStart?: (thisBody: Body, otherBody: Body) => void;
    onCollisionEnd?: (thisBody: Body, otherBody: Body) => void;
    onCollisionActive?: (thisBody: Body, otherBody: Body) => void;
};
declare const usePagination: () => ((direction: PanelTransition | number) => void) | undefined, useSetPagination: () => (value: (direction: PanelTransition | number) => void) => void;
export { useSetPagination, usePagination };
type AllOrNothing<T> = Required<T> | {
    [key in keyof T]?: never;
};
/**
 * **WARNING:** All props are _fixed_. That is, subsequent updates
 * will _not_ move the DIV.
 *
 * This is otherwise just a regular DIV, so by default it is
 * _invisible_. Pass style props to color the DIV.
 *
 * If you need movement, use a MotionDiv.
 */
export declare const StaticDiv: (props: React.HTMLAttributes<HTMLDivElement> & {
    cx: number;
    cy: number;
    width: number;
    height: number;
    /**
     * In degrees
     */
    angle?: number;
} & Pick<BodyCallbacks, "onCollisionStart" | "onCollisionEnd" | "onCollisionActive">) => React.JSX.Element | null;
/**
 * **WARNING:** this component does not support dynamic props, that
 * is, changing the starting position/velocity will _not_ change the
 * animation.
 */
export declare const MotionImg: (props: Omit<React.ImgHTMLAttributes<HTMLImageElement>, "src"> & {
    src: React.ImgHTMLAttributes<HTMLImageElement>["src"] | ResourceImg;
    startLeft: number;
    startTop: number;
    /**
     * In degrees
     */
    startAngle?: number;
    disableCollisions?: boolean;
} & AllOrNothing<{
    vx: number;
    vy: number;
}> & Pick<BodyCallbacks, "onCollisionStart" | "onCollisionEnd" | "onCollisionActive">) => React.JSX.Element;
export declare const StaticImg: (props: Omit<React.ImgHTMLAttributes<HTMLImageElement>, "src"> & {
    src: React.ImgHTMLAttributes<HTMLImageElement>["src"] | ResourceImg;
} & {
    className?: string;
}) => React.JSX.Element;
/**
 * **WARNING:** this component does not support dynamic props, that
 * is, changing the starting position/velocity will _not_ change the
 * animation.
 *
 * If you need a constraint, use MotionDiv with constraint.
 */
export declare const MotionDiv: React.ForwardRefExoticComponent<(React.HTMLAttributes<HTMLDivElement> & {
    startCx: number;
    startCy: number;
    /**
     * In degrees
     *
     * @deprecated  Use options
     */
    startAngle?: number | undefined;
    width: number;
    height: number;
    options?: Parameters<typeof Bodies.rectangle>[4];
} & AllOrNothing<{
    vx: number;
    vy: number;
}> & Pick<BodyCallbacks, "onCollisionStart" | "onCollisionEnd" | "onCollisionActive">) & React.RefAttributes<HTMLDivElement>>;
export declare const Joint: (props: React.HTMLAttributes<HTMLDivElement> & {
    startLeft: number;
    startTop: number;
    /**
     * ```text
     * hingeX = 0% => startLeft
     * hingeX = 100% => startLeft + width
     * ```
     */
    hingeX: `${number}%`;
    /**
     * ```text
     * hingeY = 0% => startTop
     * hingeY = 100% => startTop + height
     * ```
     */
    hingeY: `${number}%`;
    width: number;
    height: number;
} & AllOrNothing<{
    vx?: number;
    vy?: number;
}>) => React.JSX.Element;
export declare const WorldWithResourcesInner: React.ForwardRefExoticComponent<(Omit<{
    resources: Record<string, MetaResource>;
    loading: ReactNode;
} & {
    children?: React.ReactNode;
    onInit?: ((engine: Engine) => void) | undefined;
    className?: string | undefined;
    style?: React.HTMLAttributes<HTMLElement>["style"];
} & Required<{
    width: number;
    height: number;
}> & React.RefAttributes<HTMLDivElement>, "ref"> | Omit<{
    resources: Record<string, MetaResource>;
    loading: ReactNode;
} & {
    children?: React.ReactNode;
    onInit?: ((engine: Engine) => void) | undefined;
    className?: string | undefined;
    style?: React.HTMLAttributes<HTMLElement>["style"];
} & {
    width?: undefined;
    height?: undefined;
} & React.RefAttributes<HTMLDivElement>, "ref">) & React.RefAttributes<HTMLDivElement>>;
export declare const World: React.ForwardRefExoticComponent<(Omit<Required<{
    resources: Record<string, MetaResource>;
    loading: ReactNode;
}> & {
    children?: React.ReactNode;
    onInit?: ((engine: Engine) => void) | undefined;
    className?: string | undefined;
    style?: React.HTMLAttributes<HTMLElement>["style"];
} & Required<{
    width: number;
    height: number;
}> & React.RefAttributes<HTMLDivElement>, "ref"> | Omit<Required<{
    resources: Record<string, MetaResource>;
    loading: ReactNode;
}> & {
    children?: React.ReactNode;
    onInit?: ((engine: Engine) => void) | undefined;
    className?: string | undefined;
    style?: React.HTMLAttributes<HTMLElement>["style"];
} & {
    width?: undefined;
    height?: undefined;
} & React.RefAttributes<HTMLDivElement>, "ref"> | Omit<{
    resources?: undefined;
    loading?: undefined;
} & {
    children?: React.ReactNode;
    onInit?: ((engine: Engine) => void) | undefined;
    className?: string | undefined;
    style?: React.HTMLAttributes<HTMLElement>["style"];
} & Required<{
    width: number;
    height: number;
}> & React.RefAttributes<HTMLDivElement>, "ref"> | Omit<{
    resources?: undefined;
    loading?: undefined;
} & {
    children?: React.ReactNode;
    onInit?: ((engine: Engine) => void) | undefined;
    className?: string | undefined;
    style?: React.HTMLAttributes<HTMLElement>["style"];
} & {
    width?: undefined;
    height?: undefined;
} & React.RefAttributes<HTMLDivElement>, "ref">) & React.RefAttributes<HTMLDivElement>>;
