import React from "react";
export declare const storyZIndex = 0;
export declare const PanelBackground: import("@styled-system/jsx/index").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLImageElement, import("framer-motion").HTMLMotionProps<"img">>, {}>;
declare const PanelInner: import("@styled-system/jsx/index").StyledComponent<import("framer-motion").ForwardRefComponent<HTMLDivElement, import("framer-motion").HTMLMotionProps<"div">>, {}>;
declare const usePanelCanvas: () => HTMLCanvasElement | undefined;
export { usePanelCanvas };
declare const usePanelCanvasCtx2D: () => CanvasRenderingContext2D | undefined;
export { usePanelCanvasCtx2D };
export declare const Panel: (props: Omit<Parameters<typeof PanelInner>[0], "children"> & {
    children: React.ReactNode;
}) => React.JSX.Element;
declare const wipeTransition: readonly ["prev-wipe", "next-wipe"];
declare const fadeTransition: readonly ["prev-fade", "next-fade"];
declare const simpleTransition: readonly ["prev", "next"];
type WipeTransition = typeof wipeTransition[number];
type FadeTransition = typeof fadeTransition[number];
type SimpleTransition = typeof simpleTransition[number];
export type PanelTransition = WipeTransition | FadeTransition | SimpleTransition;
export declare const StoryBoard: (props: {
    children?: React.ReactNode[] | React.ReactNode;
    disableManualNavigation?: boolean;
}) => React.JSX.Element;
export declare const StoryBoardNav: () => React.JSX.Element;
