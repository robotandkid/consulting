import React from "react";
type Key = string;
type RenderCallback = (timeElapsed: number, timeElapsedBetweenFrames: number) => Key | void;
declare const useClearCanvasRendererCtx: () => (() => void) | undefined;
export { useClearCanvasRendererCtx };
/**
 * **WARNING:** An important caveat is that once registered, the resulting entry
 * is NEVER removed. This has performance ramifications and possible memory
 * issues.
 *
 * The way this works:
 *
 * - Every time there's a new render, we clear out the
 */
export declare const Canvas: (props: {
    children: React.ReactNode;
    /** @deprecated */
    width?: number;
    /** @deprecated */
    height?: number;
    disableClearCanvasBeforeUpdate?: boolean;
}) => React.JSX.Element;
export declare const useKeyRegistration: (name: string, props: any) => readonly [true, string] | readonly [false, string];
export declare const useCanvasRegistration: () => ((cb: RenderCallback) => void) | undefined;
