export declare const OneBit: (props: {
    threshold: number;
    disabled?: boolean;
}) => null;
export declare const OneBitSimple: (props: {
    threshold: number;
    disabled?: boolean;
}) => null;
