import React from "react";
interface VideoFns {
    start: () => void;
    stop: () => void;
}
declare const CanvasSaveVideoProvider: {
    (props: {
        children?: React.ReactNode;
        initialValue?: VideoFns | undefined;
    }): React.JSX.Element;
    displayName: string;
}, useCanvasSaveVideoCtx: () => VideoFns | undefined;
export { CanvasSaveVideoProvider, useCanvasSaveVideoCtx };
export declare const CanvasSaveVideo: React.MemoExoticComponent<(props: {
    autoStart?: boolean;
    frameRate?: number;
}) => null>;
