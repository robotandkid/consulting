import React from "react";
export declare const SimpleVideoExample: () => React.JSX.Element;
export declare const MovingVideoExample: () => React.JSX.Element;
export declare const AnotherMovingVideoExample: () => React.JSX.Element;
