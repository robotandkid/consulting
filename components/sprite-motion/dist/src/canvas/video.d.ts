import { ResourceVideo } from "../resources";
import React from "react";
interface AdvancedOpts {
    renderLeft: number;
    renderTop: number;
    renderWidth: number;
    renderHeight: number;
}
type Never<T> = {
    [K in keyof T]?: never;
};
export declare const Video: (props: {
    src: ResourceVideo;
    loop?: boolean;
    onEnd?: () => void;
    tintColor?: string;
    playbackRate?: number;
    muted?: boolean;
} & (AdvancedOpts | Never<AdvancedOpts>)) => React.JSX.Element;
export {};
