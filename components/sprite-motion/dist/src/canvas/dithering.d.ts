type HexColor = `#${string}`;
export declare const CanvasDithering: (props: {
    disabled?: boolean;
    palette: Readonly<[HexColor, ...HexColor[]]>;
}) => null;
export {};
