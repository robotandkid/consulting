import React from "react";
export declare const RawSpanExample: () => React.JSX.Element;
export declare const NeonExample: () => React.JSX.Element;
export declare const JustifiedFullExample: () => React.JSX.Element;
export declare const JustifiedFullWithDivRotationExample: () => React.JSX.Element;
export declare const JustifiedFullWithMultipleExample: () => React.JSX.Element;
export declare const AlignedLeftExample: () => React.JSX.Element;
export declare const AlignedLeftWithDivRotationExample: () => React.JSX.Element;
