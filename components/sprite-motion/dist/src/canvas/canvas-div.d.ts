import React from "react";
type TextFont = `${number}px ${string}` | `${"bold italic" | "bold" | "italic"} ${number}px ${string}`;
interface SpanStyle {
    font?: TextFont;
    bgStyle?: `#${string}`;
    colorStyle?: `#${string}`;
    textBaseline?: CanvasRenderingContext2D["textBaseline"];
    marginRight?: number;
}
interface SpanPos {
    left: number;
    top: number;
    width?: number;
    height?: number;
}
interface TransformOpts {
    rotate?: number;
}
interface DivPos {
    left: number;
    top: number;
    width: number;
    height: number;
}
interface DivStyle {
    lineHeightMultiple?: number;
}
export declare const CanvasDivJustifiedFull: (props: {
    pos: DivPos;
    style?: DivStyle;
    transform?: TransformOpts;
    children: React.ReactNode;
}) => React.JSX.Element;
export declare const CanvasDivAlignLeft: (props: {
    pos: DivPos;
    style?: DivStyle;
    transform?: TransformOpts;
    children: React.ReactNode;
}) => React.JSX.Element;
interface TransformOpts {
    rotate?: number;
}
export declare const CanvasSpan: (props: {
    children: string;
    style?: SpanStyle;
    transform?: TransformOpts;
}) => null;
/** @deprecated Do not use. Exported only for testing */
export declare const CanvasRawSpan: (props: {
    style?: SpanStyle;
    pos: SpanPos;
    children: string;
}) => null;
export {};
