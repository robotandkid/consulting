export declare const Noise: (props: {
    time: number;
    effect?: "fadeIn" | "fadeOut";
}) => null;
