import React from "react";
export declare const SpriteSheetExample: () => React.JSX.Element;
export declare const SpriteUnderCodeExample: () => React.JSX.Element;
export declare const CodeUnderSpriteExample: () => React.JSX.Element;
export declare const OnLoopEndExample: () => React.JSX.Element;
export declare const MovingExample: () => React.JSX.Element;
export declare const MovingExampleWithVideo: () => React.JSX.Element;
export declare const KeyboardControlExample: () => React.JSX.Element;
export declare const SpriteSheetFromManyExample: () => React.JSX.Element;
