import React from "react";
import { Story } from "@ladle/react";
export declare const ImageExample: Story<{
    threshold: string[];
}>;
export declare const VideoExample: () => React.JSX.Element;
