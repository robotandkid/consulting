import React from "react";
export declare const CanvasImageExample: () => React.JSX.Element;
export declare const MovingImageExample: () => React.JSX.Element;
export declare const TileExample: () => React.JSX.Element;
