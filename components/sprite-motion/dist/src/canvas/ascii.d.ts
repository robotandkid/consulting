interface TextStyle {
    fontFamily?: string;
    fillStyle?: `#${string}`;
    bgColor?: `#${string}`;
}
interface FullColorStyle extends Pick<TextStyle, "fontFamily" | "bgColor"> {
    type: "full";
}
interface InvertedColorStyle extends Pick<TextStyle, "fontFamily"> {
    type: "invert";
}
export declare const Ascii: (props: {
    resolutionX: number;
    resolutionY: number;
    style?: TextStyle;
}) => null;
/**
 * It is possible to override the characters used by specifying the
 * characterMap. Default is
 *
 *     "#&M8BW0XO%QDHAVYNERU468GQP3572CFJKLTZ1I!;.,:'^~-_|/*()[]{}<>+=? ";
 */
export declare const AsciiFullColor: (props: {
    resolutionX: number;
    resolutionY: number;
    style?: Omit<FullColorStyle, "type">;
    characterMap?: string;
}) => null;
export declare const AsciiInvertColor: (props: {
    resolutionX: number;
    resolutionY: number;
    style?: Omit<InvertedColorStyle, "type">;
    characterMap?: string;
}) => null;
export declare const AsciiLightColor: (props: {
    resolutionX: number;
    resolutionY: number;
    lightPercentFactor?: number;
    darkPercentFactor?: number;
    style?: Omit<InvertedColorStyle, "type">;
    characterMap?: string;
}) => null;
export {};
