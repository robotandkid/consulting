import React from "react";
export declare const CanvasSaveImage: React.MemoExoticComponent<(props: {
    Button: React.ReactElement<{
        onClick?: () => void;
    }>;
}) => React.JSX.Element | null>;
