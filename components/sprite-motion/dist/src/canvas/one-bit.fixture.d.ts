import React from "react";
import { Story } from "@ladle/react";
export declare const ImageExample: Story<{
    threshold: number;
}>;
export declare const SimpleExample: Story<{
    threshold: number;
}>;
export declare const VideoExample: () => React.JSX.Element;
