export declare const OvalIris: (props: {
    color?: string;
    durationInMs?: number;
    onEnd?: () => void;
}) => null;
