export declare const CanvasBlur: (props: {
    size: number;
    duration: number;
    direction: "in" | "out";
}) => null;
