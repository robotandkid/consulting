import React from "react";
export declare const AsciiExample: () => React.JSX.Element;
export declare const ColorFlipExample: () => React.JSX.Element;
export declare const FullAsciiExample: () => React.JSX.Element;
export declare const VideoExample: () => React.JSX.Element;
export declare const FullVideoExample: () => React.JSX.Element;
export declare const CustomRampExample: () => React.JSX.Element;
