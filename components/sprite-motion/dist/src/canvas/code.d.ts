type TextFont = `${number}px ${string}` | `${"bold italic" | "bold" | "italic"} ${number}px ${string}`;
export declare const Code: (props: {
    children: string | string[];
    top: number;
    left: number;
    speed?: number;
    onDone?: () => void;
    style?: {
        glitch?: boolean;
        lineHeightAsPx?: number;
        font?: TextFont;
        fillStyle?: `#${string}`;
        textBaseline?: CanvasRenderingContext2D["textBaseline"];
        shadowOffsetX?: number;
        shadowOffsetY?: number;
        shadowColor?: `#${string}`;
        shadowBlur?: number;
    };
}) => null;
export {};
