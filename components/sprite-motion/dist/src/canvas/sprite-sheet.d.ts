import React from "react";
import { ResourceImg } from "../resources";
declare const supportedFpsSkipMap: {
    readonly 60: 1;
    readonly 30: 2;
    readonly 20: 3;
    readonly 15: 4;
    readonly 12: 5;
    readonly 10: 6;
    readonly 9: 7;
    readonly 8: 8;
    readonly 6: 10;
    readonly 5: 12;
    readonly 4: 15;
    readonly 2: 30;
    readonly 1: 60;
};
export declare const SpriteSheet: (props: {
    src: React.ImgHTMLAttributes<HTMLImageElement>["src"] | ResourceImg;
    /**
     * X-coordinate to render the sprite on the canvas.
     *
     * ```text
     *     (left,top)
     *          *-------+
     *          |       |
     *          +-------+
     * ```
     */
    renderLeft: number;
    /**
     * Y-coordinate to render the sprite on the canvas.
     *
     * ```text
     *     (left,top)
     *          *-------+
     *          |       |
     *          +-------+
     * ```
     */
    renderTop: number;
    renderWidth: number;
    renderHeight: number;
    frameWidth: number;
    frameHeight: number;
    fps?: keyof typeof supportedFpsSkipMap;
    loop?: boolean;
    onLoopEnd?: () => void;
}) => null;
export declare const SpriteSheetFromMany: (props: {
    src: ResourceImg[];
    renderLeft: number;
    renderTop: number;
    renderWidth: number;
    renderHeight: number;
    fps?: keyof typeof supportedFpsSkipMap;
    loop?: boolean;
    onLoopEnd?: () => void;
}) => null;
export {};
