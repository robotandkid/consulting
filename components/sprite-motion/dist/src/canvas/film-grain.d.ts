export declare const FilmGrain: (props: {
    size?: number;
    refreshInterval?: number;
    /**
     * Number 0..255
     */
    alpha?: number;
}) => null;
