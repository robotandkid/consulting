import { ResourceImg } from "../resources";
interface RenderOpts {
    renderLeft: number;
    renderTop: number;
    renderWidth: number;
    renderHeight: number;
    tile?: undefined;
}
interface TileOpts extends Omit<RenderOpts, "tile"> {
    tile: "repeat";
    tileWidth: number;
    tileHeight: number;
}
type AdvancedOpts = RenderOpts | TileOpts;
type Never<T> = {
    [K in keyof T]?: never;
};
export declare const CanvasImage: (props: {
    src: ResourceImg;
} & (AdvancedOpts | Never<AdvancedOpts>)) => null;
export {};
