import React from "react";
import { Code } from "./code";
type TextStyleNullable = Parameters<typeof Code>[0]["style"];
declare const drawBubble: (ctx: CanvasRenderingContext2D | undefined, pos: {
    left: number;
    top: number;
    width: number;
    height: number;
    radius: number;
    pLeft?: number;
    pTop?: number;
}, style: {
    strokeColor: `#${string}`;
    strokeWidth: number;
    fillColor: `#${string}`;
}) => void;
type PositionalOpts = Parameters<typeof drawBubble>[1];
type StyleOpts = Parameters<typeof drawBubble>[2];
export declare const CanvasSpeechBubble: (props: {
    pos: PositionalOpts;
} & {
    style?: Partial<StyleOpts>;
} & {
    text: {
        rx: number;
        ry: number;
        speed?: number;
    };
    textStyle?: TextStyleNullable;
    children?: string;
    disabled?: boolean;
    onDone?: () => void;
    autoTrim?: boolean;
}) => React.JSX.Element;
export {};
