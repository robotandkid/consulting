import React from "react";
export declare const StoryboardWithAutoPaginationExample: () => React.JSX.Element;
export declare const SimpleTransitionExample: () => React.JSX.Element;
export declare const StoryBoardWithNavigationExample: () => React.JSX.Element;
export declare const FadeTransitionExample: () => React.JSX.Element;
export declare const CanvasExample: () => React.JSX.Element;
