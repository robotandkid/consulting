import React from "react";
import { Downloads, MetaResource } from "./resources";
import { StaticImg, useEngineContext, usePagination } from "./sprite-motion";
type Defined<Func extends (args: any) => any> = NonNullable<ReturnType<Func>>;
export declare const ActionBlock: <Resources extends Record<string, MetaResource>>(props: {
    children: (opts: {
        paginate: Defined<typeof usePagination>;
        engine: Defined<typeof useEngineContext>;
        resources: Downloads<Resources>;
        /**
         * This exists because you can't cancel async tasks in JS 🙄.
         *
         * So if you have a long running (async) task, check this property first.
         *
         * Example:
         *
         * ```tsx
         * <ActionBlock>
         *   {async ({ signal, isBlockCancelled }) => {
         *     const items = fetch("/items", { signal });
         *     if (!isBlockCancelled) return;
         *     const moreItems = fetch("/items", { signal });
         *     if (!isBlockCancelled) return;
         *     // ...
         *   }}
         * </ActionBlock>;
         * ```
         */
        isBlockCancelled: boolean;
        signal: AbortController["signal"];
        canvas: HTMLCanvasElement;
    }) => Promise<void> | void;
}) => null;
export declare const ExplodingImage: (props: Parameters<typeof StaticImg>[0] & {
    xNumParticles: number;
    yNumParticles: number;
    explosionFactor?: number;
    delayInMs: number;
}) => React.JSX.Element;
export {};
