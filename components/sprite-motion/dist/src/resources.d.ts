/// <reference types="react" />
declare const InternalAbortCtrlProviderInner: {
    (props: {
        children?: import("react").ReactNode;
        value: AbortController;
    }): import("react").JSX.Element;
    displayName: string;
}, useAbortCtrlCtx: () => AbortController | undefined;
export { 
/** @internal don't use this */
InternalAbortCtrlProviderInner, useAbortCtrlCtx, };
export type MetaSupportedImg = "png" | "jpg" | "jpeg" | "gif";
export type MetaSupportedSound = "ogg" | "mp3" | "wav";
export type MetaSupportedVideo = "mp4" | "mov";
export type MetaSupportedText = "txt" | "tsx" | "jsx" | "ts" | "js" | "md";
export type MetaResource = {
    type: "image";
    url: `${string}.${MetaSupportedImg}`;
} | {
    type: "sound";
    url: `${string}.${MetaSupportedSound}`;
} | {
    type: "video";
    url: `${string}.${MetaSupportedVideo}`;
} | {
    type: "text";
    url: `${string}.${MetaSupportedText}`;
};
type MetaResourceImg = Extract<MetaResource, {
    type: "image";
}>;
type MetaResourceSound = Extract<MetaResource, {
    type: "sound";
}>;
type MetaResourceVideo = Extract<MetaResource, {
    type: "video";
}>;
type MetaResourceText = Extract<MetaResource, {
    type: "text";
}>;
export interface ResourceImg extends MetaResourceImg {
    resource: Promise<string | undefined>;
}
export interface ResourceVideo extends MetaResourceVideo {
    resource: Promise<string | undefined>;
    video: HTMLVideoElement;
}
export interface ResourceText extends MetaResourceText {
    resource: Promise<string | undefined>;
}
export interface ResourceSound extends MetaResourceSound {
    resource: {
        /**
         * **Big Note**: you have to call play as a result of user interaction. For
         * the most part, you have to gate the media element behind a click. Think
         * youtube videos that you have to hit the volume to get sound.
         */
        play(opts?: {
            loop?: boolean;
        }): void;
        stop(): void;
    };
}
type Resources<MetaResources extends Record<string, MetaResource>> = {
    [name in keyof MetaResources]: MetaResources[name] extends MetaResourceImg ? ResourceImg : MetaResources[name] extends MetaResourceVideo ? ResourceVideo : MetaResources[name] extends MetaResourceText ? ResourceText : ResourceSound;
};
/**
 * @deprecated Use World instead
 * @internal use World instead
 */
export declare const useResources: <MetaResources extends Record<string, MetaResource>>(metaResources: MetaResources) => {
    /** You most likely don't want to call this directly */
    cleanup: () => void;
    readonly downloads: Resources<MetaResources>;
} | "loading";
type UseResources<MetaResources extends Record<string, MetaResource>> = ReturnType<typeof useResources<MetaResources>>;
/** This is just the downloaded type (that excludes the "loading" state) */
export type Downloads<MetaResources extends Record<string, MetaResource>> = Exclude<UseResources<MetaResources>, "loading">;
type GenericDownloadsCtx = Downloads<any>;
declare const InternalGenericDownloadsProvider: {
    (props: {
        children?: import("react").ReactNode;
        value: GenericDownloadsCtx;
    }): import("react").JSX.Element;
    displayName: string;
};
/** To use the downloads context, you have to cast it to the right type */
export declare const useDownloadsCtx: <MetaResources extends Record<string, MetaResource>>() => {
    /** You most likely don't want to call this directly */
    cleanup: () => void;
    readonly downloads: Resources<MetaResources>;
};
export { 
/** @private - Don't use this */
InternalGenericDownloadsProvider, };
