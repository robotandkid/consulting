import React from "react";
export declare const PrintKeys: <Obj>(props: {
    obj: Obj;
    keys: (keyof Obj)[];
}) => React.JSX.Element;
