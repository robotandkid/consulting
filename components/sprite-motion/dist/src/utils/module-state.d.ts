interface ModuleState<State> {
    getState: () => State;
    setState: (action: State | ((prev: State) => State)) => void;
    subscribe: (cb: () => void) => () => void;
}
export declare const createModuleState: <State>(initial: State) => ModuleState<State>;
export declare const useModuleState: <State>(moduleState: ModuleState<State>) => readonly [State, (action: State | ((prev: State) => State)) => void];
export {};
