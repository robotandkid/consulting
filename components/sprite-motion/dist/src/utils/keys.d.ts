declare const keys: readonly ["ArrowLeft", "ArrowUp", "ArrowRight", "ArrowDown", "Enter", "Space", "Esc"];
type Key = typeof keys[number];
export declare const useKeyListeners: (props: {
    onKeyDown: (key: Key) => void;
    disabled?: boolean | undefined;
}) => void;
export {};
