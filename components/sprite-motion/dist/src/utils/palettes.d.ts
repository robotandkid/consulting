/** 2 colors */
export declare const palette2funkyJam: () => readonly ["#920244", "#fec28c"];
export declare const palette2chasingLight: () => readonly ["#000000", "#ffff02"];
/** 5 colors */
export declare const palette5blessing: () => readonly ["#74569b", "#96fbc7", "#f7ffae", "#ffb3cb", "#d8bfd8"];
export declare const palette5sunsetRed: () => readonly ["#0d101b", "#281a2d", "#6b2341", "#af2747", "#ee243d"];
export declare const marioNesPalette: () => readonly ["#7e9953", "#d9a739", "#bd0808", "#0300ff", "#e8ff00"];
export declare const palette5marioNes: () => readonly ["#7e9953", "#d9a739", "#bd0808", "#0300ff", "#e8ff00"];
export declare const gameboyPalette: () => readonly ["#eefab3", "#c2d97d", "#98b253", "#4e8433", "#174f39"];
export declare const palette5gameBoy: () => readonly ["#eefab3", "#c2d97d", "#98b253", "#4e8433", "#174f39"];
export declare const dalhia5Palette: () => readonly ["#73020C", "#A60321", "#F20F38", "#F21B54", "#260101"];
export declare const palette5dalhia: () => readonly ["#73020C", "#A60321", "#F20F38", "#F21B54", "#260101"];
export declare const palette5cap5: () => readonly ["#fadda2", "#6b61ff", "#66a1ff", "#8ecde6", "#f0eff4"];
export declare const palette5maruMaruGum: () => readonly ["#fda9a9", "#f3eded", "#b9eedc", "#96beb1", "#82939b"];
export declare const palette5goldenSunset: () => readonly ["#ffecd6", "#ffb873", "#cb765c", "#7a4a5a", "#25213e"];
export declare const palette5spanishSunset: () => readonly ["#f5ddbc", "#fabb64", "#fd724e", "#a02f40", "#5f2f45"];
export declare const palette5origamiClouds: () => readonly ["#dee3e2", "#fccbcb", "#78b3d6", "#d86969", "#4f7969"];
export declare const palette5LeopoldsDreams: () => readonly ["#372134", "#474476", "#4888b7", "#6dbcb9", "#8cefb6"];
export declare const palette5sheep: () => readonly ["#480a30", "#b41360", "#ff327c", "#ff80ae", "#ffdae8"];
export declare const palette5neo5: () => readonly ["#0e0e0e", "#5433be", "#e624af", "#3df9ea", "#effafa"];
/** 6 colors */
export declare const oil6Palette: () => readonly ["#fbf5ef", "#f2d3ab", "#c69fa5", "#8b6d9c", "#494d7e", "#272744"];
export declare const palette6oil: () => readonly ["#fbf5ef", "#f2d3ab", "#c69fa5", "#8b6d9c", "#494d7e", "#272744"];
export declare const cybergumsPalette: () => readonly ["#3a2b3b", "#2d4a54", "#0c7475", "#bc4a9b", "#eb8d9c", "#ffd8ba"];
export declare const palette6cyberGums: () => readonly ["#3a2b3b", "#2d4a54", "#0c7475", "#bc4a9b", "#eb8d9c", "#ffd8ba"];
/** 8 colors */
export declare const clement8Palette: () => readonly ["#000871", "#8854f3", "#639bff", "#63ffba", "#ff8c5c", "#ff79ae", "#fff982", "#ffffff"];
export declare const palette8clement: () => readonly ["#000871", "#8854f3", "#639bff", "#63ffba", "#ff8c5c", "#ff79ae", "#fff982", "#ffffff"];
export declare const funkyFuture8Palette: () => readonly ["#2b0f53", "#ab1f65", "#ff4f69", "#fff7f8", "#ff8142", "#ffda45", "#3368dc", "#49e7ec"];
export declare const palette8funkyFuture: () => readonly ["#2b0f53", "#ab1f65", "#ff4f69", "#fff7f8", "#ff8142", "#ffda45", "#3368dc", "#49e7ec"];
export declare const bright8palette: () => readonly ["#5ca8c2", "#055d31", "#86d86a", "#fdfdf0", "#faa3ce", "#a4573d", "#02020f", "#7a2796"];
export declare const palette8bright: () => readonly ["#5ca8c2", "#055d31", "#86d86a", "#fdfdf0", "#faa3ce", "#a4573d", "#02020f", "#7a2796"];
export declare const spazbol8palette: () => readonly ["#000000", "#51007c", "#5073d3", "#8fd350", "#fff600", "#ffffff", "#ed8f0b", "#cc2718"];
export declare const palette8spazbol: () => readonly ["#000000", "#51007c", "#5073d3", "#8fd350", "#fff600", "#ffffff", "#ed8f0b", "#cc2718"];
/** 9 colors */
export declare const palette9smokey09: () => readonly ["#fafafa", "#d4d8e0", "#acacac", "#918b8c", "#6b615e", "#3b342e", "#24211a", "#0e0d0a", "#030201"];
/** 16 colors */
export declare const sweetiePalette: () => readonly ["#1a1c2c", "#5d275d", "#b13e53", "#ef7d57", "#ffcd75", "#a7f070", "#38b764", "#257179", "#29366f", "#3b5dc9", "#41a6f6", "#73eff7", "#f4f4f4", "#94b0c2", "#566c86", "#333c57"];
export declare const palette16sweetie: () => readonly ["#1a1c2c", "#5d275d", "#b13e53", "#ef7d57", "#ffcd75", "#a7f070", "#38b764", "#257179", "#29366f", "#3b5dc9", "#41a6f6", "#73eff7", "#f4f4f4", "#94b0c2", "#566c86", "#333c57"];
export declare const palette16shidoCyberNeon: () => readonly ["#00033c", "#005260", "#009d4a", "#0aff52", "#003884", "#008ac5", "#00f7ff", "#ff5cff", "#ac29ce", "#600088", "#b10585", "#ff004e", "#2a2e79", "#4e6ea8", "#add4fa", "#ffffff"];
/** 22 colors */
export declare const palette22chasm: () => readonly ["#85daeb", "#5fc9e7", "#5fa1e7", "#5f6ee7", "#4c60aa", "#444774", "#32313b", "#463c5e", "#5d4776", "#855395", "#ab58a8", "#ca60ae", "#f3a787", "#f5daa7", "#8dd894", "#5dc190", "#4ab9a3", "#4593a5", "#5efdf7", "#ff5dcc", "#fdfe89", "#ffffff"];
