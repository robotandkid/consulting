import React, { ReactNode } from "react";
export declare function createContext<T>(initial?: T): React.Context<T>;
export declare function makeReadOnlyContext<MyContext>(name: string): readonly [{
    (props: {
        children?: ReactNode;
        value: MyContext;
    }): React.JSX.Element;
    displayName: string;
}, () => MyContext | undefined, React.Context<MyContext | undefined>];
export declare function makeSimpleContext<MyContext>(name: string): readonly [{
    (props: {
        children?: ReactNode;
        initialValue?: MyContext;
    }): React.JSX.Element;
    displayName: string;
}, () => MyContext | undefined, () => (value: MyContext) => void];
