export declare class WrappedArray<Data> {
    data: Readonly<Data[]>;
    len: number;
    constructor(data: Readonly<Data[]>, start?: number);
    current(): [Data, Data, Data, number];
    next(): [Data, Data, Data, number];
    prev(): [Data, Data, Data, number];
}
export declare const useWrappedArray: <Data>(data: readonly Data[]) => readonly [Data, Data, Data, () => void, () => void];
