import React from "react";
export declare const sleep: (timeout: number) => Promise<unknown>;
export declare const useInterval: (callback: () => void | "abort", timeout: number) => void;
export declare const useInterpolate: (start: number, end: number, opts: {
    disabled?: boolean;
    /** How often setInterval should run */
    timeDelta?: number;
    /** How much the value should change between the timeDelta */
    delta: number;
    onDone?: () => void;
}) => number;
export declare const ShowAfterDelay: (props: {
    timeout: number;
    children: React.ReactNode;
}) => React.JSX.Element;
export declare const useStateSteps: <Step extends string>(props: {
    steps: Step[];
    Button?: React.ReactElement<{
        onClick?: (() => void) | undefined;
    }, string | React.JSXElementConstructor<any>> | undefined;
}) => readonly [(theseProps: {
    children: React.ReactNode;
} & {
    step: Step;
    type?: "==" | ">=" | "<=" | undefined;
}) => React.JSX.Element | null, () => React.JSX.Element, (...args: never[]) => void, Step | undefined, (...args: never[]) => void];
