type Color = `#${string}`;
/**
 * To ensure that you pick a color with accessible contrast based on a given hex
 * color, you can use the WCAG 2.1 guidelines for color contrast. Typically, a
 * contrast ratio of at least 4.5:1 is required for normal text, and 3:1 for
 * larger text.
 */
export declare const pickAccessibleColor: (baseHex: Color, palette: Readonly<Color[]>) => `#${string}`;
export {};
