export declare const useGuardedCallback: <FuncParams = void>(cb: ((...args: FuncParams[]) => void) | undefined) => (...args: FuncParams[]) => void;
