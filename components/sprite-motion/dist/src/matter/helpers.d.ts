import { Engine } from "matter-js";
export declare const matterBodyByLabel: (engine: Engine | undefined, label: string) => import("matter-js").Body | undefined;
