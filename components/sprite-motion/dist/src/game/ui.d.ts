import React from "react";
/**
 * @deprecated Do not use. It is internal
 * @internal
 */
export declare const UiPanelOuter: import("@styled-system/jsx/index").StyledComponent<"div", {}>;
/**
 * @deprecated This is internal don't use it
 * @internal don't use this
 */
export declare const UiLine: (props: {
    children: string;
    speed: number;
    onDone: () => void;
}) => React.JSX.Element;
/**
 * @deprecated This is internal don't use it
 * @internal don't use this
 */
export declare const UiLines: (props: {
    children: string | string[];
    speed: number;
    onDone: () => void;
}) => React.JSX.Element;
/**
 * Customization:
 *
 * - Set font-family on top-most component.
 * - Set bg on top-most component, immediate child DIV.
 * - Set color on top-most component, immediate child DIV.
 * - Set border color on immediate child DIV.
 *
 * Note: the difference between this one and the non-reactive version is that
 * when reactive=true, change the props and it will completely re-reinitialize
 * the component.
 */
export declare const UiDialogPanel: (props: Omit<Parameters<typeof UiPanelOuter>[0], "children"> & {
    title?: string;
    children: string | string[];
    wordSpeed?: number;
    lineSpeed?: number;
    visibleLines: number;
    onDone?: () => void;
}) => React.JSX.Element;
/**
 * Assumes the lines fit in the panel.
 *
 * Customization:
 *
 * - Set font-family on top-most component.
 * - Set bg on top-most component, immediate child DIV.
 * - Set color on top-most component, immediate child DIV.
 * - Set border color on immediate child DIV.
 */
export declare const UiPanel: (props: Omit<Parameters<typeof UiPanelOuter>[0], "children"> & {
    title?: string;
    children: React.ReactNode;
}) => React.JSX.Element;
export declare const UiKeyValue: (props: {
    items: Array<{
        key: string;
        value: string;
    }>;
}) => React.JSX.Element;
export declare const UiPlayerInfoPanel: (props: Parameters<typeof UiPanelOuter>[0] & {
    info: Array<{
        key: string;
        value: string;
    }>;
}) => React.JSX.Element;
