import React from "react";
declare const selectionMarker = "\u25B6 ";
type TextFont = `${number}px ${string}` | `${"bold" | "italic"} ${number}px ${string}`;
interface Location {
    left: number;
    top: number;
}
interface Dimensions {
    width: number;
    height: number;
}
interface ContainerOpts {
    radius?: number;
    textOffsetX: number;
    textOffsetY: number;
    speed?: number;
    useExtendedBgFill?: number;
}
interface TextStyles {
    lineHeightAsPx: number;
    font: TextFont;
    baseline?: CanvasRenderingContext2D["textBaseline"];
    fillStyle: `#${string}`;
}
interface ContainerStyles {
    strokeColor?: `#${string}`;
    strokeWidth?: number;
    fillColor?: `#${string}`;
}
export declare const UIDialogCommands: (props: {
    commands: string[];
    selectionMarker?: string | undefined;
    pos: Location & Dimensions & ContainerOpts;
    containerStyles: ContainerStyles;
    textStyles: TextStyles & {
        glitch?: boolean;
    };
    onChange?: ((newValue: string, prevValue: string) => void) | undefined;
    onSelect?: ((value: string) => void) | undefined;
    onCancel?: (() => void) | undefined;
}) => React.JSX.Element;
export declare const UiDialog: (props: {
    children: string;
    dialogPos: Location & Dimensions & ContainerOpts;
    textStyles: TextStyles & {
        glitch?: boolean;
    };
    containerStyles: ContainerStyles;
    onDone?: () => void;
}) => React.JSX.Element;
export declare const UiDialogWithCommands: <Command extends string>(props: {
    children: string;
    selectionMarker?: string | undefined;
    dialogPos: Location & Dimensions & ContainerOpts;
    commandsDelay?: number | undefined;
    commands: Command[];
    commandPos: Location & Dimensions & ContainerOpts;
    textStyles: TextStyles & {
        glitch?: boolean;
    };
    containerStyles: ContainerStyles;
    onChange?: ((newValue: Command, prevValue: Command) => void) | undefined;
    onSelect?: ((value: Command) => void) | undefined;
    onCancel?: (() => void) | undefined;
}) => React.JSX.Element;
export {};
