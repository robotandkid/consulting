import React from "react";
export declare const CommandWithDialogSubMenuExample: () => React.JSX.Element;
export declare const CommandWithSubCommandsExample: () => React.JSX.Element;
export declare const FullCommandPanelExample: () => React.JSX.Element;
export declare const UiDialogExample: () => React.JSX.Element;
