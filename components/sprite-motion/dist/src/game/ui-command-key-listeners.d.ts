export declare const useCommandKeyListeners: <Command extends string>(props: {
    commands: readonly Command[];
    onChange?: ((newValue: Command, prevValue: Command) => void) | undefined;
    onSelect?: ((value: Command) => void) | undefined;
    onCancel?: (() => void) | undefined;
    disabled?: boolean | undefined;
}) => Command;
