import typescript from "@rollup/plugin-typescript";
import acorn from "acorn-jsx";
import { terser } from "rollup-plugin-terser";

export default {
  /** Entry file */
  input: "index.ts",
  output: {
    dir: "dist",
    format: "esm",
    sourcemap: true,
  },
  /** For JSX */
  acornInjectPlugins: [acorn()],
  plugins: [
    /** For TS 🍩 */
    typescript({
      tsconfig: "tsconfig.types.json",
    }),
    terser(),
  ],
  /** Assumes this is React-focused. Remove as necessary */
  external: [
    "framer-motion",
    "react",
    "styled-components",
    "next/image",
    "next/router",
    "next/link",
    "next/head",
    "react-dom/server",
    "matter-js",
    "popmotion",
  ],
};
