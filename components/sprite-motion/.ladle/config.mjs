export default {
  stories: "**/*.fixture.{ts,tsx}",
  appendToHead: `
  <link
    rel="preload"
    href="/fonts/mrpixel.otf"
    as="font"
    crossOrigin="anonymous"
  />`,

};

