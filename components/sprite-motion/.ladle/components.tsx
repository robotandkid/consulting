import "../styled-system/styles.css";
import type { GlobalProvider } from "@ladle/react";
import React from "react";
import { LazyMotion, domAnimation } from "framer-motion";
import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'mrpixel';
    src: url('/fonts/mrpixel.otf');
  }

  body {
    font-size: 62.5%;
    margin: 0;
    padding: 0;
  }
`;
export const Provider: GlobalProvider = (props) => (
  <>
    <GlobalStyle />
    <LazyMotion features={domAnimation}>{props.children}</LazyMotion>
  </>
);
