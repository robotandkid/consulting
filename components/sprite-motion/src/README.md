## Overview

- `canvas` - wrapper for the render that uses Canvas
- `sprite-motion` - basic components like MotionDiv
- `special` - one off components like ActionBlock and ExplodingImage
- `sprite-sheet`
- `storyboard`
- `video`
