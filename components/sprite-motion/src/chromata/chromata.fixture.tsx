import { Canvas, Panel, StoryBoard, useDownloadsCtx, World } from "../../index";
import { Chromata } from "./chromata";
import React from "react";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  test: {
    type: "image",
    url: "./test.jpg",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
} as const;

const Inner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Chromata
      imageSrc={downloads.test}
      options={{
        pathFinderCount: 25,
        speed: 9,
        turningAngle: Math.PI / 6,
        colorMode: "rgb",
        lineWidth: 6,
        lineMode: "point",
        compositeOperation: "hue",
        origin: ["50% 50%"],
        key: "low",
        iterationLimit: 500,
        onIterationDone: () => ({}),
      }}
    />
  );
};

export const ChromataRGBExample = () => (
  <World
    width={512}
    height={(512 * 4) / 3}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <Inner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const ColorInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Chromata
      imageSrc={downloads.test}
      options={{
        pathFinderCount: 100,
        speed: 5,
        turningAngle: Math.PI / 2,
        colorMode: "color",
        palette: [
          "#f23f3f",
          "#f8f838",
          "#60ff1c",
          "#19f9ea",
          "#166bea",
          "#7e3df6",
          "#da14ac",
          "#ef172d",
        ],
        lineWidth: 6,
        lineMode: "point",
        compositeOperation: "saturation",
        origin: ["50% 50%"],
        key: "low",
      }}
    />
  );
};

export const ChromataColorExample = () => (
  <World
    width={512}
    height={(512 * 4) / 3}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <ColorInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
