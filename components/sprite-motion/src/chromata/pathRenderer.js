// @ts-check

import { oneBitToRgb } from "./utils";

/**
 * @typedef {import("./pathFinder").default} PathFinder
 *
 * @typedef {import("./shared.types").PathRendererOpts} Options
 *
 * @typedef {import("./shared.types").RGBA} RGBA
 *
 * @typedef {import("./shared.types").ChromataPixel} Pixel
 *
 * @typedef {{
 *   lineMode: "smooth" | "square" | "point";
 *   speed: number;
 *   lineWidth: number;
 *   colorMode: "rgb" | "grayscale";
 * }} BaseOptions
 */

/**
 * @param {RGBA} color
 * @returns
 */
const getStrokeColor = (color) => {
  return `rgba(${color.join(",")})`;
};

/**
 * @param {Pixel} aPixel
 * @param {Pixel} bPixel
 * @returns {RGBA}
 */
const middleColor = (aPixel, bPixel) => {
  const a = aPixel[2];
  const b = bPixel[2];
  return [
    Math.floor((a[0] + b[0]) / 2),
    Math.floor((a[1] + b[1]) / 2),
    Math.floor((a[2] + b[2]) / 2),
    1,
  ];
};

/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {Pixel} p1
 * @param {Pixel} p2
 * @param {Options} options
 */
const createGradient = (ctx, p1, p2, options) => {
  const [x1, y1, rgba1] = p1;
  const [x2, y2, rgba2] = p2;
  const grad = ctx.createLinearGradient(x1, y1, x2, y2);
  let startColor;
  let endColor;

  const { colorMode } = options;
  switch (colorMode) {
    case "rgb":
    case "grayscale": {
      const startValue = rgba1.find((value) => value !== 0) ?? 0;
      const endValue = rgba2.find((value) => value !== 0) ?? 0;
      const startRgb = oneBitToRgb(startValue, options);
      const endRgb = oneBitToRgb(endValue, options);
      startColor = getStrokeColor(startRgb);
      endColor = getStrokeColor(endRgb);
      break;
    }
    case "color": {
      const startValue = rgba1;
      const endValue = rgba2;
      startColor = getStrokeColor(startValue);
      endColor = getStrokeColor(endValue);
      break;
    }
    default: {
      /** @type {never} */
      const x = colorMode;
      throw new Error(`unsupported color mode ${x}`);
    }
  }

  grad.addColorStop(0, startColor);
  grad.addColorStop(1, endColor);
  return grad;
};

/**
 * Renders the points created by a Pathfinder
 *
 * # Open Qs
 *
 * How does the renderer work?
 *
 * The simplest to understand is "draw point"---it uses the RGBA data in the
 * Pixel to color the point.
 *
 * In the case of colorMode=RGB, the RGBA data contains the assigned RGB
 * channel. That is, it will be one of [1,0,0,1], [0,1,0,1], or [0,0,1,1].
 *
 * How is the ChromaPixel set?
 *
 * See utils#indexToRgbString which is used in seedPoint (setup).
 */
export default class PathRenderer {
  /** @type {CanvasRenderingContext2D} */
  context;
  /**
   * The third coordinate is the color 🤷
   *
   * @type {Pixel}
   */
  currentPoint = [0, 0, [0, 0, 0, 0]];
  /** @type {{ r: number; g: number; b: number }} */
  color;

  /**
   * @param {CanvasRenderingContext2D} context
   * @param {PathFinder} pathFinder
   * @param {Options} options
   */
  constructor(context, pathFinder, options) {
    this.context = context;
    this.pathFinder = pathFinder;
    this.options = options;
    this.color = pathFinder.getColor();
  }

  drawNextLine() {
    if (this.options.lineMode === "smooth") {
      this._drawLineSmooth();
    } else if (this.options.lineMode === "square") {
      this._drawLineSquare();
    } else {
      this._drawPoint();
    }
  }

  _drawLineSmooth() {
    const nextPoint = this.pathFinder.getNextPoint();

    if (nextPoint) {
      if (typeof this.currentPoint === "undefined") {
        this.currentPoint = nextPoint;
      }
      if (typeof this.controlPoint === "undefined") {
        this.controlPoint = nextPoint;
      }

      const midX = Math.round((this.controlPoint[0] + nextPoint[0]) / 2);
      const midY = Math.round((this.controlPoint[1] + nextPoint[1]) / 2);
      const midColor = middleColor(this.currentPoint, nextPoint);
      const lineLength = this._getLineLength(this.currentPoint, nextPoint);

      if (lineLength <= this.options.speed * 3) {
        const grad = createGradient(
          this.context,
          this.currentPoint,
          nextPoint,
          this.options
        );
        this.context.strokeStyle = grad;

        this.context.lineWidth = this.options.lineWidth;
        this.context.lineCap = "round";
        this.context.beginPath();

        this.context.moveTo(this.currentPoint[0], this.currentPoint[1]);
        this.context.quadraticCurveTo(
          this.controlPoint[0],
          this.controlPoint[1],
          midX,
          midY
        );
        this.context.stroke();
      }

      this.currentPoint = [midX, midY, midColor];
      this.controlPoint = nextPoint;
    }
  }

  _drawLineSquare() {
    var lineLength,
      nextPoint = this.pathFinder.getNextPoint();

    if (nextPoint) {
      if (typeof this.currentPoint === "undefined") {
        this.currentPoint = nextPoint;
      }

      lineLength = this._getLineLength(this.currentPoint, nextPoint);

      if (lineLength <= this.options.speed + 1) {
        const grad = createGradient(
          this.context,
          this.currentPoint,
          nextPoint,
          this.options
        );

        this.context.strokeStyle = grad;
        this.context.lineWidth = this.options.lineWidth;
        this.context.lineCap = "round";
        this.context.beginPath();

        this.context.moveTo(this.currentPoint[0], this.currentPoint[1]);
        this.context.lineTo(nextPoint[0], nextPoint[1]);
        this.context.stroke();
      }

      this.currentPoint = nextPoint;
    }
  }

  _drawPoint() {
    const nextPoint = this.pathFinder.getNextPoint();

    if (nextPoint) {
      if (typeof this.currentPoint === "undefined") {
        this.currentPoint = nextPoint;
      }

      const lineLength = this._getLineLength(this.currentPoint, nextPoint);

      if (lineLength >= this.options.speed * 2) {
        this.context.beginPath();

        this.context.arc(
          nextPoint[0],
          nextPoint[1],
          this.options.lineWidth,
          0,
          2 * Math.PI,
          false
        );
        this.context.fillStyle = getStrokeColor(nextPoint[2]);
        this.context.fill();

        this.currentPoint = nextPoint;
      }
    }
  }

  /**
   * @param {Pixel} p1
   * @param {Pixel} p2
   * @returns
   */
  _getLineLength(p1, p2) {
    var dx = p2[0] - p1[0];
    var dy = p2[1] - p1[1];
    return Math.round(Math.sqrt(dx * dx + dy * dy));
  }
}
