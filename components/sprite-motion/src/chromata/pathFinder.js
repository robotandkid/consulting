/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-check

import PathQueue from "./pathQueue";
import { oneBitToRgb } from "./utils";

const MAX = 255;

/**
 * @typedef {import("./shared.types").RGBA} RGBA
 *
 * @typedef {import("./shared.types").ChromataPixel} Pixel {x, y, rgba}
 *
 * @typedef {import("./shared.types").ChromataColor} Color
 */
export default class PathFinder {
  /** @type {RGBA[][]} */
  pixelArray;
  /** @type {Color} */
  targetColor;

  /**
   * @param {RGBA[][]} pixelArray
   * @param {any} workingArray
   * @param {Color} targetColor
   * @param {number} initX
   * @param {number} initY
   *
   * @typedef {import("./shared.types").PathFinderOpts} Options
   * @param {Options} options
   */
  constructor(pixelArray, workingArray, targetColor, initX, initY, options) {
    this.pixelArray = pixelArray;
    this.workingArray = workingArray;
    this.arrayWidth = pixelArray[0]?.length ?? 0;
    this.arrayHeight = pixelArray.length;
    this.x = Math.round(initX);
    this.y = Math.round(initY);
    this.options = options;
    this.pathQueue = new PathQueue(10);
    this.velocity = options.startingVelocity;

    this.targetColor = targetColor;
    this.rgbIndex = this._getRgbIndex(this.targetColor);

    if (this.options.key === "low") {
      this.comparatorFn = (
        /** @type {number} */ distance,
        /** @type {number} */ closest
      ) => {
        return 0 < distance && distance < closest;
      };
    } else {
      this.comparatorFn = (
        /** @type {number} */ distance,
        /** @type {number} */ closest
      ) => {
        return closest < distance && distance < MAX;
      };
    }
  }

  /** Get next coordinate point in path. */
  getNextPoint() {
    /** @type {ReturnType<typeof this._getNextPixel>} */
    let result;
    let i = 0;
    const limit = 5; // prevent an infinite loop

    do {
      result = this._getNextPixel();
      i++;
    } while (i <= limit && result.isPristine === false);

    return result.nextPixel;
  }

  /**
   * Algorithm for finding the next point by picking the closest match out of an
   * arc-shaped array of possible pixels arranged pointing in the direction of
   * velocity.
   *
   * @private
   */
  _getNextPixel() {
    let theta = this._getVelocityAngle();
    let closestColor = this.options.key === "low" ? 100000 : 0;
    /** @type {Pixel | undefined} */
    let nextPixel;
    /** @type {Pixel | undefined} */
    let defaultNextPixel;
    const arcSize = this.options.turningAngle;

    const radius = Math.round(
      Math.sqrt(Math.pow(this.velocity[0], 2) + Math.pow(this.velocity[1], 2))
    );

    const sampleSize = 4; // how many surrounding pixels to test for next point

    for (
      let angle = theta - arcSize / 2, deviance = -sampleSize / 2;
      angle <= theta + arcSize / 2;
      angle += arcSize / sampleSize, deviance++
    ) {
      const x = this.x + Math.round(radius * Math.cos(angle));
      const y = this.y + Math.round(radius * Math.sin(angle));
      let colorDistance = MAX;

      if (this._isInRange(x, y)) {
        const visited = this.workingArray[y]?.[x]?.[this.rgbIndex] ?? 0;
        const currentPixel = this.pixelArray[y]?.[x];
        const alpha = currentPixel?.[3] ?? 0;

        colorDistance = this._getColorDistance(currentPixel);

        if (
          this.comparatorFn(colorDistance, closestColor) &&
          !visited &&
          alpha === MAX
        ) {
          if (this.options.colorMode === "color") {
            /** @type {RGBA} */
            const rgba = [...this.targetColor.rgba];
            rgba[3] = MAX - colorDistance;
            nextPixel = [x, y, rgba];
          } else {
            const oneBit = MAX - colorDistance;
            const oneBitColor = oneBitToRgb(oneBit, this.options);
            nextPixel = [x, y, oneBitColor];
          }
          closestColor = colorDistance;
        }
      }

      if (deviance === 0) {
        const pa = this.pixelArray;
        if (pa[y] && pa[y]?.[x] && pa[y]?.[x]?.[3] === MAX) {
          if (this.options.colorMode === "color") {
            /** @type {RGBA} */
            const rgba = [...this.targetColor.rgba];
            rgba[3] = MAX - colorDistance;
            defaultNextPixel = [x, y, rgba];
          } else {
            const oneBit = MAX - colorDistance;
            const oneBitColor = oneBitToRgb(oneBit, this.options);
            defaultNextPixel = [x, y, oneBitColor];
          }
        } else {
          defaultNextPixel = this.pathQueue.get(-2) ?? [0, 0, [0, 0, 0, 0]];
        }
      }
    }

    const isPristine = typeof nextPixel !== "undefined";
    nextPixel = nextPixel || defaultNextPixel;

    if (nextPixel) {
      this.velocity = /** @type {[number, number]} */ ([
        nextPixel[0] - this.x,
        nextPixel[1] - this.y,
      ]);
      this.y = nextPixel[1];
      this.x = nextPixel[0];
      this._updateWorkingArray(nextPixel[1], nextPixel[0]);
      this.pathQueue.put(nextPixel);
    }

    return {
      nextPixel: nextPixel,
      isPristine: isPristine,
    };
  }

  getColor() {
    return {
      r: this.targetColor.rgba[0],
      g: this.targetColor.rgba[1],
      b: this.targetColor.rgba[2],
    };
  }

  /**
   * Get the angle indicated by the velocity vector, correcting for the case
   * that the angle would take the pathfinder off the image canvas, in which
   * case the angle will be set towards the centre of the canvas.
   *
   * @private
   * @returns {any}
   */
  _getVelocityAngle() {
    var projectedX = this.x + this.velocity[0],
      projectedY = this.y + this.velocity[1],
      margin = this.options.speed,
      dy = this.y + this.velocity[1] - this.y,
      dx = this.x + this.velocity[0] - this.x,
      angle;

    // has it gone out of bounds on the x axis?
    if (projectedX <= margin || this.arrayWidth - margin <= projectedX) {
      dx *= -1;
    }

    // has it gone out of bounds on the y axis?
    if (projectedY <= margin || this.arrayHeight - margin <= projectedY) {
      dy *= -1;
    }

    angle = Math.atan2(dy, dx);
    return angle;
  }

  /**
   * @private
   * @param {RGBA | undefined} pixel
   */
  _getColorDistance(pixel) {
    return MAX - (pixel?.[this.rgbIndex] ?? 0);
  }

  /**
   * Return true if the x, y points lie within the image dimensions.
   *
   * @private
   * @param {number} x
   * @param {number} y
   * @returns {boolean}
   */
  _isInRange(x, y) {
    return 0 < x && x < this.arrayWidth && 0 < y && y < this.arrayHeight;
  }

  /**
   * @param {number} row
   * @param {number} col
   */
  _updateWorkingArray(row, col) {
    if (this.workingArray[row]?.[col])
      this.workingArray[row][col][this.rgbIndex] = true;
  }

  /**
   * @private
   * @param {Color} color
   */
  _getRgbIndex(color) {
    return color.rgba.findIndex((value) => value > 0);
  }
}
