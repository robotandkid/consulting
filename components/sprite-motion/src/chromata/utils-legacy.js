// @ts-check

/** Static utilities class containing helper functions */
export default class Utils {
  /**
   * Get a 2d array (width x height) representing each pixel of the source as an
   * [r,g,b,a] array.
   *
   * @param {CanvasRenderingContext2D | null} sourceContext
   */
  static _getImageArray(sourceContext) {
    let width = sourceContext?.canvas.width ?? 0;
    let height = sourceContext?.canvas.height ?? 0;
    let imageData = sourceContext?.getImageData(0, 0, width, height);

    /**
     * @typedef {[number, number, number, number]} Pixel
     * @type {Pixel[][]}
     */
    const imageArray = [];

    for (let row = 0; row < height; row++) {
      imageArray.push([]);

      for (let col = 0; col < width; col++) {
        /** @type {Pixel} */
        let pixel = [0, 0, 0, 0];
        let position = row * width * 4 + col * 4;

        for (let part = 0; part < 4; part++) {
          pixel[part] = imageData?.data[position + part] ?? 0;
        }

        imageArray[row]?.push(pixel);
      }
    }

    return imageArray;
  }

  /**
   * Create a 2d array with the same dimensions as the image, but filled with
   * "null" pixels that will get filled in when a pathFinder visits each pixel.
   * Allows multiple pathFinders to communicate which pixels have been covered.
   *
   * @param {CanvasRenderingContext2D | null} sourceContext
   */
  static _getWorkingArray(sourceContext) {
    let width = sourceContext?.canvas.width ?? 0;
    let height = sourceContext?.canvas.height ?? 0;
    /** @type {any[]} */
    let workingArray = [];

    for (let row = 0; row < height; row++) {
      workingArray.push([]);

      for (let col = 0; col < width; col++) {
        workingArray[row].push([false, false, false]);
      }
    }

    return workingArray;
  }

  /**
   * @param {HTMLImageElement} image
   * @param {CanvasRenderingContext2D} ctx
   * @param {"original" | "container" | undefined} size
   * @returns
   */
  static _getOutputDimensions(image, ctx, size) {
    let width, height;

    if (size === "original") {
      width = image.width;
      height = image.height;
    } else {
      let container = ctx.canvas;
      let ratioW = (container?.clientWidth ?? 0) / image.width;
      let ratioH = (container?.clientHeight ?? 0) / image.height;
      let smallerRatio = ratioH <= ratioW ? ratioH : ratioW;

      width = image.width * smallerRatio;
      height = image.height * smallerRatio;
    }

    return {
      width: width,
      height: height,
    };
  }
}
