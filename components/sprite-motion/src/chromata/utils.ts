import { ChromataColor, ChromataOptions, HexColor, RGBA } from "./shared.types";

const hexToRgb = (color: HexColor): RGBA => {
  let hex = color;
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  }) as HexColor;

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  return result
    ? [
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        parseInt(result[1], 16),
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        parseInt(result[2], 16),
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        parseInt(result[3], 16),
        1,
      ]
    : [0, 0, 0, 1];
};

type Options =
  | {
      colorMode: "rgb";
      channel: "r" | "g" | "b";
    }
  | { colorMode: "grayscale"; channel?: undefined };

export const oneBitToRgb = (value: number, options: Options): RGBA => {
  let band: NonNullable<Options["channel"] | "grayscale">;
  const { colorMode } = options;
  switch (colorMode) {
    case "rgb":
      band = options.channel;
      break;
    case "grayscale":
      band = "grayscale";
      break;
    default: {
      const x: never = colorMode;
      throw new Error(`unsupported color mode ${x}`);
    }
  }

  switch (band) {
    case "r":
      return [value, 0, 0, 1];
    case "g":
      return [0, value, 0, 1];
    case "b":
      return [0, 0, value, 1];
    case "grayscale":
      return [value, value, value, 1];
    default: {
      const x: never = band;
      throw new Error(`unsupported band ${x}`);
    }
  }
};

/** When the palette is defined (when colorMode = color), rgba will be the color */
export const indexToRgbString = (
  i: number,
  opts: ChromataOptions
): ChromataColor => {
  const { colorMode } = opts;
  switch (colorMode) {
    case "grayscale":
    case "rgb": {
      const colors = ["#ff0000", "#00ff00", "#0000ff"] as const;
      const color = colors[i % colors.length] ?? "#ff0000";
      const rgba = hexToRgb(color);

      switch (color) {
        case "#ff0000":
          return { color, channel: "r", rgba };
        case "#00ff00":
          return { color, channel: "g", rgba };
        case "#0000ff":
          return { color, channel: "b", rgba };
        default: {
          const x: never = color;
          throw new Error(`unsupported color ${x}`);
        }
      }
    }
    case "color": {
      const colors = opts.palette;
      const color = colors[i % colors.length] ?? "#ff0000";
      const rgba = hexToRgb(color);
      const channels = ["r", "g", "b"] as const;
      const channel = channels[i % channels.length] ?? "r";
      return { color, channel, rgba };
    }
    default: {
      const x: never = colorMode;
      throw new Error(`unsupported colorMode ${x}`);
    }
  }
};
