import Utils from "./utils-legacy";
import { indexToRgbString } from "./utils";
import PathFinder from "./pathFinder";
import PathRenderer from "./pathRenderer";
import { useEffect, useState } from "react";
import { ResourceImg } from "../resources";
import { usePanelCanvasCtx2D } from "../storyboard";
import {
  ChromataBaseOptions,
  ChromataBaseColorOpts,
  ChromataOptions,
  PathFinderOpts,
  PathRendererOpts,
  RGBA,
} from "./shared.types";

type ClsOptions = Required<ChromataBaseOptions> & ChromataBaseColorOpts;

type Callback = () => void;

type SeedOptions = Required<
  Pick<ChromataBaseOptions, "speed" | "turningAngle" | "key">
> & {
  startingVelocity: Readonly<[number, number]>;
};

/**
 * Accepts as input an image, then it hides the image and renders the canvas
 * corresponding to the renderContext. See _appendRenderCanvas.
 *
 * Open Qs:
 *
 * - Why does it use setTimeout (in the loader)? Not needed. Can move the image
 *   loading to start() and convert image loading to a promise, and then just
 *   wait for that promise to resolve before starting. This eliminates the
 *   entire loader.
 * - Run seems to be the setup
 * - Tick seems to be the thing that actually does the work
 * - Can we use this w/ sprite-motion? Yes.
 * - How does the renderer work
 */
class ChromataCls {
  private options: ClsOptions;
  private sourceContext: CanvasRenderingContext2D;
  private renderContext: CanvasRenderingContext2D;
  private isRunning: boolean;
  private iterationCount: number;
  private loader: (cb: Callback) => void;
  private dimensions = { width: 0, height: 0 };
  private raf: number | undefined;

  private imageArray: RGBA[][];
  private workingArray: any;

  private _tick: Callback | undefined;

  constructor(
    imageSrc: string,
    renderContext: CanvasRenderingContext2D,
    options: ChromataOptions
  ) {
    const sourceCanvas = document.createElement("canvas");
    const sourceContext = sourceCanvas.getContext("2d");

    const image = new Image();

    let ready = false;

    this.options = this.defaults(options);

    image.width = options.dimensions?.width ?? renderContext.canvas.width;
    image.height = options.dimensions?.height ?? renderContext.canvas.height;

    image.addEventListener("load", () => {
      const dimensions = Utils._getOutputDimensions(
        image,
        renderContext,
        options.dimensions ? "original" : "container"
      );

      sourceCanvas.width = renderContext.canvas.width = dimensions.width;
      sourceCanvas.height = renderContext.canvas.height = dimensions.height;

      sourceContext?.drawImage(
        image,
        0,
        0,
        dimensions.width,
        dimensions.height
      );

      this.dimensions = dimensions;
      this.imageArray = Utils._getImageArray(sourceContext);
      this.workingArray = Utils._getWorkingArray(sourceContext);

      ready = true;
    });

    image.src = imageSrc;

    this.loader = (callback) => {
      if (!ready) {
        setTimeout(() => this.loader(callback), 50);
      } else {
        callback();
      }
    };

    this.imageArray = [];
    this.sourceContext = sourceContext ?? new CanvasRenderingContext2D();
    this.renderContext = renderContext;
    this.isRunning = false;
    this.iterationCount = 0;
  }

  /** Start the animation. */
  start() {
    this.loader(() => {
      this.isRunning = true;

      if (typeof this._tick === "undefined") {
        this._run();
      } else {
        this._tick();
      }
    });
  }

  stop(): number {
    this.isRunning = false;
    return this.iterationCount;
  }

  toggle() {
    if (this.isRunning) {
      return this.stop();
    } else {
      return this.start();
    }
  }

  /** Clear the canvas and set the animation back to the start. */
  reset() {
    this.isRunning = false;
    this._tick = undefined;
    cancelAnimationFrame(this.raf ?? 0);
    this.renderContext?.clearRect(
      0,
      0,
      this.dimensions.width,
      this.dimensions.height
    );
    this.workingArray = Utils._getWorkingArray(this.sourceContext);
  }

  private defaults(options: ChromataOptions): ClsOptions {
    const {
      iterationLimit = 0,
      onIterationDone = () => ({}),
      key = "low",
      dimensions = { width: undefined, height: undefined },
    } = options;
    let { lineWidth, pathFinderCount, speed, turningAngle } = options;

    pathFinderCount = this._limitToRange(pathFinderCount, 1, 10000);

    lineWidth = this._limitToRange(lineWidth, 1, 100);
    speed = this._limitToRange(speed, 1, 100);
    turningAngle = this._limitToRange(turningAngle, 0.1, 10);

    return {
      ...options,
      iterationLimit,
      onIterationDone,
      key,
      lineWidth,
      speed,
      turningAngle,
      pathFinderCount,
      dimensions,
    };
  }

  _limitToRange(val: number, low: number, high: number) {
    return Math.min(Math.max(val, low), high);
  }

  /** Set up the pathfinders and renderers and get the animation going. */
  private _run() {
    const renderers: PathRenderer[] = [];
    const pathFinders = this._initPathFinders(this.options);

    const renderOptions = {
      colorMode: this.options.colorMode,
      lineWidth: this.options.lineWidth,
      lineMode: this.options.lineMode,
      speed: this.options.speed,
    };

    if (!this.renderContext) return;

    this.renderContext.save();
    this.renderContext.globalCompositeOperation =
      this.options.compositeOperation;

    pathFinders.forEach((pathFinder) => {
      let options: PathRendererOpts;
      const { colorMode } = pathFinder.options;
      switch (colorMode) {
        case "rgb":
          options = {
            ...renderOptions,
            colorMode: "rgb",
            channel: pathFinder.options.channel,
          };
          break;
        case "color":
          options = {
            ...renderOptions,
            colorMode: "color",
          };
          break;
        case "grayscale":
          options = {
            ...renderOptions,
            colorMode: "grayscale",
          };
          break;
        default: {
          const x: never = colorMode;
          throw new Error(`unsupported colorMode ${x}`);
        }
      }
      renderers.push(new PathRenderer(this.renderContext, pathFinder, options));
    });

    this._tick = () => {
      const { iterationLimit = 0, onIterationDone } = this.options;
      if (0 < iterationLimit && iterationLimit <= this.iterationCount) {
        this.isRunning = false;
        this.options.iterationLimit = 0;
        this.renderContext.restore();
        onIterationDone();
      }

      renderers.forEach((renderer) => renderer.drawNextLine());
      this.iterationCount++;

      if (this.isRunning) {
        this.raf = requestAnimationFrame(this._tick ?? (() => {}));
      }
    };

    this._tick();
  }

  /**
   * Create the pathfinders
   *
   * @private
   */
  _initPathFinders(options: ClsOptions) {
    const pathFinders: PathFinder[] = [];
    const count = this.options.pathFinderCount;
    const origins = this.options.origin;
    const pathFindersPerOrigin = count / origins.length;

    const seedOptions: SeedOptions = {
      ...options,
      startingVelocity: [0, 0] as const,
    };

    if (-1 < origins.indexOf("bottom")) {
      this._seedBottom(pathFindersPerOrigin, pathFinders, seedOptions, options);
    }
    if (-1 < origins.indexOf("top")) {
      this._seedTop(pathFindersPerOrigin, pathFinders, seedOptions, options);
    }
    if (-1 < origins.indexOf("left")) {
      this._seedLeft(pathFindersPerOrigin, pathFinders, seedOptions, options);
    }
    if (-1 < origins.indexOf("right")) {
      this._seedRight(pathFindersPerOrigin, pathFinders, seedOptions, options);
    }

    origins.forEach((origin) => {
      const matches = origin.match(/(\d{1,3})% (\d{1,3})%/);
      if (matches) {
        this._seedPoint(
          pathFindersPerOrigin,
          pathFinders,
          seedOptions,
          options,
          +(matches[1] ?? "0"),
          +(matches[2] ?? "0")
        );
      }
    });

    return pathFinders;
  }

  private _seedTop(
    count: number,
    pathFinders: PathFinder[],
    seedOptions: SeedOptions,
    options: ClsOptions
  ) {
    const width = this.dimensions.width;
    const unit = width / count;
    const xPosFn = (i: number) => unit * i - unit / 2;
    const yPosFn = () => this.options.speed;

    seedOptions.startingVelocity = [0, this.options.speed];

    this._seedCreateLoop(
      count,
      pathFinders,
      xPosFn,
      yPosFn,
      seedOptions,
      options
    );
  }

  private _seedBottom(
    count: number,
    pathFinders: PathFinder[],
    seedOptions: SeedOptions,
    options: ClsOptions
  ) {
    const width = this.dimensions.width;
    const height = this.dimensions.height;
    const unit = width / count;

    const xPosFn = (i: number) => unit * i - unit / 2;
    const yPosFn = () => height - this.options.speed;

    seedOptions.startingVelocity = [0, -this.options.speed];

    this._seedCreateLoop(
      count,
      pathFinders,
      xPosFn,
      yPosFn,
      seedOptions,
      options
    );
  }

  private _seedLeft(
    count: number,
    pathFinders: PathFinder[],
    seedOptions: SeedOptions,
    options: ClsOptions
  ) {
    const height = this.dimensions.height;
    const unit = height / count;
    const xPosFn = () => this.options.speed;
    const yPosFn = (i: number) => unit * i - unit / 2;

    seedOptions.startingVelocity = [this.options.speed, 0];
    this._seedCreateLoop(
      count,
      pathFinders,
      xPosFn,
      yPosFn,
      seedOptions,
      options
    );
  }

  private _seedRight(
    count: number,
    pathFinders: PathFinder[],
    seedOptions: SeedOptions,
    options: ClsOptions
  ) {
    const width = this.dimensions.width;
    const height = this.dimensions.height;
    const unit = height / count;
    const xPosFn = () => width - this.options.speed;
    const yPosFn = (i: number) => unit * i - unit / 2;

    seedOptions.startingVelocity = [-this.options.speed, 0];

    this._seedCreateLoop(
      count,
      pathFinders,
      xPosFn,
      yPosFn,
      seedOptions,
      options
    );
  }

  private _seedPoint(
    count: number,
    pathFinders: PathFinder[],
    seedOptions: SeedOptions,
    options: ClsOptions,
    xPc: number,
    yPc: number
  ) {
    const xPos = Math.floor((this.dimensions.width * xPc) / 100);
    const yPos = Math.floor((this.dimensions.height * yPc) / 100);

    for (let i = 1; i < count + 1; i++) {
      const color = indexToRgbString(i, options);
      let pathFinderOptions: PathFinderOpts;
      const { colorMode } = this.options;
      switch (colorMode) {
        case "rgb":
          pathFinderOptions = {
            ...seedOptions,
            colorMode: "rgb",
            channel: color.channel,
          };
          break;
        case "color":
          pathFinderOptions = {
            ...seedOptions,
            colorMode: "color",
          };
          break;
        case "grayscale":
          pathFinderOptions = {
            ...seedOptions,
            colorMode: "grayscale",
          };
          break;
        default: {
          const x: never = colorMode;
          throw new Error(`unsupported color mode ${x}`);
        }
      }

      const direction = i % 4;

      switch (direction) {
        case 0:
          seedOptions.startingVelocity = [-this.options.speed, 0];
          break;
        case 1:
          seedOptions.startingVelocity = [0, this.options.speed];
          break;
        case 2:
          seedOptions.startingVelocity = [this.options.speed, 0];
          break;
        case 3:
          seedOptions.startingVelocity = [0, -this.options.speed];
          break;
      }

      pathFinders.push(
        new PathFinder(
          this.imageArray,
          this.workingArray,
          color,
          xPos,
          yPos,
          pathFinderOptions
        )
      );
    }
  }

  private _seedCreateLoop(
    count: number,
    pathFinders: PathFinder[],
    xPosFn: (i: number) => number,
    yPosFn: (i: number) => number,
    seedOptions: SeedOptions,
    options: ChromataOptions
  ) {
    for (let i = 1; i < count + 1; i++) {
      const color = indexToRgbString(i, options);
      const xPos = xPosFn(i);
      const yPos = yPosFn(i);

      let pathFinderOptions: PathFinderOpts;
      const { colorMode } = this.options;
      switch (colorMode) {
        case "color":
          pathFinderOptions = {
            ...seedOptions,
            colorMode: "color",
          };
          break;
        case "rgb":
          pathFinderOptions = {
            ...seedOptions,
            colorMode: "rgb",
            channel: color.channel,
          };
          break;
        case "grayscale":
          pathFinderOptions = {
            ...seedOptions,
            colorMode: "grayscale",
          };
          break;
        default: {
          const x: never = colorMode;
          throw new Error(`unsupported color mode ${x}`);
        }
      }

      pathFinders.push(
        new PathFinder(
          this.imageArray,
          this.workingArray,
          color,
          xPos,
          yPos,
          pathFinderOptions
        )
      );
    }
  }
}

export const Chromata = (props: {
  imageSrc: ResourceImg;
  options: ChromataOptions;
}) => {
  const { options, imageSrc } = props;
  const [chromata, setChromata] = useState<ChromataCls>();
  const ctx = usePanelCanvasCtx2D();

  if (!chromata && ctx)
    imageSrc.resource.then((url) => {
      if (url) {
        const c = new ChromataCls(url, ctx, options);
        setChromata(c);
        c.start();
      }
    });

  useEffect(() => {
    return () => {
      chromata?.stop();
    };
  }, [chromata]);

  return null;
};
