import {
  Panel,
  PanelBackground,
  StoryBoard,
  StoryBoardNav,
} from "./storyboard";
import React, { useState } from "react";
import {
  World,
  MotionDiv,
  StaticDiv,
  usePagination,
  StaticImg,
} from "./sprite-motion";
import { ActionBlock } from "./special";
import { sleep } from "./utils/timeouts";
import { useDownloadsCtx } from "./resources";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
} as const;

const StoryboardWithAutoPagination = () => {
  const downloads = useDownloadsCtx<typeof resources>();
  const paginate = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <StaticDiv
          aria-label="bottom wall"
          cx={500}
          cy={500}
          width={1000}
          height={5}
          style={{ background: "green" }}
          onCollisionStart={() => {
            const go = async () => {
              downloads.downloads?.sound1.resource.play();
              await new Promise((resolve) => setTimeout(resolve, 3000));
              paginate?.("next-wipe");
            };

            go();
          }}
        />
        <MotionDiv
          startCx={0}
          startCy={0}
          vx={8}
          vy={-8}
          width={256}
          height={256}
          style={{ background: "pink" }}
        />
      </Panel>
      <Panel>
        <PanelBackground src="storyboard-02.png" />
      </Panel>
    </StoryBoard>
  );
};

export const StoryboardWithAutoPaginationExample = () => {
  const [show, setShow] = useState(false);

  return (
    <>
      {!show && <button onClick={() => setShow(true)}>Click to play</button>}
      {show && (
        <World
          resources={resources}
          loading={<div>Loading...</div>}
          width={500}
          height={500}
          style={{ overflow: "hidden" }}
        >
          <StoryboardWithAutoPagination />
        </World>
      )}
    </>
  );
};

const SimpleTransitionInner = () => {
  const downloads = useDownloadsCtx<typeof resources>();
  const paginate = usePagination();

  return (
    <StoryBoard disableManualNavigation>
      <Panel>
        <StaticDiv
          aria-label="bottom wall"
          cx={500}
          cy={500}
          width={1000}
          height={5}
          style={{ background: "green" }}
          onCollisionStart={() => {
            const go = async () => {
              downloads.downloads?.sound1.resource.play();
              await new Promise((resolve) => setTimeout(resolve, 3000));
              paginate?.("next");
            };

            go();
          }}
        />
        <MotionDiv
          startCx={0}
          startCy={0}
          vx={8}
          vy={-8}
          width={256}
          height={256}
          style={{ background: "pink" }}
        />
      </Panel>
      <Panel>
        <PanelBackground src="storyboard-02.png" />
      </Panel>
    </StoryBoard>
  );
};

export const SimpleTransitionExample = () => {
  const [show, setShow] = useState(false);

  return (
    <>
      {!show && <button onClick={() => setShow(true)}>Click to play</button>}
      {show && (
        <World
          resources={resources}
          loading={<div>Loading...</div>}
          width={500}
          height={500}
          style={{ overflow: "hidden" }}
        >
          <SimpleTransitionInner />
        </World>
      )}
    </>
  );
};

const StoryBoardWithNavigation = () => {
  const downloads = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <StaticDiv
          aria-label="bottom wall"
          cx={500}
          cy={500}
          width={1000}
          height={5}
          style={{ background: "green" }}
          onCollisionStart={() => {
            const go = async () => {
              downloads.downloads?.sound1.resource.play();
            };

            go();
          }}
        />
        <MotionDiv
          startCx={0}
          startCy={0}
          vx={8}
          vy={-8}
          width={256}
          height={256}
          style={{ background: "pink" }}
        />
      </Panel>
      <Panel>
        <StaticImg src={downloads.downloads?.dragon} />
      </Panel>
    </StoryBoard>
  );
};

export const StoryBoardWithNavigationExample = () => {
  const [show, setShow] = useState(false);

  return (
    <>
      {!show && <button onClick={() => setShow(true)}>Click to play</button>}
      {show && (
        <World
          resources={resources}
          loading={<div>Loading...</div>}
          width={500}
          height={500}
          style={{
            overflow: "hidden",
          }}
        >
          <StoryBoardWithNavigation />
          <StoryBoardNav />
        </World>
      )}
    </>
  );
};

export const FadeTransitionExample = () => {
  return (
    <World width={250} height={250}>
      <StoryBoard disableManualNavigation>
        <Panel>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              opts.paginate("next-fade");
            }}
          </ActionBlock>
          <h1>Hello</h1>
        </Panel>
        <Panel>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              opts.paginate("next-fade");
            }}
          </ActionBlock>
          <h1>World</h1>
        </Panel>
      </StoryBoard>
    </World>
  );
};

export const CanvasExample = () => {
  return (
    <World width={250} height={250} style={{ border: "1px solid black" }}>
      <StoryBoard disableManualNavigation>
        <Panel>
          <ActionBlock>
            {async (opts) => {
              const ctx = opts.canvas.getContext("2d");
              ctx?.beginPath();
              ctx?.arc(100, 75, 50, 0, 2 * Math.PI);
              ctx?.stroke();
              await sleep(1000);
              opts.paginate("next-fade");
            }}
          </ActionBlock>
          <h1>Hello</h1>
        </Panel>
        <Panel>
          <ActionBlock>
            {async (opts) => {
              await sleep(1000);
              opts.paginate("next-fade");
            }}
          </ActionBlock>
          <h1>World</h1>
        </Panel>
      </StoryBoard>
    </World>
  );
};
