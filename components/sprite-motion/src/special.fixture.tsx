import React, { useState } from "react";
import { useDownloadsCtx } from "./resources";
import { ActionBlock, ExplodingImage } from "./special";
import { StaticImg, usePagination, World } from "./sprite-motion";
import { Panel, StoryBoard } from "./storyboard";
import { sleep } from "./utils/timeouts";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
} as const;

const ExplodingImageInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <ExplodingImage
      delayInMs={1000}
      xNumParticles={10}
      yNumParticles={10}
      explosionFactor={30}
      src={downloads.dragon}
    />
  );
};

export const ExplodingImageExample = () => {
  return (
    <>
      <World
        resources={resources}
        loading={<div>Loading...</div>}
        width={500}
        height={500}
      >
        <ExplodingImageInner />
      </World>
    </>
  );
};

const GateBehindButton = (props: {
  children: React.ReactNode;
  text: string;
}) => {
  const [show, setShow] = useState(false);

  return (
    <>
      {show && props.children}
      {!show && <button onClick={() => setShow(true)}>{props.text}</button>}
    </>
  );
};

const ActionBlockInner = () => {
  const otherPaginate = usePagination();
  const otherDownloads = useDownloadsCtx<typeof resources>();

  return (
    <StoryBoard>
      <Panel>
        <ActionBlock<typeof resources>>
          {async ({ resources, paginate }) => {
            resources.downloads.sound1.resource.play();
            await sleep(3000);
            paginate("next-wipe");
          }}
        </ActionBlock>
        <StaticImg src={otherDownloads.downloads.dragon} />
      </Panel>
      <Panel>
        <ActionBlock>
          {async () => {
            await sleep(3000);
            otherPaginate?.("next-wipe");
          }}
        </ActionBlock>
        <div>Good bye world</div>
      </Panel>
    </StoryBoard>
  );
};

export const ActionBlockExample = () => {
  return (
    <GateBehindButton text="Click">
      <World
        resources={resources}
        loading={<div>Loading...</div>}
        width={500}
        height={500}
      >
        <ActionBlockInner />
      </World>
    </GateBehindButton>
  );
};
