import { useEffect, useRef } from "react";
import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

type TextFont =
  | `${number}px ${string}`
  | `${"bold italic" | "bold" | "italic"} ${number}px ${string}`;

export const Code = (props: {
  children: string | string[];
  top: number;
  left: number;
  speed?: number;
  onDone?: () => void;
  style?: {
    glitch?: boolean;
    lineHeightAsPx?: number;
    font?: TextFont;
    fillStyle?: `#${string}`;
    textBaseline?: CanvasRenderingContext2D["textBaseline"];
    shadowOffsetX?: number;
    shadowOffsetY?: number;
    shadowColor?: `#${string}`;
    shadowBlur?: number;
  };
}) => {
  const { speed, ...rest } = props;

  const render = useCanvasRegistration();

  const textIndex = useRef(0);
  const fullText = rest.children;

  useEffect(() => {
    if (speed === undefined) {
      textIndex.current = fullText.length;
      return;
    }

    const interval = setInterval(() => {
      const i = textIndex.current;
      textIndex.current = i + 1 <= fullText.length ? i + 1 : i;
    }, speed);

    return () => {
      clearInterval(interval);
    };
  }, [fullText.length, speed]);

  const [keyChanged, key] = useKeyRegistration("code", props);
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && render && ctx) {
    let glitchLen = 0;
    const glitchStops = [0, 1];
    const shouldGlitch = (len: number) => glitchStops.indexOf(len % 60) >= 0;

    render(() => {
      const style = {
        glitch: true,
        font: "28px mrpixel",
        fillStyle: "white",
        textBaseline: "top",
        lineHeightAsPx: 28,
        ...props.style,
      } as const;

      const text = Array.isArray(props.children)
        ? props.children.join("")
        : props.children;
      const lines = text.substring(0, textIndex.current).split("\n");

      const skipRendering = shouldGlitch(glitchLen++);
      if (style.glitch && skipRendering) return key;

      ctx.save();
      ctx.font = style.font;
      ctx.textBaseline = style.textBaseline;
      ctx.fillStyle = style.fillStyle;

      if (style.shadowColor) {
        ctx.shadowOffsetX = style.shadowOffsetX ?? 0;
        ctx.shadowOffsetY = style.shadowOffsetY ?? 0;
        ctx.shadowColor = style.shadowColor;
        ctx.shadowBlur = style.shadowBlur ?? 0;
      }

      for (let line = 0; line < lines.length; line += 1) {
        ctx.fillText(
          lines[line]!,
          props.left,
          props.top + line * style.lineHeightAsPx
        );
      }
      ctx.restore();

      if (textIndex.current === props.children.length) {
        props.onDone?.();
        textIndex.current += 1;
      }

      return key;
    });
  }

  return null;
};
