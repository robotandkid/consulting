import React, { useCallback, useEffect, useRef, useState } from "react";
import { usePanelCanvas, usePanelCanvasCtx2D } from "../storyboard";
import { makeReadOnlyContext } from "../utils/context";
import { stringify } from "../utils/json";

type Key = string;

type RenderCallback = (
  timeElapsed: number,
  timeElapsedBetweenFrames: number
) => Key | void;

const [RegisterCanvasRendererProvider, useRegisterCanvasRendererCtx] =
  makeReadOnlyContext<(id: string | undefined) => string>(
    "RegisterCanvasRender"
  );

const [UnregisterCanvasRendererProvider, useUnregisterCanvasRendererCtx] =
  makeReadOnlyContext<(id: string) => void>("UnregisterCanvasRenderer");

const [RenderCanvasProvider, useRenderCanvasCtx] =
  makeReadOnlyContext<(id: string, cb: RenderCallback) => void>("RenderCanvas");

const [ClearCanvasRendererProvider, useClearCanvasRendererCtx] =
  makeReadOnlyContext<() => void>("ClearCanvasRenderer");

export { useClearCanvasRendererCtx };

let len = 0;
const cheapUUID = () => `${len++}`;

/**
 * **WARNING:** An important caveat is that once registered, the resulting entry
 * is NEVER removed. This has performance ramifications and possible memory
 * issues.
 *
 * The way this works:
 *
 * - Every time there's a new render, we clear out the
 */
export const Canvas = (props: {
  children: React.ReactNode;
  /** @deprecated */
  width?: number;
  /** @deprecated */
  height?: number;
  disableClearCanvasBeforeUpdate?: boolean;
}) => {
  const renderCallbacks = useRef<
    Array<{ id: string; cb: RenderCallback; active: boolean }>
  >([]);
  const ctx = usePanelCanvasCtx2D();
  const { disableClearCanvasBeforeUpdate = false } = props;

  useEffect(() => {
    if (!ctx) return;

    let timeElapsed = 0;
    let timeElapsedBetweenFrames = 0;
    let animation: number;
    let prevNow = Date.now();
    const start = prevNow;

    const update = () => {
      if (!disableClearCanvasBeforeUpdate && renderCallbacks.current.length > 0)
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

      for (const { cb, active } of renderCallbacks.current) {
        //    console.log("id", id, active);
        if (active) {
          cb(timeElapsed, timeElapsedBetweenFrames);
          // if (key) console.log("key", id, key);
        }
      }

      const now = Date.now();
      timeElapsed = now - start;
      timeElapsedBetweenFrames = now - prevNow;
      prevNow = now;

      animation = requestAnimationFrame(update);
    };

    update();

    return () => {
      cancelAnimationFrame(animation);
    };
  }, [disableClearCanvasBeforeUpdate, ctx]);

  const [, setLen] = useState(0);

  const clear = useCallback(() => {
    renderCallbacks.current = [];
    setLen((l) => l + 1);
  }, []);

  /** Safe to call multiple times. */
  const register = useCallback((id: string | undefined) => {
    // check that it's a state update
    const current = renderCallbacks.current.find(
      (renderer) => renderer.id === id
    );
    if (current?.active && id) return id;
    // otherwise we're here because someone cleared the callbacks
    const newId = cheapUUID();

    renderCallbacks.current.push({
      id: newId,
      cb: () => "void",
      active: true,
    });

    return newId;
  }, []);

  const unregister = useCallback((id: string) => {
    const index = renderCallbacks.current.findIndex((cb) => cb.id === id);
    if (renderCallbacks.current[index])
      renderCallbacks.current[index]!.active = false;
  }, []);

  const render = useCallback((id: string, cb: RenderCallback) => {
    const previous = renderCallbacks.current.find((cb) => cb.id === id);
    if (!previous) {
      throw new Error(`Attempted to use an inactive renderer: ${id}`);
    }
    previous.cb = cb;
  }, []);

  return (
    <RegisterCanvasRendererProvider value={register}>
      <UnregisterCanvasRendererProvider value={unregister}>
        <RenderCanvasProvider value={render}>
          <ClearCanvasRendererProvider value={clear}>
            {props.children}
          </ClearCanvasRendererProvider>
        </RenderCanvasProvider>
      </UnregisterCanvasRendererProvider>
    </RegisterCanvasRendererProvider>
  );
};

export const useKeyRegistration = (name: string, props: any) => {
  const prevKey = React.useRef<string>();
  const key = `${name}:${stringify(props)}`;

  useEffect(() => {
    prevKey.current = undefined;
  }, []);

  if (prevKey.current !== key) {
    prevKey.current = key;
    return [true, key] as const;
  }

  return [false, key] as const;
};

export const useCanvasRegistration = () => {
  const renderKey = useRef<string>();
  const register = useRegisterCanvasRendererCtx();
  const canvas = usePanelCanvas();
  const [, setForceRender] = useState(0);

  /**
   * React life cycle
   *
   * - Need to register on mount
   * - State changes - don't re-register
   */
  if (register && canvas) {
    // you wait for the canvas as well for backwards-compatibility
    renderKey.current = register(renderKey.current);
  }

  const unregister = useUnregisterCanvasRendererCtx();

  useEffect(() => {
    // force a render because if it registers need to call render again
    setForceRender((c) => c + 1);
    return () => {
      if (renderKey.current) unregister?.(renderKey.current);
    };
  }, [register, unregister]);

  const render = useRenderCanvasCtx();

  type RenderParams = Parameters<NonNullable<typeof render>>[1];

  const key = renderKey.current;

  const renderWidthKey =
    render && key ? (cb: RenderParams) => render(key, cb) : undefined;

  return renderWidthKey;
};
