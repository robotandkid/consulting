import { Story } from "@ladle/react";
import {
  Canvas,
  CanvasSaveVideo,
  CanvasSaveVideoProvider,
  Panel,
  StoryBoard,
  useCanvasSaveVideoCtx,
  useDownloadsCtx,
  World,
} from "../../index";
import { cybergumsPalette } from "../utils/palettes";
import { CanvasImage } from "./canvas-image";
import { CanvasDithering } from "./dithering";
import { CanvasSpeechBubble } from "./speech-bubble";
import React from "react";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
} as const;

const Inner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const video = useCanvasSaveVideoCtx();

  return (
    <>
      <CanvasImage
        src={downloads.dragon}
        renderTop={0}
        renderLeft={0}
        renderWidth={512}
        renderHeight={512}
      />
      <CanvasDithering palette={cybergumsPalette()} />
      <CanvasSpeechBubble
        onDone={video?.stop}
        pos={{
          left: 10,
          top: 10,
          width: 250,
          height: 250,
          radius: 50,
          pLeft: 300,
          pTop: 50,
        }}
        text={{ rx: 10, ry: 10, speed: 1 }}
        textStyle={{
          glitch: false,
          fillStyle: "#000",
        }}
      >
        {`
she has always been
beautiful but her 
constant sour
            `}
      </CanvasSpeechBubble>
      <CanvasSaveVideo autoStart />
    </>
  );
};

export const ImageExample: Story<{ threshold: string[] }> = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <CanvasSaveVideoProvider>
      <StoryBoard>
        <Panel>
          <Canvas>
            <CanvasSaveVideoProvider>
              <Inner />
            </CanvasSaveVideoProvider>
          </Canvas>
        </Panel>
      </StoryBoard>
    </CanvasSaveVideoProvider>
  </World>
);
