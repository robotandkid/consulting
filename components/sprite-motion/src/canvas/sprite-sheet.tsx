import React, { useRef, useState } from "react";
import { ResourceImg } from "../resources";
import { usePanelCanvas } from "../storyboard";
import { useCanvasRegistration } from "./canvas";

const draw = (
  frame: number,
  opts: {
    frameWidth: number;
    frameHeight: number;
    renderLeft: number;
    renderTop: number;
    renderWidth: number;
    renderHeight: number;
    ctx: CanvasRenderingContext2D | null;
    img: HTMLImageElement;
  }
) => {
  const {
    frameWidth,
    frameHeight,
    ctx,
    renderLeft,
    renderTop,
    renderWidth,
    renderHeight,
    img,
  } = opts;

  const slice = [frame * frameWidth, 0, frameWidth, frameHeight] as const;

  //  ctx?.clearRect(renderLeft, renderTop, renderWidth, renderHeight);

  ctx?.drawImage(
    img,
    slice[0],
    slice[1],
    slice[2],
    slice[3],
    renderLeft,
    renderTop,
    renderWidth,
    renderHeight
  );
};

const supportedFpsSkipMap = {
  60: 1,
  30: 2,
  20: 3,
  15: 4,
  12: 5,
  10: 6,
  9: 7,
  8: 8,
  6: 10,
  5: 12,
  4: 15,
  2: 30,
  1: 60,
} as const;

const shouldIncrementToNextFrame = (
  frame: number,
  fps: keyof typeof supportedFpsSkipMap
) => frame % supportedFpsSkipMap[fps] === 0;

export const SpriteSheet = (props: {
  src: React.ImgHTMLAttributes<HTMLImageElement>["src"] | ResourceImg;
  /**
   * X-coordinate to render the sprite on the canvas.
   *
   * ```text
   *     (left,top)
   *          *-------+
   *          |       |
   *          +-------+
   * ```
   */
  renderLeft: number;
  /**
   * Y-coordinate to render the sprite on the canvas.
   *
   * ```text
   *     (left,top)
   *          *-------+
   *          |       |
   *          +-------+
   * ```
   */
  renderTop: number;
  renderWidth: number;
  renderHeight: number;
  frameWidth: number;
  frameHeight: number;
  fps?: keyof typeof supportedFpsSkipMap;
  loop?: boolean;
  onLoopEnd?: () => void;
}) => {
  const { src, ...rest } = props;

  const render = useCanvasRegistration();

  /** Go load the URL */
  const rawSrcUrl = typeof src === "string" ? src : src?.resource;

  const [srcUrl, setSrcUrl] = useState<string | undefined>(
    typeof rawSrcUrl === "string" ? rawSrcUrl : undefined
  );

  const loadedUrl = useRef(false);
  if (!loadedUrl.current) {
    loadedUrl.current = true;
    if (rawSrcUrl && typeof rawSrcUrl !== "string") rawSrcUrl.then(setSrcUrl);
  }

  /** Now draw it */
  const prevKey = useRef<string>();
  const canvas = usePanelCanvas();
  const key = `sprite-sheet:${JSON.stringify(props)}`;
  /** To support changing props, you have to store these in refs. */
  const opts = useRef({
    canvasFrame: 0,
    spriteFrame: 0,
    keepGoing: true,
  });

  if (prevKey.current !== key && srcUrl && canvas && render) {
    prevKey.current = key;

    const img = new Image();
    img.src = srcUrl;

    img.onload = () => {
      const ctx = canvas?.getContext("2d");

      render(() => {
        const totalFrames = Math.round(img.width / rest.frameWidth);

        const style = {
          fps: 30,
          loop: true,
          ...rest,
        } as const;

        draw(opts.current.spriteFrame, { ...style, ctx, img });

        if (opts.current.keepGoing) {
          opts.current.canvasFrame += 1;
          if (shouldIncrementToNextFrame(opts.current.canvasFrame, style.fps))
            opts.current.spriteFrame += 1;
        }

        if (opts.current.spriteFrame === totalFrames) {
          if (opts.current.keepGoing) style.onLoopEnd?.();
          if (!style.loop) opts.current.keepGoing = false;
          if (opts.current.keepGoing)
            opts.current.spriteFrame = opts.current.spriteFrame % totalFrames;
          else opts.current.spriteFrame -= 1;
        }

        opts.current.canvasFrame = opts.current.canvasFrame % 60;
      });
    };
  }

  return null;
};

export const SpriteSheetFromMany = (props: {
  src: ResourceImg[];
  renderLeft: number;
  renderTop: number;
  renderWidth: number;
  renderHeight: number;
  fps?: keyof typeof supportedFpsSkipMap;
  loop?: boolean;
  onLoopEnd?: () => void;
}) => {
  const { src, ...rest } = props;

  const render = useCanvasRegistration();

  /** Go load the URL */
  const [srcUrls, setSrcUrls] = useState<string[]>();

  const loadedUrl = useRef(false);
  if (!loadedUrl.current) {
    loadedUrl.current = true;
    const rawSrcUrls = src.map(({ resource }) => resource);

    Promise.all(rawSrcUrls).then((results) => {
      const resultsNonNull: string[] = [];

      results.forEach((result) => {
        if (result) resultsNonNull.push(result);
      });

      setSrcUrls(resultsNonNull);
    });
  }

  /** Now draw it */
  const prevKey = useRef<string>();
  const canvas = usePanelCanvas();
  const key = `sprite-sheet-from-many:${JSON.stringify(props)}`;

  /** To support changing props, you have to store these in refs. */
  const opts = useRef({
    keepGoing: true,
    tickCounter: 0,
    curFrame: 0,
  });

  if (prevKey.current !== key && srcUrls && canvas && render) {
    prevKey.current = key;

    const imagesPromises = srcUrls.map((srcUrl) => {
      const img = new Image();
      img.src = srcUrl;

      return new Promise<HTMLImageElement>((resolve) => {
        img.onload = () => resolve(img);
      });
    });

    Promise.all(imagesPromises).then((images) => {
      const ctx = canvas?.getContext("2d");

      render(() => {
        const img = images[opts.current.curFrame];
        const totalFrames = images.length;

        if (!img) return;

        const style = {
          fps: 30,
          loop: true,
          ...rest,
        } as const;

        ctx?.drawImage(
          img,
          0,
          0,
          img.width,
          img.height,
          style.renderLeft,
          style.renderTop,
          style.renderWidth,
          style.renderHeight
        );

        if (opts.current.keepGoing) {
          opts.current.tickCounter += 1;
          if (shouldIncrementToNextFrame(opts.current.tickCounter, style.fps))
            opts.current.curFrame += 1;
        }

        if (opts.current.curFrame === totalFrames) {
          if (opts.current.keepGoing) style.onLoopEnd?.();
          if (!style.loop) opts.current.keepGoing = false;
          if (opts.current.keepGoing)
            opts.current.curFrame = opts.current.curFrame % totalFrames;
          else opts.current.curFrame -= 1;
        }

        opts.current.tickCounter = opts.current.tickCounter % 60;
      });
    });
  }

  return null;
};
