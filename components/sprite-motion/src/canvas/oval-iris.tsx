import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

export const OvalIris = (props: {
  color?: string;
  durationInMs?: number;
  onEnd?: () => void;
}) => {
  const render = useCanvasRegistration();

  const { durationInMs = 3000, color = "black" } = props;

  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration("oval-iris", props);

  if (keyChanged && render && ctx) {
    const maxRadius = Math.max(ctx.canvas.width, ctx.canvas.height);

    let radius = 0;
    let done = false;

    render((_, timeElapsedBetweenFrames) => {
      ctx.beginPath();
      ctx.arc(
        ctx.canvas.width / 2,
        ctx.canvas.height / 2,
        radius,
        0,
        2 * Math.PI,
        false
      );
      ctx.fillStyle = color;
      ctx.fill();
      ctx.lineWidth = 1;
      ctx.strokeStyle = color;
      ctx.stroke();

      if (radius < maxRadius) {
        /**
         * Suppose durationInMs = 1000ms, and timeElapsedBetweenFrames (dt) = 10
         * ms. Then, if we increase the radius by maxRadius (maxR) * dt /
         * duration, we get
         *
         *        maxR * 10 /1000 = maxR * 0.01
         *
         * Then in 100 more frames at 10 ms, the radius will be maxR * 10 / 1000
         *
         * - 100 = maxR, so 👍
         *
         * All this means is that we have to increase the radius by maxRadius *
         * dt / durationInMs.
         */
        const radiusDelta =
          (maxRadius * timeElapsedBetweenFrames) / durationInMs;

        radius += radiusDelta;
      } else if (!done) {
        done = true;
        props.onEnd?.();
      }

      return key;
    });
  }

  return null;
};
