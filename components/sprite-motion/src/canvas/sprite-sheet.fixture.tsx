import {
  Canvas,
  Panel,
  StoryBoard,
  useDownloadsCtx,
  Video,
  World,
} from "../../index";
import React from "react";
import { SpriteSheet, SpriteSheetFromMany } from "./sprite-sheet";
import { Code } from "./code";
import { useInterval } from "../utils/timeouts";
import { useKeyListeners } from "../utils/keys";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
  sprite01: {
    type: "image",
    url: "./sprite/01.png",
  },
  sprite02: {
    type: "image",
    url: "./sprite/02.png",
  },
  sprite03: {
    type: "image",
    url: "./sprite/03.png",
  },
  sprite04: {
    type: "image",
    url: "./sprite/04.png",
  },
} as const;

const FirstPanel = () => {
  const downloads = useDownloadsCtx<typeof resources>();

  return (
    <SpriteSheet
      src={downloads.downloads.ninja}
      frameWidth={28}
      frameHeight={16}
      renderLeft={0}
      renderTop={0}
      renderWidth={28 * 4}
      renderHeight={16 * 4}
      fps={12}
    />
  );
};

export const SpriteSheetExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <FirstPanel />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const SpriteUnderCodeExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <FirstPanel />
          <Code left={10} top={10} speed={100} style={{ fillStyle: "#000" }}>
            Hello world
          </Code>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const CodeUnderSpriteExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <Code left={10} top={10} speed={100} style={{ fillStyle: "#000" }}>
            Hello world
          </Code>
          <FirstPanel />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const OnEnd = () => {
  const [len, setLen] = React.useState(0);

  const downloads = useDownloadsCtx<typeof resources>();

  return (
    <>
      <SpriteSheet
        src={downloads.downloads.ninja}
        frameWidth={28}
        frameHeight={16}
        renderLeft={0}
        renderTop={0}
        renderWidth={28 * 4}
        renderHeight={16 * 4}
        fps={12}
        loop={false}
        onLoopEnd={() => {
          setLen((l) => l + 1);
        }}
      />
      <div>{len}</div>
    </>
  );
};

export const OnLoopEndExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <OnEnd />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const MovingInner = () => {
  const [x, setX] = React.useState(0);
  const [y, setY] = React.useState(0);

  const downloads = useDownloadsCtx<typeof resources>();

  useInterval(() => {
    if (x <= 100) {
      setX((x) => x + 1);
      setY((y) => y + 1);
    }
  }, 0);

  // if (x === 101) return null;

  return (
    <SpriteSheet
      src={downloads.downloads.ninja}
      frameWidth={28}
      frameHeight={16}
      renderLeft={x}
      renderTop={y}
      renderWidth={28 * 4}
      renderHeight={16 * 4}
      fps={12}
    />
  );
};

export const MovingExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <MovingInner />
          <Code left={10} top={10} speed={10} style={{ fillStyle: "#000" }}>
            Hello world
          </Code>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const MyVid = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return <Video src={downloads.farting} />;
};

export const MovingExampleWithVideo = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <MyVid />
          <MovingInner />
          <Code left={50} top={50} speed={10} style={{ fillStyle: "#000" }}>
            Hello world
          </Code>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const KeyboardControlInner = () => {
  const [x, setX] = React.useState(0);
  const [y, setY] = React.useState(0);

  const downloads = useDownloadsCtx<typeof resources>();

  useKeyListeners({
    onKeyDown: (key) => {
      if (key === "ArrowLeft") setX((x) => x - 5);
      if (key === "ArrowRight") setX((x) => x + 5);
      if (key === "ArrowUp") setY((y) => y - 5);
      if (key === "ArrowDown") setY((y) => y + 5);
    },
  });

  return (
    <SpriteSheet
      src={downloads.downloads.ninja}
      frameWidth={28}
      frameHeight={16}
      renderLeft={x}
      renderTop={y}
      renderWidth={28 * 4}
      renderHeight={16 * 4}
      fps={12}
    />
  );
};

export const KeyboardControlExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas width={250} height={250}>
          <KeyboardControlInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const SpriteSheetFromManyInner = () => {
  const downloads = useDownloadsCtx<typeof resources>();

  return (
    <SpriteSheetFromMany
      src={[
        downloads.downloads.sprite01,
        downloads.downloads.sprite02,
        downloads.downloads.sprite03,
        downloads.downloads.sprite04,
      ]}
      renderLeft={0}
      renderTop={0}
      renderWidth={500}
      renderHeight={500}
      fps={6}
    />
  );
};

export const SpriteSheetFromManyExample = () => (
  <World
    width={500}
    height={500}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard disableManualNavigation>
      <Panel>
        <Canvas>
          <SpriteSheetFromManyInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
