import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

const getIndex = (x: number, y: number, width: number) => {
  return (y * width + x) * 4; // 4 components per pixel (RGBA)
};

const getIntensity = (
  x: number,
  y: number,
  width: number,
  data: ImageData["data"]
) => {
  const index = getIndex(x, y, width);
  // Grayscale conversion: average of RGB components
  return (
    ((data[index] ?? 0) + (data[index + 1] ?? 0) + (data[index + 2] ?? 0)) / 3
  );
};

const getIntensity2 = (index: number, data: ImageData["data"]) => {
  // Grayscale conversion: average of RGB components
  return (
    ((data[index] ?? 0) + (data[index + 1] ?? 0) + (data[index + 2] ?? 0)) / 3
  );
};

// const setColor = (
//   pos: [number, number],
//   rgba: [number, number, number, number],
//   width: number,
//   data: ImageData["data"]
// ) => {
//   const [x, y] = pos;
//   const [r, g, b, a] = rgba;
//   const index = getIndex(x, y, width);
//   data[index] = r;
//   data[index + 1] = g;
//   data[index + 2] = b;
//   data[index + 3] = a;
// };

const setColor = (
  x: number,
  y: number,
  r: number,
  g: number,
  b: number,
  a: number,
  width: number,
  data: ImageData["data"]
) => {
  const index = getIndex(x, y, width);
  data[index] = r;
  data[index + 1] = g;
  data[index + 2] = b;
  data[index + 3] = a;
};

const setColor2 = (
  index: number,
  intensity: number,
  alpha: number,
  data: ImageData["data"]
) => {
  data[index] = intensity;
  data[index + 1] = intensity;
  data[index + 2] = intensity;
  data[index + 3] = alpha;
};

// Atkinson matrix: [0, 1, 1/8], [1, 0, 1/8], [1, 1, 1/8], [2, 0, 1/8], [0, 2, 1/8], [1, 2, 1/8]
const errorWeights = [
  [0, 1, 1 / 8],
  [1, 0, 1 / 8],
  [1, 1, 1 / 8],
  [2, 0, 1 / 8],
  [0, 2, 1 / 8],
  [1, 2, 1 / 8],
] as const;

function atkinsonDithering2(imageData: ImageData, threshold: number) {
  const width = imageData.width;
  const height = imageData.height;
  const data = imageData.data;

  // Helper function to set pixel color at (x, y)

  // Loop through pixels
  for (let index = 0; index < data.length; index += 4) {
    // Get grayscale intensity of current pixel
    const intensity = getIntensity2(index, data);
    // Quantize intensity using threshold
    const outputIntensity = intensity < threshold ? 0 : 255;
    // Set the quantized intensity to the output image
    setColor2(index, outputIntensity, 255, data);
    // Calculate error
    const error = intensity - outputIntensity;

    errorWeights.forEach(([dx, dy, weight]) => {
      const y = Math.floor(index / (width * 4));
      const x = Math.floor((index % (width * 4)) / 4);
      const nx = x + dx;
      const ny = y + dy;
      if (nx >= 0 && nx < width && ny >= 0 && ny < height) {
        const currentIntensity = getIntensity(nx, ny, width, data);
        const newIntensity = currentIntensity + error * weight;
        setColor(
          nx,
          ny,
          newIntensity,
          newIntensity,
          newIntensity,
          255,
          width,
          data
        );
      }
    });
  }

  return imageData;
}

export const OneBit = (props: { threshold: number; disabled?: boolean }) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration("one-bit", props);

  if (keyChanged && render && ctx) {
    render(() => {
      if (props.disabled) return key;

      const imageData = ctx.getImageData(
        0,
        0,
        ctx.canvas.width,
        ctx.canvas.height
      );

      const newImg = atkinsonDithering2(imageData, props.threshold);
      ctx.putImageData(newImg, 0, 0);

      return key;
    });
  }

  return null;
};

function simpleDithering(imageData: ImageData, threshold: number) {
  const data = imageData.data;

  // Loop through pixels
  for (let index = 0; index < data.length; index += 4) {
    // Get grayscale intensity of current pixel
    const intensity = getIntensity2(index, data);
    // Quantize intensity using threshold
    const outputIntensity = intensity < threshold ? 0 : 255;
    // Set the quantized intensity to the output image
    setColor2(index, outputIntensity, 255, data);
  }

  return imageData;
}

export const OneBitSimple = (props: {
  threshold: number;
  disabled?: boolean;
}) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`one-bit-simple`, props);

  if (keyChanged && render && ctx) {
    render(() => {
      if (props.disabled) return key;

      const imageData = ctx.getImageData(
        0,
        0,
        ctx.canvas.width,
        ctx.canvas.height
      );

      const newImg = simpleDithering(imageData, props.threshold);
      ctx.putImageData(newImg, 0, 0);

      return key;
    });
  }

  return null;
};
