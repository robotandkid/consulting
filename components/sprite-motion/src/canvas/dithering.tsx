import { useCanvasRegistration, useKeyRegistration } from "./canvas";
import { usePanelCanvasCtx2D } from "../storyboard";

type RGB = [number, number, number];
type Palette = [RGB, ...RGB[]];

const sq = (x: number) => x * x;

const findClosestPaletteColor = (
  palette: Palette,
  r: number,
  g: number,
  b: number
) => {
  let closestColor = palette[0];
  let closestDistance = Infinity;

  palette.forEach((color) => {
    const distance = Math.sqrt(
      sq(color[0] - r) + sq(color[1] - g) + sq(color[2] - b)
    );
    if (distance < closestDistance) {
      closestDistance = distance;
      closestColor = color;
    }
  });

  return closestColor;
};

const floydSteinbergDithering = (imageData: ImageData, palette: Palette) => {
  const width = imageData.width;
  const height = imageData.height;
  const data = imageData.data;

  // Dithering process
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      const index = (y * width + x) * 4;
      const oldR = data[index] ?? 0;
      const oldG = data[index + 1] ?? 0;
      const oldB = data[index + 2] ?? 0;

      const [newR, newG, newB] = findClosestPaletteColor(
        palette,
        oldR,
        oldG,
        oldB
      );

      data[index] = newR;
      data[index + 1] = newG;
      data[index + 2] = newB;

      const errorR = oldR - newR;
      const errorG = oldG - newG;
      const errorB = oldB - newB;

      const distributeError = (
        xOffset: number,
        yOffset: number,
        factor: number
      ) => {
        const x1 = x + xOffset;
        const y1 = y + yOffset;
        if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height) {
          const index1 = (y1 * width + x1) * 4;
          data[index1] += errorR * factor;
          data[index1 + 1] += errorG * factor;
          data[index1 + 2] += errorB * factor;
        }
      };

      distributeError(1, 0, 7 / 16);
      distributeError(-1, 1, 3 / 16);
      distributeError(0, 1, 5 / 16);
      distributeError(1, 1, 1 / 16);
    }
  }

  return imageData;
};

type HexColor = `#${string}`;

const hexToRgb = (color: HexColor): [number, number, number] => {
  let hex = color;
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  }) as HexColor;

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  return result
    ? [
        parseInt(result[1] ?? "", 16),
        parseInt(result[2] ?? "", 16),
        parseInt(result[3] ?? "", 16),
      ]
    : [0, 0, 0];
};

export const CanvasDithering = (props: {
  disabled?: boolean;
  palette: Readonly<[HexColor, ...HexColor[]]>;
}) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`dithering`, props);

  if (keyChanged && render && ctx) {
    const paletteRgb = props.palette.map(hexToRgb) as Palette;

    render(() => {
      if (props.disabled) return key;

      const imageData = ctx.getImageData(
        0,
        0,
        ctx.canvas.width,
        ctx.canvas.height
      );

      const newImg = floydSteinbergDithering(imageData, paletteRgb);
      ctx.putImageData(newImg, 0, 0);

      return key;
    });
  }

  return null;
};
