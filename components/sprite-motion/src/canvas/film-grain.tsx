import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

// change these settings
const defaultPatternSize = 1024;
const defaultPatternRefreshInterval = 1;
const defaultPatternAlpha = 255; // int between 0 and 255,

// create a canvas which will be used as a pattern
const initGrain = (patternSize: number) => {
  const patternCanvas = document.createElement("canvas");
  patternCanvas.width = patternSize;
  patternCanvas.height = patternSize;
  const patternCtx = patternCanvas.getContext("2d");
  const patternData = patternCtx?.createImageData(patternSize, patternSize);

  return [patternCanvas, patternCtx, patternData] as const;
};

// put a random shade of gray into every pixel of the pattern
const update = (
  patternData: ImageData | undefined,
  patternCtx: CanvasRenderingContext2D | null,
  patternSize: number,
  patternAlpha: number
) => {
  if (!patternData || !patternCtx) return;

  const patternPixelDataLength = patternSize * patternSize * 4;

  for (let i = 0; i < patternPixelDataLength; i += 10) {
    const value = (Math.random() * 255) | 0;

    patternData.data[i] = value;
    patternData.data[i + 1] = value;
    patternData.data[i + 2] = value;
    patternData.data[i + 3] = patternAlpha;
  }

  patternCtx.putImageData(patternData, 0, 0);
};

export const FilmGrain = (props: {
  size?: number;
  refreshInterval?: number;
  /**
   * Number 0..255
   */
  alpha?: number;
}) => {
  const render = useCanvasRegistration();

  const {
    size = defaultPatternSize,
    refreshInterval = defaultPatternRefreshInterval,
    alpha = defaultPatternAlpha,
  } = props;

  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration("film-grain", props);

  if (keyChanged && render && ctx) {
    const [grainCanvas, grainCtx, grainImageData] = initGrain(size);

    let frame = 0;

    render(() => {
      if (frame++ % refreshInterval === 0) {
        update(grainImageData, grainCtx, size, alpha);

        ctx.fillStyle = ctx.createPattern(grainCanvas, "repeat")!;
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      }
      return key;
    });
  }

  return null;
};
