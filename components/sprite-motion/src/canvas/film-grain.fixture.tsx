import {
  Canvas,
  Panel,
  StoryBoard,
  useDownloadsCtx,
  Video,
  World,
} from "../../index";
import { FilmGrain } from "./film-grain";
import React from "react";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
} as const;

const MyVid = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return <Video src={downloads.farting} />;
};

export const FilmGrainExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <MyVid />
          <FilmGrain />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
