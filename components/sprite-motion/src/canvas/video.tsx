import { useRef } from "react";
import { ResourceVideo } from "../resources";
import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";
import React from "react";

const SimpleVideo = (props: {
  src: ResourceVideo;
  tintColor?: string;
  loop?: boolean;
  playbackRate?: number;
  onEnd?: () => void;
}) => {
  const render = useCanvasRegistration();

  const videoRef = React.useRef<HTMLVideoElement>(props.src.video);
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration("simple-video", props);

  if (keyChanged && ctx && render) {
    const video = videoRef.current;
    video.autoplay = true;
    video.loop = props.loop ?? true;
    video.muted = true;
    video.playbackRate = props.playbackRate ?? 1;

    video.onended = () => {
      props.onEnd?.();
    };

    video.onplay = () => {
      // this is a referance to the video
      // the video may not match the canvas size so find a scale to fit
      const scale = Math.min(
        ctx.canvas.width / video.videoWidth,
        ctx.canvas.height / video.videoHeight
      );

      render(() => {
        ctx.save();
        const vidH = video.videoHeight;
        const vidW = video.videoWidth;
        const top = ctx.canvas.height / 2 - (vidH / 2) * scale;
        const left = ctx.canvas.width / 2 - (vidW / 2) * scale;
        const { tintColor } = props;
        if (tintColor) {
          ctx.fillStyle = tintColor;
          ctx.globalCompositeOperation = "darken";
          ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        }
        ctx.drawImage(video, left, top, vidW * scale, vidH * scale);
        ctx.restore();

        return key;
      });
    };

    video.play();
  }

  return null;
};

interface AdvancedOpts {
  renderLeft: number;
  renderTop: number;
  renderWidth: number;
  renderHeight: number;
}

const renderVideo = (
  ctx: CanvasRenderingContext2D,
  video: HTMLVideoElement,
  props: AdvancedOpts
) => {
  const { renderTop: top, renderLeft: left, renderWidth, renderHeight } = props;

  ctx.drawImage(video, left, top, renderWidth, renderHeight);
};

const ComplexVideo = (
  props: {
    src: ResourceVideo;
    loop?: boolean;
    onEnd?: () => void;
    playbackRate?: number;
    muted?: boolean;
  } & AdvancedOpts
) => {
  const render = useCanvasRegistration();

  const videoRef = useRef<HTMLVideoElement>(props.src.video);
  const [keyChanged, key] = useKeyRegistration("complex-video", props);
  const ctx = usePanelCanvasCtx2D();

  videoRef.current.autoplay = true;
  videoRef.current.loop = props.loop ?? true;
  videoRef.current.muted = props.muted ?? true;
  videoRef.current.playbackRate = props.playbackRate ?? 1;

  if (keyChanged && ctx && render) {
    const video = videoRef.current;

    video.onended = () => {
      props.onEnd?.();
    };

    video.onplay = () => {
      render(() => {
        ctx.save();
        renderVideo(ctx, video, props);
        ctx.restore();
        return key;
      });
    };

    video.play();

    if (!video.paused) {
      render(() => {
        ctx.save();
        renderVideo(ctx, video, props);
        ctx.restore();
        return key;
      });
    }
  }

  return null;
};

type Never<T> = { [K in keyof T]?: never };

export const Video = (
  props: {
    src: ResourceVideo;
    loop?: boolean;
    onEnd?: () => void;
    tintColor?: string;
    playbackRate?: number;
    muted?: boolean;
  } & (AdvancedOpts | Never<AdvancedOpts>)
) => {
  if (props.renderTop === undefined) return <SimpleVideo {...props} />;
  return <ComplexVideo {...props} />;
};
