import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

export const CanvasBlur = (props: {
  size: number;
  duration: number;
  direction: "in" | "out";
}) => {
  const [keyChanged, key] = useKeyRegistration("blur", props);
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && ctx && render) {
    if (props.direction === "out") {
      let time = 0;
      let size = props.size;

      render((_, timeElapsedBetweenFrames) => {
        if (time + timeElapsedBetweenFrames >= props.duration) return key;
        time += timeElapsedBetweenFrames;
        size -= (props.size * timeElapsedBetweenFrames) / props.duration;
        size = Math.max(0, size);
        ctx.filter = `blur(${size}px)`;
        return key;
      });
    } else {
      let time = 0;
      let size = 0;

      render((_, timeElapsedBetweenFrames) => {
        if (time + timeElapsedBetweenFrames >= props.duration) return key;
        time += timeElapsedBetweenFrames;
        size += (props.size * timeElapsedBetweenFrames) / props.duration;
        size = Math.min(props.size, size);
        ctx.filter = `blur(${size}px)`;
        return key;
      });
    }
  }

  return null;
};
