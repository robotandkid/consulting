import { useCanvasRegistration } from "./canvas";
import { usePanelCanvasCtx2D } from "../storyboard";
import React, { useRef } from "react";
import { makeSimpleContext } from "../utils/context";

interface VideoFns {
  start: () => void;
  stop: () => void;
}

const [
  CanvasSaveVideoProvider,
  useCanvasSaveVideoCtx,
  useUpdateCanvasSaveVideoCtx,
] = makeSimpleContext<VideoFns>("CanvasVideo");

export { CanvasSaveVideoProvider, useCanvasSaveVideoCtx };

const InternalRecordCanvas = (props: {
  autoStart?: boolean;
  frameRate?: number;
}) => {
  const ctx = usePanelCanvasCtx2D();
  const mediaRecorder = useRef<MediaRecorder>();
  const update = useUpdateCanvasSaveVideoCtx();
  const render = useCanvasRegistration();

  if (!mediaRecorder.current && ctx && render) {
    const videoStream = ctx.canvas.captureStream(props.frameRate ?? 30);

    mediaRecorder.current = new MediaRecorder(videoStream);
    const chunks: BlobEvent["data"][] = [];

    mediaRecorder.current.ondataavailable = (e) => {
      chunks.push(e.data);
    };

    mediaRecorder.current.onstop = () => {
      if (chunks.length === 0) return;
      const blob = new Blob(chunks, { type: "video/webm" });
      const videoURL = URL.createObjectURL(blob);
      window.open(videoURL);
    };

    if (props.autoStart) mediaRecorder.current.start();

    // can't update state in the main render
    setTimeout(() => {
      if (mediaRecorder.current)
        update({
          start: () => mediaRecorder.current?.start(),
          stop: () => mediaRecorder.current?.stop(),
        });
    });
  }

  return null;
};

export const CanvasSaveVideo = React.memo(InternalRecordCanvas);
