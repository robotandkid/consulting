import React from "react";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";
import { usePanelCanvasCtx2D } from "../storyboard";
import { Code } from "./code";

type TextStyleNullable = Parameters<typeof Code>[0]["style"];

const drawBubble = (
  ctx: CanvasRenderingContext2D | undefined,
  pos: {
    left: number;
    top: number;
    width: number;
    height: number;
    radius: number;
    pLeft?: number;
    pTop?: number;
  },
  style: {
    strokeColor: `#${string}`;
    strokeWidth: number;
    fillColor: `#${string}`;
  }
) => {
  if (!ctx) return;

  const { left, top, width, height, radius, pLeft = left, pTop = top } = pos;
  const { strokeColor, strokeWidth, fillColor } = style;

  const r = left + width;
  const b = top + height;

  let con1 = Math.min(Math.max(top + radius, pTop - 10), b - radius - 20);
  let con2 = Math.min(Math.max(top + radius + 20, pTop + 10), b - radius);

  if (pTop < top || pTop > top + height) {
    con1 = Math.min(Math.max(left + radius, pLeft - 10), r - radius - 20);
    con2 = Math.min(Math.max(left + radius + 20, pLeft + 10), r - radius);
  }

  let dir: 2 | 3 | 0 | 1 | -1 | undefined;

  if (pTop < top) dir = 2;
  if (pTop > top) dir = 3;
  if (pLeft < left && pTop >= top && pTop <= b) dir = 0;
  if (pLeft > left && pTop >= top && pTop <= b) dir = 1;
  if (pLeft >= left && pLeft <= r && pTop >= top && pTop <= b) dir = -1;

  ctx.beginPath();
  ctx.strokeStyle = strokeColor;
  ctx.lineWidth = strokeWidth;
  ctx.fillStyle = fillColor;
  ctx.moveTo(left + radius, top);
  if (dir === 2) {
    ctx.lineTo(con1, top);
    ctx.lineTo(pLeft, pTop);
    ctx.lineTo(con2, top);
    ctx.lineTo(r - radius, top);
  } else ctx.lineTo(r - radius, top);
  ctx.quadraticCurveTo(r, top, r, top + radius);
  if (dir === 1) {
    ctx.lineTo(r, con1);
    ctx.lineTo(pLeft, pTop);
    ctx.lineTo(r, con2);
    ctx.lineTo(r, b - radius);
  } else ctx.lineTo(r, b - radius);
  ctx.quadraticCurveTo(r, b, r - radius, b);
  if (dir == 3) {
    ctx.lineTo(con2, b);
    ctx.lineTo(pLeft, pTop);
    ctx.lineTo(con1, b);
    ctx.lineTo(left + radius, b);
  } else ctx.lineTo(left + radius, b);
  ctx.quadraticCurveTo(left, b, left, b - radius);
  if (dir == 0) {
    ctx.lineTo(left, con2);
    ctx.lineTo(pLeft, pTop);
    ctx.lineTo(left, con1);
    ctx.lineTo(left, top + radius);
  } else ctx.lineTo(left, top + radius);
  ctx.quadraticCurveTo(left, top, left + radius, top);
  ctx.stroke();
  ctx.fill();
};

type PositionalOpts = Parameters<typeof drawBubble>[1];
type StyleOpts = Parameters<typeof drawBubble>[2];

export const CanvasSpeechBubble = (
  props: { pos: PositionalOpts } & { style?: Partial<StyleOpts> } & {
    text: {
      rx: number;
      ry: number;
      speed?: number;
    };
    textStyle?: TextStyleNullable;
    children?: string;
    disabled?: boolean;
    onDone?: () => void;
    autoTrim?: boolean;
  }
) => {
  const { autoTrim = true } = props;
  const render = useCanvasRegistration();
  const [keyChanged, key] = useKeyRegistration("speech-bubble", props);
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && render && ctx) {
    render(() => {
      if (props.disabled) return key;

      const opts: StyleOpts = {
        fillColor: "#ffffff",
        strokeColor: "#000000",
        strokeWidth: 2,
        ...props.style,
      };

      drawBubble(ctx, props.pos, opts);

      return key;
    });
  }

  return (
    <Code
      left={props.pos.left + props.text.rx}
      top={props.pos.top + props.text.ry}
      speed={props.text.speed}
      style={props.textStyle}
      onDone={props.onDone}
    >
      {autoTrim ? (props.children ?? "").trim() : props.children ?? ""}
    </Code>
  );
};
