import React from "react";
import { usePanelCanvas } from "../storyboard";
import { useCanvasRegistration } from "./canvas";

const InternalRecordCanvas = (props: {
  Button: React.ReactElement<{ onClick?: () => void }>;
}) => {
  const { Button } = props;
  const canvas = usePanelCanvas();
  const render = useCanvasRegistration();

  const b = React.cloneElement(Button, {
    onClick: () => {
      const url = canvas
        ?.toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
      window.open(url);
    },
  });

  if (!canvas || !render) return null;
  return <div style={{ zIndex: 999 }}>{b}</div>;
};

export const CanvasSaveImage = React.memo(InternalRecordCanvas);
