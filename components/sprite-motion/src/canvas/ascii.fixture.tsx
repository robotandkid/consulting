import {
  Canvas,
  Panel,
  StoryBoard,
  useDownloadsCtx,
  Video,
  World,
} from "../../index";
import { Ascii, AsciiFullColor } from "./ascii";
import { CanvasImage } from "./canvas-image";
import React from "react";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
} as const;

const Inner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    // <Video
    //   src={downloads.farting}
    //   renderTop={0}
    //   renderLeft={0}
    //   renderWidth={512}
    //   renderHeight={512}
    // />
    <CanvasImage
      src={downloads.dragon}
      renderTop={0}
      renderLeft={0}
      renderWidth={512}
      renderHeight={512}
    />
  );
};

export const AsciiExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={512} height={512}>
          <Inner />
          <Ascii
            resolutionY={65}
            resolutionX={120}
            style={{
              fontFamily: "monospace",
              fillStyle: "#80ff00",
              bgColor: "#000000",
            }}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const ColorFlipExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={512} height={512}>
          <Inner />
          <Ascii
            resolutionY={65}
            resolutionX={120}
            style={{
              fontFamily: "monospace",
              bgColor: "#000000",
              fillStyle: "#80ff00",
            }}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const FullAsciiExample = () => (
  <div style={{ display: "flex" }}>
    <World
      width={512}
      height={512}
      style={{ border: "1px solid black" }}
      resources={resources}
      loading={"loading"}
    >
      <StoryBoard>
        <Panel>
          <Canvas width={512} height={512}>
            <Inner />
          </Canvas>
        </Panel>
      </StoryBoard>
    </World>
    <World
      width={512}
      height={512}
      style={{ border: "1px solid black" }}
      resources={resources}
      loading={"loading"}
    >
      <StoryBoard>
        <Panel>
          <Canvas width={512} height={512}>
            <Inner />
            <AsciiFullColor
              resolutionY={47}
              resolutionX={80}
              style={{
                fontFamily: "monospace",
              }}
            />
          </Canvas>
        </Panel>
      </StoryBoard>
    </World>
  </div>
);

const InnerVideo = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Video
      src={downloads.farting}
      renderTop={0}
      renderLeft={0}
      renderWidth={512}
      renderHeight={512}
    />
  );
};

export const VideoExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={512} height={512}>
          <InnerVideo />
          <Ascii
            resolutionY={65}
            resolutionX={120}
            style={{
              fontFamily: "monospace",
              bgColor: "#80ff00",
              fillStyle: "#000000",
            }}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const FullVideoExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <InnerVideo />
          <AsciiFullColor
            resolutionY={65}
            resolutionX={120}
            style={{
              fontFamily: "monospace",
            }}
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const CustomRampExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <InnerVideo />
          <AsciiFullColor
            resolutionY={30}
            resolutionX={40}
            style={{
              fontFamily: "monospace",
            }}
            characterMap="XOXOXO? "
          />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
