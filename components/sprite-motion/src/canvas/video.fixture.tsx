import {
  Canvas,
  Panel,
  StoryBoard,
  useDownloadsCtx,
  Video,
  World,
} from "../../index";
import React, { useEffect } from "react";
import { useInterpolate } from "../utils/timeouts";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
  videoTall: {
    type: "video",
    url: "./video-tall.mp4",
  },
} as const;

const VideoInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return <Video src={downloads.farting} loop />;
};

export const SimpleVideoExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <VideoInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const MovingVideoInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [x, setX] = React.useState(0);

  useEffect(() => {
    const update = setInterval(() => setX((x) => x + 1), 100);

    return () => {
      clearInterval(update);
    };
  }, []);

  return (
    <Video
      src={downloads.farting}
      loop
      renderTop={10 + x}
      renderLeft={20 + x}
      renderWidth={50}
      renderHeight={75}
    />
  );
};

export const MovingVideoExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <MovingVideoInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const AnotherMovingVideoInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const y = useInterpolate(-1500, -190, { delta: 10 });

  return (
    <Video
      src={downloads.videoTall}
      loop
      renderTop={y}
      renderLeft={0}
      renderWidth={512}
      renderHeight={2560}
    />
  );
};

export const AnotherMovingVideoExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <AnotherMovingVideoInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
