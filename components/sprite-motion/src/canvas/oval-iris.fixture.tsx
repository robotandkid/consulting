import React from "react";
import { useDownloadsCtx } from "../resources";
import { World } from "../sprite-motion";
import { Panel, StoryBoard } from "../storyboard";
import { Canvas } from "./canvas";
import { OvalIris } from "./oval-iris";
import { Video } from "./video";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
} as const;

const MyVideo = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return <Video src={downloads.farting} />;
};

const Iris = () => {
  const [x, setX] = React.useState(0);

  return (
    <>
      <OvalIris durationInMs={5_000} onEnd={() => setX((c) => c + 1)} />
      {x > 0 && (
        <div style={{ color: "red", position: "absolute", top: 5, zIndex: 2 }}>
          We&apos;re done: {x}
        </div>
      )}
    </>
  );
};

export const OvalIrisExample = () => (
  <World
    width={250}
    height={250}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={250} height={250}>
          <MyVideo />
          <Iris />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
