import {
  Canvas,
  Panel,
  StoryBoard,
  World,
  palette16shidoCyberNeon,
} from "../../index";
import React from "react";
import {
  CanvasDivJustifiedFull,
  CanvasDivAlignLeft,
  CanvasRawSpan,
  CanvasSpan,
} from "./canvas-div";
import { pickAccessibleColor } from "../utils/color";

export const RawSpanExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasRawSpan
            pos={{
              left: 10,
              top: 10,
            }}
          >
            Lorem ispum Hello world foo bar
          </CanvasRawSpan>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const quotes = [
  `Being in a situation that looks to be hopeless!`,
  `Knock Your Socks Off`,
  `I Smell a Rat`,
  `Jaws of life`,
  `Read 'Em and Weep`,
  `Keep Your Shirt On`,
  `No human being is illegal. That is a contradiction in terms. Human beings can be beautiful, or more beautiful, they can be fat or skinny, they can be right or wrong, but illegal? How can a human being be illegal?`,
] as const;

const palette = palette16shidoCyberNeon();

const randomPalette = () => {
  const color = palette[Math.floor(Math.random() * palette.length)]!;
  return [color, pickAccessibleColor(color, ["#000000", "#ffffff"])];
};

export const NeonExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasDivAlignLeft
            pos={{
              left: 10,
              top: 10,
              width: 500,
              height: 500,
            }}
            style={{
              lineHeightMultiple: 1,
            }}
          >
            {quotes.map((quote) => {
              const [bgColor, textColor] = randomPalette();
              return (
                <CanvasSpan
                  key={quote}
                  style={{ bgStyle: bgColor, colorStyle: textColor }}
                >
                  {quote}
                </CanvasSpan>
              );
            })}
          </CanvasDivAlignLeft>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const JustifiedFullExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasDivJustifiedFull
            pos={{
              left: 10,
              top: 10,
              width: 500,
              height: 500,
            }}
            style={{
              lineHeightMultiple: 1,
            }}
          >
            {quotes.map((quote) => {
              const [bgColor, textColor] = randomPalette();
              return (
                <CanvasSpan
                  key={quote}
                  style={{ bgStyle: bgColor, colorStyle: textColor }}
                >
                  {quote.replace(/ /g, "")}
                </CanvasSpan>
              );
            })}
          </CanvasDivJustifiedFull>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const JustifiedFullWithDivRotationExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasDivJustifiedFull
            pos={{
              left: 10,
              top: 10,
              width: 500,
              height: 500,
            }}
            style={{
              lineHeightMultiple: 1,
            }}
            transform={{
              rotate: 30,
            }}
          >
            {quotes.map((quote) => {
              const [bgColor, textColor] = randomPalette();
              return (
                <CanvasSpan
                  key={quote}
                  style={{ bgStyle: bgColor, colorStyle: textColor }}
                >
                  {quote.replace(/ /g, "")}
                </CanvasSpan>
              );
            })}
          </CanvasDivJustifiedFull>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const JustifiedFullWithMultipleExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasDivJustifiedFull
            pos={{
              left: 10,
              top: 10,
              width: 250,
              height: 500,
            }}
          >
            <CanvasSpan style={{ colorStyle: "#000" }}>
              However, when they liked, and left off sneezing by this time
            </CanvasSpan>
          </CanvasDivJustifiedFull>
          <CanvasDivJustifiedFull
            pos={{
              left: 250,
              top: 250,
              width: 250,
              height: 500,
            }}
          >
            <CanvasSpan style={{ colorStyle: "#000" }}>
              However, when they liked, and left off sneezing by this time
            </CanvasSpan>
          </CanvasDivJustifiedFull>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const AlignedLeftExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasDivAlignLeft
            pos={{
              left: 10,
              top: 10,
              width: 500,
              height: 500,
            }}
            style={{
              lineHeightMultiple: 1,
            }}
          >
            <CanvasSpan
              style={{ bgStyle: "#000", colorStyle: "#fff" }}
              transform={{ rotate: 3 }}
            >
              {quotes[0].replace(/ /g, "")}
            </CanvasSpan>
            <CanvasSpan
              style={{ bgStyle: "#000", colorStyle: "#fff" }}
              transform={{ rotate: -3 }}
            >
              {quotes[1].replace(/ /g, "")}
            </CanvasSpan>
          </CanvasDivAlignLeft>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

export const AlignedLeftWithDivRotationExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <CanvasDivAlignLeft
            pos={{
              left: 250,
              top: 250,
              width: 500,
              height: 500,
            }}
            style={{
              lineHeightMultiple: 1,
            }}
            transform={{
              rotate: 45,
            }}
          >
            <CanvasSpan style={{ bgStyle: "#000", colorStyle: "#fff" }}>
              Hello world
            </CanvasSpan>
          </CanvasDivAlignLeft>
          <CanvasDivAlignLeft
            pos={{
              left: 250,
              top: 250,
              width: 500,
              height: 500,
            }}
            style={{
              lineHeightMultiple: 1,
            }}
          >
            <CanvasSpan style={{ bgStyle: "#ff0000", colorStyle: "#fff" }}>
              Hello world
            </CanvasSpan>
          </CanvasDivAlignLeft>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
