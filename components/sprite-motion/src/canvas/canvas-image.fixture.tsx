import { Canvas, Panel, StoryBoard, useDownloadsCtx, World } from "../../index";
import React, { useEffect } from "react";
import { CanvasImage } from "./canvas-image";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  dragonSm: {
    type: "image",
    url: "./dragon-sm.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
} as const;

const Inner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <CanvasImage
      src={downloads.dragon}
      renderTop={0}
      renderLeft={0}
      renderWidth={512}
      renderHeight={512}
    />
  );
};

export const CanvasImageExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas width={512} height={512}>
          <Inner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const MovingInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  const [y, setY] = React.useState(0);

  useEffect(() => {
    const interval = setInterval(() => setY((c) => c + 1), 100);
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <CanvasImage
      src={downloads.dragon}
      renderTop={y}
      renderLeft={0}
      renderWidth={512}
      renderHeight={512}
    />
  );
};

export const MovingImageExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <MovingInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);

const TileInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <CanvasImage
      src={downloads.dragon}
      renderTop={0}
      renderLeft={0}
      renderWidth={512}
      renderHeight={512}
      tile="repeat"
      tileWidth={50}
      tileHeight={50}
    />
  );
};

export const TileExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <TileInner />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
