import { useRef } from "react";
import { ResourceImg } from "../resources";
import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

interface RenderOpts {
  renderLeft: number;
  renderTop: number;
  renderWidth: number;
  renderHeight: number;
  tile?: undefined;
}

interface TileOpts extends Omit<RenderOpts, "tile"> {
  tile: "repeat";
  tileWidth: number;
  tileHeight: number;
}

type AdvancedOpts = RenderOpts | TileOpts;

const renderImg = (
  ctx: CanvasRenderingContext2D,
  img: HTMLImageElement,
  props: AdvancedOpts
) => {
  const {
    renderTop: top,
    renderLeft: left,
    renderWidth,
    renderHeight,
    tile,
  } = props;

  if (tile) {
    const patternCanvas = document.createElement("canvas");
    patternCanvas.width = props.tileWidth;
    patternCanvas.height = props.tileHeight;

    const patternCtx = patternCanvas.getContext("2d");
    if (!patternCtx) return;
    patternCtx.drawImage(img, 0, 0, patternCanvas.width, patternCanvas.height);

    const pattern = ctx.createPattern(patternCanvas, props.tile);
    if (!pattern) return;
    ctx.fillStyle = pattern;
    ctx.fillRect(left, top, renderWidth, renderHeight);
  } else {
    ctx.drawImage(img, left, top, renderWidth, renderHeight);
  }
};

type Never<T> = { [K in keyof T]?: never };

export const CanvasImage = (
  props: {
    src: ResourceImg;
  } & (AdvancedOpts | Never<AdvancedOpts>)
) => {
  const render = useCanvasRegistration();

  const imgRef = useRef<HTMLImageElement>(document.createElement("img"));
  const imgLoaded = useRef(false);
  const [keyChanged, key] = useKeyRegistration(`canvas-image`, props);
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && ctx && render) {
    const image = imgRef.current;

    if (!image.src) {
      props.src.resource.then((theSrc) => {
        if (!theSrc) return;
        image.src = theSrc;
      });
    }

    image.onload = () => {
      imgLoaded.current = true;
    };

    render(() => {
      if (!imgLoaded.current) return key;
      ctx.save();
      {
        renderImg(ctx, image, {
          ...props,
          renderLeft: props.renderLeft ?? 0,
          renderTop: props.renderTop ?? 0,
          renderWidth: props.renderWidth ?? image.width,
          renderHeight: props.renderHeight ?? image.height,
        });
      }
      ctx.restore();

      return key;
    });
  }

  return null;
};
