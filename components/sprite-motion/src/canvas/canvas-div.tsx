import React, { useCallback, useRef } from "react";
import { usePanelCanvasCtx2D } from "../storyboard";
import { makeReadOnlyContext } from "../utils/context";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

type TextFont =
  | `${number}px ${string}`
  | `${"bold italic" | "bold" | "italic"} ${number}px ${string}`;

interface SpanStyle {
  font?: TextFont;
  bgStyle?: `#${string}`;
  colorStyle?: `#${string}`;
  textBaseline?: CanvasRenderingContext2D["textBaseline"];
  marginRight?: number;
}

type RequiredSpanStyle = Omit<Required<SpanStyle>, "bgStyle"> &
  Pick<SpanStyle, "bgStyle">;

interface SpanPos {
  left: number;
  top: number;
  width?: number;
  height?: number;
}

const getFontDimensions = (
  font: TextFont,
  text: string,
  ctx: CanvasRenderingContext2D
) => {
  ctx.font = font;
  const dims = ctx.measureText(text);
  return {
    width: dims.width,
    height:
      1.5 * Math.abs(dims.fontBoundingBoxAscent - dims.fontBoundingBoxDescent),
  };
};

interface TransformOpts {
  rotate?: number;
}

type RequiredSpanTransform = Required<TransformOpts> & {
  center: Readonly<[number, number]>;
};

const drawSpan = (
  ctx: CanvasRenderingContext2D | undefined,
  opts: {
    style: RequiredSpanStyle;
    pos: SpanPos;
    transform?: RequiredSpanTransform;
  },
  text: string
) => {
  if (!ctx) return;

  const { left, top, width, height } = opts.pos;
  const { bgStyle, colorStyle, textBaseline, font } = opts.style;

  let actualWidth = width ?? 0;
  let actualHeight = height ?? 0;

  if (width === undefined || height == undefined) {
    const textDims = getFontDimensions(font, text, ctx);
    actualWidth = width ?? textDims.width;
    actualHeight = height ?? textDims.height;
  }

  ctx.save();
  {
    if (opts.transform) {
      const { rotate, center } = opts.transform;
      ctx.translate(left + center[0], top + center[1]);
      ctx.rotate((rotate * Math.PI) / 180);
      ctx.translate(-left - center[0], -top - center[1]);
    }

    if (bgStyle) {
      ctx.fillStyle = bgStyle;
      ctx.fillRect(left, top, actualWidth, actualHeight);
    }

    ctx.font = font;
    ctx.textBaseline = textBaseline;
    ctx.fillStyle = colorStyle;

    ctx.fillText(text, left, top);
  }
  ctx.restore();
};

const getSpanStyles = (style?: SpanStyle): RequiredSpanStyle => ({
  colorStyle: "#fff",
  font: "24px mrpixel",
  textBaseline: "top",
  marginRight: 0,
  ...style,
});

interface DivPos {
  left: number;
  top: number;
  width: number;
  height: number;
}

interface DivStyle {
  lineHeightMultiple?: number;
}

interface SpanOpts {
  text: string;
  style?: SpanStyle;
  transform?: TransformOpts;
}

const [RegisterProvider, useRegisterCtx] = makeReadOnlyContext<
  (key: string, opts: SpanOpts) => void
>("CanvasFullJustifiedSpan");

const findCenterOfObjects = (
  objects: Array<{ left: number; top: number; width: number; height: number }>
) => {
  let totalX = 0;
  let totalY = 0;

  objects.forEach((obj) => {
    const centerX = obj.left + obj.width / 2;
    const centerY = obj.top + obj.height / 2;

    totalX += centerX;
    totalY += centerY;
  });

  // Average the center points
  const averageCenterX = totalX / objects.length;
  const averageCenterY = totalY / objects.length;

  return { x: averageCenterX, y: averageCenterY };
};

export const CanvasDivJustifiedFull = (props: {
  pos: DivPos;
  style?: DivStyle;
  transform?: TransformOpts;
  children: React.ReactNode;
}) => {
  const rawSpans = useRef<Array<{ key: string; span: SpanOpts }>>([]);

  const register = useCallback((key: string, span: SpanOpts) => {
    const prev = rawSpans.current.find((line) => line.key === key);
    if (prev) {
      prev.span = span;
    } else {
      rawSpans.current.push({ key, span });
    }
  }, []);

  const render = useCanvasRegistration();

  const [keyChanged, key] = useKeyRegistration("canvas-div-justified-full", {
    pos: props.pos,
    style: props.style,
  });

  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && render && ctx) {
    render(() => {
      const divStyle: Required<DivStyle> = {
        lineHeightMultiple: 1,
        ...props.style,
      };

      const characters = rawSpans.current.flatMap(({ span }) => {
        const allChars = span.text.split("");
        const style = getSpanStyles(span.style);

        return allChars.map((character, index) => {
          return {
            character,
            style,
            ...getFontDimensions(style.font, character, ctx),
            type:
              index === 0
                ? "start"
                : index === allChars.length - 1
                ? "end"
                : "inner",
          };
        });
      });

      type Character = typeof characters[0];

      const charactersWithPos: Array<
        Character & {
          top: number;
          left: number;
        }
      > = [];

      for (let i = 0; i < characters.length; i++) {
        const char = characters[i];
        if (!char) continue;

        const prev = charactersWithPos[charactersWithPos.length - 1] ?? {
          top: props.pos.top,
          left: props.pos.left,
          width: 0,
          height: 0,
        };

        let desiredTop = prev.top;
        let desiredLeft = prev.left + prev.width;
        const isOverflow =
          desiredLeft + char.width > props.pos.left + props.pos.width;

        if (isOverflow) {
          desiredTop += divStyle.lineHeightMultiple * char.height;
          desiredLeft = props.pos.left;

          charactersWithPos.push({
            ...char,
            top: desiredTop,
            left: desiredLeft,
          });
        } else {
          charactersWithPos.push({
            ...char,
            top: desiredTop,
            left: desiredLeft,
          });
        }
      }

      ctx.save();
      {
        const { transform: { rotate } = {} } = props;
        if (rotate) {
          const center = findCenterOfObjects(charactersWithPos);
          ctx.translate(center.x, center.y);
          ctx.rotate((rotate * Math.PI) / 180);
          ctx.translate(-center.x, -center.y);
        }
        charactersWithPos.forEach((line) => {
          drawSpan(ctx, { pos: line, style: line.style }, line.character);
        });
      }
      ctx.restore();

      return key;
    });
  }

  return <RegisterProvider value={register}>{props.children}</RegisterProvider>;
};

export const CanvasDivAlignLeft = (props: {
  pos: DivPos;
  style?: DivStyle;
  transform?: TransformOpts;
  children: React.ReactNode;
}) => {
  const rawSpans = useRef<Array<{ key: string; span: SpanOpts }>>([]);

  const register = useCallback((key: string, span: SpanOpts) => {
    const prev = rawSpans.current.find((line) => line.key === key);
    if (prev) {
      prev.span = span;
    } else {
      rawSpans.current.push({ key, span });
    }
  }, []);

  const render = useCanvasRegistration();

  const [keyChanged, key] = useKeyRegistration("canvas-div-justified-left", {
    pos: props.pos,
    style: props.style,
  });

  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && render && ctx) {
    render(() => {
      const divStyle: Required<DivStyle> = {
        lineHeightMultiple: 1,
        ...props.style,
      };

      const spans = rawSpans.current.flatMap(({ span }) => {
        const style = getSpanStyles(span.style);
        const dims = getFontDimensions(style.font, span.text, ctx);
        const center = [dims.width / 2, dims.height / 2] as const;

        return {
          text: span.text,
          style,
          ...dims,
          transform: span.transform?.rotate
            ? {
                rotate: span.transform.rotate,
                center: [center[0], center[1]] as const,
              }
            : undefined,
        };
      });

      type ThisSpan = typeof spans[0];

      const spansWithPos: Array<
        ThisSpan & {
          top: number;
          left: number;
        }
      > = [];

      for (let i = 0; i < spans.length; i++) {
        const char = spans[i];
        if (!char) continue;

        const prev = spansWithPos[spansWithPos.length - 1] ?? {
          top: props.pos.top,
          left: props.pos.left,
          width: 0,
          height: 0,
          style: {
            marginRight: 0,
          },
        };

        let desiredTop = prev.top;
        let desiredLeft = prev.left + prev.width + prev.style.marginRight;
        const isOverflow =
          desiredLeft + char.width > props.pos.left + props.pos.width;

        if (isOverflow) {
          desiredTop += divStyle.lineHeightMultiple * char.height;
          desiredLeft = props.pos.left;

          spansWithPos.push({
            ...char,
            top: desiredTop,
            left: desiredLeft,
          });
        } else {
          spansWithPos.push({
            ...char,
            top: desiredTop,
            left: desiredLeft,
          });
        }
      }

      ctx.save();
      {
        const { transform: { rotate } = {} } = props;
        if (rotate) {
          const center = findCenterOfObjects(spansWithPos);
          ctx.translate(center.x, center.y);
          ctx.rotate((rotate * Math.PI) / 180);
          ctx.translate(-center.x, -center.y);
        }

        spansWithPos.forEach((line) => {
          drawSpan(
            ctx,
            { pos: line, style: line.style, transform: line.transform },
            line.text
          );
        });
      }
      ctx.restore();

      return key;
    });
  }

  return <RegisterProvider value={register}>{props.children}</RegisterProvider>;
};

interface TransformOpts {
  rotate?: number;
}

export const CanvasSpan = (props: {
  children: string;
  style?: SpanStyle;
  transform?: TransformOpts;
}) => {
  const register = useRegisterCtx();

  const [keyChanged, key] = useKeyRegistration("canvas-span", props);

  if (keyChanged && register) {
    register(key, {
      text: props.children,
      style: props.style,
      transform: props.transform,
    });
  }

  return null;
};

/** @deprecated Do not use. Exported only for testing */
export const CanvasRawSpan = (props: {
  style?: SpanStyle;
  pos: SpanPos;
  children: string;
}) => {
  const render = useCanvasRegistration();
  const [keyChanged, key] = useKeyRegistration("canvas-span", props);
  const ctx = usePanelCanvasCtx2D();

  if (keyChanged && render && ctx) {
    render(() => {
      const style = getSpanStyles(props.style);
      drawSpan(ctx, { style, pos: props.pos }, props.children);
      return key;
    });
  }

  return null;
};
