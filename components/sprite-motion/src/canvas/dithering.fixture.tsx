import {
  Canvas,
  Panel,
  StoryBoard,
  useDownloadsCtx,
  Video,
  World,
} from "../../index";
import { CanvasImage } from "./canvas-image";
import React from "react";
import { Story } from "@ladle/react";
import { CanvasDithering } from "./dithering";
import { cybergumsPalette, oil6Palette } from "../utils/palettes";

const resources = {
  sound1: {
    type: "sound",
    url: "./mom.mp3",
  },
  dragon: {
    type: "image",
    url: "./storyboard-02.png",
  },
  ninja: {
    type: "image",
    url: "./ninja-running-left.png",
  },
  sprite: {
    type: "image",
    url: "./sprite-sheet.png",
  },
  farting: {
    type: "video",
    url: "./farting-01-pick.mp4",
  },
} as const;

const Inner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    // <Video
    //   src={downloads.farting}
    //   renderTop={0}
    //   renderLeft={0}
    //   renderWidth={512}
    //   renderHeight={512}
    // />
    <CanvasImage
      src={downloads.dragon}
      renderTop={0}
      renderLeft={0}
      renderWidth={512}
      renderHeight={512}
    />
  );
};

export const ImageExample: Story<{ threshold: string[] }> = () => (
  <div style={{ display: "flex" }}>
    <World
      width={512}
      height={512}
      style={{ border: "1px solid black" }}
      resources={resources}
      loading={"loading"}
    >
      <StoryBoard>
        <Panel>
          <Canvas>
            <Inner />
          </Canvas>
        </Panel>
      </StoryBoard>
    </World>
    <World
      width={512}
      height={512}
      style={{ border: "1px solid black" }}
      resources={resources}
      loading={"loading"}
    >
      <StoryBoard>
        <Panel>
          <Canvas>
            <Inner />
            <CanvasDithering palette={cybergumsPalette()} />
          </Canvas>
        </Panel>
      </StoryBoard>
    </World>
  </div>
);

// ImageExample.argTypes = {
//   threshold: {
//     control: {
//       type: "range",
//       min: 0,
//       max: 255,
//       step: 1,
//     },
//     defaultValue: 50,
//   },
// };

const InnerVideo = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();

  return (
    <Video
      src={downloads.farting}
      renderTop={0}
      renderLeft={0}
      renderWidth={512}
      renderHeight={512}
    />
  );
};

export const VideoExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={resources}
    loading={"loading"}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <InnerVideo />
          <CanvasDithering palette={oil6Palette()} />
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
