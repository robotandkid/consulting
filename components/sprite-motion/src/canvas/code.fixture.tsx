import { Canvas, Code, Panel, StoryBoard, World } from "../../index";
import React from "react";

export const BasicCodeExample = () => (
  <World
    width={512}
    height={512}
    style={{ border: "1px solid black" }}
    resources={undefined}
  >
    <StoryBoard>
      <Panel>
        <Canvas>
          <Code
            left={10}
            top={10}
            style={{
              fillStyle: "#000",
              glitch: false,
              font: "24px mrpixel",
            }}
          >
            Lorem ipsum foo bar
          </Code>
        </Canvas>
      </Panel>
    </StoryBoard>
  </World>
);
