import { usePanelCanvasCtx2D } from "../storyboard";
import { useCanvasRegistration, useKeyRegistration } from "./canvas";

const toGrayScale = (r: number, g: number, b: number) =>
  0.21 * r + 0.72 * g + 0.07 * b;

const convertToGrayScales = (
  canvas: HTMLCanvasElement,
  resolutionX: number,
  resolutionY: number
): [ImageData["data"], number[]] => {
  /**
   * First we draw the existing canvas on a smaller one of potentially different
   * aspect ratio.
   */
  const fakeCanvas = document.createElement("canvas");
  fakeCanvas.width = resolutionX;
  fakeCanvas.height = resolutionY;
  const fakeContext = fakeCanvas.getContext("2d");

  if (!fakeContext) return [new Uint8ClampedArray(), []];

  fakeContext.drawImage(canvas, 0, 0, resolutionX, resolutionY);

  /** Then we convert to gray scale */
  const origImageData = fakeContext.getImageData(
    0,
    0,
    resolutionX,
    resolutionY
  );

  const imageData = fakeContext.getImageData(0, 0, resolutionX, resolutionY);
  const grayScales = [];

  for (let i = 0; i < imageData.data.length; i += 4) {
    const r = imageData.data[i];
    const g = imageData.data[i + 1];
    const b = imageData.data[i + 2];

    const grayScale = toGrayScale(r!, g!, b!);

    imageData.data[i] =
      imageData.data[i + 1] =
      imageData.data[i + 2] =
        grayScale;

    grayScales.push(grayScale);
  }

  fakeContext.putImageData(imageData, 0, 0);

  return [origImageData.data, grayScales];
};

//const grayRamp =
//  "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
const getGrayRamp = (full: boolean, customFullRamp?: string) => {
  const fullRamp =
    customFullRamp ??
    "#&M8BW0XO%QDHAVYNERU468GQP3572CFJKLTZ1I!;.,:'^~-_|/*()[]{}<>+=? ";

  const ramp = full ? fullRamp : "@W#8&o:*. ";
  const reverse = ramp.split("").reverse().join("");
  return [ramp, reverse] as const;
};

const grayRampThick = "W";

const getCharacterForGrayScale = (grayScale: number, ramp: string) => {
  return ramp[Math.ceil(((ramp.length - 1) * grayScale) / 255)];
};

const drawAscii = (grayScales: number[], width: number, ramp: string) => {
  const ascii = grayScales.reduce((asciiImage, grayScale, index) => {
    let nextChars = getCharacterForGrayScale(grayScale, ramp);

    if ((index + 1) % width === 0) {
      nextChars += "\n";
    }

    return asciiImage + nextChars;
  }, "");

  return ascii;
};

const hexToRgb = (hexColor: `#${string}`) => {
  // Convert hexadecimal color to RGB components
  const hex = hexColor.replace(/^#/, "");
  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);
  return [r, g, b] as const;
};

const rgbToHex = (
  ...[r, g, b]: Readonly<
    [number | undefined, number | undefined, number | undefined]
  >
) => {
  // Convert hexadecimal color to RGB components
  const hexR = r?.toString(16).padStart(2, "0") ?? "00";
  const hexG = g?.toString(16).padStart(2, "0") ?? "00";
  const hexB = b?.toString(16).padStart(2, "0") ?? "00";

  return `#${hexR}${hexG}${hexB}` as const;
};

type RGB = [number, number, number];

const luminance = (rgbColor: Readonly<RGB>) => {
  // Calculate luminance of an RGB color
  const [r, g, b] = rgbColor;
  return 0.2126 * r + 0.7152 * g + 0.0722 * b;
};

/** Returns true when color1 is darker than color2 */
const isDarker = (color1: `#${string}`, color2: `#${string}`) => {
  // Compare luminance of two colors
  const rgbColor1 = hexToRgb(color1);
  const rgbColor2 = hexToRgb(color2);
  const luminance1 = luminance(rgbColor1);
  const luminance2 = luminance(rgbColor2);
  return luminance1 < luminance2;
};

const getFontSize = (
  text: string,
  fontFace: string,
  ctx: CanvasRenderingContext2D
) => {
  // start with a large font size
  let fontSize = 300;

  // lower the font size until the text fits the canvas. Doh! But of course 🤷
  do {
    fontSize -= 1;
    ctx.font = `bold ${fontSize}px ${fontFace}
    `;
  } while (ctx.measureText(text).width > ctx.canvas.width);

  ctx.font = `bold ${fontSize + 1}px ${fontFace}`;
  return fontSize + 1;
};

const getFontWidth = (
  fontSize: number,
  fontFace: string,
  ctx: CanvasRenderingContext2D
) => {
  ctx.font = `bold ${fontSize}px ${fontFace}`;
  return ctx.measureText(grayRampThick).width;
};

interface TextStyle {
  fontFamily?: string;
  fillStyle?: `#${string}`;
  bgColor?: `#${string}`;
}

interface FullColorStyle extends Pick<TextStyle, "fontFamily" | "bgColor"> {
  type: "full";
}

interface InvertedColorStyle extends Pick<TextStyle, "fontFamily"> {
  type: "invert";
}

interface NormalColorStyle extends TextStyle {
  type: "normal";
}

const convert = (opts: {
  canvas: HTMLCanvasElement;
  resolutionX: number;
  resolutionY: number;
  style: Required<FullColorStyle> | Required<NormalColorStyle>;
  ramp?: string;
}) => {
  const { canvas, resolutionY, resolutionX, style, ramp: customRamp } = opts;

  const [imageData, grayScales] = convertToGrayScales(
    canvas,
    resolutionX,
    resolutionY
  );

  const [grayRamp, reverseGrayRamp] = getGrayRamp(
    style.type === "full",
    customRamp
  );

  let ramp = reverseGrayRamp;

  /** Flip the gray ramp when the bgColor is darker than the fill style. */
  if (style.type === "normal")
    ramp = isDarker(style.bgColor, style.fillStyle)
      ? reverseGrayRamp
      : grayRamp;

  const ascii = drawAscii(grayScales, resolutionX, ramp);

  const ctx = canvas.getContext("2d");
  if (!ctx) return;

  const fontSize = getFontSize(
    ascii.split("\n")[0] ?? "",
    style.fontFamily,
    ctx
  );

  ctx.save();

  ctx.fillStyle = style.bgColor;
  ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  if (style.type === "normal") {
    ctx.font = `bold ${fontSize}px ${style.fontFamily}`;
    ctx.textBaseline = "top";
    ctx.fillStyle = style.fillStyle;
  }

  const lines = ascii.split("\n");
  for (let line = 0; line < lines.length; line += 1) {
    const lineStr = lines[line] ?? "";
    for (let char = 0; char < lineStr.length; char += 1) {
      const charStr = lineStr[char] ?? "";
      if (style.type === "full") {
        const projection = (resolutionX * line + char) * 4;
        const color = rgbToHex(
          imageData[projection],
          imageData[projection + 1],
          imageData[projection + 2]
        );
        ctx.fillStyle = color;
      }
      ctx.fillText(
        charStr,
        char * getFontWidth(fontSize, style.fontFamily, ctx),
        line * fontSize
      );
    }
  }
  ctx.restore();
};

export const Ascii = (props: {
  resolutionX: number;
  resolutionY: number;
  style?: TextStyle;
}) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`ascii`, props);

  if (keyChanged && render && ctx) {
    render(() => {
      const style = {
        fontFamily: "monospace",
        fillStyle: "#00ff44",
        bgColor: "#000000",
        type: "normal",
        ...props.style,
      } as const;

      convert({
        canvas: ctx.canvas,
        resolutionX: props.resolutionX,
        resolutionY: props.resolutionY,
        style,
      });

      return key;
    });
  }

  return null;
};

/**
 * It is possible to override the characters used by specifying the
 * characterMap. Default is
 *
 *     "#&M8BW0XO%QDHAVYNERU468GQP3572CFJKLTZ1I!;.,:'^~-_|/*()[]{}<>+=? ";
 */
export const AsciiFullColor = (props: {
  resolutionX: number;
  resolutionY: number;
  style?: Omit<FullColorStyle, "type">;
  characterMap?: string;
}) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`ascii-full`, props);

  if (keyChanged && render && ctx) {
    render(() => {
      const style = {
        fontFamily: "monospace",
        bgColor: "#000000",
        type: "full",
        ...props.style,
      } as const;

      convert({
        canvas: ctx.canvas,
        resolutionX: props.resolutionX,
        resolutionY: props.resolutionY,
        style,
        ramp: props.characterMap,
      });

      return key;
    });
  }

  return null;
};

// const luminance = ([r, g, b]: [number, number, number]) => {
//   // Calculate the relative luminance of a color
//   const a = [r, g, b].map(function (v) {
//     v /= 255;
//     return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4);
//   }) as [number, number, number];
//   return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
// }

const luminance2 = (r: number, g: number, b: number) => {
  const RED = 0.2126;
  const GREEN = 0.7152;
  const BLUE = 0.0722;
  const GAMMA = 2.4;
  const a = [r, g, b].map((v) => {
    v /= 255;
    return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, GAMMA);
  }) as [number, number, number];

  return a[0] * RED + a[1] * GREEN + a[2] * BLUE;
};

const whiteLuminance = luminance2(255, 255, 255);

const contrastRatio = (l1: number, l2: number) => {
  // Calculate the contrast ratio between two luminance values
  return (Math.max(l1, l2) + 0.05) / (Math.min(l1, l2) + 0.05);
};

const convert2 = (opts: {
  canvas: HTMLCanvasElement;
  resolutionX: number;
  resolutionY: number;
  style: Required<InvertedColorStyle>;
  ramp?: string;
}) => {
  const { canvas, resolutionY, resolutionX, style, ramp: customRamp } = opts;

  const [imageData, grayScales] = convertToGrayScales(
    canvas,
    resolutionX,
    resolutionY
  );

  const [, reverseGrayRamp] = getGrayRamp(true, customRamp);
  const ramp = reverseGrayRamp;
  const ascii = drawAscii(grayScales, resolutionX, ramp);

  const ctx = canvas.getContext("2d");
  if (!ctx) return;

  ctx.save();

  const fontSize = getFontSize(
    ascii.split("\n")[0] ?? "",
    style.fontFamily,
    ctx
  );

  const fontWidth = getFontWidth(fontSize, style.fontFamily, ctx);
  ctx.font = `bold ${fontSize}px ${style.fontFamily}`;
  ctx.textBaseline = "top";

  const lines = ascii.split("\n");
  for (let line = 0; line < lines.length; line += 1) {
    const lineStr = lines[line] ?? "";
    for (let char = 0; char < lineStr.length; char += 1) {
      // fill bg
      const charStr = lineStr[char] ?? "";
      const projection = (resolutionX * line + char) * 4;

      const color = rgbToHex(
        imageData[projection],
        imageData[projection + 1],
        imageData[projection + 2]
      );

      ctx.fillStyle = color;
      ctx.fillRect(char * fontWidth, line * fontSize, fontWidth, fontSize);

      const l = luminance([
        imageData[projection] ?? 0,
        imageData[projection + 1] ?? 0,
        imageData[projection + 2] ?? 0,
      ]);

      const ratio = contrastRatio(whiteLuminance, l);
      const accessibleContrast = ratio >= 4.5;
      const textColor = accessibleContrast ? "#fff" : "#000";
      ctx.fillStyle = textColor;
      ctx.fillText(charStr, char * fontWidth, line * fontSize);
    }
  }

  ctx.restore();
};

export const AsciiInvertColor = (props: {
  resolutionX: number;
  resolutionY: number;
  style?: Omit<InvertedColorStyle, "type">;
  characterMap?: string;
}) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`ascii-inverted`, props);

  if (keyChanged && render && ctx) {
    render(() => {
      const style = {
        fontFamily: "monospace",
        type: "invert",
        ...props.style,
      } as const;

      convert2({
        canvas: ctx.canvas,
        resolutionX: props.resolutionX,
        resolutionY: props.resolutionY,
        style,
        ramp: props.characterMap,
      });

      return key;
    });
  }

  return null;
};

const darkenColor = (r: number, g: number, b: number, percent: number) => {
  const factor = 1 - percent;

  r = r * factor;
  g = g * factor;
  b = b * factor;

  r = Math.round(r);
  g = Math.round(g);
  b = Math.round(b);

  return [r, g, b] as const;
};

const lightenColor = (r: number, g: number, b: number, percent: number) => {
  // Increase each color component by the given percentage
  r = Math.min(255, Math.round(r + (255 - r) * percent));
  g = Math.min(255, Math.round(g + (255 - g) * percent));
  b = Math.min(255, Math.round(b + (255 - b) * percent));

  return [r, g, b] as const;
};

const convert3 = (opts: {
  canvas: HTMLCanvasElement;
  resolutionX: number;
  resolutionY: number;
  lightPercentFactor: number;
  darkPercentFactor: number;
  style: Required<InvertedColorStyle>;
  ramp?: string;
}) => {
  const {
    canvas,
    resolutionY,
    resolutionX,
    style,
    ramp: customRamp,
    lightPercentFactor,
    darkPercentFactor,
  } = opts;

  const [imageData, grayScales] = convertToGrayScales(
    canvas,
    resolutionX,
    resolutionY
  );

  const [, reverseGrayRamp] = getGrayRamp(true, customRamp);
  const ramp = reverseGrayRamp;
  const ascii = drawAscii(grayScales, resolutionX, ramp);

  const ctx = canvas.getContext("2d");
  if (!ctx) return;

  ctx.save();

  const fontSize = getFontSize(
    ascii.split("\n")[0] ?? "",
    style.fontFamily,
    ctx
  );

  const fontWidth = getFontWidth(fontSize, style.fontFamily, ctx);
  ctx.font = `bold ${fontSize}px ${style.fontFamily}`;
  ctx.textBaseline = "top";

  const lines = ascii.split("\n");
  for (let line = 0; line < lines.length; line += 1) {
    const lineStr = lines[line] ?? "";
    for (let char = 0; char < lineStr.length; char += 1) {
      // fill bg
      const charStr = lineStr[char] ?? "";
      const projection = (resolutionX * line + char) * 4;

      const color = rgbToHex(
        imageData[projection],
        imageData[projection + 1],
        imageData[projection + 2]
      );

      ctx.fillStyle = color;
      ctx.fillRect(char * fontWidth, line * fontSize, fontWidth, fontSize);

      const l = luminance2(
        imageData[projection] ?? 0,
        imageData[projection + 1] ?? 0,
        imageData[projection + 2] ?? 0
      );

      const ratio = contrastRatio(whiteLuminance, l);
      const accessibleContrast = ratio >= 3;
      const textColorFn = accessibleContrast ? lightenColor : darkenColor;

      const textColor = textColorFn(
        imageData[projection] ?? 0,
        imageData[projection + 1] ?? 0,
        imageData[projection + 2] ?? 0,
        accessibleContrast ? lightPercentFactor : darkPercentFactor
      );

      ctx.fillStyle = rgbToHex(textColor[0], textColor[1], textColor[2]);
      ctx.fillText(charStr, char * fontWidth, line * fontSize);
    }
  }

  ctx.restore();
};

export const AsciiLightColor = (props: {
  resolutionX: number;
  resolutionY: number;
  lightPercentFactor?: number;
  darkPercentFactor?: number;
  style?: Omit<InvertedColorStyle, "type">;
  characterMap?: string;
}) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`ascii-inverted`, props);

  if (keyChanged && render && ctx) {
    render(() => {
      const style = {
        fontFamily: "monospace",
        type: "invert",
        ...props.style,
      } as const;

      convert3({
        canvas: ctx.canvas,
        resolutionX: props.resolutionX,
        resolutionY: props.resolutionY,
        style,
        ramp: props.characterMap,
        lightPercentFactor: props.lightPercentFactor ?? 0.5,
        darkPercentFactor: props.darkPercentFactor ?? 0.25,
      });

      return key;
    });
  }

  return null;
};
