import React from "react";

export const PrintKeys = <Obj,>(props: {
  obj: Obj;
  keys: Array<keyof Obj>;
}) => {
  const { keys, obj } = props;
  return (
    <>
      {keys.map((key) => (
        <div key={JSON.stringify(key)}>
          {JSON.stringify(key)}: {JSON.stringify(obj[key])}
        </div>
      ))}
    </>
  );
};
