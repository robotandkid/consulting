export const stringify = (obj: Parameters<typeof JSON.stringify>[0]) => {
  return JSON.stringify(obj, (_, value) => {
    if (typeof value === "function") return value.toString();
    return value;
  });
};
