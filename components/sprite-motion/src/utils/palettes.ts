/** 2 colors */

export const palette2funkyJam = () => ["#920244", "#fec28c"] as const;
export const palette2chasingLight = () => ["#000000", "#ffff02"] as const;

/** 5 colors */

export const palette5blessing = () =>
  ["#74569b", "#96fbc7", "#f7ffae", "#ffb3cb", "#d8bfd8"] as const;

export const palette5sunsetRed = () =>
  ["#0d101b", "#281a2d", "#6b2341", "#af2747", "#ee243d"] as const;

export const marioNesPalette = () =>
  ["#7e9953", "#d9a739", "#bd0808", "#0300ff", "#e8ff00"] as const;

export const palette5marioNes = marioNesPalette;

export const gameboyPalette = () =>
  ["#eefab3", "#c2d97d", "#98b253", "#4e8433", "#174f39"] as const;

export const palette5gameBoy = gameboyPalette;

export const dalhia5Palette = () =>
  ["#73020C", "#A60321", "#F20F38", "#F21B54", "#260101"] as const;

export const palette5dalhia = dalhia5Palette;

export const palette5cap5 = () =>
  ["#fadda2", "#6b61ff", "#66a1ff", "#8ecde6", "#f0eff4"] as const;

export const palette5maruMaruGum = () =>
  ["#fda9a9", "#f3eded", "#b9eedc", "#96beb1", "#82939b"] as const;

export const palette5goldenSunset = () =>
  ["#ffecd6", "#ffb873", "#cb765c", "#7a4a5a", "#25213e"] as const;

export const palette5spanishSunset = () =>
  ["#f5ddbc", "#fabb64", "#fd724e", "#a02f40", "#5f2f45"] as const;

export const palette5origamiClouds = () =>
  ["#dee3e2", "#fccbcb", "#78b3d6", "#d86969", "#4f7969"] as const;

export const palette5LeopoldsDreams = () =>
  ["#372134", "#474476", "#4888b7", "#6dbcb9", "#8cefb6"] as const;

export const palette5sheep = () =>
  ["#480a30", "#b41360", "#ff327c", "#ff80ae", "#ffdae8"] as const;

export const palette5neo5 = () =>
  ["#0e0e0e", "#5433be", "#e624af", "#3df9ea", "#effafa"] as const;

/** 6 colors */

export const oil6Palette = () =>
  ["#fbf5ef", "#f2d3ab", "#c69fa5", "#8b6d9c", "#494d7e", "#272744"] as const;

export const palette6oil = oil6Palette;

export const cybergumsPalette = () =>
  ["#3a2b3b", "#2d4a54", "#0c7475", "#bc4a9b", "#eb8d9c", "#ffd8ba"] as const;

export const palette6cyberGums = cybergumsPalette;

/** 8 colors */

export const clement8Palette = () =>
  [
    "#000871",
    "#8854f3",
    "#639bff",
    "#63ffba",
    "#ff8c5c",
    "#ff79ae",
    "#fff982",
    "#ffffff",
  ] as const;

export const palette8clement = clement8Palette;

export const funkyFuture8Palette = () =>
  [
    "#2b0f53",
    "#ab1f65",
    "#ff4f69",
    "#fff7f8",
    "#ff8142",
    "#ffda45",
    "#3368dc",
    "#49e7ec",
  ] as const;

export const palette8funkyFuture = funkyFuture8Palette;

export const bright8palette = () =>
  [
    "#5ca8c2",
    "#055d31",
    "#86d86a",
    "#fdfdf0",
    "#faa3ce",
    "#a4573d",
    "#02020f",
    "#7a2796",
  ] as const;

export const palette8bright = bright8palette;

export const spazbol8palette = () =>
  [
    "#000000",
    "#51007c",
    "#5073d3",
    "#8fd350",
    "#fff600",
    "#ffffff",
    "#ed8f0b",
    "#cc2718",
  ] as const;

export const palette8spazbol = spazbol8palette;

/** 9 colors */

export const palette9smokey09 = () =>
  [
    "#fafafa",
    "#d4d8e0",
    "#acacac",
    "#918b8c",
    "#6b615e",
    "#3b342e",
    "#24211a",
    "#0e0d0a",
    "#030201",
  ] as const;

/** 16 colors */

export const sweetiePalette = () =>
  [
    "#1a1c2c",
    "#5d275d",
    "#b13e53",
    "#ef7d57",
    "#ffcd75",
    "#a7f070",
    "#38b764",
    "#257179",
    "#29366f",
    "#3b5dc9",
    "#41a6f6",
    "#73eff7",
    "#f4f4f4",
    "#94b0c2",
    "#566c86",
    "#333c57",
  ] as const;

export const palette16sweetie = sweetiePalette;

export const palette16shidoCyberNeon = () =>
  [
    "#00033c",
    "#005260",
    "#009d4a",
    "#0aff52",
    "#003884",
    "#008ac5",
    "#00f7ff",
    "#ff5cff",
    "#ac29ce",
    "#600088",
    "#b10585",
    "#ff004e",
    "#2a2e79",
    "#4e6ea8",
    "#add4fa",
    "#ffffff",
  ] as const;

/** 22 colors */

export const palette22chasm = () =>
  [
    "#85daeb",
    "#5fc9e7",
    "#5fa1e7",
    "#5f6ee7",
    "#4c60aa",
    "#444774",
    "#32313b",
    "#463c5e",
    "#5d4776",
    "#855395",
    "#ab58a8",
    "#ca60ae",
    "#f3a787",
    "#f5daa7",
    "#8dd894",
    "#5dc190",
    "#4ab9a3",
    "#4593a5",
    "#5efdf7",
    "#ff5dcc",
    "#fdfe89",
    "#ffffff",
  ] as const;
