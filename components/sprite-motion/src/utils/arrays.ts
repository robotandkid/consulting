import React from "react";

export class WrappedArray<Data> {
  data: Readonly<Data[]>;
  len = 0;

  constructor(data: Readonly<Data[]>, start = 0) {
    this.data = data;
    this.len = start;
  }

  current() {
    const nextIndex = (this.len + 1) % this.data.length;
    const prevIndex =
      this.len > 0
        ? this.len - 1
        : this.data.length - ((this.len - 1) % this.data.length);

    return [
      this.data[prevIndex],
      this.data[this.len],
      this.data[nextIndex],
      this.len,
    ] as [Data, Data, Data, number];
  }

  next() {
    this.len = (this.len + 1) % this.data.length;
    return this.current();
  }

  prev() {
    this.len =
      this.len > 0
        ? this.len - 1
        : this.data.length + ((this.len - 1) % this.data.length);

    return this.current();
  }
}

export const useWrappedArray = <Data>(data: Readonly<Data[]>) => {
  // const serializedData = JSON.stringify(data);
  const [len, setLen] = React.useState(0);
  const wrapped = new WrappedArray<Data>(data, len);
  const [prev, current, next] = wrapped.current();

  return [
    prev,
    current,
    next,
    () => {
      const [, , , nextLen] = wrapped.next();
      setLen(nextLen);
    },
    () => {
      const [, , , nextLen] = wrapped.prev();
      setLen(nextLen);
    },
  ] as const;
};
