import { useCallback, useEffect, useRef } from "react";

export const useGuardedCallback = <FuncParams = void,>(
  cb: undefined | ((...args: FuncParams[]) => void)
): typeof cb extends undefined
  ? undefined
  : (...args: FuncParams[]) => void => {
  const mounted = useRef(false);

  useEffect(function checkMount() {
    mounted.current = true;
    return () => {
      mounted.current = false;
    };
  }, []);

  const cbRef = useRef(cb);
  cbRef.current = cb;

  const callback = useCallback((...args: FuncParams[]) => {
    if (mounted.current) cbRef.current?.(...args);
  }, []);

  return (cb === undefined ? undefined : callback) as any;
};
