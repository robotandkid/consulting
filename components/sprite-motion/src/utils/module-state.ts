import { useEffect, useState } from "react";

interface ModuleState<State> {
  getState: () => State;
  setState: (action: State | ((prev: State) => State)) => void;
  subscribe: (cb: () => void) => () => void;
}

export const createModuleState = <State>(
  initial: State
): ModuleState<State> => {
  let state = initial;
  const cbs = new Set<() => void>();
  const getState = () => state;

  const setState = (next: State | ((prev: State) => State)) => {
    if (typeof next === "function") {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore can't infer next
      state = next(state);
    } else {
      state = next;
    }
    cbs.forEach((cb) => cb());
  };

  const subscribe = (cb: () => void) => {
    cbs.add(cb);
    return () => {
      cbs.delete(cb);
    };
  };

  return { getState, setState, subscribe };
};

export const useModuleState = <State>(moduleState: ModuleState<State>) => {
  const [state, setState] = useState(moduleState.getState());

  useEffect(() => {
    const unsubscribe = moduleState.subscribe(() => {
      setState(moduleState.getState());
    });

    setState(moduleState.getState());

    return unsubscribe;
  }, [moduleState]);

  return [state, moduleState.setState] as const;
};
