import React, { useCallback, useEffect, useRef, useState } from "react";
import { useGuardedCallback } from "./hooks";

export const sleep = (timeout: number) =>
  new Promise((resolve) => setTimeout(resolve, timeout));

export const useInterval = (
  callback: () => void | "abort",
  timeout: number
) => {
  const ref = useRef(callback);
  ref.current = callback;

  useEffect(() => {
    const interval = setInterval(() => {
      const result = ref.current();
      if (result === "abort") clearInterval(interval);
    }, timeout);
    return () => {
      clearInterval(interval);
    };
  }, [timeout]);
};

export const useInterpolate = (
  start: number,
  end: number,
  opts: {
    disabled?: boolean;
    /** How often setInterval should run */
    timeDelta?: number;
    /** How much the value should change between the timeDelta */
    delta: number;
    onDone?: () => void;
  }
) => {
  const { timeDelta = 100, delta, onDone, disabled } = opts;
  const [value, setValue] = useState(start);
  const increasing = start < end;

  const called = useRef(false);
  const callGuard = useRef(() => {});
  callGuard.current = () => {
    if (!called.current) {
      called.current = true;
      onDone?.();
    }
  };

  useEffect(() => {
    if (disabled) return;

    const int = setInterval(() => {
      if (increasing) {
        setValue((s) => {
          if (s + delta > end) {
            clearInterval(int);
            callGuard.current();
            return s;
          }
          return s + delta;
        });
      } else {
        setValue((s) => {
          if (s - delta < end) {
            clearInterval(int);
            callGuard.current();
            return s;
          }
          return s - delta;
        });
      }
    }, timeDelta);

    return () => {
      clearInterval(int);
    };
  }, [timeDelta, delta, disabled, end, increasing]);

  return value;
};

export const ShowAfterDelay = (props: {
  timeout: number;
  children: React.ReactNode;
}) => {
  const [show, setShow] = useState(false);
  const { timeout } = props;

  useEffect(() => {
    const unsubscribe = setTimeout(() => setShow(true), timeout);
    return () => {
      clearTimeout(unsubscribe);
    };
  }, [timeout]);

  return <>{show && props.children}</>;
};

export const useStateSteps = <Step extends string>(props: {
  steps: Step[];
  Button?: React.ReactElement<{ onClick?: () => void }>;
}) => {
  const {
    Button = (
      <div style={{ zIndex: 999 }}>
        <button>Next step</button>
      </div>
    ),
  } = props;

  const steps = React.useRef(props.steps);
  steps.current = props.steps;
  const [currentStep, setStep] = React.useState(0);

  type StepOpts = {
    step: Step;
    type?: "==" | ">=" | "<=";
  };

  const StepComp = useCallback(
    (theseProps: { children: React.ReactNode } & StepOpts) => {
      const { step: targetStep, type = "==", children } = theseProps;
      if (type === "==" && targetStep === steps.current[currentStep])
        return <>{children}</>;
      else if (type === ">=") {
        const targetIndex = steps.current.indexOf(targetStep);
        if (currentStep >= targetIndex) return <>{children}</>;
      } else if (type === "<=") {
        const targetIndex = steps.current.indexOf(targetStep);
        if (currentStep <= targetIndex) return <>{children}</>;
      }
      return null;
    },
    [currentStep]
  );

  const nextStep = useGuardedCallback(() => {
    setStep((c) => Math.min(steps.current.length - 1, c + 1));
  });

  const NextButton = React.cloneElement(Button, {
    onClick: () => {
      nextStep();
    },
  });

  const reset = useGuardedCallback(() => {
    setStep(0);
  });

  return [
    StepComp,
    () => <>{NextButton}</>,
    nextStep,
    steps.current[currentStep],
    reset,
  ] as const;
};
