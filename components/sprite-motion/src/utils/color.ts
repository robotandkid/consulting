type RGB = [number, number, number];

const luminance = (r: number, g: number, b: number) => {
  const a = [r, g, b].map((v) => {
    v /= 255;
    return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4);
  }) as RGB;
  return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
};

type Color = `#${string}`;

const hexToRgb = (hex: Color) => {
  const bigint = parseInt(hex.slice(1), 16);
  const r = (bigint >> 16) & 255;
  const g = (bigint >> 8) & 255;
  const b = bigint & 255;
  return [r, g, b] as RGB;
};

const contrastRatio = (color1: Color, color2: Color) => {
  const lum1 = luminance(...hexToRgb(color1)) + 0.05;
  const lum2 = luminance(...hexToRgb(color2)) + 0.05;
  return lum1 > lum2 ? lum1 / lum2 : lum2 / lum1;
};

/**
 * To ensure that you pick a color with accessible contrast based on a given hex
 * color, you can use the WCAG 2.1 guidelines for color contrast. Typically, a
 * contrast ratio of at least 4.5:1 is required for normal text, and 3:1 for
 * larger text.
 */
export const pickAccessibleColor = (
  baseHex: Color,
  palette: Readonly<Color[]>
) => {
  const minContrastRatio = 4.5;
  for (const color of palette) {
    if (contrastRatio(baseHex, color) >= minContrastRatio) {
      return color;
    }
  }
  for (const color of ["#000000", "#ffffff"] as Color[]) {
    if (contrastRatio(baseHex, color) >= minContrastRatio) {
      return color;
    }
  }
  return "#000000";
};
