import React from "react";

const keys = [
  "ArrowLeft",
  "ArrowUp",
  "ArrowRight",
  "ArrowDown",
  "Enter",
  "Space",
  "Esc",
] as const;

type Key = typeof keys[number];

export const useKeyListeners = (props: {
  onKeyDown: (key: Key) => void;
  disabled?: boolean;
}) => {
  const { onKeyDown, disabled } = props;

  const ref = React.useRef({ onKeyDown });
  ref.current = { onKeyDown };

  React.useEffect(() => {
    if (disabled) return;

    const onKeyDown = (ev: KeyboardEvent) => {
      const curKey = keys[keys.indexOf(ev.key as Key)];
      if (curKey) {
        ref.current.onKeyDown(curKey);
      }
    };

    window.addEventListener("keydown", onKeyDown);
    return () => {
      window.removeEventListener("keydown", onKeyDown);
    };
  }, [disabled]);
};
