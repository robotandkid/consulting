import React, { useEffect, useRef, useState } from "react";
import {
  Downloads,
  MetaResource,
  useAbortCtrlCtx,
  useDownloadsCtx,
} from "./resources";
import {
  MotionImg,
  StaticImg,
  useEngineContext,
  usePagination,
} from "./sprite-motion";
import { usePanelCanvas } from "./storyboard";
import { useGuardedCallback } from "./utils/hooks";

type Defined<Func extends (args: any) => any> = NonNullable<ReturnType<Func>>;

export const ActionBlock = <
  Resources extends Record<string, MetaResource>
>(props: {
  children: (opts: {
    paginate: Defined<typeof usePagination>;
    engine: Defined<typeof useEngineContext>;
    resources: Downloads<Resources>;
    /**
     * This exists because you can't cancel async tasks in JS 🙄.
     *
     * So if you have a long running (async) task, check this property first.
     *
     * Example:
     *
     * ```tsx
     * <ActionBlock>
     *   {async ({ signal, isBlockCancelled }) => {
     *     const items = fetch("/items", { signal });
     *     if (!isBlockCancelled) return;
     *     const moreItems = fetch("/items", { signal });
     *     if (!isBlockCancelled) return;
     *     // ...
     *   }}
     * </ActionBlock>;
     * ```
     */
    isBlockCancelled: boolean;
    signal: AbortController["signal"];
    canvas: HTMLCanvasElement;
  }) => Promise<void> | void;
}) => {
  const init = useRef(false);
  const paginate = usePagination();
  const engine = useEngineContext();
  const resources = useDownloadsCtx<Resources>();
  const ctrl = useAbortCtrlCtx();
  const canvas = usePanelCanvas();

  if (!init.current && !!paginate && !!engine && !!ctrl && !!canvas) {
    init.current = true;
    props.children({
      paginate,
      engine,
      resources,
      signal: ctrl.signal,
      isBlockCancelled: ctrl.signal.aborted,
      canvas,
    });
  }

  return null;
};

/**
 * Partition image into equal parts:
 *
 * - NumPartitions must be even
 *
 *                     256
 *        *---------*---------*
 *        |         |         |
 *        *---------*---------*
 *        |         |         |
 *        *---------*---------*
 *
 *          n = 2
 *          pWidth = 128
 */
const partition = async (opts: {
  xNumPartitions: number;
  yNumPartitions: number;
  width: number;
  height: number;
  src: string;
}) => {
  const { xNumPartitions, yNumPartitions, width, height, src } = opts;

  if (xNumPartitions % 2 !== 0 || yNumPartitions % 2 !== 0)
    throw new Error("Need to partition into an even number of parts!");

  // Calculate the width and height of each partition
  const partitionWidth = width / xNumPartitions;
  const partitionHeight = height / yNumPartitions;

  const canvas = document.createElement("canvas");
  canvas.width = partitionWidth;
  canvas.height = partitionHeight;
  const ctx = canvas.getContext("2d");

  const imagePartitions: Array<{
    coords: [number, number, number, number];
    src: string;
  }> = [];

  const img = new Image();

  return new Promise<typeof imagePartitions>((resolve) => {
    /** You technically have to wait for the image to load before processing */
    img.addEventListener(
      "load",
      () => {
        for (let i = 0; i < xNumPartitions; i++) {
          for (let j = 0; j < xNumPartitions; j++) {
            const coords = {
              startX: i * partitionWidth,
              startY: j * partitionHeight,
              dx: partitionWidth,
              dy: partitionHeight,
            } as const;

            ctx?.drawImage(
              img,
              coords.startX,
              coords.startY,
              coords.dx,
              coords.dy,
              0,
              0,
              partitionWidth,
              partitionHeight
            );

            // Convert the canvas content back into a Blob (as an image)
            canvas.toBlob((blob) => {
              // Here 'blob' contains the image data of a partition
              if (blob)
                imagePartitions.push({
                  coords: [coords.startX, coords.startY, coords.dx, coords.dy],
                  src: URL.createObjectURL(blob),
                });
            });
          }
        }

        resolve(imagePartitions);
      },
      false
    );

    img.src = src;
  });
};

const random = (scale: number) =>
  Math.random() * scale * (Math.random() > 0.5 ? -1 : 1);

export const ExplodingImage = (
  props: Parameters<typeof StaticImg>[0] & {
    xNumParticles: number;
    yNumParticles: number;
    explosionFactor?: number;
    delayInMs: number;
  }
) => {
  const {
    onLoad,
    src,
    xNumParticles,
    yNumParticles,
    delayInMs,
    explosionFactor = 5,
    ...rest
  } = props;
  const rawSrcUrl = typeof src === "string" ? src : src?.resource;

  const [srcUrl, setSrcUrl] = useState<string | undefined>(
    typeof rawSrcUrl === "string" ? rawSrcUrl : undefined
  );

  const loadedUrl = useRef(false);
  if (!loadedUrl.current) {
    loadedUrl.current = true;
    if (rawSrcUrl && typeof rawSrcUrl !== "string") rawSrcUrl.then(setSrcUrl);
  }

  const [dims, setDims] = useState<[number, number]>();
  const [blobs, _setBlobs] = useState<Awaited<ReturnType<typeof partition>>>();
  const setBlobs = useGuardedCallback(_setBlobs);

  useEffect(() => {
    if (!srcUrl || !dims) return;

    const getBlobs = partition({
      xNumPartitions: xNumParticles,
      yNumPartitions: yNumParticles,
      width: dims[0],
      height: dims[1],
      src: srcUrl,
    });

    const timeout = setTimeout(async () => {
      const blobs = await getBlobs;
      setBlobs(blobs);
    }, delayInMs);

    return () => {
      clearTimeout(timeout);
    };
  }, [delayInMs, dims, xNumParticles, setBlobs, srcUrl, yNumParticles]);

  const [readyForBlobs, setReadyForBlobs] = useState(false);
  const blobsReady = useRef<Record<number, true>>({});

  const markReady = (blobIndex: number) => {
    blobsReady.current[blobIndex] = true;
    if (Object.keys(blobsReady.current).length === blobs?.length ?? -1)
      setReadyForBlobs(true);
  };

  return (
    <>
      {!readyForBlobs && (
        <StaticImg
          onLoad={(e) => {
            setDims([e.currentTarget.width, e.currentTarget.height]);
            onLoad?.(e);
          }}
          src={props.src}
          {...rest}
        />
      )}
      {blobs?.map((blob, index) => (
        <MotionImg
          onLoad={() => markReady(index)}
          key={index}
          startLeft={blob.coords[0]}
          startTop={blob.coords[1]}
          src={blob.src}
          vx={random(explosionFactor)}
          vy={random(explosionFactor)}
          style={{
            display: readyForBlobs ? "inherit" : "hidden",
          }}
        />
      ))}
    </>
  );
};
