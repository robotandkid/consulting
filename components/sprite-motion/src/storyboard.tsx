import { AnimatePresence, m } from "framer-motion";
import { wrap } from "popmotion";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { styled } from "@styled-system/jsx/index";
import { useAbortCtrlCtx } from "./resources";
import { usePagination, useSetPagination } from "./sprite-motion";
import { makeReadOnlyContext } from "./utils/context";

export const storyZIndex = 0;

/** Breakpoints: 480, 600, 768, 800, 992, 1024, 1200 */
const StoryBoardContainer = styled(m.section, {
  base: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    boxSizing: "border-box",

    // full bleed
    width: "[100%]",
    height: "[100%]",
    // left: "50%",
    // right: "50%",
    // marginLeft: "-50vw",
    // marginRight: "-50vw",
    // marginTop: "-1vh",
  },
});

const PanelContainer = styled(m.div, {
  base: {
    width: "[100%]",
    height: "[100%]",
    /** Absolute needed for swipe */
    position: "absolute",
    overflow: "hidden",
  },
});

export const PanelBackground = styled(m.img, {
  base: {
    width: "[100%]",
    height: "[100%]",
    position: "absolute",
    pointerEvents: "none",
    objectFit: "cover",
  },
});

const PanelInner = styled(m.div, {
  base: {
    width: "[100%]",
    height: "[100%]",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
  },
});

const [PanelCanvasProvider, usePanelCanvas] = makeReadOnlyContext<
  HTMLCanvasElement | undefined
>("PanelCanvas");

export { usePanelCanvas };

const [PanelCanvasCtx2DProvider, usePanelCanvasCtx2D] = makeReadOnlyContext<
  CanvasRenderingContext2D | undefined
>("PanelCanvasCtx2D");

export { usePanelCanvasCtx2D };

export const Panel = (
  props: Omit<Parameters<typeof PanelInner>[0], "children"> & {
    children: React.ReactNode;
  }
) => {
  const { children, ...rest } = props;
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [canvas, setCanvas] = useState<HTMLCanvasElement>();
  const [ctx, setCtx] = useState<CanvasRenderingContext2D>();

  useEffect(() => {
    if (!canvasRef.current) return;
    const dims = canvasRef.current.parentElement?.getBoundingClientRect();
    if (dims) {
      canvasRef.current.width = dims.width;
      canvasRef.current.height = dims.height;
    }
    setCanvas(canvasRef.current);
    const c = canvasRef.current.getContext("2d");
    if (c) setCtx(c);
  }, []);

  return (
    <PanelInner {...rest}>
      <PanelCanvasCtx2DProvider value={ctx}>
        <PanelCanvasProvider value={canvas}>{children}</PanelCanvasProvider>
      </PanelCanvasCtx2DProvider>
      <canvas
        ref={canvasRef}
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          // imageRendering: "pixelated",
        }}
      ></canvas>
    </PanelInner>
  );
};

const NavButton = styled("button", {
  base: {
    top: "[calc(50% - 1.5rem)]",
    borderRadius: "[5rem]",
    width: "[5rem]",
    height: "[5rem]",
    fontSize: "[3rem]",
    position: "absolute",
    background: "[white]",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    userSelect: "none",
    cursor: "pointer",
    fontWeight: "bold",
    zIndex: 2,
  },
  variants: {
    type: {
      prev: {
        left: "[10px]",
      },
      next: {
        right: "[10px]",
      },
    },
  },
});

const wipeTransition = ["prev-wipe", "next-wipe"] as const;
const fadeTransition = ["prev-fade", "next-fade"] as const;
const simpleTransition = ["prev", "next"] as const;

type WipeTransition = typeof wipeTransition[number];
type FadeTransition = typeof fadeTransition[number];
type SimpleTransition = typeof simpleTransition[number];

export type PanelTransition =
  | WipeTransition
  | FadeTransition
  | SimpleTransition;

const panelContainerVariants = {
  enter: (direction: PanelTransition) => {
    switch (direction) {
      case "next-wipe":
      case "prev-wipe":
        return {
          x: direction === "next-wipe" ? 1000 : -1000,
          opacity: 0,
        };
      case "next-fade":
      case "prev-fade":
        return {
          opacity: 0,
        };
      case "next":
      case "prev":
        return {};
      default: {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const x: never = direction;
        return {};
      }
    }
  },
  center: {
    zIndex: storyZIndex + 1,
    x: 0,
    opacity: 1,
  },
  exit: (direction: PanelTransition) => {
    switch (direction) {
      case "next-wipe":
      case "prev-wipe":
        return {
          zIndex: storyZIndex,
          x: direction === "prev-wipe" ? 1000 : -1000,
          opacity: 0,
        };
      case "next-fade":
      case "prev-fade":
        return {
          opacity: 0,
        };
      case "next":
      case "prev":
        return {};
      default: {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const x: never = direction;
        return {};
      }
    }
  },
};

/**
 * Experimenting with distilling swipe offset and velocity into a single
 * variable, so the less distance a user has swiped, the more velocity they need
 * to register as a swipe. Should accomodate longer swipes and short flicks
 * without having binary checks on just distance thresholds and velocity > 0.
 */
const swipeConfidenceThreshold = 10000;
const swipePower = (offset: number, velocity: number) => {
  return Math.abs(offset) * velocity;
};

export const StoryBoard = (props: {
  children?: React.ReactNode[] | React.ReactNode;
  disableManualNavigation?: boolean;
}) => {
  const { children, disableManualNavigation = false } = props;
  const items = Array.isArray(children) ? children : [children];

  const [[page, direction], setPage] = useState<[number, PanelTransition]>([
    0,
    "next-wipe",
  ]);

  /**
   * We only have 3 images, but we paginate them absolutely (ie 1, 2, 3, 4,
   * 5...) and then wrap that within 0-2 to find our image ID in the array
   * below. By passing an absolute page index as the `motion` component's `key`
   * prop, `AnimatePresence` will detect it as an entirely new image. So you can
   * infinitely paginate as few as 1 images.
   */
  const itemIndex = wrap(0, items.length, page);

  const abort = useAbortCtrlCtx();
  const abortRef = useRef(abort);

  /** Needed when callers do not memoize correctly */
  useEffect(() => {
    abortRef.current = abort;
  });

  const paginate = useCallback((newDirection: PanelTransition | number) => {
    if (abortRef.current?.signal.aborted) return;

    /**
     * Honestly not sure why this work. We were getting the dreaded cannot
     * update a component while another renders error.
     *
     * We were calling page from inside a setInterval and that was making React
     * blow up.
     */
    setTimeout(() => {
      if (typeof newDirection === "number") {
        setPage(([p]) => {
          if (newDirection <= p) return [newDirection, "prev-fade"];
          else return [newDirection, "next-fade"];
        });
        return;
      }
      switch (newDirection) {
        case "next":
        case "next-wipe":
        case "next-fade":
          setPage(([p]) => [p + 1, newDirection]);
          break;
        case "prev":
        case "prev-wipe":
        case "prev-fade":
          setPage(([p]) => [p - 1, newDirection]);
          break;
        default: {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const x: never = newDirection;
        }
      }
    });
  }, []);

  const setPaginate = useSetPagination();
  useEffect(() => {
    /**
     * Under the hood this is a setState so if you pass a function, it thinks
     * it's a state update callback
     */
    setPaginate(() => paginate);
  }, [paginate, setPaginate]);

  useEffect(() => {
    if (disableManualNavigation) return;

    const keys = (evt: KeyboardEvent) => {
      if (evt.key === "ArrowLeft") paginate("prev-wipe");
      else if (evt.key === "ArrowRight") paginate("next-wipe");
    };

    window.addEventListener("keydown", keys);

    return () => {
      window.removeEventListener("keydown", keys);
    };
  }, [disableManualNavigation, paginate]);

  return (
    <StoryBoardContainer>
      <AnimatePresence initial={false} custom={direction}>
        <PanelContainer
          key={page}
          custom={direction}
          variants={panelContainerVariants}
          initial="enter"
          animate="center"
          exit="exit"
          transition={{
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore bad panda types
            x: { type: "spring", stiffness: 300, damping: 30 },
            opacity: { duration: 0.2 },
          }}
          {...(!disableManualNavigation && {
            drag: "x",
            dragConstraints: { left: 0, right: 0 },
            dragElastic: 1,
            onDragEnd: (_, { offset, velocity }) => {
              const swipe = swipePower(offset.x, velocity.x);

              if (swipe < -swipeConfidenceThreshold) {
                paginate("next-wipe");
              } else if (swipe > swipeConfidenceThreshold) {
                paginate("prev-wipe");
              }
            },
          })}
        >
          {items[itemIndex]}
        </PanelContainer>
      </AnimatePresence>
    </StoryBoardContainer>
  );
};

export const StoryBoardNav = () => {
  const paginate = usePagination();

  return (
    <nav>
      <NavButton type="next" onClick={() => paginate?.("next-wipe")}>
        {"▶"}
      </NavButton>
      <NavButton type="prev" onClick={() => paginate?.("prev-wipe")}>
        {"◀"}
      </NavButton>
    </nav>
  );
};
