import { World } from "../sprite-motion";
import { UiPanel } from "./ui";
import React from "react";
import { UiCommandPanel } from "./ui-command-panel";
import { useDownloadsCtx } from "../resources";
import { Panel, StoryBoard } from "../storyboard";
import { Canvas } from "../canvas/canvas";
import { UiDialogWithCommands } from "./ui-dialog";

export const CommandWithDialogSubMenuExample = () => {
  const [cmd, setCmd] = React.useState("");
  return (
    <>
      <UiCommandPanel
        width="[200px]"
        height="[150px]"
        commands={["Fight", "PSI", "Goods", "Check", "Auto", "Guard", "Run"]}
        subCommands={{
          Fight: {
            type: "dialog",
            text: "You are not allowed to fight",
          },
          PSI: { type: "dialog", text: "" },
          Goods: { type: "dialog", text: "There are no goods" },
          Check: { type: "dialog", text: "" },
          Auto: { type: "dialog", text: "This is disabled" },
          Guard: { type: "dialog", text: "" },
          Run: { type: "dialog", text: "You have nowhere to run" },
        }}
        onDone={(command, subCommand) => setCmd(`${command}:${subCommand}`)}
      />
      <div>Command: {cmd}</div>
    </>
  );
};

export const CommandWithSubCommandsExample = () => {
  const [cmd, setCmd] = React.useState("");
  return (
    <>
      <UiCommandPanel
        width="[200px]"
        height="[150px]"
        commands={["Fight", "PSI", "Goods", "Check", "Auto", "Guard", "Run"]}
        subCommands={{
          Fight: {
            type: "cmd",
            commands: ["Nurse", "Doctor", "Dog"],
          },
          PSI: { type: "dialog", text: "" },
          Goods: { type: "dialog", text: "There are no goods" },
          Check: { type: "dialog", text: "" },
          Auto: { type: "dialog", text: "This is disabled" },
          Guard: { type: "dialog", text: "" },
          Run: { type: "dialog", text: "You have nowhere to run" },
        }}
        onDone={(command, subCommand) => setCmd(`${command}:${subCommand}`)}
      />
      <div>Command: {cmd}</div>
    </>
  );
};

const resources = {
  done: {
    type: "sound",
    url: "./coin-toggle.mp3",
  },
  toggle: {
    type: "sound",
    url: "./coin-toggle-final.mp3",
  },
} as const;

const FullCommandPanelInner = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  return (
    <>
      <UiPanel
        position="absolute"
        bottom="[0px]"
        width="[100%]"
        height="[5rem]"
      >
        A Drakke draws near! Command?
      </UiPanel>
      <UiCommandPanel
        position="absolute"
        bottom="[7rem]"
        height="[5rem]"
        commands={["fight", "run", "spell", "item"]}
        subCommands={{
          fight: { type: "cmd", commands: ["monster", "lamp"] },
          run: { type: "dialog", text: "You can't run" },
          spell: {
            type: "dialog",
            text: "You can't use a spell right now",
          },
          item: undefined,
        }}
        onCommandChange={() => downloads.toggle.resource.play()}
        onDone={() => downloads.done.resource.play()}
      />
    </>
  );
};

export const FullCommandPanelExample = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{ background: "black" }}
      width={500}
      height={500}
    >
      <FullCommandPanelInner />
    </World>
  );
};

export const UiDialogExample = () => {
  return (
    <World
      resources={resources}
      loading={<div>Loading...</div>}
      style={{ background: "black" }}
      width={500}
      height={500}
    >
      <StoryBoard>
        <Panel>
          <Canvas>
            <UiDialogWithCommands
              dialogPos={{
                left: 10,
                top: 10,
                width: 300,
                height: 50,
                textOffsetX: 15,
                textOffsetY: 25,
                speed: 100,
              }}
              commandsDelay={1000}
              commands={["yes", "no"]}
              commandPos={{
                left: 50,
                top: 150,
                width: 100,
                height: 100,
                textOffsetX: 5,
                textOffsetY: 25,
              }}
              textStyles={{
                glitch: false,
                font: "24px monospace",
                lineHeightAsPx: 24,
                fillStyle: "#000",
              }}
              containerStyles={{
                strokeColor: "#ffffff",
                fillColor: "#ffffff",
              }}
              onSelect={(value) => alert(value)}
            >
              well shit
            </UiDialogWithCommands>
          </Canvas>
        </Panel>
      </StoryBoard>
    </World>
  );
};
