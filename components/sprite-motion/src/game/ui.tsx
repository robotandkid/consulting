import { css } from "@styled-system/css";
import { styled } from "@styled-system/jsx/index";
import { m } from "framer-motion";
import React, { useState } from "react";
import { useGuardedCallback } from "../utils/hooks";
import { useInterval } from "../utils/timeouts";

/**
 * @deprecated Do not use. It is internal
 * @internal
 */
export const UiPanelOuter = styled("div", {
  base: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    boxSizing: "border-box",
    backgroundColor: "sprite-motion-gb.50",
    color: "sprite-motion-gb.53",
    fontFamily: "sprite-motion",
    padding: "[0.25em]",
    fontSize: "[1em]",
    lineHeight: "[1]",
  },
});

const UiPanelInner = styled(m.div, {
  base: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    overflow: "hidden",
    boxSizing: "border-box",
    borderRadius: "[0.5em]",
    border: "[0.25em solid white]",
    borderColor: "sprite-motion-gb.53",
    width: "[100%]",
    height: "[100%]",
    paddingTop: "[1em]",
    paddingBottom: "[0.5em]",
    paddingLeft: "[1rem]",
    paddingRight: "[1rem]",
    backgroundColor: "sprite-motion-gb.50",
  },
});

const Title = styled(m.h1, {
  base: {
    position: "absolute",
    background: "sprite-motion-gb.50",
    padding: "[0]",
    margin: "[0]",
    top: "[0]",
    zIndex: "[1]",
    fontSize: "[inherit]",
    textTransform: "uppercase",
  },
});

/**
 * @deprecated This is internal don't use it
 * @internal don't use this
 */
export const UiLine = (props: {
  children: string;
  speed: number;
  onDone: () => void;
}) => {
  const { children: line, speed, onDone } = props;
  const [index, _setIndex] = useState(1);
  const setIndex = useGuardedCallback(_setIndex);

  useInterval(() => {
    const newIndex = Math.min(index + 1, line.length);
    setIndex(newIndex);
    if (newIndex === line.length) {
      onDone();
      return "abort";
    }
  }, speed);

  const text = line.substring(0, index);

  return (
    <pre style={{ fontFamily: "inherit", margin: 0, minHeight: "1em" }}>
      {text}
    </pre>
  );
};

/**
 * @deprecated This is internal don't use it
 * @internal don't use this
 */
export const UiLines = (props: {
  children: string | string[];
  speed: number;
  onDone: () => void;
}) => {
  const { onDone, children } = props;
  const childrenAsArray = Array.isArray(children) ? children : [children];
  const [index, _setIndex] = useState(1);
  const setIndex = useGuardedCallback(_setIndex);

  const reveal = () => {
    setIndex(index + 1);
    if (index === childrenAsArray.length) onDone();
  };

  const lines = childrenAsArray.slice(0, index);

  return (
    <>
      {lines.map((line) => (
        <UiLine key={line} speed={props.speed} onDone={reveal}>
          {line}
        </UiLine>
      ))}
    </>
  );
};

/**
 * Customization:
 *
 * - Set font-family on top-most component.
 * - Set bg on top-most component, immediate child DIV.
 * - Set color on top-most component, immediate child DIV.
 * - Set border color on immediate child DIV.
 *
 * Note: the difference between this one and the non-reactive version is that
 * when reactive=true, change the props and it will completely re-reinitialize
 * the component.
 */
export const UiDialogPanel = (
  props: Omit<Parameters<typeof UiPanelOuter>[0], "children"> & {
    title?: string;
    children: string | string[];
    wordSpeed?: number;
    lineSpeed?: number;
    visibleLines: number;
    onDone?: () => void;
  }
) => {
  const {
    children,
    title,
    wordSpeed = 50,
    lineSpeed = 1000,
    visibleLines,
    onDone,
    ...rest
  } = props;

  const lines = Array.isArray(children)
    ? children.flatMap((line) => line.split("\n"))
    : children.trim().split("\n");

  const [index, _setIndex] = React.useState(0);
  const setIndex = useGuardedCallback(_setIndex);
  const timeout = React.useRef<ReturnType<typeof setTimeout>>();

  const next = () => {
    timeout.current = setTimeout(() => {
      const newIndex = index + visibleLines;
      if (newIndex > lines.length - 1) {
        onDone?.();
      } else {
        setIndex(newIndex);
      }
    }, lineSpeed);
  };

  const text = lines.slice(index, index + visibleLines);
  const key = JSON.stringify(text);

  return (
    <UiPanelOuter {...rest}>
      {title && <Title>{title}</Title>}
      <UiPanelInner alignItems="start" justifyContent="start">
        <UiLines key={key} speed={wordSpeed} onDone={next}>
          {text}
        </UiLines>
      </UiPanelInner>
    </UiPanelOuter>
  );
};

/**
 * Assumes the lines fit in the panel.
 *
 * Customization:
 *
 * - Set font-family on top-most component.
 * - Set bg on top-most component, immediate child DIV.
 * - Set color on top-most component, immediate child DIV.
 * - Set border color on immediate child DIV.
 */
export const UiPanel = (
  props: Omit<Parameters<typeof UiPanelOuter>[0], "children"> & {
    title?: string;
    children: React.ReactNode;
  }
) => {
  const { children, title, ...rest } = props;

  return (
    <UiPanelOuter {...rest}>
      {title && <Title>{title}</Title>}
      <UiPanelInner>{children}</UiPanelInner>
    </UiPanelOuter>
  );
};

const KeyValueContainer = styled(m.div, {
  base: {
    columnCount: 2,
  },
});

export const UiKeyValue = (props: {
  items: Array<{ key: string; value: string }>;
}) => {
  return (
    <>
      {props.items.map((item) => (
        <KeyValueContainer key={`${item.key}`}>
          <div style={{ textTransform: "uppercase" }}>{item.key}</div>
          <div>{item.value}</div>
        </KeyValueContainer>
      ))}
    </>
  );
};

export const UiPlayerInfoPanel = (
  props: Parameters<typeof UiPanelOuter>[0] & {
    info: Array<{ key: string; value: string }>;
  }
) => {
  const { info, ...rest } = props;

  return (
    <UiPanel {...rest}>
      <div
        style={{ display: "flex", position: "absolute", top: 0, gap: "2.5em" }}
      >
        {info.map((data) => (
          <div
            key={JSON.stringify(data)}
            className={css({
              background: "sprite-motion-gb.50",
              textAlign: "right",
            })}
          >
            <div>{data.key}</div>
            <div>{data.value}</div>
          </div>
        ))}
      </div>
      <div style={{ height: "0.75em" }}></div>
    </UiPanel>
  );
};
