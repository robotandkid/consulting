import React, { useState } from "react";
import { useCanvasRegistration, useKeyRegistration } from "../canvas/canvas";
import { CanvasSpeechBubble } from "../canvas/speech-bubble";
import { usePanelCanvasCtx2D } from "../storyboard";
import { sleep } from "../utils/timeouts";
import { useCommandKeyListeners } from "./ui-command-key-listeners";

const selectionMarker = "▶ ";

type TextFont =
  | `${number}px ${string}`
  | `${"bold" | "italic"} ${number}px ${string}`;

interface Location {
  left: number;
  top: number;
}

interface Dimensions {
  width: number;
  height: number;
}

interface ContainerOpts {
  radius?: number;
  textOffsetX: number;
  textOffsetY: number;
  speed?: number;
  useExtendedBgFill?: number;
}

interface TextStyles {
  lineHeightAsPx: number;
  font: TextFont;
  baseline?: CanvasRenderingContext2D["textBaseline"];
  fillStyle: `#${string}`;
}

interface ContainerStyles {
  strokeColor?: `#${string}`;
  strokeWidth?: number;
  fillColor?: `#${string}`;
}

const StackedCommandHitAreas = (props: {
  selectionMarker: string;
  commands: string[];
  pos: Location & {
    width: number;
  };
  textStyles: TextStyles;
  onClick: (value: string) => void;
}) => {
  const { pos, commands, textStyles } = props;
  const fontSize = +textStyles.font.replace(/.*?([0-9]+).*/, "$1");
  const fontFamily = textStyles.font.replace(/.*[0-9]+px(.*)/, "$1");
  const selectionMarker = props.selectionMarker.replace(/\s/g, "&nbsp;");

  return (
    <ol
      style={{
        position: "absolute",
        top: pos.top,
        left: pos.left,
        width: pos.width,
        fontSize,
        fontFamily,
        zIndex: 999,
        opacity: 0,
        lineHeight: 1.3,
        color: "white",
        background: "green",
        cursor: `url('/images/cursor-transparent.png'), crosshair`,
      }}
    >
      {commands.map((cmd) => (
        <li key={cmd} onClick={() => props.onClick(cmd)}>
          {selectionMarker}
          {cmd}
        </li>
      ))}
    </ol>
  );
};

const StackedCommands = (props: {
  pos: Location;
  textStyles: Required<TextStyles>;
  commands: string[];
  selectionMarker: string;
  selected: string;
}) => {
  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`stacked-commands`, props);

  if (keyChanged && render && ctx) {
    render(() => {
      const { textStyles, commands, selectionMarker, selected, pos } = props;

      ctx.save();
      ctx.font = textStyles.font;
      ctx.textBaseline = textStyles.baseline;
      ctx.fillStyle = textStyles.fillStyle;

      const selectorWidth = ctx.measureText(selectionMarker).width;

      for (let line = 0; line < commands.length; line += 1) {
        if (commands[line] === selected)
          ctx.fillText(
            selectionMarker,
            pos.left,
            pos.top + line * textStyles.lineHeightAsPx
          );
        ctx.fillText(
          commands[line]!,
          pos.left + selectorWidth,
          pos.top + line * textStyles.lineHeightAsPx
        );
      }

      ctx.restore();

      return key;
    });
  }

  return null;
};

export const UIDialogCommands = (props: {
  commands: string[];
  selectionMarker?: string;
  pos: Location & Dimensions & ContainerOpts;
  containerStyles: ContainerStyles;
  textStyles: TextStyles & {
    glitch?: boolean;
  };
  onChange?: (newValue: string, prevValue: string) => void;
  onSelect?: (value: string) => void;
  onCancel?: () => void;
}) => {
  const {
    commands,
    pos: {
      height,
      left,
      top,
      width,
      radius = 5,
      textOffsetX,
      textOffsetY,
      speed,
      useExtendedBgFill,
    },
    containerStyles,
    textStyles: {
      fillStyle,
      font,
      lineHeightAsPx,
      baseline = "top",
      glitch = false,
    },
    onChange,
    onSelect,
    onCancel,
  } = props;

  const selected = useCommandKeyListeners({
    commands,
    onChange,
    onSelect,
    onCancel,
  });

  const render = useCanvasRegistration();
  const ctx = usePanelCanvasCtx2D();
  const [keyChanged, key] = useKeyRegistration(`ui-dialog-commands`, props);

  if (useExtendedBgFill && keyChanged && render && ctx) {
    render(() => {
      ctx.fillStyle = containerStyles.fillColor ?? "black";
      ctx.fillRect(
        left - useExtendedBgFill,
        top - useExtendedBgFill,
        width + 2 * useExtendedBgFill,
        height + 2 * useExtendedBgFill
      );
      return key;
    });
  }

  return (
    <>
      <CanvasSpeechBubble
        pos={{
          height,
          left,
          top,
          width,
          pLeft: left + width / 2,
          pTop: top,
          radius,
        }}
        text={{ rx: textOffsetX, ry: textOffsetY, speed }}
        style={containerStyles}
        textStyle={{
          fillStyle,
          font,
          lineHeightAsPx,
          textBaseline: baseline,
          glitch,
        }}
        autoTrim={false}
      />
      <StackedCommands
        commands={commands}
        pos={{
          left: left + textOffsetX,
          top: top + textOffsetY,
        }}
        selectionMarker={selectionMarker}
        selected={selected}
        textStyles={{
          fillStyle,
          font,
          lineHeightAsPx,
          baseline,
        }}
      />
      <StackedCommandHitAreas
        commands={commands}
        pos={{
          left: left + textOffsetX,
          top: top + textOffsetY,
          width,
        }}
        textStyles={{ fillStyle, font, lineHeightAsPx, baseline }}
        selectionMarker={selectionMarker}
        onClick={(value: string) => {
          props.onChange?.(value, selected);
          props.onSelect?.(value);
        }}
      />
    </>
  );
};

export const UiDialog = (props: {
  children: string;
  dialogPos: Location & Dimensions & ContainerOpts;
  textStyles: TextStyles & { glitch?: boolean };
  containerStyles: ContainerStyles;
  onDone?: () => void;
}) => {
  const { dialogPos, textStyles, containerStyles, onDone } = props;

  return (
    <CanvasSpeechBubble
      pos={{
        ...dialogPos,
        pLeft: dialogPos.left,
        pTop: dialogPos.top,
        radius: dialogPos.radius ?? 5,
      }}
      text={{
        rx: dialogPos.textOffsetX,
        ry: dialogPos.textOffsetY,
        speed: dialogPos.speed,
      }}
      style={{
        fillColor: containerStyles?.fillColor,
        strokeColor: containerStyles.strokeColor,
        strokeWidth: containerStyles.strokeWidth,
      }}
      textStyle={{
        fillStyle: textStyles.fillStyle,
        font: textStyles.font,
        glitch: textStyles.glitch,
        lineHeightAsPx: textStyles.lineHeightAsPx,
        textBaseline: textStyles.baseline,
      }}
      onDone={onDone}
    >
      {props.children}
    </CanvasSpeechBubble>
  );
};

export const UiDialogWithCommands = <Command extends string>(props: {
  children: string;
  selectionMarker?: string;
  dialogPos: Location & Dimensions & ContainerOpts;
  commandsDelay?: number;
  commands: Command[];
  commandPos: Location & Dimensions & ContainerOpts;
  textStyles: TextStyles & { glitch?: boolean };
  containerStyles: ContainerStyles;
  onChange?: (newValue: Command, prevValue: Command) => void;
  onSelect?: (value: Command) => void;
  onCancel?: () => void;
}) => {
  const {
    selectionMarker: _selectionMarker = selectionMarker,
    dialogPos,
    commandPos,
    commands,
    textStyles,
    containerStyles,
    commandsDelay = 0,
  } = props;
  const [done, setDone] = useState(false);

  const textLineHeightAsPx = 1.5 * textStyles.lineHeightAsPx;

  return (
    <>
      <CanvasSpeechBubble
        pos={{
          ...dialogPos,
          pLeft: dialogPos.left,
          pTop: dialogPos.top,
          radius: dialogPos.radius ?? 5,
        }}
        text={{
          rx: dialogPos.textOffsetX,
          ry: dialogPos.textOffsetY,
          speed: dialogPos.speed,
        }}
        style={{
          fillColor: containerStyles?.fillColor,
          strokeColor: containerStyles.strokeColor,
          strokeWidth: containerStyles.strokeWidth,
        }}
        textStyle={{
          fillStyle: textStyles.fillStyle,
          font: textStyles.font,
          glitch: textStyles.glitch,
          lineHeightAsPx: textStyles.lineHeightAsPx,
          textBaseline: textStyles.baseline,
        }}
        onDone={async () => {
          await sleep(commandsDelay);
          setDone(true);
        }}
      >
        {props.children}
      </CanvasSpeechBubble>
      {done && (
        <UIDialogCommands
          selectionMarker={_selectionMarker}
          pos={commandPos}
          commands={commands}
          containerStyles={containerStyles}
          textStyles={{ ...textStyles, lineHeightAsPx: textLineHeightAsPx }}
          onCancel={props.onCancel}
          onChange={(value, prev) =>
            props.onChange?.(value as Command, prev as Command)
          }
          onSelect={(value) => props.onSelect?.(value as Command)}
        />
      )}
    </>
  );
};
