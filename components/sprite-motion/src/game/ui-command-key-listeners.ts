import React from "react";
import { useWrappedArray } from "../utils/arrays";

export const useCommandKeyListeners = <Command extends string>(props: {
  commands: Readonly<Command[]>;
  onChange?: (newValue: Command, prevValue: Command) => void;
  onSelect?: (value: Command) => void;
  onCancel?: () => void;
  disabled?: boolean;
}) => {
  const { commands, onChange, onSelect, onCancel, disabled } = props;

  const [prev, cur, next, goNext, goPrev] = useWrappedArray(commands);

  // This is just the standard pattern to guard against bad memoization
  const ref = React.useRef({
    onChange,
    onSelect,
    goNext,
    goPrev,
    onCancel,
  });
  ref.current = { onChange, onSelect: onSelect, goNext, goPrev, onCancel };

  React.useEffect(() => {
    if (disabled) return;

    const prevKey = ["ArrowLeft", "ArrowUp"];
    const nextKey = ["ArrowRight", "ArrowDown"];
    const selectKey = ["Enter", "Space"];
    const cancelKey = ["Esc"];

    const keys = (ev: KeyboardEvent) => {
      if (prevKey.includes(ev.key)) {
        ref.current.onChange?.(next, cur);
        ref.current.goPrev();
      } else if (nextKey.includes(ev.key)) {
        ref.current.onChange?.(prev, cur);
        ref.current.goNext();
      } else if (selectKey.includes(ev.key) && cur) {
        ref.current.onSelect?.(cur);
        window.removeEventListener("keydown", keys);
      } else if (cancelKey.includes(ev.key)) {
        ref.current.onCancel?.();
        window.removeEventListener("keydown", keys);
      }
    };

    window.addEventListener("keydown", keys);
    return () => {
      window.removeEventListener("keydown", keys);
    };
  }, [next, prev, cur, disabled]);

  return cur;
};
