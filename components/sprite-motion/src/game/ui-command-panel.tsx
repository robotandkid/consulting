import { styled } from "@styled-system/jsx/index";
import { m } from "framer-motion";
import React from "react";
import { sleep } from "../utils/timeouts";
import { UiDialogPanel, UiPanel, UiPanelOuter } from "./ui";
import { useCommandKeyListeners } from "./ui-command-key-listeners";

const CurrentCommandInner = styled(m.li, {
  base: {
    listStyleType: "none",
    margin: "[0]",
    padding: "[0]",
  },
  variants: {
    selected: {
      yes: {
        _before: {
          content: '"▶"',
          color: "[inherit]",
          position: "absolute",
          left: "[-1em]",
          lineHeight: "[1]",
        },
      },
    },
  },
});

const CurrentCommandInnerContainer = styled(m.a, {
  base: {
    cursor: "url('/images/cursor-transparent.png'), auto",
    position: "relative",
    display: "block",
  },
});

const CurrentCommand = <Command extends string>(
  props: Omit<
    Parameters<typeof CurrentCommandInner>[0],
    "children" | "onClick"
  > & {
    children: Command;
    onClick: () => void;
  }
) => {
  const { children, onClick, ...rest } = props;
  return (
    <CurrentCommandInnerContainer
      style={{ position: "relative", display: "block" }}
      onClick={onClick}
    >
      <CurrentCommandInner {...rest}>{children}</CurrentCommandInner>
    </CurrentCommandInnerContainer>
  );
};

type SubCommandsStyleProps = Omit<
  Parameters<typeof UiPanel>[0],
  "onCancel" | "onChange" | "onDone" | "children" | "onSelect"
>;

const SubCommands = <SubCommand extends string>(
  props: {
    commands: Readonly<SubCommand[]>;
    onCancel: () => void;
    onChange?: (newValue: SubCommand, prevValue: SubCommand) => void;
    onSelect: (value: SubCommand) => void;
  } & SubCommandsStyleProps
) => {
  const { commands, onChange, onSelect, onCancel, ...rest } = props;

  const curCommand = useCommandKeyListeners({
    commands,
    onChange,
    onSelect,
    onCancel,
  });

  return (
    <UiPanel {...rest}>
      <ul
        style={{
          display: "flex",
          flexDirection: "column",
          gap: "0.75em",
          alignItems: "flex-start",
          alignSelf: "flex-start",
          paddingLeft: "1.5em",
          paddingRight: "1.5em",
        }}
      >
        {commands.map((command) => (
          <CurrentCommand
            key={command}
            selected={curCommand === command ? "yes" : undefined}
            onClick={() => onSelect(command)}
          >
            {command}
          </CurrentCommand>
        ))}
      </ul>
    </UiPanel>
  );
};

const SubCommandsContainer = <SubCommand extends string>(props: {
  items:
    | { type: "cmd"; commands: Readonly<SubCommand[]> }
    | { type: "dialog"; text: string }
    | undefined;
  onChange?: (newValue: SubCommand, prevValue: SubCommand) => void;
  onCancel: () => void;
  onSelect: (value: SubCommand | undefined) => void;
}) => {
  const { items, ...rest } = props;

  if (items?.type === "cmd")
    return (
      <SubCommands
        top="[3em]"
        left="[2em]"
        minWidth="[5em]"
        minHeight="[5em]"
        {...rest}
        position="absolute"
        commands={items.commands}
      />
    );

  return (
    <div
      style={{
        position: "absolute",
        minWidth: "20rem",
        minHeight: "5rem",
        top: "5rem",
        left: "5rem",
      }}
    >
      <UiDialogPanel
        width="[100%]"
        height="[100%]"
        onDone={async () => {
          await sleep(1000);
          return props.onSelect(undefined);
        }}
        visibleLines={3}
      >
        {items?.text ?? ""}
      </UiDialogPanel>
    </div>
  );
};

export const UiCommandPanel = <
  Command extends string,
  SubCommand extends string
>(
  props: Parameters<typeof UiPanelOuter>[0] & {
    title?: string;
    commands: Command[];
    subCommands: {
      [cmd in Command]:
        | { type: "cmd"; commands: Readonly<SubCommand[]> }
        | { type: "dialog"; text: string }
        | undefined;
    };
    onCommandChange?: (newValue: Command, prevValue: Command) => void;
    onSubCommandChange?: (newValue: SubCommand, prevValue: SubCommand) => void;
    onDone: (command: Command, subCommand: SubCommand | undefined) => void;
  }
) => {
  const {
    title = "command",
    commands,
    onCommandChange,
    onDone,
    subCommands,
    onSubCommandChange,
    ...rest
  } = props;

  const [selectedCommand, setSelectedCommand] = React.useState<
    Command | undefined
  >();

  const curCommand = useCommandKeyListeners({
    commands,
    onChange: onCommandChange,
    onSelect: setSelectedCommand,
    disabled: !!selectedCommand,
  });

  return (
    <>
      <UiPanel {...rest} title={title} overflow="visible">
        <div
          style={{
            columnCount: 2,
            columnWidth: "3.5em",
          }}
        >
          {commands.map((command) => (
            <CurrentCommand
              key={command}
              selected={curCommand === command ? "yes" : undefined}
              onClick={() => setSelectedCommand(command)}
            >
              {command}
            </CurrentCommand>
          ))}
        </div>
        {selectedCommand && (
          <SubCommandsContainer
            items={subCommands[selectedCommand]}
            onChange={onSubCommandChange}
            onCancel={() => setSelectedCommand(undefined)}
            onSelect={(value) => {
              setSelectedCommand(undefined);
              onDone?.(selectedCommand, value);
            }}
          />
        )}
      </UiPanel>
    </>
  );
};
