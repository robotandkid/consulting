import React, { useEffect, useState } from "react";
import {
  UiDialogPanel,
  UiKeyValue,
  UiLine,
  UiLines,
  UiPanel,
  UiPlayerInfoPanel,
} from "./ui";

export const UiPanelExample = () => {
  return (
    <UiPanel title="dnka" width="[200px]" height="[100px]">
      LV 1
    </UiPanel>
  );
};

export const UiTwoColumnExample = () => {
  return (
    <UiPanel title="dnka" width="[200px]" height="[100px]">
      <UiKeyValue
        items={[
          { key: "LV", value: "1" },
          { key: "HP", value: "80" },
        ]}
      />
    </UiPanel>
  );
};

export const UiLineExample = () => {
  const [done, setDone] = useState(false);
  return (
    <div>
      <UiLine speed={100} onDone={() => setDone(true)}>
        hello world! how are you doing?
      </UiLine>
      {done && <div>I&apos;m done</div>}
    </div>
  );
};

export const UiLinesExample = () => {
  const [done, setDone] = useState(false);
  return (
    <div>
      <UiLines speed={100} onDone={() => setDone(true)}>
        {`line 1: hello
line 2: world`}
      </UiLines>
      {done && <div>I&apos;m done</div>}
    </div>
  );
};

export const UiLinesDynamicStateExample = () => {
  const [done, setDone] = useState(false);
  const [text, setText] = useState(["line 1: hello"]);

  useEffect(() => {
    let i = 0;
    const int = setInterval(() => {
      setText((text) =>
        text.concat(
          `${i++}${Array.from({ length: 10 })
            .map(() => "x")
            .join("")}`
        )
      );
    }, 3000);
    return () => {
      clearInterval(int);
    };
  }, []);

  return (
    <div>
      <UiLines speed={100} onDone={() => setDone(true)}>
        {text}
      </UiLines>
      {done && <div>I&apos;m done</div>}
    </div>
  );
};

export const UiDialogExample = () => {
  const [done, setDone] = useState(false);
  return (
    <>
      <UiDialogPanel
        width="[500px]"
        height="[20rem]"
        visibleLines={1}
        wordSpeed={100}
        lineSpeed={500}
        onDone={() => setDone(true)}
      >
        {`A Drakke draws near!
Command?`}
      </UiDialogPanel>
      {done && <div>done</div>}
    </>
  );
};

export const UiDialogLongExample = () => {
  const [done, setDone] = useState(false);
  return (
    <>
      <UiDialogPanel
        width="[500px]"
        height="[20rem]"
        visibleLines={3}
        wordSpeed={100}
        lineSpeed={500}
        onDone={() => setDone(true)}
      >
        {`Turmoil has engulfed the Galactic Republic.
The taxation of trade routes to 
outlying star system is in dispute.
Hoping to resolve the matter with a 
blockade of deadly battleships,
the greedy Trade Federation has stopped
all shipping to the small planet of Naboo`}
      </UiDialogPanel>
      {done && <div>done</div>}
    </>
  );
};

export const UiDialogPanelLongExample = () => {
  const [done, setDone] = useState(0);
  const [text] = useState([
    `Hello doctor.
We'd like you to recover some very
expensive equipment in the Amazon,
and complete the research.
Everything will be explained if 
you choose to accept.`,
  ]);

  return (
    <div>
      <UiDialogPanel
        width="[500px]"
        height="[6rem]"
        visibleLines={2}
        wordSpeed={100}
        lineSpeed={500}
        onDone={() => setDone((c) => c + 1)}
      >
        {text}
      </UiDialogPanel>
      {done && <div>I&apos;m done: {done}</div>}
    </div>
  );
};

export const UiPlayerInfoExample = () => {
  return (
    <UiPlayerInfoPanel
      info={[
        { key: "Name", value: "DNKA" },
        { key: "Lvl", value: "1" },
        { key: "HP", value: "80" },
        { key: "Exp", value: "1" },
      ]}
    />
  );
};

export const WeirdDialogExample = () => {
  const [done, setDone] = useState(false);
  return (
    <>
      <UiDialogPanel
        visibleLines={3}
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore 🤷
        width="[30rem]"
        height="[8rem]"
        zIndex={2}
        position="absolute"
        bottom="[5rem]"
        onDone={() => setDone(true)}
      >
        {`* You can't leave your bed.
* If you need to use the bathroom,
  use this portable urinal.`}
      </UiDialogPanel>
      {done && <div>done</div>}
    </>
  );
};

export const BlankLineDialogExample = () => {
  const [done, setDone] = useState(false);
  return (
    <>
      <UiDialogPanel
        visibleLines={3}
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore 🤷
        width="[30rem]"
        height="[8rem]"
        zIndex={2}
        position="absolute"
        bottom="[5rem]"
        onDone={() => setDone(true)}
      >
        {`* Oops

Robot loses a point.`}
      </UiDialogPanel>
      {done && <div>done</div>}
    </>
  );
};
