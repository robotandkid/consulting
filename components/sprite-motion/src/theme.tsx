import { definePreset } from "@pandacss/dev";

export const themePreset = definePreset({
  theme: {
    tokens: {
      colors: {
        black: { 100: { value: "black" } },
        customGray: { 100: { value: "#a9a9a9" } },
        dark: { value: "#081820" },
        "sprite-motion-gb": {
          50: { value: "#081820" },
          51: { value: "#346856" },
          52: { value: "#88c070" },
          53: { value: "#e0f8d0" },
        },
      },
      fonts: {
        "sprite-motion": {
          value: "mrpixel, 'var(--font-mrpixel)', monospace",
        },
      },
    },
  },
});
