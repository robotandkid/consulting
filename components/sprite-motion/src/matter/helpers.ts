import { Engine } from "matter-js";

export const matterBodyByLabel = (engine: Engine | undefined, label: string) =>
  engine?.world.bodies.find((bod) => bod.label === label);

// export const matterDisableBodyByLabel = (engine: Engine | undefined, label: string) => {
//   const body = matterBodyByLabel(engine, label);
//   if (body) Sleeping.set(body, true);
// };

// // eslint-disable-next-line @typescript-eslint/no-unused-vars
// const enableBodyByLabel = (engine: Engine | undefined, label: string) => {
//   const body = bodyByLabel(engine, label);
//   if (body) Sleeping.set(body, false);
// };
