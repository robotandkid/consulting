import {
  MotionDiv,
  Joint,
  MotionImg,
  StaticDiv,
  World,
  useEngineContext,
} from "./sprite-motion";
import React, { useEffect, useState } from "react";
import { Bodies, Composite, Sleeping } from "matter-js";
import { PrintKeys } from "./utils/debug";
import { useResources } from "./resources";

const MatterJsExplorationFixtureInner = () => {
  const engine = useEngineContext();
  const [rect] = useState(Bodies.rectangle(0, 0, 25, 25, { isStatic: true }));
  const [, update] = useState(0);

  useEffect(() => {
    if (!engine) return;
    Composite.add(engine.world, rect);
    update((c) => c + 1);
  }, [engine, rect]);

  return (
    <section>
      <h1>Rectangle Props</h1>
      <div>dimensions: 25x25</div>
      <PrintKeys obj={rect} keys={["position", "bounds"]} />
      <div>
        vertices:{" "}
        {JSON.stringify(rect.vertices.map((v) => ({ x: v.x, y: v.y })))}
      </div>
    </section>
  );
};

export const MatterJsExplorationFixture = () => {
  return (
    <World>
      <MatterJsExplorationFixtureInner />
    </World>
  );
};

const FixedBackground = (props: { width?: number; height?: number }) => (
  <div
    style={{
      width: props.width ?? 500,
      height: props.height ?? 500,
      background: "gray",
      opacity: 0.5,
    }}
  />
);

export const StaticDivFixture = () => {
  return (
    <World>
      <FixedBackground />
      <div>These should line up with the gray rectangle</div>
      <StaticDiv
        aria-label="top wall"
        cx={250}
        cy={0}
        width={500}
        height={5}
        style={{ background: "green" }}
      />
      <StaticDiv
        aria-label="left w all"
        cx={0}
        cy={250}
        width={5}
        height={500}
        style={{ background: "red" }}
      />
    </World>
  );
};

export const StaticDivWithRotationFixture = () => {
  return (
    <World>
      <FixedBackground />
      <StaticDiv
        cx={250}
        cy={250}
        width={700}
        height={5}
        angle={45}
        style={{ background: "pink" }}
      />
      <MotionDiv
        startCx={50}
        startCy={0}
        width={25}
        height={25}
        style={{
          background: "green",
        }}
      />
    </World>
  );
};

export const StaticDivAsWallFixture = () => {
  return (
    <World>
      <div
        style={{
          width: 1000,
          height: 500,
          background: "gray",
          opacity: 0.5,
        }}
      />
      <StaticDiv
        aria-label="bottom wall"
        cx={500}
        cy={500}
        width={1000}
        height={5}
        style={{ background: "green" }}
      />
      <MotionDiv
        startCx={0}
        startCy={0}
        vx={8}
        vy={-8}
        width={256}
        height={256}
        style={{ background: "pink" }}
      />
    </World>
  );
};

export const StaticDivWithCollisionCallbackFixture = () => {
  const [color, setColor] = useState("green");

  return (
    <World>
      <div
        style={{
          width: 1000,
          height: 500,
          background: "gray",
          opacity: 0.5,
        }}
      />
      <StaticDiv
        aria-label="bottom wall"
        cx={500}
        cy={500}
        width={1000}
        height={5}
        style={{ background: color }}
        onCollisionStart={(_, otherBody) => {
          setColor("red");
          Sleeping.set(otherBody, true);
        }}
      />
      <MotionDiv
        startCx={0}
        startCy={0}
        vx={8}
        vy={-8}
        width={256}
        height={256}
        style={{ background: "pink" }}
      />
    </World>
  );
};

export const MotionImgFixture = () => {
  return (
    <World>
      <StaticDiv
        aria-label="right wall"
        cx={1000}
        cy={250}
        width={5}
        height={500}
        style={{ background: "pink" }}
      />
      <StaticDiv
        aria-label="bottom wall"
        cx={500}
        cy={500}
        width={1000}
        height={5}
        style={{ background: "green" }}
      />
      <MotionImg
        startLeft={0}
        startTop={0}
        vx={10}
        vy={-10}
        src="./eli-02.png"
        width={256}
        height={256}
      />
    </World>
  );
};

const random = (scale: number) =>
  Math.random() * scale * (Math.random() > 0.5 ? -1 : 1);

type MotionDivProps = Parameters<typeof MotionDiv>[0];

const rightEye: Pick<MotionDivProps, "startCx" | "startCy"> = {
  startCx: 125,
  startCy: 125,
};
const leftEye: Pick<MotionDivProps, "startCx" | "startCy"> = {
  startCx: 55,
  startCy: 125,
};

const TearsInTheRainInner = () => {
  return (
    <World>
      <img src="./tears-in-the-rain.png" />
      {Array.from({ length: 100 }).map((_, index) => {
        const eye = index % 2 ? rightEye : leftEye;
        return (
          <MotionDiv
            key={Math.random()}
            height={5}
            width={5}
            {...eye}
            vx={random(3)}
            vy={random(3)}
            style={{
              borderRadius: "5px",
              borderColor: "blue",
              background: "blue",
            }}
          />
        );
      })}
    </World>
  );
};

export const ExampleTearsInTheRain = () => {
  const [key, setKey] = useState(Math.random());

  useEffect(() => {
    const tears = () => setKey(Math.random());
    const unsubscribe = setInterval(tears, 800);
    return () => {
      clearInterval(unsubscribe);
    };
  });

  return <TearsInTheRainInner key={key} />;
};

export const JointFixture = () => {
  return (
    <World>
      <Joint
        startLeft={0}
        startTop={0}
        hingeX="0%"
        hingeY="50%"
        width={100}
        height={20}
        style={{
          background: "green",
        }}
      />
      {/** A red dot at (0,0) */}
      <div
        style={{
          position: "absolute",
          background: "red",
          height: "3px",
          width: "3px",
        }}
      />
    </World>
  );
};

export const InverseGravityFixture = () => {
  return (
    <World
      onInit={(engine) => {
        engine.gravity.x = 0;
        engine.gravity.y = -1;
      }}
    >
      <MotionDiv
        startCx={0}
        startCy={500}
        width={25}
        height={25}
        style={{
          background: "green",
        }}
      />
    </World>
  );
};

export const ImageAssetsExample = () => {
  const resources = useResources({
    image1: {
      type: "image",
      url: "./eli-02.png",
    },
  });

  if (resources === "loading") return "loading";

  return (
    <World>
      <MotionImg
        startLeft={0}
        startTop={0}
        vx={10}
        vy={-10}
        src={resources.downloads?.image1}
        width={256}
        height={256}
      />
      <StaticDiv
        aria-label="bottom wall"
        cx={500}
        cy={500}
        width={2000}
        height={5}
        style={{ background: "green" }}
      />
    </World>
  );
};

export const SoundAssetsExample = () => {
  const resources = useResources({
    image1: {
      type: "image",
      url: "./eli-02.png",
    },
    sound1: {
      type: "sound",
      url: "./mom.mp3",
    },
  });

  const [show, setShow] = useState(false);
  const canShow = show && resources !== "loading";

  return (
    <>
      {resources === "loading" && <div>Loading...</div>}
      {!canShow && <button onClick={() => setShow(true)}>Click to play</button>}
      {canShow && (
        <World>
          <MotionImg
            startLeft={0}
            startTop={0}
            vx={10}
            vy={-10}
            src={resources.downloads?.image1}
            width={256}
            height={256}
          />
          <StaticDiv
            aria-label="bottom wall"
            cx={500}
            cy={500}
            width={2000}
            height={5}
            style={{ background: "green" }}
            onCollisionStart={() =>
              resources.downloads?.sound1.resource?.play()
            }
          />
        </World>
      )}
    </>
  );
};
