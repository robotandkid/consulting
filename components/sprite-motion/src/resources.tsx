import { useEffect, useRef, useState } from "react";
import { makeReadOnlyContext } from "./utils/context";

const [InternalAbortCtrlProviderInner, useAbortCtrlCtx] =
  makeReadOnlyContext<AbortController>("AbortCtrl");

export {
  /** @internal don't use this */
  InternalAbortCtrlProviderInner,
  useAbortCtrlCtx,
};

export type MetaSupportedImg = "png" | "jpg" | "jpeg" | "gif";
export type MetaSupportedSound = "ogg" | "mp3" | "wav";
export type MetaSupportedVideo = "mp4" | "mov";
export type MetaSupportedText = "txt" | "tsx" | "jsx" | "ts" | "js" | "md";

export type MetaResource =
  | {
      type: "image";
      url: `${string}.${MetaSupportedImg}`;
    }
  | {
      type: "sound";
      url: `${string}.${MetaSupportedSound}`;
    }
  | { type: "video"; url: `${string}.${MetaSupportedVideo}` }
  | {
      type: "text";
      url: `${string}.${MetaSupportedText}`;
    };

type MetaResourceImg = Extract<MetaResource, { type: "image" }>;
type MetaResourceSound = Extract<MetaResource, { type: "sound" }>;
type MetaResourceVideo = Extract<MetaResource, { type: "video" }>;
type MetaResourceText = Extract<MetaResource, { type: "text" }>;

export interface ResourceImg extends MetaResourceImg {
  resource: Promise<string | undefined>;
}

export interface ResourceVideo extends MetaResourceVideo {
  resource: Promise<string | undefined>;
  video: HTMLVideoElement;
}

export interface ResourceText extends MetaResourceText {
  resource: Promise<string | undefined>;
}

export interface ResourceSound extends MetaResourceSound {
  resource: {
    /**
     * **Big Note**: you have to call play as a result of user interaction. For
     * the most part, you have to gate the media element behind a click. Think
     * youtube videos that you have to hit the volume to get sound.
     */
    play(opts?: { loop?: boolean }): void;
    stop(): void;
  };
}

type Resources<MetaResources extends Record<string, MetaResource>> = {
  [name in keyof MetaResources]: MetaResources[name] extends MetaResourceImg
    ? ResourceImg
    : MetaResources[name] extends MetaResourceVideo
    ? ResourceVideo
    : MetaResources[name] extends MetaResourceText
    ? ResourceText
    : ResourceSound;
};

/** Returns a method that lazy loads an AudioContext */
const getAudioContextFactory = () => {
  let _ctx: AudioContext | undefined;

  return () => ({
    ctx() {
      _ctx =
        _ctx ?? typeof AudioContext !== "undefined"
          ? new AudioContext()
          : undefined;
      return _ctx;
    },
    cleanup() {
      if (_ctx?.state === "running") _ctx.close();
      _ctx = undefined;
    },
  });
};

const cloneBufferSource = (
  original: AudioBufferSourceNode,
  ctx: AudioContext
) => {
  const source = ctx.createBufferSource();
  source.buffer = original.buffer;
  source.loop = original.loop;
  source.connect(ctx.destination);

  return source;
};

const fetchBlobOrNothing = <Result,>(
  url: string,
  signal: AbortController["signal"] | undefined,
  cb: (response: Response) => Promise<Result>
) => {
  if (typeof window === "undefined") return Promise.resolve(undefined);
  return fetch(url, {
    signal,
  }).then(async (result) => (result.ok ? cb(result) : undefined));
};

/**
 * Complexity:
 *
 * - The async needs to happen in the edges. Here's what I mean---given the
 *   resource:
 *
 *       sound1: {
 *         type: "sound",
 *         url: "./mom.mp3",
 *       }
 *
 *   - It needs to return a resource of type
 *
 * Sound1: { type: "sound"; url: string; resource: Promise<unknown>; }
 *
 *     - That way callers don't have to resolve anything.
 *     - The other complexity is the audio context, this needs to be
 *       killed/restarted
 */
const getResources = async <MetaResources extends Record<string, MetaResource>>(
  metaResources: MetaResources,
  opts: { signal: AbortController["signal"] | undefined }
) => {
  const { signal } = opts;

  const rawResources = Object.entries(metaResources).map(([name, meta]) => {
    const metaType = meta.type;
    switch (metaType) {
      case "text": {
        return {
          ...meta,
          name,
          resource: fetch(meta.url, { signal }).then((resp) => resp.text()),
        };
      }
      case "image":
      case "video":
        return {
          ...meta,
          name,
          resource: fetchBlobOrNothing(meta.url, signal, async (result) =>
            URL.createObjectURL(await result.blob())
          ),
        };
      case "sound":
        return {
          ...meta,
          name,
          resource: fetchBlobOrNothing(meta.url, signal, async (result) => {
            /**
             * We need to do this because array buffers can only be used once.
             * Alternatively, we could probably clone the array buffer
             */
            const buffer = await result.arrayBuffer();
            /** Params recycled from pixijs-sound */
            const ctx = new OfflineAudioContext(
              1,
              2,
              OfflineAudioContext ? 96000 : 44100
            );
            return await ctx.decodeAudioData(buffer);
          }),
        };
      default: {
        const x: never = metaType;
        throw new Error(`Unexpected type ${x}`);
      }
    }
  });

  const resourcesPromises = rawResources.map((item) => {
    const itemType = item.type;
    switch (itemType) {
      case "text":
      case "image":
        return item.resource.then((resolvedResource) => ({
          ...item,
          resource: resolvedResource,
        }));
      case "video":
        return item.resource
          .then((resolvedResource) => ({
            ...item,
            resource: resolvedResource,
          }))
          .then((item) => {
            const video = document.createElement("video");
            video.src = item.resource ?? "";
            return new Promise<{ video: HTMLVideoElement } & typeof item>(
              (res, fail) => {
                video.addEventListener("canplay", () => {
                  res({
                    ...item,
                    video,
                  });
                });
                video.onerror = fail;
              }
            );
          });
      case "sound":
        return item.resource.then((resolvedResource) => ({
          ...item,
          resource: resolvedResource,
        }));
      default: {
        const x: never = itemType;
        throw new Error(`Unexpected download ${x}`);
      }
    }
  });

  const resolvedResources = await Promise.all(resourcesPromises);

  const getAudioContext = getAudioContextFactory();
  const { ctx, cleanup } = getAudioContext();

  const resourcesWithCtx = resolvedResources.map((item) => {
    const resourceType = item.type;
    switch (resourceType) {
      case "text":
      case "image":
      case "video":
        return {
          ...item,
          // Adding back a promise to avoid massive refactoring
          resource: Promise.resolve(item.resource),
        };
      case "sound": {
        if (!item.resource)
          return {
            ...item,
            resource: {
              play: async () => ({} as unknown as void),
              stop: () => ({} as unknown as void),
            },
          };

        let source: AudioBufferSourceNode | undefined;

        return {
          ...item,
          resource: {
            play(opts?: { loop?: boolean }) {
              const _ctx = ctx();
              if (!_ctx) return;
              if (signal?.aborted) return;

              const audioBuffer = item.resource;
              if (!audioBuffer) return;

              /** First stop the previous playback */
              source?.stop();

              const originalSource = _ctx.createBufferSource();
              originalSource.buffer = audioBuffer;

              /**
               * You have to clone the source otherwise it doesn't always work
               * 🤷
               */
              source = cloneBufferSource(originalSource, _ctx);
              source.loop = !!opts?.loop;
              source.start();
            },
            stop() {
              if (!source) return;
              source.stop();
            },
          },
        };
      }
      default: {
        const x: never = resourceType;
        throw new Error(`Unexpected download ${x}`);
      }
    }
  });

  const downloadsMap = resourcesWithCtx.reduce((total, current) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore this should work
    total[current.name] = current;
    return total;
  }, {} as Resources<typeof metaResources>);

  return {
    /** You most likely don't want to call this directly */
    cleanup,
    get downloads() {
      return downloadsMap;
    },
  };
};

/**
 * @deprecated Use World instead
 * @internal use World instead
 */
export const useResources = <
  MetaResources extends Record<string, MetaResource>
>(
  metaResources: MetaResources
) => {
  type GetResources = typeof getResources<typeof metaResources>;

  const [resources, setResources] = useState<
    "loading" | Awaited<ReturnType<GetResources>>
  >("loading");

  const abort = useAbortCtrlCtx();

  const init = useRef(false);
  if (!init.current && !!abort) {
    init.current = true;
    getResources(metaResources, { signal: abort.signal })
      .then(setResources)
      .catch(() => setResources("loading"));
  }

  useEffect(
    function cleanup() {
      return () => {
        init.current = false;
        if (resources === "loading") return;
        resources.cleanup();
      };
    },
    [resources]
  );

  return resources;
};

type UseResources<MetaResources extends Record<string, MetaResource>> =
  ReturnType<typeof useResources<MetaResources>>;

/** This is just the downloaded type (that excludes the "loading" state) */
export type Downloads<MetaResources extends Record<string, MetaResource>> =
  Exclude<UseResources<MetaResources>, "loading">;

type GenericDownloadsCtx = Downloads<any>;

const [InternalGenericDownloadsProvider, useGenericDownloadsCtx] =
  makeReadOnlyContext<GenericDownloadsCtx>("GenericDownloads");

/** To use the downloads context, you have to cast it to the right type */
export const useDownloadsCtx = <
  MetaResources extends Record<string, MetaResource>
>() => {
  return useGenericDownloadsCtx() as Downloads<MetaResources>;
};

export {
  /** @private - Don't use this */
  InternalGenericDownloadsProvider,
};
