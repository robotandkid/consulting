import React, { useEffect, useState } from "react";
import { useDownloadsCtx } from "./resources";
import { World } from "./sprite-motion";

const GateBehindButton = (props: {
  children: React.ReactNode;
  text: string;
}) => {
  const [show, setShow] = useState(false);

  return (
    <>
      {show && props.children}
      {!show && <button onClick={() => setShow(true)}>{props.text}</button>}
    </>
  );
};

const resources = {
  music: {
    type: "sound",
    url: "./mom.mp3",
  },
} as const;

const SoundAutoCleanup = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  useEffect(() => {
    downloads.music.resource.play();
  }, [downloads.music.resource]);

  return null;
};

const SoundAutoCleanupInner = () => {
  const [key, setKey] = useState(Math.random());

  return (
    <World key={key} loading={<div>Loading...</div>} resources={resources}>
      <SoundAutoCleanup />
      <button onClick={() => setKey(Math.random())}>Restart sound</button>
    </World>
  );
};

export const SoundAutoCleanupExample = () => (
  <GateBehindButton text="Click">
    <SoundAutoCleanupInner />
  </GateBehindButton>
);

const StopSound = () => {
  const { downloads } = useDownloadsCtx<typeof resources>();
  useEffect(() => {
    downloads.music.resource.play();
  }, [downloads.music.resource]);

  return (
    <button onClick={() => downloads.music.resource.stop()}>Stop sound</button>
  );
};

const StopSoundInner = () => {
  return (
    <World loading={<div>Loading...</div>} resources={resources}>
      <StopSound />
    </World>
  );
};

export const SoundStopExample = () => (
  <GateBehindButton text="Click">
    <StopSoundInner />
  </GateBehindButton>
);
