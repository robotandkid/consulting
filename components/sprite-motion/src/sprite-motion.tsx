import {
  Bodies,
  Body,
  Composite,
  Constraint,
  Engine,
  Events,
  Runner,
} from "matter-js";
import React, {
  forwardRef,
  ReactNode,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import {
  InternalAbortCtrlProviderInner,
  InternalGenericDownloadsProvider,
  MetaResource,
  ResourceImg,
  useResources,
} from "./resources";
import { PanelTransition } from "./storyboard";
import { makeReadOnlyContext, makeSimpleContext } from "./utils/context";
import { useGuardedCallback } from "./utils/hooks";

/**
 * Escape hatch to access the matter-js Engine
 */
export const [EngineProvider, useEngineContext] =
  makeReadOnlyContext<Engine>("Engine");

/**
 * **WARNING**:
 *
 * - All of these are called by the World, which is the global parent.
 * - You will get state-update warnings when you naively try to update
 *   state on a child from the parent.
 * - Instead, you have to check the component is still mounted before
 *   updating state.
 */
type BodyCallbacks = {
  onUpdateBodyState: (body: Body) => void;
  onCollisionStart?: (thisBody: Body, otherBody: Body) => void;
  onCollisionEnd?: (thisBody: Body, otherBody: Body) => void;
  onCollisionActive?: (thisBody: Body, otherBody: Body) => void;
};

type AddBody = (body: Body | Composite, callbacks: BodyCallbacks) => void;

/**
 * @private  internal  Method used to add a body to the engine
 */
const [AddBodyProvider, useAddBodyContext] =
  makeReadOnlyContext<AddBody>("AddBody");

type RemoveBody = (body: Body | Composite) => void;

const [RemoveBodyProvider, useRemoveBodyContext] =
  makeReadOnlyContext<RemoveBody>("RemoveBody");

const [PaginationProvider, usePagination, useSetPagination] =
  makeSimpleContext<(direction: PanelTransition | number) => void>("Paginate");

export { useSetPagination, usePagination };

const AbortCtrlProvider = (props: { children: React.ReactNode }) => {
  const [ctrl, setCtrl] = useState(new AbortController());

  useEffect(() => {
    if (ctrl.signal.aborted) setCtrl(new AbortController());
    return () => {
      ctrl.abort("unmounted");
    };
  }, [ctrl]);

  return (
    <InternalAbortCtrlProviderInner value={ctrl}>
      {props.children}
    </InternalAbortCtrlProviderInner>
  );
};

const isBody = (body: Body | Composite | undefined): body is Body =>
  body?.type === "body";

type AllOrNothing<T> = Required<T> | { [key in keyof T]?: never };

/**
 * This is the main context provider. You must wrap everything with
 * _one_ of these. But note that it's OK to have more than one World
 * component on the page.
 */
const InternalWorld = forwardRef<
  HTMLDivElement,
  {
    children?: React.ReactNode;
    onInit?: (engine: Engine) => void;
    className?: string;
    style?: React.HTMLAttributes<HTMLElement>["style"];
  } & AllOrNothing<{
    width: number;
    height: number;
  }>
>((props, ref) => {
  const matter = useRef<{ engine: Engine; runner: Runner }>();

  const bodies = useRef<
    Record<number, { body: Body | Composite } & BodyCallbacks>
  >({});

  const addBody = useCallback(
    (body: Body | Composite, callbacks: BodyCallbacks) => {
      if (!matter.current?.engine) return;
      bodies.current[body.id] = { body, ...callbacks };
      Composite.add(matter.current.engine.world, body);
    },
    []
  );

  const removeBody = useCallback((body: Body | Composite) => {
    if (!matter.current?.engine) return;
    delete bodies.current[body.id];
    Composite.remove(matter.current.engine.world, body);
  }, []);

  const init = useRef(false);

  if (!init.current) {
    init.current = true;

    matter.current = {
      engine: Engine.create(),
      runner: Runner.create(),
    };

    Runner.run(matter.current.runner, matter.current.engine);
    props.onInit?.(matter.current.engine);
  }

  useEffect(function cleanupMatter() {
    return () => {
      if (matter.current) Runner.stop(matter.current.runner);
    };
  }, []);

  useEffect(function setupCollisionCallbacks() {
    if (!matter.current) return;

    /**
     * This exists because calling a BodyCallback may remove the body
     * from the world, so need to get fresh closures for everything
     */
    const invokeCallback = (
      id1: number,
      id2: number,
      callbackName: keyof BodyCallbacks
    ) => {
      const body1 = bodies.current[id1];
      const body2 = bodies.current[id2];

      if (body1 && body2 && isBody(body1?.body) && isBody(body2?.body)) {
        body1[callbackName]?.(body1.body, body2.body);
      }
    };

    Events.on(matter.current.engine, "collisionStart", (event) => {
      const pairs = event.pairs;

      for (const pair of pairs) {
        const idA = pair.bodyA.id;
        const idB = pair.bodyB.id;

        invokeCallback(idA, idB, "onCollisionStart");
        // the callback might remove the bodies so have to redo the check
        invokeCallback(idB, idA, "onCollisionStart");
      }
    });

    Events.on(matter.current.engine, "collisionEnd", (event) => {
      const pairs = event.pairs;

      for (const pair of pairs) {
        const idA = pair.bodyA.id;
        const idB = pair.bodyB.id;

        invokeCallback(idA, idB, "onCollisionEnd");
        // the callback might remove the bodies so have to redo the check
        invokeCallback(idB, idA, "onCollisionEnd");
      }
    });

    Events.on(matter.current.engine, "collisionActive", (event) => {
      const pairs = event.pairs;

      for (const pair of pairs) {
        const idA = pair.bodyA.id;
        const idB = pair.bodyB.id;

        invokeCallback(idA, idB, "onCollisionActive");
        // the callback might remove the bodies so have to redo the check
        invokeCallback(idB, idA, "onCollisionActive");
      }
    });
  });

  useEffect(function setupUpdateBodyCallback() {
    if (!matter.current) return;
    let unsubscribe: number;

    function animate() {
      Object.values(bodies.current).forEach(({ body, onUpdateBodyState }) => {
        if (isBody(body)) onUpdateBodyState(body);
        else if (body.bodies[0]) onUpdateBodyState(body.bodies[0]);
      });

      unsubscribe = requestAnimationFrame(animate);
    }

    unsubscribe = requestAnimationFrame(animate);

    return () => {
      cancelAnimationFrame(unsubscribe);
    };
  }, []);

  if (!matter.current) return null;

  return (
    <div
      ref={ref}
      className={props.className}
      style={{
        ...props.style,
        position: "relative",
        width: props.width,
        height: props.height,
      }}
    >
      <EngineProvider value={matter.current.engine}>
        <AddBodyProvider value={addBody}>
          <RemoveBodyProvider value={removeBody}>
            <PaginationProvider>{props.children}</PaginationProvider>
          </RemoveBodyProvider>
        </AddBodyProvider>
      </EngineProvider>
    </div>
  );
});

/**
 * This exists because matter-js Bodies.rectangle takes the center but
 * we're working with the top-left coordinates.
 *
 * ```text
 *   (start) ----------------+
 *     |                     |
 *     |                     |
 *     |     (position)      |
 *
 *     initial position = start + dimensions / 2
 * ```
 */
const createBodyFromTopLeft = (
  ...args: Parameters<typeof Bodies.rectangle>
) => {
  const [x, y, width, height, ...rest] = args;
  return Bodies.rectangle(
    // x and y are the middle of the rectangle
    x + width / 2,
    y + height / 2,
    width,
    height,
    ...rest
  );
};

type StyleProps = React.HTMLAttributes<HTMLElement>["style"] & {
  transformTranslate: string;
  transformRotate: string;
};

/**
 * @deprecated  The reason this exists is because matter-js
 *   coordinates are relative to the center of the rectangle, while in
 *   CSS, we usually work from the _corners_ of the DIV.
 *
 *   ```text
 *     (start) ----------------+
 *   |                     |
 *   |                     |
 *   |     (position)      |
 *
 *   initial position = start + dimensions / 2
 *   start = initial position - dimensions / 2
 * ```
 *
 *   Therefore,
 *
 *   ```
 *   delta = position - dimensions / 2 - start;
 *   ```
 */
const deprecatedRectCoordsToStyleProps = (opts: {
  start: {
    left: number;
    top: number;
  };
  body: ReturnType<typeof Bodies.rectangle> | undefined;
  dimensions: {
    width: number | undefined;
    height: number | undefined;
  };
}): StyleProps => {
  const {
    start: { left, top },
    body,
    dimensions: { width = 0, height = 0 },
  } = opts;
  const x = body?.position.x ?? 0;
  const y = body?.position.y ?? 0;
  const angle = body?.angle ?? 0;
  const dx = x - width / 2 - left;
  const dy = y - height / 2 - top;
  const angleInDeg = Math.floor((angle * 180) / Math.PI);
  const transformTranslate = `translate(${dx}px,${dy}px)`;
  const transformRotate = `rotate(${angleInDeg}deg)`;
  /**
   * This is read right to left---so rotate first along the center of
   * the object, then translate by the delta
   */
  const transform = `${transformTranslate} ${transformRotate}`;

  return {
    position: "absolute",
    left,
    top,
    transform,
    transformTranslate,
    transformRotate,
    width,
    height,
  };
};

const rectCoordsToStyleProps = (opts: {
  start: {
    cx: number;
    cy: number;
  };
  body: Pick<Body, "position" | "angle">;
  dimensions: {
    width: number | undefined;
    height: number | undefined;
  };
}): StyleProps => {
  const {
    start: { cx, cy },
    body,
    dimensions: { width = 0, height = 0 },
  } = opts;
  const left = cx - width / 2;
  const top = cy - height / 2;
  const x = body.position.x ?? 0;
  const y = body.position.y ?? 0;
  const dx = x - cx;
  const dy = y - cy;
  const angle = body.angle ?? 0;
  const angleInDeg = Math.floor((angle * 180) / Math.PI);
  const transformTranslate = `translate(${dx}px,${dy}px)`;
  const transformRotate = `rotate(${angleInDeg}deg)`;
  /**
   * This is read right to left---so rotate first along the center of
   * the object, then translate by the delta
   */
  const transform = `${transformTranslate} ${transformRotate}`;

  return {
    position: "absolute",
    left,
    top,
    transform,
    transformTranslate,
    transformRotate,
    width,
    height,
  };
};

/**
 * When an object changes position, we have to redraw
 */
const useForceRenderAndUpdateBodyState = () => {
  const [body, setBody] = useState<Pick<Body, "position" | "angle">>();

  const guardedUpdate = useGuardedCallback((body: Body) => {
    setBody({ position: body.position, angle: body.angle });
  });

  return [body, guardedUpdate] as const;
};

const toRadians = (angle: number | undefined) =>
  angle === undefined ? 0 : (angle * Math.PI) / 180;

/**
 * **WARNING:** All props are _fixed_. That is, subsequent updates
 * will _not_ move the DIV.
 *
 * This is otherwise just a regular DIV, so by default it is
 * _invisible_. Pass style props to color the DIV.
 *
 * If you need movement, use a MotionDiv.
 */
export const StaticDiv = (
  props: React.HTMLAttributes<HTMLDivElement> & {
    cx: number;
    cy: number;
    width: number;
    height: number;
    /**
     * In degrees
     */
    angle?: number;
  } & Pick<
      BodyCallbacks,
      "onCollisionStart" | "onCollisionEnd" | "onCollisionActive"
    >
) => {
  const {
    cx,
    cy,
    width,
    height,
    angle,
    onCollisionStart,
    onCollisionEnd,
    onCollisionActive,
    ...rest
  } = props;
  const addBody = useAddBodyContext();
  const removeBody = useRemoveBodyContext();
  const [body, setBodyAndForceRender] = useForceRenderAndUpdateBodyState();
  const init = useRef(false);

  const initialPos = useRef({
    cx,
    cy,
    width,
    height,
    angle: toRadians(angle),
    ariaLabel: rest["aria-label"],
  });

  const guardedCollisionStart = useGuardedCallback(onCollisionStart);
  const guardedCollisionEnd = useGuardedCallback(onCollisionEnd);
  const guardedCollisionActive = useGuardedCallback(onCollisionActive);

  useEffect(() => {
    if (init.current) return;

    init.current = true;

    // if you have an angle, it gets weird

    const newBody = Bodies.rectangle(
      initialPos.current.cx,
      initialPos.current.cy,
      initialPos.current.width,
      initialPos.current.height,
      {
        isStatic: true,
        angle: initialPos.current.angle,
        label: initialPos.current.ariaLabel,
      }
    );

    addBody?.(newBody, {
      onUpdateBodyState: setBodyAndForceRender,
      onCollisionStart: guardedCollisionStart,
      onCollisionEnd: guardedCollisionEnd,
      onCollisionActive: guardedCollisionActive,
    });

    return () => {
      if (newBody) removeBody?.(newBody);
    };
  }, [
    addBody,
    guardedCollisionActive,
    guardedCollisionEnd,
    guardedCollisionStart,
    removeBody,
    setBodyAndForceRender,
  ]);

  if (!body) return null;

  const styleProps = rectCoordsToStyleProps({
    start: initialPos.current,
    body,
    dimensions: initialPos.current,
  });

  return (
    <div
      {...rest}
      style={{
        ...rest.style,
        ...styleProps,
      }}
    />
  );
};

/**
 * **WARNING:** this component does not support dynamic props, that
 * is, changing the starting position/velocity will _not_ change the
 * animation.
 */
export const MotionImg = (
  props: Omit<React.ImgHTMLAttributes<HTMLImageElement>, "src"> & {
    src: React.ImgHTMLAttributes<HTMLImageElement>["src"] | ResourceImg;
    startLeft: number;
    startTop: number;
    /**
     * In degrees
     */
    startAngle?: number;
    disableCollisions?: boolean;
  } & AllOrNothing<{ vx: number; vy: number }> &
    Pick<
      BodyCallbacks,
      "onCollisionStart" | "onCollisionEnd" | "onCollisionActive"
    >
) => {
  const {
    startLeft,
    startTop,
    vx,
    vy,
    src,
    onLoad: origOnLoad,
    onCollisionStart,
    onCollisionEnd,
    onCollisionActive,
    disableCollisions = false,
    ...rest
  } = props;

  const addBody = useAddBodyContext();
  const removeBody = useRemoveBodyContext();
  const ref = useRef<HTMLImageElement>(null);
  const [, setBodyAndForceRender] = useForceRenderAndUpdateBodyState();

  const body = useRef<Body>();

  const initialPos = useRef({
    left: startLeft,
    top: startTop,
    vx,
    vy,
    label: rest["aria-label"],
    disableCollisions,
  });

  const guardedCollisionStart = useGuardedCallback(onCollisionStart);
  const guardedCollisionEnd = useGuardedCallback(onCollisionEnd);
  const guardedCollisionActive = useGuardedCallback(onCollisionActive);

  const onLoad = useCallback(() => {
    if (!ref.current) return;

    body.current = createBodyFromTopLeft(
      initialPos.current.left,
      initialPos.current.top,
      ref.current.width,
      ref.current.height,
      { label: initialPos.current.label }
    );

    if (initialPos.current.disableCollisions) {
      body.current.collisionFilter = {
        group: -1,
        category: 2,
        mask: 0,
      };
    }

    const thisBody = body.current;

    addBody?.(thisBody, {
      onUpdateBodyState: setBodyAndForceRender,
      onCollisionStart: guardedCollisionStart,
      onCollisionEnd: guardedCollisionEnd,
      onCollisionActive: guardedCollisionActive,
    });

    if (
      initialPos.current.vx !== undefined &&
      initialPos.current.vy !== undefined
    )
      Body.setVelocity(thisBody, {
        x: initialPos.current.vx,
        y: initialPos.current.vy,
      });
  }, [
    addBody,
    guardedCollisionActive,
    guardedCollisionEnd,
    guardedCollisionStart,
    setBodyAndForceRender,
  ]);

  useEffect(() => {
    const thisBody = body.current;
    return () => {
      if (thisBody) removeBody?.(thisBody);
    };
  }, [removeBody]);

  const styleProps = deprecatedRectCoordsToStyleProps({
    start: initialPos.current,
    body: body.current,
    dimensions: { width: ref.current?.width, height: ref.current?.height },
  });

  delete styleProps?.width;
  delete styleProps?.height;

  const rawSrcUrl = typeof src === "string" ? src : src?.resource;
  const [srcUrl, setSrcUrl] = useState<string | undefined>(
    typeof rawSrcUrl === "string" ? rawSrcUrl : undefined
  );
  const loadedUrl = useRef(false);
  if (!loadedUrl.current) {
    loadedUrl.current = true;
    if (rawSrcUrl && typeof rawSrcUrl !== "string") rawSrcUrl.then(setSrcUrl);
  }

  if (!body.current) styleProps.display = "none";

  return (
    <img
      {...rest}
      src={srcUrl}
      ref={ref}
      onLoad={(e) => {
        onLoad();
        origOnLoad?.(e);
      }}
      style={{
        ...rest.style,
        position: "absolute",
        ...styleProps,
      }}
    />
  );
};

export const StaticImg = (
  props: Omit<React.ImgHTMLAttributes<HTMLImageElement>, "src"> & {
    src: React.ImgHTMLAttributes<HTMLImageElement>["src"] | ResourceImg;
  } & {
    className?: string;
  }
) => {
  const { src, ...rest } = props;
  const rawSrcUrl = typeof src === "string" ? src : src?.resource;

  const [srcUrl, setSrcUrl] = useState<string | undefined>(
    typeof rawSrcUrl === "string" ? rawSrcUrl : undefined
  );

  const loadedUrl = useRef(false);
  if (!loadedUrl.current) {
    loadedUrl.current = true;
    if (rawSrcUrl && typeof rawSrcUrl !== "string") rawSrcUrl.then(setSrcUrl);
  }

  return (
    <img
      {...rest}
      src={srcUrl}
      style={{
        ...rest.style,
        ...(!srcUrl && { display: "none" }),
      }}
    />
  );
};

/**
 * **WARNING:** this component does not support dynamic props, that
 * is, changing the starting position/velocity will _not_ change the
 * animation.
 *
 * If you need a constraint, use MotionDiv with constraint.
 */
export const MotionDiv = forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLDivElement> & {
    startCx: number;
    startCy: number;
    /**
     * In degrees
     *
     * @deprecated  Use options
     */
    startAngle?: number;
    width: number;
    height: number;
    options?: Parameters<typeof Bodies.rectangle>[4];
  } & AllOrNothing<{ vx: number; vy: number }> &
    Pick<
      BodyCallbacks,
      "onCollisionStart" | "onCollisionEnd" | "onCollisionActive"
    >
>((props, ref) => {
  const {
    startCx,
    startCy,
    startAngle,
    width,
    height,
    vx,
    vy,
    onCollisionStart,
    onCollisionEnd,
    onCollisionActive,
    options,
    ...rest
  } = props;

  const addBody = useAddBodyContext();
  const removeBody = useRemoveBodyContext();
  const [body, setBodyAndForceRender] = useForceRenderAndUpdateBodyState();

  const initialPos = useRef({
    cx: startCx,
    cy: startCy,
    angle: toRadians(startAngle),
    vx,
    vy,
    width,
    height,
    label: rest["aria-label"],
    options,
  });

  const guardedCollisionStart = useGuardedCallback(onCollisionStart);
  const guardedCollisionEnd = useGuardedCallback(onCollisionEnd);
  const guardedCollisionActive = useGuardedCallback(onCollisionActive);

  useEffect(() => {
    const newBody = Bodies.rectangle(
      initialPos.current.cx,
      initialPos.current.cy,
      initialPos.current.width,
      initialPos.current.height,
      {
        angle: initialPos.current.angle,
        ...initialPos.current.options,
        label: initialPos.current.label,
      }
    );

    addBody?.(newBody, {
      onUpdateBodyState: setBodyAndForceRender,
      onCollisionStart: guardedCollisionStart,
      onCollisionEnd: guardedCollisionEnd,
      onCollisionActive: guardedCollisionActive,
    });

    if (
      initialPos.current.vx !== undefined &&
      initialPos.current.vy !== undefined
    ) {
      Body.setVelocity(newBody, {
        x: initialPos.current.vx,
        y: initialPos.current.vy,
      });
    }

    return () => {
      if (newBody) removeBody?.(newBody);
    };
  }, [
    addBody,
    guardedCollisionActive,
    guardedCollisionEnd,
    guardedCollisionStart,
    options,
    removeBody,
    setBodyAndForceRender,
  ]);

  if (!body) return null;

  const styleProps = rectCoordsToStyleProps({
    start: initialPos.current,
    body,
    dimensions: initialPos.current,
  });

  return (
    <div
      {...rest}
      ref={ref}
      style={{
        ...rest.style,
        ...styleProps,
      }}
    />
  );
});

const lerp = (startValue: number, endValue: number, t: number) => {
  // Ensure that t is within the range [0, 1]
  t = Math.max(0, Math.min(1, t));

  // Perform linear interpolation
  return startValue * (1 - t) + endValue * t;
};

export const Joint = (
  props: React.HTMLAttributes<HTMLDivElement> & {
    startLeft: number;
    startTop: number;
    /**
     * ```text
     * hingeX = 0% => startLeft
     * hingeX = 100% => startLeft + width
     * ```
     */
    hingeX: `${number}%`;
    /**
     * ```text
     * hingeY = 0% => startTop
     * hingeY = 100% => startTop + height
     * ```
     */
    hingeY: `${number}%`;
    width: number;
    height: number;
  } & AllOrNothing<{
      vx?: number;
      vy?: number;
    }>
) => {
  const {
    startLeft,
    startTop,
    hingeX,
    hingeY,
    width,
    height,
    vx,
    vy,
    ...rest
  } = props;

  const addBody = useAddBodyContext();
  const removeBody = useRemoveBodyContext();
  const ref = useRef<HTMLImageElement>(null);
  const [, forceRender] = useForceRenderAndUpdateBodyState();
  const body = useRef<Body>();

  const initialPos = useRef({
    left: startLeft,
    top: startTop,
    hingeX,
    hingeY,
    hingeXp: +hingeX.replace("%", "") / 100,
    hingeYp: +hingeY.replace("%", "") / 100,
    vx,
    vy,
    width,
    height,
    label: rest["aria-label"],
  });

  useEffect(() => {
    if (!ref.current) return;

    body.current = createBodyFromTopLeft(
      initialPos.current.left,
      initialPos.current.top,
      initialPos.current.width,
      initialPos.current.height,
      { label: initialPos.current.label }
    );

    /**
     * This is just a light wrapper. I wish Bodies supported
     * Constraints. But in order to add a constraint, we have to wrap
     * the Body with a Composite.
     */
    const wrapper = Composite.create();
    const rectangle = body.current;
    Composite.add(wrapper, rectangle);

    /**
     * Again, this is quirk where matter-js works relative to the
     * object center but this API works relative to the top-left
     * corner.
     *
     * This maps hingeXp, which is from [startLeft, startLeft+width]
     * to the center of pointB.
     */
    const pointBx = lerp(
      -initialPos.current.width / 2,
      initialPos.current.width / 2,
      initialPos.current.hingeXp
    );

    /**
     * Ditto
     */
    const pointBy = lerp(
      -initialPos.current.height / 2,
      initialPos.current.height / 2,
      initialPos.current.hingeYp
    );

    Composite.add(
      wrapper,
      Constraint.create({
        length: 0,
        pointA: { x: rectangle.position.x, y: rectangle.position.y },
        pointB: { x: pointBx, y: pointBy },
        bodyB: rectangle,
        stiffness: 1,
      })
    );

    addBody?.(wrapper, { onUpdateBodyState: forceRender });

    if (
      initialPos.current.vx !== undefined &&
      initialPos.current.vy !== undefined
    ) {
      Body.setVelocity(rectangle, {
        x: initialPos.current.vx,
        y: initialPos.current.vy,
      });
    }

    return () => {
      removeBody?.(wrapper);
    };
  }, [addBody, removeBody, forceRender]);

  const styleProps = deprecatedRectCoordsToStyleProps({
    start: initialPos.current,
    body: body.current,
    dimensions: initialPos.current,
  });

  return (
    <div
      {...rest}
      ref={ref}
      style={{
        ...rest.style,
        ...styleProps,
        transformOrigin: `${initialPos.current.hingeX} ${initialPos.current.hingeY}`,
        transform: styleProps.transformRotate,
      }}
    />
  );
};

const WorldWithResourcesInnerFn = <
  MetaResources extends Record<string, MetaResource>
>(
  props: {
    resources: MetaResources;
    loading: ReactNode;
  } & Parameters<typeof InternalWorld>[0],
  ref: React.Ref<HTMLDivElement>
) => {
  const { resources, loading, ...rest } = props;
  const downloaded = useResources(resources);

  if (downloaded === "loading") return <>{loading}</>;

  return (
    <InternalGenericDownloadsProvider value={downloaded}>
      <InternalWorld ref={ref} {...rest} />
    </InternalGenericDownloadsProvider>
  );
};

export const WorldWithResourcesInner = forwardRef(WorldWithResourcesInnerFn);

const emptyDownloads = {
  cleanup: () => ({}),
  downloads: {},
} as const;

const WorldWithResourcesFn = <
  MetaResources extends Record<string, MetaResource>
>(
  props: AllOrNothing<{
    resources: MetaResources;
    loading: ReactNode;
  }> &
    Parameters<typeof InternalWorld>[0],
  ref: React.Ref<HTMLDivElement>
) => {
  if (props.resources && props.loading) {
    return (
      <AbortCtrlProvider>
        <WorldWithResourcesInner ref={ref} {...props} />
      </AbortCtrlProvider>
    );
  }
  return (
    <AbortCtrlProvider>
      <InternalGenericDownloadsProvider value={emptyDownloads}>
        <InternalWorld ref={ref} {...props} />
      </InternalGenericDownloadsProvider>
    </AbortCtrlProvider>
  );
};

export const World = forwardRef(WorldWithResourcesFn);
