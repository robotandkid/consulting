import "pixi.js";
import { ILoaderResource, IPoint } from "pixi.js";
import { Point as AugmentedPoint } from "@pixi/math";

declare module "pixi.js" {
  interface LoaderResource extends ILoaderResource {
    sound: {
      stop: () => void;
      play: (opts: { loop: boolean }) => void;
      isPlaying: boolean;
    };
  }

  class Point extends AugmentedPoint implements IPoint {
    constructor(x: number, y: number): Point;

    add(p: Point): Point;
    subtract(p: Point): Point;
    magnitude(): number;
    multiplyScalar(scalar: number): Point;
    normalize(): Point;
  }
}

declare module "@pixi/math" {
  class Point extends AugmentedPoint implements IPoint {
    constructor(x: number, y: number): Point;

    add(p: Point): Point;
    subtract(p: Point): Point;
    magnitude(): number;
    multiplyScalar(scalar: number): Point;
    normalize(): Point;
  }
}

declare global {
  interface HTMLCanvasElement {
    captureStream(fps: number): MediaStream;
  }

  class MediaRecorder {
    static isTypeSupported(
      mimeType: "video/webm" | "video/mp4" | "video/mp4;codecs=avc1.4d002a"
    );

    constructor(
      stream: MediaStream,
      opts?: { mimeType: "video/mp4" | "video/mp4;codecs=avc1.4d002a" }
    );

    addEventListener(
      name: "dataavailable",
      cb: (props: { data: BlobPart }) => void
    );

    removeEventListener(
      name: "dataavailable",
      cb: (props: { data: BlobPart }) => void
    );

    start();
    stop();
  }
}

declare module "*.frag" {
  const value: string;
  export default value;
}

declare module "*.glsl" {
  const value: string;
  export default value;
}
