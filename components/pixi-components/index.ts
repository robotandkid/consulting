import { PixiApp, PixiAppWrapper } from "./src/core/PixiApp/PixiApp";
import {
  PixiAppContext,
  PixiLoaderResourcesContext,
} from "./src/core/PixiApp/PixiContexts";
import { useAddToParent } from "./src/core/PixiApp/useAddToParent";
import {
  PixiFilter,
  PixiFilter2,
  useAddPixiFilter2,
} from "./src/core/PixiFilter/PixiFilter";
import { defaultDotFilter } from "./src/core/PixiFilter/PixiFilter.defaults.dot";
import { defaultGlitchFilter } from "./src/core/PixiFilter/PixiFilter.defaults.glitch";
import { defaultGlowFilter } from "./src/core/PixiFilter/PixiFilter.defaults.glow";
import { defaultOldFilmFilter } from "./src/core/PixiFilter/PixiFilter.defaults.oldfilm";
import { defaultPixelateFilter } from "./src/core/PixiFilter/PixiFilter.defaults.pixelate";
import { useLightningShader } from "./src/core/PixiShader/useLightningShader";
import { useLoadPixiSprite } from "./src/core/PixiSprite/PixiSprite";
import { defaultTextStyle } from "./src/core/PixiText/defaultTextStyle";
import { useGlowingEyes } from "./src/effects/GlowingEyes/glowingEyes";
import { flocking } from "./src/effects/Flocking/Flocking";
import { useAddFlock } from "./src/effects/Flocking/useAddFlock";
import { Oscilloscope } from "./src/effects/Oscilloscope/Oscilloscope";
import { TypewriterText } from "./src/components/TypewriterText/TypewriterText";
import {
  ApplyFilter,
  Background,
  Delay,
  PhysicsSprite,
  Say,
  Scene,
  SceneProvider,
} from "./src/awesome-retro-cutscenes/AwesomeRetroCutscenes";
import {
  PixiEvents,
  RegisterPixiEventListeners,
  usePixiEvents,
} from "./src/core/PixiEvents/PixiEvents";

export type PixiAppOpts = Parameters<typeof PixiApp>[0];
export type OscilloscopeOpts = Parameters<typeof Oscilloscope>[0];
export type TypewriterTextOpts = Parameters<typeof TypewriterText>[0];

export {
  PixiApp,
  PixiAppWrapper,
  PixiAppContext,
  PixiLoaderResourcesContext,
  useAddToParent,
};
export { useLoadPixiSprite };
export {
  PixiFilter,
  PixiFilter2,
  useAddPixiFilter2,
  defaultGlowFilter,
  defaultOldFilmFilter,
  defaultPixelateFilter,
  defaultDotFilter,
  defaultGlitchFilter,
};
export { Oscilloscope, TypewriterText };
export { flocking, useAddFlock };
export { useGlowingEyes, useLightningShader };
export { defaultTextStyle };

export {
  SceneProvider,
  Scene,
  Background,
  Delay,
  PhysicsSprite,
  Say,
  ApplyFilter,
};

export { PixiEvents, usePixiEvents, RegisterPixiEventListeners };
