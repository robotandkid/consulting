import typescript from "@rollup/plugin-typescript";
import acorn from "acorn-jsx";

export default {
  input: "index.ts",
  output: {
    dir: "dist",
    format: "esm",
    sourcemap: true,
  },
  acornInjectPlugins: [acorn()],
  plugins: [
    typescript({
      tsconfig: "tsconfig.types.json",
    }),
  ],
  external: [
    "framer-motion",
    "react",
    "styled-components",
    "next/image",
    "next/router",
    "next/link",
    "next/head",
    "react-dom/server",
    "@pixi/graphics",
    "pixi-sound",
    "pixi.js",
    "pixi-filters",
    "@pixi/math-extras",
    "matter-js",
  ],
  treeshake: {
    moduleSideEffects: ["@pixi/math-extras", "pixi-sound"],
  },
};
