# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.11.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.11.0...@robotandkid/pixi-components@0.11.1) (2025-03-05)

**Note:** Version bump only for package @robotandkid/pixi-components





# [0.11.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.10.5...@robotandkid/pixi-components@0.11.0) (2025-03-05)


### Features

* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





## [0.10.5](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.10.4...@robotandkid/pixi-components@0.10.5) (2023-12-06)

**Note:** Version bump only for package @robotandkid/pixi-components





## [0.10.4](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.10.3...@robotandkid/pixi-components@0.10.4) (2023-11-07)

**Note:** Version bump only for package @robotandkid/pixi-components





## [0.10.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.10.2...@robotandkid/pixi-components@0.10.3) (2023-09-19)

**Note:** Version bump only for package @robotandkid/pixi-components





## [0.10.2](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.10.1...@robotandkid/pixi-components@0.10.2) (2023-09-18)

**Note:** Version bump only for package @robotandkid/pixi-components





## [0.10.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.10.0...@robotandkid/pixi-components@0.10.1) (2023-09-04)


### Bug Fixes

* enabled stricer array access in TS ([8b89be7](https://gitlab.com/robotandkid/consulting/commit/8b89be793edef75156025f9b3ee4a7944a308dc0))





# [0.10.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.9.0...@robotandkid/pixi-components@0.10.0) (2023-08-07)


### Features

* upgraded node, pixi.js ([c06a0b4](https://gitlab.com/robotandkid/consulting/commit/c06a0b470991876a2763336d76a4272ca536af75))





# [0.9.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.8.0...@robotandkid/pixi-components@0.9.0) (2023-08-07)


### Features

* **digital-apps:** eli garcia published ([331476e](https://gitlab.com/robotandkid/consulting/commit/331476ecc8574bc6371c17268e674f68a6919c1e))
* **masonrypro:** added about ([8ef4b29](https://gitlab.com/robotandkid/consulting/commit/8ef4b29b8a84a0f0d0df4c552d9474eccbb5e8b1))
* **root:** added netlify nextjs plugin ([7d348bb](https://gitlab.com/robotandkid/consulting/commit/7d348bb3db7c27ec55e3aa33a94411ad49d8a8b0))





# [0.8.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.7.0...@robotandkid/pixi-components@0.8.0) (2022-11-27)


### Bug Fixes

* **pixi-components:** has correct types ([157d1f2](https://gitlab.com/robotandkid/consulting/commit/157d1f2e47c809789f1fc2c9b675c08b769f3c63))
* **urielavalos:** correctly building component libraries ([2e2bf3f](https://gitlab.com/robotandkid/consulting/commit/2e2bf3fa85830cd4f16ad4de7196c70d500f0bc0))


### Features

* **urielavalos:** reduced bundle size ([2964056](https://gitlab.com/robotandkid/consulting/commit/2964056f73168b847f811150bc536b30e86c3e46))





## [0.6.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.6.0...@robotandkid/pixi-components@0.6.1) (2022-02-02)


### Bug Fixes

* **pixi-components:** pixi-sound needs to be a sideEffect ([88502b5](https://gitlab.com/robotandkid/consulting/commit/88502b57269215cac0dc3d7c6fee94357a6eaa9a))





# [0.6.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.5.1...@robotandkid/pixi-components@0.6.0) (2022-02-01)


### Bug Fixes

* **pixi-components:** correctly generated library ([fa86c17](https://gitlab.com/robotandkid/consulting/commit/fa86c1725e42b438444ee874019f5a15e50425c3))


### Features

* **pixi-components:** did some stuff ([3816990](https://gitlab.com/robotandkid/consulting/commit/3816990b4eb51068659abb7c135bf498e90277dc))





## [0.5.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.5.0...@robotandkid/pixi-components@0.5.1) (2022-01-23)


### Bug Fixes

* **pixi-components:** added background color support to typewriter text ([86ece77](https://gitlab.com/robotandkid/consulting/commit/86ece77776ffb2d1854f0fe431f61e91462c33b6))





# [0.5.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.4.0...@robotandkid/pixi-components@0.5.0) (2022-01-23)


### Bug Fixes

* **pixi-components:** almost compiles ([3413e6b](https://gitlab.com/robotandkid/consulting/commit/3413e6b28e6cf641e920434cc8e314b6e92b44a8))
* **pixi-components:** almost compiles ([7b2c96b](https://gitlab.com/robotandkid/consulting/commit/7b2c96b90d67c7718945f27dd9a63cd6ee717bda))
* **pixi-components:** exported latest filter ([394950b](https://gitlab.com/robotandkid/consulting/commit/394950bb37044b69a7fc47531dd33ff0b0740823))


### Features

* **pixi-components:** added PixiSprite ([4b29afb](https://gitlab.com/robotandkid/consulting/commit/4b29afbca63caa263e22576818bf8aebc9e9a9c1))
* **pixi-components:** properly bundles the math library ([e3425ca](https://gitlab.com/robotandkid/consulting/commit/e3425ca22c82022597e4146ccd7b8240d9197767))
* **pixi-components:** so much ([271296c](https://gitlab.com/robotandkid/consulting/commit/271296ccbe871e9e7ca2ca415120b1bf55c048e5))





# [0.4.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.3.1...@robotandkid/pixi-components@0.4.0) (2022-01-13)


### Features

* **pixi-components:** canvas is a motion component ([4ff2d8d](https://gitlab.com/robotandkid/consulting/commit/4ff2d8de5c00fd50f268402ea4257c8a91b2395d))





## [0.3.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.3.0...@robotandkid/pixi-components@0.3.1) (2022-01-13)


### Bug Fixes

* **pixi-copmonents:** compiles again ([aa61068](https://gitlab.com/robotandkid/consulting/commit/aa6106869d837e63d36ac4578c3c4cc30520cfb8))





# [0.3.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.2.0...@robotandkid/pixi-components@0.3.0) (2022-01-09)


### Bug Fixes

* **pixi-components:** compiles correctly ([e237923](https://gitlab.com/robotandkid/consulting/commit/e237923154c8296ba888d1eaee198b0a302fc4f1))
* **pixi-components:** dist generated correctly ([b236d7a](https://gitlab.com/robotandkid/consulting/commit/b236d7a520d48b499671838abcc07e88494a9fa8))
* **pixi-components:** oscilloscope properly renders ([2bf1d43](https://gitlab.com/robotandkid/consulting/commit/2bf1d43faad1a40f44b1611bdfcc53a9d438e773))
* **pixi-components:** oscilloscope renders correctly ([0877a35](https://gitlab.com/robotandkid/consulting/commit/0877a35f0415f63607c4972f4f4ac4cab5eed8cb))


### Features

* **pixi-components:** added pixi filter comp ([0421385](https://gitlab.com/robotandkid/consulting/commit/042138534542527138bd48879752edd0f6564651))
* **pixi-components:** created ([e473096](https://gitlab.com/robotandkid/consulting/commit/e473096fea05eeb028b864220aea021e44d75e54))
* **pixi-components:** glitch filter is now configuratble ([da765c9](https://gitlab.com/robotandkid/consulting/commit/da765c9648d99c8b8cefbdcec4b8378dde11ff68))
* **pixi-components:** properly centering typewriter text ([80de51b](https://gitlab.com/robotandkid/consulting/commit/80de51bd5d8b40148b7031080e2081bbb5798328))
* **pixi-components:** typewriter component now has a style property ([ac749e4](https://gitlab.com/robotandkid/consulting/commit/ac749e4d5226c7a53ec0cefa84b2e1062af7c8f8))





# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.1.1...@robotandkid/pixi-components@0.2.0) (2021-12-14)


### Bug Fixes

* **pixi-components:** typewriter text almost works ([590541f](https://gitlab.com/robotandkid/consulting/commit/590541f76ed0c738df93416a0dbc98d2412b9218))


### Features

* **components:** enabled tree-shaking ([2efc5fe](https://gitlab.com/robotandkid/consulting/commit/2efc5fe8256f5005ffd83c55c94a5d9c7ffdda03))
* **pixicomponents:** added typewriter text story ([1d48978](https://gitlab.com/robotandkid/consulting/commit/1d48978100c3b513ce616a53b82e338823305611))
* **pixicomponents:** added vitro ([9aaae29](https://gitlab.com/robotandkid/consulting/commit/9aaae2946400a3f743b94fe5dba55971031929a5))
* **pixicomponents:** added vitro ([c4490a2](https://gitlab.com/robotandkid/consulting/commit/c4490a2e49147116ea1a6fa88638a806df564410))
* **pixicomponents:** migrated TypewriterText ([46da16c](https://gitlab.com/robotandkid/consulting/commit/46da16ced3a7f326045fd6b770bd5fe771278d43))





## [0.1.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/pixi-components@0.1.0...@robotandkid/pixi-components@0.1.1) (2021-12-03)


### Bug Fixes

* **pixi-components:** compiles again ([80cb3ea](https://gitlab.com/robotandkid/consulting/commit/80cb3eaf8ac5bc8fd39db925172e234106a18d4d))





# 0.1.0 (2021-12-03)


### Features

* **pixi-components:** added base ([a36da89](https://gitlab.com/robotandkid/consulting/commit/a36da8972a52be4b4f18d598a02ed0b8b33f521b))
* **pixi:** added missing files ([2f55a79](https://gitlab.com/robotandkid/consulting/commit/2f55a7976ad93e7a48e40eb92491d97a642f4809))
* **pixi:** added utils ([38799f0](https://gitlab.com/robotandkid/consulting/commit/38799f06419f37879fc0b684188d9e3f011e52c3))
* **pixi:** migrated awesome library ([7fa73f2](https://gitlab.com/robotandkid/consulting/commit/7fa73f21f500d48b1505bfbbab5ba08775e824bd))
