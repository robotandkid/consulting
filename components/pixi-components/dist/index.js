import { m } from 'framer-motion';
import 'pixi-sound';
import { Loader, Application, settings, SCALE_MODES, Point, Filter, Sprite, Graphics, Container, Text } from 'pixi.js';
import React, { useState, useContext, useRef, useEffect, useCallback, useLayoutEffect, useMemo, cloneElement } from 'react';
import { AdjustmentFilter, ColorReplaceFilter, ZoomBlurFilter, TiltShiftFilter, ReflectionFilter, PixelateFilter, OutlineFilter, OldFilmFilter, MotionBlurFilter, KawaseBlurFilter, GodrayFilter, GlowFilter, GlitchFilter, DropShadowFilter, DotFilter, CRTFilter } from 'pixi-filters';
import '@pixi/math-extras';
import { Graphics as Graphics$1 } from '@pixi/graphics';
import { Engine, Runner, Bodies, Composite } from 'matter-js';

function rgbToHex(color) {
    return color ? +`0x${color.replace(/#/g, "")}` : undefined;
}

function createComponent(hook) {
    return (props) => {
        hook(props);
        return null;
    };
}

function createContext(initial) {
    return React.createContext(initial ?? {});
}
function makeReadOnlyContext() {
    const context = createContext();
    const provider = (props) => {
        const { children, value } = props;
        return React.createElement(context.Provider, { value: value }, children);
    };
    return [provider, context];
}
function makeSimpleContext(name) {
    const context = createContext();
    const updateContext = createContext();
    const Provider = (props) => {
        const { children, initialValue } = props;
        const [state, setState] = useState(initialValue);
        return (React.createElement(updateContext.Provider, { value: setState },
            React.createElement(context.Provider, { value: state }, children)));
    };
    if (name)
        Provider.displayName = `${name}Provider`;
    return [Provider, context, updateContext];
}

const PixiAppContext = createContext();
const PixiAppTickerContext = createContext();
const PixiCanvasRefContext = createContext();
const PixiLoaderResourcesContext = createContext();

const pixiTapEvent = "pixi:tap";
const pixiDoubleTapEvent = "pixi:doubletap";
const pixiDragLeftEvent = "pixi:dragleft";
const pixiDragRightEvent = "pixi:dragright";

const rightArrow = 39;
const leftArrow = 37;
const doubleKeyPressThresholdInMs = 300;
const [PixiEventProvider, PixiEventContext, PixiEventUpdateContext] = makeSimpleContext("PixiEvent");
function useEventListener(props) {
    const { element, event, callback } = props;
    const fnsRef = useRef(callback);
    const eventRef = useRef(event);
    useEffect(() => {
        fnsRef.current = callback;
        eventRef.current = event;
    });
    useEffect(function addEventListener() {
        if (!element)
            return;
        const { listener } = fnsRef.current();
        const _event = eventRef.current;
        element.addEventListener(_event, listener);
        return () => {
            element.removeEventListener(_event, listener);
        };
    }, [element]);
}
function SetupListeners() {
    const canvasRef = useContext(PixiAppContext)?.view;
    const broadcast = useContext(PixiEventUpdateContext);
    useEventListener({
        event: "keydown",
        element: canvasRef,
        callback: function _leftArrow() {
            let previousTime = Date.now();
            return {
                listener: function detectLeftArrow(ev) {
                    if (ev.keyCode === leftArrow) {
                        const curTime = Date.now();
                        if (curTime - previousTime < doubleKeyPressThresholdInMs) {
                            broadcast({ type: "previousEventDoublePress" });
                        }
                        else {
                            broadcast({ type: "previousEvent" });
                        }
                        previousTime = curTime;
                    }
                },
            };
        },
    });
    useEventListener({
        event: "keydown",
        element: canvasRef,
        callback: function _rightArrow() {
            let previousTime = Date.now();
            return {
                listener: function detectRightArrow(ev) {
                    if (ev.keyCode === rightArrow) {
                        const curTime = Date.now();
                        if (curTime - previousTime < doubleKeyPressThresholdInMs) {
                            broadcast({ type: "nextEventDoublePress" });
                        }
                        else {
                            broadcast({ type: "nextEvent" });
                        }
                        previousTime = curTime;
                    }
                },
            };
        },
    });
    useEventListener({
        event: pixiTapEvent,
        element: canvasRef,
        callback: () => {
            return {
                listener: function detect() {
                    broadcast({ type: "nextEvent" });
                },
            };
        },
    });
    useEventListener({
        event: pixiDragRightEvent,
        element: canvasRef,
        callback: () => {
            return {
                listener: function detect() {
                    broadcast({ type: "nextEvent" });
                },
            };
        },
    });
    useEventListener({
        event: pixiDragLeftEvent,
        element: canvasRef,
        callback: () => {
            return {
                listener: function detect() {
                    broadcast({ type: "previousEvent" });
                },
            };
        },
    });
    useEventListener({
        event: pixiDoubleTapEvent,
        element: canvasRef,
        callback: () => {
            return {
                listener: function detect() {
                    broadcast({ type: "nextEventDoublePress" });
                },
            };
        },
    });
    return null;
}
function PixiEvents(props) {
    return (React.createElement(PixiEventProvider, { initialValue: undefined },
        React.createElement(SetupListeners, null),
        props.children));
}
function usePixiEvents() {
    return useContext(PixiEventContext);
}
/**
 * If you want to register callbacks for the next/previous events, use this.
 */
function RegisterPixiEventListeners(props) {
    const { onNextEvent, onNextEventDoublePress, onPreviousEvent, onPreviousEventDoublePress, } = props;
    const onNextEventRef = useRef(onNextEvent);
    const onNextEventDoublePressRef = useRef(onNextEventDoublePress);
    const onPreviousEventRef = useRef(onPreviousEvent);
    const onPreviousEventDoublePressRef = useRef(onPreviousEventDoublePress);
    useEffect(() => {
        onNextEventRef.current = onNextEvent;
        onNextEventDoublePressRef.current = onNextEventDoublePress;
        onPreviousEventRef.current = onPreviousEvent;
        onPreviousEventDoublePressRef.current = onPreviousEventDoublePress;
    });
    const event = usePixiEvents();
    useEffect(() => {
        if (event?.type === "nextEvent") {
            onNextEventRef.current?.();
        }
    }, [event]);
    useEffect(() => {
        if (event?.type === "nextEventDoublePress") {
            onNextEventDoublePressRef.current?.();
        }
    }, [event]);
    useEffect(() => {
        if (event?.type === "previousEvent") {
            onPreviousEventRef.current?.();
        }
    }, [event]);
    useEffect(() => {
        if (event?.type === "previousEventDoublePress") {
            onPreviousEventDoublePressRef.current?.();
        }
    }, [event]);
    return null;
}

function useLoadResources(props) {
    const { resourcesToLoad, onResourcesChange } = props;
    const loader = useRef(new Loader());
    useEffect(
    // Need to handle
    // - [x] initial load
    // - [x] resource change
    // - clearing out resources
    function loadResources() {
        if (!resourcesToLoad || resourcesToLoad.length === 0) {
            onResourcesChange({});
            return;
        }
        resourcesToLoad.forEach((resource) => {
            const { id, path } = {
                id: `${resource.type}:${resource.path}`,
                path: resource.path,
            };
            if (!loader.current.resources[id]) {
                if (resource.type === "image") {
                    loader.current.add({
                        name: id,
                        url: path,
                        loadType: 2,
                    });
                }
                else {
                    loader.current.add(id, path);
                }
            }
        });
        loader.current.load((_, resources) => {
            onResourcesChange({ ...resources });
        });
    }, [onResourcesChange, resourcesToLoad]);
}
const PixiResources = createComponent(useLoadResources);

function useLoadPixiApp(props) {
    const { onAppInit } = props;
    const initCalled = useRef(false);
    const application = useRef();
    useEffect(function resize() {
        if (application.current) {
            application.current.renderer.resize(props.width, props.height);
        }
    }, [props.height, props.width]);
    useEffect(function create() {
        if (initCalled.current) {
            return;
        }
        initCalled.current = true;
        const instance = new Application({
            width: props.width,
            height: props.height,
            backgroundColor: rgbToHex(props.backgroundColor) ?? 0xffffff,
            resolution: props.resolution ?? (window.devicePixelRatio || 1),
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            view: props.pixiRef.current,
        });
        // Scale mode for all textures, will retain pixelation
        settings.SCALE_MODE = SCALE_MODES.NEAREST;
        // enable key handlers
        instance.view.tabIndex = 0;
        onAppInit?.(instance);
        application.current = instance;
    }, [
        onAppInit,
        props.backgroundColor,
        props.height,
        props.pixiRef,
        props.resolution,
        props.width,
    ]);
}
const LoadPixiApp = createComponent(useLoadPixiApp);
function PixiApp(props) {
    const { onCanvasClick, onCanvasFocus, onAppInit } = props;
    const [resources, setResources] = useState();
    const [application, setApplication] = useState();
    const pixiRef = useRef(null);
    useEffect(function () {
        if (onCanvasClick) {
            window.addEventListener("click", onCanvasClick);
        }
        if (onCanvasFocus) {
            window.addEventListener("focus", onCanvasFocus);
        }
        return () => {
            onCanvasClick && window.removeEventListener("click", onCanvasClick);
            onCanvasFocus && window.removeEventListener("focus", onCanvasFocus);
        };
    }, [onCanvasClick, onCanvasFocus]);
    return (React.createElement(React.Fragment, null,
        React.createElement(m.canvas, { layoutId: props.layoutId, ref: pixiRef, width: props.width, height: props.height }),
        React.createElement(PixiResources, { resourcesToLoad: props.resources, onResourcesChange: setResources }),
        React.createElement(LoadPixiApp, { ...props, pixiRef: pixiRef, onAppInit: useCallback((app) => {
                onAppInit?.(app);
                setApplication(app);
            }, [onAppInit]) }),
        React.createElement(PixiAppContext.Provider, { value: application },
            React.createElement(PixiCanvasRefContext.Provider, { value: application?.view },
                React.createElement(PixiAppTickerContext.Provider, { value: application?.ticker },
                    React.createElement(PixiLoaderResourcesContext.Provider, { value: resources },
                        React.createElement(PixiEvents, null, props.children)))))));
}
function PixiAppWrapper(props) {
    const { children: Comp, ...rest } = props;
    return (React.createElement(PixiApp, { ...rest },
        React.createElement(Comp, null)));
}

function isApplication(cmp) {
    return !!cmp?.stage;
}
function isContainer(cmp) {
    return !!cmp?.addChild;
}
function getDims(cmp) {
    if (isApplication(cmp)) {
        return {
            width: cmp.screen.width,
            height: cmp.screen.height,
        };
    }
    if (isContainer(cmp)) {
        return {
            width: cmp.width,
            height: cmp.height,
        };
    }
    return {
        width: undefined,
        height: undefined,
    };
}
function getFilters(cmp) {
    const _cmp = cmp;
    if (isApplication(_cmp))
        return _cmp.stage;
    return _cmp;
}
const isDisplayObjectArray = (array) => !array.some((item) => !item);
function useAddToParent(props) {
    const { child } = props;
    const [added, setAdded] = useState(false);
    const app = useContext(PixiAppContext);
    let stage;
    if (props.parentType === "app")
        stage = app?.stage;
    else if (props.parentType === "container")
        stage = isApplication(props.parent) ? props.parent.stage : props.parent;
    useEffect(() => {
        if (!child || !stage || !app)
            return;
        const children = Array.isArray(child) ? child : [child];
        if (!isDisplayObjectArray(children))
            return;
        stage.addChild(...children);
        setAdded(true);
        return () => {
            stage?.removeChild(...children);
            setAdded(false);
        };
    }, [stage, child, app]);
    if (props.parentType === "none")
        return true;
    return added;
}

function instantiatePixiFilter(cmd) {
    switch (cmd.filter) {
        case "crt": {
            const filter = new CRTFilter({
                lineWidth: 3,
                lineContrast: 0.3,
                noise: 0.2,
                time: 0.5,
                vignetting: 0,
            });
            return {
                filter,
                animation() {
                    filter.seed = Math.random();
                    filter.time += 0.5;
                },
            };
        }
        case "dot":
            return {
                filter: new DotFilter(cmd.scale, cmd.radius),
                animation: undefined,
            };
        case "dropshadow":
            return { filter: new DropShadowFilter({}), animation: undefined };
        case "glitch": {
            const filter = new GlitchFilter({
                red: new Point(2, 2),
                blue: new Point(10, -4),
                green: new Point(-10, 4),
                slices: cmd.slices ?? 10,
                offset: cmd.offset ?? 100,
                direction: 0,
                fillMode: 2,
                average: false,
                seed: Math.random(),
                minSize: 8,
                sampleSize: 1,
            });
            return {
                filter,
                animation() {
                    filter.seed = Math.random();
                },
            };
        }
        case "glow":
            return {
                filter: new GlowFilter({
                    distance: 15,
                    outerStrength: cmd.outerStrength ?? 2,
                    innerStrength: cmd.innerStrength ?? 0,
                    color: rgbToHex(cmd.color ?? "#ffffff"),
                    quality: cmd.quality ?? 0.2,
                    knockout: false,
                }),
                animation: undefined,
            };
        case "godray": {
            const filter = new GodrayFilter({
                alpha: 1,
                angle: 30,
                gain: 0.6,
                lacunarity: 2.75,
                center: new Point(100, -100),
                parallel: true,
                time: 0,
            });
            return {
                filter,
                animation(elapsedTimeMs) {
                    filter.time += elapsedTimeMs / 1000;
                },
            };
        }
        case "kawaseblur":
            return {
                filter: new KawaseBlurFilter([4, 3]),
                animation: undefined,
            };
        case "motionblur":
            return {
                filter: new MotionBlurFilter([40, 40], 15),
                animation: undefined,
            };
        case "oldfilm": {
            const filter = new OldFilmFilter();
            return {
                filter,
                animation() {
                    filter.seed = Math.random();
                },
            };
        }
        case "outline": {
            const filter = new OutlineFilter(4, 0x0, 0.25);
            return { filter, animation: undefined };
        }
        case "pixelate":
            return {
                filter: new PixelateFilter(cmd.size || 3),
                animation: undefined,
            };
        case "reflection": {
            const filter = new ReflectionFilter();
            return {
                filter,
                animation() {
                    filter.time += 0.1;
                },
            };
        }
        case "tiltshift":
            return { filter: new TiltShiftFilter(), animation: undefined };
        case "zoomblur":
            return {
                filter: new ZoomBlurFilter({
                    strength: 0.1,
                    innerRadius: 80,
                }),
                animation: undefined,
            };
        case "colorreplace":
            return {
                filter: new ColorReplaceFilter(cmd.originalColor, cmd.newColor, cmd.epsilon),
                animation: undefined,
            };
        case "adjustment":
            return {
                filter: new AdjustmentFilter({
                    gamma: cmd.gamma,
                    saturation: cmd.saturation,
                    contrast: cmd.contrast,
                    brightness: cmd.brightness,
                    red: cmd.red,
                    green: cmd.green,
                    blue: cmd.blue,
                    alpha: cmd.alpha,
                }),
                animation: undefined,
            };
    }
}

function filterMap(filters) {
    return filters.reduce((total, filter) => Object.assign(total, { [filter.constructor.name]: true }), {});
}
/**
 * @deprecated use version2
 */
function useAddPixiFilter(props) {
    const { comp, filters } = props;
    const instantiatedFilters = useMemo(() => {
        if (!filters)
            return [];
        const filtersAsArray = Array.isArray(filters) ? filters : [filters];
        return filtersAsArray.map(
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        (filter) => instantiatePixiFilter(filter));
    }, [filters]);
    return useAddPixiFilter2({
        comp,
        filters: instantiatedFilters,
    });
}
/**
 * The only catch with this method is that changing the filters doesn't trigger
 * a re-render. But you can use the force render prop to force a re-render.
 */
function useAddPixiFilter2(props) {
    const { comp, filters } = props;
    const appTicker = useContext(PixiAppTickerContext);
    const [addedFilters, setAddedFilters] = useState([]);
    const filtersRef = useRef(filters);
    useLayoutEffect(() => {
        filtersRef.current = filters;
    });
    useEffect(function addFilter() {
        if (!comp || !appTicker || !filtersRef.current) {
            return;
        }
        let dedupedFilters = [];
        const filtersAsArray = Array.isArray(filtersRef.current)
            ? filtersRef.current
            : [filtersRef.current];
        const _filterClassnames = filterMap(getFilters(comp).filters || []);
        dedupedFilters = filtersAsArray.filter(({ filter }) => !_filterClassnames[filter.constructor.name]);
        setAddedFilters(dedupedFilters);
        appTicker.addOnce(() => {
            // now add the filters
            getFilters(comp).filters = [
                ...(getFilters(comp).filters || []),
                ...dedupedFilters.map(({ filter }) => filter),
            ];
        });
        return () => {
            appTicker.addOnce(() => {
                getFilters(comp).filters = null;
            });
        };
    }, [appTicker, comp]);
    useEffect(function triggerAnimations() {
        if (!appTicker)
            return;
        const callbacks = [];
        addedFilters.forEach(({ filter, animation, onStart }) => {
            let skipFirst = false;
            if (onStart) {
                appTicker.addOnce(() => onStart(0, filter));
                skipFirst = true;
            }
            if (animation) {
                let elapsedMS = 0;
                const callback = () => {
                    if (skipFirst === true) {
                        skipFirst = "done";
                        return;
                    }
                    elapsedMS += appTicker.elapsedMS / 16.66;
                    animation(elapsedMS, filter);
                };
                callbacks.push(callback);
                appTicker.add(callback);
            }
        });
        return () => {
            callbacks.forEach((callback) => {
                appTicker.remove(callback);
            });
        };
    }, [appTicker, addedFilters]);
    return addedFilters.length > 0 ? comp : undefined;
}
/**
 * @deprecated use version2
 */
const PixiFilter = createComponent(useAddPixiFilter);
const PixiFilter2 = createComponent(useAddPixiFilter2);

function defaultDotFilter(opts = {}) {
    return {
        filter: new DotFilter(opts.scale ?? 1, opts.radius ?? 1),
        animation: undefined,
    };
}

function defaultGlitchFilter(opts = {}) {
    const filter = new GlitchFilter({
        red: opts.red ?? [2, 2],
        blue: opts.blue ?? [10, -4],
        green: opts.green ?? [-10, 4],
        slices: opts.slices ?? 10,
        offset: opts.offset ?? 100,
        direction: opts.direction ?? 0,
        fillMode: opts.fillMode ?? 2,
        average: opts.average ?? false,
        seed: opts.seed ?? Math.random(),
        minSize: opts.seed ?? 8,
        sampleSize: opts.sampleSize ?? 1,
    });
    return {
        filter,
        animation() {
            filter.seed = Math.random();
        },
    };
}

function defaultGlowFilter(opts) {
    return {
        filter: new GlowFilter({
            distance: 15,
            outerStrength: opts.outerStrength ?? 2,
            innerStrength: opts.innerStrength ?? 0,
            color: rgbToHex(opts.color ?? "#ffffff"),
            quality: opts.quality ?? 0.2,
            knockout: false,
        }),
        animation: undefined,
    };
}

function defaultOldFilmFilter(props = {}) {
    return {
        filter: new OldFilmFilter(props),
        animation(_, filter) {
            filter.seed = Math.random();
        },
    };
}

function defaultPixelateFilter(opts) {
    return {
        filter: new PixelateFilter(opts.size || 3),
        animation: undefined,
    };
}

function usePixiShader(props) {
    const { frag, comp, uniforms, animate, onInit } = props;
    const resources = useContext(PixiLoaderResourcesContext);
    const app = useContext(PixiAppContext);
    const [shader, setShader] = useState();
    const [compWithShader, setCompWithShader] = useState();
    const uniformsRef = useRef(uniforms);
    const animateRef = useRef(animate);
    const onInitRef = useRef(onInit);
    useLayoutEffect(() => {
        uniformsRef.current = uniforms;
        animateRef.current = animate;
        onInitRef.current = onInit;
    });
    useEffect(() => {
        if (!frag || !app || !comp)
            return;
        onInitRef.current?.(comp, app);
        const filter = new Filter(undefined, frag, {
            ...(uniformsRef.current ?? {}),
        });
        setShader(filter);
    }, [app, frag, resources, comp]);
    useEffect(function animate() {
        if (!app || !shader || !comp)
            return;
        comp.filters = [shader];
        let animate;
        if (animateRef.current) {
            animate = (delta) => {
                if (shader)
                    animateRef.current?.(shader, delta);
            };
            app.ticker.add(animate);
        }
        setCompWithShader(comp);
        return () => {
            if (animate)
                app.ticker.remove(animate);
        };
    }, [app, comp, shader]);
    return comp ? { comp: compWithShader, shader } : undefined;
}

const frag = (command, ...values) => {
    return command.reduce((total, cmd, index) => (total += `${cmd}${values[index] ?? ""}`), "");
};
const shader = (precision) => frag `
precision ${precision} float;

uniform float time;
uniform vec2 u_resolution;

const float count = 10.0;
const float speed = 2.0;

float Hash(vec2 p, in float s) {
    vec3 p2 = vec3(p.xy, 17.0 * abs(sin(s)));
    return fract(sin(dot(p2, vec3(5.1, 36.7, 12.4))) * 258.5453123);
}

float noise(in vec2 p, in float s) {
    vec2 i = floor(p);
    vec2 f = fract(p);
    f *= f * (3.0 - 2.0 * f);

    return mix(mix(Hash(i + vec2(0., 0.), s), Hash(i + vec2(1., 0.), s), f.x), mix(Hash(i + vec2(0., 1.), s), Hash(i + vec2(1., 1.), s), f.x), f.y) * s;
}

float fbm(vec2 p) {
    float v = -noise(p * 02., 0.25);
    v += noise(p * 01.1, 0.5) - noise(p * 01.1, 0.25);
    v += noise(p * 09.1, 0.25) - noise(p * 2.1, 0.125);
    v += noise(p * 04.1, 0.125) - noise(p * 08.1, 0.0625);
    v += noise(p * 08.1, 0.0625) - noise(p * 16., 0.015);
    v += noise(p * 16.1, 0.03125);
    v += noise(p * 42.1, 0.0125);
    return v;
}

void main() {
    float worktime = (time * (speed * 1.5)) + 1.0;

    vec2 uv = (gl_FragCoord.xy / u_resolution.xy) * 2.0 - 1.225;
    uv.y *= u_resolution.y / u_resolution.x;

    vec3 finalColor = vec3(0.0, 0.0, 0.0);
    for(float i = 1.0; i < count + 1.0; i++) {
        float t = abs(1.01 / ((uv.y + fbm(uv + worktime / i)) * (i * 35.0)));
        finalColor += t * vec3(1.95, 1.5, .9);
    }

    gl_FragColor = vec4(finalColor, 1.0);
}
`;
function useLightningShader(props) {
    return usePixiShader({
        comp: props.comp,
        frag: shader(props.uniforms.u_precision),
        uniforms: {
            time: props.uniforms.u_time,
            u_resolution: new Point(props.uniforms.u_resolution[0], props.uniforms.u_resolution[1]),
        },
        animate(filter, delta) {
            filter.uniforms.time += delta * props.uniforms.u_speed;
        },
    });
}

function fitSpriteInContainer(props) {
    const originalAspectRatio = props.height / props.width;
    const computedWidth = props.containerWidth;
    const computedHeight = props.containerWidth * originalAspectRatio;
    if (computedHeight <= props.containerHeight) {
        return { computedWidth, computedHeight };
    }
    return {
        computedHeight: props.containerHeight,
        computedWidth: props.containerHeight / originalAspectRatio,
    };
}
/**
 * Loads image and resizes it (to scale) to fit
 * inside the container
 *
 * @param props
 */
function useLoadPixiSprite(props) {
    const resources = useContext(PixiLoaderResourcesContext);
    const [data, setData] = useState();
    useEffect(function loadSprite() {
        if (!props.path || !resources) {
            return;
        }
        const { id, path } = {
            id: `image:${props.path}`,
            path: props.path,
        };
        if (!resources[id])
            return;
        const resource = resources[id]?.texture;
        const sprite = new Sprite(resource);
        const { computedWidth, computedHeight } = fitSpriteInContainer({
            height: sprite.height,
            width: sprite.width,
            containerWidth: props.width,
            containerHeight: props.height,
        });
        sprite.width = computedWidth;
        sprite.height = computedHeight;
        sprite.anchor.set(0.5);
        sprite.x = props.width / 2;
        sprite.y = props.height / 2;
        setData({
            resource: { id, path },
            sprite,
            computedHeight,
            computedWidth,
        });
    }, [props.height, props.path, props.width, resources]);
    const added = useAddToParent({
        ...props,
        child: data?.sprite,
    });
    return added ? data : undefined;
}

function defaultTextStyle() {
    return {
        fontFamily: "Arial",
        fontSize: 12,
        fontStyle: "italic",
        fontWeight: "bold",
        fill: ["#ffffff", "#00ff99"],
        stroke: "#4a1850",
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: "#000000",
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
        align: "center",
        wordWrap: true,
    };
}

function useGlowingEyes(props) {
    const { eyeColor = "#ffffff", radius = 1, blurRadius = 20, leftEye: { x: x1, y: y1 }, rightEye: { x: x2, y: y2 }, } = props;
    const [eyeGfx, setEyeGfx] = useState();
    const start = !!parent;
    useEffect(() => {
        if (!start)
            return;
        const eyes = new Graphics();
        eyes.lineStyle(0);
        eyes.beginFill(rgbToHex(eyeColor), 2);
        eyes.drawCircle(x1, y1, radius);
        eyes.endFill();
        eyes.beginFill(rgbToHex(eyeColor), 2);
        eyes.drawCircle(x2, y2, radius);
        eyes.endFill();
        setEyeGfx(eyes);
    }, [eyeColor, radius, start, x1, x2, y1, y2]);
    const glowingEyes = useAddPixiFilter2({
        comp: eyeGfx,
        filters: {
            filter: new GlowFilter({
                distance: blurRadius,
                color: rgbToHex(eyeColor),
                knockout: false,
                innerStrength: 20,
                outerStrength: 20,
                quality: 0.5,
            }),
        },
    });
    return glowingEyes;
}

/**
 *
 * Limit the magnitude of the point to the given max.
 */
function limit(p, max) {
    const x = p.x;
    const y = p.y;
    const l = p.magnitude();
    if (l > max) {
        const a = Math.atan2(y, x);
        return new Point(Math.cos(a) * max, Math.sin(a) * max);
    }
    return new Point(p.x, p.y);
}
/**
 * Returns the distance between two points.
 */
function distance(p, q) {
    return p.subtract(q).magnitude();
}
class Boid {
    get position() {
        return new Point(this._position.x, this._position.y);
    }
    get r() {
        return 1.0;
    }
    constructor(opts) {
        this.acceleration = new Point(0, 0);
        const angle = Math.random() * (2 * Math.PI);
        this.velocity = new Point(Math.cos(angle), Math.sin(angle));
        this._position = new Point(opts.x, opts.y);
        this.renderCallback = opts.render;
        this.config = {
            width: opts.config.width,
            height: opts.config.height,
            maxSpeed: opts.config.maxSpeed ?? 4,
            maxForce: opts.config.maxForce ?? 0.03,
            separationWeight: opts.config.separationWeight ?? 1.5,
            alignmentWeight: opts.config.alignmentWeight ?? 1,
            cohesionWeight: opts.config.cohesionWeight ?? 1,
        };
    }
    run(boids) {
        this.flock(boids);
        this.update();
        this.borders();
        this.renderCallback(this._position);
    }
    applyForce(force) {
        // We could add mass here if we want A = F / M
        this.acceleration = this.acceleration.add(force);
    }
    /**
     * We accumulate a new acceleration each time based on three rules
     */
    flock(boids) {
        let sep = this.separate(boids); // Separation
        let ali = this.align(boids); // Alignment
        let coh = this.cohesion(boids); // Cohesion
        // Arbitrarily weight these forces
        sep = sep.multiplyScalar(this.config.separationWeight);
        ali = ali.multiplyScalar(this.config.alignmentWeight);
        coh = coh.multiplyScalar(this.config.cohesionWeight);
        // Add the force vectors to acceleration
        this.applyForce(sep);
        this.applyForce(ali);
        this.applyForce(coh);
    }
    /**
     * Method to update position
     */
    update() {
        // Update velocity
        this.velocity = this.velocity.add(this.acceleration);
        // Limit speed
        this.velocity = limit(this.velocity, this.config.maxSpeed);
        this._position = this._position.add(this.velocity);
        // Reset acceleration to 0 each cycle
        this.acceleration.multiplyScalar(0);
    }
    /**
     * A method that calculates and applies a steering force towards a target
     *
     * STEER = DESIRED MINUS VELOCITY
     */
    seek(target) {
        const desired = target
            .subtract(this._position) // A vector pointing from the position to the target
            // Scale to maximum speed
            .normalize()
            .multiplyScalar(this.config.maxSpeed);
        // Steering = Desired minus Velocity
        const steer = desired.subtract(this.velocity);
        return limit(steer, this.config.maxForce);
    }
    /**
     * Wraparound
     */
    borders() {
        if (this._position.x < -this.r)
            this._position.x = this.config.width + this.r;
        if (this._position.y < -this.r)
            this._position.y = this.config.height + this.r;
        if (this._position.x > this.config.width + this.r)
            this._position.x = -this.r;
        if (this._position.y > this.config.height + this.r)
            this._position.y = -this.r;
    }
    /*
     * Separation Method checks for nearby boids and steers away
     */
    separate(boids) {
        const desiredSeparation = 25.0;
        let steer = new Point(0, 0);
        let count = 0;
        // For every boid in the system, check if it's too close
        for (const other of boids) {
            const d = distance(this._position, other._position);
            // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
            if (d > 0 && d < desiredSeparation) {
                const diff = this._position
                    .subtract(other._position)
                    // Calculate vector pointing away from neighbor
                    .normalize()
                    .multiplyScalar(1 / d); // Weight by distance
                steer = steer.add(diff);
                count++; // Keep track of how many
            }
        }
        // Average -- divide by how many
        if (count > 0) {
            steer = steer.multiplyScalar(1 / count);
        }
        // As long as the vector is greater than 0
        if (steer.magnitude() > 0) {
            // First two lines of code below could be condensed with newPoint setMag() method
            // Not using this method until Processing.js catches up
            // steer.setMag(maxspeed);
            // Implement Reynolds: Steering = Desired - Velocity
            steer = steer
                .normalize()
                .multiplyScalar(this.config.maxSpeed)
                .subtract(this.velocity);
            steer = limit(steer, this.config.maxForce);
        }
        return steer;
    }
    /**
     * Alignment - For every nearby boid in the system, calculate the average
     * velocity
     */
    align(boids) {
        const neighborDist = 50;
        let sum = new Point(0, 0);
        let count = 0;
        for (const other of boids) {
            const d = distance(this._position, other._position);
            if (d > 0 && d < neighborDist) {
                sum = sum.add(other.velocity);
                count++;
            }
        }
        if (count > 0) {
            sum = sum
                .multiplyScalar(1 / count)
                // Implement Reynolds: Steering = Desired - Velocity
                .normalize()
                .multiplyScalar(this.config.maxSpeed);
            const steer = sum.subtract(this.velocity);
            return limit(steer, this.config.maxForce);
        }
        else {
            return new Point(0, 0);
        }
    }
    /**
     * Cohesion - For the average position (i.e. center) of all nearby boids,
     * calculate steering vector towards that position
     */
    cohesion(boids) {
        const neighborDist = 50;
        let sum = new Point(0, 0); // Start with empty vector to accumulate all positions
        let count = 0;
        for (const other of boids) {
            const d = distance(this._position, other._position);
            if (d > 0 && d < neighborDist) {
                sum = sum.add(other._position); // Add position
                count++;
            }
        }
        if (count > 0) {
            sum = sum.multiplyScalar(1 / count);
            return this.seek(sum); // Steer towards the position
        }
        else {
            return new Point(0, 0);
        }
    }
}

/**
 * This class is dumb and should be replaced by flocking.ts;
 */
class Flock {
    constructor() {
        this.boids = []; // An ArrayList for all the boids
    }
    get positions() {
        return this.boids.map((boid) => boid.position);
    }
    run() {
        for (const b of this.boids) {
            b.run(this.boids); // Passing the entire list of boids to each boid individually
        }
    }
    addBoid(b) {
        this.boids.push(b);
    }
}

const flocking = (opts) => {
    const flock = new Flock();
    const centerX = opts.config.width / 2;
    const centerY = opts.config.height / 2;
    // Add an initial set of boids into the system
    for (let i = 0; i < opts.config.count; i++) {
        const x = opts.config.randomInitialPosition
            ? Math.random() * opts.config.width
            : centerX;
        const y = opts.config.randomInitialPosition
            ? Math.random() * opts.config.height
            : centerY;
        const boid = new Boid({
            ...opts,
            render: opts.renderBoid,
            x,
            y,
        });
        opts.onStartBoid(boid.position);
        flock.addBoid(boid);
    }
    return {
        draw() {
            opts.preRender();
            flock.run();
        },
        // Add a new boid into the System
        mousePressed(mouseX, mouseY) {
            flock.addBoid(new Boid({
                ...opts,
                render: opts.renderBoid,
                x: mouseX,
                y: mouseY,
            }));
        },
    };
};

const defaultRenderBoid = (graphics, pos, flockColor) => {
    graphics.lineStyle(0);
    graphics.beginFill(rgbToHex(flockColor), 1);
    graphics.drawCircle(pos.x, pos.y, 3);
    graphics.endFill();
};
const defaultPreRender = (graphics) => {
    graphics.clear();
};
function fallback(graphics, defaultCallback, callback) {
    if (callback) {
        return (pos) => callback(graphics, pos);
    }
    return (pos) => defaultCallback?.(graphics, pos);
}
function useAddFlock(props = {}) {
    const app = useContext(PixiAppContext);
    const { start = false, config, ...callbacks } = props;
    const { count = 500, flockColor = "#ffffff", randomInitialPosition = true, separationWeight = 3, alignmentWeight, cohesionWeight, ...configProps } = config ?? {};
    const callbackRefs = useRef(callbacks);
    const configRefs = useRef(configProps);
    useLayoutEffect(() => {
        callbackRefs.current = callbacks;
        configRefs.current = configProps;
    });
    useEffect(function flock() {
        if (!app || !start)
            return;
        const container = new Container();
        container.width = app.screen.width;
        container.height = app.screen.height;
        const flockGraphics = new Graphics();
        container.addChild(flockGraphics);
        app.stage.addChild(container);
        const preRender = fallback(flockGraphics, callbackRefs.current.preRender, defaultPreRender);
        const onStartBoid = fallback(flockGraphics, callbackRefs.current.onStartBoid, (graphics, pos) => defaultRenderBoid(graphics, pos, flockColor));
        const renderBoid = fallback(flockGraphics, callbackRefs.current.onStartBoid, (graphics, pos) => defaultRenderBoid(graphics, pos, flockColor));
        const flock = flocking({
            config: {
                count,
                width: app.screen.width,
                height: app.screen.height,
                randomInitialPosition,
                separationWeight,
                alignmentWeight,
                cohesionWeight,
                ...configProps,
            },
            preRender: () => preRender({}),
            onStartBoid,
            renderBoid,
        });
        app.ticker.add(flock.draw);
        let lastTime = 0;
        const tick = setInterval(() => {
            if (++lastTime === 250) {
                clearInterval(tick);
                return;
            }
            flock.mousePressed(Math.random() * app.screen.width, Math.random() * app.screen.height);
        }, 300);
        return () => {
            clearInterval(tick);
            app.ticker.remove(flock.draw);
            app.stage.removeChild(container);
        };
    }, [
        alignmentWeight,
        app,
        cohesionWeight,
        configProps,
        count,
        flockColor,
        randomInitialPosition,
        separationWeight,
        start,
    ]);
}

function useObjectAsRef(obj) {
    const ref = useRef(obj);
    useLayoutEffect(() => {
        ref.current = obj;
    });
    return ref;
}

function drawOscilloscope(source, graphics, style, lineStyle) {
    const bufferLength = source.length;
    graphics.clear();
    graphics.x = style.x ?? 0;
    graphics.y = style.y ?? 0;
    graphics.beginFill(style.backgroundColor);
    graphics.drawRect(0, 0, style.width, style.height);
    graphics.endFill();
    graphics.lineStyle(lineStyle);
    const sliceWidth = (style.width * 1.0) / bufferLength;
    let x = 0;
    for (let i = 0; i < bufferLength; i++) {
        const v = source[i] / 128.0;
        const y = (v * style.height) / 2;
        if (i === 0) {
            graphics.moveTo(x, y);
        }
        else {
            graphics.lineTo(x, y);
        }
        x += sliceWidth;
    }
    graphics.lineTo(style.width, style.height / 2);
}
function Oscilloscope(props) {
    const { analyser, style, lineStyle, parent } = props;
    const styleRef = useObjectAsRef(style);
    const lineStyleRef = useObjectAsRef(lineStyle);
    const ticker = useContext(PixiAppTickerContext);
    const [child, setChild] = useState();
    const dataArray = useRef();
    const [ready, setReady] = useState(false);
    useEffect(function captureAudioData() {
        if (!analyser)
            return;
        analyser.fftSize = 2048;
        const bufferLength = analyser.fftSize;
        dataArray.current = new Uint8Array(bufferLength);
        const capture = () => {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            analyser.getByteTimeDomainData(dataArray.current);
            requestAnimationFrame(capture);
        };
        requestAnimationFrame(capture);
        setReady(true);
    }, [analyser]);
    useEffect(function analyse() {
        if (!parent || !ticker || !ready || !dataArray.current)
            return;
        const { width: parentWidth = 0, height: parentHeight = 0 } = getDims(parent);
        const { backgroundColor = "#ffffff", width = parentWidth, height = parentHeight, x, y, } = styleRef.current;
        const graphics = new Graphics$1();
        setChild(graphics);
        const _render = () => {
            if (!dataArray.current)
                return;
            drawOscilloscope(dataArray.current, graphics, {
                backgroundColor: rgbToHex(backgroundColor),
                width,
                height,
                x,
                y,
            }, {
                ...lineStyleRef.current,
                color: rgbToHex(lineStyleRef.current.color),
            });
        };
        ticker.add(_render);
        return () => {
            ticker.remove(_render);
            graphics?.destroy();
        };
    }, 
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [parent, ticker, ready]);
    useAddToParent({
        child: child,
        parentType: "container",
        parent: parent,
    });
    return null;
}

function useTypeEffect(props) {
    const { event, typeAppTicker: type } = props;
    const appTicker = useContext(PixiAppTickerContext);
    const currentTick = useRef(0);
    // appTicker runs at 60 frames per second
    // so to get, for example, a typing delay of 500ms
    // you need to run the ticker every 30 frames
    // => 30 frames =  1 / 2 second *  60 frames / second
    const delayInSec = props.delayMs / 1000;
    const tickRate = Math.ceil(delayInSec * 60);
    useEffect(function typeEffect() {
        if (!appTicker) {
            return;
        }
        function typeTicker() {
            if (event === "stop") {
                return;
            }
            currentTick.current += 1;
            if (currentTick.current === tickRate) {
                currentTick.current = 0;
                if (event === "type") {
                    type();
                }
            }
        }
        if (event === "type") {
            appTicker.add(typeTicker);
        }
        return () => {
            appTicker.remove(typeTicker);
        };
    }, [appTicker, event, tickRate, type]);
}
function usePixiText(parent, style) {
    const { width: parentWidth = 0, height: parentHeight = 0 } = getDims(parent);
    const { backgroundColor, width, height, top, left, ...textStyle } = style;
    const textStyleRef = useObjectAsRef(textStyle);
    const styleRef = useObjectAsRef({
        backgroundColor,
        width,
        height,
        top,
        left,
    });
    return useMemo(() => {
        const container = new Graphics();
        const _width = styleRef.current.width ?? parentWidth;
        const _height = styleRef.current.height ?? parentHeight;
        container.width = _width;
        container.height = _height;
        container.x = styleRef.current.left ?? 0;
        container.y = styleRef.current.top ?? 0;
        if (styleRef.current.backgroundColor) {
            container
                .lineStyle(0)
                .beginFill(rgbToHex(styleRef.current.backgroundColor), 2)
                .drawRect(0, 0, _width, _height)
                .endFill();
        }
        const text = new Text("", textStyleRef.current);
        text.anchor.set(0.5);
        text.x = (textStyleRef.current.wordWrapWidth ?? _width) / 2;
        text.y = _height / 2;
        container.addChild(text);
        return {
            containerCmp: container,
            textCmp: text,
            setPixiText(_text) {
                text.text = _text;
            },
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [parentWidth, parentHeight]);
}
/**
 * Types the given text with the given typing speed, and calls onDone when
 * finished.
 *
 * - Change the `event` to `stop`, to jump to the done state.
 * - Change the `event` to `type` to restart typing
 *
 * **Note:** Because of the way React components work, changing the `event` to
 * `type` by itself _twice_ in a row will *not* restart the component _twice_.
 * _In order to achieve this effect, you must also change the lineId_.
 */
function _TypewriterText(props) {
    const { lineId = "0", event = "type", children: textFromProps = "", onLineDone: onDone, onLineDoneDelayMs: onDoneDelayMs, } = props;
    const appTicker = useContext(PixiAppTickerContext);
    const resources = useContext(PixiLoaderResourcesContext);
    const [finishedTyping, setFinishedTyping] = useState(false);
    const { containerCmp, textCmp, setPixiText } = usePixiText(props.parent, props.style);
    useTypeEffect({
        event,
        delayMs: props.typingSpeedInMs,
        typeAppTicker: useCallback(() => {
            if (textCmp.text === textFromProps) {
                setFinishedTyping(true);
                return;
            }
            const newText = textFromProps.substring(0, textCmp.text.length + 1);
            setPixiText(newText);
        }, [textCmp, setPixiText, textFromProps]),
    });
    useEffect(function handleRestartEvent() {
        // changes to any of these props need to trigger a restart
        if (textFromProps &&
            typeof lineId !== "undefined" &&
            event === "type" &&
            appTicker) {
            setFinishedTyping(false);
            appTicker.addOnce(() => {
                setPixiText("");
            });
        }
    }, [appTicker, event, lineId, setPixiText, textFromProps]);
    useEffect(function handleEndEvent() {
        if (textFromProps.includes(textCmp.text) &&
            textCmp.text !== textFromProps &&
            event === "stop" &&
            appTicker) {
            setFinishedTyping(true);
            appTicker.addOnce(() => {
                setPixiText(textFromProps);
            });
        }
    }, [appTicker, event, textCmp, setPixiText, textFromProps]);
    useEffect(function handleDoneEvent() {
        let unsubscribe;
        // changes that trigger a restart need to kill the onDone timer
        if (textFromProps &&
            typeof lineId !== "undefined" &&
            (finishedTyping || event === "stop") &&
            onDone) {
            unsubscribe = setTimeout(() => {
                onDone();
            }, onDoneDelayMs || 0);
        }
        return () => {
            if (typeof unsubscribe !== "undefined") {
                clearTimeout(unsubscribe);
            }
        };
    }, [event, finishedTyping, lineId, onDone, onDoneDelayMs, textFromProps]);
    const textScrollId = `sound:${props.soundPath}`;
    useEffect(function sound() {
        if (finishedTyping || event === "stop") {
            resources?.[textScrollId]?.sound.stop();
        }
        else if (event === "type") {
            resources?.[textScrollId]?.sound.play({ loop: true });
        }
        return () => {
            resources?.[textScrollId]?.sound.stop();
        };
    }, [event, finishedTyping, resources, textScrollId]);
    useEffect(function turnOffSound() {
        return () => {
            resources?.[textScrollId]?.sound.stop();
        };
    }, 
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []);
    useAddToParent({
        child: containerCmp,
        parent: props.parent,
        parentType: "container",
    });
    return null;
}
function TypewriterText(props) {
    const { onLineDone: onDone } = props;
    const [event, setEvent] = useState(props.children ? "type" : "stop");
    const pixiEvent = usePixiEvents();
    useEffect(function handleNextEvent() {
        if (pixiEvent?.type === "nextEvent") {
            setEvent((cmd) => {
                if (cmd === "stop") {
                    onDone?.();
                    return cmd;
                }
                else {
                    return "stop";
                }
            });
        }
    }, [onDone, pixiEvent]);
    useEffect(function handleNextEventDoublePress() {
        if (pixiEvent?.type === "nextEventDoublePress") {
            onDone?.();
        }
    }, [onDone, pixiEvent]);
    useEffect(function handlePreviousEvent() {
        if (pixiEvent?.type === "previousEvent") {
            setEvent("type");
        }
    }, [pixiEvent]);
    useEffect(function whenTextChangesRestart() {
        if (props.children) {
            setEvent(undefined);
        }
    }, [props.children]);
    return React.createElement(_TypewriterText, { ...props, event: event });
}

const [PhysicsProvider, PhysicsContext] = makeReadOnlyContext();
/**
 * Reveals children one at a time. The last child is passed the onNext prop.
 *
 * An alternative is to create an onNext context. We can let children register
 * themselves and possibly auto advanced if they never register.
 *
 * The only downside to this alternate version is that we have no way of
 * preventing a random grand-child from calling onNext.
 */
function RevealChildren(props) {
    const { onNext: onParentNext, currentScene } = props;
    const originalChildren = useMemo(() => {
        const array = Array.isArray(props.children)
            ? props.children
            : [props.children];
        return array.map((child, key) => cloneElement(child, { key }));
    }, [props.children]);
    const [index, setIndex] = useState({ index: 0, key: 0 });
    const [children, setChildren] = useState([
        originalChildren[index.index],
    ]);
    const onChildNext = useCallback(() => setIndex(({ index, key }) => ({ index: index + 1, key })), []);
    useEffect(() => {
        let callback = onChildNext;
        if (onParentNext && index.index === originalChildren.length - 1) {
            callback = onParentNext;
        }
        if (!originalChildren[index.index])
            return;
        const _children = originalChildren
            .slice(0, index.index)
            .map((child, key) => cloneElement(child, { key: `${index.key}-${key}` }));
        _children.push(cloneElement(originalChildren[index.index], {
            key: `${index.key}-${index.index}`,
            onNext: callback,
        }));
        setChildren(_children);
    }, [index, onChildNext, onParentNext, originalChildren]);
    useEffect(() => {
        if (currentScene?.index === "next") {
            setIndex(({ index, key }) => ({ index: index + 1, key: key + 1 }));
        }
    }, [currentScene]);
    useEffect(() => {
        if (currentScene?.index === "prev") {
            setIndex(({ index, key }) => ({
                index: Math.max(index - 1, 0),
                key: key + 1,
            }));
        }
    }, [currentScene]);
    useEffect(() => {
        if (currentScene?.index === "restart") {
            setIndex(({ key }) => ({
                index: 0,
                key: key + 1,
            }));
        }
    }, [currentScene]);
    return React.createElement(React.Fragment, null, children);
}
/**
 * - Press the right arrow and advance to the next scene. If at the last scene,
 *   then jump to the last node.
 * - Press the left arrow and advance to the previous scene. If at the first
 *   scene, then restart the first scene.
 */
function SceneProvider(props) {
    const { physics } = props;
    const physicsRef = useRef(physics);
    useEffect(() => {
        physicsRef.current = physics;
    });
    const physicsContext = useMemo(() => ({
        engine: Engine.create(physicsRef.current?.engine),
        runner: Runner.create(),
    }), []);
    useEffect(function setupPhysics() {
        Runner.run(physicsContext.runner, physicsContext.engine);
    }, [physicsContext]);
    const [currentScene, setCurrentScene] = useState();
    return (React.createElement(PhysicsProvider, { value: physicsContext },
        React.createElement(RegisterPixiEventListeners, { onNextEventDoublePress: () => setCurrentScene({ index: "next" }), onNextEvent: () => setCurrentScene({ index: "next" }), onPreviousEventDoublePress: () => setCurrentScene({ index: "prev" }), onPreviousEvent: () => setCurrentScene({ index: "prev" }) }),
        React.createElement(RevealChildren, { currentScene: currentScene, onNext: () => setCurrentScene({ index: "restart" }) }, props.children)));
}
function Scene(props) {
    return React.createElement(RevealChildren, { ...props });
}
const [FilterableComponentProvider, FilterableComponentContext] = makeReadOnlyContext();
function Background(props) {
    const { onNext, width, height } = props;
    const sprite = useLoadPixiSprite({
        path: props.path,
        width,
        height,
        parentType: "app",
    });
    useEffect(() => {
        if (sprite) {
            onNext?.();
        }
    }, [onNext, sprite]);
    return (React.createElement(FilterableComponentProvider, { value: sprite?.sprite }, props.children));
}
function Delay(props) {
    const { timeout, onNext } = props;
    useEffect(() => {
        if (!onNext)
            return;
        const _timeout = setTimeout(() => onNext(), timeout);
        return () => {
            clearTimeout(_timeout);
        };
    }, [onNext, timeout]);
    const app = useContext(PixiAppContext);
    return (React.createElement(FilterableComponentProvider, { value: app }, props.children));
}
function PhysicsSprite(props) {
    const { onNext, path, x, y, width, height } = props;
    const app = useContext(PixiAppContext);
    const spriteData = useLoadPixiSprite({
        path: path,
        width: app?.renderer.width ?? 0,
        height: app?.renderer.height ?? 0,
        parentType: "app",
    });
    const sprite = spriteData?.sprite;
    const physics = useContext(PhysicsContext);
    useEffect(function createPhysicsBody() {
        if (!sprite && path)
            return;
        const spriteBody = Bodies.rectangle(x, y, width ?? sprite?.width ?? 0, height ?? sprite?.height ?? 0);
        Composite.add(physics.engine.world, spriteBody);
        return () => {
            Composite.remove(physics.engine.world, spriteBody);
        };
    }, [height, path, physics.engine.world, sprite, width, x, y]);
    useEffect(() => {
        if (sprite) {
            onNext?.();
        }
    }, [sprite, onNext]);
    useEffect(function syncPosition() {
        function animate() {
            for (const dot of physics.engine.world.bodies) {
                if (dot.isStatic || !sprite)
                    continue;
                const { x, y } = dot.position;
                sprite.position.x = x;
                sprite.position.y = y;
                break;
            }
        }
        if (!app || !sprite)
            return;
        app.ticker.add(animate);
        return () => {
            app.ticker.remove(animate);
        };
    }, [app, physics.engine.world.bodies, sprite]);
    return (React.createElement(FilterableComponentProvider, { value: sprite }, props.children));
}
function Say(props) {
    const { children, onNext, style, hideActor } = props;
    const app = useContext(PixiAppContext);
    const lines = Array.isArray(children) ? children : [children];
    const [lineNumber, setLineNumber] = useState(0);
    const line = lines[lineNumber];
    const lineWithActor = `${props.actor}: ${line}`;
    const done = lineNumber === lines.length - 1;
    const text = !line ? undefined : hideActor ? line : lineWithActor;
    const onLineDone = useCallback(() => {
        setLineNumber((l) => l + 1);
        if (done)
            onNext?.();
    }, [done, onNext]);
    return (React.createElement(TypewriterText, { lineId: text ?? "", style: style, soundPath: "/sounds/textscroll.wav", typingSpeedInMs: 20, onLineDone: onLineDone, onLineDoneDelayMs: 500, parent: app }, text ?? ""));
}
function ApplyFilter(props) {
    const { filters } = props;
    const comp = useContext(FilterableComponentContext);
    useAddPixiFilter2({
        filters,
        comp,
    });
    return null;
}

export { ApplyFilter, Background, Delay, Oscilloscope, PhysicsSprite, PixiApp, PixiAppContext, PixiAppWrapper, PixiEvents, PixiFilter, PixiFilter2, PixiLoaderResourcesContext, RegisterPixiEventListeners, Say, Scene, SceneProvider, TypewriterText, defaultDotFilter, defaultGlitchFilter, defaultGlowFilter, defaultOldFilmFilter, defaultPixelateFilter, defaultTextStyle, flocking, useAddFlock, useAddPixiFilter2, useAddToParent, useGlowingEyes, useLightningShader, useLoadPixiSprite, usePixiEvents };
//# sourceMappingURL=index.js.map
