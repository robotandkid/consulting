import { LoaderResource } from "pixi.js";
import { InternalPixiResourceMetaData, PixiResourceMetaData } from "../PixiApp/PixiApp.types";
export type Resources = Record<InternalPixiResourceMetaData["id"], LoaderResource | undefined>;
export interface LoadResourcesOpts {
    resourcesToLoad: PixiResourceMetaData[] | undefined;
    onResourcesChange: (resources: Resources) => void;
}
export declare function useLoadResources(props: LoadResourcesOpts): void;
export declare const PixiResources: (props: LoadResourcesOpts) => null;
