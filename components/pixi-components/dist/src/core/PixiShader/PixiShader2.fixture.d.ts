import React from "react";
export declare function CyberFujiFixture(): React.JSX.Element;
export declare function NeonWebFixture(): React.JSX.Element;
export declare function HaloFixture(): React.JSX.Element;
export declare function VoxelEdgesFixture(): React.JSX.Element;
export declare function CloudsFixture(): React.JSX.Element;
export declare function WarpFixture(): React.JSX.Element;
