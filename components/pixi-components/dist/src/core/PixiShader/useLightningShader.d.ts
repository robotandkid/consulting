import { Sprite } from "pixi.js";
export declare function useLightningShader(props: {
    comp: Sprite | undefined;
    uniforms: {
        u_precision: "mediump" | "highp";
        u_time: number;
        u_resolution: [number, number];
        u_speed: number;
    };
}): {
    comp: Sprite | undefined;
    shader: import("pixi.js").Filter | undefined;
} | undefined;
