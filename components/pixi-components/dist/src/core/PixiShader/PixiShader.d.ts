import { Application, Filter, Sprite } from "pixi.js";
export declare function usePixiShaderFromResource<_Sprite extends Sprite>(props: {
    path: string;
    comp: _Sprite | undefined;
    onInit?: (container: _Sprite, app: Application) => void;
    uniforms?: Record<string, unknown>;
    animate?: (filter: Filter, delta: number) => void;
}): {
    comp: _Sprite | undefined;
    shader: Filter | undefined;
} | undefined;
export declare function usePixiShader<_Sprite extends Sprite>(props: {
    frag?: string;
    comp: _Sprite | undefined;
    onInit?: (container: _Sprite, app: Application) => void;
    uniforms?: Record<string, unknown>;
    animate?: (filter: Filter, delta: number) => void;
}): {
    comp: _Sprite | undefined;
    shader: Filter | undefined;
} | undefined;
export declare function useToyShader<Uniforms extends Record<string, unknown>>(props: {
    pathToShader: string;
    onInit?: (app: Application) => void;
    uniforms?: Uniforms;
    animate?: (uniforms: Uniforms, delta: number) => void;
    withMouse?: {
        uniform: string;
    };
    withChannel0?: {
        uniform: string;
        path: string;
    };
    withChannel1?: {
        uniform: string;
        path: string;
    };
}): {
    shader: Filter | undefined;
} | undefined;
