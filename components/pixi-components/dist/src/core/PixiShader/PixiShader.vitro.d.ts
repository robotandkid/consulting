import React from "react";
export declare function BasicShaderFixture(): React.JSX.Element;
export declare function CutShaderFixture(): React.JSX.Element;
export declare function LightningShaderFixture(): React.JSX.Element;
export declare function LensFlareShaderFixture(): React.JSX.Element;
export declare function SandboxShaderFixture(): React.JSX.Element;
