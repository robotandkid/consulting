import { Sprite } from "pixi.js";
import { PixiParentProps } from "../PixiApp/useAddToParent";
import { InternalPixiResourceMetaData } from "../PixiApp/PixiApp.types";
export interface SpriteData {
    resource: InternalPixiResourceMetaData;
    sprite: Sprite;
    computedHeight: number;
    computedWidth: number;
}
type PixiSpriteOpts = {
    path: string | undefined;
    width: number;
    height: number;
} & PixiParentProps;
/**
 * Loads image and resizes it (to scale) to fit
 * inside the container
 *
 * @param props
 */
export declare function useLoadPixiSprite(props: PixiSpriteOpts): SpriteData | undefined;
export {};
