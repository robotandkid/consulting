import React from "react";
export declare function PixiSpriteBasic(): React.JSX.Element;
export declare function PixiSpriteAddToParent(): React.JSX.Element;
export declare function PixiSpriteAddToApp(): React.JSX.Element;
export declare function PixiSpriteWithFilter(): React.JSX.Element;
