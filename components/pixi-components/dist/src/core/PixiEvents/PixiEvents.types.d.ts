export declare const pixiTapEvent = "pixi:tap";
export declare const pixiDoubleTapEvent = "pixi:doubletap";
export declare const pixiDragLeftEvent = "pixi:dragleft";
export declare const pixiDragRightEvent = "pixi:dragright";
