import React, { ReactNode } from "react";
interface NextEventProps {
    onNextEvent?: () => void;
    onNextEventDoublePress?: () => void;
}
interface PreviousEventProps {
    onPreviousEvent?: () => void;
    onPreviousEventDoublePress?: () => void;
}
interface PixiEvent {
    type: "nextEventDoublePress" | "nextEvent" | "previousEventDoublePress" | "previousEvent";
}
export declare function PixiEvents(props: {
    children: ReactNode;
}): React.JSX.Element;
export declare function usePixiEvents(): PixiEvent | undefined;
/**
 * If you want to register callbacks for the next/previous events, use this.
 */
export declare function RegisterPixiEventListeners(props: NextEventProps & PreviousEventProps): null;
export {};
