import { Application, Filter } from "pixi.js";
import { PixiFilter as PixiFilterType, PixiFilter2 as PixiFilterType2 } from "./PixiFilter.types";
/**
 * @deprecated use version2
 */
export declare function useAddPixiFilter<Comp extends Application | {
    filters: Filter[] | null;
}>(props: {
    comp: Comp | undefined;
    filters: PixiFilterType | Array<PixiFilterType> | undefined;
}): Comp | undefined;
/**
 * The only catch with this method is that changing the filters doesn't trigger
 * a re-render. But you can use the force render prop to force a re-render.
 */
export declare function useAddPixiFilter2<Comp extends Application | {
    filters: Filter[] | null;
}>(props: {
    comp: Comp | undefined;
    filters: PixiFilterType2 | PixiFilterType2[];
}): Comp | undefined;
/**
 * @deprecated use version2
 */
export declare const PixiFilter: <Comp extends Application | {
    filters: Filter[] | null;
}>(props: {
    comp: Comp | undefined;
    filters: PixiFilterType | Array<PixiFilterType> | undefined;
}) => null;
export declare const PixiFilter2: <Comp extends Application | {
    filters: Filter[] | null;
}>(props: {
    comp: Comp | undefined;
    filters: PixiFilterType2 | PixiFilterType2[];
}) => null;
