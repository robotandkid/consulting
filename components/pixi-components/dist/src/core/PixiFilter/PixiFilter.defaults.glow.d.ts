import { GlowFilter, GlowFilterOptions } from "pixi-filters";
import { PixiGlowFilterOpts } from "./PixiFilter.types";
export declare function defaultGlowFilter(opts: Partial<GlowFilterOptions> & Omit<PixiGlowFilterOpts, "filter">): {
    filter: GlowFilter;
    animation: undefined;
};
