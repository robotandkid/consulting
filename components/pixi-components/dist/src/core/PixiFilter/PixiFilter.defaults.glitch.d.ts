import { GlitchFilterOptions } from "pixi-filters";
import { GlitchFilterOpts, PixiFilter2 } from "./PixiFilter.types";
export declare function defaultGlitchFilter(opts?: Partial<GlitchFilterOptions> & Omit<GlitchFilterOpts, "filter">): PixiFilter2;
