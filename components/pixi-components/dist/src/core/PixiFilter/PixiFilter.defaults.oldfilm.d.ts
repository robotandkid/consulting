import { OldFilmFilterOptions } from "pixi-filters";
import { PixiFilter2 } from "./PixiFilter.types";
export declare function defaultOldFilmFilter(props?: Partial<OldFilmFilterOptions>): PixiFilter2;
