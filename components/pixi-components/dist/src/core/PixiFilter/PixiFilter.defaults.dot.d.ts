import { DotFilterOpts, PixiFilter2 } from "./PixiFilter.types";
export declare function defaultDotFilter(opts?: Omit<DotFilterOpts, "filter">): PixiFilter2;
