import { AdjustmentFilter, ColorReplaceFilter, CRTFilter, DotFilter, DropShadowFilter, GlitchFilter, GlowFilter, GodrayFilter, KawaseBlurFilter, MotionBlurFilter, OldFilmFilter, OutlineFilter, PixelateFilter, ReflectionFilter, TiltShiftFilter, ZoomBlurFilter } from "pixi-filters";
import { PixiFilter } from "./PixiFilter.types";
export declare function instantiatePixiFilter(cmd: PixiFilter): {
    filter: CRTFilter;
    animation(): void;
} | {
    filter: DotFilter;
    animation: undefined;
} | {
    filter: DropShadowFilter;
    animation: undefined;
} | {
    filter: GlitchFilter;
    animation(): void;
} | {
    filter: GlowFilter;
    animation: undefined;
} | {
    filter: GodrayFilter;
    animation(elapsedTimeMs: number): void;
} | {
    filter: KawaseBlurFilter;
    animation: undefined;
} | {
    filter: MotionBlurFilter;
    animation: undefined;
} | {
    filter: OldFilmFilter;
    animation(): void;
} | {
    filter: OutlineFilter;
    animation: undefined;
} | {
    filter: PixelateFilter;
    animation: undefined;
} | {
    filter: ReflectionFilter;
    animation(): void;
} | {
    filter: TiltShiftFilter;
    animation: undefined;
} | {
    filter: ZoomBlurFilter;
    animation: undefined;
} | {
    filter: ColorReplaceFilter;
    animation: undefined;
} | {
    filter: AdjustmentFilter;
    animation: undefined;
} | undefined;
