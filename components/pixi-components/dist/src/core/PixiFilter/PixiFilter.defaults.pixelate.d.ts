import { PixelateFilterOpts, PixiFilter2 } from "./PixiFilter.types";
export declare function defaultPixelateFilter(opts: Omit<PixelateFilterOpts, "filter">): PixiFilter2;
