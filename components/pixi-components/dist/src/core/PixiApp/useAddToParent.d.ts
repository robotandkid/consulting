import { Application, Container, DisplayObject, Filter } from "pixi.js";
export type PixiParent = Application | Container | null;
export declare function isApplication(cmp: PixiParent | undefined): cmp is Application;
export declare function isContainer(cmp: PixiParent | undefined): cmp is Container;
export declare function getDims(cmp: PixiParent | undefined): {
    width: number;
    height: number;
} | {
    width: undefined;
    height: undefined;
};
export declare function getFilters<Comp extends Application | {
    filters: Filter[] | null;
}>(cmp: Comp | undefined): {
    filters: Filter[] | undefined | null;
};
export type PixiParentProps = {
    parentType: "app";
} | {
    parentType: "container";
    parent: PixiParent | undefined;
} | {
    parentType: "none";
};
type NullableDisplayObject = DisplayObject | undefined;
export declare function useAddToParent(props: {
    child: NullableDisplayObject | NullableDisplayObject[];
} & PixiParentProps): boolean;
export {};
