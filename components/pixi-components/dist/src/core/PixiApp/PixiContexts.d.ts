/// <reference types="react" />
import { Application, LoaderResource } from "pixi.js";
export declare const PixiAppContext: import("react").Context<Application | undefined>;
export declare const PixiAppTickerContext: import("react").Context<import("pixi.js").Ticker | undefined>;
export declare const PixiCanvasRefContext: import("react").Context<HTMLCanvasElement | undefined>;
export declare const PixiLoaderResourcesContext: import("react").Context<Record<`image:${string}` | `sound:${string}` | `shader:${string}`, LoaderResource | undefined> | undefined>;
