import "pixi-sound";
import React, { RefObject } from "react";
import { PixiAppOpts } from "./PixiApp.types";
export declare function useLoadPixiApp(props: PixiAppOpts & {
    pixiRef: RefObject<HTMLCanvasElement | null>;
}): void;
export declare function PixiApp(props: PixiAppOpts): React.JSX.Element;
export declare function PixiAppWrapper(props: Omit<PixiAppOpts, "children"> & {
    children: () => JSX.Element | null;
}): React.JSX.Element;
