import { ITextStyle } from "pixi.js";
import React from "react";
import { PixiParent } from "../../core/PixiApp/useAddToParent";
type TypewriterEvent = "type" | "stop";
type Color = `#${string}`;
interface TextProps {
    lineId: string;
    children: string;
    soundPath?: string;
    typingSpeedInMs: number;
}
interface ControlProps {
    autoStart?: boolean;
    /**
     * Defaults to 0
     */
    onLineDoneDelayMs?: number;
    onLineDone?: () => void;
}
interface StyleProps {
    style: Partial<ITextStyle> & {
        backgroundColor?: Color;
        width?: number;
        height?: number;
        top?: number;
        left?: number;
    };
}
interface AppProps {
    parent: PixiParent | undefined;
}
export declare function useTypeEffect(props: {
    event: TypewriterEvent;
    delayMs: number;
    typeAppTicker: () => void;
}): void;
export type TypewriterTextProps = TextProps & ControlProps & StyleProps & AppProps;
export declare function TypewriterText(props: TypewriterTextProps): React.JSX.Element;
export {};
