import React from "react";
export declare function ASingleLine(): React.JSX.Element;
export declare function DuplicateLines(): React.JSX.Element;
export declare function MultipleLines(): React.JSX.Element;
export declare function Dimensions(): React.JSX.Element;
