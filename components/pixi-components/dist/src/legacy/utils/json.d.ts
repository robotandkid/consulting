import React from "react";
export declare function PrettyJson(props: {
    json: Record<string, unknown>;
}): React.JSX.Element;
