import { Application } from "pixi.js";
import { RefObject } from "react";
import { PixiAppOpts } from "../pixi";
export declare function useLoadPixiApp(props: PixiAppOpts & {
    pixiRef: RefObject<HTMLCanvasElement | null>;
    onInitApp(app: Application): void;
}): void;
export declare const LoadPixiApp: (props: PixiAppOpts & {
    pixiRef: RefObject<HTMLCanvasElement | null>;
    onInitApp(app: Application): void;
}) => null;
