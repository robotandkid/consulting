import { LoaderResource } from "pixi.js";
import { PixiResourceMetaData } from "../pixi";
export type Resources = Partial<Record<string, LoaderResource>>;
export interface LoadResourcesOpts {
    /**
     * Set to `false` when there are no resources.
     */
    resourcesToLoad: false | PixiResourceMetaData[] | undefined;
    onResourcesChange: (resources: Resources) => void;
}
export declare function useLoadResources(props: LoadResourcesOpts): void;
export declare const LoadResources: (props: LoadResourcesOpts) => null;
