import { AdjustmentFilter, ColorReplaceFilter, ConfigurableFilter, Filter, SimpleFilter } from "./Filter.types";
type BaseFilter = Filter;
export interface FilterConfigNumberType {
    min?: number;
    max?: number;
    step?: number;
    default: number;
}
export type FilterConfig<Filter extends BaseFilter> = Omit<{
    [key in keyof Filter]: FilterConfigNumberType;
}, "filter" | "target"> & {
    filter: Filter["filter"];
    target?: Filter["target"];
};
export declare const filterConfigs: (FilterConfig<SimpleFilter> | FilterConfig<ConfigurableFilter> | FilterConfig<ColorReplaceFilter> | FilterConfig<AdjustmentFilter>)[];
export {};
