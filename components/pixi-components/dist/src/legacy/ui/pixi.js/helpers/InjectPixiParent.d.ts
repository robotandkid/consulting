import React from "react";
import { PixiParent, PixiNode } from "../pixi";
export declare function InjectPixiParent(props: {
    parent?: PixiParent;
    children?: PixiNode;
}): React.JSX.Element;
