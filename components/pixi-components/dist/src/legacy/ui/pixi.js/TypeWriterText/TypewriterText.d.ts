import { TextStyle } from "pixi.js";
import React from "react";
import { PixiElementProps } from "../pixi";
type TypewriterEvent = "type" | "stop";
type TypewriterTextProps = {
    lineId: string;
    children: string;
    event?: TypewriterEvent;
    typingSpeedInMs: number;
    /**
     * Defaults to 0
     */
    onDoneDelayMs?: number;
    onDone?: () => void;
    style: TextStyle;
    width: number;
    height: number;
} & PixiElementProps;
type EventingTypewriterTextProps = Pick<TypewriterTextProps, "lineId" | "children" | "typingSpeedInMs" | "onDoneDelayMs" | "onDone" | "style" | "width" | "height"> & {
    autoStart?: boolean;
} & PixiElementProps;
export declare const textScrollId = "textScrollId";
export declare function EventingTypewriterText(props: EventingTypewriterTextProps): React.JSX.Element;
export declare function useTypeEffect(props: {
    event: TypewriterEvent;
    delayMs: number;
    typeAppTicker: () => void;
}): void;
/**
 * Types the given text with the given typing speed, and calls onDone when
 * finished.
 *
 * - Change the `event` to `stop`, to jump to the done state.
 * - Change the `event` to `type` to restart typing
 *
 * **Note:** Because of the way React components work, changing the `event` to
 * `type` by itself _twice_ in a row will *not* restart the component _twice_.
 * _In order to achieve this effect, you must also change the lineId_.
 */
export declare function TypewriterText(props: TypewriterTextProps): React.JSX.Element;
export {};
