import React, { JSXElementConstructor, ReactNode } from "react";
import { LoadResourcesOpts } from "../ui/pixi.js/loaders/useLoadResources";
import { AwesomeRetroCutscenesProps } from "./AwesomeRetroCutscenes.types";
export declare const AwesomeRetroCutscenesPixiJSContext: React.Context<"loaded" | "ready" | "init">;
export declare const AwesomeRetroCutscenesResourcesContext: React.Context<"loaded" | "ready" | "init" | "loading">;
export declare function AwesomeRetroCutscenes(props: AwesomeRetroCutscenesProps & {
    __loadResourcesComp?: JSXElementConstructor<LoadResourcesOpts>;
    children?: ReactNode;
}): React.JSX.Element;
