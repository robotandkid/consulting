import { PixiResourceMetaData } from "../ui/pixi.js/pixi";
export declare function useResourcesFromTextScrollConfig(textScrollSoundPath: string | undefined): PixiResourceMetaData[];
