import React from "react";
declare const _default: {
    hookRenderCountTest: React.JSX.Element;
    naiveProviderRenderCountTest: React.JSX.Element;
    providerRenderCountTest: React.JSX.Element;
};
export default _default;
