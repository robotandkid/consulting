import React from "react";
import { DialogNode } from "./dialog.types";
export declare const DialogEngineCommandContext: React.Context<{
    nodeTitle: string;
    cmdNodeIndex: number;
    cmd: import("./dialog.types").InternalCommand;
}>;
export declare const DialogEngineResourcesContext: React.Context<import("../ui/pixi.js/pixi").PixiResourceMetaData[]>;
export declare const DialogEngineCallbackContext: React.Context<{
    advance(): void;
    restartNodeOrPreviousNode(): void;
    restart(): void;
}>;
export declare function DialogEngineProvider(props: {
    nodes: DialogNode[];
    children?: React.ReactNode;
}): React.JSX.Element;
