import { Filter } from "../ui/pixi.js/PixiSprite/Filter.types";
export type CmdFilter = Filter;
export type CmdSayLine = {
    say: string;
    actor: "narrator" | string;
};
type CmdDrawBg = {
    "draw-bg": string;
    name?: string;
};
type CmdJump = {
    jump: string;
};
interface CmdDelay {
    delayInMs: number;
}
export type Command = CmdSayLine | CmdDrawBg | CmdJump | CmdFilter | CmdDelay;
/**
 * This is the more user-friendly interface
 */
export interface DialogNode {
    title: string;
    commands: Command[];
}
export type InternalCmdFilter = CmdFilter & {
    type: "filter";
};
export type InternalCommand = (CmdSayLine & {
    type: "say-line";
}) | (CmdDrawBg & {
    type: "draw-bg";
}) | (CmdJump & {
    type: "jump";
}) | (CmdDelay & {
    type: "delay";
}) | {
    type: "end";
} | InternalCmdFilter;
export interface InternalDialogNode {
    title: string;
    commands: InternalCommand[];
}
export declare function commandToInternalCommand(cmd: Command): InternalCommand;
export {};
