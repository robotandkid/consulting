import React from "react";
export declare function StepThruSingleScene(): React.JSX.Element;
export declare function StepThruScene(): React.JSX.Element;
export declare function BackgroundWithFilter(): React.JSX.Element;
export declare function Demo(): React.JSX.Element;
