/// <reference types="matter-js" />
import React, { ReactNode } from "react";
import { useAddPixiFilter2 } from "../core/PixiFilter/PixiFilter";
import { TypewriterTextProps } from "../components/TypewriterText/TypewriterText";
interface OnNext {
    onNext?: () => void;
}
interface SceneProps {
    name: string;
    onNext?: () => void;
}
/**
 * - Press the right arrow and advance to the next scene. If at the last scene,
 *   then jump to the last node.
 * - Press the left arrow and advance to the previous scene. If at the first
 *   scene, then restart the first scene.
 */
export declare function SceneProvider(props: {
    loop?: boolean;
    physics?: {
        engine: Matter.IEngineDefinition;
    };
    children: React.ReactElement<SceneProps>[] | React.ReactElement<SceneProps>;
}): React.JSX.Element;
export declare function Scene(props: {
    children: React.ReactElement<OnNext> | React.ReactElement<OnNext>[];
} & SceneProps & OnNext): React.JSX.Element;
export declare function Background(props: {
    path: string;
    width: number;
    height: number;
    children?: ReactNode;
} & OnNext): React.JSX.Element;
export declare function Delay(props: {
    timeout: number;
    children?: ReactNode;
} & OnNext): React.JSX.Element;
export declare function PhysicsSprite(props: {
    path?: string;
    width?: number;
    height?: number;
    x: number;
    y: number;
    children?: React.ReactNode;
} & OnNext): React.JSX.Element;
export declare function Say(props: {
    actor: string;
    hideActor?: boolean;
    children: string | string[];
} & OnNext & Pick<TypewriterTextProps, "style">): React.JSX.Element;
type Filter = Parameters<typeof useAddPixiFilter2>[0]["filters"];
export declare function ApplyFilter(props: {
    filters: Filter;
}): null;
export {};
