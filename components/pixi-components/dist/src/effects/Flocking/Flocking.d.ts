import { Point } from "pixi.js";
export declare const flocking: (opts: {
    /**
     * Called before each render loop.
     */
    preRender: () => void;
    onStartBoid: (p: Point) => void;
    /**
     * Renders a single boid
     */
    renderBoid: (p: Point) => void;
    config: {
        count: number;
        width: number;
        height: number;
        randomInitialPosition?: boolean;
        /**
         * The maximum speed of the boids.
         *
         * Defaults to 4.
         */
        maxSpeed?: number;
        /**
         * The separation, alignment, cohesions forces are all limited by maxForce *
         * their respective weights.
         *
         * Defaults to 0.03.
         */
        maxForce?: number;
        /**
         * If two boids are too close, a separation force will steer them away from
         * each other. This force will be multiplied by this weight.
         *
         * Defaults to 1.5.
         */
        separationWeight?: number;
        /**
         * For the average velocity of all nearby boids, an alignment force will
         * nudge all nearby boids towards this average velocity. This alignment
         * force will be multiplied by this weight.
         *
         * Defaults to 1.
         */
        alignmentWeight?: number;
        /**
         * For the average position of all nearby boids, a cohesion force will move
         * all nearby boids towards that position. This cohesion force will be
         * multiplied by this weight.
         *
         * Defaults to 1.
         */
        cohesionWeight?: number;
    };
}) => {
    draw(): void;
    mousePressed(mouseX: number, mouseY: number): void;
};
