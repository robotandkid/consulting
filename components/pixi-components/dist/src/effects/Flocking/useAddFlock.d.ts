import { Graphics, Point } from "pixi.js";
import { flocking } from "./Flocking";
type OriginalFlockProps = Parameters<typeof flocking>[0];
type OriginalConfigProps = Partial<OriginalFlockProps["config"]>;
interface AddFlockProps {
    start?: boolean;
    /**
     * Called before each render loop with the graphics the boids will be rendered
     * in.
     */
    preRender?: (boidCanvas: Graphics) => void;
    /**
     * Called once before the first render loop with the graphics the boids will
     * be rendered in.
     */
    onStartBoid?: (boidCanvas: Graphics, position: Point) => void;
    /**
     * Called reach render loop with the graphics the boids will be rendered in.
     */
    renderBoid?: (boidCanvas: Graphics, position: Point) => void;
    config?: OriginalConfigProps & {
        flockColor?: `#${string}`;
    };
}
export declare function useAddFlock(props?: AddFlockProps): void;
export {};
