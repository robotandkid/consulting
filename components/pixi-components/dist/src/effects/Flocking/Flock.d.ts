import { Boid } from "./Boid";
/**
 * This class is dumb and should be replaced by flocking.ts;
 */
export declare class Flock {
    private boids;
    get positions(): import("pixi.js").Point[];
    run(): void;
    addBoid(b: Boid): void;
}
