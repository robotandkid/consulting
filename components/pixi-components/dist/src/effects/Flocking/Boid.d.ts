import "@pixi/math-extras";
import { Point } from "pixi.js";
interface BoidConfig {
    width: number;
    height: number;
    maxSpeed: number;
    maxForce: number;
    separationWeight: number;
    alignmentWeight: number;
    cohesionWeight: number;
}
export declare class Boid {
    get position(): Point;
    private _position;
    private velocity;
    private acceleration;
    private renderCallback;
    private config;
    private get r();
    constructor(opts: {
        x: number;
        y: number;
        render: (p: Point) => void;
        config: Partial<BoidConfig> & Required<Pick<BoidConfig, "width" | "height">>;
    });
    run(boids: Boid[]): void;
    private applyForce;
    /**
     * We accumulate a new acceleration each time based on three rules
     */
    private flock;
    /**
     * Method to update position
     */
    private update;
    /**
     * A method that calculates and applies a steering force towards a target
     *
     * STEER = DESIRED MINUS VELOCITY
     */
    private seek;
    /**
     * Wraparound
     */
    private borders;
    private separate;
    /**
     * Alignment - For every nearby boid in the system, calculate the average
     * velocity
     */
    private align;
    /**
     * Cohesion - For the average position (i.e. center) of all nearby boids,
     * calculate steering vector towards that position
     */
    private cohesion;
}
export {};
