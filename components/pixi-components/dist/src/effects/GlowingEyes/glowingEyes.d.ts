import { Graphics, Point } from "pixi.js";
export declare function useGlowingEyes(props: {
    leftEye: Point;
    rightEye: Point;
    radius?: number;
    blurRadius?: number;
    eyeColor?: `#${string}`;
}): Graphics | undefined;
