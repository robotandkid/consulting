import { ILineStyleOptions } from "@pixi/graphics";
import { ReactNode } from "react";
import { PixiParent } from "../../core/PixiApp/useAddToParent";
type Color = `#${string}`;
export declare function Oscilloscope(props: {
    style: {
        backgroundColor?: Color;
        width?: number;
        height?: number;
        x?: number;
        y?: number;
    };
    lineStyle: Omit<ILineStyleOptions, "color"> & {
        color?: Color;
    };
    children?: ReactNode;
    analyser: AnalyserNode | undefined;
    parent: PixiParent | undefined;
}): null;
export {};
