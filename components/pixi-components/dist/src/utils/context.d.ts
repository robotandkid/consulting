import React, { ReactNode } from "react";
export declare function createContext<T>(initial?: T): React.Context<T>;
export declare function makeReadOnlyContext<MyContext>(): [(props: {
    children?: ReactNode;
    value: MyContext;
}) => React.JSX.Element, React.Context<MyContext>];
export declare function makeSimpleContext<MyContext>(name?: string): [{
    (props: {
        children?: ReactNode;
        initialValue: MyContext;
    }): React.JSX.Element;
    displayName: string;
}, React.Context<MyContext>, React.Context<(value: MyContext) => void>];
