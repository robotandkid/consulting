type Color = `#${string}`;
type ColorFromType<T> = T extends Color ? number : undefined;
export declare function rgbToHex<C extends Color | undefined>(color?: C): ColorFromType<C>;
export {};
