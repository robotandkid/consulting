type F<T> = (props: T) => void;
export declare function createComponent<T>(hook: F<T>): (props: T) => null;
export {};
