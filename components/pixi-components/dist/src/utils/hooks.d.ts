/// <reference types="react" />
export declare function useObjectAsRef<T>(obj: T): import("react").MutableRefObject<T>;
