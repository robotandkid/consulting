precision mediump float;

varying vec2 vTextureCoord;//The coordinates of the current pixel
uniform sampler2D uSampler;//The image data

void main(void) {
  vec2 offset = vTextureCoord + 0.04;
  vec4 offsetImage = texture2D(uSampler, offset);
  vec4 image = texture2D(uSampler, vTextureCoord);

  vec4 color = vec4(offsetImage.x, image.y, image.z, 1.0);

  gl_FragColor = color;
}
