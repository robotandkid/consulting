precision mediump float;

uniform float time;
uniform vec2 resolution;

void main() {
	float time_ = (time + 10.) * 30.0;
	vec2 uv = (-resolution.xy + 2.0 * gl_FragCoord.xy) / resolution;
	float s = 0.0;
	float v = -0.010;
	float t = time_ * 0.005;
	uv.x += sin(t) * 0.15;
	vec3 col_ = vec3(0.0);
	vec3 init = vec3(-0.25, -0.25 + sin(time_ * 0.001) * 0.4, floor(time_) * 0.0008);

	for(int r = 0; r < 100; r++) {
		vec3 p = init + s * vec3(uv, 0.143);
		p.z = mod(-p.z, 2.0) - 1.0;
		for(int i = 0; i < 10; i++) {
			p = abs(p * 2.003) / dot(p, p) - 0.75;
		}
		v += length(p * p) * smoothstep(0.0, 0.1, 0.9 - s) * .0005;
		// Get a purple and cyan effect by biasing the RGB in different ways...
		//col_ +=  vec3(v * 0.8, 1.1 - s * 0.5, .7 + v * 1.5) * v * 0.016;
		col_ += vec3(v * 0.02);
		s += .005;
	}
	gl_FragColor = vec4(col_, 1.0);
}
