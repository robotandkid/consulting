precision highp float;

uniform float time;
uniform vec2 u_resolution;

void main(void) {

  vec2 uPos = (gl_FragCoord.xy / u_resolution.xy);//normalize wrt y axis
	//suPos -= vec2((resolution.x/resolution.y)/2.0, 0.0);//shift origin to center

  uPos.x -= 0.5;
  uPos.y -= 0.5;

  vec3 color = vec3(0.0);

  for(float i = 0.0; i < 10.0; ++i) {
    float t = time * (01.9);

    uPos.y += sin(uPos.x * (i + 1.0) + t + i / 2.0) * 0.16;
    float fTemp = abs(1.0 / uPos.y / 100.0);
    color += vec3(fTemp * (10.0 - i) / 10.0, fTemp * i / 10.0, pow(fTemp, 0.99) * 1.5);
  }

  float tolerance = 1.0;

  if(color.x < tolerance && color.y < tolerance && color.z < tolerance) {
    color = vec3(1.0);
  }

  vec4 color_final = vec4(color, 1.0);
  gl_FragColor = color_final;
}