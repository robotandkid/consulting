precision highp float;

varying vec2 vTextureCoord;//The coordinates of the current pixel

void main(void){
	vec2 pos = vTextureCoord.xy;

	pos.x -= 0.5;
	pos.y -= 0.5;

	const float n = 40.0;
	
	float radius = length(pos) * 2.0 - 0.4;
	float t = atan(pos.y, pos.y);
	
	float color = 0.025;
	
	for (float i = 1.0; i <= n; i++){
		color += 0.002 / abs(0.3 * sin(
			4. * (t + i/n * 1.0 * 0.025)
		    ) - radius
		);
	}

  vec3 c = vec3(1.5, 0.3, 0.15) * color;
	
	gl_FragColor = vec4(c, 1.0);
	
}