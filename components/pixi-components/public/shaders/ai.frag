#version 300 es 
precision highp float;

uniform float iTime;
uniform vec3 iResolution; 
uniform sampler2D iChannel0;
out vec4 fragColor;

// vec3 mix(vec3 x, vec3 y, float a) {
//     return x * (1.0 - a) + y * a;
// }

vec2 noise(vec2 inputVec) {
    vec2 i = floor(inputVec);
    vec2 f = fract(inputVec);
    vec2 u = f * f * (3.0 - 2.0 * f);
    return mix(
            dot(i, vec2(127.1, 311.7)), 
            dot(i + vec2(1.0, 0.0), vec2(127.1, 311.7)), 
            u.x
        );
 }

void main(void)
{
    vec2 uv = gl_FragCoord.xy / iResolution.xy;
    vec3 color = vec3(0);
    
    // Use the sin() function to create an animated pattern
    color += sin(iTime + uv.x * 10.0) * 0.5 + 0.5;
    color += sin(iTime + uv.y * 10.0) * 0.5 + 0.5;
    
    // Use the noise() function to add some randomness to the pattern
    color += vec3(noise(uv * 20.0 + vec2(iTime * 0.1)) * 0.25, 1.0);
    
    // Use the texture() function to add a subtle texture to the pattern
    color += texture(iChannel0, uv * 2.0).rgb * 0.1;
    
    // Output the final color
    fragColor = vec4(color, 1.0);
}