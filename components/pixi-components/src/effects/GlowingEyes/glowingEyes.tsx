import { GlowFilter } from "pixi-filters";
import { Graphics, Point } from "pixi.js";
import { useEffect, useState } from "react";
import { useAddPixiFilter2 } from "../../core/PixiFilter/PixiFilter";
import { rgbToHex } from "../../utils/color";

export function useGlowingEyes(props: {
  leftEye: Point;
  rightEye: Point;
  radius?: number;
  blurRadius?: number;
  eyeColor?: `#${string}`;
}) {
  const {
    eyeColor = "#ffffff",
    radius = 1,
    blurRadius = 20,
    leftEye: { x: x1, y: y1 },
    rightEye: { x: x2, y: y2 },
  } = props;

  const [eyeGfx, setEyeGfx] = useState<Graphics>();
  const start = !!parent;

  useEffect(() => {
    if (!start) return;

    const eyes = new Graphics();
    eyes.lineStyle(0);
    eyes.beginFill(rgbToHex(eyeColor), 2);
    eyes.drawCircle(x1, y1, radius);
    eyes.endFill();

    eyes.beginFill(rgbToHex(eyeColor), 2);
    eyes.drawCircle(x2, y2, radius);
    eyes.endFill();

    setEyeGfx(eyes);
  }, [eyeColor, radius, start, x1, x2, y1, y2]);

  const glowingEyes = useAddPixiFilter2({
    comp: eyeGfx,
    filters: {
      filter: new GlowFilter({
        distance: blurRadius,
        color: rgbToHex(eyeColor),
        knockout: false,
        innerStrength: 20,
        outerStrength: 20,
        quality: 0.5,
      }),
    },
  });

  return glowingEyes;
}
