import { Point } from "pixi.js";
import React from "react";
import { useAddToParent } from "../../core/PixiApp/useAddToParent";
import { PixiAppWrapper } from "../../core/PixiApp/PixiApp";
import { useLoadPixiSprite } from "../../core/PixiSprite/PixiSprite";
import { useGlowingEyes } from "./glowingEyes";

const width = 150;
const height = 150;

export function AddGlowingEyesToSpriteFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "image", path: "/images/test-portrait.jpeg" }]}
    >
      {function useFixture() {
        const sprite = useLoadPixiSprite({
          path: "/images/test-portrait.jpeg",
          height,
          width,
          parentType: "none",
        });

        const glow = useGlowingEyes({
          eyeColor: "#00ffff",
          leftEye: new Point(72, 38),
          rightEye: new Point(88, 38),
          radius: 2,
          blurRadius: 20,
        });

        const added = useAddToParent({
          child: sprite?.sprite,
          parentType: "app",
        });

        /**
         * Not sure why these don't run in order
         */
        useAddToParent({
          child: glow,
          parentType: added ? "app" : "none",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}
