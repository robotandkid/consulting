import { Application } from "pixi.js";
import React, { useEffect, useState } from "react";
import { PixiApp } from "../../core/PixiApp/PixiApp";
import { Oscilloscope } from "./Oscilloscope";

function Fixture() {
  const [app, setApp] = useState<Application>();
  const [analyser, setAnalyser] = useState<AnalyserNode>();

  /**
   * Recycled from https://mdn.github.io/voice-change-o-matic
   *
   * This can most definitely be simplified.
   */
  useEffect(() => {
    const audioCtx = new (window.AudioContext ||
      (window as any).webkitAudioContext)();

    const _analyzer = audioCtx.createAnalyser();

    _analyzer.minDecibels = -90;
    _analyzer.maxDecibels = -10;
    _analyzer.smoothingTimeConstant = 0.85;

    const distortion = audioCtx.createWaveShaper();
    const gainNode = audioCtx.createGain();
    const biquadFilter = audioCtx.createBiquadFilter();
    const convolver = audioCtx.createConvolver();

    navigator.mediaDevices.getUserMedia({ audio: true }).then((stream) => {
      const source = audioCtx.createMediaStreamSource(stream);
      source.connect(distortion);
      distortion.connect(biquadFilter);
      biquadFilter.connect(gainNode);
      convolver.connect(gainNode);
      gainNode.connect(_analyzer);
      _analyzer.connect(audioCtx.destination);

      distortion.oversample = "4x";
      biquadFilter.gain.setTargetAtTime(0, audioCtx.currentTime, 0);
      biquadFilter.disconnect(0);
      biquadFilter.connect(gainNode);

      setAnalyser(_analyzer);
    });
  }, []);

  return (
    <>
      <PixiApp
        width={200}
        height={100}
        resolution={window.devicePixelRatio || 1}
        backgroundColor="#ffffff"
        onAppInit={setApp}
      >
        <Oscilloscope
          style={{
            backgroundColor: "#cdcdcd",
          }}
          lineStyle={{
            width: 1,
            color: "#000000",
          }}
          parent={app}
          analyser={analyser}
        />
      </PixiApp>
    </>
  );
}

export function OscilloscopeTest() {
  return <Fixture />;
}
