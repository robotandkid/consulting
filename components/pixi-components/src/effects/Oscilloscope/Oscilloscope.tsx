import { Graphics, ILineStyleOptions } from "@pixi/graphics";
import { ReactNode, useContext, useEffect, useRef, useState } from "react";
import {
  getDims,
  PixiParent,
  useAddToParent,
} from "../../core/PixiApp/useAddToParent";
import { PixiAppTickerContext } from "../../core/PixiApp/PixiContexts";
import { rgbToHex } from "../../utils/color";
import { useObjectAsRef } from "../../utils/hooks";

function drawOscilloscope(
  source: Uint8Array,
  graphics: Graphics,
  style: {
    backgroundColor: number;
    width: number;
    height: number;
    x?: number;
    y?: number;
  },
  lineStyle: ILineStyleOptions
) {
  const bufferLength = source.length;

  graphics.clear();

  graphics.x = style.x ?? 0;
  graphics.y = style.y ?? 0;

  graphics.beginFill(style.backgroundColor);
  graphics.drawRect(0, 0, style.width, style.height);
  graphics.endFill();

  graphics.lineStyle(lineStyle);

  const sliceWidth = (style.width * 1.0) / bufferLength;
  let x = 0;

  for (let i = 0; i < bufferLength; i++) {
    const v = source[i] / 128.0;
    const y = (v * style.height) / 2;
    if (i === 0) {
      graphics.moveTo(x, y);
    } else {
      graphics.lineTo(x, y);
    }
    x += sliceWidth;
  }
  graphics.lineTo(style.width, style.height / 2);
}

type Color = `#${string}`;

export function Oscilloscope(props: {
  style: {
    backgroundColor?: Color;
    width?: number;
    height?: number;
    x?: number;
    y?: number;
  };
  lineStyle: Omit<ILineStyleOptions, "color"> & { color?: Color };
  children?: ReactNode;
  analyser: AnalyserNode | undefined;
  parent: PixiParent | undefined;
}) {
  const { analyser, style, lineStyle, parent } = props;
  const styleRef = useObjectAsRef(style);
  const lineStyleRef = useObjectAsRef(lineStyle);
  const ticker = useContext(PixiAppTickerContext);
  const [child, setChild] = useState<Graphics>();
  const dataArray = useRef<Uint8Array>();
  const [ready, setReady] = useState(false);

  useEffect(
    function captureAudioData() {
      if (!analyser) return;

      analyser.fftSize = 2048;
      const bufferLength = analyser.fftSize;
      dataArray.current = new Uint8Array(bufferLength);

      const capture = () => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        analyser.getByteTimeDomainData(dataArray.current!);
        requestAnimationFrame(capture);
      };

      requestAnimationFrame(capture);
      setReady(true);
    },
    [analyser]
  );

  useEffect(
    function analyse() {
      if (!parent || !ticker || !ready || !dataArray.current) return;

      const { width: parentWidth = 0, height: parentHeight = 0 } =
        getDims(parent);

      const {
        backgroundColor = "#ffffff",
        width = parentWidth,
        height = parentHeight,
        x,
        y,
      } = styleRef.current;

      const graphics = new Graphics();

      setChild(graphics);

      const _render = () => {
        if (!dataArray.current) return;

        drawOscilloscope(
          dataArray.current,
          graphics,
          {
            backgroundColor: rgbToHex(backgroundColor),
            width,
            height,
            x,
            y,
          },
          {
            ...lineStyleRef.current,
            color: rgbToHex(lineStyleRef.current.color),
          }
        );
      };

      ticker.add(_render);

      return () => {
        ticker.remove(_render);
        graphics?.destroy();
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [parent, ticker, ready]
  );

  useAddToParent({
    child: child,
    parentType: "container",
    parent: parent,
  });

  return null;
}
