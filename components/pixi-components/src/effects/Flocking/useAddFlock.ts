import { Container, Graphics, Point } from "pixi.js";
import { useContext, useEffect, useLayoutEffect, useRef } from "react";
import { PixiAppContext } from "../../core/PixiApp/PixiContexts";
import { rgbToHex } from "../../utils/color";
import { flocking } from "./Flocking";

type OriginalFlockProps = Parameters<typeof flocking>[0];

type OriginalConfigProps = Partial<OriginalFlockProps["config"]>;

interface AddFlockProps {
  start?: boolean;
  /**
   * Called before each render loop with the graphics the boids will be rendered
   * in.
   */
  preRender?: (boidCanvas: Graphics) => void;
  /**
   * Called once before the first render loop with the graphics the boids will
   * be rendered in.
   */
  onStartBoid?: (boidCanvas: Graphics, position: Point) => void;
  /**
   * Called reach render loop with the graphics the boids will be rendered in.
   */
  renderBoid?: (boidCanvas: Graphics, position: Point) => void;
  config?: OriginalConfigProps & { flockColor?: `#${string}` };
}

const defaultRenderBoid = (
  graphics: Graphics,
  pos: Point,
  flockColor: `#${string}`
) => {
  graphics.lineStyle(0);
  graphics.beginFill(rgbToHex(flockColor), 1);
  graphics.drawCircle(pos.x, pos.y, 3);
  graphics.endFill();
};

const defaultPreRender = (graphics: Graphics) => {
  graphics.clear();
};

function fallback<
  Callback extends
    | AddFlockProps["onStartBoid"]
    | AddFlockProps["preRender"]
    | AddFlockProps["renderBoid"]
>(
  graphics: Graphics,
  defaultCallback: Callback,
  callback: Callback | undefined
) {
  if (callback) {
    return (pos: Point) => callback(graphics, pos);
  }

  return (pos: Point) => defaultCallback?.(graphics, pos);
}

export function useAddFlock(props: AddFlockProps = {}) {
  const app = useContext(PixiAppContext);
  const { start = false, config, ...callbacks } = props;
  const {
    count = 500,
    flockColor = "#ffffff",
    randomInitialPosition = true,
    separationWeight = 3,
    alignmentWeight,
    cohesionWeight,
    ...configProps
  } = config ?? {};

  const callbackRefs = useRef(callbacks);
  const configRefs = useRef(configProps);

  useLayoutEffect(() => {
    callbackRefs.current = callbacks;
    configRefs.current = configProps;
  });

  useEffect(
    function flock() {
      if (!app || !start) return;

      const container = new Container();

      container.width = app.screen.width;
      container.height = app.screen.height;

      const flockGraphics = new Graphics();

      container.addChild(flockGraphics);
      app.stage.addChild(container);

      const preRender = fallback(
        flockGraphics,
        callbackRefs.current.preRender,
        defaultPreRender
      );

      const onStartBoid = fallback(
        flockGraphics,
        callbackRefs.current.onStartBoid,
        (graphics: Graphics, pos: Point) =>
          defaultRenderBoid(graphics, pos, flockColor)
      );

      const renderBoid = fallback(
        flockGraphics,
        callbackRefs.current.onStartBoid,
        (graphics: Graphics, pos: Point) =>
          defaultRenderBoid(graphics, pos, flockColor)
      );

      const flock = flocking({
        config: {
          count,
          width: app.screen.width,
          height: app.screen.height,
          randomInitialPosition,
          separationWeight,
          alignmentWeight,
          cohesionWeight,
          ...configProps,
        },
        preRender: () => preRender({} as any),
        onStartBoid,
        renderBoid,
      });

      app.ticker.add(flock.draw);

      let lastTime = 0;
      const tick = setInterval(() => {
        if (++lastTime === 250) {
          clearInterval(tick);
          return;
        }
        flock.mousePressed(
          Math.random() * app.screen.width,
          Math.random() * app.screen.height
        );
      }, 300);

      return () => {
        clearInterval(tick);
        app.ticker.remove(flock.draw);
        app.stage.removeChild(container);
      };
    },
    [
      alignmentWeight,
      app,
      cohesionWeight,
      configProps,
      count,
      flockColor,
      randomInitialPosition,
      separationWeight,
      start,
    ]
  );
}
