import React from "react";
import { PixiAppWrapper } from "../../core/PixiApp/PixiApp";
import { useAddFlock } from "./useAddFlock";

export function UseAddFlocking() {
  return (
    <PixiAppWrapper width={300} height={300}>
      {function useMain() {
        useAddFlock({
          start: true,
          config: {
            flockColor: "#000000",
          },
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}
