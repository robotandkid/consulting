import { Boid } from "./Boid";

/**
 * This class is dumb and should be replaced by flocking.ts;
 */
export class Flock {
  private boids: Boid[] = []; // An ArrayList for all the boids

  get positions() {
    return this.boids.map((boid) => boid.position);
  }

  run() {
    for (const b of this.boids) {
      b.run(this.boids); // Passing the entire list of boids to each boid individually
    }
  }

  addBoid(b: Boid) {
    this.boids.push(b);
  }
}
