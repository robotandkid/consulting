import { Point } from "pixi.js";
import { Boid } from "./Boid";
import { Flock } from "./Flock";

export const flocking = (opts: {
  /**
   * Called before each render loop.
   */
  preRender: () => void;
  onStartBoid: (p: Point) => void;
  /**
   * Renders a single boid
   */
  renderBoid: (p: Point) => void;
  config: {
    count: number;
    width: number;
    height: number;
    randomInitialPosition?: boolean;
    /**
     * The maximum speed of the boids.
     *
     * Defaults to 4.
     */
    maxSpeed?: number;
    /**
     * The separation, alignment, cohesions forces are all limited by maxForce *
     * their respective weights.
     *
     * Defaults to 0.03.
     */
    maxForce?: number;
    /**
     * If two boids are too close, a separation force will steer them away from
     * each other. This force will be multiplied by this weight.
     *
     * Defaults to 1.5.
     */
    separationWeight?: number;
    /**
     * For the average velocity of all nearby boids, an alignment force will
     * nudge all nearby boids towards this average velocity. This alignment
     * force will be multiplied by this weight.
     *
     * Defaults to 1.
     */
    alignmentWeight?: number;
    /**
     * For the average position of all nearby boids, a cohesion force will move
     * all nearby boids towards that position. This cohesion force will be
     * multiplied by this weight.
     *
     * Defaults to 1.
     */
    cohesionWeight?: number;
  };
}) => {
  const flock = new Flock();
  const centerX = opts.config.width / 2;
  const centerY = opts.config.height / 2;

  // Add an initial set of boids into the system
  for (let i = 0; i < opts.config.count; i++) {
    const x = opts.config.randomInitialPosition
      ? Math.random() * opts.config.width
      : centerX;

    const y = opts.config.randomInitialPosition
      ? Math.random() * opts.config.height
      : centerY;

    const boid = new Boid({
      ...opts,
      render: opts.renderBoid,
      x,
      y,
    });

    opts.onStartBoid(boid.position);
    flock.addBoid(boid);
  }

  return {
    draw() {
      opts.preRender();
      flock.run();
    },

    // Add a new boid into the System
    mousePressed(mouseX: number, mouseY: number) {
      flock.addBoid(
        new Boid({
          ...opts,
          render: opts.renderBoid,
          x: mouseX,
          y: mouseY,
        })
      );
    },
  };
};
