import { DotFilter } from "pixi-filters";
import { DotFilterOpts, PixiFilter2 } from "./PixiFilter.types";

export function defaultDotFilter(
  opts: Omit<DotFilterOpts, "filter"> = {}
): PixiFilter2 {
  return {
    filter: new DotFilter(opts.scale ?? 1, opts.radius ?? 1),
    animation: undefined,
  };
}
