import { Filter } from "pixi.js";

const simpleFilterType = [
  "crt",
  "dropshadow",
  "outline",
  "godray",
  "kawaseblur",
  "motionblur",
  "oldfilm",
  "reflection",
  //  | "simplelightmap"
  "tiltshift",
  "zoomblur",
] as const;

interface SimpleFilter {
  filter: typeof simpleFilterType[number];
}

const configurableFilterType = ["dot", "pixelate", "glitch", "glow"] as const;

type ConfigurableFilterType = typeof configurableFilterType[number];

export interface DotFilterOpts {
  filter: Extract<ConfigurableFilterType, "dot">;
  /**
   * Defaults to 1.
   * @TJS-type number
   * @minimum 0
   * @maximum 1
   */
  scale?: number;
  /**
   * The radius. Defaults to 1.
   * @TJS-type number
   * @minimum 0
   */
  radius?: number;
}

export interface PixelateFilterOpts {
  filter: Extract<ConfigurableFilterType, "pixelate">;
  /**
   * The width/height of the pixels. Defaults to 3.
   * @minimum 0
   * @TJS-type number
   */
  size?: number;
}

export interface GlitchFilterOpts {
  filter: Extract<ConfigurableFilterType, "glitch">;
  /**
   * The bigger the number, the more glitches.
   * Defaults to 10.
   * @TJS-type number
   * @minimum 0
   */
  slices?: number;
  /**
   * The offset of the slices. In pixels?
   * Defaults to 100.
   * @TJS-type number
   * @minimum 0
   */
  offset?: number;
}

export interface PixiGlowFilterOpts {
  filter: Extract<ConfigurableFilterType, "glow">;
  /**
   * The strength of the glow outward from the edge of the sprite.
   * Defaults to 2
   * @TJS-type number
   * @minimum 0
   * @maximum 20
   */
  outerStrength?: number;
  /**
   * The strength of the glow inward from the edge of the sprite.
   * Defaults to 0
   * @TJS-type number
   * @minimum 0
   * @maximum 20
   */
  innerStrength?: number;
  /**
   * Defaults to #ffffff
   * @TJS-type string
   */
  color?: `#${string}`;
  /**
   * A number between 0 and 1 that describes the quality of the glow.
   * The higher the number the less performant.
   * @TJS-type number
   * @minimum 0
   * @maximum 1
   */
  quality?: number;
}

type ConfigurableFilter =
  | DotFilterOpts
  | PixelateFilterOpts
  | GlitchFilterOpts
  | PixiGlowFilterOpts;

const filterWithNoDefaultsType = ["colorreplace", "adjustment"] as const;

type FilterWithNoDefaultsType = typeof filterWithNoDefaultsType[number];

interface ColorReplaceFilter {
  filter: Extract<FilterWithNoDefaultsType, "colorreplace">;
  /**
   * HTML color (ex: 0xfffff)
   * @TJS-type number
   */
  originalColor: number;
  /**
   * HTML color (ex: 0xfffff)
   * @TJS-type number
   */
  newColor: number;
  /**
   * The tolerance. The lower the more accurate.
   *
   * @minimum 0
   * @maximum 1
   * @TJS-type number
   */
  epsilon: number;
}

interface AdjustmentFilter {
  filter: Extract<FilterWithNoDefaultsType, "adjustment">;
  /**
   * The amount of luminance. Defaults to 1
   * @TJS-type number
   * @minimum 0
   */
  gamma?: number;
  /**
   * The amount of color saturation. Defaults to 1
   * @TJS-type number
   * @minimum 0
   */
  saturation?: number;
  /**
   * The amount of contrast. Defaults to 1
   * @TJS-type number
   * @minimum 0
   */
  contrast?: number;
  /**
   * The overall brightness. Defaults to 1
   * @TJS-type number
   * @minimum 0
   */
  brightness?: number;
  /**
   * Defaults to 1
   * @TJS-type number
   * @minimum 0
   */
  red?: number;
  /**
   * Defaults to 1
   * @TJS-type number;
   * @minimum 0
   */
  green?: number;
  /**
   * Defaults to 1
   * @TJS-type number;
   * @minimum 0
   */
  blue?: number;
  /**
   * The overall alpha amount. Defaults to 1.
   * @TJS-type number;
   * @minimum 0
   * @maximum 1
   */
  alpha?: number;
}

type FilterWithNoDefaults = ColorReplaceFilter | AdjustmentFilter;

export const supportedFilterType = [
  ...simpleFilterType,
  ...configurableFilterType,
  ...filterWithNoDefaultsType,
] as const;

/**
 * @deprecated use PixiFilter2 instead.
 *
 * The reason is because every filter added to PixiFilter will
 * increase the bundle size, even if you don't use it.
 */
export type PixiFilter =
  | SimpleFilter
  | ConfigurableFilter
  | FilterWithNoDefaults;

export interface PixiFilter2 {
  filter: Filter;
  onStart?: (elapsedTimeMs: number, filter: Filter) => void;
  animation?: (elapsedTimeMs: number, filter: Filter) => void;
}
