import { PixelateFilter } from "pixi-filters";
import { PixelateFilterOpts, PixiFilter2 } from "./PixiFilter.types";

export function defaultPixelateFilter(
  opts: Omit<PixelateFilterOpts, "filter">
): PixiFilter2 {
  return {
    filter: new PixelateFilter(opts.size || 3),
    animation: undefined,
  };
}
