import {
  AdjustmentFilter,
  ColorReplaceFilter,
  CRTFilter,
  DotFilter,
  DropShadowFilter,
  GlitchFilter,
  GlowFilter,
  GodrayFilter,
  KawaseBlurFilter,
  MotionBlurFilter,
  OldFilmFilter,
  OutlineFilter,
  PixelateFilter,
  ReflectionFilter,
  TiltShiftFilter,
  ZoomBlurFilter,
} from "pixi-filters";
import { Point } from "pixi.js";
import { rgbToHex } from "../../utils/color";

import { PixiFilter } from "./PixiFilter.types";

export function instantiatePixiFilter(cmd: PixiFilter) {
  switch (cmd.filter) {
    case "crt": {
      const filter = new CRTFilter({
        lineWidth: 3,
        lineContrast: 0.3,
        noise: 0.2,
        time: 0.5,
        vignetting: 0,
      });

      return {
        filter,
        animation() {
          filter.seed = Math.random();
          filter.time += 0.5;
        },
      };
    }
    case "dot":
      return {
        filter: new DotFilter(cmd.scale, cmd.radius),
        animation: undefined,
      };
    case "dropshadow":
      return { filter: new DropShadowFilter({}), animation: undefined };
    case "glitch": {
      const filter = new GlitchFilter({
        red: new Point(2, 2),
        blue: new Point(10, -4),
        green: new Point(-10, 4),
        slices: cmd.slices ?? 10,
        offset: cmd.offset ?? 100,
        direction: 0,
        fillMode: 2,
        average: false,
        seed: Math.random(),
        minSize: 8,
        sampleSize: 1,
      });

      return {
        filter,
        animation() {
          filter.seed = Math.random();
        },
      };
    }
    case "glow":
      return {
        filter: new GlowFilter({
          distance: 15,
          outerStrength: cmd.outerStrength ?? 2,
          innerStrength: cmd.innerStrength ?? 0,
          color: rgbToHex(cmd.color ?? "#ffffff"),
          quality: cmd.quality ?? 0.2,
          knockout: false,
        }),
        animation: undefined,
      };
    case "godray": {
      const filter = new GodrayFilter({
        alpha: 1,
        angle: 30,
        gain: 0.6,
        lacunarity: 2.75,
        center: new Point(100, -100),
        parallel: true,
        time: 0,
      });

      return {
        filter,
        animation(elapsedTimeMs: number) {
          filter.time += elapsedTimeMs / 1000;
        },
      };
    }
    case "kawaseblur":
      return {
        filter: new KawaseBlurFilter([4, 3]),
        animation: undefined,
      };
    case "motionblur":
      return {
        filter: new MotionBlurFilter([40, 40], 15),
        animation: undefined,
      };
    case "oldfilm": {
      const filter = new OldFilmFilter();
      return {
        filter,
        animation() {
          filter.seed = Math.random();
        },
      };
    }
    case "outline": {
      const filter = new OutlineFilter(4, 0x0, 0.25);

      return { filter, animation: undefined };
    }
    case "pixelate":
      return {
        filter: new PixelateFilter(cmd.size || 3),
        animation: undefined,
      };
    case "reflection": {
      const filter = new ReflectionFilter();

      return {
        filter,
        animation() {
          filter.time += 0.1;
        },
      };
    }
    case "tiltshift":
      return { filter: new TiltShiftFilter(), animation: undefined };
    case "zoomblur":
      return {
        filter: new ZoomBlurFilter({
          strength: 0.1,
          innerRadius: 80,
        }),
        animation: undefined,
      };
    case "colorreplace":
      return {
        filter: new ColorReplaceFilter(
          cmd.originalColor,
          cmd.newColor,
          cmd.epsilon
        ),
        animation: undefined,
      };
    case "adjustment":
      return {
        filter: new AdjustmentFilter({
          gamma: cmd.gamma,
          saturation: cmd.saturation,
          contrast: cmd.contrast,
          brightness: cmd.brightness,
          red: cmd.red,
          green: cmd.green,
          blue: cmd.blue,
          alpha: cmd.alpha,
        }),
        animation: undefined,
      };

    default: {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const x: never = cmd;
    }
  }
}
