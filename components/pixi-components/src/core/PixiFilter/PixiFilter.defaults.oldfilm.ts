import { OldFilmFilter, OldFilmFilterOptions } from "pixi-filters";
import { PixiFilter2 } from "./PixiFilter.types";

export function defaultOldFilmFilter(
  props: Partial<OldFilmFilterOptions> = {}
): PixiFilter2 {
  return {
    filter: new OldFilmFilter(props),
    animation(_, filter) {
      (filter as OldFilmFilter).seed = Math.random();
    },
  };
}
