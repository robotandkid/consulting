import { Application } from "pixi.js";
import React, { useCallback, useState } from "react";
import { TypewriterText } from "../../components/TypewriterText/TypewriterText";
import { PixiApp, PixiAppWrapper } from "../PixiApp/PixiApp";
import { RegisterPixiEventListeners } from "../PixiEvents/PixiEvents";
import { useLoadPixiSprite } from "../PixiSprite/PixiSprite";
import { defaultTextStyle } from "../PixiText/defaultTextStyle";
import { PixiFilter2, useAddPixiFilter2 } from "./PixiFilter";
import { defaultDotFilter } from "./PixiFilter.defaults.dot";

function Fixture(props: { text: string[] }) {
  const [line, setLine] = useState(0);
  const [lineId, setLineId] = useState(0);

  const [app, setApp] = useState<Application>();

  return (
    <>
      <PixiApp
        width={200}
        height={100}
        resolution={window.devicePixelRatio || 1}
        backgroundColor="#000000"
        resources={[
          {
            type: "sound",
            path: "/sounds/textscroll.wav",
          },
        ]}
        onAppInit={setApp}
      >
        <PixiFilter2 comp={app} filters={defaultDotFilter()} />
        <TypewriterText
          lineId={`${lineId}`}
          style={{
            ...defaultTextStyle(),
            width: 200,
            height: 100,
          }}
          soundPath={"/sounds/textscroll.wav"}
          typingSpeedInMs={20}
          onLineDone={useCallback(() => {
            setLine((index) => {
              if (index < props.text.length - 1) {
                setLineId((c) => c + 1);
                return index + 1;
              }
              return index;
            });
          }, [props.text.length])}
          onLineDoneDelayMs={500}
          parent={app}
        >
          {props.text[line]}
        </TypewriterText>
        <RegisterPixiEventListeners
          onPreviousEvent={useCallback(() => {
            setLine(0);
            setLineId((c) => c + 1);
          }, [])}
        />
      </PixiApp>
    </>
  );
}

export function JSXFilter() {
  return (
    <Fixture
      text={[
        "the first line",
        "the second line",
        "the third line",
        "the fourth line",
        "the fifth line",
      ]}
    />
  );
}

export function HookFilter() {
  return (
    <PixiAppWrapper
      width={200}
      height={100}
      resolution={window.devicePixelRatio || 1}
      backgroundColor="#000000"
      resources={[{ type: "image", path: "/images/test-portrait.jpeg" }]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/test-portrait.jpeg",
          height: 100,
          width: 200,
          parentType: "app",
        });

        useAddPixiFilter2({
          comp: sprite?.sprite,
          filters: defaultDotFilter(),
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}
