import { GlitchFilter, GlitchFilterOptions } from "pixi-filters";
import { GlitchFilterOpts, PixiFilter2 } from "./PixiFilter.types";

export function defaultGlitchFilter(
  opts: Partial<GlitchFilterOptions> & Omit<GlitchFilterOpts, "filter"> = {}
): PixiFilter2 {
  const filter = new GlitchFilter({
    red: opts.red ?? [2, 2],
    blue: opts.blue ?? [10, -4],
    green: opts.green ?? [-10, 4],
    slices: opts.slices ?? 10,
    offset: opts.offset ?? 100,
    direction: opts.direction ?? 0,
    fillMode: opts.fillMode ?? 2,
    average: opts.average ?? false,
    seed: opts.seed ?? Math.random(),
    minSize: opts.seed ?? 8,
    sampleSize: opts.sampleSize ?? 1,
  });

  return {
    filter,
    animation() {
      filter.seed = Math.random();
    },
  };
}
