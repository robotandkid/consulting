import { Application, Filter } from "pixi.js";
import {
  useContext,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { createComponent } from "../../utils/createComponent";
import { PixiAppTickerContext } from "../PixiApp/PixiContexts";
import { getFilters } from "../PixiApp/useAddToParent";
import { instantiatePixiFilter } from "./instantiatePixiFilter";
import {
  PixiFilter as PixiFilterType,
  PixiFilter2 as PixiFilterType2,
} from "./PixiFilter.types";

function filterMap(filters: Filter[]) {
  return filters.reduce(
    (total, filter) =>
      Object.assign(total, { [filter.constructor.name]: true }),
    {} as { [key: string]: true }
  );
}

/**
 * @deprecated use version2
 */
export function useAddPixiFilter<
  Comp extends Application | { filters: Filter[] | null }
>(props: {
  comp: Comp | undefined;
  filters: PixiFilterType | Array<PixiFilterType> | undefined;
}) {
  const { comp, filters } = props;

  const instantiatedFilters = useMemo(() => {
    if (!filters) return [];

    const filtersAsArray = Array.isArray(filters) ? filters : [filters];

    return filtersAsArray.map(
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (filter) => instantiatePixiFilter(filter)!
    );
  }, [filters]);

  return useAddPixiFilter2({
    comp,
    filters: instantiatedFilters,
  });
}

/**
 * The only catch with this method is that changing the filters doesn't trigger
 * a re-render. But you can use the force render prop to force a re-render.
 */
export function useAddPixiFilter2<
  Comp extends Application | { filters: Filter[] | null }
>(props: {
  comp: Comp | undefined;
  filters: PixiFilterType2 | PixiFilterType2[];
}) {
  const { comp, filters } = props;
  const appTicker = useContext(PixiAppTickerContext);
  const [addedFilters, setAddedFilters] = useState<PixiFilterType2[]>([]);

  const filtersRef = useRef(filters);

  useLayoutEffect(() => {
    filtersRef.current = filters;
  });

  useEffect(
    function addFilter() {
      if (!comp || !appTicker || !filtersRef.current) {
        return;
      }

      let dedupedFilters: PixiFilterType2[] = [];

      const filtersAsArray = Array.isArray(filtersRef.current)
        ? filtersRef.current
        : [filtersRef.current];

      const _filterClassnames = filterMap(getFilters(comp).filters || []);

      dedupedFilters = filtersAsArray.filter(
        ({ filter }) => !_filterClassnames[filter.constructor.name]
      );

      setAddedFilters(dedupedFilters);

      appTicker.addOnce(() => {
        // now add the filters
        getFilters(comp).filters = [
          ...(getFilters(comp).filters || []),
          ...dedupedFilters.map(({ filter }) => filter),
        ];
      });

      return () => {
        appTicker.addOnce(() => {
          getFilters(comp).filters = null as any;
        });
      };
    },
    [appTicker, comp]
  );

  useEffect(
    function triggerAnimations() {
      if (!appTicker) return;

      const callbacks: Array<(args: any) => void> = [];

      addedFilters.forEach(({ filter, animation, onStart }) => {
        let skipFirst: boolean | "done" = false;

        if (onStart) {
          appTicker.addOnce(() => onStart(0, filter));
          skipFirst = true;
        }

        if (animation) {
          let elapsedMS = 0;

          const callback = () => {
            if (skipFirst === true) {
              skipFirst = "done";
              return;
            }
            elapsedMS += appTicker.elapsedMS / 16.66;
            animation(elapsedMS, filter);
          };
          callbacks.push(callback);
          appTicker.add(callback);
        }
      });

      return () => {
        callbacks.forEach((callback) => {
          appTicker.remove(callback);
        });
      };
    },
    [appTicker, addedFilters]
  );

  return addedFilters.length > 0 ? comp : undefined;
}

/**
 * @deprecated use version2
 */
export const PixiFilter = createComponent(useAddPixiFilter);

export const PixiFilter2 = createComponent(useAddPixiFilter2);
