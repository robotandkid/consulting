import { GlowFilter, GlowFilterOptions } from "pixi-filters";
import { rgbToHex } from "../../utils/color";
import { PixiGlowFilterOpts } from "./PixiFilter.types";

export function defaultGlowFilter(
  opts: Partial<GlowFilterOptions> & Omit<PixiGlowFilterOpts, "filter">
) {
  return {
    filter: new GlowFilter({
      distance: 15,
      outerStrength: opts.outerStrength ?? 2,
      innerStrength: opts.innerStrength ?? 0,
      color: rgbToHex(opts.color ?? "#ffffff"),
      quality: opts.quality ?? 0.2,
      knockout: false,
    }),
    animation: undefined,
  };
}
