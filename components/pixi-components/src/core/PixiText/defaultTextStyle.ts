import { ITextStyle } from "pixi.js";

export function defaultTextStyle(): Partial<ITextStyle> {
  return {
    fontFamily: "Arial",
    fontSize: 12,
    fontStyle: "italic",
    fontWeight: "bold",
    fill: ["#ffffff", "#00ff99"], // gradient
    stroke: "#4a1850",
    strokeThickness: 5,
    dropShadow: true,
    dropShadowColor: "#000000",
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,
    align: "center",
    wordWrap: true,
  };
}
