import { m } from "framer-motion";
import "pixi-sound";
import { Application, SCALE_MODES, settings } from "pixi.js";
import React, {
  RefObject,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import { rgbToHex } from "../../utils/color";
import { createComponent } from "../../utils/createComponent";
import { PixiEvents } from "../PixiEvents/PixiEvents";
import { PixiResources, Resources } from "../PixiResources/PixiResources";
import { PixiAppOpts } from "./PixiApp.types";
import {
  PixiAppContext,
  PixiAppTickerContext,
  PixiCanvasRefContext,
  PixiLoaderResourcesContext,
} from "./PixiContexts";

export function useLoadPixiApp(
  props: PixiAppOpts & {
    pixiRef: RefObject<HTMLCanvasElement | null>;
  }
) {
  const { onAppInit } = props;

  const initCalled = useRef(false);
  const application = useRef<Application>();

  useEffect(
    function resize() {
      if (application.current) {
        application.current.renderer.resize(props.width, props.height);
      }
    },
    [props.height, props.width]
  );

  useEffect(
    function create() {
      if (initCalled.current) {
        return;
      }

      initCalled.current = true;

      const instance = new Application({
        width: props.width,
        height: props.height,
        backgroundColor: rgbToHex(props.backgroundColor) ?? 0xffffff,
        resolution: props.resolution ?? (window.devicePixelRatio || 1),
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        view: props.pixiRef.current!,
      });

      // Scale mode for all textures, will retain pixelation
      settings.SCALE_MODE = SCALE_MODES.NEAREST;

      // enable key handlers
      instance.view.tabIndex = 0;

      onAppInit?.(instance);

      application.current = instance;
    },
    [
      onAppInit,
      props.backgroundColor,
      props.height,
      props.pixiRef,
      props.resolution,
      props.width,
    ]
  );
}

const LoadPixiApp = createComponent(useLoadPixiApp);

export function PixiApp(props: PixiAppOpts) {
  const { onCanvasClick, onCanvasFocus, onAppInit } = props;

  const [resources, setResources] = useState<Resources>();
  const [application, setApplication] = useState<Application>();
  const pixiRef = useRef<HTMLCanvasElement>(null);

  useEffect(
    function () {
      if (onCanvasClick) {
        window.addEventListener("click", onCanvasClick);
      }

      if (onCanvasFocus) {
        window.addEventListener("focus", onCanvasFocus);
      }

      return () => {
        onCanvasClick && window.removeEventListener("click", onCanvasClick);
        onCanvasFocus && window.removeEventListener("focus", onCanvasFocus);
      };
    },
    [onCanvasClick, onCanvasFocus]
  );

  return (
    <>
      <m.canvas
        layoutId={props.layoutId}
        ref={pixiRef}
        width={props.width}
        height={props.height}
      />
      <PixiResources
        resourcesToLoad={props.resources}
        onResourcesChange={setResources}
      />
      <LoadPixiApp
        {...props}
        pixiRef={pixiRef}
        onAppInit={useCallback(
          (app: Application) => {
            onAppInit?.(app);
            setApplication(app);
          },
          [onAppInit]
        )}
      />
      <PixiAppContext.Provider value={application}>
        <PixiCanvasRefContext.Provider value={application?.view}>
          <PixiAppTickerContext.Provider value={application?.ticker}>
            <PixiLoaderResourcesContext.Provider value={resources}>
              <PixiEvents>{props.children}</PixiEvents>
            </PixiLoaderResourcesContext.Provider>
          </PixiAppTickerContext.Provider>
        </PixiCanvasRefContext.Provider>
      </PixiAppContext.Provider>
    </>
  );
}

export function PixiAppWrapper(
  props: Omit<PixiAppOpts, "children"> & { children: () => JSX.Element | null }
) {
  const { children: Comp, ...rest } = props;

  return (
    <PixiApp {...rest}>
      <Comp />
    </PixiApp>
  );
}
