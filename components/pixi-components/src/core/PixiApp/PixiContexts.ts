import { Application, LoaderResource } from "pixi.js";
import { createContext } from "../../utils/context";
import { InternalPixiResourceMetaData } from "./PixiApp.types";

interface PixiContextValue {
  app: Application | undefined;
  appTicker: Application["ticker"] | undefined;
  canvasRef: HTMLCanvasElement | undefined;
  resources: Record<
    InternalPixiResourceMetaData["id"],
    LoaderResource | undefined
  >;
}

export const PixiAppContext = createContext<PixiContextValue["app"]>();

export const PixiAppTickerContext =
  createContext<PixiContextValue["appTicker"]>();

export const PixiCanvasRefContext =
  createContext<PixiContextValue["canvasRef"]>();

export const PixiLoaderResourcesContext = createContext<
  PixiContextValue["resources"] | undefined
>();
