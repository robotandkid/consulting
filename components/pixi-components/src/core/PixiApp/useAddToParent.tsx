import { Application, Container, DisplayObject, Filter } from "pixi.js";
import { useContext, useEffect, useState } from "react";
import { PixiAppContext } from "./PixiContexts";

export type PixiParent = Application | Container | null;

export function isApplication(cmp: PixiParent | undefined): cmp is Application {
  return !!(cmp as Application | undefined)?.stage;
}

export function isContainer(cmp: PixiParent | undefined): cmp is Container {
  return !!(cmp as Container | undefined)?.addChild;
}

export function getDims(cmp: PixiParent | undefined) {
  if (isApplication(cmp)) {
    return {
      width: cmp.screen.width,
      height: cmp.screen.height,
    };
  }

  if (isContainer(cmp)) {
    return {
      width: cmp.width,
      height: cmp.height,
    };
  }

  return {
    width: undefined,
    height: undefined,
  };
}

export function getFilters<
  Comp extends Application | { filters: Filter[] | null }
>(cmp: Comp | undefined): { filters: Filter[] | undefined | null } {
  const _cmp = cmp as any;

  if (isApplication(_cmp)) return _cmp.stage;

  return _cmp;
}

export type PixiParentProps =
  | {
      parentType: "app";
    }
  | {
      parentType: "container";
      parent: PixiParent | undefined;
    }
  | {
      parentType: "none";
    };

type NullableDisplayObject = DisplayObject | undefined;

const isDisplayObjectArray = (
  array: NullableDisplayObject[]
): array is DisplayObject[] => !array.some((item) => !item);

export function useAddToParent(
  props: {
    child: NullableDisplayObject | NullableDisplayObject[];
  } & PixiParentProps
) {
  const { child } = props;
  const [added, setAdded] = useState(false);
  const app = useContext(PixiAppContext);

  let stage: Container | undefined | null;

  if (props.parentType === "app") stage = app?.stage;
  else if (props.parentType === "container")
    stage = isApplication(props.parent) ? props.parent.stage : props.parent;

  useEffect(() => {
    if (!child || !stage || !app) return;

    const children = Array.isArray(child) ? child : [child];

    if (!isDisplayObjectArray(children)) return;

    stage.addChild(...children);
    setAdded(true);

    return () => {
      stage?.removeChild(...children);
      setAdded(false);
    };
  }, [stage, child, app]);

  if (props.parentType === "none") return true;

  return added;
}
