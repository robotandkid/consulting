import { Application } from "pixi.js";

export interface PixiResourceMetaData {
  type: "image" | "sound" | "shader";
  path: string;
}

export interface InternalPixiResourceMetaData {
  id: `${PixiResourceMetaData["type"]}:${PixiResourceMetaData["path"]}`;
  path: string;
}

export interface PixiAppOpts {
  /**
   * For use with FramerMotion#AnimatePresence
   */
  layoutId?: string;
  width: number;
  height: number;
  resolution?: number;
  backgroundColor?: `#${string}`;
  resources?: PixiResourceMetaData[];
  onAppInit?: (app: Application) => void;
  onCanvasFocus?: () => void;
  onCanvasClick?: () => void;
  exportToVideo?: boolean;
  children?: React.ReactNode | React.ReactNodeArray;
}
