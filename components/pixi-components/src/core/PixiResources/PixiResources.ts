import { Loader, LoaderResource } from "pixi.js";
import { useEffect, useRef } from "react";
import { createComponent } from "../../utils/createComponent";
import {
  InternalPixiResourceMetaData,
  PixiResourceMetaData,
} from "../PixiApp/PixiApp.types";

export type Resources = Record<
  InternalPixiResourceMetaData["id"],
  LoaderResource | undefined
>;

export interface LoadResourcesOpts {
  resourcesToLoad: PixiResourceMetaData[] | undefined;
  onResourcesChange: (resources: Resources) => void;
}

export function useLoadResources(props: LoadResourcesOpts) {
  const { resourcesToLoad, onResourcesChange } = props;

  const loader = useRef(new Loader());

  useEffect(
    // Need to handle
    // - [x] initial load
    // - [x] resource change
    // - clearing out resources
    function loadResources() {
      if (!resourcesToLoad || resourcesToLoad.length === 0) {
        onResourcesChange({});
        return;
      }

      resourcesToLoad.forEach((resource) => {
        const { id, path }: InternalPixiResourceMetaData = {
          id: `${resource.type}:${resource.path}`,
          path: resource.path,
        };

        if (!loader.current.resources[id]) {
          if (resource.type === "image") {
            loader.current.add({
              name: id,
              url: path,
              loadType: 2,
            });
          } else {
            loader.current.add(id, path);
          }
        }
      });

      loader.current.load((_, resources) => {
        onResourcesChange({ ...resources });
      });
    },
    [onResourcesChange, resourcesToLoad]
  );
}

export const PixiResources = createComponent(useLoadResources);
