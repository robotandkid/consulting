import React, { ReactNode, useContext, useEffect, useRef } from "react";
import { makeSimpleContext } from "../../utils/context";
import { PixiAppContext } from "../PixiApp/PixiContexts";
import {
  pixiDoubleTapEvent,
  pixiDragLeftEvent,
  pixiDragRightEvent,
  pixiTapEvent,
} from "./PixiEvents.types";

const rightArrow = 39;
const leftArrow = 37;
const doubleKeyPressThresholdInMs = 300;

interface NextEventProps {
  onNextEvent?: () => void;
  onNextEventDoublePress?: () => void;
}

interface PreviousEventProps {
  onPreviousEvent?: () => void;
  onPreviousEventDoublePress?: () => void;
}

interface KeyDownEventHandler {
  event: "keydown";
  callback: () => {
    listener: (this: Element, evt: KeyboardEvent) => any;
  };
}

interface PixiTapEventHandler {
  event: typeof pixiTapEvent;
  callback: () => {
    listener: () => any;
  };
}

interface PixiDragRightEventHandler {
  event: typeof pixiDragRightEvent;
  callback: () => {
    listener: () => any;
  };
}

interface PixiDragLeftEventHandler {
  event: typeof pixiDragLeftEvent;
  callback: () => {
    listener: () => any;
  };
}

interface PixiDoubleTapEventHandler {
  event: typeof pixiDoubleTapEvent;
  callback: () => {
    listener: () => any;
  };
}

interface PixiEvent {
  type:
    | "nextEventDoublePress"
    | "nextEvent"
    | "previousEventDoublePress"
    | "previousEvent";
}

const [PixiEventProvider, PixiEventContext, PixiEventUpdateContext] =
  makeSimpleContext<PixiEvent | undefined>("PixiEvent");

function useEventListener(
  props: { element: Element | undefined } & (
    | KeyDownEventHandler
    | PixiTapEventHandler
    | PixiDragRightEventHandler
    | PixiDragLeftEventHandler
    | PixiDoubleTapEventHandler
  )
) {
  const { element, event, callback } = props;
  const fnsRef = useRef(callback);
  const eventRef = useRef(event);

  useEffect(() => {
    fnsRef.current = callback;
    eventRef.current = event;
  });

  useEffect(
    function addEventListener() {
      if (!element) return;

      const { listener } = fnsRef.current();
      const _event = eventRef.current;

      element.addEventListener(_event, listener as any);

      return () => {
        element.removeEventListener(_event, listener as any);
      };
    },
    [element]
  );
}

function SetupListeners() {
  const canvasRef = useContext(PixiAppContext)?.view;
  const broadcast = useContext(PixiEventUpdateContext);

  useEventListener({
    event: "keydown",
    element: canvasRef,
    callback: function _leftArrow() {
      let previousTime = Date.now();

      return {
        listener: function detectLeftArrow(ev: KeyboardEvent) {
          if (ev.keyCode === leftArrow) {
            const curTime = Date.now();

            if (curTime - previousTime < doubleKeyPressThresholdInMs) {
              broadcast({ type: "previousEventDoublePress" });
            } else {
              broadcast({ type: "previousEvent" });
            }

            previousTime = curTime;
          }
        },
      };
    },
  });

  useEventListener({
    event: "keydown",
    element: canvasRef,
    callback: function _rightArrow() {
      let previousTime = Date.now();

      return {
        listener: function detectRightArrow(ev: KeyboardEvent) {
          if (ev.keyCode === rightArrow) {
            const curTime = Date.now();

            if (curTime - previousTime < doubleKeyPressThresholdInMs) {
              broadcast({ type: "nextEventDoublePress" });
            } else {
              broadcast({ type: "nextEvent" });
            }

            previousTime = curTime;
          }
        },
      };
    },
  });

  useEventListener({
    event: pixiTapEvent,
    element: canvasRef,
    callback: () => {
      return {
        listener: function detect() {
          broadcast({ type: "nextEvent" });
        },
      };
    },
  });

  useEventListener({
    event: pixiDragRightEvent,
    element: canvasRef,
    callback: () => {
      return {
        listener: function detect() {
          broadcast({ type: "nextEvent" });
        },
      };
    },
  });

  useEventListener({
    event: pixiDragLeftEvent,
    element: canvasRef,
    callback: () => {
      return {
        listener: function detect() {
          broadcast({ type: "previousEvent" });
        },
      };
    },
  });

  useEventListener({
    event: pixiDoubleTapEvent,
    element: canvasRef,
    callback: () => {
      return {
        listener: function detect() {
          broadcast({ type: "nextEventDoublePress" });
        },
      };
    },
  });

  return null;
}

export function PixiEvents(props: { children: ReactNode }) {
  return (
    <PixiEventProvider initialValue={undefined}>
      <SetupListeners />
      {props.children}
    </PixiEventProvider>
  );
}

export function usePixiEvents() {
  return useContext(PixiEventContext);
}

/**
 * If you want to register callbacks for the next/previous events, use this.
 */
export function RegisterPixiEventListeners(
  props: NextEventProps & PreviousEventProps
) {
  const {
    onNextEvent,
    onNextEventDoublePress,
    onPreviousEvent,
    onPreviousEventDoublePress,
  } = props;

  const onNextEventRef = useRef(onNextEvent);
  const onNextEventDoublePressRef = useRef(onNextEventDoublePress);
  const onPreviousEventRef = useRef(onPreviousEvent);
  const onPreviousEventDoublePressRef = useRef(onPreviousEventDoublePress);

  useEffect(() => {
    onNextEventRef.current = onNextEvent;
    onNextEventDoublePressRef.current = onNextEventDoublePress;
    onPreviousEventRef.current = onPreviousEvent;
    onPreviousEventDoublePressRef.current = onPreviousEventDoublePress;
  });

  const event = usePixiEvents();

  useEffect(() => {
    if (event?.type === "nextEvent") {
      onNextEventRef.current?.();
    }
  }, [event]);

  useEffect(() => {
    if (event?.type === "nextEventDoublePress") {
      onNextEventDoublePressRef.current?.();
    }
  }, [event]);

  useEffect(() => {
    if (event?.type === "previousEvent") {
      onPreviousEventRef.current?.();
    }
  }, [event]);

  useEffect(() => {
    if (event?.type === "previousEventDoublePress") {
      onPreviousEventDoublePressRef.current?.();
    }
  }, [event]);

  return null;
}
