export const pixiTapEvent = "pixi:tap";
export const pixiDoubleTapEvent = "pixi:doubletap";
export const pixiDragLeftEvent = "pixi:dragleft";
export const pixiDragRightEvent = "pixi:dragright";
