import { Texture } from "pixi.js";
import React from "react";
import { PixiAppWrapper } from "../PixiApp/PixiApp";
import { useToyShader } from "./PixiShader";

const width = 250;
const height = (width * 8) / 10;

export function CyberFujiFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "shader", path: "/shaders/cyber-fuji.frag" }]}
    >
      {function useMain() {
        useToyShader({
          pathToShader: "/shaders/cyber-fuji.frag",

          uniforms: {
            iResolution: [width, height],
            iTime: 0,
          },
          animate: (uniforms, delta) => {
            uniforms.iTime += 0.01 * delta;
          },
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function NeonWebFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "shader", path: "/shaders/neon-web.frag" }]}
    >
      {function useMain() {
        useToyShader({
          pathToShader: "/shaders/neon-web.frag",

          uniforms: {
            u_resolution: [width, height],
            time: 2.0,
          },
          animate: (uniforms, delta) => {
            uniforms.time += 10 * delta;
          },
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function HaloFixture() {
  return (
    <PixiAppWrapper
      width={250}
      height={250}
      resources={[{ type: "shader", path: "/shaders/halo.frag" }]}
    >
      {function useMain() {
        useToyShader({
          pathToShader: "/shaders/halo.frag",
          uniforms: {},
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function VoxelEdgesFixture() {
  return (
    <PixiAppWrapper
      width={250}
      height={(250 * 8) / 10}
      resources={[
        { type: "shader", path: "/shaders/voxel-edges.frag" },
        {
          type: "image",
          path: "/images/gray_noise_medium.png",
        },
      ]}
    >
      {function useMain() {
        useToyShader({
          pathToShader: "/shaders/voxel-edges.frag",
          uniforms: {
            iTime: 0,
            iResolution: [800, (800 * 8) / 10],
            iChannel0: Texture.from("/images/gray_noise_medium.png"),
          },
          animate: (uniforms, delta) => {
            uniforms.iTime += 0.005 * delta;
          },
          withMouse: { uniform: "uMouse" },
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function CloudsFixture() {
  const width = 500;
  const height = (width * 720) / 1280;

  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[
        { type: "shader", path: "/shaders/ai.frag" },
        {
          type: "image",
          path: "/images/tex-abstract-art.png",
        },
        {
          type: "image",
          path: "/images/cloud_channel_1b.png",
        },
      ]}
    >
      {function useMain() {
        useToyShader({
          pathToShader: "/shaders/ai.frag",
          withChannel0: {
            uniform: "iChannel0",
            path: "/images/tex-abstract-art.png",
          },
          withChannel1: {
            uniform: "iChannel1",
            path: "/images/cloud_channel_1b.png",
          },
          withMouse: {
            uniform: "iMouse",
          },
          uniforms: {
            iTime: 0,
            iResolution: [width, height],
          },
          animate: (uniforms, delta) => {
            uniforms.iTime += 0.01 * delta;
          },
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function WarpFixture() {
  const width = 500;
  const height = (width * 8) / 10;

  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "shader", path: "/shaders/warp.frag" }]}
    >
      {function useMain() {
        useToyShader({
          pathToShader: "/shaders/warp.frag",
          uniforms: {
            iTime: 0,
            iResolution: [width, height],
          },
          animate: (uniforms, delta) => {
            uniforms.iTime += 0.001 * delta;
          },
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}
