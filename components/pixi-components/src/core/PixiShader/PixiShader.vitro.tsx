import { Point } from "pixi.js";
import React from "react";
import { PixiAppWrapper } from "../PixiApp/PixiApp";
import { useAddToParent } from "../PixiApp/useAddToParent";
import { useLoadPixiSprite } from "../PixiSprite/PixiSprite";
import { usePixiShaderFromResource } from "./PixiShader";
import { useLightningShader } from "./useLightningShader";

const width = 150;
const height = 150;

export function BasicShaderFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[
        { type: "image", path: "/images/summerportrait.jpeg" },
        { type: "shader", path: "/shaders/basic.frag" },
      ]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/summerportrait.jpeg",
          width,
          height,
          parentType: "none",
        });

        const spriteWithShader = usePixiShaderFromResource({
          path: "/shaders/basic.frag",
          comp: sprite?.sprite,
          uniforms: {
            customUniform: 0,
          },
          animate: (filter, delta) => {
            filter.uniforms.customUniform += 0.04 * delta;
          },
        });

        useAddToParent({
          child: spriteWithShader?.comp,
          parentType: "app",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function CutShaderFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[
        { type: "image", path: "/images/summerportrait.jpeg" },
        { type: "shader", path: "/shaders/cuts.frag" },
      ]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/summerportrait.jpeg",
          width,
          height,
          parentType: "none",
        });

        const comp = usePixiShaderFromResource({
          path: "/shaders/cuts.frag",
          comp: sprite?.sprite,
          uniforms: {
            u_resolution: [width / 2, height],
            u_mouse: [width / 2, height / 2],
            u_time: 0,
          },
          animate: (filter, delta) => {
            filter.uniforms.u_time += 0.03 * delta;
          },
        });

        useAddToParent({
          child: comp?.comp,
          parentType: "app",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function LightningShaderFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[
        { type: "image", path: "/images/summerportrait.jpeg" },
        { type: "shader", path: "/shaders/lightning.frag" },
      ]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/summerportrait.jpeg",
          width,
          height,
          parentType: "none",
        });

        const shader = useLightningShader({
          comp: sprite?.sprite,
          uniforms: {
            u_precision: "highp",
            u_time: 0,
            u_resolution: [4 * width, 2.5 * height],
            u_speed: 0.005,
          },
        });

        useAddToParent({
          child: shader?.comp,
          parentType: "app",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function LensFlareShaderFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[
        { type: "image", path: "/images/summerportrait.jpeg" },
        { type: "shader", path: "/shaders/lensflare.frag" },
      ]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/summerportrait.jpeg",
          width,
          height,
          parentType: "none",
        });

        const shader = usePixiShaderFromResource({
          path: "/shaders/lensflare.frag",
          comp: sprite?.sprite,
          uniforms: {
            u_time: 0,
            u_resolution: new Point(width, height),
            u_center: [width / 2, 0.5, 0],
          },
          animate: (filter, delta) => {
            filter.uniforms.u_time += 0.005 * delta;
          },
        });

        useAddToParent({
          child: shader?.comp,
          parentType: "app",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function SandboxShaderFixture() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[
        { type: "image", path: "/images/summerportrait.jpeg" },
        { type: "shader", path: "/shaders/rgb-split.frag" },
      ]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/summerportrait.jpeg",
          width,
          height,
          parentType: "none",
        });

        const spriteWithShader = usePixiShaderFromResource({
          path: "/shaders/rgb-split.frag",
          comp: sprite?.sprite,
          uniforms: {
            customUniform: 0,
          },
          animate: (filter, delta) => {
            filter.uniforms.customUniform += 0.04 * delta;
          },
        });

        useAddToParent({
          child: spriteWithShader?.comp,
          parentType: "app",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}
