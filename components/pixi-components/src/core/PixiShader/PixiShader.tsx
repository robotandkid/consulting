import {
  Application,
  Filter,
  Graphics,
  MIPMAP_MODES,
  Sprite,
  WRAP_MODES,
} from "pixi.js";
import {
  useContext,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import { InternalPixiResourceMetaData } from "../PixiApp/PixiApp.types";
import {
  PixiAppContext,
  PixiLoaderResourcesContext,
} from "../PixiApp/PixiContexts";

export function usePixiShaderFromResource<_Sprite extends Sprite>(props: {
  path: string;
  comp: _Sprite | undefined;
  onInit?: (container: _Sprite, app: Application) => void;
  uniforms?: Record<string, unknown>;
  animate?: (filter: Filter, delta: number) => void;
}) {
  const { path, ...rest } = props;
  const resources = useContext(PixiLoaderResourcesContext);

  const { id }: InternalPixiResourceMetaData = {
    id: `shader:${path}`,
    path,
  };

  return usePixiShader({
    frag: resources?.[id]?.data,
    ...rest,
  });
}

export function usePixiShader<_Sprite extends Sprite>(props: {
  frag?: string;
  comp: _Sprite | undefined;
  onInit?: (container: _Sprite, app: Application) => void;
  uniforms?: Record<string, unknown>;
  animate?: (filter: Filter, delta: number) => void;
}) {
  const { frag, comp, uniforms, animate, onInit } = props;
  const resources = useContext(PixiLoaderResourcesContext);
  const app = useContext(PixiAppContext);
  const [shader, setShader] = useState<Filter>();
  const [compWithShader, setCompWithShader] = useState<_Sprite>();

  const uniformsRef = useRef(uniforms);
  const animateRef = useRef(animate);
  const onInitRef = useRef(onInit);

  useLayoutEffect(() => {
    uniformsRef.current = uniforms;
    animateRef.current = animate;
    onInitRef.current = onInit;
  });

  useEffect(() => {
    if (!frag || !app || !comp) return;

    onInitRef.current?.(comp, app);

    const filter = new Filter(undefined, frag, {
      ...(uniformsRef.current ?? {}),
    });

    setShader(filter);
  }, [app, frag, resources, comp]);

  useEffect(
    function animate() {
      if (!app || !shader || !comp) return;

      comp.filters = [shader];

      let animate: undefined | ((delta: number) => void);

      if (animateRef.current) {
        animate = (delta: number) => {
          if (shader) animateRef.current?.(shader, delta);
        };
        app.ticker.add(animate);
      }

      setCompWithShader(comp);

      return () => {
        if (animate) app.ticker.remove(animate);
      };
    },
    [app, comp, shader]
  );

  return comp ? { comp: compWithShader, shader } : undefined;
}

const defaultVertexFragFor2_0 = `
#version 300 es
precision mediump float;
in vec2 aVertexPosition;
uniform mat3 projectionMatrix;
out vec2 vTextureCoord;
uniform vec4 inputSize;
uniform vec4 outputFrame;
vec4 filterVertexPosition( void )
{
  vec2 position = aVertexPosition * max(outputFrame.zw, vec2(0.)) + outputFrame.xy;
  return vec4((projectionMatrix * vec3(position, 1.0)).xy, 0.0, 1.0);
}
vec2 filterTextureCoord( void )
{
  return aVertexPosition * (outputFrame.zw * inputSize.zw);
}
void main(void)
{
  gl_Position = filterVertexPosition();
  vTextureCoord = filterTextureCoord();
}`;

export function useToyShader<Uniforms extends Record<string, unknown>>(props: {
  pathToShader: string;
  onInit?: (app: Application) => void;
  uniforms?: Uniforms;
  animate?: (uniforms: Uniforms, delta: number) => void;
  withMouse?: {
    uniform: string;
  };
  withChannel0?: {
    uniform: string;
    path: string;
  };
  withChannel1?: {
    uniform: string;
    path: string;
  };
}) {
  const {
    pathToShader,
    uniforms,
    animate,
    onInit,
    withMouse,
    withChannel0,
    withChannel1,
  } = props;
  const app = useContext(PixiAppContext);
  const [shader, setShader] = useState<Filter>();
  const [loaded, setLoaded] = useState(false);

  const uniformsRef = useRef(uniforms);
  const animateRef = useRef(animate);
  const onInitRef = useRef(onInit);

  useLayoutEffect(() => {
    uniformsRef.current = uniforms;
    animateRef.current = animate;
    onInitRef.current = onInit;
  });

  const resources = useContext(PixiLoaderResourcesContext);

  const { id }: InternalPixiResourceMetaData = {
    id: `shader:${pathToShader}`,
    path: pathToShader,
  };

  const frag = resources?.[id]?.data;

  const { id: channel0Id }: InternalPixiResourceMetaData = {
    id: `image:${withChannel0?.path}`,
    path: withChannel0?.path ?? "",
  };

  const channel0 = resources?.[channel0Id]?.texture;

  const { id: channel1Id }: InternalPixiResourceMetaData = {
    id: `image:${withChannel1?.path}`,
    path: withChannel1?.path ?? "",
  };

  const channel1 = resources?.[channel1Id]?.texture;

  useEffect(
    function createFilterShader() {
      if (!frag || !app || !resources) return;

      onInitRef.current?.(app);

      const isWebGL2 = /^#version 300 es/.test(frag);

      [channel0, channel1].forEach((channel) => {
        if (channel) {
          channel.baseTexture.mipmap = MIPMAP_MODES.ON;
          channel.baseTexture.wrapMode = WRAP_MODES.REPEAT;
        }
      });

      const filter = new Filter(
        isWebGL2 ? defaultVertexFragFor2_0 : undefined,
        frag,
        {
          ...uniformsRef.current,
          ...(withMouse?.uniform && { [withMouse.uniform]: [0, 0, 0, 0] }),
          ...(channel0 &&
            withChannel0?.uniform && { [withChannel0?.uniform]: channel0 }),
          ...(channel1 &&
            withChannel1?.uniform && { [withChannel1.uniform]: channel1 }),
        }
      );

      setShader(filter);
    },
    [
      app,
      channel0,
      channel1,
      frag,
      resources,
      withChannel0?.uniform,
      withChannel1?.uniform,
      withMouse?.uniform,
    ]
  );

  useEffect(
    function animate() {
      if (!app || !shader) return;

      const background = new Graphics();
      background.beginFill(0xffffff);
      background.drawRect(0, 0, app.screen.width, app.screen.height);
      background.endFill();

      background.filters = [shader];

      app.stage.addChild(background);

      let animate: undefined | ((delta: number) => void);

      if (animateRef.current) {
        animate = (delta: number) => {
          if (shader) animateRef.current?.(shader.uniforms as Uniforms, delta);
        };
        app.ticker.add(animate);
      }

      setLoaded(true);

      return () => {
        if (animate) app.ticker.remove(animate);
        app.stage.removeChild(background);
      };
    },
    [app, shader]
  );

  useEffect(
    function mouse() {
      if (!app || !shader || !loaded || !withMouse?.uniform) return;

      const move = (e: MouseEvent) => {
        if (e.buttons !== 0) return;

        // app.ticker.addOnce(() => {
        //   shader.uniforms[withMouseUniform] = [
        //     // between [0, app.screen.width]
        //     (e.offsetX / app.view.width) * app.screen.width,
        //     // ditto
        //     (e.offsetY / app.view.height) * app.screen.height,
        //     0,
        //     0,
        //   ];
        // });
      };

      app.view.addEventListener("mousemove", move);

      const click = (e: MouseEvent) => {
        // between [0, app.screen.width]
        const x = (e.offsetX / app.view.width) * app.screen.width;
        // ditto but for y bottom is 0
        const y =
          app.screen.height - (e.offsetY / app.view.height) * app.screen.height;

        app.ticker.addOnce(() => {
          shader.uniforms[withMouse?.uniform] = [
            x,
            y,
            // TODO: https://shadertoyunofficial.wordpress.com/2016/07/20/special-shadertoy-features/
            -x,
            -y,
          ];
        });
      };

      app.view.addEventListener("click", click);

      return () => {
        app.view.removeEventListener("mousemove", move);
        app.view.removeEventListener("click", click);
      };
    },
    [app, loaded, shader, withMouse?.uniform]
  );

  return loaded ? { shader } : undefined;
}
