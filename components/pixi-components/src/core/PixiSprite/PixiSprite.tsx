import { Sprite } from "pixi.js";
import { useContext, useEffect, useState } from "react";
import { PixiParentProps, useAddToParent } from "../PixiApp/useAddToParent";
import { InternalPixiResourceMetaData } from "../PixiApp/PixiApp.types";
import { PixiLoaderResourcesContext } from "../PixiApp/PixiContexts";

export interface SpriteData {
  resource: InternalPixiResourceMetaData;
  sprite: Sprite;
  computedHeight: number;
  computedWidth: number;
}

function fitSpriteInContainer(props: {
  height: number;
  width: number;
  containerHeight: number;
  containerWidth: number;
}) {
  const originalAspectRatio = props.height / props.width;

  const computedWidth = props.containerWidth;
  const computedHeight = props.containerWidth * originalAspectRatio;

  if (computedHeight <= props.containerHeight) {
    return { computedWidth, computedHeight };
  }

  return {
    computedHeight: props.containerHeight,
    computedWidth: props.containerHeight / originalAspectRatio,
  };
}

type PixiSpriteOpts = {
  path: string | undefined;
  width: number;
  height: number;
} & PixiParentProps;

/**
 * Loads image and resizes it (to scale) to fit
 * inside the container
 *
 * @param props
 */
export function useLoadPixiSprite(
  props: PixiSpriteOpts
): SpriteData | undefined {
  const resources = useContext(PixiLoaderResourcesContext);
  const [data, setData] = useState<SpriteData>();

  useEffect(
    function loadSprite() {
      if (!props.path || !resources) {
        return;
      }

      const { id, path }: InternalPixiResourceMetaData = {
        id: `image:${props.path}`,
        path: props.path,
      };

      if (!resources[id]) return;

      const resource = resources[id]?.texture;
      const sprite = new Sprite(resource);

      const { computedWidth, computedHeight } = fitSpriteInContainer({
        height: sprite.height,
        width: sprite.width,
        containerWidth: props.width,
        containerHeight: props.height,
      });

      sprite.width = computedWidth;
      sprite.height = computedHeight;
      sprite.anchor.set(0.5);
      sprite.x = props.width / 2;
      sprite.y = props.height / 2;

      setData({
        resource: { id, path },
        sprite,
        computedHeight,
        computedWidth,
      });
    },
    [props.height, props.path, props.width, resources]
  );

  const added = useAddToParent({
    ...props,
    child: data?.sprite,
  });

  return added ? data : undefined;
}
