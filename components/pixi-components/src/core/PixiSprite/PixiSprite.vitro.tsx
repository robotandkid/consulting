import React, { useContext } from "react";
import { useAddToParent } from "../PixiApp/useAddToParent";
import { PixiAppWrapper } from "../PixiApp/PixiApp";
import { PixiAppContext } from "../PixiApp/PixiContexts";
import { useAddPixiFilter2 } from "../PixiFilter/PixiFilter";
import { defaultOldFilmFilter } from "../PixiFilter/PixiFilter.defaults.oldfilm";
import { useLoadPixiSprite } from "./PixiSprite";

const width = 150;
const height = 150;

export function PixiSpriteBasic() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "image", path: "/images/test-portrait.jpeg" }]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/test-portrait.jpeg",
          height,
          width,
          parentType: "none",
        });

        useAddToParent({
          child: sprite?.sprite,
          parentType: "app",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function PixiSpriteAddToParent() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "image", path: "/images/test-portrait.jpeg" }]}
    >
      {function useMain() {
        const app = useContext(PixiAppContext);

        useLoadPixiSprite({
          path: "/images/test-portrait.jpeg",
          height,
          width,
          parentType: "container",
          parent: app,
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function PixiSpriteAddToApp() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "image", path: "/images/test-portrait.jpeg" }]}
    >
      {function useMain() {
        useLoadPixiSprite({
          path: "/images/test-portrait.jpeg",
          height,
          width,
          parentType: "app",
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}

export function PixiSpriteWithFilter() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      resources={[{ type: "image", path: "/images/test-portrait.jpeg" }]}
    >
      {function useMain() {
        const sprite = useLoadPixiSprite({
          path: "/images/test-portrait.jpeg",
          height,
          width,
          parentType: "app",
        });

        useAddPixiFilter2({
          comp: sprite?.sprite,
          filters: defaultOldFilmFilter(),
        });

        return null;
      }}
    </PixiAppWrapper>
  );
}
