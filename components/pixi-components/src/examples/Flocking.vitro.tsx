import { ColorOverlayFilter } from "pixi-filters";
import { Point } from "pixi.js";
import React, { useContext, useEffect } from "react";
import { PixiApp } from "../core/PixiApp/PixiApp";
import { PixiAppContext } from "../core/PixiApp/PixiContexts";
import { useAddToParent } from "../core/PixiApp/useAddToParent";
import { useAddPixiFilter2 } from "../core/PixiFilter/PixiFilter";
import { useLoadPixiSprite } from "../core/PixiSprite/PixiSprite";
import { useGlowingEyes } from "../effects/GlowingEyes/glowingEyes";
import { useAddFlock } from "../effects/Flocking/useAddFlock";
import { rgbToHex } from "../utils/color";

function CustomSprite() {
  const sprite = useLoadPixiSprite({
    path: "/images/summerportrait.jpeg",
    height: 300,
    width: 300,
    parentType: "app",
  });

  const mask = useLoadPixiSprite({
    path: "/images/summerportrait_mask.png",
    height: 300,
    width: 300,
    parentType: "app",
  });

  const app = useContext(PixiAppContext);

  useEffect(
    function applyMask() {
      if (!app || !sprite || !mask) return;

      app.stage.mask = mask.sprite;

      return () => {
        app.stage.mask = null;
      };
    },
    [app, sprite, mask]
  );

  useAddPixiFilter2({
    comp: sprite?.sprite,
    filters: {
      filter: new ColorOverlayFilter(rgbToHex("#000000")),
    },
  });

  const glowingEyes = useGlowingEyes({
    leftEye: new Point(146, 80),
    rightEye: new Point(173, 80),
    radius: 1,
  });

  const added = useAddToParent({
    child: [sprite?.sprite, mask?.sprite, glowingEyes],
    parentType: "app",
  });

  useAddFlock({
    start: added,
  });

  return null;
}

export function MaskWithOverlayAndFlockingFixture() {
  return (
    <PixiApp
      width={300}
      height={300}
      resources={[
        { type: "image", path: "/images/summerportrait.jpeg" },
        { type: "image", path: "/images/summerportrait_mask.png" },
      ]}
    >
      <CustomSprite />
    </PixiApp>
  );
}
