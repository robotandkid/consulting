import { useContext, useMemo } from "react";
import { DialogEngineResourcesContext } from "../DialogEngine/DialogEngineProvider";
import { PixiResourceMetaData } from "../ui/pixi.js/pixi";
import { textScrollId } from "../ui/pixi.js/TypeWriterText/TypewriterText";

export function useResourcesFromTextScrollConfig(
  textScrollSoundPath: string | undefined
): PixiResourceMetaData[] {
  const resourcesFromDialog = useContext(DialogEngineResourcesContext);
  const soundPath = textScrollSoundPath;

  return useMemo(() => {
    return soundPath
      ? [
          ...resourcesFromDialog,
          {
            type: "sound",
            path: soundPath,
            id: textScrollId,
          },
        ]
      : resourcesFromDialog;
  }, [resourcesFromDialog, soundPath]);
}
