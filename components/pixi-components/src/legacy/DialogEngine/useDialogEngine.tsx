import { useEffect, useMemo, useRef, useState } from "react";
import { PixiResourceMetaData } from "../ui/pixi.js/pixi";
import {
  commandToInternalCommand,
  DialogNode,
  InternalCommand,
  InternalDialogNode,
} from "./dialog.types";

export interface DialogEngineProps {
  nodes: DialogNode[];
}

export type DialogEngineOutput = InternalCommand;

export type SubscribeCommandCallback = (cmd: InternalCommand) => void;
export type UnsubscribeCommand = () => void;

export interface DialogEngineInterface {
  current: { nodeTitle: string; cmdNodeIndex: number; cmd: InternalCommand };
  resources: PixiResourceMetaData[];
  callbacks: {
    advance(): void;
    restartNodeOrPreviousNode(): void;
    restart(): void;
  };
}

type CommandData = {
  /**
   * Every node has a `title` and a list of `commands`
   */
  node: InternalDialogNode;
  /**
   * The command's index in the node's list of commands
   */
  cmdNodeIndex: number;
  /**
   * The command. Note that this is for convenience since `cmd` =
   * `node.commands[cmdNodeIndex]`
   */
  cmd: InternalCommand;
  /**
   * The node's title. Note that this is for convenience since `nodeTitle` =
   * `node.title`
   */
  nodeTitle: string;
};

function nextCommand(props: CommandData): CommandData {
  const nextCommandIndex = Math.min(
    props.cmdNodeIndex + 1,
    props.node.commands.length - 1
  );

  return {
    node: props.node,
    cmdNodeIndex: nextCommandIndex,
    cmd: props.node.commands[nextCommandIndex],
    nodeTitle: props.node.title,
  };
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function prevCommand(props: CommandData): CommandData {
  const prevCommandIndex = Math.max(props.cmdNodeIndex - 1, 0);

  return {
    node: props.node,
    cmdNodeIndex: prevCommandIndex,
    cmd: props.node.commands[prevCommandIndex],
    nodeTitle: props.node.title,
  };
}

type NodesByTitle = { [title: string]: InternalDialogNode };

/**
 * - Jumps to the node given by `title`
 * - pushes the title node to the `visitedNodes` array.
 */
function jump(props: {
  nodesByTitle: NodesByTitle;
  visitedNodes: InternalDialogNode[];
  title: string;
}): { command: CommandData; visitedNodes: InternalDialogNode[] } {
  if (!props.nodesByTitle[props.title]) {
    throw new Error(`Node with title ${props.title} does not exist`);
  }

  const node = props.nodesByTitle[props.title];

  return {
    visitedNodes: [...props.visitedNodes, node],
    command: {
      node,
      cmdNodeIndex: 0,
      cmd: node.commands[0],
      nodeTitle: node.title,
    },
  };
}

function handleJumpCommand(
  command: CommandData,
  nodesByTitle: NodesByTitle,
  visitedNodes: InternalDialogNode[]
): { command: CommandData; visitedNodes: InternalDialogNode[] } | undefined {
  if (command.cmd.type === "jump") {
    return jump({
      nodesByTitle,
      title: command.cmd.jump,
      visitedNodes,
    });
  }
}

/**
 * - for this to make sense, see the definition of CommandData.
 * - finds the command that satisfies the predicate in the node's list of
 *   commands, starting backwards from the given command.
 * - if nothing is found, it will recursively search the list of visitedNodes,
 *   starting from the end.
 */
function reverseFindByPredicate(
  { cmdNodeIndex, node }: CommandData,
  visitedNodes: InternalDialogNode[],
  predicate: (cmd: InternalCommand) => boolean
): { command: CommandData; visitedNodes: InternalDialogNode[] } | undefined {
  for (let i = cmdNodeIndex - 1; i >= 0; i--) {
    if (predicate(node.commands[i])) {
      return {
        command: {
          node,
          cmdNodeIndex: i,
          cmd: node.commands[i],
          nodeTitle: node.title,
        },
        visitedNodes,
      };
    }
  }

  if (visitedNodes.length >= 2) {
    const visitedNode = visitedNodes[visitedNodes.length - 2];

    return reverseFindByPredicate(
      {
        node: visitedNode,
        cmdNodeIndex: visitedNode.commands.length - 1,
        cmd: visitedNode.commands[visitedNode.commands.length - 1],
        nodeTitle: visitedNode.title,
      },
      visitedNodes.slice(0, visitedNodes.length - 1),
      predicate
    );
  }
}

function reverseFind(
  command: CommandData,
  visitedNodes: InternalDialogNode[],
  type: InternalCommand["type"]
): { command: CommandData; visitedNodes: InternalDialogNode[] } | undefined {
  switch (type) {
    case "say-line":
    case "draw-bg":
      return reverseFindByPredicate(command, visitedNodes, (cmd) => {
        return cmd.type === type;
      });
    default:
      return reverseFindByPredicate(command, visitedNodes, () => true);
  }
}

/**
 * The idea here is that we can swap this out for something like yarn dialog
 */
export function useDialogEngine(
  props: DialogEngineProps
): DialogEngineInterface {
  const nodes: InternalDialogNode[] = useMemo(() => {
    const _nodes = props.nodes.map((node) => ({
      ...node,
      commands: node.commands.map(commandToInternalCommand),
    }));

    _nodes[_nodes.length - 1].commands.push({ type: "end" });

    return _nodes;
  }, [props.nodes]);

  const resources = useMemo(() => {
    return nodes.reduce((_resources, cur) => {
      cur.commands.forEach((cmd) => {
        if (cmd.type === "draw-bg") {
          _resources.push({
            type: "image",
            path: cmd["draw-bg"],
          });
        }
      });
      return _resources;
    }, [] as PixiResourceMetaData[]);
  }, [nodes]);

  const nodesByTitle = useMemo(() => {
    return nodes.reduce(
      (total, cur) => ({
        ...total,
        [cur.title.trim().toLowerCase()]: cur,
      }),
      {} as { [title: string]: InternalDialogNode }
    );
  }, [nodes]);

  const jumpData = jump({ nodesByTitle, title: "start", visitedNodes: [] });
  const visitedNodes = useRef<InternalDialogNode[]>(jumpData.visitedNodes);
  const [command, setCommand] = useState<CommandData>(jumpData.command);

  useEffect(
    function autojump() {
      const result = handleJumpCommand(
        command,
        nodesByTitle,
        visitedNodes.current
      );

      if (result) {
        visitedNodes.current = result.visitedNodes;
        setCommand(result.command);
      }
    },
    [command, nodesByTitle]
  );

  const callbacks = useMemo(
    () => ({
      advance() {
        setCommand((cmd) => nextCommand(cmd));
      },
      restartNodeOrPreviousNode() {
        setCommand((cmd) => {
          // Search backwards for the first say-command
          const say = reverseFind(cmd, visitedNodes.current, "say-line");
          if (say) {
            // jumping will restart the node
            // where the say-command was found
            // and if none is found then
            // you're at the very first node
            const jumpData = jump({
              nodesByTitle,
              title: say.command.node.title,
              visitedNodes: say.visitedNodes,
            });
            visitedNodes.current = jumpData.visitedNodes;
            return jumpData.command;
          }
          return cmd;
        });
      },
      restart() {
        const jumpData = jump({
          nodesByTitle,
          title: "start",
          visitedNodes: visitedNodes.current,
        });
        setCommand(jumpData.command);
        visitedNodes.current = jumpData.visitedNodes;
      },
    }),
    [nodesByTitle]
  );

  return {
    current: command,
    resources,
    callbacks,
  };
}
