import React from "react";
import { DialogNode } from "./dialog.types";
import { DialogEngineInterface, useDialogEngine } from "./useDialogEngine";
import { createContext } from "../../utils/context";

export const DialogEngineCommandContext =
  createContext<DialogEngineInterface["current"]>();

export const DialogEngineResourcesContext =
  createContext<DialogEngineInterface["resources"]>();

export const DialogEngineCallbackContext =
  createContext<DialogEngineInterface["callbacks"]>();

export function DialogEngineProvider(props: {
  nodes: DialogNode[];
  children?: React.ReactNode;
}) {
  const {
    current: currentCommand,
    resources,
    callbacks,
  } = useDialogEngine({
    nodes: props.nodes,
  });

  return (
    <DialogEngineResourcesContext.Provider value={resources}>
      <DialogEngineCallbackContext.Provider value={callbacks}>
        <DialogEngineCommandContext.Provider value={currentCommand}>
          {props.children}
        </DialogEngineCommandContext.Provider>
      </DialogEngineCallbackContext.Provider>
    </DialogEngineResourcesContext.Provider>
  );
}
