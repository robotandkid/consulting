import React, { useContext, useRef } from "react";
import { DialogNode } from "./dialog.types";
import {
  DialogEngineCallbackContext,
  DialogEngineCommandContext,
  DialogEngineProvider,
} from "./DialogEngineProvider";
import { useDialogEngine } from "./useDialogEngine";

const nodes: DialogNode[] = [
  {
    title: "start",
    commands: [
      {
        actor: "mac",
        say: "cheese",
      },
      {
        jump: "scene2",
      },
    ],
  },
  {
    title: "scene2",
    commands: [
      {
        actor: "cheese",
        say: "delicious",
      },
      {
        actor: "son",
        say: "huh",
      },
    ],
  },
];

function HookRenderCountTest() {
  const {
    current: currentCommand,
    callbacks: { restartNodeOrPreviousNode, advance },
  } = useDialogEngine({
    nodes,
  });

  const renderCount = useRef(0);
  renderCount.current++;

  return (
    <>
      <div>Render count: {renderCount.current}</div>
      <button onClick={advance}>Go to next command</button>
      <button onClick={restartNodeOrPreviousNode}>Go back</button>
      <div>Current command: {JSON.stringify(currentCommand)}</div>
    </>
  );
}

function NaiveProviderRenderCountTest() {
  const currentCommand = useContext(DialogEngineCommandContext);
  const { advance, restartNodeOrPreviousNode: previousNode } = useContext(
    DialogEngineCallbackContext
  );

  const renderCount = useRef(0);
  renderCount.current++;

  return (
    <>
      <div>Render count: {renderCount.current}</div>
      <button onClick={advance}>Go to next command</button>
      <button onClick={previousNode}>Go back</button>
      <div>Current command: {JSON.stringify(currentCommand)}</div>
    </>
  );
}

function NextButton() {
  const { advance } = useContext(DialogEngineCallbackContext);

  return <button onClick={advance}>Go to next command</button>;
}

function PrevButtion() {
  const { restartNodeOrPreviousNode: previousNode } = useContext(
    DialogEngineCallbackContext
  );

  return <button onClick={previousNode}>Go back</button>;
}

function CurrentCommand() {
  const currentCommand = useContext(DialogEngineCommandContext);

  return <div>Current command: {JSON.stringify(currentCommand)}</div>;
}

function ProviderRenderCountTest() {
  const renderCount = useRef(0);
  renderCount.current++;

  return (
    <>
      <div>Render count: {renderCount.current}</div>
      <NextButton />
      <PrevButtion />
      <CurrentCommand />
    </>
  );
}

export default {
  hookRenderCountTest: <HookRenderCountTest />,
  naiveProviderRenderCountTest: (
    <DialogEngineProvider nodes={nodes}>
      <NaiveProviderRenderCountTest />
    </DialogEngineProvider>
  ),
  providerRenderCountTest: (
    <DialogEngineProvider nodes={nodes}>
      <ProviderRenderCountTest />
    </DialogEngineProvider>
  ),
};
