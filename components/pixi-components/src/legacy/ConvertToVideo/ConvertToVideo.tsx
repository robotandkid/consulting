import React, { useContext, useEffect, useState } from "react";

import { DialogEngineCommandContext } from "../DialogEngine/DialogEngineProvider";
import { PixiCanvasRefContext } from "../ui/pixi.js/pixi";

function ConvertToVideoWorker() {
  const command = useContext(DialogEngineCommandContext);
  const canvas = useContext(PixiCanvasRefContext);
  const [recorder, setRecorder] = useState<MediaRecorder>();

  useEffect(
    function initialize() {
      if (canvas) {
        setRecorder(new MediaRecorder(canvas.captureStream(25)));
      }
    },
    [canvas]
  );

  useEffect(() => {
    recorder?.start();
  }, [recorder]);

  useEffect(
    function save() {
      recorder?.addEventListener("dataavailable", function (e) {
        const videoData = [e.data];
        const blob = new Blob(videoData, { type: "video/webm" });
        window.open(URL.createObjectURL(blob));
      });
    },
    [recorder]
  );

  useEffect(
    function stop() {
      if (command.cmd?.type === "end") {
        recorder?.stop();
      }
    },
    [command, recorder]
  );

  return null;
}

export function ConvertToVideo(props: { enabled: boolean }) {
  const canvas = useContext(PixiCanvasRefContext);
  const recorderAvailable =
    canvas?.captureStream &&
    typeof MediaRecorder !== "undefined" &&
    MediaRecorder.isTypeSupported("video/webm");

  return <>{props.enabled && recorderAvailable && <ConvertToVideoWorker />}</>;
}
