import { Loader, LoaderResource } from "pixi.js";
import { useEffect, useRef } from "react";
import { createComponent } from "../../../utils/createComponent";
import { internalPixiResourceMetaData, PixiResourceMetaData } from "../pixi";

export type Resources = Partial<Record<string, LoaderResource>>;

export interface LoadResourcesOpts {
  /**
   * Set to `false` when there are no resources.
   */
  resourcesToLoad: false | PixiResourceMetaData[] | undefined;
  onResourcesChange: (resources: Resources) => void;
}

export function useLoadResources(props: LoadResourcesOpts) {
  const { resourcesToLoad, onResourcesChange } = props;

  const loader = useRef(new Loader());

  useEffect(
    // Need to handle
    // - [x] initial load
    // - [x] resource change
    // - clearing out resources
    function loadResources() {
      (resourcesToLoad || []).forEach((resource) => {
        const { id, path } = internalPixiResourceMetaData(resource);

        if (!loader.current.resources[id]) {
          if (resource.type === "image") {
            loader.current.add({
              name: id,
              url: path,
              loadType: 2,
            });
          } else {
            loader.current.add(id, path);
          }
        }
      });

      loader.current.load((_, resources) => {
        onResourcesChange({ ...resources });
      });
    },
    [onResourcesChange, resourcesToLoad]
  );
}

export const LoadResources = createComponent(useLoadResources);
