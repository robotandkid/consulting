import { InteractionEvent, Point } from "pixi.js";
import { useEffect } from "react";
import { AllPartial } from "../../utils/typescript";
import { PixiElementProps, isApplication } from "./pixi";

export const pixiTapEvent = "pixi:tap";
export const pixiDoubleTapEvent = "pixi:doubletap";
export const pixiDragLeftEvent = "pixi:dragleft";
export const pixiDragRightEvent = "pixi:dragright";

const dragThresholdInPx = 35;
const doubleTapThresholdTimeInMs = 200;

interface TimeProps {
  startTime: number;
  endTime: number;
}

interface XYcoords {
  start: Point;
  end: Point;
}

/**
 * A touch event is a tap if the start/end happens
 * in a hypercube slice constrained by
 * [start, dragThresholdInPx, doubleTapThresholdTimeInMs].
 *
 * A touch event is a double tap if the same touch
 * happens in the same hypercube.
 */
function isInTapHyperCube(
  props:
    | (TimeProps & XYcoords)
    | (AllPartial<TimeProps> & XYcoords)
    | (TimeProps & AllPartial<XYcoords>)
) {
  if (!props.start) {
    return props.endTime - props.startTime <= doubleTapThresholdTimeInMs;
  } else if (!props.startTime) {
    return Math.abs(props.end.x - props.start.x) <= dragThresholdInPx;
  }

  return (
    props.endTime - props.startTime <= doubleTapThresholdTimeInMs &&
    Math.abs(props.end.x - props.start.x) <= dragThresholdInPx
  );
}

export function PixiEventing(props: PixiElementProps) {
  const { parent } = props;

  useEffect(
    function enableAppEvents() {
      if (isApplication(parent)) {
        parent.stage.buttonMode = true;
        parent.stage.interactive = true;
      }
    },
    [parent]
  );

  useEffect(
    function handleTouchEvents() {
      type EventData =
        | {
            type: "init";
          }
        | {
            type: "tap-in-progress";
            startTime: number;
            startPoint: Point;
            startPointData: InteractionEvent["data"];
          }
        | {
            type: "tap-end";
            startTime: number;
            startPoint: Point;
            startPointData: InteractionEvent["data"];
            dispatchTapEvent: number;
            endTime: number;
          };

      let eventData: EventData = {
        type: "init",
      };

      function startPossibleTapOrDoubleTap(evt: InteractionEvent) {
        if (isApplication(parent)) {
          switch (eventData.type) {
            case "init": {
              eventData = {
                type: "tap-in-progress",
                startTime: Date.now(),
                startPointData: evt.data,
                startPoint: evt.data.getLocalPosition(parent.stage),
              };
              break;
            }
            case "tap-end": {
              const endTime = Date.now();

              if (isInTapHyperCube({ endTime, startTime: eventData.endTime })) {
                // it was a double tap
                clearTimeout(eventData.dispatchTapEvent);
                eventData = { type: "init" };
                parent.view.dispatchEvent(new Event(pixiDoubleTapEvent));
              }
              break;
            }
            default:
              // multi-touch
              break;
          }
        }
      }

      function endPossibleTapOrDoubleTap() {
        if (isApplication(parent)) {
          switch (eventData.type) {
            case "tap-in-progress": {
              const end = eventData.startPointData.getLocalPosition(
                parent.stage
              );
              const endTime = Date.now();

              if (
                isInTapHyperCube({
                  end: end as any,
                  start: eventData.startPoint,
                  endTime,
                  startTime: eventData.startTime,
                })
              ) {
                eventData = {
                  ...eventData,
                  type: "tap-end",
                  endTime,
                  dispatchTapEvent: setTimeout(() => {
                    eventData = { type: "init" };
                    parent.view.dispatchEvent(new Event(pixiTapEvent));
                  }, doubleTapThresholdTimeInMs) as any,
                };
              } else {
                eventData = {
                  type: "init",
                };
              }
              break;
            }
            default:
              break;
          }
        }
      }

      if (isApplication(parent)) {
        parent.stage
          .on("pointerdown", startPossibleTapOrDoubleTap)
          .on("pointerupoutside", endPossibleTapOrDoubleTap)
          .on("pointerup", endPossibleTapOrDoubleTap);
      }

      return () => {
        if (isApplication(parent)) {
          parent.stage
            .off("pointerdown", startPossibleTapOrDoubleTap)
            .on("pointerupoutside", endPossibleTapOrDoubleTap)
            .on("pointerup", endPossibleTapOrDoubleTap);
        }
      };
    },
    [parent]
  );

  useEffect(
    function handleDragEvents() {
      type EventData =
        | {
            type: "init";
          }
        | {
            type: "drag-in-progress";
            startTime: number;
            startPoint: Point;
            startPointData: InteractionEvent["data"];
          };

      let eventData: EventData = {
        type: "init",
      };

      function startDrag(evt: InteractionEvent) {
        if (isApplication(parent)) {
          switch (eventData.type) {
            case "init": {
              eventData = {
                type: "drag-in-progress",
                startTime: Date.now(),
                startPointData: evt.data,
                startPoint: evt.data.getLocalPosition(parent.stage),
              };
              break;
            }
            default:
              // multi-touch
              break;
          }
        }
      }

      function endDrag() {
        if (isApplication(parent)) {
          switch (eventData.type) {
            case "drag-in-progress": {
              const end = eventData.startPointData.getLocalPosition(
                parent.stage
              );

              if (
                !isInTapHyperCube({
                  start: eventData.startPoint,
                  end: end as any,
                })
              ) {
                if (end.x > eventData.startPoint.x) {
                  parent.view.dispatchEvent(new Event(pixiDragRightEvent));
                } else {
                  parent.view.dispatchEvent(new Event(pixiDragLeftEvent));
                }
              }
              eventData = {
                type: "init",
              };
              break;
            }
            default:
              break;
          }
        }
      }

      if (isApplication(parent)) {
        parent.stage
          .on("pointerdown", startDrag)
          .on("pointerupoutside", endDrag)
          .on("pointerup", endDrag);
      }

      return () => {
        if (isApplication(parent)) {
          parent.stage
            .off("pointerdown", startDrag)
            .on("pointerupoutside", endDrag)
            .on("pointerup", endDrag);
        }
      };
    },
    [parent]
  );

  return null;
}
