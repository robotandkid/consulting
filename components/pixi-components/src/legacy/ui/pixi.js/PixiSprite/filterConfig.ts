import {
  AdjustmentFilter,
  ColorReplaceFilter,
  ConfigurableFilter,
  DotFilter,
  Filter,
  PixelateFilter,
  SimpleFilter,
  configurableFilterType,
  filterWithNoDefaultsType,
  simpleFilterType,
} from "./Filter.types";

type BaseFilter = Filter;

export interface FilterConfigNumberType {
  min?: number;
  max?: number;
  step?: number;
  default: number;
}

export type FilterConfig<Filter extends BaseFilter> = Omit<
  {
    [key in keyof Filter]: FilterConfigNumberType;
  },
  "filter" | "target"
> & {
  filter: Filter["filter"];
  target?: Filter["target"];
};

const simpleFilterConfig = simpleFilterType.map(
  (filter) =>
    ({
      filter,
    } as FilterConfig<SimpleFilter>)
);

const filterConfigWithDefaults = configurableFilterType.map((filter) => {
  switch (filter) {
    case "dot":
      return {
        filter: "dot",
        scale: {
          min: 0,
          max: 1,
          step: 0.01,
          default: 1,
        },
        radius: {
          min: 0,
          default: 1,
          step: 0.01,
        },
      } as FilterConfig<DotFilter>;
    case "pixelate":
      return {
        filter: "pixelate",
        size: {
          min: 0,
          step: 0.1,
          default: 3,
        },
      } as FilterConfig<PixelateFilter>;
    default:
      return {
        filter,
      } as FilterConfig<ConfigurableFilter>;
  }
});

const filterConfigWithNoDefaults = filterWithNoDefaultsType.map((filter) => {
  switch (filter) {
    case "colorreplace":
      return {
        filter: "colorreplace",
        originalColor: {
          default: 0xffffff,
        },
        newColor: {
          default: 0xffffff,
        },
        epsilon: {
          min: 0,
          max: 1,
          step: 0.01,
          default: 1,
        },
      } as FilterConfig<ColorReplaceFilter>;
    case "adjustment":
      return {
        filter: "adjustment",
        gamma: {
          min: 0,
          step: 0.1,
          default: 1,
        },
        saturation: {
          min: 0,
          step: 0.1,
          default: 1,
        },
        contrast: {
          min: 0,
          step: 0.1,
          default: 1,
        },
        brightness: {
          min: 0,
          step: 0.1,
          default: 1,
        },
        red: {
          min: 0,
          step: 0.25,
          default: 1,
        },
        green: {
          min: 0,
          step: 0.25,
          default: 1,
        },
        blue: {
          min: 0,
          step: 0.25,
          default: 1,
        },
        alpha: {
          min: 0,
          max: 1,
          step: 0.01,
          default: 1,
        },
      } as FilterConfig<AdjustmentFilter>;
    default: {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      return filter as never;
    }
  }
});

export const filterConfigs = [
  ...simpleFilterConfig,
  ...filterConfigWithDefaults,
  ...filterConfigWithNoDefaults,
];
