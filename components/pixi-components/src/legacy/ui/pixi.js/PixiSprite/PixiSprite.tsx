import {
  AdjustmentFilter,
  ColorReplaceFilter,
  CRTFilter,
  DotFilter,
  DropShadowFilter,
  GlitchFilter,
  GlowFilter,
  GodrayFilter,
  KawaseBlurFilter,
  MotionBlurFilter,
  OldFilmFilter,
  OutlineFilter,
  PixelateFilter,
  ReflectionFilter,
  TiltShiftFilter,
  ZoomBlurFilter,
} from "pixi-filters";
import { Filter, Point, Sprite } from "pixi.js";
import React, { useContext, useEffect, useState } from "react";
import { CmdFilter } from "../../../DialogEngine/dialog.types";
import { AddToParentOnMount } from "../AddToParentOnMount/AddToParentOnMount";
import {
  internalPixiResourceMetaData,
  InternalPixiResourceMetaData,
  PixiAppTickerContext,
  PixiElementProps,
  PixiLoaderResourcesContext,
} from "../pixi";

export interface SpriteData {
  resource: InternalPixiResourceMetaData;
  sprite: Sprite;
  computedHeight: number;
  computedWidth: number;
}

function fitSpriteInContainer(props: {
  height: number;
  width: number;
  containerHeight: number;
  containerWidth: number;
}) {
  const originalAspectRatio = props.height / props.width;

  const computedWidth = props.containerWidth;
  const computedHeight = props.containerWidth * originalAspectRatio;

  if (computedHeight <= props.containerHeight) {
    return { computedWidth, computedHeight };
  }

  return {
    computedHeight: props.containerHeight,
    computedWidth: props.containerHeight / originalAspectRatio,
  };
}

function createFilter(cmd: CmdFilter) {
  switch (cmd.filter) {
    case "crt": {
      const filter = new CRTFilter({
        lineWidth: 3,
        lineContrast: 0.3,
        noise: 0.2,
        time: 0.5,
        vignetting: 0,
      });

      return {
        filter,
        animation() {
          filter.seed = Math.random();
          filter.time += 0.5;
        },
      };
    }
    case "dot":
      return {
        filter: new DotFilter(cmd.scale, cmd.radius),
        animation: undefined,
      };
    case "dropshadow":
      return { filter: new DropShadowFilter({}), animation: undefined };
    case "glitch": {
      const filter = new GlitchFilter({
        red: new Point(2, 2),
        blue: new Point(10, -4),
        green: new Point(-10, 4),
        slices: 10,
        offset: 100,
        direction: 0,
        fillMode: 2,
        average: false,
        seed: Math.random(),
        minSize: 8,
        sampleSize: 1,
      });

      return {
        filter,
        animation() {
          filter.seed = Math.random();
        },
      };
    }
    case "glow":
      return {
        filter: new GlowFilter({
          distance: 15,
          outerStrength: 2,
          innerStrength: 0,
          color: 0xffffff,
          quality: 0.2,
          knockout: false,
        }),
        animation: undefined,
      };
    case "godray": {
      const filter = new GodrayFilter({
        alpha: 1,
        angle: 30,
        gain: 0.6,
        lacunarity: 2.75,
        center: new Point(100, -100),
        parallel: true,
        time: 0,
      });

      return {
        filter,
        animation(elapsedTimeMs: number) {
          filter.time += elapsedTimeMs / 1000;
        },
      };
    }
    case "kawaseblur":
      return {
        filter: new KawaseBlurFilter([4, 3]),
        animation: undefined,
      };
    case "motionblur":
      return {
        filter: new MotionBlurFilter([40, 40], 15),
        animation: undefined,
      };
    case "oldfilm": {
      const filter = new OldFilmFilter();
      return {
        filter,
        animation() {
          filter.seed = Math.random();
        },
      };
    }
    case "outline": {
      const filter = new OutlineFilter(4, 0x0, 0.25);

      return { filter, animation: undefined };
    }
    case "pixelate":
      return {
        filter: new PixelateFilter(cmd.size || 3),
        animation: undefined,
      };
    case "reflection": {
      const filter = new ReflectionFilter();

      return {
        filter,
        animation() {
          filter.time += 0.1;
        },
      };
    }
    case "tiltshift":
      return { filter: new TiltShiftFilter(), animation: undefined };
    case "zoomblur":
      return {
        filter: new ZoomBlurFilter({
          strength: 0.1,
          innerRadius: 80,
        }),
        animation: undefined,
      };
    case "colorreplace":
      return {
        filter: new ColorReplaceFilter(
          cmd.originalColor,
          cmd.newColor,
          cmd.epsilon
        ),
        animation: undefined,
      };
    case "adjustment":
      return {
        filter: new AdjustmentFilter({
          gamma: cmd.gamma,
          saturation: cmd.saturation,
          contrast: cmd.contrast,
          brightness: cmd.brightness,
          red: cmd.red,
          green: cmd.green,
          blue: cmd.blue,
          alpha: cmd.alpha,
        }),
        animation: undefined,
      };

    default: {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const x: never = cmd;
    }
  }
}

interface PixiSpriteOpts {
  path: string | undefined;
  filter?: CmdFilter | Array<CmdFilter>;
  containerWidth: number;
  containerHeight: number;
}

function filterClassnames(filters: Filter[]) {
  return filters.reduce(
    (total, filter) =>
      Object.assign(total, { [filter.constructor.name]: true }),
    {} as { [key: string]: true }
  );
}

/**
 * Loads image and resizes it (to scale) to fit
 * inside the container
 *
 * @param props
 */
export function useLoadImage(props: PixiSpriteOpts): SpriteData | undefined {
  const appTicker = useContext(PixiAppTickerContext);
  const resources = useContext(PixiLoaderResourcesContext);
  const [data, setData] = useState<SpriteData>();

  useEffect(
    function loadSprite() {
      if (!props.path || !resources) {
        return;
      }

      const { id, path } = internalPixiResourceMetaData({
        type: "image",
        path: props.path,
      });

      const resource = (resources?.[id] as any)?.texture;
      const sprite = new Sprite(resource);

      const { computedWidth, computedHeight } = fitSpriteInContainer({
        height: sprite.height,
        width: sprite.width,
        containerWidth: props.containerWidth,
        containerHeight: props.containerHeight,
      });

      sprite.width = computedWidth;
      sprite.height = computedHeight;
      sprite.anchor.set(0.5);
      sprite.x = props.containerWidth / 2;
      sprite.y = props.containerHeight / 2;

      setData({
        resource: { id, path },
        sprite,
        computedHeight,
        computedWidth,
      });
    },
    [resources, props.containerHeight, props.containerWidth, props.path]
  );

  useEffect(
    function addFilter() {
      if (!data) {
        return;
      }

      let dedupedFiltersData: Array<
        NonNullable<ReturnType<typeof createFilter>>
      > = [];

      if (props.filter) {
        const newFilterNames = Array.isArray(props.filter)
          ? props.filter
          : [props.filter];

        const newFiltersData = newFilterNames.map(
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          (filter) => createFilter(filter)!
        );

        const _filterClassnames = filterClassnames(data.sprite.filters || []);

        dedupedFiltersData = newFiltersData.filter(
          ({ filter }) => !_filterClassnames[filter.constructor.name]
        );

        data.sprite.filters = [
          ...(data.sprite.filters || []),
          // TODO for some reason none of the filters are PixiFilters /thinking:face
          ...(dedupedFiltersData.map(({ filter }) => filter) as any),
        ];

        dedupedFiltersData.forEach(({ animation }) => {
          if (animation) {
            appTicker?.add(() => animation(appTicker.elapsedMS));
          }
        });
      }

      return () => {
        data.sprite.filters = null as any;

        dedupedFiltersData.forEach(({ animation }) => {
          if (animation) {
            appTicker?.remove(animation);
          }
        });
      };
    },
    [appTicker, data, props.filter]
  );

  return data;
}

export function PixiSprite(props: PixiSpriteOpts & PixiElementProps) {
  const bgData = useLoadImage(props);
  const sprite = bgData?.sprite;

  return <AddToParentOnMount parent={props.parent} child={sprite} />;
}
