import React from "react";
import { PixiParent, PixiNode } from "../pixi";

export function InjectPixiParent(props: {
  parent?: PixiParent;
  children?: PixiNode;
}) {
  return (
    <>
      {React.Children.map(props.children, (child) => {
        if (React.isValidElement<any>(child)) {
          return React.cloneElement(child, { parent: props.parent });
        }

        return child;
      })}
    </>
  );
}
