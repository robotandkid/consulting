import React from "react";
import styled from "styled-components";

const Json = styled.pre`
  background: #e6dddb;
  padding: 1rem;
`;

export function PrettyJson(props: { json: Record<string, unknown> }) {
  return <Json>{JSON.stringify(props.json, null, "  ")}</Json>;
}
