export type AllPartial<T> = {
  [key in keyof T]?: undefined;
};
