import { useCallback, useContext, useEffect } from "react";
import { PixiCanvasRefContext } from "../ui/pixi.js/pixi";
import {
  pixiDoubleTapEvent,
  pixiDragLeftEvent,
  pixiDragRightEvent,
  pixiTapEvent,
} from "../ui/pixi.js/PixiEventing";

const rightArrow = 39;
const leftArrow = 37;
const doubleKeyPressThresholdInMs = 300;

interface NextEventProps {
  onNextEvent: () => void;
  onNextEventDoublePress?: () => void;
}

interface PreviousEventProps {
  onPreviousEvent: () => void;
  onPreviousEventDoublePress?: () => void;
}

type EventingProps =
  | (NextEventProps & Partial<PreviousEventProps>)
  | (Partial<NextEventProps> & PreviousEventProps)
  | (NextEventProps & PreviousEventProps);

interface KeyDownEventHandler {
  event: "keydown";
  callback: () => (this: Element, evt: KeyboardEvent) => any;
}

interface PixiTapEventHandler {
  event: typeof pixiTapEvent;
  callback: () => () => any;
}

interface PixiDragRightEventHandler {
  event: typeof pixiDragRightEvent;
  callback: () => () => any;
}

interface PixiDragLeftEventHandler {
  event: typeof pixiDragLeftEvent;
  callback: () => () => any;
}

interface PixiDoubleTapEventHandler {
  event: typeof pixiDoubleTapEvent;
  callback: () => () => any;
}

function useEventListener(
  props: { element: Element | undefined } & (
    | KeyDownEventHandler
    | PixiTapEventHandler
    | PixiDragRightEventHandler
    | PixiDragLeftEventHandler
    | PixiDoubleTapEventHandler
  )
) {
  const { element, event, callback } = props;

  useEffect(
    function () {
      const listener = callback() as any;

      element?.addEventListener(event, listener);

      return () => {
        element?.removeEventListener(event, listener);
      };
    },
    [callback, element, event]
  );
}

function useEventing(props: EventingProps) {
  const {
    onNextEvent,
    onNextEventDoublePress,
    onPreviousEvent,
    onPreviousEventDoublePress,
  } = props;
  const canvasRef = useContext(PixiCanvasRefContext);

  useEventListener({
    event: "keydown",
    element: canvasRef,
    callback: useCallback(
      function _leftArrow() {
        let previousTime = Date.now();

        return function detectLeftArrow(ev: KeyboardEvent) {
          if (ev.keyCode === leftArrow) {
            const curTime = Date.now();

            if (curTime - previousTime < doubleKeyPressThresholdInMs) {
              setTimeout(() => onPreviousEventDoublePress?.());
            } else {
              setTimeout(() => onPreviousEvent?.());
            }

            previousTime = curTime;
          }
        };
      },
      [onPreviousEvent, onPreviousEventDoublePress]
    ),
  });

  useEventListener({
    event: "keydown",
    element: canvasRef,
    callback: useCallback(
      function _rightArrow() {
        let previousTime = Date.now();

        return function detectRightArrow(ev: KeyboardEvent) {
          if (ev.keyCode === rightArrow) {
            const curTime = Date.now();

            if (curTime - previousTime < doubleKeyPressThresholdInMs) {
              setTimeout(() => onNextEventDoublePress?.());
            } else {
              setTimeout(() => onNextEvent?.());
            }

            previousTime = curTime;
          }
        };
      },
      [onNextEvent, onNextEventDoublePress]
    ),
  });

  useEventListener({
    event: pixiTapEvent,
    element: canvasRef,
    callback: useCallback(() => {
      return function detect() {
        setTimeout(() => onNextEvent?.());
      };
    }, [onNextEvent]),
  });

  useEventListener({
    event: pixiDragRightEvent,
    element: canvasRef,
    callback: useCallback(() => {
      return function detect() {
        setTimeout(() => onNextEvent?.());
      };
    }, [onNextEvent]),
  });

  useEventListener({
    event: pixiDragLeftEvent,
    element: canvasRef,
    callback: useCallback(() => {
      return function detect() {
        setTimeout(() => onPreviousEvent?.());
      };
    }, [onPreviousEvent]),
  });

  useEventListener({
    event: pixiDoubleTapEvent,
    element: canvasRef,
    callback: useCallback(() => {
      return function detect() {
        setTimeout(() => onNextEventDoublePress?.());
      };
    }, [onNextEventDoublePress]),
  });
}

export function AppEventing(props: EventingProps) {
  useEventing({
    ...props,
    onNextEventDoublePress: props.onNextEventDoublePress ?? props.onNextEvent,
    onPreviousEventDoublePress:
      props.onPreviousEventDoublePress ?? props.onPreviousEvent,
  });

  return null;
}
