import { PixelateFilter } from "pixi-filters";
import React from "react";
import { PixiAppWrapper } from "../core/PixiApp/PixiApp";
import { defaultTextStyle } from "../core/PixiText/defaultTextStyle";
import {
  ApplyFilter,
  Background,
  Delay,
  PhysicsSprite,
  Say,
  Scene,
  SceneProvider,
} from "./AwesomeRetroCutscenes";

const width = 150;
const height = 200;

export function StepThruSingleScene() {
  return (
    <PixiAppWrapper width={width} height={height} backgroundColor="#cecece">
      {function Test() {
        const dialogStyle = {
          style: {
            ...defaultTextStyle(),
            fontSize: "1em",
            height: height / 2,
            top: height / 2,
          },
        };

        return (
          <SceneProvider>
            <Scene name="scene1">
              <Say actor="me" hideActor {...dialogStyle}>
                Hello
              </Say>
              <Say actor="me" hideActor {...dialogStyle}>
                World
              </Say>
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}

export function StepThruScene() {
  return (
    <PixiAppWrapper width={width} height={height} backgroundColor="#cecece">
      {function Test() {
        const dialogStyle = {
          style: {
            ...defaultTextStyle(),
            fontSize: "1em",
            height: height / 2,
            top: height / 2,
          },
        };

        return (
          <SceneProvider>
            <Scene name="scene1">
              <Say actor="me" hideActor {...dialogStyle}>
                Hello
              </Say>
              <Say actor="me" hideActor {...dialogStyle}>
                World
              </Say>
            </Scene>
            <Scene name="scene2">
              <Say actor="me" hideActor {...dialogStyle}>
                Again
              </Say>
              <Say actor="me" hideActor {...dialogStyle}>
                with feeling
              </Say>
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}

export function BackgroundWithFilter() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/images/background.jpeg",
        },
      ]}
    >
      {function Test() {
        return (
          <SceneProvider>
            <Scene name="scene1">
              <Background
                path="/images/background.jpeg"
                width={width}
                height={height}
              >
                <ApplyFilter
                  filters={{
                    filter: new PixelateFilter(0),
                    animation(elapsed, filter) {
                      (filter as PixelateFilter).size = elapsed / 100;
                    },
                  }}
                />
              </Background>
              <Delay timeout={1000} />
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}

export function Demo() {
  return (
    <PixiAppWrapper
      width={width}
      height={height}
      backgroundColor="#cecece"
      resources={[
        {
          type: "image",
          path: "/images/background.jpeg",
        },
        {
          type: "image",
          path: "/images/square_gray.png",
        },
      ]}
    >
      {function Test() {
        const dialogStyle = {
          style: {
            ...defaultTextStyle(),
            fontSize: "1em",
            height: height / 2,
            top: height / 2,
          },
        };

        return (
          <SceneProvider loop>
            <Scene name="scene1">
              <Background
                path="/images/background.jpeg"
                width={width}
                height={height}
              />
              <Delay timeout={500} />
              <PhysicsSprite
                path="/images/square_gray.png"
                x={width / 2}
                y={height / 2}
              />
              <Delay timeout={1000} />
              <Say actor="Grandma" hideActor {...dialogStyle}>
                mmmm.... this is good!
              </Say>
              <Delay timeout={1000} />
              <Say actor="Me" {...dialogStyle}>
                yo yo yo!
              </Say>
              <Delay timeout={1000}>
                <ApplyFilter
                  filters={{
                    filter: new PixelateFilter(0),
                    animation(elapsed, filter) {
                      (filter as PixelateFilter).size = elapsed;
                    },
                  }}
                />
              </Delay>
            </Scene>
          </SceneProvider>
        );
      }}
    </PixiAppWrapper>
  );
}
