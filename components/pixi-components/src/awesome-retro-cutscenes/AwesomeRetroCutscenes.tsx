import { Bodies, Composite, Engine, Runner } from "matter-js";
import { Application, Filter as PixiFilter } from "pixi.js";
import React, {
  cloneElement,
  ReactElement,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { PixiAppContext } from "../core/PixiApp/PixiContexts";
import { RegisterPixiEventListeners } from "../core/PixiEvents/PixiEvents";
import { useAddPixiFilter2 } from "../core/PixiFilter/PixiFilter";
import { useLoadPixiSprite } from "../core/PixiSprite/PixiSprite";
import {
  TypewriterText,
  TypewriterTextProps,
} from "../components/TypewriterText/TypewriterText";
import { makeReadOnlyContext } from "../utils/context";

interface PhysicsContextType {
  engine: ReturnType<typeof Engine.create>;
  runner: ReturnType<typeof Runner.create>;
}

const [PhysicsProvider, PhysicsContext] =
  makeReadOnlyContext<PhysicsContextType>();

interface OnNext {
  onNext?: () => void;
}

/**
 * Reveals children one at a time. The last child is passed the onNext prop.
 *
 * An alternative is to create an onNext context. We can let children register
 * themselves and possibly auto advanced if they never register.
 *
 * The only downside to this alternate version is that we have no way of
 * preventing a random grand-child from calling onNext.
 */
function RevealChildren(props: {
  onNext?: () => void;
  children: ReactElement<OnNext> | ReactElement<OnNext>[];
  currentScene?: {
    index: "next" | "prev" | "restart";
  };
}) {
  const { onNext: onParentNext, currentScene } = props;

  const originalChildren = useMemo(() => {
    const array = Array.isArray(props.children)
      ? props.children
      : [props.children];

    return array.map((child, key) => cloneElement(child, { key }));
  }, [props.children]);

  const [index, setIndex] = useState({ index: 0, key: 0 });
  const [children, setChildren] = useState<ReactElement[]>([
    originalChildren[index.index],
  ]);

  const onChildNext = useCallback(
    () => setIndex(({ index, key }) => ({ index: index + 1, key })),
    []
  );

  useEffect(() => {
    let callback = onChildNext;

    if (onParentNext && index.index === originalChildren.length - 1) {
      callback = onParentNext;
    }

    if (!originalChildren[index.index]) return;

    const _children = originalChildren
      .slice(0, index.index)
      .map((child, key) => cloneElement(child, { key: `${index.key}-${key}` }));

    _children.push(
      cloneElement(originalChildren[index.index], {
        key: `${index.key}-${index.index}`,
        onNext: callback,
      })
    );

    setChildren(_children);
  }, [index, onChildNext, onParentNext, originalChildren]);

  useEffect(() => {
    if (currentScene?.index === "next") {
      setIndex(({ index, key }) => ({ index: index + 1, key: key + 1 }));
    }
  }, [currentScene]);

  useEffect(() => {
    if (currentScene?.index === "prev") {
      setIndex(({ index, key }) => ({
        index: Math.max(index - 1, 0),
        key: key + 1,
      }));
    }
  }, [currentScene]);

  useEffect(() => {
    if (currentScene?.index === "restart") {
      setIndex(({ key }) => ({
        index: 0,
        key: key + 1,
      }));
    }
  }, [currentScene]);

  return <>{children}</>;
}

interface SceneProps {
  name: string;
  onNext?: () => void;
}

/**
 * - Press the right arrow and advance to the next scene. If at the last scene,
 *   then jump to the last node.
 * - Press the left arrow and advance to the previous scene. If at the first
 *   scene, then restart the first scene.
 */
export function SceneProvider(props: {
  loop?: boolean;
  physics?: {
    engine: Matter.IEngineDefinition;
  };
  children: React.ReactElement<SceneProps>[] | React.ReactElement<SceneProps>;
}) {
  const { physics } = props;

  const physicsRef = useRef(physics);
  useEffect(() => {
    physicsRef.current = physics;
  });

  const physicsContext = useMemo(
    () => ({
      engine: Engine.create(physicsRef.current?.engine),
      runner: Runner.create(),
    }),
    []
  );

  useEffect(
    function setupPhysics() {
      Runner.run(physicsContext.runner, physicsContext.engine);
    },
    [physicsContext]
  );

  const [currentScene, setCurrentScene] = useState<{
    index: "next" | "prev" | "restart";
  }>();

  return (
    <PhysicsProvider value={physicsContext}>
      <RegisterPixiEventListeners
        onNextEventDoublePress={() => setCurrentScene({ index: "next" })}
        onNextEvent={() => setCurrentScene({ index: "next" })}
        onPreviousEventDoublePress={() => setCurrentScene({ index: "prev" })}
        onPreviousEvent={() => setCurrentScene({ index: "prev" })}
      />
      <RevealChildren
        currentScene={currentScene}
        onNext={() => setCurrentScene({ index: "restart" })}
      >
        {props.children}
      </RevealChildren>
    </PhysicsProvider>
  );
}

export function Scene(
  props: {
    children: React.ReactElement<OnNext> | React.ReactElement<OnNext>[];
  } & SceneProps &
    OnNext
) {
  return <RevealChildren {...props} />;
}

const [FilterableComponentProvider, FilterableComponentContext] =
  makeReadOnlyContext<
    Application | { filters: PixiFilter[] | null } | undefined
  >();

export function Background(
  props: {
    path: string;
    width: number;
    height: number;
    children?: ReactNode;
  } & OnNext
) {
  const { onNext, width, height } = props;

  const sprite = useLoadPixiSprite({
    path: props.path,
    width,
    height,
    parentType: "app",
  });

  useEffect(() => {
    if (sprite) {
      onNext?.();
    }
  }, [onNext, sprite]);

  return (
    <FilterableComponentProvider value={sprite?.sprite}>
      {props.children}
    </FilterableComponentProvider>
  );
}

export function Delay(
  props: { timeout: number; children?: ReactNode } & OnNext
) {
  const { timeout, onNext } = props;

  useEffect(() => {
    if (!onNext) return;

    const _timeout = setTimeout(() => onNext(), timeout);

    return () => {
      clearTimeout(_timeout);
    };
  }, [onNext, timeout]);

  const app = useContext(PixiAppContext);

  return (
    <FilterableComponentProvider value={app}>
      {props.children}
    </FilterableComponentProvider>
  );
}

export function PhysicsSprite(
  props: {
    path?: string;
    width?: number;
    height?: number;
    x: number;
    y: number;
    children?: React.ReactNode;
  } & OnNext
) {
  const { onNext, path, x, y, width, height } = props;
  const app = useContext(PixiAppContext);

  const spriteData = useLoadPixiSprite({
    path: path,
    width: app?.renderer.width ?? 0,
    height: app?.renderer.height ?? 0,
    parentType: "app",
  });

  const sprite = spriteData?.sprite;
  const physics = useContext(PhysicsContext);

  useEffect(
    function createPhysicsBody() {
      if (!sprite && path) return;

      const spriteBody = Bodies.rectangle(
        x,
        y,
        width ?? sprite?.width ?? 0,
        height ?? sprite?.height ?? 0
      );

      Composite.add(physics.engine.world, spriteBody);

      return () => {
        Composite.remove(physics.engine.world, spriteBody);
      };
    },
    [height, path, physics.engine.world, sprite, width, x, y]
  );

  useEffect(() => {
    if (sprite) {
      onNext?.();
    }
  }, [sprite, onNext]);

  useEffect(
    function syncPosition() {
      function animate() {
        for (const dot of physics.engine.world.bodies) {
          if (dot.isStatic || !sprite) continue;

          const { x, y } = dot.position;

          sprite.position.x = x;
          sprite.position.y = y;

          break;
        }
      }

      if (!app || !sprite) return;

      app.ticker.add(animate);

      return () => {
        app.ticker.remove(animate);
      };
    },
    [app, physics.engine.world.bodies, sprite]
  );

  return (
    <FilterableComponentProvider value={sprite}>
      {props.children}
    </FilterableComponentProvider>
  );
}

export function Say(
  props: {
    actor: string;
    hideActor?: boolean;
    children: string | string[];
  } & OnNext &
    Pick<TypewriterTextProps, "style">
) {
  const { children, onNext, style, hideActor } = props;
  const app = useContext(PixiAppContext);
  const lines = Array.isArray(children) ? children : [children];
  const [lineNumber, setLineNumber] = useState(0);
  const line = lines[lineNumber] as string | undefined;
  const lineWithActor = `${props.actor}: ${line}`;
  const done = lineNumber === lines.length - 1;
  const text = !line ? undefined : hideActor ? line : lineWithActor;

  const onLineDone = useCallback(() => {
    setLineNumber((l) => l + 1);
    if (done) onNext?.();
  }, [done, onNext]);

  return (
    <TypewriterText
      lineId={text ?? ""}
      style={style}
      soundPath="/sounds/textscroll.wav"
      typingSpeedInMs={20}
      onLineDone={onLineDone}
      onLineDoneDelayMs={500}
      parent={app}
    >
      {text ?? ""}
    </TypewriterText>
  );
}

type Filter = Parameters<typeof useAddPixiFilter2>[0]["filters"];

export function ApplyFilter(props: { filters: Filter }) {
  const { filters } = props;
  const comp = useContext(FilterableComponentContext);

  useAddPixiFilter2({
    filters,
    comp,
  });

  return null;
}
