type Color = `#${string}`;

type ColorFromType<T> = T extends Color ? number : undefined;

export function rgbToHex<C extends Color | undefined>(
  color?: C
): ColorFromType<C> {
  return color ? +`0x${color.replace(/#/g, "")}` : (undefined as any);
}
