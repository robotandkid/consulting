import { useLayoutEffect, useRef } from "react";

export function useObjectAsRef<T>(obj: T) {
  const ref = useRef(obj);

  useLayoutEffect(() => {
    ref.current = obj;
  });

  return ref;
}
