import { Quintus } from "@robotandkid/quintus";
import React, { useEffect, useRef } from "react";
import { level } from "./level";

const getScreenDims = () => {
  const browserWidth = Math.max(
    document.documentElement.clientWidth || 0,
    window.innerWidth || 0
  );
  const browserHeight = Math.max(
    document.documentElement.clientHeight || 0,
    window.innerHeight || 0
  );
  return {
    browserWidth,
    browserHeight,
    aspect:
      browserWidth > browserHeight
        ? ("landscape" as const)
        : ("portrait" as const),
  };
};

/**
 * We want it to take up as much horizontal space as possible
 */
const getCanvasDims = (opts: ReturnType<typeof getScreenDims>) => {
  const { browserWidth, browserHeight, aspect } = opts;

  let canvasWidth = browserWidth;
  let canvasHeight = canvasWidth * 0.75;

  if (aspect === "portrait" && canvasHeight > browserHeight) {
    canvasHeight = browserHeight;
    canvasWidth = canvasHeight / 0.75;
  }

  return { canvasWidth, canvasHeight };
};

const getGameDims = () => {
  const gameWidth = 500;
  return {
    gameWidth,
    gameHeight: gameWidth * 0.75,
  };
};

export function QuintusDemo() {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const { canvasWidth, canvasHeight } = getCanvasDims(getScreenDims());
  const { gameWidth } = getGameDims();

  useEffect(() => {
    // Set up an instance of the Quintus engine  and include
    // the Sprites, Scenes, Input and 2D module. The 2D module
    // includes the `TileLayer` class as well as the `2d` componet.
    const Q = Quintus({
      audioPath: "quintus/audio/",
      imagePath: "quintus/images/",
      dataPath: "quintus/data/",
    })
      .include(["Sprites", "Scenes", "Input", "2D", "Anim", "Touch", "UI"])
      // Maximize this game to whatever the size of the browser is
      .setup(canvasRef.current?.id, {})
      // And turn on default input controls and touch input (for UI)
      .controls()
      .touch();

    // ## Player Sprite
    // The very basic player sprite, this is just a normal sprite
    // using the player sprite sheet with default controls added to it.
    Q.Sprite.extend("Player", {
      // the init constructor is called on creation
      init: function (p) {
        // You can call the parent's constructor with this._super(..)
        this._super(p, {
          sprite: "player",
          sheet: "player", // Setting a sprite sheet sets sprite width and height
          x: 450, // You can also set additional properties that can
          y: 90, // be overridden on object creation
          scale: 1.1,
        });

        // Add in pre-made components to get up and running quickly
        // The `2d` component adds in default 2d collision detection
        // and kinetics (velocity, gravity)
        // The `platformerControls` makes the player controllable by the
        // default input actions (left, right to move,  up or action to jump)
        // It also checks to make sure the player is on a horizontal surface before
        // letting them jump.
        this.add(["2d", "platformerControls", "animation"]);

        // Write event handlers to respond hook into behaviors.
        // hit.sprite is called everytime the player collides with a sprite
        this.on("hit.sprite", function (collision) {
          // Check the collision, if it's the Tower, you win!
          if (collision.obj.isA("Tower")) {
            Q.stageScene("endGame", 1, { label: "You Won!" });
            this.destroy();
          }
        });
      },

      step: function () {
        if (Q.inputs.right) this.play("walk_right");
        else if (Q.inputs.left) this.play("walk_left");
        else if (this.p.direction) {
          this.play(`stand_${this.p.direction}`);
        }
      },
    });

    // ## Tower Sprite
    // Sprites can be simple, the Tower sprite just sets a custom sprite sheet
    Q.Sprite.extend("Tower", {
      init: function (p) {
        this._super(p, { sheet: "tower" });
      },
    });

    // ## Enemy Sprite
    // Create the Enemy class to add in some baddies
    Q.Sprite.extend("Enemy", {
      init: function (p) {
        this._super(p, {
          sheet: "chicken",
          sprite: "chicken",
          vx: 100,
          scale: 0.8,
        });

        // Enemies use the Bounce AI to change direction
        // whenver they run into something.
        this.add(["2d", "aiBounce", "animation"]);

        // Listen for a sprite collision, if it's the player,
        // end the game unless the enemy is hit on top
        this.on("bump.left,bump.right,bump.bottom", function (collision) {
          if (collision.obj.isA("Player")) {
            Q.stageScene("endGame", 1, { label: "You Died" });
            collision.obj.destroy();
          }
        });

        // If the enemy gets hit on the top, destroy it
        // and give the user a "hop"
        this.on("bump.top", function (collision) {
          if (collision.obj.isA("Player")) {
            this.destroy();
            collision.obj.p.vy = -300;
          }
        });
      },

      step: function () {
        this.play(`walk_${this.p.vx < 0 ? "left" : "right"}`);
      },
    });

    // ## Level1 scene
    // Create a new scene called level 1
    Q.scene("level1", function (stage) {
      // Add in a repeater for a little parallax action
      stage.insert(
        new Q.Repeater({
          asset: "background.png",
          speedX: 0.5,
          speedY: 0.5,
        })
      );

      // Add in a tile layer, and make it the collision layer
      stage.collisionLayer(
        new Q.TileLayer({
          dataAsset: "level.json",
          sheet: "tiles",
        })
      );

      // Create the player and add them to the stage
      const player = stage.insert(new Q.Player());

      // Give the stage a moveable viewport and tell it
      // to follow the player.
      stage.add("viewport").follow(player);
      stage.viewport.scale = canvasWidth / gameWidth;

      // Add in a couple of enemies
      stage.insert(new Q.Enemy({ x: 700, y: 0 }));
      stage.insert(new Q.Enemy({ x: 800, y: 0 }));
      stage.insert(new Q.Enemy({ x: 1200, y: 0 }));

      // Finally add in the tower goal
      stage.insert(new Q.Tower({ x: 180, y: 50 }));
    });

    // To display a game over / game won popup box,
    // create a endGame scene that takes in a `label` option
    // to control the displayed message.
    Q.scene("endGame", function (stage) {
      const container = stage.insert(
        new Q.UI.Container({
          x: Q.width / 2,
          y: Q.height / 2,
          fill: "#ffffff",
        })
      );

      const button = container.insert(
        new Q.UI.Button({ x: 0, y: 0, fill: "#CCCCCC", label: "Play Again" })
      );

      container.insert(
        new Q.UI.Text({
          x: 10,
          y: -10 - button.p.h,
          label: stage.options.label,
        })
      );
      // When the button is clicked, clear all the stages
      // and restart the game.
      button.on("click", function () {
        Q.clearStages();
        Q.stageScene("level1");
      });

      // Expand the container to visibily fit it's contents
      // (with a padding of 20 pixels)
      container.fit(20);
    });

    // ## Asset Loading and Game Launch
    // Q.load can be called at any time to load additional assets
    // assets that are already loaded will be skipped
    // The callback will be triggered when everything is loaded
    Q.load(
      [
        "sprites.png",
        "sprites.json",
        "tiles.png",
        "background.png",
        "player.png",
        "chicken.png",
      ],
      function () {
        // Sprites sheets can be created manually
        Q.sheet("tiles", "tiles.png", { tileW: 32, tileH: 32 });

        Q.assets["level.json"] = level;

        Q.sheet("player", "player.png", {
          tileW: 19,
          tileH: 28,
          cols: 5,
        });

        Q.animations("player", {
          walk_right: {
            frames: [0, 1, 2, 3],
            rate: 4 / 30,
            flip: "x",
            loop: true,
          },
          walk_left: {
            frames: [0, 1, 2, 3],
            rate: 4 / 30,
            flip: false,
            loop: true,
          },
          run_right: { frames: [0, 1, 2, 3], flip: true, loop: true },
          run_left: { frames: [0, 1, 2, 3], flip: false, loop: true },
          jump_right: { frames: [0, 1, 2, 3], flip: true, loop: true },
          jump_left: { frames: [0, 1, 2, 3], flip: false, loop: true },
          stand_right: { frames: [3], flip: "x" },
          stand_left: { frames: [3], flip: false },
        });

        Q.sheet("chicken", "chicken.png", {
          tileW: 32,
          tileH: 34,
          cols: 14,
        });

        Q.animations("chicken", {
          walk_right: {
            frames: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
            rate: 1 / 30,
            flip: "x",
            loop: true,
          },
          walk_left: {
            frames: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
            rate: 1 / 30,
            flip: false,
            loop: true,
          },
        });

        // Or from a .json asset that defines sprite locations
        Q.compileSheets("sprites.png", "sprites.json");

        // Finally, call stageScene to run the game
        Q.stageScene("level1");
      }
    );

    // ## Possible Experimentations:
    //
    // The are lots of things to try out here.
    //
    // 1. Modify level.json to change the level around and add in some more enemies.
    // 2. Add in a second level by creating a level2.json and a level2 scene that gets
    //    loaded after level 1 is complete.
    // 3. Add in a title screen
    // 4. Add in a hud and points for jumping on enemies.
    // 5. Add in a `Repeater` behind the TileLayer to create a paralax scrolling effect.
  }, [canvasWidth, gameWidth]);

  return (
    <div>
      <canvas
        id="mainGame"
        ref={canvasRef}
        width={canvasWidth}
        height={canvasHeight}
      />
    </div>
  );
}
