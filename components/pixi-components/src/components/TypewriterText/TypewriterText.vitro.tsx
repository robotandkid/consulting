import { Application, TextStyle } from "pixi.js";
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { PixiApp, PixiAppWrapper } from "../../core/PixiApp/PixiApp";
import { PixiAppContext } from "../../core/PixiApp/PixiContexts";
import { RegisterPixiEventListeners } from "../../core/PixiEvents/PixiEvents";
import { defaultTextStyle } from "../../core/PixiText/defaultTextStyle";
import { TypewriterText } from "./TypewriterText";

function Fixture(props: { text: string[] }) {
  const style = useMemo(
    () =>
      new TextStyle({
        fontFamily: "Arial",
        fontSize: 12,
        fontStyle: "italic",
        fontWeight: "bold",
        fill: ["#ffffff", "#00ff99"], // gradient
        stroke: "#4a1850",
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: "#000000",
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
        wordWrap: true,
      }),
    []
  );

  const [line, setLine] = useState(0);
  const [lineId, setLineId] = useState(0);

  useEffect(() => {
    // eslint-disable-next-line no-console
    console.log("line", line);
  }, [line]);

  const [app, setApp] = useState<Application | null>(null);

  return (
    <>
      <PixiApp
        width={200}
        height={100}
        resolution={window.devicePixelRatio || 1}
        backgroundColor="#000000"
        resources={[
          {
            type: "sound",
            path: "/sounds/textscroll.wav",
          },
        ]}
        onAppInit={setApp}
      >
        <TypewriterText
          lineId={`${lineId}`}
          style={{
            ...style,
            width: 200,
            height: 100,
            backgroundColor: "#cecec",
          }}
          soundPath={"/sounds/textscroll.wav"}
          typingSpeedInMs={20}
          onLineDone={useCallback(() => {
            setLine((index) => {
              if (index < props.text.length - 1) {
                setLineId((c) => c + 1);
                return index + 1;
              }
              return index;
            });
          }, [props.text.length])}
          onLineDoneDelayMs={500}
          parent={app}
        >
          {props.text[line]}
        </TypewriterText>
        <RegisterPixiEventListeners
          onPreviousEvent={useCallback(() => {
            setLine(0);
            setLineId((c) => c + 1);
          }, [])}
        />
      </PixiApp>
    </>
  );
}

export function ASingleLine() {
  return <Fixture text={["a single line"]} />;
}

export function DuplicateLines() {
  return <Fixture text={["duplicate line", "duplicate line", "last line"]} />;
}

export function MultipleLines() {
  return (
    <Fixture
      text={[
        "the first line",
        "the second line",
        "the third line",
        "the fourth line",
        "the fifth line",
      ]}
    />
  );
}

export function Dimensions() {
  return (
    <PixiAppWrapper
      width={200}
      height={200}
      resolution={window.devicePixelRatio || 1}
      backgroundColor="#fffff"
      resources={[
        {
          type: "sound",
          path: "/sounds/textscroll.wav",
        },
      ]}
    >
      {function useMain() {
        const app = useContext(PixiAppContext);

        return (
          <TypewriterText
            lineId="hello"
            style={{
              ...defaultTextStyle(),
              height: 100,
              backgroundColor: "#f34141",
            }}
            soundPath={"/sounds/textscroll.wav"}
            typingSpeedInMs={20}
            onLineDoneDelayMs={500}
            parent={app}
          >
            hello world
          </TypewriterText>
        );
      }}
    </PixiAppWrapper>
  );
}
