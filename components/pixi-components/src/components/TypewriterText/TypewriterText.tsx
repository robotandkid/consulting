import { Graphics, ITextStyle, Text } from "pixi.js";
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { InternalPixiResourceMetaData } from "../../core/PixiApp/PixiApp.types";
import {
  PixiAppTickerContext,
  PixiLoaderResourcesContext,
} from "../../core/PixiApp/PixiContexts";
import {
  getDims,
  PixiParent,
  useAddToParent,
} from "../../core/PixiApp/useAddToParent";
import { usePixiEvents } from "../../core/PixiEvents/PixiEvents";
import { rgbToHex } from "../../utils/color";
import { useObjectAsRef } from "../../utils/hooks";

type TypewriterEvent = "type" | "stop";

type Color = `#${string}`;

interface TextProps {
  lineId: string;
  children: string;
  soundPath?: string;
  typingSpeedInMs: number;
}

interface ControlProps {
  autoStart?: boolean;
  /**
   * Defaults to 0
   */
  onLineDoneDelayMs?: number;
  onLineDone?: () => void;
}

interface StyleProps {
  style: Partial<ITextStyle> & {
    backgroundColor?: Color;
    width?: number;
    height?: number;
    top?: number;
    left?: number;
  };
}

interface AppProps {
  parent: PixiParent | undefined;
}

export function useTypeEffect(props: {
  event: TypewriterEvent;
  delayMs: number;
  typeAppTicker: () => void;
}) {
  const { event, typeAppTicker: type } = props;

  const appTicker = useContext(PixiAppTickerContext);
  const currentTick = useRef(0);

  // appTicker runs at 60 frames per second
  // so to get, for example, a typing delay of 500ms
  // you need to run the ticker every 30 frames
  // => 30 frames =  1 / 2 second *  60 frames / second

  const delayInSec = props.delayMs / 1000;
  const tickRate = Math.ceil(delayInSec * 60);

  useEffect(
    function typeEffect() {
      if (!appTicker) {
        return;
      }

      function typeTicker() {
        if (event === "stop") {
          return;
        }

        currentTick.current += 1;

        if (currentTick.current === tickRate) {
          currentTick.current = 0;

          if (event === "type") {
            type();
          }
        }
      }

      if (event === "type") {
        appTicker.add(typeTicker);
      }

      return () => {
        appTicker.remove(typeTicker);
      };
    },
    [appTicker, event, tickRate, type]
  );
}

function usePixiText(
  parent: PixiParent | undefined,
  style: StyleProps["style"]
) {
  const { width: parentWidth = 0, height: parentHeight = 0 } = getDims(parent);
  const { backgroundColor, width, height, top, left, ...textStyle } = style;
  const textStyleRef = useObjectAsRef(textStyle);
  const styleRef = useObjectAsRef({
    backgroundColor,
    width,
    height,
    top,
    left,
  });

  return useMemo(() => {
    const container = new Graphics();

    const _width = styleRef.current.width ?? parentWidth;
    const _height = styleRef.current.height ?? parentHeight;

    container.width = _width;
    container.height = _height;
    container.x = styleRef.current.left ?? 0;
    container.y = styleRef.current.top ?? 0;

    if (styleRef.current.backgroundColor) {
      container
        .lineStyle(0)
        .beginFill(rgbToHex(styleRef.current.backgroundColor), 2)
        .drawRect(0, 0, _width, _height)
        .endFill();
    }

    const text = new Text("", textStyleRef.current);
    text.anchor.set(0.5);
    text.x = (textStyleRef.current.wordWrapWidth ?? _width) / 2;
    text.y = _height / 2;

    container.addChild(text);

    return {
      containerCmp: container,
      textCmp: text,
      setPixiText(_text: string) {
        text.text = _text;
      },
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [parentWidth, parentHeight]);
}

/**
 * Types the given text with the given typing speed, and calls onDone when
 * finished.
 *
 * - Change the `event` to `stop`, to jump to the done state.
 * - Change the `event` to `type` to restart typing
 *
 * **Note:** Because of the way React components work, changing the `event` to
 * `type` by itself _twice_ in a row will *not* restart the component _twice_.
 * _In order to achieve this effect, you must also change the lineId_.
 */
function _TypewriterText(
  props: TextProps &
    ControlProps &
    StyleProps &
    AppProps & { event?: TypewriterEvent }
) {
  const {
    lineId = "0",
    event = "type",
    children: textFromProps = "",
    onLineDone: onDone,
    onLineDoneDelayMs: onDoneDelayMs,
  } = props;

  const appTicker = useContext(PixiAppTickerContext);
  const resources = useContext(PixiLoaderResourcesContext);
  const [finishedTyping, setFinishedTyping] = useState(false);

  const { containerCmp, textCmp, setPixiText } = usePixiText(
    props.parent,
    props.style
  );

  useTypeEffect({
    event,
    delayMs: props.typingSpeedInMs,
    typeAppTicker: useCallback(() => {
      if (textCmp.text === textFromProps) {
        setFinishedTyping(true);
        return;
      }

      const newText = textFromProps.substring(0, textCmp.text.length + 1);

      setPixiText(newText);
    }, [textCmp, setPixiText, textFromProps]),
  });

  useEffect(
    function handleRestartEvent() {
      // changes to any of these props need to trigger a restart
      if (
        textFromProps &&
        typeof lineId !== "undefined" &&
        event === "type" &&
        appTicker
      ) {
        setFinishedTyping(false);
        appTicker.addOnce(() => {
          setPixiText("");
        });
      }
    },
    [appTicker, event, lineId, setPixiText, textFromProps]
  );

  useEffect(
    function handleEndEvent() {
      if (
        textFromProps.includes(textCmp.text) &&
        textCmp.text !== textFromProps &&
        event === "stop" &&
        appTicker
      ) {
        setFinishedTyping(true);
        appTicker.addOnce(() => {
          setPixiText(textFromProps);
        });
      }
    },
    [appTicker, event, textCmp, setPixiText, textFromProps]
  );

  useEffect(
    function handleDoneEvent() {
      let unsubscribe: number;

      // changes that trigger a restart need to kill the onDone timer
      if (
        textFromProps &&
        typeof lineId !== "undefined" &&
        (finishedTyping || event === "stop") &&
        onDone
      ) {
        unsubscribe = setTimeout(() => {
          onDone();
        }, onDoneDelayMs || 0) as any;
      }

      return () => {
        if (typeof unsubscribe !== "undefined") {
          clearTimeout(unsubscribe);
        }
      };
    },
    [event, finishedTyping, lineId, onDone, onDoneDelayMs, textFromProps]
  );

  const textScrollId: InternalPixiResourceMetaData["id"] = `sound:${props.soundPath}`;

  useEffect(
    function sound() {
      if (finishedTyping || event === "stop") {
        resources?.[textScrollId]?.sound.stop();
      } else if (event === "type") {
        resources?.[textScrollId]?.sound.play({ loop: true });
      }
      return () => {
        resources?.[textScrollId]?.sound.stop();
      };
    },
    [event, finishedTyping, resources, textScrollId]
  );

  useEffect(
    function turnOffSound() {
      return () => {
        resources?.[textScrollId]?.sound.stop();
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  useAddToParent({
    child: containerCmp,
    parent: props.parent,
    parentType: "container",
  });

  return null;
}

export type TypewriterTextProps = TextProps &
  ControlProps &
  StyleProps &
  AppProps;

export function TypewriterText(props: TypewriterTextProps) {
  const { onLineDone: onDone } = props;

  const [event, setEvent] = useState<TypewriterEvent | undefined>(
    props.children ? "type" : "stop"
  );

  const pixiEvent = usePixiEvents();

  useEffect(
    function handleNextEvent() {
      if (pixiEvent?.type === "nextEvent") {
        setEvent((cmd) => {
          if (cmd === "stop") {
            onDone?.();
            return cmd;
          } else {
            return "stop";
          }
        });
      }
    },
    [onDone, pixiEvent]
  );

  useEffect(
    function handleNextEventDoublePress() {
      if (pixiEvent?.type === "nextEventDoublePress") {
        onDone?.();
      }
    },
    [onDone, pixiEvent]
  );

  useEffect(
    function handlePreviousEvent() {
      if (pixiEvent?.type === "previousEvent") {
        setEvent("type");
      }
    },
    [pixiEvent]
  );

  useEffect(
    function whenTextChangesRestart() {
      if (props.children) {
        setEvent(undefined);
      }
    },
    [props.children]
  );

  return <_TypewriterText {...props} event={event} />;
}
