# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.1](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/quintus@0.2.0...@robotandkid/quintus@0.2.1) (2025-03-05)


### Bug Fixes

* **quintus:** compiles again ([fc0b9be](https://gitlab.com/robotandkid/consulting/commit/fc0b9bec5f2787ea539c6e01e3b3d88e5a230e44))





# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/quintus@0.1.0...@robotandkid/quintus@0.2.0) (2025-03-05)


### Features

* quintus ([ae1e1c2](https://gitlab.com/robotandkid/consulting/commit/ae1e1c2d1f5d92ceb92443780c3a91271496c7e1))
* **urielavalos:** migrated from nft-portal version of sprite-motion ([17a3bae](https://gitlab.com/robotandkid/consulting/commit/17a3bae24a0bd6ed32f20f72672b52da711fe98a))





# [0.1.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/quintus@0.0.3...@robotandkid/quintus@0.1.0) (2023-11-07)


### Features

* **quintus:** migrated detect ([bac786d](https://gitlab.com/robotandkid/consulting/commit/bac786dd9c7d20efda059d8a06d3922157691ea2))
* **quintus:** migrated each ([7134548](https://gitlab.com/robotandkid/consulting/commit/7134548def74a194248914eabd43414ccb6b4276))
* **quintus:** migrated has ([b569d4d](https://gitlab.com/robotandkid/consulting/commit/b569d4dd6bbd400be94afb289633b851106899d0))
* **quintus:** migrated invoke ([3d81e80](https://gitlab.com/robotandkid/consulting/commit/3d81e80adfe40de9fb75bd92091aa2ae0dab238c))
* **quintus:** migrated isNumber,isFunction ([510ed96](https://gitlab.com/robotandkid/consulting/commit/510ed96a33cec415d34495033de751e8da0199b6))
* **quintus:** migrated isUndefined ([5ad37da](https://gitlab.com/robotandkid/consulting/commit/5ad37da2bd54886aecdfcdc62ce2b32ed334e418))
* **quintus:** migrated map ([e8fb83a](https://gitlab.com/robotandkid/consulting/commit/e8fb83ac232c3ac206d2230cf63de4879676d12b))
* **quintus:** migrated popProperty ([f9091db](https://gitlab.com/robotandkid/consulting/commit/f9091db1b7f5ad69dc66d3b00be05192ebcbe7ef))
* **quntus:** migrated isArray,isString,normalizedArg ([0a95449](https://gitlab.com/robotandkid/consulting/commit/0a95449d24f18d6ed0bbbde583d18d23b9373f8b))


### Reverts

* Revert "refactor(quintus): super simple on refactoring" ([1a63c40](https://gitlab.com/robotandkid/consulting/commit/1a63c40187f44bcbd2b46704bf2fd9f6cc2d9bfe))





## [0.0.3](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/quintus@0.0.2...@robotandkid/quintus@0.0.3) (2022-11-27)

**Note:** Version bump only for package @robotandkid/quintus
