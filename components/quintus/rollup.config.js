import { terser } from "rollup-plugin-terser";
import typescript from "@rollup/plugin-typescript";

export default {
  input: "src/Quintus.mjs",
  output: {
    dir: "dist",
    format: "esm",
    sourcemap: true,
  },
  external: [],
  plugins: [
    terser() /** For TS 🍩 */,
    typescript({
      tsconfig: "tsconfig.types.json",
    }),
  ],
};
