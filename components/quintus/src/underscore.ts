/**
 * Check if something is a string
 *
 * NOTE: this fails for non-primitives
 *
 * @for Quintus
 */
export const isString = (obj: unknown): obj is string => {
  return typeof obj === "string";
};

/**
 * Check if something is an Array
 *
 * @for Quintus
 */
export const isArray = (obj: unknown): obj is Array<any> => {
  return Array.isArray(obj);
};

/**
 * An internal utility method (utility methods are prefixed with underscores)
 * It's used to take a string of comma separated names and turn it into an
 * `Array` of names. If an array of names is passed in, it's left as is. Example
 * usage:
 *
 *          Q._normalizeArg("Sprites, Scenes, Physics   ");
 *          // returns [ "Sprites", "Scenes", "Physics" ]
 *
 *      Used by `Q.include` and `Q.Sprite.add` to add modules and components, respectively.
 *
 *      Most of these utility methods are a subset of Underscore.js,
 *      Most are pulled directly from underscore and some are
 *      occasionally optimized for speed and memory usage in lieu of flexibility.
 *
 *      Underscore.js is (c) 2009-2012 Jeremy Ashkenas, DocumentCloud Inc.
 *
 *      Underscore is freely distributable under the MIT license.
 *
 *      http://underscorejs.org
 *
 * @for Quintus
 */
export const normalizeArg = <Type extends string | string[] | unknown>(
  arg: Type
): Type extends string ? string[] : Type extends string[] ? string[] : Type => {
  type Return = Type extends string
    ? string[]
    : Type extends string[]
    ? string[]
    : any;

  if (isString(arg)) {
    return arg.replace(/\s+/g, "").split(",") as Return;
  }
  if (!isArray(arg)) {
    return [arg] as Return;
  }
  return arg as Return;
};

/**
 * Shortcut for hasOwnProperty
 *
 * @for Quintus
 */
export const has = (obj: unknown, key: string) => {
  return Object.prototype.hasOwnProperty.call(obj, key);
};

/**
 * Check if something is a number
 *
 * @for Quintus
 */
export const isNumber = (obj: unknown): obj is number => {
  return typeof obj === "number";
};

type AnyFunction = (...args: any[]) => any;

/**
 * Check if something is a function
 *
 * @for Quintus
 */
export const isFunction = (obj: unknown): obj is AnyFunction => {
  return typeof obj === "function";
};

/**
 * Check if something is an Object
 *
 * @for Quintus
 */
export const isObject = (obj: unknown): obj is object => {
  return Object.prototype.toString.call(obj) === "[object Object]";
};

/**
 * Removes a property from an object and returns it if it exists
 *
 * @for Quintus
 */
export const popProperty = <Type, ObjType extends Record<string, Type>>(
  obj: ObjType,
  property: string
) => {
  const val = obj[property];
  delete obj[property];
  return val;
};

/**
 * Check if something is undefined
 *
 * @for Quintus
 */
export const isUndefined = (obj: unknown): obj is undefined => {
  return obj === void 0;
};

/**
 * Basic iteration method. This can often be a performance handicap when the
 * callback iterator is created inline, as this leads to lots of functions that
 * need to be GC'd. Better is to define the iterator as a private method so.
 * Uses the built in `forEach` method
 *
 * @refactor we can probably refactor this to use built-in types
 * @for Quintus
 */
export const each = <Type>(
  obj: any,
  iterator: (value: Type, index: number, array: Type[]) => void,
  context?: unknown
) => {
  if (obj == null) {
    // careful this checks for undefined as well!
    return;
  }
  if (obj.forEach) {
    // arrays and array-like objects (ex: arguments list)
    obj.forEach(iterator, context);
  } else if (obj.length === +obj.length) {
    /** Have no idea---the issue is not sure what would already be caught above */
    for (let i = 0, l = obj.length; i < l; i++) {
      iterator.call(context, obj[i], i, obj);
    }
  } else {
    for (const key in obj) {
      iterator.call(context, obj[key], key as unknown as number, obj);
    }
  }
};

/**
 * Invoke the named property on each element of the array
 *
 * @for Quintus
 * @refactor
 */
export const invoke = function (
  arr: any[] | null | undefined,
  property: string | number,
  arg1: any,
  arg2: any
) {
  if (arr == null) {
    return;
  }
  for (let i = 0, l = arr.length; i < l; i++) {
    arr[i][property](arg1, arg2);
  }
};

/**
 * Basic detection method, returns the first instance where the iterator returns
 * truthy.
 *
 * @for Quintus
 */
export const detect = (
  obj: any,
  iterator: {
    call: (
      arg0: any,
      arg1: any,
      arg2: string | number,
      arg3: any,
      arg4: any
    ) => any;
  },
  context: any,
  arg1: any,
  arg2: any
) => {
  let result;
  if (obj == null) {
    return;
  }
  if (obj.length === +obj.length) {
    for (let i = 0, l = obj.length; i < l; i++) {
      result = iterator.call(context, obj[i], i, arg1, arg2);
      if (result) {
        return result;
      }
    }
    return false;
  } else {
    for (const key in obj) {
      result = iterator.call(context, obj[key], key, arg1, arg2);
      if (result) {
        return result;
      }
    }
    return false;
  }
};

/**
 * Returns a new Array with entries set to the return value of the iterator.
 *
 * @for Quintus
 */
export const map = <Type>(
  obj: any,
  iterator: (value: Type, index: number | string, array: Type[]) => void,
  context: any
) => {
  const results: typeof obj = [];
  if (obj == null) {
    return results;
  }
  if (obj.map) {
    return obj.map(iterator, context);
  }
  each<Type>(obj, (value, index, list) => {
    results[results.length] = iterator.call(context, value, index, list);
  });
  if (obj.length === +obj.length) {
    results.length = obj.length;
  }
  return results;
};

/**
 * Returns a sorted copy of unique array elements with null removed
 *
 * @for Quintus
 */
export const uniq = <Type>(arr: Type[]) => {
  arr = arr.slice().sort();

  const output: Type[] = [];

  let last = null;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== void 0 && last !== arr[i]) {
      output.push(arr[i]!);
    }
    last = arr[i];
  }
  return output;
};

/**
 * Returns a new array with the same entries as the source but in a random
 * order.
 *
 * @for Quintus
 */
export const shuffle = <Type>(obj: Type[]) => {
  const shuffled: typeof obj = [];
  each<Type>(obj, (value, index) => {
    const rand = Math.floor(Math.random() * (index + 1));
    shuffled[index] = shuffled[rand]!;
    shuffled[rand] = value;
  });
  return shuffled;
};

/**
 * Return an object's keys as a new Array
 *
 * @for Quintus
 */
export const keys = Object.keys;

/**
 * Return an array in the range from start to stop
 *
 * @for Quintus
 */
export const range = (start: number, stop: number, step: number) => {
  step = step || 1;

  const len = Math.max(Math.ceil((stop - start) / step), 0);
  let idx = 0;
  const range: number[] = new Array(len);

  while (idx < len) {
    range[idx++] = start;
    start += step;
  }

  return range;
};

let idIndex = 0;

/**
 * Return a new unique identifier
 *
 * @for Quintus
 */
export const uniqueId = () => {
  return idIndex++;
};
