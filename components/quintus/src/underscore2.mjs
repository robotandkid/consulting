// @ts-check
/**
 * Extends a destination object with a source object (modifies destination
 * object)
 *
 * @template T
 * @template U
 * @function Q._extend
 * @param {T} dest - Destination object
 * @param {U} source - Source object
 * @returns {T & U} Returns the dest object
 */
export const extend = (dest, source) => {
  if (!source) {
    return /** @type {T & U} */ (dest);
  }
  for (let prop in source) {
    // @ts-expect-error it's fine
    dest[prop] = source[prop];
  }
  return /** @type {T & U} */ (dest);
};
