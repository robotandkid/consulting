interface Animation {
  frames: number[];
  rate?: number;
  flip?: boolean | "x";
  loop?: boolean;
}

type LeftRight = "left" | "right";

type AnimationState =
  | "run"
  | "fire"
  | "stand"
  | "fall"
  | "walk"
  | "jump"
  | "duck";

export type AnimationName = `${AnimationState}_${LeftRight}`;

type AnimationsFnOpts = {
  [name in AnimationName]?: Animation;
};

export type AnimationsFn<SpriteSheetName extends string = string> = (
  spriteSheetName: SpriteSheetName,
  opts: Partial<AnimationsFnOpts>
) => void;
