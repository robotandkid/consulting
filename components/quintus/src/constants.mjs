/** Bitmask 0 to indicate no sprites */
export const SPRITE_NONE = 0;
/** Default sprite type 1 */
export const SPRITE_DEFAULT = 1;
/** Particle sprite type 2 */
export const SPRITE_PARTICLE = 2;
/** Active sprite type 4 */
export const SPRITE_ACTIVE = 4;
/** UI sprite type 64 */
export const SPRITE_UI = 64;
