/* eslint-disable @typescript-eslint/no-empty-interface */

import { AnimationName, AnimationsFn } from "./animations";

interface QuintusClass {
  (): QuintusInstance;
}

type Filename = string;

interface Point {
  x: number;
  y: number;
}

interface VPoint {
  vx: number;
  vy: number;
}

interface CollisionObject {
  isA(kind: "Tower" | "Player"): boolean;
  destroy(): void;
  p: Partial<Point> & {
    vx?: number;
    vy?: number;
  };
}

type SpriteEvent =
  | "bump.left"
  | "bump.right"
  | "bump.bottom"
  | "bump.top"
  | "hit.sprite";

interface SpriteConstructor {
  extend(
    sprite: "Player" | "Tower" | "Enemy",
    opts: {
      init?: (this: Sprite, p: Sprite) => void;
      step?: (this: Sprite, dt: number) => void;
    }
  ): void;
}

type ComponentFeature =
  | "2d"
  | "platformerControls"
  | "aiBounce"
  | "animation"
  | "tween";

interface SpriteOpts extends Partial<Point>, Partial<VPoint> {
  /**
   * the stacking order, higher in front, of the sprite (used by the Scenes module)
   */
  z?: number;
  /**
   * sprite width
   */
  w?: number;
  /**
   * sprite height
   */
  h?: number;
  /**
   * the distance from the left of the sprite to its center
   */
  cx?: number;
  /**
   * the distance from the top of the sprite to its center
   */
  cy?: number;
  /**
   * the scaling multiplier to grow or shrink the sprite. If not set, it defaults to 1.
   */
  scale?: number;
  /**
   * the angle of rotation in degrees of the sprite. If not set it defaults to 0
   */
  angle?: number;
  // p.type - the type of Sprite, a bit-mask used for collision detection, defaults to Q.SPRITEDEFAULT | Q.SPRITEACTIVE
  /**
   * an array of points in the form [[x0,y0],[x1,y1]..] that define convex collision shape used for collision detection (more on this in the next chapter). If not set defaults to the square bounding box.
   */
  points?: Array<[number, number]>;
  /**
   * the name of the asset used to render a static image for the sprite (see the next couple of sections)
   */
  sprite?: string;
  asset?: string;
  /**
   * use a sprite sheet instead of an asset to render (also, see the next couple of sections)
   */
  sheet?: string;
  /**
   * if using a sprite sheet, this is the frame in the sheet to use
   */
  frame?: number;
}

interface Sprite {
  _super(p: Sprite, opts: SpriteOpts): void;

  add(feature: ComponentFeature[] | string): void;

  on(
    event: // This is dumb. There's gotta be a better way
    | `${SpriteEvent}`
      | `${SpriteEvent},${SpriteEvent}`
      | `${SpriteEvent},${SpriteEvent},${SpriteEvent}`
      | `${SpriteEvent},${SpriteEvent},${SpriteEvent},${SpriteEvent}`
      | `${SpriteEvent},${SpriteEvent},${SpriteEvent},${SpriteEvent},${SpriteEvent}`,
    cb: (
      this: Sprite,
      collision: {
        obj: CollisionObject;
      }
    ) => void
  ): void;

  destroy(): void;

  p: Point &
    VPoint & {
      landed: number;
      jump: number;
      direction: "left" | "right" | undefined;
    };

  play(animationName: AnimationName): void;
}

type SupportedImage = "png" | "jpg" | "gif" | "jpeg";

type SupportedAudio = "ogg" | "wav" | "m4a" | "mp3";

interface RepeaterConstructor {
  new (opts: {
    asset: `${string}.${SupportedImage}`;
    speedX: number;
    speedY: number;
  }): Repeater;
}

interface Repeater {}

interface TileLayerConstructor {
  new (opts: { dataAsset: `${string}.json`; sheet: string }): TileLayer;
}

interface TileLayer {}

interface PlayerConstructor {
  new (): Player;
}

interface Player {}

interface Viewport {
  follow(thing: Player): void;
}

interface EnemyConstructor {
  new (opts: Point): Enemy;
}

interface Enemy {}

interface TowerConstructor {
  new (opts: Point): Tower;
}

interface Tower {}

type HtmlColor = {
  fill: `#${string}` | `rgba(${number},${number},${number},${number})`;
};

interface ButtonConstructor {
  new (opts: Point & HtmlColor & { label: string }): Button;
}

interface Button {
  p: {
    h: number;
  };

  on(event: "click", cb: () => void): void;
}

interface TextConstructor {
  new (opts: Point & { label: string }): Text;
}

interface Text {}

interface ContainerConstructor {
  new (opts: Point & HtmlColor): Container;
}

type UIItem = Button | Text;

interface Container {
  insert<Item extends UIItem>(item: Item): Item;
  fit(padding: number): void;
}

type StageItem = Repeater | Player | Enemy | Tower;

interface Stage {
  insert<Item extends StageItem>(item: Item): Item;
  collisionLayer(tileLayer: TileLayer): void;
  add(thing: "viewport"): Viewport;

  options: {
    label: string;
  };

  viewport: {
    scale: number;
  };
}

interface SpriteSheetOpts {
  tileW?: 64 | number;
  tileH?: 64 | number;
  /**
   * sprite sheet width
   */
  w?: number;
  /**
   * sprite sheet height
   */
  h?: number;
  /**
   * start x
   */
  sx?: number;
  /**
   * start y
   */
  sy?: number;
  /**
   * spacing between each tile x (after 1st)
   */
  spacingX?: number;
  /**
   * spacing between each tile y
   */
  spacingY?: number;
  /**
   * margin around each tile x
   */
  marginX?: number;
  /**
   * margin around each tile y
   */
  marginY?: number;
  /**
   * number of columns per row
   */
  cols?: number;
}

interface QuintusOpts {
  imagePath?: "images/" | `${string}/`;
  audioPath?: "audio/" | `${string}/`;
  dataPath?: "data/" | `${string}/`;
  audioSupported?: ["mp3", "ogg"] | Array<SupportedAudio>;
  sound?: true | boolean;
  frameTimeLimit?: 100 | number;
}

export function Quintus(opts?: QuintusOpts): QuintusInstance;

interface SetupOpts {
  width?: number;
  height?: number;
  maximize?: boolean;
  maxWidth?: 5000 | number;
  maxHeight?: 5000 | number;
  /**
   * If the canvas width is larger, it will resize the game to fit
   * @deprecated use stage.viewport.scale
   */
  resampleWidth?: number;
  /**
   * if the canvas height is larger, it will resize the game to fit
   * @deprecated use stage.viewport.scale
   */
  resampleHeight?: number;
  /**
   * @deprecated use stage.viewport.scale
   * If the canvas width is smaller, it will resize the canvas to fit
   */
  upsampleWidth?: number;
  /**
   * If the canvas height is smaller, it will resize the canvas to fit
   * @deprecated use stage.viewport.scale
   */
  upsampleHeight?: number;
  scaleToFit?: boolean;
}

function setup(canvasId = "quintus"): QuintusInstance;
function setup(opts: SetupOpts = {}): QuintusInstance;
function setup(canvasId = "quintus", opts: SetupOpts = {}): QuintusInstance;

interface QuintusInstance {
  include(
    feature: Array<
      "Sprites" | "Scenes" | "Input" | "2D" | "Anim" | "Touch" | "UI"
    >
  ): QuintusInstance;

  setup: typeof setup;

  controls(): QuintusInstance;
  touch(): QuintusInstance;

  pauseGame(): void;
  unpauseGame(): void;

  load(
    assets: Array<`${string}.${SupportedAudio | SupportedImage | "json"}`>,
    cb: () => void
  ): void;

  animations: AnimationsFn;

  /**
   * To create an animation, stack them vertically.
   */
  sheet<Name extends string>(
    name: Name,
    fileName: `${Name}.png`,
    opts: SpriteSheetOpts
  ): void;

  /**
   * Create a number of `Q.SpriteSheet` objects from an image asset and a sprite data JSON asset
   */
  compileSheets<Filename extends string>(
    image: `${Filename}.${string}`,
    spriteData: `${Filename}.json`
  ): void;

  stageScene(
    kind: "endGame" | "level1",
    numOpt?: 1,
    opts?: { label: string }
  ): void;
  scene(name: string, cb: (stage: Stage) => void): void;
  clearStages(): void;

  width: number;
  height: number;

  /**
   * The currently selected inputs
   */
  inputs: {
    [key in "up" | "down" | "left" | "right" | "fire" | "action"]: true;
  };

  Sprite: SpriteConstructor;

  Repeater: RepeaterConstructor;

  TileLayer: TileLayerConstructor;

  Player: PlayerConstructor;

  Enemy: EnemyConstructor;

  Tower: TowerConstructor;

  UI: {
    Container: ContainerConstructor;
    Button: ButtonConstructor;
    Text: TextConstructor;
  };

  assets: {
    [key in name]: string[][];
  };
}
