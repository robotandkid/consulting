/**
 * Check if something is a string
 *
 * NOTE: this fails for non-primitives
 *
 * @for Quintus
 */
export declare const isString: (obj: unknown) => obj is string;
/**
 * Check if something is an Array
 *
 * @for Quintus
 */
export declare const isArray: (obj: unknown) => obj is any[];
/**
 * An internal utility method (utility methods are prefixed with underscores)
 * It's used to take a string of comma separated names and turn it into an
 * `Array` of names. If an array of names is passed in, it's left as is. Example
 * usage:
 *
 *          Q._normalizeArg("Sprites, Scenes, Physics   ");
 *          // returns [ "Sprites", "Scenes", "Physics" ]
 *
 *      Used by `Q.include` and `Q.Sprite.add` to add modules and components, respectively.
 *
 *      Most of these utility methods are a subset of Underscore.js,
 *      Most are pulled directly from underscore and some are
 *      occasionally optimized for speed and memory usage in lieu of flexibility.
 *
 *      Underscore.js is (c) 2009-2012 Jeremy Ashkenas, DocumentCloud Inc.
 *
 *      Underscore is freely distributable under the MIT license.
 *
 *      http://underscorejs.org
 *
 * @for Quintus
 */
export declare const normalizeArg: <Type extends unknown>(arg: Type) => Type extends string ? string[] : Type extends string[] ? string[] : Type;
/**
 * Shortcut for hasOwnProperty
 *
 * @for Quintus
 */
export declare const has: (obj: unknown, key: string) => boolean;
/**
 * Check if something is a number
 *
 * @for Quintus
 */
export declare const isNumber: (obj: unknown) => obj is number;
type AnyFunction = (...args: any[]) => any;
/**
 * Check if something is a function
 *
 * @for Quintus
 */
export declare const isFunction: (obj: unknown) => obj is AnyFunction;
/**
 * Check if something is an Object
 *
 * @for Quintus
 */
export declare const isObject: (obj: unknown) => obj is object;
/**
 * Removes a property from an object and returns it if it exists
 *
 * @for Quintus
 */
export declare const popProperty: <Type, ObjType extends Record<string, Type>>(obj: ObjType, property: string) => Type | undefined;
/**
 * Check if something is undefined
 *
 * @for Quintus
 */
export declare const isUndefined: (obj: unknown) => obj is undefined;
/**
 * Basic iteration method. This can often be a performance handicap when the
 * callback iterator is created inline, as this leads to lots of functions that
 * need to be GC'd. Better is to define the iterator as a private method so.
 * Uses the built in `forEach` method
 *
 * @refactor we can probably refactor this to use built-in types
 * @for Quintus
 */
export declare const each: <Type>(obj: any, iterator: (value: Type, index: number, array: Type[]) => void, context?: unknown) => void;
/**
 * Invoke the named property on each element of the array
 *
 * @for Quintus
 * @refactor
 */
export declare const invoke: (arr: any[] | null | undefined, property: string | number, arg1: any, arg2: any) => void;
/**
 * Basic detection method, returns the first instance where the iterator returns
 * truthy.
 *
 * @for Quintus
 */
export declare const detect: (obj: any, iterator: {
    call: (arg0: any, arg1: any, arg2: string | number, arg3: any, arg4: any) => any;
}, context: any, arg1: any, arg2: any) => any;
/**
 * Returns a new Array with entries set to the return value of the iterator.
 *
 * @for Quintus
 */
export declare const map: <Type>(obj: any, iterator: (value: Type, index: number | string, array: Type[]) => void, context: any) => any;
/**
 * Returns a sorted copy of unique array elements with null removed
 *
 * @for Quintus
 */
export declare const uniq: <Type>(arr: Type[]) => Type[];
/**
 * Returns a new array with the same entries as the source but in a random
 * order.
 *
 * @for Quintus
 */
export declare const shuffle: <Type>(obj: Type[]) => Type[];
/**
 * Return an object's keys as a new Array
 *
 * @for Quintus
 */
export declare const keys: {
    (o: object): string[];
    (o: {}): string[];
};
/**
 * Return an array in the range from start to stop
 *
 * @for Quintus
 */
export declare const range: (start: number, stop: number, step: number) => number[];
/**
 * Return a new unique identifier
 *
 * @for Quintus
 */
export declare const uniqueId: () => number;
export {};
