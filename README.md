# Project Root

This is a mono repo. Yay!

## Development Flow

### Add a New Component

```bash
lerna create <@scope/name> components
yarn workspace:create:component <name> # don't use the scope
```

**Note:** The last command will override most `package.json` settings except for
the package name!

### Add a New App

```bash
yarn workspace:create:app <name>
```

**Note:** The last command will override most `package.json` settings except for
the package name!

Then setup nextjs:

- MDX: <https://mdxjs.com/getting-started/next>
- font caching
- setup the custom MDX loader for YAML support
- this is all done though `next.config.js`

### Yarn Commands

- yarn-specific tasks follow the yarn workspace naming convention:
  - tasks specific to a workspace are `workspace:*`
  - tasks specific to all workspaces are `workspaces:*`

### Develop

- make some changes
- commit
- then when you're ready, run `yarn lerna:version` to bump the package version.
- note that this will bump _all_ packages
- to bump a single package, you likely want to use the `--force-publish` option.
  Ex: `yarn lerna:version --force-publish=@robotandkid/components`
