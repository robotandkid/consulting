const {
  findSitePackageName,
  findSiteWorkspaceDependencies,
  findChangedDependencies,
} = require("./helpers");

module.exports = {
  onPreBuild: async ({ constants: { PUBLISH_DIR }, utils }) => {
    const sitePackageName = findSitePackageName(PUBLISH_DIR);

    utils.status.show({
      summary: `Checking to see ${sitePackageName} has changes`,
    });

    const dependencies = await findSiteWorkspaceDependencies(
      { utils },
      sitePackageName
    );

    const hasChange = await findChangedDependencies(
      { utils },
      dependencies,
      process.env.CACHED_COMMIT_REF
    );

    if (!hasChange) {
      utils.build.cancelBuild(
        `There have been no changes to ${sitePackageName} or its dependencies. Cancelling build.`
      );
    }
  },
};
