const fs = require("fs");
const path = require("path");

/**
 * @param  {String}  publishDir  - Is something like
 *   `apps/urielavalos/out`.
 */
function findSitePackageName(publishDir) {
  const rootDir = path.join(publishDir, "../");
  const packageJsonStr = fs.readFileSync(path.join(rootDir, "package.json"));
  const packageJson = JSON.parse(packageJsonStr);

  return packageJson.name;
}

/**
 * @param  {Object}  param.utils  - Netlify utils
 * @param  {Object}  param.build  - Netlify build
 * @param  {String}  command
 * @param  {String[]}  args
 * @param  {Object}  option.resultType  - One of "object" | "array"
 */
async function runAsJson(
  { utils },
  command,
  args = [],
  { resultType = "object" }
) {
  const { stdout } = await utils.run(command, args);

  let delimiters = ["{", "}"];
  if (resultType === "array") delimiters = ["[", "]"];

  const graphArray = stdout.split("\n");
  const graphStart = graphArray.indexOf(delimiters[0]);
  const graphEnd = graphArray.indexOf(delimiters[1]);

  if (graphStart === -1) {
    utils.build.cancelBuild(
      `Given command ${command} ${args.join(
        " "
      )} does not contain JSON output ${resultType}`
    );
    throw new Error("Aborting plugin");
  }

  return JSON.parse(
    graphArray.reduce((total, cur, index) => {
      if (index >= graphStart && index <= graphEnd) {
        return `${total}${cur}`;
      }
      return total;
    }, "")
  );
}

/**
 * Returns the package's dependencies that are also workspaces.
 * Includes transitive dependencies.
 *
 * @param  {helpers.utils}  -  Netlify utils
 * @param  {Map}  rapGraphMap  - The map of workspace dependencies
 *   returned by lerna.
 *
 *   Example:
 *
 *   ```
 *   {
 *     "@robotandkid/urielavalos": [
 *       "@mapbox/rehype-prism",
 *       "@mdx-js/loader",
 *       "@mdx-js/react",
 *      ]
 *   }
 *   @param {String} packageName
 * ```
 */
function findAllWorkspaceDependencies({ utils }, rawGraphMap, packageName) {
  const rawDeps = rawGraphMap[packageName];

  if (!rawDeps) {
    utils.build.cancelBuild(
      `${packageName} not found in dependency graph /shrug`
    );
    throw new Error("Aborting plugin");
  }

  const _workspaceDeps = rawDeps.filter((dep) => rawGraphMap[dep]);

  return [
    packageName,
    ..._workspaceDeps,
    ..._workspaceDeps.flatMap((dep) =>
      findAllWorkspaceDependencies({ utils }, rawGraphMap, dep)
    ),
  ];
}

/**
 * Returns the entire site's workspace dependencies (including
 * transitive dependencies) as paths in the root repo.
 *
 * @param  {Object}  param.utils  - Netlify utils
 * @param  {String}  sitePackageName
 * @returns
 */
async function findSiteWorkspaceDependencies({ utils }, sitePackageName) {
  utils.status.show({
    summary: `Finding workspace dependencies for ${sitePackageName}`,
  });

  /**
   * Example:
   *
   * ```
   * {
   *   "@robotandkid/urielavalos": [
   *     "@mapbox/rehype-prism",
   *     "@mdx-js/loader",
   *     "@mdx-js/react"
   *   ]
   * }
   * ```
   */
  const rawGraphMap = await runAsJson(
    { utils },
    "yarn",
    ["lerna", "ls", "--all", "--graph"],
    { resultType: "object" }
  );

  const dependencies = findAllWorkspaceDependencies(
    { utils },
    rawGraphMap,
    sitePackageName
  );

  if (dependencies.length === 0) {
    utils.build.cancelBuild(
      `Something prevented us from finding the dependencies for ${sitePackageName}`
    );
    throw new Error("Aborting plugin");
  }

  utils.status.show({
    summary: `Found workspace dependencies for ${sitePackageName}`,
  });

  return [...new Set(dependencies)];
}

/**
 * Returns true when one or more of the given packages have changed
 * since the given commit
 *
 * @param  {{ utils: { build: Object; status: Object } }}  param.utils
 * @param  {String[]}  packageNames
 * @param  {String}  commitHash
 */
async function findChangedDependencies({ utils }, packageNames, commitHash) {
  const changes = await runAsJson(
    { utils },
    "yarn",
    ["lerna", "ls", "--all", "--json", "--since", commitHash],
    { resultType: "array" }
  );

  utils.status.show({
    summary: "Found changeset",
    text: `[${changes.map(({ name }) => name)}]`,
  });

  return packageNames.some((packageName) =>
    changes.find(({ name }) => name === packageName)
  );
}

module.exports = {
  findSitePackageName,
  findSiteWorkspaceDependencies,
  findChangedDependencies,
};
