# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.2.0](https://gitlab.com/robotandkid/consulting/compare/@robotandkid/monorepo-build-check@0.1.0...@robotandkid/monorepo-build-check@0.2.0) (2023-08-07)


### Features

* **root:** added prettier jsdoc plugin ([a9c86fc](https://gitlab.com/robotandkid/consulting/commit/a9c86fc36756ee6398a6ea996c52c16846061cb5))





# 0.1.0 (2021-07-01)


### Bug Fixes

* **leandertile:** form should submit now ([96b7e5c](https://gitlab.com/robotandkid/consulting/commit/96b7e5c967cfae610f63d0fe334c785937a1ba95))
* **netlify:** added plugin manifest ([e1acb87](https://gitlab.com/robotandkid/consulting/commit/e1acb8760e3a9c575a0db67b659d91a2f16c5ec8))
* **netlify:** added plugin manifest ([15d8050](https://gitlab.com/robotandkid/consulting/commit/15d8050426b4db90792a2062802dc73de2083389))
* **netlify:** should build again ([099a111](https://gitlab.com/robotandkid/consulting/commit/099a1112157e94370fe2c1c46bee395646ae0b7d))
* **netlify-utils/build:** also added to lerna + more debug info ([dd2c3f7](https://gitlab.com/robotandkid/consulting/commit/dd2c3f7edf6cf9ccd53f01957f528e4819e55c2d))


### Features

* **netlify:** 1st pass build plugin ([c194efe](https://gitlab.com/robotandkid/consulting/commit/c194efef941b31e65ac117594cb3d8c8b7667376))
* **netlify:** added 1st pass build-check plugin ([4966f67](https://gitlab.com/robotandkid/consulting/commit/4966f671629fcc29f120963e001b6e4f258e80cd))
