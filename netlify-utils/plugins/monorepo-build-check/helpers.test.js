const {
  findSiteWorkspaceDependencies,
  findChangedDependencies,
} = require("./helpers");

const utilsGenerator = (runoutput1) => ({
  status: {
    show(args) {
      // eslint-disable-next-line no-console
      console.log(args);
    },
  },
  async run(command, args) {
    // eslint-disable-next-line no-console
    console.log("run", command, args);

    return Promise.resolve({ stdout: JSON.stringify(runoutput1, null, "  ") });
  },
  build: {
    cancelBuild(msg) {
      // eslint-disable-next-line no-console
      console.log("canceling build", msg);
    },
  },
});

describe("findSiteWorkspaceDependencies", () => {
  it("always returns at least the site as a dependency", async () => {
    const utils = utilsGenerator({
      site: [],
    });

    const actual = await findSiteWorkspaceDependencies({ utils }, "site");

    expect(actual).toEqual(["site"]);
  });

  it("finds transitive dependencies", async () => {
    const utils = utilsGenerator({
      site: ["workspace-dep1", "dep1"],
      "workspace-dep1": ["workspace-dep2"],
      "workspace-dep2": ["foo"],
    });

    const actual = await findSiteWorkspaceDependencies({ utils }, "site");

    expect(actual).toEqual(["site", "workspace-dep1", "workspace-dep2"]);
  });
});

describe("findChangedDependencies", () => {
  it("returns false when site is not found in change set", async () => {
    const utils = utilsGenerator([
      {
        name: "@robotandkid",
      },
    ]);

    const actual = await findChangedDependencies({ utils }, ["site", "dep1"]);

    expect(actual).toBe(false);
  });

  it("returns true when site  is found in change set", async () => {
    const utils = utilsGenerator([
      {
        name: "site",
      },
    ]);

    const actual = await findChangedDependencies({ utils }, ["site", "dep1"]);

    expect(actual).toBe(true);
  });

  it("returns true when site dependency is found in change set", async () => {
    const utils = utilsGenerator([
      {
        name: "dep1",
      },
    ]);

    const actual = await findChangedDependencies({ utils }, ["site", "dep1"]);

    expect(actual).toBe(true);
  });
});
