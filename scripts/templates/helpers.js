/* eslint-disable no-console */
import child from "child_process";
import path from "path";
import fs from "fs";
import chalk from "chalk";

const rootPath = path.join(__dirname, "../../");

export const errorStyle = chalk.bold.red;
export const successStyle = chalk.bold.green;

/**
 * Returns the package/version that is installed.
 */
export function getInstalledVersion(pkgName) {
  const buffer = child.execSync(`yarn list --depth 0 --pattern ${pkgName}`, {
    cwd: rootPath,
  });

  const packageRegexp = new RegExp(`^.* (${pkgName}@.*).*$`);
  const result = buffer.toString().split("\n");
  const pkgMatch = result.find((line) => packageRegexp.test(line));

  if (pkgMatch) {
    return pkgMatch.replace(packageRegexp, "$1");
  }

  return undefined;
}

export function addDevDeps(scope, componentName, deps = []) {
  deps.forEach((pkg) => {
    const pkgPath = getInstalledVersion(pkg) || pkg;

    console.log(`installing ${pkgPath}`);

    child.execSync(
      `yarn workspace @${scope}/${componentName} add --exact --dev ${pkgPath}`,
      {
        cwd: rootPath,
      }
    );
  });
}

export function addPeerDeps(scope, componentName, deps = []) {
  deps.forEach((pkg) => {
    const pkgPath = getInstalledVersion(pkg) || pkg;

    console.log(`installing ${pkgPath}`);

    child.execSync(
      `yarn workspace @${scope}/${componentName} add --exact --peer ${pkgPath}`,
      {
        cwd: rootPath,
      }
    );
  });
}

exports.addProdDeps = (scope, componentName, deps = []) => {
  deps.forEach((pkg) => {
    const pkgPath = getInstalledVersion(pkg) || pkg;

    console.log(`installing ${pkgPath}`);

    child.execSync(
      `yarn workspace @${scope}/${componentName} add --exact ${pkgPath}`,
      {
        cwd: rootPath,
      }
    );
  });
};

exports.saveJson = function saveJson(path, json) {
  fs.writeFileSync(path, JSON.stringify(json, null, "  "));
};
exports.loadJson = (path) => JSON.parse(fs.readFileSync(path));

exports.copyFile = function (src, dest) {
  const file = fs.readFileSync(src);
  fs.writeFileSync(dest, file);
};
