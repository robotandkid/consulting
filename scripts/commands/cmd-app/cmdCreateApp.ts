import { Command, Option } from "clipanion";
import child from "child_process";
import path from "path";
import rm from "rimraf";

import {
  bash,
  copyFile,
  copyJsonFile,
  rootPath,
  successStyle,
} from "../helpers";

const deps = [
  "next",
  "react",
  "react-dom",
  "styled-components",
  "framer-motion",
] as const;

const devDeps = [
  // Testing
  "@testing-library/react",
  // MDX
  "@next/mdx",
  "@mdx-js/loader",
  "gray-matter",
  // Cosmos
  "react-cosmos",
  "html-webpack-plugin",
  // Nextjs
  "babel-plugin-styled-components",
] as const;

export class CreateApp extends Command {
  static paths = [[`workspace:create:app`]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  appName = Option.String({ required: true })!;
  withImgix = Option.Boolean("--with-imgix", false);
  withI18n = Option.Boolean("--with-i18n", false);

  async execute() {
    bash(rootPath)`
      lerna create --yes ${this.appName} apps
    `;

    const appDirectory = this.appName.replace(/.*\/(.*)/, "$1");
    const targetDirectory = path.join(rootPath, "apps", appDirectory);

    this.context.stdout.write(
      successStyle(`Adding dev deps for ${this.appName}\n`)
    );

    await this.cli.run(["workspace:add", this.appName, "--dev", ...devDeps]);

    this.context.stdout.write(
      successStyle(`Adding deps for ${this.appName}\n`)
    );

    await this.cli.run(["workspace:add", this.appName, ...deps]);

    this.context.stdout.write(
      successStyle(`Installed deps for ${this.appName}!\n`)
    );

    copyJsonFile(
      this.context.stdout,
      path.join(rootPath, "scripts/templates/tsconfig.json"),
      path.join(targetDirectory, "tsconfig.json"),
      (template) => template
    );

    copyJsonFile(
      this.context.stdout,
      [
        path.join(rootPath, "scripts/templates/apps/package.json"),
        path.join(targetDirectory, "package.json"),
      ],
      path.join(targetDirectory, "package.json"),
      (template, target) => ({
        ...target,
        ...template,
        directories: undefined,
        files: undefined,
        main: undefined,
      })
    );

    copyJsonFile(
      this.context.stdout,
      path.join(rootPath, "scripts/templates/cosmos.config.json"),
      path.join(targetDirectory, "cosmos.config.json"),
      (template) => template
    );

    this.context.stdout.write("Setting up nextjs\n");

    copyFile(
      path.join(rootPath, "scripts/templates/apps/mdx-fm-loader.js"),
      path.join(targetDirectory, "mdx-fm-loader.js")
    );
    copyFile(
      path.join(rootPath, "scripts/templates/apps/next.config.js"),
      path.join(targetDirectory, "next.config.js")
    );

    this.context.stdout.write("Setting up prettier\n");

    copyFile(
      path.join(rootPath, "scripts/templates/.prettierrc.json"),
      path.join(targetDirectory, ".prettierrc.json")
    );

    this.context.stdout.write("Setting up styled-components\n");

    bash(targetDirectory)`
      mkdir -p types &&
      mkdir -p src/styles &&
      cp ${path.join(
        rootPath,
        "scripts/templates/apps/theme.d.ts"
      )} types/theme.d.ts &&
      echo "export const theme = {}" > src/styles/theme.tsx
    `;
    copyJsonFile(
      this.context.stdout,
      path.join(targetDirectory, "tsconfig.json"),
      path.join(targetDirectory, "tsconfig.json"),
      (template: { include?: string[] }) => ({
        ...template,
        include: (template.include || []).concat(["types/theme.d.ts"]),
      })
    );

    this.context.stdout.write("Formatting...\n");

    child.execSync("yarn format", { cwd: targetDirectory });

    this.context.stdout.write("Killing unnecessary files\n");

    rm(path.join(targetDirectory, "lib"), () => undefined);
    rm(path.join(targetDirectory, "__tests__"), () => undefined);

    this.context.stdout.write(successStyle("Done!\n"));
  }
}
