import child from "child_process";
import { Command, Option } from "clipanion";
import { listFiles } from "../helpers";

const mp4 = (name: string, crf: string, width: string) =>
  `ffmpeg -i ${name} -vcodec libx264 -crf ${crf} -vf "scale=${width}:trunc(ow/a/2)*2" /tmp/${name}`;

const mov = (name: string, crf: string, width: string) =>
  `ffmpeg -i ${name} -vcodec libx264 -crf ${crf} -vf "scale=${width}:trunc(ow/a/2)*2" /tmp/${name}.mp4`;

const mp3 = (name: string, bitrate: string, duration: string) =>
  `ffmpeg -i ${name} -ss 0 -t ${duration} -codec:a libmp3lame -b:a ${bitrate} /tmp/${name}`;

const wav = (name: string, bitrate: string, duration: string) =>
  `ffmpeg -i ${name} -ss 0 -t ${duration} -codec:a libmp3lame -b:a ${bitrate} /tmp/${name}.mp3`;

const mv = (src: string) => `mv /tmp/${src} ${src}`;

export class AssetDownsize extends Command {
  static paths = [["asset:downsize"]];
  dirPath = Option.String("--path", { required: true });
  vCrf = Option.String("--vCrf", "32");
  vResizeWidth = Option.String("--vResizeWidth", "512");
  aBitrate = Option.String("--aBitrate", "28k");
  aDuration = Option.String("--aDuration", "62"); // seconds

  static usage = Command.Usage({
    category: `cli`,
    description: `Downsizes all MP4s and MP3s in the given directory`,
    examples: [["", "yarn asset:downsize --path ~/foobar"]],
  });

  async execute() {
    this.context.stdout.write(`Downsizing assets in ${this.dirPath}...\n`);

    this.context.stdout.write(`Downsizing mp4s in ${this.dirPath}...\n`);

    const mp4s = await listFiles(this.dirPath, /.*\.mp4$/i);
    mp4s.forEach((file) => {
      this.context.stdout.write(`Downsizing ${file.filename}`);

      child.execSync(
        `${mp4(file.filename, this.vCrf, this.vResizeWidth)} && ${mv(
          file.filename
        )}`,
        { cwd: this.dirPath }
      );
    });

    const MOVs = await listFiles(this.dirPath, /.*\.mov$/i);
    MOVs.forEach((file) => {
      this.context.stdout.write(`Downsizing ${file.filename}`);

      child.execSync(
        `${mov(file.filename, this.vCrf, this.vResizeWidth)} && ${mv(
          file.filename + ".mp4"
        )}`,
        { cwd: this.dirPath }
      );
    });

    this.context.stdout.write(`Downsizing audio in ${this.dirPath}...\n`);

    const mp3s = await listFiles(this.dirPath, /.*\.mp3$/i);
    mp3s.forEach((file) => {
      this.context.stdout.write(`Downsizing ${file.filename}`);

      child.execSync(
        `${mp3(file.filename, this.aBitrate, this.aDuration)} && ${mv(
          file.filename
        )}`,
        { cwd: this.dirPath }
      );
    });

    const WAVs = await listFiles(this.dirPath, /.*\.wav$/i);
    WAVs.forEach((file) => {
      this.context.stdout.write(`Downsizing ${file.filename}`);

      child.execSync(
        `${wav(file.filename, this.aBitrate, this.aDuration)} && ${mv(
          file.filename + ".mp3"
        )}`,
        { cwd: this.dirPath }
      );
    });

    this.context.stdout.write("Done");
  }
}
