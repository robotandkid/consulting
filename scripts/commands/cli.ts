import { Cli, Builtins } from "clipanion";
import { CreateApp } from "./cmd-app/cmdCreateApp";
import { CreateComponent } from "./cmd-component/cmdCreateComponent";
import { GetInstalledVersion } from "./cmd-package/cmdGetInstalledVersion";
import { AddPackage, AddPackageToRoot } from "./cmd-package/cmdAddPackage";
import { UpgradePackage } from "./cmd-package/cmdUpgradePackage";
import { ListWorkspaces } from "./cli/cmdListWorkspaces";
import { AddCypress } from "./workspace/cypress/cmdAddCypress";
import { LernaChanged } from "./cli/cmdLernaChanged";
import { LernaChangedTests } from "./cli/cmdLernaChangedTests";
import { AddMDXCodeBlocks } from "./workspace/codeblocks/cmdAddMDXCodeBlocks";
import { AddLingui } from "./workspace/lingui/cmdAddLingui";
import { RemoveVitro } from "./workspace/vitro/cmdRemoveVitro";
import { AddVitro } from "./workspace/vitro/cmdAddVitro";
import { GetWorkspacePath } from "./cli/cmdGetWorkspacePath";
import { AddVitest } from "./workspace/vitest/addVitest";
import { RemoveVitest } from "./workspace/vitest/removeVitest";
import { AddTypeSafeI18n } from "./workspace/typesafe-i18n/cmdAddTypesafe-i18n";
import { Help } from "./cmdHelp";
import { AddLinaria } from "./workspace/linaria/cmdAddLinaria";
import { AddNextBundleAnalyzer } from "./workspace/next/bundle-analyzer/cmdAddNextBundleAnalyzer";
import { AddLadle } from "./workspace/ladle/cmdAddLadle";
import { AssetDownsize } from "./assets/cmdAssetDownsize";

const [, , ...args] = process.argv;

const cli = new Cli({
  binaryLabel: `Robotandkid CLI`,
  binaryName: `yarn cli`,
  binaryVersion: `1.0.0`,
});

cli.register(GetInstalledVersion);
cli.register(AddPackage);
cli.register(AddPackageToRoot);
cli.register(CreateApp);
cli.register(CreateComponent);
cli.register(UpgradePackage);
cli.register(ListWorkspaces);
cli.register(AddCypress);
cli.register(LernaChanged);
cli.register(LernaChangedTests);
cli.register(AddMDXCodeBlocks);
cli.register(AddLingui);
cli.register(RemoveVitro);
cli.register(AddVitro);
cli.register(GetWorkspacePath);
cli.register(AddVitest);
cli.register(RemoveVitest);
cli.register(AddTypeSafeI18n);
cli.register(AddLinaria);
cli.register(AddNextBundleAnalyzer);
cli.register(AddLadle);
cli.register(Help);
cli.register(AssetDownsize);
cli.register(Builtins.HelpCommand);
cli.runExit(args, Cli.defaultContext);
