import { Command, Option } from "clipanion";
import path from "path";
import {
  bash,
  copyJsonFile,
  rootPath,
  successStyle,
  withOnceStream,
} from "../../helpers";

interface PackageJson {
  scripts?: Record<string, string>;
  babel?: {
    presets?: string[];
    plugins?: Array<string | Array<any>>;
  };
}

/**
 * Adds lingui support to a nextjs repo
 */
export class AddLingui extends Command {
  static paths = [["workspace:add:lingui"]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  async execute() {
    await this.cli.run([
      "workspace:add",
      this.workspace,
      "--dev",
      "@lingui/cli",
      "babel-plugin-macros",
      "@babel/core",
    ]);

    await this.cli.run([
      "workspace:add",
      this.workspace,
      "@lingui/core",
      "@lingui/react",
      "@lingui/macro",
    ]);

    const [workspaces, setWorkspaces] = withOnceStream();

    await this.cli.run(["workspaces:list"], {
      stdout: setWorkspaces,
    });

    const relLocation = JSON.parse(await workspaces)[this.workspace]?.location;

    if (!relLocation) throw new Error(`${this.workspace} was not found!`);

    const location = path.join(rootPath, relLocation);

    const templates = [
      {
        src: "scripts/templates/lingui/.linguirc",
        target: ".linguirc",
      },
      {
        src: "scripts/templates/lingui/locales/en/messages.json",
        target: "src/locales/en/messages.json",
      },
      {
        src: "scripts/templates/lingui/locales/es/messages.json",
        target: "src/locales/es/messages.json",
      },
    ];

    copyJsonFile(
      this.context.stdout,
      path.join(location, "package.json"),
      path.join(location, "package.json"),
      (json: PackageJson) => {
        json.scripts = json.scripts ?? {};
        json.scripts["lingui:extract"] = "NODE_ENV=test lingui extract";
        json.scripts["lingui:compile"] = "NODE_ENV=test lingui compile";

        json.babel = json.babel ?? {};
        json.babel.plugins = json.babel.plugins ?? [];
        json.babel.plugins.push("macros");

        return json as Record<string, unknown>;
      }
    );

    bash(location)`
      mkdir -p src/locales/en && \
      mkdir -p src/locales/es && \
      ${templates
        .map(({ src, target }) => {
          return `cat ${path.join(rootPath, src)} > ${target}`;
        })
        .join(" && \\\n")}
    `;

    this.context.stdout.write(
      successStyle(`Successfully added lingui to ${this.workspace}!\n`)
    );
    this.context.stdout.write(
      successStyle(`Docs: https://lingui.js.org/tutorials/react.html\n`)
    );
  }
}
