import { Command, Option } from "clipanion";
import {
  bash,
  copyJsonFile,
  infoStyle,
  rootPath,
  successStyle,
  withOnceStream,
} from "../../helpers";
import path from "path";
import chalk from "chalk";

const devDeps = ["@linaria/webpack-loader", "@linaria/babel-preset"];

const deps = ["@linaria/core", "@linaria/react"];

const name = `linaria`;

/**
 * Returns the package/version that is installed.
 */
export class AddLinaria extends Command {
  static paths = [[`workspace:add:${name}`]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  static usage = Command.Usage({
    category: `Next.js`,
    description: `Adds ${name}`,
    details: `
    Note: you'll also need a vscode extension stylelint.
    And also optionally configure this in the repo.
    `,
    examples: [[`yarn cli workspace:add:${name}`, ""]],
  });

  async execute() {
    await this.cli.run(["workspace:add", this.workspace, "--dev", ...devDeps]);
    await this.cli.run(["workspace:add", this.workspace, ...deps]);

    const [workspaces, setWorkspaces] = withOnceStream();

    await this.cli.run(["workspaces:list"], {
      stdout: setWorkspaces,
    });

    const relLocation = JSON.parse(await workspaces)[this.workspace]?.location;

    if (!relLocation) throw new Error(`${this.workspace} was not found!`);

    const location = path.join(rootPath, relLocation);

    this.context.stdout.write(infoStyle(`Adding ${name} to next config...\n`));

    bash(location)`
    node ${path.join(rootPath, "node_modules/.bin/jscodeshift")} -t ${path.join(
      rootPath,
      "scripts/commands/workspace/linaria/jscodeshift.ts"
    )} next.config.js &&
    yarn format
  `;

    this.context.stdout.write(
      infoStyle("Updating babel config in package.json...")
    );

    copyJsonFile(
      this.context.stdout,
      path.join(location, "package.json"),
      path.join(location, "package.json"),
      (json: { babel?: { presets?: string[] } }) => {
        const babel = json.babel || {};
        const presets = babel.presets || [];

        if (!presets.some((p) => p === "@linaria")) presets.push("@linaria");

        babel.presets = presets;
        json.babel = babel;

        return json;
      }
    );

    this.context.stdout.write(
      successStyle(`Successfully added ${name} to ${this.workspace}!\n`)
    );
    this.context.stdout.write(
      chalk.redBright(`Don't forget to install stylelint. \n`)
    );
  }
}
