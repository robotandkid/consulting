import { StatementKind } from "ast-types/gen/kinds";
import core, {
  Transform,
  Property,
  ObjectProperty,
  SpreadElement,
  SpreadProperty,
  ObjectMethod,
  JSCodeshift,
} from "jscodeshift";

/**
 * Step 1. Find config (assume there's a config variable) Step 2. Add
 * webpack rule
 */

type J = typeof core;
type Source = ReturnType<J>;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const findVariableDeclaration = (
  j: JSCodeshift,
  source: Source,
  name: string
) => {
  const vars = source.find(j.VariableDeclarator);

  const match = vars.filter((varDeclarator) => {
    if (varDeclarator.value.type !== "VariableDeclarator") return false;
    if (varDeclarator.value.id.type !== "Identifier") return false;
    if (varDeclarator.value.id.name === name) return true;
  });

  return match.at(0);
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const findProperty = (j: JSCodeshift, source: Source, name: string) => {
  const matches = source.find(j.Property);

  const match = matches.filter((prop) => {
    if (prop.value.type !== "Property") return false;
    if (prop.value.key.type !== "Identifier") return false;
    if (prop.value.key.name === name) return true;
  });

  return match.at(0);
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const addRequireImports = (
  j: JSCodeshift,
  source: Source,
  varName: string,
  pkgName: string
) => {
  source.find(j.Program).forEach((program) => {
    // add requires
    const requireExp = j.variableDeclaration("const", [
      j.variableDeclarator(
        j.identifier(varName),
        j.callExpression(j.identifier("require"), [j.literal(pkgName)])
      ),
    ]);
    program.value.body = [requireExp, ...program.value.body];
  });
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const findPropertyByName =
  (name: string) =>
  (
    property:
      | Property
      | ObjectProperty
      | SpreadElement
      | SpreadProperty
      | ObjectMethod
  ): property is Property => {
    return (
      property.type === "Property" &&
      property.key.type === "Identifier" &&
      property.key.name === name
    );
  };

const traverse = `
function traverse(rules) {
  for (let rule of rules) {
    if (typeof rule.loader === "string" && rule.loader.includes("css-loader")) {
      if (
        rule.options &&
        rule.options.modules &&
        typeof rule.options.modules.getLocalIdent === "function"
      ) {
        let nextGetLocalIdent = rule.options.modules.getLocalIdent;
        rule.options.modules.mode = "local";
        rule.options.modules.auto = true;
        rule.options.modules.exportGlobals = true;
        rule.options.modules.exportOnlyLocals = false;
        rule.options.modules.getLocalIdent = (
          context,
          _,
          exportName,
          options
        ) => {
          if (context.resourcePath.includes(".linearia.module.css")) {
            return exportName;
          }
          return nextGetLocalIdent(context, _, exportName, options);
        };
      }
    }
    if (typeof rule.use === "object") {
      traverse(Array.isArray(rule.use) ? rule.use : [rule.use]);
    }
    if (Array.isArray(rule.oneOf)) {
      traverse(rule.oneOf);
    }
  }
}
`;

const withLinaria = `
const withLinaria = (nextConfig = {}) => {
  return {
    ...nextConfig,
    webpack(config, options) {
      traverse(config.module.rules);

      config.module.rules.push({
        test: /\\.(ts|tsx|js|mjs|jsx)$/,
        use: [
          {
            loader: require.resolve("@linaria/webpack-loader"),
            options: {
              sourceMap: process.env.NODE_ENV !== "production",
              extension: ".linearia.module.css",
            },
          },
        ],
      });

      if (typeof nextConfig.webpack === "function") {
        return nextConfig.webpack(config, options);
      }
      return config;
    },
  };
};
`;

const transform: Transform = (fileInfo, api) => {
  if (!fileInfo.path.match(/next\.config\.js$/)) return null;

  const j = api.jscodeshift;
  const source = j(fileInfo.source);

  const traverseSource = j(traverse);
  let traverseBody: StatementKind[] | undefined;
  traverseSource.find(j.Program).forEach((program) => {
    traverseBody = program.value.body;
  });

  const withSource = j(withLinaria);
  let withBody: StatementKind[] | undefined;
  withSource.find(j.Program).forEach((program) => {
    withBody = program.value.body;
  });

  source.find(j.Program).forEach((program) => {
    program.value.body = [...traverseBody, ...withBody, ...program.value.body];
  });

  source.find(j.AssignmentExpression).forEach((assignExpr) => {
    if (assignExpr.value.left.type !== "MemberExpression") return;
    if (assignExpr.value.left.object.type !== "Identifier") return;
    if (assignExpr.value.left.object.name !== "module") return;
    if (assignExpr.value.left.property.type !== "Identifier") return;
    if (assignExpr.value.left.property.name !== "exports") return;

    assignExpr.value.right = j.callExpression(j.identifier("withLinaria"), [
      assignExpr.value.right,
    ]);
  });

  return source.toSource();
};

module.exports = transform;
