import { Command, Option } from "clipanion";
import path from "path";
import { bash, rootPath, successStyle } from "../../helpers";

const devDeps = [
  "jscodeshift",
  "@types/jscodeshift",
  "@mapbox/rehype-prism",
] as const;

export class AddMDXCodeBlocks extends Command {
  static paths = [[`workspace:add:MDXCodeBlocks`]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  appName = Option.String({ required: true })!;

  async execute() {
    await this.cli.run(["workspace:add", this.appName, "--dev", ...devDeps]);

    const appDirectory = this.appName.replace(/.*\/(.*)/, "$1");
    const targetDirectory = path.join(rootPath, "apps", appDirectory);

    this.context.stdout.write("Adding plugin to existing next.config\n");

    bash(targetDirectory)`
      node ${path.join(
        rootPath,
        "node_modules/.bin/jscodeshift"
      )} -t ${path.join(
      rootPath,
      "scripts/commands/workspace/codeblocks/jscodeshift-highlight-code.ts"
    )} next.config.js &&
      yarn format
    `;

    this.context.stdout.write(successStyle("Done!\n"));
  }
}
