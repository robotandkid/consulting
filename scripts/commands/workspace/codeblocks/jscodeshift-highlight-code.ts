import {
  Transform,
  Property,
  ObjectProperty,
  SpreadElement,
  SpreadProperty,
  ObjectMethod,
} from "jscodeshift";

const findPropertyByName =
  (name: string) =>
  (
    property:
      | Property
      | ObjectProperty
      | SpreadElement
      | SpreadProperty
      | ObjectMethod
  ): property is Property => {
    return (
      property.type === "Property" &&
      property.key.type === "Identifier" &&
      property.key.name === name
    );
  };

const pluginVariableName = "rehypePrism";
const pluginPackageName = "@mapbox/rehype-prism";

const transform: Transform = (fileInfo, api) => {
  if (!fileInfo.path.match(/next\.config\.js$/)) return null;

  const j = api.jscodeshift;
  const source = j(fileInfo.source);

  source.find(j.Program).forEach((program) => {
    // add requires
    const MDXRequire = j.variableDeclaration("const", [
      j.variableDeclarator(
        j.identifier(pluginVariableName),
        j.callExpression(j.identifier("require"), [
          j.literal(pluginPackageName),
        ])
      ),
    ]);
    program.value.body = [MDXRequire, ...program.value.body];
  });

  source.find(j.CallExpression).forEach((callExpression) => {
    if (callExpression.value.callee.type !== "CallExpression") return;

    const exp = callExpression.value.callee;
    const isRequire =
      exp.callee.type === "Identifier" && exp.callee.name === "require";
    const isMdx =
      exp.arguments[0]?.type === "Literal" &&
      exp.arguments[0]?.value === "@next/mdx";

    if (!isRequire || !isMdx) return;

    const arg = callExpression.value.arguments[0];

    if (arg?.type !== "ObjectExpression") return;

    let options = arg.properties.find(findPropertyByName("options"));

    if (!options) {
      options = j.property(
        "init",
        j.identifier("options"),
        j.objectExpression([])
      );

      arg.properties.push(options);
    }

    if (options.type !== "Property") return;

    const properties = options.value;

    if (properties.type !== "ObjectExpression") return;

    let rehypePlugins = properties.properties.find(
      findPropertyByName("rehypePlugins")
    );

    if (!rehypePlugins) {
      rehypePlugins = j.property(
        "init",
        j.identifier("rehypePlugins"),
        j.arrayExpression([])
      );

      properties.properties.push(rehypePlugins);
    }

    if (rehypePlugins.value.type !== "ArrayExpression") return;

    rehypePlugins.value.elements.push(j.identifier(pluginVariableName));
  });

  return source.toSource();
};

module.exports = transform;
