import { Command, Option } from "clipanion";
import {
  bash,
  infoStyle,
  rootPath,
  successStyle,
  withOnceStream,
} from "../../../helpers";
import path from "path";

const deps = ["@next/bundle-analyzer"];
const name = "@next/bundle-analyzer";

/**
 * Returns the package/version that is installed.
 */
export class AddNextBundleAnalyzer extends Command {
  static paths = [[`workspace:add:${name}`]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  static usage = Command.Usage({
    category: `Next.js`,
    description: `Adds ${name}`,
    examples: [[`yarn cli workspace:add:${name}`, ""]],
  });

  async execute() {
    await this.cli.run(["workspace:add", this.workspace, "--dev", ...deps]);

    const [workspaces, setWorkspaces] = withOnceStream();

    await this.cli.run(["workspaces:list"], {
      stdout: setWorkspaces,
    });

    const relLocation = JSON.parse(await workspaces)[this.workspace]?.location;

    if (!relLocation) throw new Error(`${this.workspace} was not found!`);

    const location = path.join(rootPath, relLocation);

    this.context.stdout.write(infoStyle(`Adding ${name} to next config...\n`));

    bash(location)`
    node ${path.join(rootPath, "node_modules/.bin/jscodeshift")} -t ${path.join(
      rootPath,
      "scripts/commands/workspace/next/bundle-analyzer/jscodeshift.ts"
    )} next.config.js &&
    yarn format
  `;

    this.context.stdout.write(
      successStyle(`Successfully added ${name} to ${this.workspace}!\n`)
    );
  }
}
