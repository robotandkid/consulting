import { StatementKind } from "ast-types/gen/kinds";
import core, {
  Transform,
  Property,
  ObjectProperty,
  SpreadElement,
  SpreadProperty,
  ObjectMethod,
  JSCodeshift,
} from "jscodeshift";

/**
 * Step 1. Find config (assume there's a config variable) Step 2. Add
 * webpack rule
 */

type J = typeof core;
type Source = ReturnType<J>;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const findVariableDeclaration = (
  j: JSCodeshift,
  source: Source,
  name: string
) => {
  const vars = source.find(j.VariableDeclarator);

  const match = vars.filter((varDeclarator) => {
    if (varDeclarator.value.type !== "VariableDeclarator") return false;
    if (varDeclarator.value.id.type !== "Identifier") return false;
    if (varDeclarator.value.id.name === name) return true;
  });

  return match.at(0);
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const findProperty = (j: JSCodeshift, source: Source, name: string) => {
  const matches = source.find(j.Property);

  const match = matches.filter((prop) => {
    if (prop.value.type !== "Property") return false;
    if (prop.value.key.type !== "Identifier") return false;
    if (prop.value.key.name === name) return true;
  });

  return match.at(0);
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const addRequireImports = (
  j: JSCodeshift,
  source: Source,
  varName: string,
  pkgName: string
) => {
  source.find(j.Program).forEach((program) => {
    // add requires
    const requireExp = j.variableDeclaration("const", [
      j.variableDeclarator(
        j.identifier(varName),
        j.callExpression(j.identifier("require"), [j.literal(pkgName)])
      ),
    ]);
    program.value.body = [requireExp, ...program.value.body];
  });
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const findPropertyByName =
  (name: string) =>
  (
    property:
      | Property
      | ObjectProperty
      | SpreadElement
      | SpreadProperty
      | ObjectMethod
  ): property is Property => {
    return (
      property.type === "Property" &&
      property.key.type === "Identifier" &&
      property.key.name === name
    );
  };

const transform: Transform = (fileInfo, api) => {
  if (!fileInfo.path.match(/next\.config\.js$/)) return null;

  const j = api.jscodeshift;
  const source = j(fileInfo.source);

  const varName = "withBundleAnalyzer";
  const pkgName = "@next/bundle-analyzer";

  const expr = `
  const ${varName} = require(${pkgName})({
    enabled: process.env.ANALYZE === "true",
  });
  `;

  // add the imports

  const exprSource = j(expr);

  source.find(j.Program).forEach((program) => {
    let exprBody: StatementKind[] | undefined;

    exprSource.find(j.Program).forEach((program) => {
      exprBody = program.value.body;
    });

    program.value.body = [...exprBody, ...program.value.body];
  });

  // wrap the config with the plugin

  source.find(j.AssignmentExpression).forEach((assignExpr) => {
    if (assignExpr.value.left.type !== "MemberExpression") return;
    if (assignExpr.value.left.object.type !== "Identifier") return;
    if (assignExpr.value.left.object.name !== "module") return;
    if (assignExpr.value.left.property.type !== "Identifier") return;
    if (assignExpr.value.left.property.name !== "exports") return;

    assignExpr.value.right = j.callExpression(j.identifier(varName), [
      assignExpr.value.right,
    ]);
  });

  return source.toSource();
};

module.exports = transform;
