import { Command, Option } from "clipanion";
import path from "path";
import {
  bash,
  copyJsonFile,
  EslintJson,
  successStyle,
  TsconfigJson,
  withOnceStream,
} from "../../helpers";
import { vitestDeps } from "./addVitest";

export class RemoveVitest extends Command {
  static paths = [["workspace:remove:vitest"]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  async execute() {
    await this.cli.run(["workspace:remove", this.workspace, ...vitestDeps]);

    const [locationPromise, setLocation] = withOnceStream();

    await this.cli.run(["workspace:get:path", this.workspace], {
      stdout: setLocation,
    });

    const location = await locationPromise;

    copyJsonFile(
      this.context.stdout,
      path.join(location, "package.json"),
      path.join(location, "package.json"),
      (json: PackageJson) => {
        json.scripts = json.scripts ?? {};
        delete json.scripts["test"];

        return json as Record<string, any>;
      }
    );

    copyJsonFile(
      this.context.stdout,
      path.join(location, "tsconfig.json"),
      path.join(location, "tsconfig.json"),
      (json: TsconfigJson) => {
        json.compilerOptions = json.compilerOptions ?? {};
        json.compilerOptions.types = (json.compilerOptions.types ?? []).filter(
          (_type) => _type !== "vitest/globals"
        );

        return json as Record<string, any>;
      }
    );

    copyJsonFile(
      this.context.stdout,
      path.join(location, ".eslintrc.json"),
      path.join(location, ".eslintrc.json"),
      (json: EslintJson) => {
        json.plugins = json.plugins ?? [];
        json.plugins = json.plugins.filter(
          (plugin) => plugin !== "testing-library"
        );

        return json as Record<string, any>;
      }
    );

    bash(location)`rm vitest.config.ts`;
    bash(location)`rm -rf test_infra`;

    this.context.stdout.write(
      successStyle(`Successfully removed vitest from ${this.workspace}!\n`)
    );
  }
}
