import { Command, Option } from "clipanion";
import path from "path";
import {
  copyFile,
  copyJsonFile,
  EslintJson,
  rootBash,
  successStyle,
  TsconfigJson,
  withOnceStream,
} from "../../helpers";

const thisDirectory = `${__dirname}`.replace(/^.*bin\//, "scripts/commands/");

const templateVitestConfig = `${thisDirectory}/vitest.config.ts`;
const testInfraDir = `${thisDirectory}/test_infra`;

export const vitestDeps = [
  "vitest",
  "@testing-library/react",
  "eslint-plugin-testing-library",
  "@testing-library/user-event",
  "@testing-library/jest-dom",
  "jest-webgl-canvas-mock",
] as const;

export class AddVitest extends Command {
  static paths = [["workspace:add:vitest"]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  async execute() {
    await this.cli.run([
      "workspace:add",
      this.workspace,
      "--dev",
      ...vitestDeps,
    ]);

    const [locationPromise, setLocation] = withOnceStream();

    await this.cli.run(["workspace:get:path", this.workspace], {
      stdout: setLocation,
    });

    const location = await locationPromise;

    copyFile(
      templateVitestConfig,
      path.join(location, "vitest.config.ts"),
      this.context.stdout
    );

    copyJsonFile(
      this.context.stdout,
      path.join(location, "package.json"),
      path.join(location, "package.json"),
      (json: PackageJson) => {
        json.scripts = json.scripts ?? {};
        json.scripts["test"] = "vitest";

        return json as Record<string, any>;
      }
    );

    copyJsonFile(
      this.context.stdout,
      path.join(location, "tsconfig.json"),
      path.join(location, "tsconfig.json"),
      (json: TsconfigJson) => {
        json.compilerOptions = json.compilerOptions ?? {};
        json.compilerOptions.types = json.compilerOptions.types ?? [];
        if (json.compilerOptions.types.indexOf("vitest/globals") === -1)
          json.compilerOptions.types.push("vitest/globals");

        return json as Record<string, any>;
      }
    );

    copyJsonFile(
      this.context.stdout,
      path.join(location, ".eslintrc.json"),
      path.join(location, ".eslintrc.json"),
      (json: EslintJson) => {
        json.plugins = json.plugins ?? [];
        if (json.plugins.indexOf("testing-library") === -1)
          json.plugins.push("testing-library");

        return json as Record<string, any>;
      }
    );

    rootBash`
      cp -r ${testInfraDir} ${location}
    `;

    this.context.stdout.write(
      successStyle(`Successfully added vitest to ${this.workspace}!\n`)
    );

    this.context.stdout.write(
      `
    You'll most likely want to configure the eslint plugin:\n https://github.com/testing-library/eslint-plugin-testing-library`.trim()
    );
  }
}
