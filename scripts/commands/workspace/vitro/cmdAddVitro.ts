import { Command, Option } from "clipanion";
import path from "path";
import {
  bash,
  copyJsonFile,
  rootBash,
  successStyle,
  withOnceStream,
} from "../../helpers";

interface PackageJson {
  scripts?: Record<string, string>;
}

export class AddVitro extends Command {
  static paths = [["workspace:add:vitro"]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  async execute() {
    await this.cli.run([
      "workspace:add",
      this.workspace,
      "--dev",
      "react",
      "react-dom",
    ]);

    const [locationPromise, setLocation] = withOnceStream();

    await this.cli.run(["workspace:get:path", this.workspace], {
      stdout: setLocation,
    });

    const location = await locationPromise;

    rootBash`yarn global add @vitro/cli`;
    bash(location)`vitro init && yarn format`;

    copyJsonFile(
      this.context.stdout,
      path.join(location, "package.json"),
      path.join(location, "package.json"),
      (json: PackageJson) => {
        json.scripts = json.scripts ?? {};
        json.scripts["vitro"] = "vitro dev";

        return json as Record<string, any>;
      }
    );

    this.context.stdout.write(
      successStyle(`Successfully added vitro to ${this.workspace}!\n`)
    );
    this.context.stdout.write(
      successStyle(`Docs: https://vitro.vercel.app/docs\n`)
    );
  }
}
