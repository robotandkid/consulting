import { Command, Option } from "clipanion";
import path from "path";
import { copyJsonFile, successStyle, withOnceStream } from "../../helpers";

interface PackageJson {
  scripts?: Record<string, string>;
}

export class RemoveVitro extends Command {
  static paths = [["workspace:remove:vitro"]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  async execute() {
    const [locationPromise, setLocation] = withOnceStream();

    await this.cli.run(["workspace:get:path", this.workspace], {
      stdout: setLocation,
    });

    const location = await locationPromise;

    copyJsonFile(
      this.context.stdout,
      path.join(location, "package.json"),
      path.join(location, "package.json"),
      (json: PackageJson) => {
        json.scripts = json.scripts ?? {};
        delete json.scripts["vitro"];

        return json as Record<string, any>;
      }
    );

    this.context.stdout.write(
      successStyle(`Successfully removed vitro to ${this.workspace}!\n`)
    );
  }
}
