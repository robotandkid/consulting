import { Command, Option } from "clipanion";
import {
  bash,
  copyJsonFile,
  infoStyle,
  rootPath,
  successStyle,
  withOnceStream,
} from "../../helpers";
import path from "path";
import fs from "fs";

const name = "ladle";

const thisPath = path
  .resolve(__filename.replace(/[a-zA-Z]+\.js$/, ""))
  .replace("/bin/", "/commands/");

/**
 * Returns the package/version that is installed.
 */
export class AddLadle extends Command {
  static paths = [[`workspace:add:${name}`]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  static usage = Command.Usage({
    category: `Component Explorers`,
    description: `Adds ${name}`,
    examples: [[`yarn cli workspace:add:${name}`, ""]],
  });

  async execute() {
    await this.cli.run([
      "workspace:add",
      this.workspace,
      "--dev",
      "@ladle/react",
    ]);

    const [workspaces, setWorkspaces] = withOnceStream();

    await this.cli.run(["workspaces:list"], {
      stdout: setWorkspaces,
    });

    const relLocation = JSON.parse(await workspaces)[this.workspace]?.location;
    if (!relLocation) throw new Error(`${this.workspace} was not found!`);
    const workspaceLocation = path.join(rootPath, relLocation);

    this.context.stdout.write(infoStyle("Adding config file\n"));

    const configFile = fs
      .readFileSync(path.join(thisPath, "./config.mjs"))
      .toString();

    bash(workspaceLocation)`
      mkdir .ladle && \
        echo '${configFile}' > .ladle/config.mjs;
    `;

    this.context.stdout.write(infoStyle("Adding yarn script\n"));

    copyJsonFile(
      this.context.stdout,
      path.join(workspaceLocation, "package.json"),
      path.join(workspaceLocation, "package.json"),
      (json: { scripts?: Record<string, string> }) => {
        const scripts = json.scripts || {};
        scripts.ladle = "ladle serve";

        const newScripts = {
          ...scripts,
        };

        json.scripts = newScripts;
        return json;
      }
    );

    this.context.stdout.write(
      successStyle(`Successfully added ${name} to ${this.workspace}!\n`)
    );
  }
}
