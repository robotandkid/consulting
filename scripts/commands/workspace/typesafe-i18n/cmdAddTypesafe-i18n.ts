import { Command, Option } from "clipanion";
import { bash, rootPath, successStyle, withOnceStream } from "../../helpers";
import path from "path";

/**
 * Returns the package/version that is installed.
 */
export class AddTypeSafeI18n extends Command {
  static paths = [["workspace:add:typesafe-i18n"]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  async execute() {
    await this.cli.run(["workspace:add", this.workspace, "typesafe-i18n"]);

    const [workspaces, setWorkspaces] = withOnceStream();

    await this.cli.run(["workspaces:list"], {
      stdout: setWorkspaces,
    });

    const relLocation = JSON.parse(await workspaces)[this.workspace]?.location;

    if (!relLocation) throw new Error(`${this.workspace} was not found!`);

    const location = path.join(rootPath, relLocation);

    // const templates = [
    //   {
    //     src: "scripts/commands/workspace/cypress/tsconfig.json",
    //     target: "cypress/tsconfig.json",
    //   },
    //   {
    //     src: "scripts/commands/workspace/cypress/cypress.config.ts.txt",
    //     target: "cypress.config.ts",
    //   },
    //   {
    //     src: "scripts/commands/workspace/cypress/e2e.ts.txt",
    //     target: "cypress/support/e2e.ts",
    //   },
    //   {
    //     src: "scripts/commands/workspace/cypress/.eslintrc.json",
    //     target: "cypress/.eslintrc.json",
    //   },
    // ];

    // copyJsonFile(
    //   this.context.stdout,
    //   path.join(location, "package.json"),
    //   path.join(location, "package.json"),
    //   (json: { scripts?: Record<string, string> }) => {
    //     const scripts = json.scripts || {};

    //     Object.keys(cypressPackageJson).forEach((script) => {
    //       if (scripts[script])
    //         this.context.stdout.write(
    //           errorStyle(`Script ${script} already exists in package.json`)
    //         );
    //     });

    //     const newScripts = {
    //       ...scripts,
    //       ...cypressPackageJson.scripts,
    //     };

    //     json.scripts = newScripts;
    //     return json;
    //   }
    // );

    bash(location)`
      yarn typesafe-i18n --setup
    
    `;
    //   mkdir -p cypress && \
    //   mkdir -p cypress/support && \
    //   ${templates
    //     .map(({ src, target }) => {
    //       return `cat ${path.join(rootPath, src)} > ${target}`;
    //     })
    //     .join(" && \\\n")}

    this.context.stdout.write(
      successStyle(`Successfully added typesafe-i18n to ${this.workspace}!\n`)
    );
  }
}
