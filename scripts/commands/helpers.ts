import chalk from "chalk";
import fs from "fs";
import stream from "stream";
import child from "child_process";
import path from "path";

export const rootPath = process.env.PWD || "";

export const errorStyle = chalk.bold.red;
export const successStyle = chalk.bold.green;
export const infoStyle = chalk.bold.blue;

/**
 * @deprecated  Most likely use #copyJsonFile instead
 */
export function saveJson(
  path: string,
  json: Record<string, unknown>,
  stdout?: stream.Writable
) {
  if (stdout) stdout.write(infoStyle(`Saving ${path}\n`));
  fs.writeFileSync(path, JSON.stringify(json, null, "  "));
}

/**
 * @deprecated  Most likely use #copyJsonFile instead
 */
export const loadJson = (path: string) =>
  JSON.parse(fs.readFileSync(path).toString());

export function copyFile(src: string, dest: string, stdout?: stream.Writable) {
  if (stdout) stdout.write(infoStyle(`Copying ${src} to ${dest}\n`));
  const file = fs.readFileSync(src);
  fs.writeFileSync(dest, file);
}

const defaultTransform = (
  ...args: Record<string, unknown>[]
): Record<string, unknown> => {
  return args.reduce((total, cur) => ({ ...total, ...cur }), {});
};

export function copyJsonFile(
  stdout: stream.Writable,
  sources: string | [string],
  target: string,
  transform: (file: Record<string, unknown>) => Record<string, unknown>
): void;

export function copyJsonFile(
  stdout: stream.Writable,
  sources: string | [string, string],
  target: string,
  transform: (
    file1: Record<string, unknown>,
    file2: Record<string, unknown>
  ) => Record<string, unknown>
): void;

export function copyJsonFile(
  stdout: stream.Writable,
  sources: string | string[],
  target: string,
  transform: (
    ...args: Record<string, unknown>[]
  ) => Record<string, unknown> = defaultTransform
) {
  const filename = target.replace(/.*\/(.*)/, "$1");

  stdout.write(infoStyle(`Copying ${filename}\n`));

  const _sources = Array.isArray(sources) ? sources : [sources];

  const json: Record<string, unknown>[] = _sources.map((src) =>
    JSON.parse(fs.readFileSync(src).toString())
  );

  fs.writeFileSync(target, JSON.stringify(transform(...json), null, "  "));
  stdout.write(successStyle(`Done copying ${filename}!\n`));
}

export const bash =
  (cwd: string = rootPath) =>
  (command: TemplateStringsArray, ...values: string[]) => {
    const str = command.reduce(
      (total, cmd, index) => (total += `${cmd}${values[index] ?? ""}`),
      ""
    );

    return child.execSync(str, {
      cwd,
    });
  };

export const rootBash = bash();

/**
 * Returns a stream and a promise that resolves with the first value
 * of the stream. (Note that this promise resolves only once :-) )
 */
export function withOnceStream(): [Promise<string>, stream.Writable] {
  let _resolve: (value: string) => void;

  const _stream = new stream.Writable({
    write: function (chunk, _, next) {
      _resolve(chunk.toString().trim());
      next();
    },
  });

  return [
    new Promise((resolve) => {
      _resolve = resolve;
    }),
    _stream,
  ];
}

export interface PackageJson {
  scripts?: Record<string, string>;
}

export interface TsconfigJson {
  compilerOptions?: {
    types?: string[];
  };
}

export interface EslintJson {
  rules?: Record<string, unknown>;
  plugins?: string[];
}

export async function listFiles(directoryPath: string, regex: RegExp) {
  const files = fs.readdirSync(directoryPath);
  const output: Array<{ filePath: string; filename: string }> = [];

  for (const file of files) {
    const filePath = path.join(directoryPath, file);

    if (regex.test(file)) {
      output.push({ filePath, filename: file });
    }
  }

  return output;
}
