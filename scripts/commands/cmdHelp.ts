import chalk from "chalk";
import { Command } from "clipanion";
import { GetWorkspacePath } from "./cli/cmdGetWorkspacePath";
import { LernaChanged } from "./cli/cmdLernaChanged";
import { LernaChangedTests } from "./cli/cmdLernaChangedTests";
import { ListWorkspaces } from "./cli/cmdListWorkspaces";
import { CreateApp } from "./cmd-app/cmdCreateApp";
import { CreateComponent } from "./cmd-component/cmdCreateComponent";
import { AddPackage } from "./cmd-package/cmdAddPackage";
import { GetInstalledVersion } from "./cmd-package/cmdGetInstalledVersion";
import { UpgradePackage } from "./cmd-package/cmdUpgradePackage";
import { infoStyle } from "./helpers";
import { AddNextBundleAnalyzer } from "./workspace/next/bundle-analyzer/cmdAddNextBundleAnalyzer";
import { AddMDXCodeBlocks } from "./workspace/codeblocks/cmdAddMDXCodeBlocks";
import { AddCypress } from "./workspace/cypress/cmdAddCypress";
import { AddLinaria } from "./workspace/linaria/cmdAddLinaria";
import { AddLingui } from "./workspace/lingui/cmdAddLingui";
import { AddVitest } from "./workspace/vitest/addVitest";
import { RemoveVitest } from "./workspace/vitest/removeVitest";
import { AddVitro } from "./workspace/vitro/cmdAddVitro";
import { RemoveVitro } from "./workspace/vitro/cmdRemoveVitro";
import { AddLadle } from "./workspace/ladle/cmdAddLadle";
import { AssetDownsize } from "./assets/cmdAssetDownsize";

export class Help extends Command {
  static paths = [Command.Default, ["list"]];

  static usage = Command.Usage({
    category: `Help`,
    description: `Lists available commands`,
    details: `
      List available commands
    `,
    examples: [
      ["yarn cli list", ""],
      ["yarn cli", ""],
    ],
  });

  async execute() {
    this.context.stdout.write(infoStyle("Available commands:\n"));
    this.context.stdout.write(infoStyle("--------------------\n\n"));

    const commands = [
      GetWorkspacePath,
      LernaChanged,
      LernaChangedTests,
      ListWorkspaces,
      CreateApp,
      CreateComponent,
      AddPackage,
      GetInstalledVersion,
      UpgradePackage,
      AddMDXCodeBlocks,
      AddCypress,
      AddLingui,
      AddVitest,
      RemoveVitest,
      AddVitro,
      RemoveVitro,
      AddLinaria,
      AddNextBundleAnalyzer,
      AddLadle,
      AssetDownsize,
    ];

    const sortedCommands = commands.sort((cmd1, cmd2) => {
      const path1 = cmd1.paths.join(",");
      const path2 = cmd2.paths.join(",");
      return path1 < path2 ? -1 : path1 === path2 ? 0 : 1;
    });

    sortedCommands.forEach((cmdClass, line) => {
      const color = line % 2 === 0 ? chalk.yellow : chalk.redBright;
      this.context.stdout.write(`  ${color(cmdClass.paths.join(","))}`);
      if (cmdClass.usage?.description)
        this.context.stdout.write(` - ${cmdClass.usage?.description}`);
      this.context.stdout.write("\n\n");
    });
    // Flush this shit 🤷
    // eslint-disable-next-line no-console
    console.log("---------------");
  }
}
