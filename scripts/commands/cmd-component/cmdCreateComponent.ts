import { Command, Option } from "clipanion";
import child from "child_process";
import path from "path";

import {
  bash,
  copyFile,
  copyJsonFile,
  rootPath,
  saveJson,
  successStyle,
} from "../helpers";

const peerDeps = [
  "react",
  "react-dom",
  "styled-components",
  "framer-motion",
] as const;

const devDeps = [
  "@testing-library/react",
  // rollup
  "@rollup/plugin-node-resolve",
  "@rollup/plugin-typescript",
  "rollup-plugin-terser",
  "acorn-jsx",
  "rollup",
  "tslib",
] as const;

import templateEslintConfig from "./.eslintrc.json";
import templateTsTypesConfig from "./tsconfig.types.json";

const thisDirectory = "scripts/commands/cmd-component";

const templatePackageJson = `${thisDirectory}/package.json`;
const templateRollupConfig = `${thisDirectory}/rollup.config.js`;
const templateEntryPoint = `${thisDirectory}/index.ts`;
const sharedTemplatePrettierConfig = "scripts/templates/.prettierrc.json";
const sharedTemplateTsconfig = "scripts/templates/tsconfig.json";

export class CreateComponent extends Command {
  static paths = [[`workspace:create:component`]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  componentName = Option.String({ required: true })!;

  async execute() {
    child.execSync(`lerna create --yes ${this.componentName} components`, {
      cwd: rootPath,
    });

    const compDirectory = this.componentName.replace(/.*\/(.*)/, "$1");
    const targetDirectory = path.join(rootPath, "components", compDirectory);

    this.context.stdout.write(
      successStyle(`Adding dev deps for ${this.componentName}\n`)
    );

    await this.cli.run([
      "workspace:add",
      this.componentName,
      "--dev",
      ...devDeps,
    ]);

    this.context.stdout.write(
      successStyle(`Adding peer deps for ${this.componentName}\n`)
    );

    await this.cli.run([
      "workspace:add",
      this.componentName,
      "--peer",
      ...peerDeps,
    ]);

    this.context.stdout.write(
      successStyle(`Installed deps for ${this.componentName}!\n`)
    );

    saveJson(
      path.join(targetDirectory, ".eslintrc.json"),
      templateEslintConfig,
      this.context.stdout
    );

    copyFile(
      templateRollupConfig,
      path.join(targetDirectory, "rollup.config.js"),
      this.context.stdout
    );

    saveJson(
      path.join(targetDirectory, "tsconfig.types.json"),
      templateTsTypesConfig,
      this.context.stdout
    );

    copyFile(
      path.join(rootPath, sharedTemplateTsconfig),
      path.join(targetDirectory, "tsconfig.json"),
      this.context.stdout
    );

    copyJsonFile(
      this.context.stdout,
      [templatePackageJson, path.join(targetDirectory, "package.json")],
      path.join(targetDirectory, "package.json"),
      (template, lerna) => ({
        ...lerna,
        ...template,
        // override lerna defaults
        directories: undefined,
      })
    );

    this.context.stdout.write("Setting up prettier\n");

    copyFile(
      path.join(rootPath, sharedTemplatePrettierConfig),
      path.join(targetDirectory, ".prettierrc.json")
    );

    copyFile(templateEntryPoint, path.join(targetDirectory, "index.ts"));

    this.context.stdout.write("Cleanup...\n");

    bash(targetDirectory)`
      yarn format && \
      rm -rf lib && \
      rm -rf __tests__ && \
      touch index.ts && \
      mkdir src
  `;

    this.context.stdout.write(successStyle("Done!\n"));
  }
}
