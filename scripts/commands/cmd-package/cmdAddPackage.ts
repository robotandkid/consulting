import child from "child_process";
import { Command, Option } from "clipanion";
import stream from "stream";
import { rootPath } from "../helpers";

/**
 * Installs the current version of a package that is currently
 * installed in this repo. If a package is currently not installed, it
 * will install the latest version.
 */
export class AddPackage extends Command {
  static paths = [["workspace:add"]];
  workspace = Option.String({ required: true });
  devInstall = Option.Boolean("--dev", false);
  peerInstall = Option.Boolean("--peer", false);
  packages = Option.Rest();

  async execute() {
    const pkgNames: string[] = new Array(this.packages.length);

    await Promise.all([
      this.packages.map((name, index) =>
        this.cli.run(["get-installed-version", name], {
          stdout: new stream.Writable({
            write: function (chunk, _, next) {
              pkgNames[index] = chunk.toString().trim();
              next();
            },
          }),
        })
      ),
    ]);

    this.context.stdout.write(
      `Adding ${pkgNames.join(", ")} to ${this.workspace}...\n`
    );

    const dev = this.devInstall ? "--dev" : "";
    const peer = this.peerInstall ? "--peer" : "";

    child.execSync(
      `yarn workspace ${
        this.workspace
      } add --exact ${dev} ${peer} ${pkgNames.join(" ")}`,
      {
        cwd: rootPath,
      }
    );
  }
}

export class AddPackageToRoot extends Command {
  static paths = [["root:add"]];
  devInstall = Option.Boolean("--dev", false);
  peerInstall = Option.Boolean("--peer", false);
  packages = Option.Rest();

  async execute() {
    const pkgNames: string[] = new Array(this.packages.length);

    await Promise.all([
      this.packages.map((name, index) =>
        this.cli.run(["get-installed-version", name], {
          stdout: new stream.Writable({
            write: function (chunk, _, next) {
              pkgNames[index] = chunk.toString().trim();
              next();
            },
          }),
        })
      ),
    ]);

    this.context.stdout.write(`Adding ${pkgNames.join(", ")}...\n`);

    const dev = this.devInstall ? "--dev" : "";
    const peer = this.peerInstall ? "--peer" : "";

    child.execSync(`yarn add -W --exact ${dev} ${peer} ${pkgNames.join(" ")}`, {
      cwd: rootPath,
    });
  }
}
