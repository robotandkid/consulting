import child from "child_process";
import { Command, Option } from "clipanion";

import { rootPath } from "../helpers";

/**
 * Returns the package/version that is installed.
 */
export class GetInstalledVersion extends Command {
  static paths = [[`get-installed-version`]];
  package = Option.String({ required: true });

  async execute() {
    const buffer = child.execSync(
      `yarn list --depth 0 --pattern ${this.package}`,
      {
        cwd: rootPath,
      }
    );

    const packageRegexp = new RegExp(`^.* (${this.package}@.*).*$`);
    const result = buffer.toString().split("\n");
    const pkgMatch = result.filter((line) => packageRegexp.test(line));

    if (pkgMatch.length >= 2)
      throw new Error(
        `${this.package} has two versions: ${pkgMatch.join(", ")}`
      );

    if (pkgMatch.length === 1) {
      this.context.stdout.write(
        `${pkgMatch[0].replace(packageRegexp, "$1")}\n`
      );
    } else {
      this.context.stdout.write(`${this.package}\n`);
    }
  }
}
