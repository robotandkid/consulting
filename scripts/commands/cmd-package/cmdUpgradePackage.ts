import child from "child_process";
import path from "path";
import { Command, Option } from "clipanion";
import * as t from "typanion";

import { infoStyle, loadJson, rootPath, saveJson } from "../helpers";

/**
 * Returns the package/version that is installed.
 */
export class UpgradePackage extends Command {
  static paths = [["workspaces:upgrade"]];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  packageWithVersion = Option.String({
    required: true,
    validator: t.applyCascade(t.isString(), [t.matchesRegExp(/.+@.+/)]),
  })!;

  async execute() {
    if (!/.+@.+/.test(this.packageWithVersion)) {
      // for some reason, the above validator does not throw an error /shrug
      throw new Error(`${this.packageWithVersion} needs a name or version`);
    }

    const pkg = this.packageWithVersion.replace(/(.+)@.+/, "$1");
    const version = this.packageWithVersion.replace(/.+@(.+)/, "$1");

    const workspaces = JSON.parse(
      child
        .execSync(`yarn workspaces info --json`, {
          cwd: rootPath,
        })
        .toString()
    );

    Object.keys(workspaces).forEach((workspace) => {
      const location = workspaces[workspace]?.location;

      this.context.stdout.write(infoStyle(`Upgrading ${workspace}...\n`));

      const pkgJson = loadJson(path.join(rootPath, location, "package.json"));
      const { dependencies, devDependencies, peerDependencies } = pkgJson;

      if (peerDependencies?.[pkg]) {
        // there doesn't seem to be a way to upgrade peer deps except by
        // manually changing the semversion
        this.context.stdout.write(
          infoStyle(`${workspace}: peer dependency found for ${pkg}...\n`)
        );
        peerDependencies[pkg] = version;
        saveJson(path.join(rootPath, location, "package.json"), pkgJson);
      }

      if (devDependencies?.[pkg]) {
        this.context.stdout.write(
          infoStyle(`${workspace}: dev dependency found for ${pkg}...\n`)
        );
        // there seems to be a yarn bug where upgrading the dev version does not
        // bump the version number
        devDependencies[pkg] = version;
        saveJson(path.join(rootPath, location, "package.json"), pkgJson);
        child.execSync(`yarn upgrade --dev ${this.packageWithVersion}`, {
          cwd: path.join(rootPath, location),
        });
      }

      if (dependencies?.[pkg]) {
        this.context.stdout.write(
          infoStyle(`${workspace}: dependency found for ${pkg}...\n`)
        );
        child.execSync(`yarn upgrade ${this.packageWithVersion}`, {
          cwd: path.join(rootPath, location),
        });
      }
    });
  }
}
