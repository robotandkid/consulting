import { Command } from "clipanion";
import { rootBash } from "../helpers";

export class ListWorkspaces extends Command {
  static paths = [["workspaces:list"]];

  async execute() {
    const workspaces = rootBash`yarn workspaces info --json`.toString();

    this.context.stdout.write(`${workspaces}\n`);
  }
}
