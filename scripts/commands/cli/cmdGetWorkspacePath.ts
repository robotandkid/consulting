import { Command, Option } from "clipanion";
import path from "path";
import { rootPath, withOnceStream } from "../helpers";

export class GetWorkspacePath extends Command {
  static paths = [["workspace:get:path"]];

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  workspace = Option.String({
    required: true,
  })!;

  static usage = Command.Usage({
    category: `cli`,
    description: `Returns the absolute path to the given workspace`,
    examples: [["", "yarn workspace:get:path @robotandkid/uriel"]],
  });

  async execute() {
    const [workspaces, setWorkspaces] = withOnceStream();

    await this.cli.run(["workspaces:list"], {
      stdout: setWorkspaces,
    });

    const relLocation = JSON.parse(await workspaces)[this.workspace]?.location;

    if (!relLocation) throw new Error(`${this.workspace} was not found!`);

    const location = path.join(rootPath, relLocation);

    this.context.stdout.write(`${location}\n`);
  }
}
