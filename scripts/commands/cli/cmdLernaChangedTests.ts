import { Command } from "clipanion";
import { rootBash, withOnceStream } from "../helpers";

export class LernaChangedTests extends Command {
  static paths = [["workspaces:changed:test"]];

  async execute() {
    const [workspaces, setWorkspaces] = withOnceStream();

    await this.cli.run(["workspaces:changed"], {
      stdout: setWorkspaces,
    });

    const list = (await workspaces).split("\n");

    list.forEach((workspace) => {
      rootBash`
        yarn lerna run test --scope ${workspace}
      `;
    });
  }
}
