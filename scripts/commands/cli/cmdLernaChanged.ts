import { Command } from "clipanion";
import { rootBash } from "../helpers";

export class LernaChanged extends Command {
  static paths = [["workspaces:changed"]];

  async execute() {
    const result = rootBash`
      yarn lerna changed --all -p --json
    `
      .toString()
      .split("\n");

    result.shift();

    const list: Array<{ name: string; version: string }> = JSON.parse(
      result.join("\n")
    );
    const workspaces = list.map((workspace) => workspace.name);

    this.context.stdout.write(`${workspaces.join("\n")}\n`);
  }
}
