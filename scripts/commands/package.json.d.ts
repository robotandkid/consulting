declare type PackageJson = {
  scripts: Record<string, string>;
};
